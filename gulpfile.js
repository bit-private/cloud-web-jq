var gulp = require('gulp');
var sass = require('gulp-sass');
var cssminify = require('gulp-minify-css');
var browserSync = require('browser-sync');
var contentIncluder = require('gulp-content-includer');
var rename = require('gulp-rename');
var cheerio = require('gulp-cheerio');
var babel = require('gulp-babel');
var fileinclude = require('gulp-file-include');
/* var $ = require('./src/public/static/jquery-1.12.4.min.js') */


gulp.task('concatHtml', function() {
    gulp.src("src/**/*.html")
        .pipe(contentIncluder({
            includerReg: /<!\-\-include\s+"([^"]+)"\-\->/g,
            /* deepConcat:true */ //递归合并
        }))
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(cheerio(function($) {
            //$('#mockData').remove();
        }))
        .pipe(gulp.dest('./dist'));
});


gulp.task('jsTask', function() {
    gulp.src("src/**/*.js")
        .pipe(gulp.dest('./dist'));

    gulp.src("src/views/**/*.js")
        // .pipe(babel({
        //     presets: ['@babel/env']
        // }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('imgTask', function() {
    gulp.src(['src/**/*.jpg', 'src/**/*.png'])
        .pipe(gulp.dest('./dist'));
});

gulp.task('cssTask', function() {
    gulp.src("src/**/*.css")
        .pipe(gulp.dest('./dist'));
});

gulp.task('copyPublic', function() {
    gulp.src("src/public/**/*.*")
        .pipe(gulp.dest('./dist/public'));
});



gulp.task('serve', ['concatHtml', 'jsTask', 'imgTask', 'cssTask', 'copyPublic'], function() {
    browserSync.init({
        //server: "./dist"
        server: {
            baseDir: './dist', // 启动服务的目录 默认 index.html    
            index: 'views/index.html' // 自定义启动文件名
        }
    });

    /* server: {
            baseDir: './dist', // 启动服务的目录 默认 index.html    
            index: 'views/login.html' // 自定义启动文件名
        }, */

    gulp.watch("src/**/*.html", ['concatHtml']);

    gulp.watch("src/**/*.js", ['jsTask']);
    gulp.watch("src/**/img/*.*", ['imgTask']);
    gulp.watch("src/**/*.css", ['cssTask']);
    gulp.watch("./dist/**/*.*").on('change', browserSync.reload);
});

gulp.task('default', ['serve'])