function getCookie(name) {
  var cookie = document.cookie
  // console.log(cookie);
  var arr = cookie.split("; ") //将字符串分割成数组
  // console.log(arr);
  for (var i = 0; i < arr.length; i++) {
    var arr1 = arr[i].split("=")
    if (arr1[0] == name) {
      return unescape(arr1[1])
    }
  }
  return "GG"
}

var token = getCookie("token")
$.ajaxSetup({
  // contentType:"application/x-www-form-urlencoded;charset=utf-8",
  headers: {
    token: token
  },
  crossDomain: true,
  xhrFields: {
    withCredentials: true // 要在这里设置 跨域设置cookie
  },
  complete: function(XMLHttpRequest, textStatus) {
    // console.log(XMLHttpRequest,textStatus)
    if (XMLHttpRequest.status == 401) {
      window.location.href = "/"
    }
  }
})
