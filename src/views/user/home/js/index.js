
(function () {
  layui.use(['element', 'table', 'layer', 'jquery', 'form', 'tree', 'laypage'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var $ = layui.jquery;
    var form = layui.form;
    var tree = layui.tree;
    var laypage = layui.laypage;

    var userId = getCookie("userId"); // 获取用户id

    // var firstLogin = getCookie('isFirstLogin')
    // if(firstLogin == 'false'){
    //   // console.log(this)
    // }

    $.ajax({ // 我的连接 表格数据
      type: "POST",
      url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
      // url: 'http://10.1.1.213:9006/AppConnection/appConnectionPageForUser/10/0',
      data: JSON.stringify({
        "userId": userId
      }), // id的参数obj
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      dataType: "json",
      success: function (data) {
        console.log(data)
        table.render({
          elem: "#tableData",
          data: data.data.content,
          limit: 10,
          page: true,
          id: 'idTest',
          cols: [
            [{
              checkbox: true,
              fixed: true
            },
            {
              field: 'appName',
              title: '软件',
              sort: true,
              event: 'hover',
              'unresize': true
            },
            {
              field: 'equipmentName',
              title: '服务器',
              sort: true,
              'unresize': true
            },
            {
              field: 'ip',
              title: 'IP',
              sort: true,
              'unresize': true
            },
            {
              field: 'openTime',
              title: '打开时间',
              sort: true,
              'unresize': true
            },
            {
              field: 'leaveTime',
              title: '闲置时间',
              sort: true,
              templet: function (d) {
                return d.leaveTime + '小时'
              },
              'unresize': true
            },
            {
              field: 'operation',
              title: '操作',
              // event: 'click',
              toolbar: '#conntClick',
              'unresize': true
            }
            ]
          ]
        });
      }
    });
    // console.log(url)
    //var url = "http://10.1.1.132:9006";  // 佳豪
    // const GETAPPLICATION = url + ':9001/application/getDictionaryList';  //获取应用 - 书军9002
    const GETAPPLICATION = url + ':9001/application/getApplicationType'; //获取应用 - 书军9002
    const BYUSERID = url + ':9001/application/getApplicationListByUserId'; // 授权软件 - 书军
    const GETSYSTEMUSER = url + ':9001/appConnection/getSysUserListByUserId'; // 获取系统用户 - 书军
    const OPENSOFTWARE = url + ':9006/AppConnection/open'; // 打开软件 - 佳豪
    const DOWNLOAD = url + ':9006/AppConnection/downLoadVnc'; // 下载连接 - 佳豪


    var userId = getCookie("userId");
    var userName = getCookie("userName");
    var token1 = getCookie("token1");
    var token3 = getCookie("token3");
    var loginPageAccess = getCookie('loginPageAccess');
    if (loginPageAccess == 1) {
      if (token1 === '') {
        window.location.href = url + port + "/dist/views";
      }
    } else if (loginPageAccess == 3) {
      if (token3 === '') {
        window.location.href = url + port + "/dist/views";
      }
    } else if (loginPageAccess == '') {
      window.location.href = url + port + "dist/views";
    }

    // console.log(userId1)
    // if(token3 === ''){
    //   window.location.href = url + ":8080/dist/views";
    // }

    $.ajax({ // 获取我的应用
      type: "POST",
      url: GETAPPLICATION,
      // data:{typeKey:'app_type'},
      data: {
        "userId": userId
      },
      success: function (data) {
        // console.log(data.data)
        var res = data.data,
          inputs;
        for (var i = 0; i < res.length; i++) { // 循环 动态创建单选input
          // console.log(res[i])
          inputs = $("<input type='radio'lay-filter='acId' value=" + res[i].typeKey + " name='radio' title=" + res[i].value + " >");
          $(".input-block").append(inputs)
        }
        $(".input-block").prepend("<input type='radio'lay-filter='acId' value='all' name='radio' title='显示全部'>")
        form.render('radio')
      }
    })
    $.ajax({ // 默认先显示所有软件
      type: "POST",
      url: BYUSERID,
      data: {
        userId: userId
      }, // useId根据平台用户获取,
      success: function (data) {
        console.log(data)
        // console.log(data.data.length)
        var data = data.data;
        var lis = '';
        for (var i = 0; i < data.length; i++) {
          lis += `<li class="layui-form-item">
                      <div class="software-show">
                          <div class="software-show-left">
                              <img width="50" height="50" src="./../../../../${data[i].path}" />
                              <select id="screen" lay-filter="screen">
                                  <option value=""></option>
                                  <option value="1" selected="">1屏幕</option>
                                  <option value="2">2屏幕</option>
                                  <option value="3">3屏幕</option>
                                  <option value="4">4屏幕</option>
                              </select>
                          </div>
                          <div class="software-show-right">
                              <h3>${data[i].softName}</h3>
                              <p>${data[i].versionName}</p>
                              <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${data[i].systemId}" data-appId="${data[i].id}" data-isTimeout="${data[i].isTimeout}">打开软件</button>
                          </div>
                      </div>
                  </li>`;
        }
        $(".software-box").append(lis)
        form.render('select')
      }
    });

    var searchInfo = ''
    // 点击站内信
    $("#sendMessage").on("click", function () { // 点击发送消息
      layer.open({
        type: 1,
        title: "详情", //不显示标题栏
        closeBtn: 1,
        area: ['50%', '400px'],
        shade: 0.3,
        id: 'message', //设定一个id，防止重复弹出
        btn: ['确定'],
        btnAlign: 'c',
        moveType: 1, //拖拽模式，0或者1
        content: $('#messageDiaog'),
        yes: function (index) {
          layer.close(index);
        },
        end: function (params) {
          $('#messageDiaog').hide()
        }
      });

      $.ajax({ // 切换选项卡到发送记录,查询消息
        type: "POST",
        url: url + ':9006/Message/putMessageHistory',
        // url: 'http://10.1.1.213:9006/Message/putMessageHistory',
        data: JSON.stringify({
          "userId": userId,
          "searchInfo": searchInfo
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          console.log(data)
          if (data.code === 200) {
            var data = data.data;
            var messageInfo = '';
            for (var i = 0; i < data.length; i++) {
              console.log(data[i])
              messageInfo +=
                `<div class="tips-list">
                                  <div>${data[i].datetime}</div>

                                  <p>通知：${data[i].note}</p>
                              </div><br/>`
            }
            $(".tips-box").append(messageInfo)
          }
        }
      })


      if ($('.layui-badge').html() > 0) {
        $.ajax({ // 按userid收消息
          type: "POST",
          url: url + ':9006/Message/updateState/' + userId,
          // url: 'http://10.1.1.213:9006/Message/updateState/' + userId,
          // data: JSON.stringify({ "userId": userId, "searchInfo": searchInfo }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function (data) {
            $('.layui-badge').html('0')
          }
        })
      }
    })
    // 历史记录搜索
    $("#history-btn").click(function () { // 点击搜索按钮
      searchInfo = $("#history-input").val();
      $(".tips-box").html('');
      console.log(searchInfo)
      $.ajax({ // 点击搜索按钮,查询消息
        type: "POST",
        url: url + ':9006/Message/putMessageHistory',
        // url: 'http://10.1.1.213:9006/Message/putMessageHistory',
        data: JSON.stringify({
          "userId": userId,
          "searchInfo": searchInfo
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          console.log(data)
          if (data.code === 200) {
            var data = data.data;
            var messageInfo = '';
            for (var i = 0; i < data.length; i++) {
              console.log(data[i])
              messageInfo +=
                `<div class="tips-list">
                                  <div>${data[i].datetime}</div>

                                  <p>通知：${data[i].note}</p>
                              </div><br/>`
            }
            // console.log(messageInfo)
            $(".tips-box").append(messageInfo)
          } else {
            layer.msg(data)
          }
        }
      })
    })







    form.on('radio(acId)', function (data) { // 根据单选按钮的选择显示软件列表
      console.log(data)
      var acId = data.value
      if (acId == 'all') {
        $(".software-box").html('')
        $.ajax({ // 默认先显示所有软件
          type: "POST",
          url: BYUSERID,
          data: {
            userId: userId
          }, // useId根据平台用户获取,
          success: function (data) {
            console.log(data)
            // console.log(data.data.length)
            var data = data.data;
            var lis = '';
            // console.log( $(".software-box") )
            for (var i = 0; i < data.length; i++) {
              // console.log(data[i])
              lis += `<li class="layui-form-item">
                          <div class="software-show">
                              <div class="software-show-left">
                                  <img width="50" height="50" src="./../../../../${data[i].path}" />
                                  <select id="screen" lay-filter="screen">
                                      <option value=""></option>
                                      <option value="1" selected="">1屏幕</option>
                                      <option value="2">2屏幕</option>
                                      <option value="3">3屏幕</option>
                                      <option value="4">4屏幕</option>
                                  </select>
                              </div>
                              <div class="software-show-right">
                                  <h3>${data[i].softName}</h3>
                                  <p>${data[i].versionName}</p>
                                  <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${data[i].systemId}" data-appId="${data[i].id}" data-isTimeout="${data[i].isTimeout}">打开软件</button>
                              </div>
                          </div>
                      </li>`;
            }
            $(".software-box").append(lis)
            form.render('select')
          }
        });
      }
      $(".software-box").html('')
      $.ajax({
        type: "POST",
        url: BYUSERID,
        data: {
          userId: userId,
          acId: acId
        }, // useId根据平台用户获取,
        success: function (data) {
          console.log(data)
          // console.log(data.data.length)
          var data = data.data;
          var lis = '';
          // console.log( $(".software-box") )
          for (var i = 0; i < data.length; i++) {
            // console.log(data[i])
            lis += `<li class="layui-form-item">
                          <div class="software-show">
                              <div class="software-show-left">
                                <img width="50" height="50" src="./../../../../${data[i].path}" />
                                  <select id="screen" lay-filter="screen">
                                      <option value=""></option>
                                      <option value="1" selected="">1屏幕</option>
                                      <option value="2">2屏幕</option>
                                      <option value="3">3屏幕</option>
                                      <option value="4">4屏幕</option>
                                  </select>
                              </div>
                              <div class="software-show-right">
                                  <h3>${data[i].softName}</h3>
                                  <p>${data[i].versionName}</p>
                                  <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${data[i].systemId}" data-appId="${data[i].id}" data-isTimeout="${data[i].isTimeout}">打开软件</button>
                              </div>
                          </div>
                      </li>`;
          }
          $(".software-box").append(lis)
          form.render('select')
        }
      });
    })
    var screenValue = 1;
    var width = screen.width;
    var height = screen.height;
    var selects = width + "x" + height;
    form.on('select(screen)', function (data) { // 获取屏幕数量
      console.log(data);
      screenValue = data.value;
      selects = (width * screenValue) + "x" + height;
    });

    // 点击打开软件按钮
    $(".software-box").on('click', 'button', function (e) {
      // console.log(this)
      // var userId = this.getAttribute("data-userId");
      var appId = this.getAttribute("data-appid");
      var systemId = this.getAttribute("data-systemId");
      var isTimeout = this.getAttribute("data-isTimeout");
      if (isTimeout == 1) {
        layer.msg('软件已过期!')
        return false
      }
      console.log(userId, appId)
      $.ajax({ // 根据平台用户获取系统用户
        type: 'POST',
        url: GETSYSTEMUSER,
        // url: 'http://10.1.1.234:9001/appConnection/getSysUserListByUserId',
        data: {
          "userId": userId,
          "systemId": systemId
        }, // useId根据平台用户获取
        async: false,
        success: function (data) {
          console.log(data)
          var res = data.data;
          var htmlValue = '';
          var systemUserId = '';
          for (var i = 0; i < res.length; i++) {
            htmlValue += `<form class="layui-form">
                                <div class="layui-form-item">
                                  
                                  <div class="input-block">
                                    <input type="radio" name="radio" lay-filter='sysUserId' value="${res[i].sysUserId}" title="${res[i].name}">
                                  </div>
                            </div>`
          }
          form.on('radio(sysUserId)', function (data) { // 弹出框选择单选按钮
            console.log(data.value)
            systemUserId = data.value;
          })
          if (res.length === 0) {
            layer.msg('没有可用的系统用户')
            return false
          } else if (res.length === 1) {
            var systemUserId = res[0].sysUserId
            $.ajax({ // 打开软件 功能
              type: "POST",
              url: OPENSOFTWARE,
              // url:'http://10.1.1.213:9006/AppConnection/open',
              data: JSON.stringify({
                "userId": userId,
                "appId": appId,
                "systemUserId": systemUserId,
                "screenCount": screenValue,
                "resolution": selects
              }), // 传用户id和软件id
              contentType:"application/json;charset=utf-8",
              dataType: "json",
              success: function (data) { // 下载软件
                console.log(data)
                var data_ = data;
                if (data.code === 200) {
                  var data = data.data;
                  var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
                  form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
                  form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
                  form.appendTo('body').submit().remove();

                  $.ajax({ // 表格数据
                    type: "POST",
                    // url: url + ":9006/AppConnection/appConnectionPage/10/0/appName/desc",
                    url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
                    data: JSON.stringify({
                      "userId": userId
                    }), // id的参数obj
                    contentType:"application/json;charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                      console.log(data)
                      table.render({
                        elem: "#tableData",
                        data: data.data.content,
                        limit: 10,
                        page: true,
                        id: 'idTest',
                        cols: [
                          [{
                            checkbox: true,
                            fixed: true
                          },
                          {
                            field: 'appName',
                            title: '软件',
                            sort: true,
                            event: 'hover',
                            'unresize': true
                          },
                          {
                            field: 'equipmentName',
                            title: '服务器',
                            sort: true,
                            'unresize': true
                          },
                          {
                            field: 'ip',
                            title: 'IP',
                            sort: true,
                            'unresize': true
                          },
                          {
                            field: 'openTime',
                            title: '打开时间',
                            sort: true,
                            'unresize': true
                          },
                          {
                            field: 'leaveTime',
                            title: '闲置时间',
                            sort: true,
                            templet: function (d) {
                              return d.leaveTime + '小时'
                            },
                            'unresize': true
                          },
                          {
                            field: 'operation',
                            title: '操作',
                            sort: true,
                            event: 'click',
                            toolbar: '#conntClick',
                            'unresize': true
                          }
                          ]
                        ]
                      });
                    }
                  });
                  layer.msg(data_.message)
                  // layer.close(loading);
                } else {
                  layer.msg(data.message)
                  // layer.close(loading);
                }
              }
            });
            return false
          } else {
            layer.open({ // 弹出选择系统用户窗口
              title: '系统用户',
              content: htmlValue,
              area: ['300px', '400px'],
              yes: function (index) {
                layer.close(index);
                console.log(systemUserId) // 系统用户id
                var loading = layer.load(0, {
                  shade: [0.8, '#393D49']
                })
                if (systemUserId != '') {
                  $.ajax({ // 打开软件 功能
                    type: "POST",
                    url: OPENSOFTWARE,
                    // url:'http://10.1.1.213:9006/AppConnection/open',
                    data: JSON.stringify({
                      "userId": userId,
                      "appId": appId,
                      "systemUserId": systemUserId,
                      "screenCount": screenValue,
                      "resolution": selects
                    }), // 传用户id和软件id
                    contentType:"application/json;charset=utf-8",
                    dataType: "json",
                    success: function (data) { // 下载软件
                      // console.log(JSON.stringify(data))
                      if (data.code === 200) {
                        var data = data.data;
                        var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
                        form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
                        form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
                        form.appendTo('body').submit().remove();

                        $.ajax({ // 表格数据
                          type: "POST",
                          // url: url + ":9006/AppConnection/appConnectionPage/10/0/appName/desc",
                          url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
                          data: JSON.stringify({
                            "userId": userId
                          }), // id的参数obj
                          headers: {
                            "Content-Type": "application/json;charset=utf-8"
                          },
                          dataType: "json",
                          success: function (data) {
                            console.log(data)
                            table.render({
                              elem: "#tableData",
                              data: data.data.content,
                              limit: 10,
                              page: true,
                              id: 'idTest',
                              cols: [
                                [{
                                  checkbox: true,
                                  fixed: true
                                },
                                {
                                  field: 'appName',
                                  title: '软件',
                                  sort: true,
                                  event: 'hover',
                                  'unresize': true
                                },
                                {
                                  field: 'equipmentName',
                                  title: '服务器',
                                  sort: true,
                                  'unresize': true
                                },
                                {
                                  field: 'ip',
                                  title: 'IP',
                                  sort: true,
                                  'unresize': true
                                },
                                {
                                  field: 'openTime',
                                  title: '打开时间',
                                  sort: true,
                                  'unresize': true
                                },
                                {
                                  field: 'leaveTime',
                                  title: '闲置时间',
                                  sort: true,
                                  templet: function (d) {
                                    return d.leaveTime + '小时'
                                  },
                                  'unresize': true
                                },
                                {
                                  field: 'operation',
                                  title: '操作',
                                  sort: true,
                                  event: 'click',
                                  toolbar: '#conntClick',
                                  'unresize': true
                                }
                                ]
                              ]
                            });
                          }
                        });
                        layer.close(loading);
                      } else {
                        layer.msg(data.message)
                        layer.close(loading);
                      }
                    }
                  });
                } else {
                  layer.msg('请选择一个用户')
                }
              }
            });
          }
          form.render('radio')
        }
      })
    })

    var fnTool = {
      init: function (params) {
        this.loop();

      },
      /* 个人详情 */
      openInfo: function () {
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['95%', '95%'],
          shade: 0.3,
          id: 'user_info', //设定一个id，防止重复弹出
          btn: ['确认'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#userInfo'),
          end: function () {
            $("#userInfo").hide();
          }
        });
      },
      /* 文字滚动 */
      loop: function (params) {
        var speed = 30;
        var msgSHow = document.getElementById("msg-box-show");
        var msgCopy = document.getElementById("msg-box-copy");
        var msgBar = document.getElementById("msg-bar");

        msgCopy.innerHTML = msgSHow.innerHTML;

        function Marquee (params) {
          if (msgCopy.offsetLeft - msgBar.scrollLeft <= 0) {
            msgBar.scrollLeft -= msgSHow.offsetWidth;
            // msgCopy.offsetLeft = 0
          } else {
            msgBar.scrollLeft++;
          }
        }
        var myLoop = setInterval(Marquee, speed);
        msgBar.onmouseenter = function () {
          clearInterval(myLoop)
        }
        msgBar.onmouseleave = function () {
          myLoop = setInterval(Marquee, speed)
        }
      },
      graphsPie: function (params) { // 软件监控图
        $.ajax({
          type: 'POST',
          url: url + ':9006/AppMonitorHistory/WeekOrMonth/30',
          data: JSON.stringify({
            'userId': userId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            if (data.data != '') {
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = []
              for (var key in data.data[0]) { // 将软件名push进数组
                software.push(key)
              }
              software = software[index]

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i])
                xName.push(datas[software][i]['X1'].date)
                userCount.push(datas[software][i]['X1'].count)
                if (datas[software][i]['X2'].mNames != '') { // 计算占满天数
                  fullDay++
                }
                if (datas[software][i]['X2'].isfull === 'yes') { // 计算是否占满
                  isFull.push(2)
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('')
                }
              }
              console.log(isFull, index)
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function (params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value'
                },
                series: [{
                  name: '许可占满',
                  type: 'bar',
                  color: ['#EEB422'],
                  stack: '总量',
                  data: isFull,
                  barMaxWidth: 80,
                },
                {
                  name: '使用人数',
                  type: 'line',
                  color: ['#66CD00'],
                  data: userCount
                },
                ]
              };
              var myChart = echarts.init(document.getElementById('graphs-left'));
              myChart.setOption(option);
            } else {
              layer.msg('暂无数据')
            }

          }
        })
        // var myChart = echarts.init(document.getElementById('graphs-left'));
        // myChart.setOption(option);
      },
      graphsLine: function (params) { // 线图
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/WorkTimeCountByTimeType/30',
          data: JSON.stringify({
            "userId": userId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            // console.log(data)
            var dates = [];
            var datas = [];
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              // console.log(data[i].timeCount)
              var arr = data[i].timeCount.split("  ");
              var arr1 = arr[1].split("小")
              // console.log(arr)
              dates.push(arr[0])
              datas.push(arr1[0])
              // console.log(arr1)
            }
            // console.log(dates,datas)
            option = {
              title: {
                text: '近30天登录平台时间(小时)',
                // subtext: '纯属虚构',
                x: 'center'
              },
              xAxis: {
                type: 'category',
                data: dates
              },
              tooltip: {
                trigger: 'axis'
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                data: datas,
                type: 'line'
              }]
            };

            var myChart = echarts.init(document.getElementById('graphs-right'));
            myChart.setOption(option);
          }
        })

      },
      tableInfoFn: function (params) {
        table.render({
          elem: '#tableInfo',
          // url: '/mock/table',
          // method: 'post',
          cols: [
            [{
              field: 'id',
              title: '用户',
              // width: 80,
              // sort: true,
              // event: 'hover',
              // style: 'cursor: pointer;'
            }, {
              field: 'username',
              title: '系统用户'
            }, {
              field: 'sex',
              title: '科室',
              // sort: true
            }, {
              field: 'city',
              title: 'IP'
            }, {
              field: 'sign',
              title: '软件'
            }]
          ],
          id: 'tableInfo',
          page: {
            count: 1000,
            layout: ['prev', 'page', 'next', 'skip'],
          }
        });
      }
    }
    fnTool.init();

    $("#userInfoLog").on("click", function (params) { // 点击个人信息
      fnTool.graphsPie(); // 饼图
      fnTool.graphsLine(); // 线图
      // fnTool.tableInfoFn();  // 表格
      fnTool.openInfo(); // 弹窗

    })

    element.on('tab(tab)', function (data) { //选项卡切换
      // console.log(data.index);
      // var userId = getCookie("userId");

      $(".tips-box").html('');
      if (data.index === 0) {
        $("#share-search-val").val("") // 切换回 我的连接时, 清除我的分享input里面的值
        $.ajax({ // 表格数据
          type: "POST",
          url: url + ":9006/AppConnection/appConnectionPageForUser/10/0/appName/desc",
          // url: "http://10.1.1.213:9006/AppConnection/appConnectionPage/10/0",
          data: JSON.stringify({
            "userId": userId
          }), // id的参数obj
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            table.render({
              elem: "#tableData",
              data: data.data.content,
              limit: 10,
              page: true,
              id: 'idTest',
              cols: [
                [{
                  checkbox: true,
                  fixed: true
                },
                {
                  field: 'appName',
                  title: '软件',
                  sort: true,
                  event: 'hover',
                  'unresize': true
                },
                {
                  field: 'equipmentName',
                  title: '服务器',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'ip',
                  title: 'IP',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'openTime',
                  title: '打开时间',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'leaveTime',
                  title: '闲置时间',
                  sort: true,
                  templet: function (d) {
                    return d.leaveTime + '小时'
                  },
                  'unresize': true
                },
                {
                  field: 'operation',
                  title: '操作',
                  sort: true,
                  event: 'click',
                  toolbar: '#conntClick',
                  'unresize': true
                }
                ]
              ]
            });
          }
        });
      }
      if (data.index === 1) {
        $("#link-search-val").val("") // 切换回 我的分享时, 清除我的连接input里面的值
        $.ajax({ // 分享分页
          type: "POST",
          url: url + ':9006/AppShare/pageInfo/10/0/datetime/desc',
          data: JSON.stringify({
            "shareId": userId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function (data) {
            console.log(data)
            table.render({ // 我的分享
              elem: '#tableData1',
              data: data.data.content,
              id: 'shareTable',
              // method: 'post',
              limit: 10,
              page: true,
              cols: [
                [{
                  checkbox: true,
                  fixed: true
                }, {
                  field: 'datetime',
                  title: '时间',
                  sort: true,
                  'unresize': true
                }, {
                  field: 'app_name',
                  title: '软件',
                  sort: true,
                  'unresize': true
                }, {
                  field: 'type',
                  title: '状态',
                  sort: true,
                  templet: function (d) {
                    console.log(userName)
                    if (d.status== 1) {
                      return '无分享'
                    }else if(d.status== 2) {
                      return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                    }else if(d.status== 3) {
                      return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                    }
                  },
                  'unresize': true
                }, {
                  field: 'userName',
                  title: '用户',
                  sort: true,
                  templet: function (d) {
                    // console.log(d)
                    if(d.status == 1) {
                      return '无分享'
                    }else if(d.status == 2) {
                      return d.realName
                    }else if(d.status == 3) {
                      return d.shareRealName
                    }
                    // if (d.shareUserId === userId) {
                    //   return d.userName || '未分享'
                    // } else {
                    //   return d.shareUserName
                    // }
                  },
                  'unresize': true
                }, {
                  field: 'operation',
                  title: '操作',
                  sort: true,
                  event: 'click',
                  width: 280,
                  templet: function (d) {
                    if (d.shareUserId === userId) {
                      return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                    } else {
                      return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                    }
                  },
                  'unresize': true
                }]
              ]
            });
          }
        })
      }
    });

    $("#link-search").click(function () { // 我的连接搜索按钮
      var searchValue = $("#link-search-val").val()
      // console.log($("#link-search-val").val())
      $.ajax({ // 表格数据
        type: "POST",
        url: url + ":9006/AppConnection/appConnectionPageForUser/10/0/appName/desc",
        // url:  "http://10.1.1.213:9006/AppConnection/appConnectionPageForUser/10/0",
        data: JSON.stringify({
          "userId": userId,
          "objValue": searchValue
        }), // id的参数obj
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          table.render({
            elem: "#tableData",
            data: data.data.content,
            limit: 10,
            page: true,
            cols: [
              [{
                checkbox: true,
                fixed: true
              },
              {
                field: 'appName',
                title: '软件',
                sort: true,
                event: 'hover',
                'unresize': true
              },
              {
                field: 'equipmentName',
                title: '服务器',
                sort: true,
                'unresize': true
              },
              {
                field: 'ip',
                title: 'IP',
                sort: true,
                'unresize': true
              },
              {
                field: 'openTime',
                title: '打开时间',
                sort: true,
                'unresize': true
              },
              {
                field: 'leaveTime',
                title: '闲置时间',
                sort: true,
                templet: function (d) {
                  return d.leaveTime + '小时'
                },
                'unresize': true
              },
              {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                toolbar: '#conntClick',
                'unresize': true
              }
              ]
            ]
          });
        }
      });
    })
    $("#share-search").click(function () { // 我的分享搜索按钮
      var searchValue = $("#share-search-val").val()
      // console.log($("#link-search-val").val())
      $.ajax({ // 分享分页
        type: "POST",
        url: url + ':9006/AppShare/pageInfo/10/0/datetime/desc',
        // url:  'http://10.1.1.213:9006/AppShare/pageInfo/10/0',
        data: JSON.stringify({
          "shareId": userId,
          "appName": searchValue
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          console.log(data)
          table.render({ // 我的分享
            elem: '#tableData1',
            data: data.data.content,
            id: 'shareTable',
            page: true,
            // method: 'post',
            cols: [
              [{
                checkbox: true,
                fixed: true
              }, {
                field: 'datetime',
                title: '时间',
                sort: true,
                'unresize': true
              }, {
                field: 'app_name',
                title: '软件',
                sort: true,
                'unresize': true
              }, {
                field: 'type',
                title: '状态',
                sort: true,
                templet: function (d) {
                  console.log(userName)
                  if (d.status== 1) {
                    return '无分享'
                  }else if(d.status== 2) {
                    return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                  }else if(d.status== 3) {
                    return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                  }
                },
                'unresize': true
              }, {
                field: 'userName',
                title: '用户',
                sort: true,
                templet: function (d) {
                  console.log(d)
                  if (d.shareUserId === userId) {
                    return d.userName || '未分享'
                  } else {
                    return d.shareUserName
                  }
                },
                'unresize': true
              }, {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                width: 280,
                templet: function (d) {
                  if (d.shareUserId === userId) {
                    return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                  } else {
                    return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                  }
                },
                'unresize': true
              }]
            ]
          });
        }
      })
    })

    table.on('tool(tableData1)', function (obj) { // 我的分享点击操作列的按钮
      // console.log(obj)
      var connId = obj.data.connId;
      var shareId = getCookie("userId");
      var shareUserId = obj.data.shareUserId;
      if (obj.event === 'shareApp') {
        // $("#brand_sel").html('')  // 每次点击的时候都 清空brand_sel中的内容
        // $("#choose_sel").html('')  // 每次点击的时候都 清空choose_sel中的内容
        $.ajax({ // 获取左侧树状图
          type: 'GET',
          url: url + ":9006/AppConnection/departmentTree",
          success: function (data) {
            // console.log(data)
            $("#treeList").html('')
            var data = data.data;
            tree({
              elem: '#treeList',
              nodes: data,
              click: function (node) {
                console.log(node)
                orgId = node.id
                $("#brand_sel").html("")
                var $select = $($(this)[0].elem).parents(".layui-form-select");
                $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='selectID']").val(node.id);
                $.ajax({
                  type: "POST",
                  url: url + ":9006/UserMonitorHistory/FindByOrgId",
                  data: JSON.stringify({
                    "orgId": orgId
                  }),
                  headers: {
                    "Content-Type": "application/json;charset=utf-8"
                  },
                  dataType: "json",
                  success: function (data) {
                    console.log(data)
                    var data = data.data;
                    var root = document.getElementById("brand_sel");
                    for (var i = 0; i < data.length; i++) {
                      var option = document.createElement("option");
                      // option.setAttribute("value", data[i].groupName);
                      // option.setAttribute("title", data[i].id);
                      option.setAttribute("dataid", data[i].id);
                      option.innerText = data[i].userName;
                      root.appendChild(option);
                      form.render("select");
                      // console.log(data[i].groupName)
                    }
                  }
                })
              }

            });
          }
        })
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['690px', '335px'],
          shade: 0.3,
          id: 'message1', //设定一个id，防止重复弹出
          btn: ['确定'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $("#userAddDiaog"),
          yes: function (index) {
            layer.close(index);
            var optionsId = $("#choose_sel option")
            var userIds = '';
            // var shareId = getCookie("userId");

            for (var i = 0; i < optionsId.length; i++) {
              userIds += "," + optionsId[i].getAttribute("dataid") // 将逗号拼接在前面
              // userIds.push(optionsId[i].value)
            }
            userIds = userIds.substring(1); // 截取 userIds 字符串最前面的逗号
            console.log(userIds)
            $.ajax({ // 查询分享链接数据
              type: "POST",
              url: url + ':9006/AppShare/share',
              // url: 'http://10.1.1.213:9006/AppShare/share',
              data: JSON.stringify({
                "userIds": userIds,
                "shareId": shareId,
                "appConnectionId": connId
              }),
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              success: function (data) {
                console.log(data)
                layer.msg(data.message)
                $.ajax({ // 分享分页
                  type: "POST",
                  url: url + ':9006/AppShare/pageInfo/10/0/datetime/desc',
                  data: JSON.stringify({
                    "shareId": userId
                  }),
                  headers: {
                    "Content-Type": "application/json;charset=utf-8"
                  },
                  success: function (data) {
                    console.log(data)
                    table.render({ // 我的分享
                      elem: '#tableData1',
                      data: data.data.content,
                      id: 'shareTable',
                      page: true,
                      // method: 'post',
                      cols: [
                        [{
                          checkbox: true,
                          fixed: true
                        }, {
                          field: 'datetime',
                          title: '时间',
                          sort: true,
                          'unresize': true
                        }, {
                          field: 'app_name',
                          title: '软件',
                          sort: true,
                          'unresize': true
                        }, {
                          field: 'type',
                          title: '状态',
                          sort: true,
                          templet: function (d) {
                            console.log(userName)
                            if (d.status== 1) {
                              return '无分享'
                            }else if(d.status== 2) {
                              return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                            }else if(d.status== 3) {
                              return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                            }
                          },
                          'unresize': true
                        }, {
                          field: 'userName',
                          title: '用户',
                          sort: true,
                          templet: function (d) {
                            console.log(d)
                            if (d.shareUserId === userId) {
                              return d.userName || '未分享'
                            } else {
                              return d.shareUserName
                            }
                          },
                          'unresize': true
                        }, {
                          field: 'operation',
                          title: '操作',
                          sort: true,
                          event: 'click',
                          width: 280,
                          templet: function (d) {
                            if (d.shareUserId === userId) {
                              return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                            } else {
                              return '<span class="layui-btn layui-btn-xs openApp"  lay-event="openApp">打开</span>'
                            }
                          },
                          'unresize': true
                        }]
                      ]
                    });
                  }
                })
              }
            })
          },
          end: function () {
            // $("#userAddDiaog").hidie();
            $("#userAddDiaog").css("display", "none");
            // $(".addressee").val($("#choose_sel").text())
            // console.log($("#choose_sel").text())
            var ids = $("#choose_sel option");
            var values = '';
            // console.log(ids.attr("dataid"))
            for (var i = 0; i < ids.length; i++) {
              values += ',' + ids[i].getAttribute("dataid")
            }
            values = values.substring(1);
            $(".addressee").val(values)
            console.log(values)
          }
        });
      }

      if (obj.event === 'openApp') {
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/open",
          data: JSON.stringify({
            "connId": connId
          }), // 传id
          contentType:"application/json;charset=utf-8",
          dataType: "json",
          success: function (data) {
            var data = data.data;
            var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
            form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
            form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
            form.appendTo('body').submit().remove();
          }
        });
      }

      if (obj.event === 'closeApp') {
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          data: JSON.stringify({
            "ids": connId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            layer.msg(data.message)
            // setTimeout(function(){  // 延迟一秒请求一次接口, 实现模拟关闭连接之后 页面也关闭
            $.ajax({ // 分享分页
              type: "POST",
              url: url + ':9006/AppShare/pageInfo/10/0/datetime/desc',
              data: JSON.stringify({
                "shareId": userId
              }),
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              success: function (data) {
                console.log(data)
                table.render({ // 我的分享
                  elem: '#tableData1',
                  data: data.data.content,
                  id: 'shareTable',
                  page: true,
                  // method: 'post',
                  cols: [
                    [{
                      checkbox: true,
                      fixed: true
                    }, {
                      field: 'datetime',
                      title: '时间',
                      sort: true,
                      'unresize': true
                    }, {
                      field: 'app_name',
                      title: '软件',
                      sort: true,
                      'unresize': true
                    }, {
                      field: 'type',
                      title: '状态',
                      sort: true,
                      templet: function (d) {
                        console.log(userName)
                        if (d.status== 1) {
                          return '无分享'
                        }else if(d.status== 2) {
                          return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                        }else if(d.status== 3) {
                          return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                        }
                      },
                      'unresize': true
                    }, {
                      field: 'userName',
                      title: '用户',
                      sort: true,
                      templet: function (d) {
                        console.log(d)
                        if (d.shareUserId === userId) {
                          return d.userName || '未分享'
                        } else {
                          return d.shareUserName
                        }
                      },
                      'unresize': true
                    }, {
                      field: 'operation',
                      title: '操作',
                      sort: true,
                      event: 'click',
                      width: 280,
                      templet: function (d) {
                        if (d.shareUserId === userId) {
                          return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                        } else {
                          return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                        }
                      },
                      'unresize': true
                    }]
                  ]
                });
              }
            })
            // },1000);
          }
        });

      }

      if (obj.event === 'emptyApp') {
        $.ajax({ // 查询分享链接数据
          type: "POST",
          url: url + ':9006/AppShare/clearShare',
          // url: 'http://10.1.1.213:9006/AppShare/clearShare',
          data: JSON.stringify({
            "shareId": shareId,
            "appConnectionId": connId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function (data) {
            console.log(data)
            layer.msg(data.message)
            $.ajax({ // 分享分页
              type: "POST",
              url: url + ':9006/AppShare/pageInfo/10/0/datetime/desc',
              data: JSON.stringify({
                "shareId": userId
              }),
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              success: function (data) {
                console.log(data)
                table.render({ // 我的分享
                  elem: '#tableData1',
                  data: data.data.content,
                  id: 'shareTable',
                  // method: 'post',
                  limit: 10,
                  page: true,
                  cols: [
                    [{
                      checkbox: true,
                      fixed: true
                    }, {
                      field: 'datetime',
                      title: '时间',
                      sort: true,
                      'unresize': true
                    }, {
                      field: 'app_name',
                      title: '软件',
                      sort: true,
                      'unresize': true
                    }, {
                      field: 'type',
                      title: '状态',
                      sort: true,
                      templet: function (d) {
                        console.log(userName)
                        if (d.status== 1) {
                          return '无分享'
                        }else if(d.status== 2) {
                          return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                        }else if(d.status== 3) {
                          return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                        }
                      },
                      'unresize': true
                    }, {
                      field: 'userName',
                      title: '用户',
                      sort: true,
                      templet: function (d) {
                        // console.log(d)
                        if (d.shareUserId === userId) {
                          return d.userName || '未分享'
                        } else {
                          return d.shareUserName
                        }
                      },
                      'unresize': true
                    }, {
                      field: 'operation',
                      title: '操作',
                      sort: true,
                      event: 'click',
                      width: 280,
                      templet: function (d) {
                        if (d.shareUserId === userId) {
                          return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                        } else {
                          return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                        }
                      },
                      // 'unresize': true
                    }]
                  ]
                });
              }
            })
          }
        })
      }
    });

    table.on('tool(tableData)', function (obj) { //我的连接 点击操作列的按钮
      console.log(obj)
      var Id = obj.data.id;
      var shareUserId = obj.data.shareUserId;
      if (obj.event === 'openApp') {
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/open",
          // url: "http://10.1.1.234:9006/AppConnection/open",
          data: JSON.stringify({
            "connId": Id
          }), // 传id
          contentType:"application/json;charset=utf-8",
          dataType: "json",
          success: function (data) {
            var data = data.data;
            var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
            form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
            form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
            form.appendTo('body').submit().remove();
          }
        });
      }
      if (obj.event === 'closeApp') {
        var closeLoading = layer.load(0, {
          shade: [0.8, '#393D49']
        })
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          // url: "http://10.1.1.213:9006/AppConnection/close",
          data: JSON.stringify({
            "ids": Id
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            layer.msg(data.message)
            // setTimeout(function(){
            $.ajax({ // 表格数据
              type: "POST",
              // url: url + ":9006/AppConnection/appConnectionPage/10/0",
              url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
              data: JSON.stringify({
                "userId": userId
              }), // id的参数obj
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              dataType: "json",
              success: function (data) {
                // console.log(data)
                table.render({
                  elem: "#tableData",
                  data: data.data.content,
                  limit: 10,
                  page: true,
                  cols: [
                    [{
                      checkbox: true,
                      fixed: true
                    },
                    {
                      field: 'appName',
                      title: '软件',
                      sort: true,
                      event: 'hover',
                      'unresize': true
                    },
                    {
                      field: 'equipmentName',
                      title: '服务器',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'ip',
                      title: 'IP',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'openTime',
                      title: '打开时间',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'leaveTime',
                      title: '闲置时间',
                      sort: true,
                      templet: function (d) {
                        return d.leaveTime + '小时'
                      },
                      'unresize': true
                    },
                    {
                      field: 'operation',
                      title: '操作',
                      sort: true,
                      event: 'click',
                      toolbar: '#conntClick',
                      'unresize': true
                    }
                    ]
                  ]
                });
              }
            });
            // },1000);
            layer.close(closeLoading);
          }
        });

      }
      if (obj.event === 'appInfo') {
        openDetail()
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/detail",
          data: JSON.stringify({
            "id": Id
          }), // id的参数obj
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            // console.log(data.data.appModuleInfo)
            console.log(data)
            $(".userid").html('');
            $(".rankName").html('');
            $(".institutes").html('');
            $(".department").html('');
            $(".tel").html('');
            $(".phone").html('');
            $(".software").html('');
            $(".city").html('');
            $(".sysusername").html('')
            $(".server").html('')
            $(".port").html('')
            $(".opentime").html('')
            $(".leavetime").html('')
            if (data.code === 400) {
              layer.msg(data.message)
            } else {
              var data = data.data;
              $(".userid").html(data.userInfo.userName);
              $(".rankName").html(data.userInfo.rankName + "：");
              $(".institutes").html(data.userInfo.placesDepartment);
              $(".department").html(data.userInfo.department);
              $(".tel").html(data.userInfo.telephone);
              $(".phone").html(data.userInfo.phone);
              $(".software").html(data.userInfo.appName);
              $(".city").html(data.userInfo.ip);
              $(".sysusername").html(data.userInfo.sysUserName)
              $(".server").html(data.userInfo.equipmentName)
              $(".port").html(data.userInfo.port)
              $(".opentime").html(data.userInfo.openTime)
              $(".leavetime").html(data.userInfo.leaveTime)
              table.render({
                elem: "#detailTable",
                data: data.appModuleInfo,
                id: 'detailTable',
                limit: 10,
                // width: 800,
                cols: [
                  [{
                    checkbox: true
                  },
                  // { field: 'id', title: 'ID', width: 50, style: 'display:none;', 'unresize': true },
                  {
                    field: 'moduleName',
                    title: '模块',
                    unresize: true,
                    'unresize': true
                  },
                  {
                    field: 'handle',
                    title: 'Handle No.',
                    unresize: true,
                    'unresize': true
                  },
                  {
                    field: 'appOpentime',
                    title: '打开时间',
                    unresize: true,
                    'unresize': true,

                  },
                  {
                    field: 'operation',
                    title: '操作',
                    unresize: true,
                    templet: "#releaseBtn",
                    event: 'click',
                    'unresize': true,
                    templet: function (d) {
                      return '<span class="layui-btn layui-btn-xs release" lay-event="release">释放</span>'

                    }
                  }
                  ]
                ],
                done: function (res, curr, count) { }
              });
            }
          }
        });
      }

    });

    //移到右边
    $(document).on('click', '.adds', function () {
      var flag
      if (!$('#brand_sel option').is(':selected')) {
        layer.alert('请选择移动的选项')
      } else if ($('#choose_sel option').length == 0) {
        $('#brand_sel option:selected').appendTo('#choose_sel')
      } else {
        var arr = Array.prototype.slice.call($('#choose_sel option'))
        for (var i = 0; i < arr.length; i++) {
          if ($('#brand_sel option:selected').text() == arr[i].text) {
            flag = false
            layer.alert('不可以选择重复的选项!')
            return false
          } else {
            flag = true
          }
        }
        console.log(flag)
        if (flag) {
          $('#brand_sel option:selected').appendTo('#choose_sel')
        }
      }
    });
    //移到左边
    $(document).on('click', '.removes', function () {
      if (!$("#choose_sel option").is(":selected")) {
        layer.alert("请选择移动的选项")
      } else {
        $('#choose_sel option:selected').appendTo('#brand_sel');
      }
    });
    // //双击选项
    // $(document).on('dblclick', '#brand_sel', function () {
    //   $("option:selected", this).appendTo('#choose_sel');
    // });
    // //双击选项
    // $(document).on('dblclick', '#choose_sel', function () {
    //   $("option:selected", this).appendTo('#brand_sel');
    // });
    //全部移到右边
    $(document).on('click', '.add_alls', function () {
      $('#brand_sel option').appendTo('#choose_sel');
    });
    //全部移到左边
    $(document).on('click', '.remove_alls', function () {
      $('#choose_sel option').appendTo('#brand_sel');
    });




    var active = {
      init: function () {
        this.loginPrivilege();
      },
      //封装一个函数 获取cookie
      getCookie: function (name) {
        var cookie = document.cookie;
        // console.log(cookie);
        var arr = cookie.split("; "); //将字符串分割成数组
        // console.log(arr);
        for (var i = 0; i < arr.length; i++) {
          var arr1 = arr[i].split("=");
          if (arr1[0] == name) {
            return unescape(arr1[1]);
          }
        }
        return "GG"
      },
      //获取权限
      loginPrivilege: function () {
        var userId;
        var loginPageAccess = active.getCookie('loginPageAccess');
        if (loginPageAccess == 2) {
          userId = active.getCookie("userId2");
        } else if (loginPageAccess == 3) {
          userId = active.getCookie("userId3");
        }
        if (userId == "GG") {
          // window.location.href = "http://localhost:3000";
          window.location.href = url + port + "/dist/views";
          layer.msg('请登录用户！', {
            icon: 2,
            closeBtn: 1
          })
        } else {
          $.ajax({
            url: url + ":9000/login/loginPrivilege",
            // url:"http://localhost:9000/login/loginPrivilege",
            type: "post",
            async: true,
            data: {
              userId
            },
            success: function (data) {
              console.log(JSON.stringify(data));
              if (data.code == 200) {
                $('#userName').html(active.getCookie("userName"));
                if (data.data.length > 0) {
                  JSON1.list = data.data[0].children;
                  var json = JSON1;
                  var getTpl = menuTpl.innerHTML,
                    view = document.getElementById('menuBox');
                  laytpl(getTpl).render(json, function (html) {
                    view.innerHTML = html;
                  });
                  element.init();
                  $("#menuBox .layui-nav-child dd").on("click", function (params) {
                    sessionStorage.setItem("navItem", $(this).data("link"));
                    console.log(sessionStorage.getItem("navItem"))
                  })
                  $("dd.layui-this").parents(".layui-nav-item").addClass("layui-nav-itemed");
                  $("#menuBox .layui-nav-item > a").on("click", function (params) {
                    sessionStorage.setItem("navItem", $(this).data("link"));
                  })
                }

              } else if (data.code == 400) {
                layer.msg(data.message, {
                  icon: 2,
                  closeBtn: 1
                })
              }
            },
            error: function (data) {
              console.log(data)
            }
          })
        }

      },
      //注销
      logout: function (userId, token) {
        console.log(userId);
        console.log(token);
        $.ajax({
          url: url + ":9000/login/logout",
          // url:"http://10.1.2.251:9000/login/logout",
          type: "post",
          async: true,
          data: {
            userId,
            token
          },
          success: function (data) {
            console.log(JSON.stringify(data));
            if (data.code == 200 && data.data == 1) {
              // active.removeCookie("userId");
              // window.location.href = "http://localhost:3000";
              window.location.href = url + port + "/dist/views";
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function (data) {
            console.log(data)
          }
        })
      },
      //修改密码弹框
      updataPassword: function () {
        layer.open({
          type: 1,
          title: "修改密码" //不显示标题栏
          ,
          closeBtn: 1,
          area: ['360px', '260px'],
          shade: 0.8,
          id: 'LAY_layuipro' //设定一个id，防止重复弹出
          // ,resize: false
          // ,btn: ['确定']
          ,
          btnAlign: 'c',
          moveType: 1 //拖拽模式，0或者1
          ,
          content: $('.updata-password'),
          success: function (layero) {
            //     var btn = layero.find('.layui-layer-btn');
            //     // btn.find('.layui-layer-btn0').attr({
            //     //     href: 'http://www.layui.com/'
            //     //     ,target: '_blank'
            //     // });
            //     btn.find('.layui-layer-btn0').click(function(){
            //         // alert(1111111111)
            //     });
          },
          end: function (index, layero) {
            active.clearUpdata();
            $(".updata-password").hide()
          }
        });
      },
      //清空修改密码弹框数据
      clearUpdata: function () {
        $('.updata-password input[name=oldPassword]').val('');
        $('.updata-password input[name=newPassword]').val('');
        $('.updata-password input[name=verifyPassword]').val('');
      },
      //修改密码提交
      updataSubmit: function (data) {
        // console.log(JSON.stringify(data));
        $.ajax({
          url: url + ":9000/login/userUpdatePassword",
          // url:"http://localhost:9000/login/userUpdatePassword",
          type: "post",
          async: true,
          data,
          success: function (data) {
            // console.log(JSON.stringify(data));
            if (data.code == 200 && data.data == 1) {
              // window.location.href = "http://localhost:3000";
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
              window.location.href = url + port + "/dist/views";
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function (data) {
            console.log(data)
          }
        })
      },
    }
    // active.init();

    //自定义验证规则
    form.verify({
      oldPass: [/(.+){6,12}$/, '密码必须6到12位'],
      newPass1: [/(.+){6,12}$/, '密码必须6到12位'],
      newPass2: [/(.+){6,12}$/, '密码必须6到12位'],
      newPass1: function (value, item) {
        if (value != $('input[name=verifyPassword]').val()) {
          return '两次密码不一致'
        }
      }
    });

    //监听修改密码提交
    form.on('submit(newUpdataPassword)', function (data) {
      data.field.userId = active.getCookie("userId");
      // console.log(JSON.stringify(data.field));
      active.updataSubmit(data.field);
      return false
    });

    //注销
    $('body .signOut').on('click', function () {
      var userID;
      var loginPageAccess = active.getCookie('loginPageAccess');
      userID = active.getCookie("userId");
      active.logout(userID, active.getCookie('token3'));
      window.location.href = url + port + "/dist/views";
    });
    //修改密码
    $('#updataUserPassword').on('click', function () {
      // console.log(111111111);
      active.updataPassword()
    });
    $('#userName').html(active.getCookie("realName")); // 显示页面用户名




    // var userId = getCookie("userId");
    $.ajax({ // 收消息
      type: "POST",
      url: url + ':9006/Message/putMessage/' + userId,
      // url: 'http://10.1.1.213:9006/Message/putMessage/'+userId,
      // data: JSON.stringify({ "userId": userId, "searchInfo": searchInfo }),
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      success: function (data) {
        console.log(data)
        $(".layui-badge").html(data.data.length)
        if (data.code === 200) {
          var data = data.data;
          var messageInfo = '';
          for (var i = 0; i < data.length; i++) {
            console.log(data[i])
            messageInfo = `<div>
                                  ${data[0].datetime} 通知：${data[0].note}
                                </div>`
          }
          // console.log(messageInfo)
          $("#msg-box-show").append(messageInfo)
        } else {
          // layer.msg(data)
          console.log(data.message)
        }
      }
    })



    function setCookie (name, value, num) { // 设置cookie
      var val = escape(value); //对value进行编码
      var date = new Date();
      date.setDate(date.getDate() + num);
      // document.cookie = name+"="+val+";expires="+date;
      document.cookie = `${name}=${val};expires=${date}`;
    }

    function removeCookie (name) { // 删除cookie
      // console.log(name)
      setCookie(name, 123, -1);
    }

    function getCookie (name) { // 获取cookie
      var strcookie = document.cookie; //获取cookie字符串
      var arrcookie = strcookie.split("; "); //分割
      //遍历匹配
      for (var i = 0; i < arrcookie.length; i++) {
        var arr = arrcookie[i].split("=");
        if (arr[0] == name) {
          return arr[1];
        }
      }
      return "";
    }

    function openDetail () { // 打开详情页
      layer.open({
        type: 1,
        title: "详情", //不显示标题栏
        closeBtn: 1,
        area: ["95%", "520px"],
        shade: 0.3,
        id: "detail_layui", //设定一个id，防止重复弹出
        btn: ["确定"],
        btnAlign: "c",
        moveType: 1, //拖拽模式，0或者1
        content: $("#detailHtml"),
        end: function () {
          $("#detailHtml").hide();
        }
      });
    }


    var closeIds = '';
    table.on("checkbox(required)", function (obj) { // 选择我的连接页面多选按钮
      console.log(obj)

    });

    $("#link-close").click(function () { // 选择我的连接页面多选按钮
      // table.reload('idTest', {
      // });
      var checkStatus = table.checkStatus('idTest'),
        data = checkStatus.data;
      // layer.alert(JSON.stringify(data));
      for (var i = 0; i < data.length; i++) {
        // closeIds += "," + data[i].id;
        closeIds += `,${data[i].id}`;
        // console.log(data[i].id)
      }
      // console.log(closeIds)
      closeIds = closeIds.substring(1); // 去掉id串前面的逗号
      console.log(closeIds)
      if (closeIds != '') {
        var batchClose = layer.load(0, {
          shade: [0.8, '#393D49']
        })
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          // url: "http://10.1.1.213:9006/AppConnection/close",
          data: JSON.stringify({
            "ids": closeIds
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            // layer.msg(data.data)
            // setTimeout(function(){
            $.ajax({ // 我的连接 表格数据
              type: "POST",
              // url: url + ':9006/AppConnection/appConnectionPage/10/0',
              url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
              // url: 'http://10.1.1.213:9006/AppConnection/appConnectionPage/10/0',
              data: JSON.stringify({
                "userId": userId
              }), // id的参数obj
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              dataType: "json",
              success: function (data) {
                console.log(data)
                table.render({
                  elem: "#tableData",
                  data: data.data.content,
                  limit: 10,
                  page: true,
                  id: 'idTest',
                  cols: [
                    [{
                      checkbox: true,
                      fixed: true
                    },
                    {
                      field: 'appName',
                      title: '软件',
                      sort: true,
                      event: 'hover',
                      'unresize': true
                    },
                    {
                      field: 'equipmentName',
                      title: '服务器',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'ip',
                      title: 'IP',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'openTime',
                      title: '打开时间',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'leaveTime',
                      title: '闲置时间',
                      sort: true,
                      templet: function (d) {
                        return d.leaveTime + '小时'
                      },
                      'unresize': true
                    },
                    {
                      field: 'operation',
                      title: '操作',
                      sort: true,
                      event: 'click',
                      toolbar: '#conntClick',
                      'unresize': true
                    }
                    ]
                  ]
                });
              }
            });
            // },500);
            layer.close(batchClose); // 关闭加载层
          }
        });
        closeIds = '';
      } else {
        layer.msg('请选择要关闭的连接!');
        return false
      }
    })

    table.on('sort(tableData)', function (obj) { // 我的连接表格 排序
      console.log(obj.field);
      console.log(obj.type)
      $.ajax({ // 我的连接 表格数据
        type: "POST",
        url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/' + obj.field + "/" + obj.type,
        // url: 'http://10.1.1.213:9006/AppConnection/appConnectionPageForUser/10/0/' + obj.field + "/" + obj.type,
        data: JSON.stringify({
          "userId": userId
        }), // id的参数obj
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          table.render({
            elem: "#tableData",
            data: data.data.content,
            limit: 10,
            page: true,
            id: 'idTest',
            cols: [
              [{
                checkbox: true,
                fixed: true
              },
              {
                field: 'appName',
                title: '软件',
                sort: true,
                event: 'hover',
                'unresize': true
              },
              {
                field: 'equipmentName',
                title: '服务器',
                sort: true,
                'unresize': true
              },
              {
                field: 'ip',
                title: 'IP',
                sort: true,
                'unresize': true
              },
              {
                field: 'openTime',
                title: '打开时间',
                sort: true,
                'unresize': true
              },
              {
                field: 'leaveTime',
                title: '闲置时间',
                sort: true,
                templet: function (d) {
                  return d.leaveTime + '小时'
                },
                'unresize': true
              },
              {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                toolbar: '#conntClick',
                'unresize': true
              }
              ]
            ]
          });
        }
      });
    });

    table.on('sort(tableData1)', function (obj) { // 我的分享表格 排序
      console.log(obj.field);
      console.log(obj.type)
      $.ajax({ // 分享分页
        type: "POST",
        url: url + ':9006/AppShare/pageInfo/10/0/' + obj.field + "/" + obj.type,
        // url: 'http://10.1.1.213:9006/AppShare/pageInfo/10/0/' + obj.field + "/" + obj.type,
        data: JSON.stringify({
          "shareId": userId
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          console.log(data)
          table.render({ // 我的分享
            elem: '#tableData1',
            data: data.data.content,
            id: 'shareTable',
            // method: 'post',
            limit: 10,
            page: true,
            cols: [
              [{
                checkbox: true,
                fixed: true
              }, {
                field: 'datetime',
                title: '时间',
                sort: true,
                'unresize': true
              }, {
                field: 'app_name',
                title: '软件',
                sort: true,
                'unresize': true
              }, {
                field: 'type',
                title: '状态',
                sort: true,
                templet: function (d) {
                  console.log(userName)
                  if (d.status== 1) {
                    return '无分享'
                  }else if(d.status== 2) {
                    return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                  }else if(d.status== 3) {
                    return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                  }
                },
                'unresize': true
              }, {
                field: 'userName',
                title: '用户',
                sort: true,
                templet: function (d) {
                  // console.log(d)
                  if (d.shareUserId === userId) {
                    return d.userName || '未分享'
                  } else {
                    return d.shareUserName
                  }
                },
                'unresize': true
              }, {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                width: 280,
                templet: function (d) {
                  if (d.shareUserId === userId) {
                    return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                  } else {
                    return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                  }
                },
                'unresize': true
              }]
            ]
          });
        }
      })
    });



  });
})()
