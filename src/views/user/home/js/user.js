vm = new Vue({
  el: '#app',
  data: function() {
    return {
      tips: false
    }
  },
  mounted() {
    this.isTips()
  },
  methods: {
    downLoad() {
      var _this = this
      window.location.href = url + ':9000/login/downloadApp'
      this.$message('下载中')
      this.tips = false
    },
    handleClose() {
      this.tips = false
      sessionStorage.removeItem('isFirstLogin')
    },
    isTips() {
      if (sessionStorage.getItem('isFirstLogin')) {
        this.tips = true
      }
    },
  }
})
