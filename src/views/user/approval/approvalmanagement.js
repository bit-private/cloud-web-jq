
vm = new Vue({
    el: '#app',
    data: function () {
      return {
          taskId:'',
          processInstanceId:'',
          reviewProgress:'',
          reviewProgressValue:'',
          paramsState:'',
          paramsStateValue:'',
          otherUnitsTree:[],
          otherSelectNodes:[],
          otherTreeIDs:[],
          otherTreeNames:[],
          otherNamesStr:'',

          ownSelectNodes:[],
          ownTree:[],
          ownTreeIDs:[],
          ownTreeNames:[],
          ownNamesStr:'',
          props: {
              children: 'children',
              label: 'name',
              id: 'id'
          },
          otherProps: {
              children: 'children',
              label: 'name',
              id: 'id'
          },
          isEdit:false,
       ownSelect:{},
       title:'',
       adoptNum:0,
       recordNum:0,
       tableDataPage:0,
       tableDataTotal:0,
       table1DataPage:0,
       table1DataTotal:0,
       table2DataPage:0,
       table2DataTotal:0,
       multipleSelectionRows:[],
       projectName:"",
       show1:true,
       show2:false,
       show3:false,
       activeName:'',
       activeIndex: '1',
       activeIndex2: '1',
       search:'',
       submitSerch:'',//提交申请搜索
       adoptSerch:'',//已通过搜索
       recordSerch:'',//提交记录搜索
       typeList:false,//申请类别
       typeList2:false,//申请类别
       typeList3:false,//申请类别
       name:'',//用户名
       radio:0,//单选框
       value1:'',//开始时间
       value2:'',//结束时间
       state:'',//存储容量
       showDialog: false,//弹框显隐
       tips:'',//备注
       softwareTypes:[],//软件类型下拉框
       softwareType:'',//选中软件名称
       softwareTypeList:{},//已添加软件类型
       softwareEdition:[],//软件名称/类型
       tableData:[],//提交申请数据
       tableData1:[],//已通过数据
       tableData2:[],//提交记录数据
      }
    },
    methods: {
        init(){
              let _that=this
              let userId = this.getCookie("userId"); // 获取用户id
              $.ajax({
                  url: 'http://10.1.3.150:9001/examineAndApproveResource/myApplyDetail',
                  // url: 'http://10.1.1.37:9001/examineAndApproveResource/myApplyDetail',
                  type: 'post',
                  async: false,
                  data: {currentUserId:'2326985c996744408673ad2f211cdb60',
                  page:_that.tableDataPage,size:10,conditions:this.submitSerch},
                  success: function(data) {
                      _that.$set(_that,"tableData",data.data.list)
                      _that.tableDataTotal=data.data.count
                  },
                  error: function(data) {
                      console.log(data)
                  }
              })
        },
        getMyPass(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: 'http://10.1.3.150:9001/examineAndApproveResource/myPass',
                type: 'post',
                async: false,
                data: {currentUserId:'2326985c996744408673ad2f211cdb60',
                    page:_that.table1DataPage,size:10,conditions:this.adoptSerch},
                success: function(data) {
                    _that.$set(_that,"tableData1",data.data.list)
                    _that.table1DataTotal=data.data.count
                    _that.adoptNum=data.data.count
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getMyRecord(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: 'http://10.1.3.150:9001/examineAndApproveResource/myRecord',
                type: 'post',
                async: false,
                data: {currentUserId:'2326985c996744408673ad2f211cdb60',
                    page:_that.table2DataPage,size:10,conditions:this.recordSerch},
                success: function(data) {
                    _that.$set(_that,"tableData2",data.data.list)
                    _that.table2DataTotal=data.data.count
                    _that.recordNum=data.data.count
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getAddMessage(){
              let _that=this
              let userId = this.getCookie("userId"); // 获取用户id
              $.ajax({
                  url: 'http://10.1.3.150:9001/examineAndApproveResource/getPageByUserId',
                  type: 'post',
                  async: false,
                  data: {currentUserId:'2326985c996744408673ad2f211cdb60'},
                  success: function(data) {
                      _that.softwareTypes=data.data.appTypeList
                      _that.name=data.data.realName
                      _that.getSoftWareList(data.data.appTypeList)
                  },
                  error: function(data) {
                      console.log(data)
                  }
              })
        },
        getOrgTreeAndName(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: 'http://10.1.3.150:9001/examineAndApproveResource/orgTreeAndName',
                type: 'post',
                async: false,
                data: {currentUserId:'2326985c996744408673ad2f211cdb60'},
                success: function(data) {
                    _that.otherUnitsTree=data.data.orgTree
                    _that.ownTree=data.data.rankTree
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        cutOut(){
            let arr=[]
            for(let i=0;i<this.multipleSelectionRows.length;i++){
                arr.push(this.multipleSelectionRows[i].processInstanceId)
            }
            let _that=this
            $.ajax({
                url: 'http://10.1.3.150:9001/examineAndApproveResource/revocationApply ',
                type: 'post',
                async: false,
                data: {currentUserId:'2326985c996744408673ad2f211cdb60',processInstanceId :arr.join(',')},
                success: function(data) {
                    if(data.data==true){
                        _that.$message({
                            message: '删除成功',
                            type: 'success'
                        });
                        _that.init();
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        searchInput(params){
           if(params=="submitList"){
               this.tableDataPage=0
               this.init();
           }else if(params=="adoptList"){
               this.table1DataPage=0
               this.getMyPass();
           }else if(params=="recordSerch"){
               this.table2DataPage=0
               this.getMyRecord();
           }
        },
        handleCurrentChange(val){
          if(this.activeIndex==1){
              this.submitSerch=''
              this.tableDataPage=val
              this.init()
          }else if(this.activeIndex==2){
              this.adoptSerch='';
              this.table1DataPage=val
              this.getMyPass()
          }else if(this.activeIndex==3){
              this.recordSerch=''
              this.table2DataPage=val
              this.getMyRecord()
          }
        },
        handleSelectChangeLeft(rows){
            this.multipleSelectionRows=rows;
        },
        handleOpen(key, keyPath) {
            console.log(key, keyPath);
        },
        handleClose(key, keyPath) {
            console.log(key, keyPath);
        },
        handleSelect(val){
            if(val==1){
                this.submitSerch = ''
                this.table1DataPage=0
                this.init()
            }else if(val==2){
                this.adoptSerch=''
                this.table1DataPage=0
                this.getMyPass();
            }else if(val==3){
                this.recordSerch=''
                this.table2DataPage=0
                this.getMyRecord();
            }
        },
        handleEdit(index, row) {//催办
            console.log(index, row);
            let _that=this;
            $.ajax({
                url: 'http://10.1.3.150:9001/examineAndApproveResource/urgeTransaction',
                type: 'post',
                async: false,
                data: {processInstanceId :row.processInstanceId},
                success: function(data) {
                    if(data.data==true){
                        _that.$message({
                            message: '催办成功',
                            type: 'success'
                        });
                        _that.init();
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        handleDelete(index, row) {//撤回
            console.log(index, row);
            let _that=this
            $.ajax({
                url: 'http://10.1.3.150:9001/examineAndApproveResource/revocationApply ',
                type: 'post',
                async: false,
                data: {currentUserId:'2326985c996744408673ad2f211cdb60',processInstanceId :row.processInstanceId},
                success: function(data) {
                    if(data.data==true){
                        _that.$message({
                            message: '撤回成功',
                            type: 'success'
                        });
                        _that.init();
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        submit(){//切换
              this.show1=true
              this.show2=false
              this.show3=false
              // console.log(this.show1)
        },
        adopt(){//切换
              this.show1=false
              this.show2=true
              this.show3=false
          },
        record(){//切换
              this.show1=false
              this.show2=false
              this.show3=true
        },
        x(){//清空搜索
            this.submitSerch = ''
            this.tableDataPage=0
            this.init();
        },
        y(){//清空搜索
            this.adoptSerch=''
            this.table1DataPage=0
            this.getMyRecord();
        },
        z(){//清空搜索
            this.recordSerch=''
            this.table2DataPage=0
            this.getMyRecord();
        },
        handleClose1(done) {
            this.$confirm('是否关闭当前页面?', '', {
              confirmButtonText: '是',
              cancelButtonText: '否',
              // type: 'warning'
            }).then(() => {
              done()
            }).catch(() => {        
            });
        },
        closeBtn () {
            this.showDialog = false
        },
        newly(params){//新建请求
              if(params==='edit'){
                  this.isEdit=true
                  let _that=this
                  if(this.multipleSelectionRows.length==1){
                      _that.processInstanceId=_that.multipleSelectionRows[0].processInstanceId
                      _that.taskId=_that.multipleSelectionRows[0].taskId
                      _that.reviewProgress=_that.multipleSelectionRows[0].reviewProgress
                      _that.reviewProgressValue=_that.multipleSelectionRows[0].reviewProgressValue
                      _that.paramsState=_that.multipleSelectionRows[0].state
                      _that.paramsStateValue=_that.multipleSelectionRows[0].stateValue
                      _that.showDialog = true
                      $.ajax({
                          url: 'http://10.1.3.150:9001/examineAndApproveResource/getUpdatePage',
                          type: 'post',
                          async: false,
                          data: {processInstanceId:_that.multipleSelectionRows[0].processInstanceId},
                          success: function(data) {
                              console.log(111)
                              _that.title="编辑"
                              _that.name=data.data.currentUserName;
                              _that.radio=data.data.applyType;
                              if(data.data.applyType==1){
                                  _that.typeList=true
                              }else if(data.data.applyType==2){
                                  _that.typeList2=true
                                  _that.$set(_that,'ownSelectNodes',data.data.ids.split(','))
                                  _that.$set(_that,'ownNamesStr',data.data.names)
                              }else if(data.data.applyType==3){
                                  _that.typeList3=true
                                  _that.$set(_that,'otherSelectNodes',data.data.ids.split(','))
                                  _that.$set(_that,'otherNamesStr',data.data.names)
                              }
                              _that.projectName=data.data.projectName;
                              _that.value1=data.data.startTime;
                              _that.value2=data.data.endTime;
                              _that.state=data.data.capacity;
                              _that.tips=data.data.remark;
                              let arr=data.data.softwareValue.split(",");
                              let arr1=data.data.softwareIds.split(",");
                              let arr2=data.data.softwareTypeKey.split(",");
                              for(let i=0;i<arr.length;i++){
                                  _that.$set(_that.softwareTypeList,"index_"+i,{name:arr[i],softName:arr1[i],select: arr2[i]})
                              }
                          },
                          error: function(data) {

                          }
                      })
                  }else{
                      layer.msg("只能选择一项进行编辑", {
                          icon: 2,
                          closeBtn:1
                      });
                  }
              }else{
                  this.isEdit=false
                  this.resetBtn()
                  this.processInstanceId=''
                  this.taskId=''
                  this.reviewProgress=''
                  this.reviewProgressValue=''
                  this.paramsState=''
                  this.paramsStateValue=''
                  this.title="新建"
                  this.showDialog = true
              }
        },
        addSoftwareType(){//添加软件类型
            if (this.softwareType == "") {
              // this.$message("请选择软件类型")
              layer.msg("请选择策略名称", {
                    icon: 2,
                    closeBtn:1
                });
              return
            }
            let obj = JSON.parse(JSON.stringify(this.softwareTypeList))//深拷贝对象
            let name=''
            for(let i=0;i<this.softwareTypes.length;i++){
                if(this.softwareTypes[i].value==this.softwareType){
                    name=this.softwareTypes[i].typeKey
                }
            }
            this.$set(this.softwareTypeList, this.transformation(obj), { name:this.softwareType, softName: '',select:name})
        },
        transformation: function (obj) {
            let arr = Object.keys(obj)//对象转化数组
            let lastIndex = ''
            let join = ''
            if (arr.length == 0) {
              join = 'index_0'
            } else {
              lastIndex = arr[arr.length - 1].split("_")[1]//获取最后一项数字
              join = 'index_' + (parseInt(lastIndex) + 1)//将要新增进对象的key值
            }
            return join
        },
        delSoftwareEdition(key){//删除软件类型
            Vue.delete(this.softwareTypeList,key);//vue方法
        },
        resetBtn(){//重置
            this.radio = 0 
            this.typeList=false
            this.typeList2=false
            this.typeList3=false
            this.value1=''//开始时间
            this.value2=''//结束时间
            this.state=''//存储容量
            this.softwareType=''//软件类型
            this.softwareTypeList={}
            this.tips=''//备注
            this.taskId=''

            this.otherSelectNodes=[]
            this.otherTreeIDs=[]
            this.otherTreeNames=[]
            this.otherNamesStr=''
            this.ownSelectNodes=[]
            this.ownTreeIDs=[]
            this.ownTreeNames=[]
            this.ownNamesStr=''
        },
        application(){//提交抓取数据
            let str = ''
            let _that=this;
            if(this.radio===0){
                  layer.msg("请选择申请类别", {
                    icon: 2,
                    closeBtn:1
                });
              return
            }
            else if(this.radio==1){
              str = '个人'
            }
            else if(this.radio==2){
              str = '组织单位'
            }
            else if(this.radio==3){
              str = '外协用户/单位'
            }
            let arr = []
            if(this.value1==''||this.value2==''||this.state==''){
                  layer.msg("必填项不能为空", {
                    icon: 2,
                    closeBtn:1
                });
              return
            }
            let softwareTypeKey=[];
            let softwareValue=[];
            let softwareIds=[];
            for (const i in this.softwareTypeList) {
              let obj = {}
              obj.name = this.softwareTypeList[i].name
              softwareValue.push(this.softwareTypeList[i].name)
              softwareIds.push(this.softwareTypeList[i].softName)
              obj.softName = this.softwareTypeList[i].softName
              arr.push(obj)
            }
            for(let i=0;i<softwareValue.length;i++){
                for(let j=0;j<this.softwareTypes.length;j++){
                    if(softwareValue[i]==this.softwareTypes[j].value){
                        softwareTypeKey.push(this.softwareTypes[j].typeKey)
                    }
                }
            }
            let params = {
              taskId:this.taskId,//任务id
              processInstanceId:this.processInstanceId,//工作流流程实例id
              currentUserId:'2326985c996744408673ad2f211cdb60',//申请用户id
              currentUserName:this.name,//用户名称
                applyType:this.radio,//申请类型
                projectName:this.projectName,//projectName
                applyTypeValue:str,//申请类型值
                ids:'',//类型选择后的组织单位id或外协用户的ids
                names:'',//类型选择后的组织单位name或外协用户的names
                startTime:this.value1,//开始时间
                endTime:this.value2,//结束时间
                capacity:this.state,//存储容量
                remark:this.tips,//备注信息
                softwareTypeKey:softwareTypeKey.join(','),//软件类型key(多个类型用英文逗号分隔)
                softwareValue:softwareValue.join(','),//软件类型value(多个类型用英文逗号分隔)
                softwareIds:softwareIds.join(','),
                updateTime:'',//操作时间(每次修改状态的时间)
                reviewProgress:this.reviewProgress,	//审核进度(1提交审批, 2二级审批通过, 3一级审批通过, 4资源授权成功)
                reviewProgressValue:this.reviewProgressValue,//审核进度值
                state:this.paramsState	,//审核状态(1审核通过, 2审核未通过, 3审核中, 4已催办)
                stateValue:this.paramsStateValue//审核状态值
          }
            console.log(params.softwareTypeKey,params.softwareValue)
            let  startTime = new Date(params.startTime);
            params.startTime=startTime.getFullYear() + '-' + (startTime.getMonth() + 1) + '-' + startTime.getDate();
            let  endTime = new Date(params.startTime);
            params.endTime=endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' + endTime.getDate();
            if(this.radio=='2'){
                params.ids=JSON.parse(JSON.stringify(this.ownTreeIDs)).join(',')
                params.names=JSON.parse(JSON.stringify(this.ownTreeNames)).join(',')
            }else if(this.radio=='3'){
                params.ids=JSON.parse(JSON.stringify(this.otherTreeIDs)).join(',')
                params.names=JSON.parse(JSON.stringify(this.otherTreeNames)).join(',')
            }
            let url=''
            if(this.isEdit){
                url='updateApply'
            }else{
                url='submitApproveData'
            }
              $.ajax({
                  url: 'http://10.1.3.150:9001/examineAndApproveResource/'+url,
                  type: 'post',
                  async: false,
                  data: params,
                  success: function(data) {
                      if(data.data==true){
                          _that.$message({
                              message: '提交成功',
                              type: 'success'
                          });
                          _that.showDialog=false
                          _that.init();
                      }
                      // _that.$set(_that,"tableData",data.data)
                  },
                  error: function(data) {
                      console.log(data)
                  }
              })
          },
        tab () {//申请类型切换
            if (this.radio === 0) {
              this.typeList=false
              this.typeList2=false
              this.typeList3=false
            } else if(this.radio == 1) {
              this.typeList=true
              this.typeList2=false
              this.typeList3=false
            }else if(this.radio == 2) {
              this.typeList=false
              this.typeList2=true
              this.typeList3=false
            }else if(this.radio == 3) {
              this.typeList=false
              this.typeList2=false
              this.typeList3=true
            }
          },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        handleCheckChange(){
            let nodeNameArr = []
            let nodeKeyArr = [];
            console.log(this.$refs.otherTree)
            this.$refs.otherTree.getCheckedNodes().map(item => {
                if (!item.children) {
                    nodeNameArr.push(item.name)
                    nodeKeyArr.push(item.id)
                }
            });
            this.otherTreeIDs=nodeKeyArr
            this.otherTreeNames=nodeNameArr
            let nodeStr=JSON.parse(JSON.stringify(nodeNameArr))
            this.otherNamesStr=nodeStr.join(',')
        },
        handleOwnChange(){
            let nodeNameArr = []
            let nodeKeyArr = [];
            this.$refs.myOwnTree.getCheckedNodes().map(item => {
                if (!item.children || item.children==0) {
                    nodeNameArr.push(item.name)
                    nodeKeyArr.push(item.id)
                }
            });
            this.ownTreeIDs=nodeKeyArr
            this.ownTreeNames=nodeNameArr
            let nodeStr=JSON.parse(JSON.stringify(nodeNameArr))
            this.ownNamesStr=nodeStr.join(',')
        },
        getSoftWareList(params){
            let _that=this
             for(let i=0;i<params.length;i++){
                 $.ajax({
                     url: 'http://10.1.3.150:9001/examineAndApproveResource/softwareList',
                     type: 'post',
                     async: false,
                     data: {typeKey:params[i].typeKey},
                     success: function(data) {
                         _that.$set(_that.ownSelect,params[i].typeKey,data.data)
                     },
                     error: function(data) {
                         console.log(data)
                     }
                 })
             }
             console.log(_that.ownSelect)
        },
    },
    computed: {
      //搜索过滤数组
      submitData: function() {//提交申请
        return this.tableData.filter((item)=>{
          return item.user.indexOf(this.submitSerch)!==-1
        })
      },
      adoptData: function() {//已通过
        return this.tableData1.filter((item)=>{
          return item.user.indexOf(this.adoptSerch)!==-1
        })
      },
      recordData: function() {//提交记录
        return this.tableData2.filter((item)=>{
          return item.user.indexOf(this.recordSerch)!==-1
        })
      },
      
      },
    mounted() {
     this.init();
     this.getMyPass();
     this.getMyRecord();
     this.getAddMessage();
     this.getOrgTreeAndName();
    },
})

// var a = document.getElementsByClassName('el-table_1_column_11')
// var b = Array.from(a)
// console.log(b)
// b[0].innerHTML = '操作'