/*  
    * When I wrote this,only God and I understood what I was doing
    * Now,God only knows
    * 写这段代码的时候，只有上帝和我知道它是干嘛的。
    * 现在，只有上帝知道
    * @author : zbw
    * @Date : 2018/9/11
    * @Last Modified time : 2018/9/11
    * */
//JavaScript代码区域
let defaultTitle  = ''
let defaultText  = ''
function getNoticefirst() {
  // var _that = this
  $.ajax({
    url: url + ':9006/notification/getDefault',
    type: "post",
    headers: {
      "token": token,
    },
    dataType: 'json',
    success: function (data) {
      console.log(data)
      if(data.data.stress) {
        $('.tips').css('display', 'inline-block')
      }
      $('.lunbo').html(data.data.title)
      defaultTitle = data.data.title
      defaultText = data.data.content
    }
  });
}
getNoticefirst()
layui.use(['element', 'layer', 'form'], function () {
  var element = layui.element,
      layer = layui.layer,
      form = layui.form;
  var active = {
    init: function () {
      this.removeCookie("token");
      // this.removeCookie("token1");
      // this.removeCookie("token2");
      // this.removeCookie("token3");
      this.removeCookie("userName");
      this.removeCookie("realName");
      this.removeCookie("loginPageAccess");
      this.removeCookie("userId");
      this.lincenses()
    },
    lincenses: function() {
      $.ajax({
        url: url + ":9000/login/license",
        type: "get",
        async: false,
        success: function (data) {
          if (data.data != "SUCCESS") {
            window.location.href = "./error/error.html"
          }
        },
        error: function (data) {
          window.location.href = "./error/error.html"
        }
      })
    },
    //登录
    loginInfo: function (data) {
      var token = active.getCookie("token")
      $.ajax({
        url: url + ":9000/login/info",
        // url: "http://10.1.3.98:9000/login/info",
        type: "post",
        async: true,
        data: data,
        success: function (data) {
          console.log(data)
          if(data.code == 200) {
            active.setCookie("token", data.data.token);
            active.setCookie("userName", data.data.userName);
            active.setCookie("userId", data.data.userId);
            active.setCookie("realName", data.data.realName);
            sessionStorage.setItem('isFirstLogin', data.data.isFirstLogin)
            // window.location.href = "views/" + data.data.url;
            window.location.href = "./" + data.data.url;  // 线上地址
          }else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            })
          }
        }
      })
    },
    getCookie: function (name) {
      var cookie = document.cookie;
      // console.log(cookie);
      var arr = cookie.split("; "); //将字符串分割成数组
      // console.log(arr);
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=");
        if (arr1[0] == name) {
          return unescape(arr1[1]);
        }
      }
      return "GG"
    },

    //封装一个函数，设置cookie
    setCookie: function (name, value, num) {
      var val = escape(value);//对value进行编码
      var date = new Date();
      date.setDate(date.getDate() + num);
      document.cookie = name+"="+val+";expires="+date;
    },
    //删除cookie
    removeCookie: function (name) {
      active.setCookie(name, 123, -1);
    },
  };
  active.init();

  $('#login').click(function() {
    // alert(1111)
    var userName = $('.userName').val()
    var password = $('.password').val()
    var loginData = {userName: userName, password: password}
    active.loginInfo(loginData)
    // console.log(111)
  })
  // 回车登录
  $(document).keypress(function(e) {
    var eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    if (eCode == 13){
      var userName = $('.userName').val()
      var password = $('.password').val()
      var loginData = {userName: userName, password: password}
      active.loginInfo(loginData)
    }
  });
  $('#dscsd').on('keypress',function() {
    // alert(1111)
    if (event.keyCode == 13) {
      var userName = $('.userName').val()
      var password = $('.password').val()
      var loginData = {userName: userName, password: password}
      active.loginInfo(loginData)
    }
    // console.log(111)
  })

  $('#updata').on('click',function(){
    window.location.href = url+':9000/login/downloadApp'
  })
  $("#searchPassword").on("click", function () {
    layer.open({
      type: 1,
      title: "找回密码", //不显示标题栏
      closeBtn: 1,
      area: ['500px', '210px'],
      shade: 0.3,
      id: 'password_layui', //设定一个id，防止重复弹出
      //btn: ['发送'],
      btnAlign: 'c',
      moveType: 1, //拖拽模式，0或者1
      content: $('#passwordHtml'),
      success: function (layero) {
        form.on('submit(test2)', function (data) {
          alert("yangzheng")
          layer.msg('也可以这样', {
            time: 2000, //20s后自动关闭
            //btn: ['明白了', '知道了']
          });
          return false;
        });
        var btn = layero.find('.layui-layer-btn');
        btn.find('.layui-layer-btn0').on('click', function (params) { });
        return false;
      }
    });
  });
  $("#usermanual").on("click", function () {
    let arr =  [
      {
        name:'用户手册8',
        text:'用户手册8'
      },
    ]
    let data = {
      currentPage:1,
      showCount:2147483647,
      pd:{
        title:""
      }
    }
    $.ajax({
      url: url + ':9006/userManual/getList',
      type: "post",
      contentType: "application/json;charset=utf-8",
      headers: {
        "token": token,
      },
      dataType: 'json',
      data: JSON.stringify(data),
      success: function (data) {
        console.log(data)
        arr = data.data.content
        let str = ''
        for (let i = 0; i < arr.length; i++) {
          // str +=`<li index='${i}'>${arr[i].title}<button id='${arr[i].id}' class='download'>下载</button></li>`
          str +="<li index='"+i+"'>"+arr[i].title+"<button id='"+arr[i].id+"' class='download'>下载</button></li>"
        }
        // console.log(str)
        $('.left>ul').html(str)
        $('.leftlist>li').on('click',function(){
          $(this).addClass('colors')
          $(this).siblings().removeClass('colors')
          // console.log($(this).attr('index'))
          $('.userinit').html(arr[$(this).attr('index')].explain)
        })
        $('.download').on('click',function(){
          // console.log($(this).attr('id'))
          let str = $(this).attr('id')
          window.location.assign(url+':9006/userManual/download?id='+str)
        })
        layer.open({
          type: 1,
          title: "用户手册", //不显示标题栏
          closeBtn: 1,
          area: ['865px', '600px'],
          shade: 0.3,
          id: 'usermanual_layui', //设定一个id，防止重复弹出
          //btn: ['发送'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#usermanualHtml'),
          cancel : function (index, layero) {
            $("#usermanualHtml").hide()
          }
        });
      }
    });

  });


  $('.lunbo').on('click',function () {
    $('.noticetitle').html(defaultTitle)
    $('.noticeinit').html(defaultText)
    let data = {
      currentPage:1,
      showCount:2147483647,
      pd:{
        title:""
      }
    }
    $.ajax({
      url: url + ':9006/notification/getList',
      type: "post",
      contentType: "application/json;charset=utf-8",
      headers: {
        "token": token,
      },
      dataType: 'json',
      data: JSON.stringify(data),
      success: function (data) {
        console.log(data)
        let arr1 = data.data.content
        let str2 = ''
        for (let i = 0; i < arr1.length; i++) {
          console.log(defaultTitle)
          if(arr1[i].title == defaultTitle){
            if(arr1[i].stress) {
              str2 += "<li index='"+i+"' class='colors'>" + "<img src='../assets/img/new.png'>" +arr1[i].title+"</li>"
            }else {
              str2 += "<li index='"+i+"' class='colors'>"+arr1[i].title+"</li>"
            }
          }else{
            if(arr1[i].stress) {
              str2 += "<li index='"+i+"'>" + "<img src='../assets/img/new.png'>" +arr1[i].title+"</li>"
            }else {
              str2 += "<li index='"+i+"'>" +arr1[i].title+"</li>"
            }
          }
        }
        console.log(str2)
        $('.lunbolist').html(str2)
        $('.lunbolist>li').on('click',function(){
          $(this).addClass('colors')
          $(this).siblings().removeClass('colors')
          console.log($(this).attr('index'))
          $('.noticetitle').html(arr1[$(this).attr('index')].title)
          $('.noticeinit').html(arr1[$(this).attr('index')].content)
        })
        // $('.lunbolist>li[text=defaultTitle]').addClass('colors')
        layer.open({
          type: 1,
          title: "通知公告", //不显示标题栏
          closeBtn: 1,
          area: ['865px', '600px'],
          shade: 0.3,
          id: 'lunbo_layui', //设定一个id，防止重复弹出
          //btn: ['发送'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#lunbolHtml'),
          cancel : function (index, layero) {
            $("#lunbolHtml").hide()
          }
        });
      }
    });

  });
})




