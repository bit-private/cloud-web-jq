vm = new Vue({
  el: "#app",
  data: function() {
    return {
      userName: "郝彦超", //用户名
      userdatapath: [],
      activeName: "first", //tab切换
      percentage: 0, //百分比
      newSpace: "0", //已用空间
      allSpace: "0", //总空间
      color: "", //进度条颜色
      folderindex: 0, //wenjianid
      newFolder: false, //新建文件夹
      editFolder: false, //编辑
      uploadFile: false, //上传文件
      fileList: [], //上传文件列表
      list: [], //上传文件列表
      moveTitle: "",
      btn: true,
      cShare: false,
      fileTypeName: false,
      cShareTime: "",
      defaultProps: {
        children: "children",
        label: "name"
      },
      isOwnFile: 0,
      cSharePeople: [],
      newFolderName: "", //新文件夹名称
      editFolderName: "", //编辑文件夹名称
      editFolderTips: "", //编辑文件夹名称
      editfileName: "", //编辑文件名称
      editfiletips: "", //编辑文件备注
      myDataShow: true, //我的数据显隐
      myDatagx: false, //共享显隐
      tipsShow: false, //备注编辑框显隐
      multipleSelectionRows: [], //选中行
      tableData: [],
      cradio: "cMyShare",
      cMyShareTableData: [],
      cMyShareEditBox: false,
      cForm: {
        flie: "",
        tips: ""
      },
      cSee: false,
      cAcceptTableData: [],
      cMySharePath: [],
      cAcceptPath: [],
      cSaveOwnBox: false, //保存到弹框
      cSaveRadio: "",
      cSaveRow: {}, //选中的保存表格row
      cWantRow: {},
      cSaveTableData: [], //保存表格数据
      cSavePath: [], //保存到路径
      cAdd: false,
      cAddName: "",
      cAddTips: "",
      cSaveAdd: false,
      cSaveAddName: "",
      cSaveAddTips: "",
      pickerOptions: {
        disabledDate:function(time){
          return time.getTime() < new Date()
        }
      },
      informationDataOrUrl: "",

      maxSize: 1024 * 1024 * 1024 * 1024, // 上传最大文件限制
      multiUploadSize: 1 * 1024 * 1024, // 大于这个大小的文件使用分块上传(后端可以支持断点续传)
      // eachSize: 1024 * 1024 * 1024 * 1024, // 每块文件大小
      eachSize: 500 * 1024 * 1024, // 每块文件大小
      requestCancelQueue: [] // 请求方法队列（调用取消上传
    }
  },
  methods: {
    checkedFile: function checkedFile(options) {
      var maxSize = this.maxSize,
        multiUploadSize = this.multiUploadSize,
        getSize = this.getSize,
        splitUpload = this.splitUpload,
        singleUpload = this.singleUpload
      var file = options.file,
        onProgress = options.onProgress,
        onSuccess = options.onSuccess,
        onError = options.onError
      console.log(file)
      var _this = this
      $.ajax({
        url: url + ":9008/uploader/beforeUpload",
        type: "post",
        data: {
          name: file.name,
          size: file.size,
          userPanId: this.userdatapath[1].id
        },
        success: function success(data) {
          if (data.code == 200) {
            var eachSize = _this.eachSize
            var chunks = Math.ceil(file.size / eachSize)
            var fileChunks = _this.splitFile(file, eachSize, chunks)
            var currentChunk = 0
            var chunkNum = 0
            _this.postFile(
              {
                chunked: true,
                md5File: md5(file.name + file.size),
                chunk: 0,
                chunks: chunks,
                eachSize: eachSize,
                fileName: file.name,
                fullSize: file.size,
                uid: file.uid,
                file: file
              },
              onProgress
            )
          } else {
            _this.$message({
              message: data.message,
              type: "error"
            })
            // _this.$refs.upload.clearFiles()
            _this.handleRemove(file)
          }
        }
      })
    },
    // 合并分片
    validateFile: function validateFile(file) {
      var _this3 = this
      console.log(file)
      var formData = new FormData()

      for (var p in file) {
        formData.append(p, file[p])
      }

      formData.append("userPanId", this.userdatapath[1].id)

      if (this.userdatapath.length > 2) {
        formData.append("fileId", this.userdatapath[this.userdatapath.length - 1].id)
      }

      return axios.post(url + ":9008/uploader/merge", formData).then(function(rs) {
        // return axios.post('http://10.1.3.59:9008/uploader/merge', formData).then(function (rs) {
        $('.upload-demo .el-progress-bar__inner').css('width', '100%')
        // debugger
        _this3.$message({
          message: "上传成功!",
          type: "success"
        })
        if (rs.status == 200) return rs.data
      })
    },
    postFile: function postFile(param, onProgress) {
      var _this = this
      var formData = new FormData()
      for (var p in param) {
        formData.append(p, param[p])
      }
      formData.append("userPanId", this.userdatapath[1].id)
      if (this.userdatapath.length > 2) {
        formData.append("fileId", this.userdatapath[this.userdatapath.length - 1].id)
      }
      var requestCancelQueue = this.requestCancelQueue
      var config = {
        cancelToken: new axios.CancelToken(function executor(cancel) {
          if (requestCancelQueue[param.uid]) {
            requestCancelQueue[param.uid]()
            delete requestCancelQueue[param.uid]
          }
          requestCancelQueue[param.uid] = cancel
        }),
        onUploadProgress: function onUploadProgress(e) {
          if (param.chunked) {
            e.percent = Number((((param.chunk * (param.eachSize - 1) + e.loaded) / param.fullSize) * 100).toFixed(2))
            console.log(e.percent)
          } else {
            e.percent = Number(((e.loaded / e.total) * 100).toFixed(2))
          }

          onProgress(e)
        }
      }
      return axios.post(url + ":9008/uploader/upload", formData, config).then(function(rs) {
        console.log(param)
        var fileData = {
          md5File: param.md5File,
          chunks: 1,
          name: param.fileName,
          fullSize: param.fullSize,
          uid: param.uid
        }
        if (rs.data) {
          _this.validateFile(fileData)
        }
        return rs.data
      })
    },
    // 文件分块,利用Array.prototype.slice方法
    splitFile: function splitFile(file, eachSize, chunks) {
      // return
      var fileChunk = []

      for (var chunk = 0; chunks > 0; chunks--) {
        fileChunk.push(file.slice(chunk, chunk + eachSize))
        chunk += eachSize
      }
      fileChunk
    },
    // 删除
    removeFile: function removeFile(file) {
      console.log(file)
      this.requestCancelQueue[file.uid]()
      delete this.requestCancelQueue[file.uid]
      return true
    },
    submitUpload: function submitUpload() {},
    //
    choseShare: function choseShare() {
      this.cShareTime = ""
      this.$refs.tree.setCheckedNodes([])
    },
    cGetTree: function cGetTree() {
      var _that = this

      $.ajax({
        url: url + ":9008/panFileShareManage/findDepartmental",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          userId: this.getCookie("userId")
        },
        success: function success(data) {
          _that.cSharePeople = data.data
        },
        error: function error(data) {
          console.log(data)
        }
      })
    },
    submitUpload:function() {},
    //
    handlePath:function(index) {
      if (index == 0) {
        this.userdatapath = [this.userdatapath[0]]
      } else {
        let arr = this.userdatapath.slice(0, index + 1)
        this.userdatapath = arr
      }
      this.btnshow()
      if (this.userdatapath.length == 1) {
        this.spacesize()
        this.getDatas()
      } else {
        this.callBackdata()
      }
    },
    handleSharePathFirst:function() {
      this.cMySharePath = []
      this.getCMyShareTableData()
    },
    handleSharePath:function(index) {
      if (index == 0) {
        this.cMySharePath = [this.cMySharePath[0]]
      } else {
        let arr = this.cMySharePath.slice(0, index + 1)
        this.cMySharePath = arr
      }
      if (this.cMySharePath.length != 0) {
        this.getCMyShareTableDataLength()
      } else {
        this.getCMyShareTableData()
      }
    },
    handleCAcceptPathFirst:function() {
      this.cAcceptPath = []
      this.getCAcceptTableData()
    },
    handleCAcceptPath:function(index) {
      if (index == 0) {
        this.cAcceptPath = [this.cAcceptPath[0]]
      } else {
        let arr = this.cAcceptPath.slice(0, index + 1)
        this.cAcceptPath = arr
      }
      if (this.cAcceptPath.length != 0) {
        this.getClickLength()
      } else {
        this.getCAcceptTableData()
      }
    },
    choseShare:function() {
      this.cShareTime = ""
      this.$refs.tree.setCheckedNodes([])
    },
    cGetTree:function() {
      let _that = this
      $.ajax({
        url: url + ":9008/panFileShareManage/findDepartmental",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: { userId: this.getCookie("userId") },
        success: function(data) {
          _that.cSharePeople = data.data
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //我的共享文件夹点击
    cClickMyShare:function(param) {
      let _that = this
      this.cMySharePath.push(param)
      $.ajax({
        url: url + ":9008/panFileShareManage/shareList",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          userId: this.getCookie("userId"),
          shareId: this.cMySharePath[0].id,
          parentId: this.cMySharePath[this.cMySharePath.length - 1].panFileId
        },
        success: function(data) {
          if (data.code == 200) {
            _that.cMyShareTableData = data.data
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //接受共享文件夹点击
    cClickMyAccept:function(param) {
      let _that = this
      this.cAcceptPath.push(param)

      this.getClickLength()
    },
    getClickLength:function() {
      let _that = this
      $.ajax({
        url: url + ":9008/panFileShareManage/acceptList",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          userId: this.getCookie("userId"),
          shareId: this.cAcceptPath[0].id,
          parentId: this.cAcceptPath[this.cAcceptPath.length - 1].panFileId
        },
        success: function(data) {
          if (data.code == 200) {
            _that.cAcceptTableData = data.data
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //切换页面
    cRadioChange:function() {
      if (this.cradio == "cMyShare") {
        this.cMySharePath = []
        this.cAcceptPath = []
        this.getCMyShareTableData()
      } else {
        this.cMySharePath = []
        this.cAcceptPath = []
        this.getCAcceptTableData()
      }
    },
    //我的共享
    getCMyShareTableData:function() {
      let _that = this
      $.ajax({
        url: url + ":9008/panFileShareManage/shareList",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: { userId: this.getCookie("userId") },
        success: function(data) {
          _that.cMyShareTableData = data.data
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getCMyShareTableDataLength:function() {
      let _that = this
      $.ajax({
        url: url + ":9008/panFileShareManage/shareList",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          userId: this.getCookie("userId"),
          shareId: this.cMySharePath[0].id,
          parentId: this.cMySharePath[this.cMySharePath.length - 1].panFileId
        },
        success: function(data) {
          if (data.code == 200) {
            _that.cMyShareTableData = data.data
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //接受共享
    getCAcceptTableData:function() {
      let _that = this
      $.ajax({
        url: url + ":9008/panFileShareManage/acceptList",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: { userId: this.getCookie("userId") },
        success: function(data) {
          _that.cAcceptTableData = data.data
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    btnshow:function() {
      if (this.userdatapath.length > 1) {
        this.btn = true
      } else {
        this.btn = false
      }
    },
    gofolder:function(a) {
      this.userdatapath.push(a)
      let _that = this
      if (this.userdatapath.length == 2) {
        $.ajax({
          url: url + ":9008/userPanManage/getPanSpace",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: { id: a.id },
          success: function(data) {
            console.log(data)
            if (data.code == 200) {
              _that.newSpace = data.data.usage //已用空间
              _that.allSpace = data.data.spaceSize //总空间
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          // url: "http://10.1.3.59:9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: "",
            userPanId: a.id,
            parentId: ""
          },
          success: function(data) {
            console.log(data)
            _that.tableData = data.data
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: "",
            userPanId: this.userdatapath[1].id,
            parentId: a.id
          },
          success: function(data) {
            console.log(data)
            _that.tableData = data.data
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
      this.percentages()
      this.btnshow()
    },
    nofolder:function() {
      console.log(22222)
    },
    changeCMySharePath:function() {
      if (this.cMySharePath.length > 0) {
        this.cMySharePath.pop()
      }
      if (this.cMySharePath.length != 0) {
        this.getCMyShareTableDataLength()
      } else {
        this.getCMyShareTableData()
      }
    },
    changeCAcceptPath:function() {
      if (this.cAcceptPath.length > 0) {
        this.cAcceptPath.pop()
      }
      if (this.cAcceptPath.length != 0) {
        this.getClickLength()
      } else {
        this.getCAcceptTableData()
      }
    },
    init:function() {
      this.getDatas()
      this.btnshow()
      this.userchinese()
      this.spacesize()
      this.percentages()
      this.customColorMethod()
      // this.getsize()
    },
    cShareClick:function() {
      if (this.multipleSelectionRows.length === 0) {
        layer.msg("请选择要共享的文件", {
          icon: 7,
          closeBtn: 1
        })
      } else {
        this.cShare = true
      }
    },
    cShareSure:function() {
      let nodeNameArr = []
      let nodeKeyArr = []
      let _that = this
      this.$refs.tree.getCheckedNodes().map(function (item){
        if (item.children == undefined) {
          nodeNameArr.push(item.name)
          nodeKeyArr.push(item.id)
        }
      })
      if (this.cShareTime == "") {
        layer.msg("请选择共享截止时间", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      if (nodeKeyArr.length == 0) {
        layer.msg("请选择共享人", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      let checkRow = []
      for (let i = 0; i < this.multipleSelectionRows.length; i++) {
        checkRow.push(this.multipleSelectionRows[i].id)
      }
      $.ajax({
        url: url + ":9008/panFileShareManage/shareFileToUser",
        type: "post",
        async: false,
        headers: {
          token: token
        },
        data: {
          userId: this.getCookie("userId"),
          appectIds: nodeKeyArr.join(","),
          fileIds: checkRow.join(","),
          endDate: this.dateFtt("yyyy-MM-dd", new Date(this.cShareTime))
        },
        success: function(data) {
          layer.msg(data.message, {
            icon: 1,
            closeBtn: 1
          })
          _that.cShare = false
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    cShareCancel:function() {
      this.cShareTime = ""
      this.$refs.tree.setCheckedNodes([])
    },
    mydatas:function() {
      if (this.activeName == "first") {
        this.myDataShow = true
        this.myDatagx = false
        this.cMySharePath = []
        this.cAcceptPath = []
        this.userdatapath = []
        this.init()
      } else {
        this.myDataShow = false
        this.myDatagx = true
        this.userdatapath = []
        this.getCMyShareTableData()
      }
    },
    percentages:function() {
      this.percentage = (this.newSpace / this.allSpace) * 100
      this.customColorMethod()
    },
    handleClick:function(tab, event) {
      this.mydatas()
    },
    customColorMethod:function() {
      console.log(this.percentage)
      if (this.percentage < 30) {
        this.color = "#1989fa"
      } else if (this.percentage <= 70) {
        this.color = "orange"
      } else {
        this.color = "#f56c6c"
      }
    },
    handleSelectChangeLeft:function(rows) {
      this.multipleSelectionRows = rows
    },
    handleEdit:function(index, row) {
      console.log(row)
      console.log(index)
    },
    handleClose:function(done) {
      let _this=this
      this.$confirm("确认关闭？")
        .then(function(){
          _this.$refs.upload.clearFiles()
          _this.callBackdata()
          done()
        })
        .catch(function(){})
    },
    //新建文件夹
    newFolders:function() {
      this.newFolder = true
    },
    // 确认新建
    addFolder:function() {
      if (this.cAddName == "" || String(this.cAddName).replace(/\s+/g, "") == "") {
        layer.msg("文件夹名称不能为空", {
          icon: 7,
          closeBtn: 1
        })
        this.cAddName = ""
        return
      }
      let _that = this
      if (this.userdatapath.length == 2) {
        $.ajax({
          url: url + ":9008/panFileManage/addDirectory",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userPanId: this.userdatapath[this.userdatapath.length - 1].id,
            parentId: "",
            name: this.cAddName,
            remarks: this.cAddTips
          },
          success: function(data) {
            console.log(data)
            if (data.code == 200) {
              _that.cAddName = ""
              _that.cAddTips = ""
              _that.cAdd = false
              _that.callBackdata()
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        $.ajax({
          url: url + ":9008/panFileManage/addDirectory",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userPanId: "",
            parentId: this.userdatapath[this.userdatapath.length - 1].id,
            name: this.cAddName,
            remarks: this.cAddTips
          },
          success: function(data) {
            if (data.code == 200) {
              _that.cAdd = false
              _that.callBackdata()
              _that.cAddName = ""
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
            _that.cAddTips = ""
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
    },
    // 确认新建
    addSaveFolder:function() {
      let _that = this
      if (this.cSaveAddName == "" || String(this.cSaveAddName).replace(/\s+/g, "") == "") {
        layer.msg("文件夹名称不能为空", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      if (this.cSavePath.length == 1) {
        console.log(3434)
        $.ajax({
          url: url + ":9008/panFileManage/addDirectory",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userPanId: this.cSavePath[this.cSavePath.length - 1].id,
            parentId: "",
            name: this.cSaveAddName,
            remarks: this.cSaveAddTips
          },
          success: function(data) {
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              _that.cSaveAdd = false
              _that.cgetSaveTable()
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        $.ajax({
          url: url + ":9008/panFileManage/addDirectory",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userPanId: "",
            parentId: this.cSavePath[this.cSavePath.length - 1].id,
            name: this.cSaveAddName,
            remarks: this.cSaveAddTips
          },
          success: function(data) {
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              _that.cSaveAdd = false
              _that.cgetSaveTable()
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
    },
    cSaveAddClosed:function() {
      this.cSaveAddName = ""
      this.cSaveAddTips = ""
    },
    //   取消新建
    noFolder:function() {
      let _that = this
      _that.cAddName = ""
      _that.cAddTips = ""
      _that.cAdd = false
    },
    //编辑
    edit:function() {
      if (this.multipleSelectionRows.length === 0 || this.multipleSelectionRows.length > 1) {
        layer.msg("请选择一项进行编辑", {
          icon: 7,
          closeBtn: 1
        })
      } else if (this.userdatapath.length == 1) {
        this.fileTypeName = true
        this.editFolder = true
        this.editFolderName = this.multipleSelectionRows[0].fileName
        this.editfiletips = this.multipleSelectionRows[0].remarks
      } else {
        if (this.multipleSelectionRows[0].fileType == "文件夹") {
          this.fileTypeName = false
          this.editFolder = true
          this.editFolderName = this.multipleSelectionRows[0].fileName
          this.editfiletips = this.multipleSelectionRows[0].remarks
        } else {
          this.fileTypeName = true
          this.editFolder = true
          this.editFolderName = this.multipleSelectionRows[0].fileName
          this.editfiletips = this.multipleSelectionRows[0].remarks
        }
      }
    },
    // 确认编辑
    trueFolder:function() {
      if (this.userdatapath.length == 1) {
        let _that = this
        let obj = {
          remarks: this.editfiletips,
          id: this.multipleSelectionRows[0].id
        }
        $.ajax({
          url: url + ":9008/userPanManage/editUserPanRemarks",
          type: "post",
          async: false,
          contentType: "application/json;charset=utf-8",
          headers: {
            token: this.getCookie("token")
          },
          data: JSON.stringify(obj),
          success: function(data) {
            if (data.code == 200) {
              _that.editFolderName = ""
              _that.editfiletips = ""
              _that.editFolder = false
              _that.fileTypeName = false
              _that.getDatas()
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        let _that = this
        console.log(22)
        if (this.editFolderName == "" || String(this.editFolderName).replace(/\s+/g, "") == "") {
          layer.msg("请填写文件夹名称", {
            icon: 7,
            closeBtn: 1
          })
          return
        }
        let obj = {
          fileName: this.editFolderName,
          remarks: this.editfiletips,
          id: this.multipleSelectionRows[0].id,
          fileType: this.fileTypeName ? 0 : 1
        }
        $.ajax({
          url: url + ":9008/panFileManage/editUserPanFile",
          type: "post",
          async: false,
          contentType: "application/json;charset=utf-8",
          headers: {
            token: this.getCookie("token")
          },
          data: JSON.stringify(obj),
          success: function(data) {
            if (data.code == 200) {
              _that.editFolderName = ""
              _that.editfiletips = ""
              _that.editFolder = false
              _that.fileTypeName = false
              _that.callBackdata()
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              // _that.editFolderName = ''
              // _that.editfiletips = ''
              // _that.editFolder = false
              // _that.fileTypeName = false
              // _that.callBackdata()
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
    },
    // 取消编辑
    noEditFolder:function() {
      let _that = this
      _that.editFolderName = ""
      _that.editfiletips = ""
      _that.editFolder = false
    },
    //   删除
    delFolder:function() {
      if (this.multipleSelectionRows.length === 0) {
        layer.msg("请选择一项删除", {
          icon: 7,
          closeBtn: 1
        })
      } else {
        let _that = this
        let arr = []
        for (let j = 0; j < this.multipleSelectionRows.length; j++) {
          arr.push(this.multipleSelectionRows[j].id)
        }
        this.$confirm("是否删除选中文件文件?", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        })
          .then(function(){
            $.ajax({
              url: url + ":9008/panFileManage/batchDeleteUserFile",
              type: "post",
              async: false,
              headers: {
                token: token
                // "Content-Type": "application/json;charset=utf-8"
              },
              data: { ids: arr.join(",") },
              success: function(data) {
                if (data.code == 200) {
                  layer.msg("删除成功", {
                    icon: 1,
                    closeBtn: 1
                  })
                  _that.callBackdata()
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          })
          .catch(function(){})
      }
    },
    //   上传
    uploads:function() {
      this.uploadFile = true
    },
    cDownloadFile:function(arr) {
      //获得id为downLoadListFrame的frame
      var src = url + ":9008/uploader/downloadFile?fileIds=" + arr.join(",")
      var divFrame = window.parent.document.getElementById("downLoadListFrame")
      //判断是否存在，如果存在先移除，再重新创建
      if (divFrame != null) {
        window.parent.document.body.removeChild(divFrame)
      }
      //重新创建
      var iframe = window.parent.document.createElement("iframe")
      iframe.setAttribute("id", "downLoadListFrame")
      //download_file.id = "downFrame";
      window.parent.document.body.appendChild(iframe)
      divFrame = window.parent.document.getElementById("downLoadListFrame")
      //隐式调用，（注意：window.parent.document 适应对于，farme内部代码，调用frame 外部dom；如果项目未用frame，改写成 document即可）
      divFrame.src = src
      divFrame.style.display = "none"
      if (!/*@cc_on!@*/ 0) {
        //if not IE
        iframe.onload = function() {
          layer.msg("下载文件失败", {
            icon: 7,
            closeBtn: 1
          })
        }
      } else {
        iframe.onreadystatechange = function() {
          if (iframe.readyState == "complete") {
            layer.msg("下载文件失败", {
              icon: 7,
              closeBtn: 1
            })
          }
        }
      }
    },
    upDatas:function() {
      if (this.multipleSelectionRows.length === 0) {
        layer.msg("请选择一项下载", {
          icon: 7,
          closeBtn: 1
        })
      } else {
        let arr = []
        for (let i = 0; i < this.multipleSelectionRows.length; i++) {
          arr.push(this.multipleSelectionRows[i].id)
        }
        console.log(arr)
        let filesize = 0
        $.ajax({
          url: url + ":9008/uploader/getFileSizeLimit",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            fileIds: arr.join(",")
          },
          success: function(data) {
            console.log(data)
            filesize = data.data
          },
          error: function(data) {
            console.log(data)
          }
        })
        if (filesize == "-1") {
          this.cDownloadFile(arr)
          // window.location.assign(url + ':9008/uploader/downloadFile?fileIds=' + arr.join(','));
        } else if (filesize == "0") {
          layer.msg("下载文件超出大小限制", {
            icon: 7,
            closeBtn: 1
          })
        }
      }
    },
    handleChange:function(file, fileList) {
      for (let i = 0; i < fileList.length; i++) {
        fileList[i].tips = ""
      }
      this.fileList = fileList
    },
    trueload:function() {
      let _this=this
      this.$confirm("确认关闭？")
        .then(function(){
          _this.uploadFile = false
          _this.$refs.upload.clearFiles()
          _this.callBackdata()
          done()
        })
        .catch(function(){})
    },
    noload:function() {
      let _this=this
      this.$confirm("确认关闭？")
        .then(function(){
          _this.uploadFile = false
          _this.$refs.upload.clearFiles()
          _this.callBackdata()
          done()
        })
        .catch(function(){})
    },
    handlePreview:function() {},
    handleRemove:function(file, fileList) {
      for (let i = 0; i < this.$refs.upload.uploadFiles.length; i++) {
        // if (this.fileList[i].name == file.name) {
        //   this.fileList.splice(i, 1)
        // }
        if (this.$refs.upload.uploadFiles[i].name == file.name) {
          this.$refs.upload.uploadFiles.splice(i, 1)
        }
      }
      // this.fileList = fileList
    },
    fileTips:function() {
      // console.log(this.fileList)
    },
    cMyShareSelectChange:function() {},
    cMyShareEdit:function() {
      if (this.$refs.cMyShareTable.selection.length != 1) {
        layer.msg("请选择一项进行编辑", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      this.cMyShareEditBox = true
      this.$set(this.cForm, "flie", this.$refs.cMyShareTable.selection[0].panFileName)
      this.$set(this.cForm, "tips", this.$refs.cMyShareTable.selection[0].remarks)
      this.isOwnFile = this.$refs.cMyShareTable.selection[0].fileType == "文件夹" ? 1 : 0
    },
    cMyShareCancel:function() {
      let _that = this
      if (this.$refs.cMyShareTable.selection.length == 0) {
        layer.msg("请选择要取消共享的文件", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      let arr = []
      for (let i = 0; i < this.$refs.cMyShareTable.selection.length; i++) {
        arr.push(this.$refs.cMyShareTable.selection[i].id)
      }
      this.$confirm("确定要取消共享吗？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(function() {
          $.ajax({
            url: url + ":9008/panFileShareManage/cancelShare",
            type: "post",
            async: true,
            headers: {
              token: token
            },
            data: { ids: arr.join(",") },
            success: function(data) {
              if (data.code == 200) {
                layer.msg(data.message, {
                  icon: 1,
                  closeBtn: 1
                })
                if (_that.cMySharePath.length != 0) {
                  _that.getCMyShareTableDataLength()
                } else {
                  _that.getCMyShareTableData()
                }
              } else {
                layer.msg(data.message, {
                  icon: 7,
                  closeBtn: 1
                })
              }
            },
            error: function(data) {
              console.log(data)
            }
          })
        })
        .catch(function() {})
    },
    cMyShareDown:function() {
      if (this.$refs.cMyShareTable.selection.length == 0) {
        layer.msg("请选择要下载的文件", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      let arr = []
      for (let i = 0; i < this.$refs.cMyShareTable.selection.length; i++) {
        arr.push(this.$refs.cMyShareTable.selection[i].panFileId)
      }
      console.log(arr)
      let filesize = 0
      $.ajax({
        url: url + ":9008/uploader/getFileSizeLimit",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          fileIds: arr.join(",")
        },
        success: function(data) {
          console.log(data)
          filesize = data.data
        },
        error: function(data) {
          console.log(data)
        }
      })
      if (filesize == "-1") {
        this.cDownloadFile(arr)
        // window.location.assign(url + ':9008/uploader/downloadFile?fileIds=' + arr.join(','));
      } else if (filesize == "0") {
        layer.msg("下载文件超出大小限制", {
          icon: 7,
          closeBtn: 1
        })
      }
    },
    cSubmit:function() {
      let _that = this
      let params = {}
      if (this.cradio == "cMyShare") {
        params = {
          fileName: this.cForm.flie,
          remarks: this.cForm.tips,
          id: this.$refs.cMyShareTable.selection[0].panFileId,
          fileType: this.isOwnFile
        }
      } else {
        params = {
          fileName: this.cForm.flie,
          remarks: this.cForm.tips,
          id: this.$refs.cAcceptTable.selection[0].panFileId,
          fileType: this.isOwnFile
        }
      }
      $.ajax({
        url: url + ":9008/panFileManage/editUserPanFile",
        type: "post",
        async: false,
        contentType: "application/json;charset=utf-8",
        headers: {
          token: token
        },
        data: JSON.stringify(params),
        dataType: "json",
        success: function(data) {
          if (data.code == 200) {
            layer.msg("编辑成功", {
              icon: 1,
              closeBtn: 1
            })
            _that.cMyShareEditBox = false
            if (_that.cMySharePath.length != 0) {
              _that.getCMyShareTableDataLength()
            } else {
              _that.getCMyShareTableData()
            }
            if (_that.cAcceptPath.length != 0) {
              _that.getClickLength()
            } else {
              _that.getCAcceptTableData()
            }
          } else {
            layer.msg(data.message, {
              icon: 7,
              closeBtn: 1
            })
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    cClickSave:function() {
      if (this.moveTitle == "保存到") {
        if (this.cSaveRadio == "") {
          layer.msg("请选择目标文件夹", {
            icon: 7,
            closeBtn: 1
          })
          return
        }
        let _that = this
        let param = {}
        if (this.cSavePath.length > 0) {
          param = {
            userPanId: this.cSaveRow.userPanId,
            placeId: this.cSaveRow.id,
            fileIds: this.cWantRow.row.panFileId
          }
        } else {
          param = {
            userPanId: this.cSaveRow.id,
            placeId: "",
            fileIds: this.cWantRow.row.panFileId
          }
        }
        $.ajax({
          url: url + ":9008/panFileManage/addShareFile",
          type: "post",
          async: false,
          headers: {
            token: token
          },
          data: param,
          success: function(data) {
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              _that.cSaveOwnBox = false
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else if (this.moveTitle == "移动到") {
        if (this.cSaveRadio == "") {
          layer.msg("请选择目标文件夹", {
            icon: 7,
            closeBtn: 1
          })
          return
        }
        let _that = this
        let param = {}
        let arr = []
        for (let i = 0; i < this.multipleSelectionRows.length; i++) {
          arr.push(this.multipleSelectionRows[i].id)
        }
        if (this.cSavePath.length > 0) {
          param = {
            userPanId: "",
            placeId: this.cSaveRow.id,
            fileIds: arr.join(",")
          }
        } else {
          param = {
            userPanId: this.cSaveRow.id,
            placeId: "",
            fileIds: arr.join(",")
          }
        }
        $.ajax({
          url: url + ":9008/panFileManage/userPanFileMove",
          type: "post",
          async: false,
          headers: {
            token: token
          },
          data: param,
          success: function(data) {
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              _that.cSaveOwnBox = false
              _that.callBackdata()
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else if (this.moveTitle == "复制到") {
        if (this.cSaveRadio == "") {
          layer.msg("请选择目标文件夹", {
            icon: 7,
            closeBtn: 1
          })
          return
        }
        let _that = this
        let param = {}
        let arr = []
        for (let i = 0; i < this.multipleSelectionRows.length; i++) {
          arr.push(this.multipleSelectionRows[i].id)
        }
        if (this.cSavePath.length > 0) {
          param = {
            userPanId: "",
            placeId: this.cSaveRow.id,
            fileIds: arr.join(",")
          }
        } else {
          param = {
            userPanId: this.cSaveRow.id,
            placeId: "",
            fileIds: arr.join(",")
          }
        }
        $.ajax({
          url: url + ":9008/panFileManage/userPanFileCopy",
          type: "post",
          async: false,
          headers: {
            token: token
          },
          data: param,
          success: function(data) {
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              _that.cSaveOwnBox = false
              _that.callBackdata()
            } else {
              layer.msg(data.message, {
                icon: 7,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
    },
    clfgav:function() {
      this.cSaveRadio = ""
      this.cSavePath = []
    },
    open:function(param) {
      // this.cSee = true;
      let _that = this
      if (param.panFileId) {
        window.open("./../../../public/static/web/viewer.html?url_file=panFileManage/filePreview?fileId=" + param.panFileId)
      } else {
        window.open("./../../../public/static/web/viewer.html?url_file=panFileManage/filePreview?fileId=" + param.id)
      }
    },
    delGetUrl:function() {
      let _that = this
      if (this.informationDataOrUrl != "") {
        $.ajax({
          url: url + ":9008/panFileManage/deletePDF",
          type: "post",
          async: false,
          headers: {
            token: token
          },
          data: { filePath: this.informationDataOrUrl },
          success: function(data) {
            if (data.code == 200) {
            } else {
              // layer.msg(data.message, {
              //   icon: 7,
              //   closeBtn: 1
              // })
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
    },
    cAcceptEdit:function() {
      if (this.$refs.cAcceptTable.selection.length != 1) {
        layer.msg("请选择一项进行编辑", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      this.cMyShareEditBox = true
      this.$set(this.cForm, "flie", this.$refs.cAcceptTable.selection[0].panFileName)
      this.$set(this.cForm, "tips", this.$refs.cAcceptTable.selection[0].remarks)
      this.isOwnFile = this.$refs.cAcceptTable.selection[0].fileType == "文件夹" ? 1 : 0
    },
    cAcceptDown:function() {
      if (this.$refs.cAcceptTable.selection.length == 0) {
        layer.msg("请选择要下载的文件", {
          icon: 7,
          closeBtn: 1
        })
        return
      }
      let arr = []
      for (let i = 0; i < this.$refs.cAcceptTable.selection.length; i++) {
        arr.push(this.$refs.cAcceptTable.selection[i].panFileId)
      }
      console.log(arr)
      let filesize = 0
      $.ajax({
        url: url + ":9008/uploader/getFileSizeLimit",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          fileIds: arr.join(",")
        },
        success: function(data) {
          console.log(data)
          filesize = data.data
        },
        error: function(data) {
          console.log(data)
        }
      })
      if (filesize == "-1") {
        this.cDownloadFile(arr)
        // window.location.assign(url + ':9008/uploader/downloadFile?fileIds=' + arr.join(','));
      } else if (filesize == "0") {
        layer.msg("下载文件超出大小限制", {
          icon: 7,
          closeBtn: 1
        })
      }
    },
    cAcceptSelectChange:function() {},
    cAcceptOpen:function() {},
    dateFtt:function(fmt, date) {
      //author: meizz
      var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        S: date.getMilliseconds() //毫秒
      }
      if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length))
      for (var k in o) if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length))
      return fmt
    },
    getCookie: function(name) {
      var cookie = document.cookie
      var arr = cookie.split("; ") //将字符串分割成数组
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=")
        if (arr1[0] == name) {
          return unescape(arr1[1])
        }
      }
      return "GG"
    },
    cSaveBtn:function(a, b) {
      // console.log(this.multipleSelectionRows)
      console.log(a, b)
      if (a == "move") {
        if (this.multipleSelectionRows.length < 1) {
          layer.msg("请选择一项进行移动", {
            icon: 7,
            closeBtn: 1
          })
        } else {
          this.moveTitle = "移动到"
          this.cSaveOwnBox = true
          let _that = this
          if (this.cSavePath.length == 0) {
            let _that = this
            $.ajax({
              url: url + ":9008/panFileManage/getFileList",
              type: "post",
              async: false,
              headers: {
                token: this.getCookie("token")
              },
              data: {
                userId: this.getCookie("userId"),
                userPanId: "",
                parentId: ""
              },
              success: function(data) {
                console.log(data)
                // _that.tableData = data.data
                _that.cSaveTableData = []
                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].fileType == "文件夹") {
                    _that.cSaveTableData.push(data.data[i])
                  }
                  // _that.cSaveTableData = data.data
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          } else if (this.cSavePath.length == 1) {
            $.ajax({
              url: url + ":9008/panFileManage/getFileList",
              type: "post",
              async: false,
              headers: {
                token: this.getCookie("token")
              },
              data: {
                userId: "",
                userPanId: this.cSavePath[this.cSavePath.length - 1].id,
                parentId: ""
              },
              success: function(data) {
                console.log(data)
                _that.cSaveTableData = []
                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].fileType == "文件夹") {
                    _that.cSaveTableData.push(data.data[i])
                  }
                  // _that.cSaveTableData = data.data
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          } else {
            $.ajax({
              url: url + ":9008/panFileManage/getFileList",
              type: "post",
              async: false,
              headers: {
                token: this.getCookie("token")
              },
              data: {
                userId: "",
                userPanId: this.cSavePath[0].id,
                parentId: this.cSavePath[this.cSavePath.length - 1].id
              },
              success: function(data) {
                _that.cSaveTableData = []
                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].fileType == "文件夹") {
                    _that.cSaveTableData.push(data.data[i])
                  }
                  // _that.cSaveTableData = data.data
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          }
        }
      } else if (a == "copy") {
        if (this.multipleSelectionRows.length < 1) {
          layer.msg("请选择一项进行复制", {
            icon: 7,
            closeBtn: 1
          })
        } else {
          this.moveTitle = "复制到"
          this.cSaveOwnBox = true
          let _that = this
          if (this.cSavePath.length == 0) {
            let _that = this
            $.ajax({
              url: url + ":9008/panFileManage/getFileList",
              type: "post",
              async: false,
              headers: {
                token: this.getCookie("token")
              },
              data: {
                userId: this.getCookie("userId"),
                userPanId: "",
                parentId: ""
              },
              success: function(data) {
                console.log(data)
                _that.cSaveTableData = []
                // _that.tableData = data.data
                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].fileType == "文件夹") {
                    _that.cSaveTableData.push(data.data[i])
                  }
                  // _that.cSaveTableData = data.data
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          } else if (this.cSavePath.length == 1) {
            $.ajax({
              url: url + ":9008/panFileManage/getFileList",
              type: "post",
              async: false,
              headers: {
                token: this.getCookie("token")
              },
              data: {
                userId: "",
                userPanId: this.cSavePath[this.cSavePath.length - 1].id,
                parentId: ""
              },
              success: function(data) {
                console.log(data)
                _that.cSaveTableData = []
                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].fileType == "文件夹") {
                    _that.cSaveTableData.push(data.data[i])
                  }
                  // _that.cSaveTableData = data.data
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          } else {
            $.ajax({
              url: url + ":9008/panFileManage/getFileList",
              type: "post",
              async: false,
              headers: {
                token: this.getCookie("token")
              },
              data: {
                userId: "",
                userPanId: this.cSavePath[0].id,
                parentId: this.cSavePath[this.cSavePath.length - 1].id
              },
              success: function(data) {
                _that.cSaveTableData = []
                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].fileType == "文件夹") {
                    _that.cSaveTableData.push(data.data[i])
                  }
                  // _that.cSaveTableData = data.data
                }
              },
              error: function(data) {
                console.log(data)
              }
            })
          }
        }
      } else if (a == "save") {
        this.moveTitle = "保存到"
        this.cSaveOwnBox = true
        this.getDatas()
        this.cWantRow = b
      }
    },
    cGetTemplateRow:function(index, row) {
      this.cSaveRow = row
    },
    cSaveClickRow:function(index, row) {
      this.cSavePath.push(row)
      this.cSaveRadio = ""
      this.cgetSaveTable()
    },
    cgetSaveTable:function() {
      let _that = this
      if (this.cSavePath.length == 0) {
        let _that = this
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: this.getCookie("userId"),
            userPanId: "",
            parentId: ""
          },
          success: function(data) {
            console.log(data)
            // _that.tableData = data.data
            _that.cSaveTableData = []
            for (let i = 0; i < data.data.length; i++) {
              if (data.data[i].fileType == "文件夹") {
                _that.cSaveTableData.push(data.data[i])
              }
              // _that.cSaveTableData = data.data
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else if (this.cSavePath.length == 1) {
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: "",
            userPanId: this.cSavePath[this.cSavePath.length - 1].id,
            parentId: ""
          },
          success: function(data) {
            console.log(data)
            _that.cSaveTableData = []
            for (let i = 0; i < data.data.length; i++) {
              if (data.data[i].fileType == "文件夹") {
                _that.cSaveTableData.push(data.data[i])
              }
              // _that.cSaveTableData = data.data
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: "",
            userPanId: this.cSavePath[0].id,
            parentId: this.cSavePath[this.cSavePath.length - 1].id
          },
          success: function(data) {
            _that.cSaveTableData = []
            for (let i = 0; i < data.data.length; i++) {
              if (data.data[i].fileType == "文件夹") {
                _that.cSaveTableData.push(data.data[i])
              }
              // _that.cSaveTableData = data.data
            }
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
    },
    handleSavePath:function(index) {
      if (index == 0) {
        this.cSavePath = [this.cSavePath[0]]
      } else {
        let arr = this.cSavePath.slice(0, index + 1)
        this.cSavePath = arr
      }
      this.cSaveRadio = ""
      this.cgetSaveTable()
    },
    changeCSavePath:function() {
      if (this.cSavePath.length > 0) {
        this.cSavePath.pop()
      }
      this.cSaveRadio = ""
      this.cgetSaveTable()
    },
    changeuserPath:function() {
      if (this.userdatapath.length > 1) {
        this.userdatapath.pop()
      }
      this.btnshow()
      if (this.userdatapath.length == 1) {
        this.spacesize()
        this.getDatas()
      } else {
        this.callBackdata()
      }
    },
    newFolders:function() {
      this.cAdd = true
    },
    cNewFolders:function() {
      this.cSaveAdd = true
    },
    getDatas:function() {
      let _that = this
      $.ajax({
        url: url + ":9008/panFileManage/getFileList",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        data: {
          userId: this.getCookie("userId"),
          userPanId: "",
          parentId: ""
        },
        success: function(data) {
          console.log(data)
          _that.tableData = data.data
          _that.cSaveTableData = []
          for (let i = 0; i < data.data.length; i++) {
            if (data.data[i].fileType == "文件夹") {
              _that.cSaveTableData.push(data.data[i])
            }
            // _that.cSaveTableData = data.data
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    callBackdata:function() {
      let _that = this
      if (this.userdatapath.length == 2) {
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: "",
            userPanId: this.userdatapath[1].id,
            parentId: ""
          },
          success: function(data) {
            console.log(data)
            _that.tableData = data.data
          },
          error: function(data) {
            console.log(data)
          }
        })
      } else {
        $.ajax({
          url: url + ":9008/panFileManage/getFileList",
          type: "post",
          async: false,
          headers: {
            token: this.getCookie("token")
          },
          data: {
            userId: "",
            userPanId: this.userdatapath[1].id,
            parentId: this.userdatapath[this.userdatapath.length - 1].id
          },
          success: function(data) {
            console.log(data)
            _that.tableData = data.data
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
      this.btnshow()
    },
    userchinese:function() {
      let obj = {
        fileName: this.getCookie("realName"),
        id: this.getCookie("userId")
      }
      this.userdatapath.push(obj)
    },
    spacesize:function() {
      let _that = this
      let realName = this.getCookie("userName") // 获取用户id
      $.ajax({
        url: url + ":9008/userPanManage/getUserPanList",
        type: "post",
        contentType: "application/json;charset=utf-8",
        async: false,
        data: JSON.stringify({
          pd: { title: realName },
          currentPage: 1,
          showCount: 10
        }),
        headers: {
          token: _that.getCookie("token")
        },
        dataType: "json",
        success: function(data) {
          if (data.code == 200) {
            _that.newSpace = data.data.content[0].useSize //已用空间
            _that.allSpace = data.data.content[0].spaceSize //总空间
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //获取限制下载大小
    getsize:function() {
      $.ajax({
        url: url + ":9008/uploader/getFileSizeLimit",
        type: "post",
        async: false,
        headers: {
          token: this.getCookie("token")
        },
        success: function(data) {
          console.log(data)
        },
        error: function(data) {
          console.log(data)
        }
      })
    }
  },
  // computed: {},
  mounted:function() {
    this.cGetTree()
    this.init()
  }
})
