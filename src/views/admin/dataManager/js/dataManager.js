
vm = new Vue({
    el: '#app',
    data: function () {
        return {
         search:"",
         dialogVisible:false,
         customColors: [
             {color: '#f56c6c', percentage: 20},
             {color: '#6f7ad3', percentage: 40},
             {color: '#00B070', percentage: 60},
             {color: '#1E9FFF', percentage: 80},
             {color: '#ca320d', percentage: 100}
         ],
          form:{
              name:'',
              space:''
          },
          currentPage:1,
          pageSize:10,
          tableDataTotal:0,
          tableData:[],
          detailTableData:[],
          deatilVisible:false,
          editID:{},
          user:{},
        }
    },
    methods: {
        resetSearch(){
            this.search=''
            this.currentPage=1;
            this.init()
        },
        searchTable(){
            this.currentPage=1;
            this.init()
        },
        init(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9008/userPanManage/getUserPanList',
                type: 'post',
                contentType: "application/json;charset=utf-8",
                async: false,
                data:JSON.stringify({pd:{ title:_that.search},currentPage:_that.currentPage,showCount:_that.pageSize}),
                headers: {
                    "token":  _that.getCookie("token")
                },
                dataType: 'json',
                success: function(data) {
                    _that.$set(_that,"tableData",data.data.content)
                    _that.tableDataTotal=data.data.pageCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getAddMessage(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9001/examineAndApproveResource/getPageByUserId',
                type: 'post',
                async: false,
                data: {currentUserId:userId},
                headers: {
                    "token":  _that.getCookie("token")
                },
                success: function(data) {
                    _that.softwareTypes=data.data.appTypeList
                    _that.name=data.data.realName
                    _that.getSoftWareList(data.data.appTypeList)
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getOrgTreeAndName(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9001/examineAndApproveResource/orgTreeAndName',
                type: 'post',
                async: false,
                data: {currentUserId:userId},
                headers: {
                    "token":  _that.getCookie("token")
                },
                success: function(data) {
                    _that.otherUnitsTree=data.data.orgTree
                    _that.ownBMID=data.data.orgId
                    _that.dadnode(data.data.rankTree)
                    _that.ownTree=data.data.rankTree
                    _that.allPeople=data.data.orgUserCount
                    _that.branchName=data.data.orgName
                    _that.branchID=data.data.orgId
                    _that.branchNum=data.data.orgUserCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        dateFtt(fmt,date) { //author: meizz
            var o = {
                "M+" : date.getMonth()+1,     //月份
                "d+" : date.getDate(),     //日
                "h+" : date.getHours(),     //小时
                "m+" : date.getMinutes(),     //分
                "s+" : date.getSeconds(),     //秒
                "q+" : Math.floor((date.getMonth()+3)/3), //季度
                "S" : date.getMilliseconds()    //毫秒
            };
            if(/(y+)/.test(fmt))
                fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
            for(var k in o)
                if(new RegExp("("+ k +")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
            return fmt;
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        getOwnTable(parma){
            this.user=parma
            this.deatilVisible=true
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9008/userPanManage/getUserPan',
                type: 'post',
                async: false,
                data:{userId:parma.row.userId},
                headers: {
                    "token":  _that.getCookie("token")
                },
                success: function(data) {
                    _that.$set(_that,"detailTableData",data.data.content)
                    // _that.tableDataTotal=data.data.pageCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        open(parma){
            this.dialogVisible=true
            let _that=this
            _that.editID=parma
            _that.$set(_that.form,'name',this.user.row.userName)
            _that.$set(_that.form,'space',parma.row.spaceSize)
        },
        changEdit(){
            let _that=this
          if(this.form.space=='' || JSON.stringify(this.form.space).replace(/\s+/g, "")=='' || this.form.space%1!==0 || parseInt(this.form.space)<=0){
              layer.msg("空间大小只能为大于0的整数", {
                  icon: 2,
                  closeBtn:1
              });
              this.form.space=''
              return
          }
            $.ajax({
                url: url+':9008/userPanManage/editUserPanSpaceSize',
                type: 'post',
                async: false,
                contentType: "application/json;charset=utf-8",
                dataType: 'json',
                data: JSON.stringify({id:this.editID.row.userPanId,spaceSize:this.form.space}),
                headers: {
                    "token":  _that.getCookie("token")
                },
                success: function(data) {
                    _that.currentPage=1
                    _that.getOwnTable(_that.user);
                    _that.init()
                    if(data.code==200){
                        console.log(2222)
                        _that.$message({
                            message:data.message,
                            type: 'success'
                        });
                    }else{
                        _that.$message({
                            message:data.message,
                            type: 'error'
                        });
                    }
                    _that.dialogVisible=false
                },
                error: function(data) {
                    _that.$message({
                        message:data.message,
                        type: 'error'
                    });
                }
            })
        },
        handleSizeChange(val) {
            this.currentPage=1
            this.pageSize=val
            this.init();
        },
        handleCurrentChange(val) {
            this.currentPage=val
            this.init();
        }
    },
    mounted() {
        this.init();
    },
})
