
(function () {
  layui.use(['element', 'table', 'layer', 'jquery', 'form', 'tree', 'laypage'], function () {
    var element = layui.element;
    var layer = layui.layer;
    var table = layui.table;
    var $ = layui.jquery;
    var form = layui.form;
    var tree = layui.tree;
    var laypage = layui.laypage;
    var time1 = ''
    var time2 = ''
    var time3 = ''

    var userId = getCookie("userId"); // 获取用户id

    $.ajax({ // 我的连接 表格数据
      type: "POST",
      url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
      // url: 'http://10.1.1.213:9006/AppConnection/appConnectionPageForUser/10/0',
      data: JSON.stringify({
        "userId": userId
      }), // id的参数obj
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      dataType: "json",
      success: function (data) {
        console.log(data)
        table.render({
          elem: "#tableData",
          data: data.data.content,
          limit: 10,
             width:1675,
          page: { layout: ['prev', 'page', 'next', 'skip'] },
          id: 'idTest',
          cols: [
            [{
              checkbox: true,
              fixed: true
            },
            {
              field: 'appName',
              title: '软件',
              sort: true,
              event: 'hover',
              'unresize': true
            },
            {
              field: 'equipmentName',
              title: '服务器',
              sort: true,
              'unresize': true
            },
            {
              field: 'ip',
              title: 'IP',
              sort: true,
              'unresize': true
            },
            {
              field: 'openTime',
              title: '打开时间',
              sort: true,
              'unresize': true
            },
            {
              field: 'openMode',
              title: '打开方式',
              sort: true,
              'unresize': true
            },
            {
              field: 'leaveTime',
              title: '闲置时间',
              sort: true,
              templet: function (d) {
                return d.leaveTime + '小时'
              },
              'unresize': true
            },
            {
              field: 'operation',
              title: '操作',
              
              // event: 'click',
              toolbar: '#conntClick',
              'unresize': true
            }
            ]
          ]
        });
      }
    });
    const GETAPPLICATION = url + ':9001/application/getApplicationType'; //获取应用 - 书军9002
    const BYUSERID = url + ':9001/application/getApplicationListByUserId'; // 授权软件 - 书军
    const GETSYSTEMUSER = url + ':9001/appConnection/getSysUserListByUserId'; // 获取系统用户 - 书军
    const OPENSOFTWARE = url + ':9006/AppConnection/open'; // 打开软件 - 佳豪
    const DOWNLOAD = url + ':9006/AppConnection/downLoadVnc'; // 下载连接 - 佳豪


    var userId = getCookie("userId");
    var userName = getCookie("userName");
    var token = getCookie("token");
    var token1 = getCookie("token1");
    var token3 = getCookie("token3");
    var loginPageAccess = getCookie('loginPageAccess');
    if (token == '') {
      window.location.href = url + port + "dist/views";
    }


    $.ajax({ // 获取我的应用
      type: "POST",
      url: GETAPPLICATION,
      // data:{typeKey:'app_type'},
      data: {
        "userId": userId
      },
      success: function (data) {
        // console.log(data.data)
        var res = data.data,
          inputs;
        for (var i = 0; i < res.length; i++) { // 循环 动态创建单选input
          // console.log(res[i])
          inputs = $("<input type='radio'lay-filter='acId' value=" + res[i].typeKey + " name='radio' title=" + res[i].value + " >");
          $(".input-block").append(inputs)
        }
        $(".input-block").prepend("<input type='radio'lay-filter='acId' value='all' name='radio' title='显示全部' checked>")
        form.render('radio')
      }
    })
    $.ajax({ // 默认先显示所有软件
      type: "POST",
      url: BYUSERID,
      data: {
        userId: userId
      }, // useId根据平台用户获取,
      success: function (data) {
        console.log(data)
        // console.log(data.data.length)
        var data = data.data;
        var lis = '';
        for (var i = 0; i < data.length; i++) {
          lis += `<li class="layui-form-item" title="${data[i].hardwareName}">
                      <div class="software-show">
                          <div class="software-show-left">
                              <img width="50" height="50" src="./../../../../${data[i].path}" />
                              <select id="screen" lay-filter="screen">
                                  <option value=""></option>
                                  <option value="1" selected="">1屏幕</option>
                                  <option value="2">2屏幕</option>
                                  <option value="3">3屏幕</option>
                                  <option value="4">4屏幕</option>
                              </select>
                          </div>
                          <div class="software-show-right">
                              <div class="changryu" style="display:${data[i].checkLink == true ? 'block' : 'none'}">
                                <div class="layui-unselect layui-form-radio layui-form-radioed onwtt${i} accEvn" daAccEv="${i}" name="down"><i class="layui-anim layui-icon"></i><div>客户端</div></div>
                                <div class="layui-unselect layui-form-radio onwtr${i} accEvn" daAccEv="${i}"  name="page"><i class="layui-anim layui-icon"></i><div>网页</div></div>
                              </div>
                              <h3 style="margin-top:${data[i].checkLink == true ? '12px' : '40px'}" title="${data[i].softName} ${data[i].versionName}">${data[i].softName} ${data[i].versionName}</h3>
                              <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${data[i].systemId}" data-appId="${data[i].id}" data-isTimeout="${data[i].isTimeout}" openIndex='${i}'>打开软件</button>
                          </div>
                      </div>
                  </li>`;
          // lis=noSelect(lis,data[i].hardwareName,data[i].path,i,data[i].softName,data[i].versionName,data[i].systemId,data[i].id,data[i].isTimeout)
        }
        $(".software-box").append(lis)
        // form.render('radio')
        
        
        $(".software-box").children("li").width(208)
        if(data.length<6){
          $(".software-box").width(248*data.length)
        }else{
          $(".software-box").width(248*6)
        }

        form.render('select')
        $('.accEvn').on('click', function (e) {
          if (this.getAttribute('name') == 'page') {
            $('.onwtr' + this.getAttribute('daAccEv')).addClass('layui-form-radioed')
            $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).addClass('layui-anim-scaleSpring')
            $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).html('')
            $('.onwtt' + this.getAttribute('daAccEv')).removeClass('layui-form-radioed')
            $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).removeClass('layui-anim-scaleSpring')
            $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).html('')
          } else {
            $('.onwtt' + this.getAttribute('daAccEv')).addClass('layui-form-radioed')
            $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).addClass('layui-anim-scaleSpring')
            $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).html('')
            $('.onwtr' + this.getAttribute('daAccEv')).removeClass('layui-form-radioed')
            $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).removeClass('layui-anim-scaleSpring')
            $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).html('')
          }

        })
      }
    });

    var searchInfo = ''

    function noSelect(lis, hardwareName, path, i, softName, versionName, systemId, id, isTimeout) {
      lis += `<li class="layui-form-item" title="${hardwareName}">
                          <div class="software-show">
                              <div class="software-show-left">
                                  <img width="50" height="50" src="./../../../../${path}" />
                                  <select id="screen" lay-filter="screen">
                                      <option value=""></option>
                                      <option value="1" selected="">1屏幕</option>
                                      <option value="2">2屏幕</option>
                                      <option value="3">3屏幕</option>
                                      <option value="4">4屏幕</option>
                                  </select>
                              </div>
                              <div class="software-show-right">
                                  <div class="changryu"  style="display:${data[i].checkLink == true ? 'block' : 'none'}">
                                      <div class="layui-unselect layui-form-radio layui-form-radioed onwtt${i} accEvn" daAccEv="${i}" name="down"><i class="layui-anim layui-icon"></i><div>客户端</div></div>
                                      <div class="layui-unselect layui-form-radio onwtr${i} accEvn" daAccEv="${i}"  name="page"><i class="layui-anim layui-icon"></i><div>网页</div></div>
                                  </div>
                                  <h3 style="margin-top:${data[i].checkLink == true ? '12px' : '40px'}" title="${data[i].softName} ${data[i].versionName}">${softName} ${versionName}</h3>
                                  <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${systemId}" data-appId="${id}" data-isTimeout="${isTimeout}" openIndex='${i}'>打开软件</button>
                              </div>
                          </div>
                      </li>`;
      return lis
    }
    form.on('radio(acId)', function (data) { // 根据单选按钮的选择显示软件列表
      console.log(data)
      var acId = data.value
      if (acId == 'all') {
        $(".software-box").html('')
        $.ajax({ // 默认先显示所有软件
          type: "POST",
          url: BYUSERID,
          data: {
            userId: userId
          }, // useId根据平台用户获取,
          success: function (data) {
            console.log(data)
            // console.log(data.data.length)
            var data = data.data;
            var lis = '';
            // console.log( $(".software-box") )
            for (var i = 0; i < data.length; i++) {
              // console.log(data[i])
              lis += `<li class="layui-form-item" title="${data[i].hardwareName}">
                          <div class="software-show">
                              <div class="software-show-left">
                                  <img width="50" height="50" src="./../../../../${data[i].path}" />
                                  <select id="screen" lay-filter="screen">
                                      <option value=""></option>
                                      <option value="1" selected="">1屏幕</option>
                                      <option value="2">2屏幕</option>
                                      <option value="3">3屏幕</option>
                                      <option value="4">4屏幕</option>
                                  </select>
                              </div>
                              <div class="software-show-right">
                                  <div class="changryu" style="display:${data[i].checkLink == true ? 'block' : 'none'}">
                                      <div class="layui-unselect layui-form-radio layui-form-radioed onwtt${i} accEvn" daAccEv="${i}" name="down"><i class="layui-anim layui-icon"></i><div>客户端</div></div>
                                      <div class="layui-unselect layui-form-radio onwtr${i} accEvn" daAccEv="${i}"  name="page"><i class="layui-anim layui-icon"></i><div>网页</div></div>
                                  </div>
                                  <h3 style="margin-top:${data[i].checkLink == true ? '12px' : '40px'}" title="${data[i].softName} ${data[i].versionName}">${data[i].softName} ${data[i].versionName}</h3>
                                  <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${data[i].systemId}" data-appId="${data[i].id}" data-isTimeout="${data[i].isTimeout}" openIndex='${i}'>打开软件</button>
                              </div>
                          </div>
                      </li>`;
            }
            $(".software-box").append(lis)

            if(data.length<6){
              $(".software-box").width(248*data.length)
            }else{
              $(".software-box").width(248*6)
            }
            $(".software-box").children("li").width(208)

            form.render('select')
            $('.accEvn').on('click', function (e) {
              if (this.getAttribute('name') == 'page') {
                $('.onwtr' + this.getAttribute('daAccEv')).addClass('layui-form-radioed')
                $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).addClass('layui-anim-scaleSpring')
                $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).html('')
                $('.onwtt' + this.getAttribute('daAccEv')).removeClass('layui-form-radioed')
                $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).removeClass('layui-anim-scaleSpring')
                $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).html('')
              } else {
                $('.onwtt' + this.getAttribute('daAccEv')).addClass('layui-form-radioed')
                $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).addClass('layui-anim-scaleSpring')
                $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).html('')
                $('.onwtr' + this.getAttribute('daAccEv')).removeClass('layui-form-radioed')
                $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).removeClass('layui-anim-scaleSpring')
                $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).html('')
              }

            })
          }
        });
      }
      $(".software-box").html('')
      $.ajax({
        type: "POST",
        url: BYUSERID,
        data: {
          userId: userId,
          acId: acId
        }, // useId根据平台用户获取,
        success: function (data) {
          console.log(data)
          // console.log(data.data.length)
          var data = data.data;
          var lis = '';
          // console.log( $(".software-box") )
          for (var i = 0; i < data.length; i++) {
            // console.log(data[i])
            lis += `<li class="layui-form-item" title="${data[i].hardwareName}">
                          <div class="software-show">
                              <div class="software-show-left">
                                <img width="50" height="50" src="./../../../../${data[i].path}" />
                                  <select id="screen" lay-filter="screen">
                                      <option value=""></option>
                                      <option value="1" selected="">1屏幕</option>
                                      <option value="2">2屏幕</option>
                                      <option value="3">3屏幕</option>
                                      <option value="4">4屏幕</option>
                                  </select>
                              </div>
                              <div class="software-show-right">
                                 <div class="changryu" style="display:${data[i].checkLink == true ? 'block' : 'none'}">
                                          <div class="layui-unselect layui-form-radio layui-form-radioed onwtt${i} accEvn" daAccEv="${i}" name="down"><i class="layui-anim layui-icon"></i><div>客户端</div></div>
                                          <div class="layui-unselect layui-form-radio onwtr${i} accEvn" daAccEv="${i}"  name="page"><i class="layui-anim layui-icon"></i><div>网页</div></div>
                                  </div>
                                  <h3 style="margin-top:${data[i].checkLink == true ? '12px' : '40px'}" title="${data[i].softName} ${data[i].versionName}">${data[i].softName} ${data[i].versionName}</h3>
                                  <button class="layui-btn layui-btn-normal layui-btn-xs search-btn" data-userId="4" data-systemId="${data[i].systemId}" data-appId="${data[i].id}" data-isTimeout="${data[i].isTimeout}" openIndex='${i}'>打开软件</button>
                              </div>
                          </div>
                      </li>`;
          }
          $(".software-box").append(lis)

          $(".software-box").children("li").width(208)

          if(data.length<6){
            $(".software-box").width(248*data.length)
          }else{
            $(".software-box").width(248*6)
          }
          

          form.render('select')
          $('.accEvn').on('click', function (e) {
            if (this.getAttribute('name') == 'page') {
              $('.onwtr' + this.getAttribute('daAccEv')).addClass('layui-form-radioed')
              $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).addClass('layui-anim-scaleSpring')
              $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).html('')
              $('.onwtt' + this.getAttribute('daAccEv')).removeClass('layui-form-radioed')
              $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).removeClass('layui-anim-scaleSpring')
              $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).html('')
            } else {
              $('.onwtt' + this.getAttribute('daAccEv')).addClass('layui-form-radioed')
              $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).addClass('layui-anim-scaleSpring')
              $($('.onwtt' + this.getAttribute('daAccEv')).children('i')).html('')
              $('.onwtr' + this.getAttribute('daAccEv')).removeClass('layui-form-radioed')
              $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).removeClass('layui-anim-scaleSpring')
              $($('.onwtr' + this.getAttribute('daAccEv')).children('i')).html('')
            }

          })

        }
      });
    })
    var screenValue = 1;
    var width = screen.width;
    var height = screen.height;
    var selects = width + "x" + height;
    form.on('select(screen)', function (data) { // 获取屏幕数量
      console.log(data);
      screenValue = data.value;
      selects = (width * screenValue) + "x" + height;
    });
    // 点击打开软件按钮
    $(".software-box").on('click', 'button', function (e) {
      if (time1 == '') {
        time1 = setTimeout(function () {
          clearTimeout(time1);
          console.log(111)
          time1 = ''
        }, 1000);
        var appId = this.getAttribute("data-appid");
        var systemId = this.getAttribute("data-systemId");
        var isTimeout = this.getAttribute("data-isTimeout");
        if (isTimeout == 1) {
          layer.msg('软件已过期!')
          return false
        }
        if ($('.onwtt' + this.getAttribute('openIndex')).hasClass('layui-form-radioed')) {
          $.ajax({ // 根据平台用户获取系统用户
            type: 'POST',
            url: GETSYSTEMUSER,
            data: {
              "userId": userId,
              "systemId": systemId
            }, // useId根据平台用户获取
            async: false,
            success: function (data) {
              console.log(data)
              var res = data.data;
              var htmlValue = '';
              var systemUserId = '';
              for (var i = 0; i < res.length; i++) {
                htmlValue += `<form class="layui-form">
                                <div class="layui-form-item">
                                  
                                  <div class="input-block">
                                    <input type="radio" name="radio" lay-filter='sysUserId' value="${res[i].sysUserId}" title="${res[i].name}">
                                  </div>
                            </div>`
              }
              form.on('radio(sysUserId)', function (data) { // 弹出框选择单选按钮
                console.log(data.value)
                systemUserId = data.value;
              })
              if (res.length === 0) {
                layer.msg('没有可用的系统用户')
                return false
              } else if (res.length === 1) {
                var systemUserId = res[0].sysUserId
                layer.load(0, { shade: [0.1] })

                $.ajax({ // 打开软件 功能
                  type: "POST",
                  url: OPENSOFTWARE,
                  // url:'http://10.1.1.213:9006/AppConnection/open',
                  data: JSON.stringify({
                    "userId": userId,
                    "appId": appId,
                    "systemUserId": systemUserId,
                    "screenCount": screenValue,
                    "resolution": selects,
                    'openMode': '客户端'
                  }), // 传用户id和软件id
                  contentType: "application/json;charset=utf-8",
                  dataType: "json",
                  success: function (data) { // 下载软件
                    console.log(data)
                    layer.close(layer.load(0, { shade: [0.1] }))
                    var data_ = data;
                    if (data.code === 200) {
                      var data = data.data;
                      var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
                      form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
                      form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
                      form.appendTo('body').submit().remove();

                      $.ajax({ // 表格数据
                        type: "POST",
                        // url: url + ":9006/AppConnection/appConnectionPage/10/0/appName/desc",
                        url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
                        data: JSON.stringify({
                          "userId": userId
                        }), // id的参数obj
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                          console.log(data)
                          table.render({
                            elem: "#tableData",
                            data: data.data.content,
                            limit: 10,
                               width:1675,
                            page: { layout: ['prev', 'page', 'next', 'skip'] },
                            id: 'idTest',
                            cols: [
                              [{
                                checkbox: true,
                                fixed: true
                              },
                              {
                                field: 'appName',
                                title: '软件',
                                sort: true,
                                event: 'hover',
                                'unresize': true
                              },
                              {
                                field: 'equipmentName',
                                title: '服务器',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'ip',
                                title: 'IP',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'openTime',
                                title: '打开时间',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'openMode',
                                title: '打开方式',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'leaveTime',
                                title: '闲置时间',
                                sort: true,
                                templet: function (d) {
                                  return d.leaveTime + '小时'
                                },
                                'unresize': true
                              },
                              {
                                field: 'operation',
                                title: '操作',
                                sort: true,
                                event: 'click',
                                toolbar: '#conntClick',
                                'unresize': true
                              }
                              ]
                            ]
                          });
                        }
                      });
                      layer.msg(data_.message)
                      // layer.close(loading);
                    } else {
                      layer.msg(data.message)
                      // layer.close(loading);
                    }
                  }
                });
                return false
              } else {
                layer.open({ // 弹出选择系统用户窗口
                  title: '系统用户',
                  content: htmlValue,
                  area: ['300px', '400px'],
                  yes: function (index) {
                    layer.close(index);
                    console.log(systemUserId) // 系统用户id
                    var loading = layer.load(0, {
                      shade: [0.8, '#393D49']
                    })
                    if (systemUserId != '') {
                      $.ajax({ // 打开软件 功能
                        type: "POST",
                        url: OPENSOFTWARE,
                        data: JSON.stringify({
                          "userId": userId,
                          "appId": appId,
                          "systemUserId": systemUserId,
                          "screenCount": screenValue,
                          "resolution": selects,
                          'openMode': '客户端'
                        }), // 传用户id和软件id
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (data) { // 下载软件
                          // console.log(JSON.stringify(data))
                          if (data.code === 200) {
                            var data = data.data;
                            var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
                            form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
                            form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
                            form.appendTo('body').submit().remove();

                            $.ajax({ // 表格数据
                              type: "POST",
                              url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
                              data: JSON.stringify({
                                "userId": userId
                              }), // id的参数obj
                              headers: {
                                "Content-Type": "application/json;charset=utf-8"
                              },
                              dataType: "json",
                              success: function (data) {
                                console.log(data)
                                table.render({
                                  elem: "#tableData",
                                  data: data.data.content,
                                  limit: 10,
                                     width:1675,
                                  page: { layout: ['prev', 'page', 'next', 'skip'] },
                                  id: 'idTest',
                                  cols: [
                                    [{
                                      checkbox: true,
                                      fixed: true
                                    },
                                    {
                                      field: 'appName',
                                      title: '软件',
                                      sort: true,
                                      event: 'hover',
                                      'unresize': true
                                    },
                                    {
                                      field: 'equipmentName',
                                      title: '服务器',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'ip',
                                      title: 'IP',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'openTime',
                                      title: '打开时间',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'openMode',
                                      title: '打开方式',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'leaveTime',
                                      title: '闲置时间',
                                      sort: true,
                                      templet: function (d) {
                                        return d.leaveTime + '小时'
                                      },
                                      'unresize': true
                                    },
                                    {
                                      field: 'operation',
                                      title: '操作',
                                      sort: true,
                                      event: 'click',
                                      toolbar: '#conntClick',
                                      'unresize': true
                                    }
                                    ]
                                  ]
                                });
                              }
                            });
                            layer.close(loading);
                          } else {
                            layer.msg(data.message)
                            layer.close(loading);
                          }
                        }
                      });
                    } else {
                      layer.msg('请选择一个用户')
                    }
                  }
                });
              }
              form.render('radio')
            }
          })
        } else {
          $.ajax({ // 根据平台用户获取系统用户
            type: 'POST',
            url: GETSYSTEMUSER,
            data: {
              "userId": userId,
              "systemId": systemId
            }, // useId根据平台用户获取
            async: false,
            success: function (data) {
              console.log(data)
              var res = data.data;
              var htmlValue = '';
              var systemUserId = '';
              for (var i = 0; i < res.length; i++) {
                htmlValue += `<form class="layui-form">
                                <div class="layui-form-item">
                                  
                                  <div class="input-block">
                                    <input type="radio" name="radio" lay-filter='sysUserId' value="${res[i].sysUserId}" title="${res[i].name}">
                                  </div>
                            </div>`
              }
              form.on('radio(sysUserId)', function (data) { // 弹出框选择单选按钮
                console.log(data.value)
                systemUserId = data.value;
              })
              if (res.length === 0) {
                layer.msg('没有可用的系统用户')
                return false
              } else if (res.length === 1) {
                var systemUserId = res[0].sysUserId
                layer.load(0, { shade: [0.1] })

                $.ajax({ // 打开软件 功能
                  type: "POST",
                  url: OPENSOFTWARE,
                  // url:'http://10.1.1.213:9006/AppConnection/open',
                  data: JSON.stringify({
                    "userId": userId,
                    "appId": appId,
                    "systemUserId": systemUserId,
                    "screenCount": screenValue,
                    "resolution": selects,
                    'openMode': '网页'
                  }), // 传用户id和软件id
                  contentType: "application/json;charset=utf-8",
                  dataType: "json",
                  success: function (data) { // 下载软件
                    console.log(data)
                    layer.close(layer.load(0, { shade: [0.1] }))
                    var data_ = data;
                    if (data.code === 200) {
                      var data = data.data;
                      window.open('https://' + data[4].substring(3, data[4].length) + ':8443/?authToken=' + data[3].substring(10, data[3].length) + '#' + data[2].substring(10, data[2].length))
                      $.ajax({ // 表格数据
                        type: "POST",
                        // url: url + ":9006/AppConnection/appConnectionPage/10/0/appName/desc",
                        url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
                        data: JSON.stringify({
                          "userId": userId
                        }), // id的参数obj
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                          console.log(data)
                          table.render({
                            elem: "#tableData",
                            data: data.data.content,
                               width:1675,
                            limit: 10,
                            page: { layout: ['prev', 'page', 'next', 'skip'] },
                            id: 'idTest',
                            cols: [
                              [{
                                checkbox: true,
                                fixed: true
                              },
                              {
                                field: 'appName',
                                title: '软件',
                                sort: true,
                                event: 'hover',
                                'unresize': true
                              },
                              {
                                field: 'equipmentName',
                                title: '服务器',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'ip',
                                title: 'IP',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'openTime',
                                title: '打开时间',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'openMode',
                                title: '打开方式',
                                sort: true,
                                'unresize': true
                              },
                              {
                                field: 'leaveTime',
                                title: '闲置时间',
                                sort: true,
                                templet: function (d) {
                                  return d.leaveTime + '小时'
                                },
                                'unresize': true
                              },
                              {
                                field: 'operation',
                                title: '操作',
                                sort: true,
                                event: 'click',
                                toolbar: '#conntClick',
                                'unresize': true
                              }
                              ]
                            ]
                          });
                        }
                      });
                      layer.msg(data_.message)
                      // layer.close(loading);
                    } else {
                      layer.msg(data.message)
                      // layer.close(loading);
                    }
                  }
                });
                return false
              } else {
                layer.open({ // 弹出选择系统用户窗口
                  title: '系统用户',
                  content: htmlValue,
                  area: ['300px', '400px'],
                  yes: function (index) {
                    layer.close(index);
                    console.log(systemUserId) // 系统用户id
                    var loading = layer.load(0, {
                      shade: [0.8, '#393D49']
                    })
                    if (systemUserId != '') {
                      $.ajax({ // 打开软件 功能
                        type: "POST",
                        url: OPENSOFTWARE,
                        data: JSON.stringify({
                          "userId": userId,
                          "appId": appId,
                          "systemUserId": systemUserId,
                          "screenCount": screenValue,
                          "resolution": selects,
                          'openMode': '网页'
                        }), // 传用户id和软件id
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (data) { // 下载软件
                          // console.log(JSON.stringify(data))
                          if (data.code === 200) {
                            var data = data.data;
                            window.open('https://' + data[4].substring(3, data[4].length) + ':8443/?authToken=' + data[3].substring(10, data[3].length) + '#' + data[2].substring(10, data[2].length))

                            $.ajax({ // 表格数据
                              type: "POST",
                              url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
                              data: JSON.stringify({
                                "userId": userId
                              }), // id的参数obj
                              headers: {
                                "Content-Type": "application/json;charset=utf-8"
                              },
                              dataType: "json",
                              success: function (data) {
                                console.log(data)
                                table.render({
                                  elem: "#tableData",
                                  data: data.data.content,
                                     width:1675,
                                  limit: 10,
                                  page: { layout: ['prev', 'page', 'next', 'skip'] },
                                  id: 'idTest',
                                  cols: [
                                    [{
                                      checkbox: true,
                                      fixed: true
                                    },
                                    {
                                      field: 'appName',
                                      title: '软件',
                                      sort: true,
                                      event: 'hover',
                                      'unresize': true
                                    },
                                    {
                                      field: 'equipmentName',
                                      title: '服务器',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'ip',
                                      title: 'IP',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'openTime',
                                      title: '打开时间',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'openMode',
                                      title: '打开方式',
                                      sort: true,
                                      'unresize': true
                                    },
                                    {
                                      field: 'leaveTime',
                                      title: '闲置时间',
                                      sort: true,
                                      templet: function (d) {
                                        return d.leaveTime + '小时'
                                      },
                                      'unresize': true
                                    },
                                    {
                                      field: 'operation',
                                      title: '操作',
                                      sort: true,
                                      event: 'click',
                                      toolbar: '#conntClick',
                                      'unresize': true
                                    }
                                    ]
                                  ]
                                });
                              }
                            });
                            layer.close(loading);
                          } else {
                            layer.msg(data.message)
                            layer.close(loading);
                          }
                        }
                      });
                    } else {
                      layer.msg('请选择一个用户')
                    }
                  }
                });
              }
              form.render('radio')
            }
          })
        }
      }
    })


    var fnTool = {
      init: function (params) {
        this.loop();

      },
      /* 个人详情 */
      openInfo: function () {
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['95%', '95%'],
          shade: 0.3,
          id: 'user_info', //设定一个id，防止重复弹出
          btn: ['确认'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#userInfo'),
          end: function () {
            $("#userInfo").hide();
          }
        });
      },
      /* 文字滚动 */
      loop: function (params) {
        var speed = 30;
        var msgSHow = document.getElementById("msg-box-show");
        var msgCopy = document.getElementById("msg-box-copy");
        var msgBar = document.getElementById("msg-bar");

        msgCopy.innerHTML = msgSHow.innerHTML;

        function Marquee(params) {
          if (msgCopy.offsetLeft - msgBar.scrollLeft <= 0) {
            msgBar.scrollLeft -= msgSHow.offsetWidth;
            // msgCopy.offsetLeft = 0
          } else {
            msgBar.scrollLeft++;
          }
        }
        var myLoop = setInterval(Marquee, speed);
        msgBar.onmouseenter = function () {
          clearInterval(myLoop)
        }
        msgBar.onmouseleave = function () {
          myLoop = setInterval(Marquee, speed)
        }
      },
      graphsPie: function (params) { // 软件监控图
        $.ajax({
          type: 'POST',
          url: url + ':9006/AppMonitorHistory/WeekOrMonth/30',
          data: JSON.stringify({
            'userId': userId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            if (data.data != '') {
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = []
              for (var key in data.data[0]) { // 将软件名push进数组
                software.push(key)
              }
              software = software[index]

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i])
                xName.push(datas[software][i]['X1'].date)
                userCount.push(datas[software][i]['X1'].count)
                if (datas[software][i]['X2'].mNames != '') { // 计算占满天数
                  fullDay++
                }
                if (datas[software][i]['X2'].isfull === 'yes') { // 计算是否占满
                  isFull.push(2)
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('')
                }
              }
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function (params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value'
                },
                series: [{
                  name: '许可占满',
                  type: 'bar',
                  color: ['#EEB422'],
                  stack: '总量',
                  data: isFull,
                  barMaxWidth: 80,
                },
                {
                  name: '使用人数',
                  type: 'line',
                  color: ['#66CD00'],
                  data: userCount
                },
                ]
              };
              var myChart = echarts.init(document.getElementById('graphs-left'));
              myChart.setOption(option);
            } else {
              layer.msg('暂无数据')
            }

          }
        })
        // var myChart = echarts.init(document.getElementById('graphs-left'));
        // myChart.setOption(option);
      },
      graphsLine: function (params) { // 线图
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/WorkTimeCountByTimeType/30',
          data: JSON.stringify({
            "userId": userId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            // console.log(data)
            var dates = [];
            var datas = [];
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              // console.log(data[i].timeCount)
              var arr = data[i].timeCount.split("  ");
              var arr1 = arr[1].split("小")
              // console.log(arr)
              dates.push(arr[0])
              datas.push(arr1[0])
              // console.log(arr1)
            }
            // console.log(dates,datas)
            option = {
              title: {
                text: '近30天登录平台时间(小时)',
                // subtext: '纯属虚构',
                x: 'center'
              },
              xAxis: {
                type: 'category',
                data: dates
              },
              tooltip: {
                trigger: 'axis'
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                data: datas,
                type: 'line'
              }]
            };

            var myChart = echarts.init(document.getElementById('graphs-right'));
            myChart.setOption(option);
          }
        })

      },
      tableInfoFn: function (params) {
        table.render({
          elem: '#tableInfo',
          // url: '/mock/table',
          // method: 'post',
          cols: [
            [{
              field: 'id',
              title: '用户',
              // width: 80,
              // sort: true,
              // event: 'hover',
              // style: 'cursor: pointer;'
            }, {
              field: 'username',
              title: '系统用户'
            }, {
              field: 'sex',
              title: '科室',
              // sort: true
            }, {
              field: 'city',
              title: 'IP'
            }, {
              field: 'sign',
              title: '软件'
            }]
          ],
          id: 'tableInfo',
          page: {
            count: 1000,
            layout: ['prev', 'page', 'next', 'skip'],
          }
        });
      }
    }
    fnTool.init();

    $("#userInfoLog").on("click", function (params) { // 点击个人信息
      fnTool.graphsPie(); // 饼图
      fnTool.graphsLine(); // 线图
      fnTool.openInfo(); // 弹窗

    })

    element.on('tab(tab1)', function (data) { //选项卡切换
      $(".tips-box").html('');
      if (data.index === 0) {
        $("#share-search-val").val("") // 切换回 我的连接时, 清除我的分享input里面的值
        $.ajax({ // 表格数据
          type: "POST",
          url: url + ":9006/AppConnection/appConnectionPageForUser/10/0/appName/desc",
          // url: "http://10.1.1.213:9006/AppConnection/appConnectionPage/10/0",
          data: JSON.stringify({
            "userId": userId
          }), // id的参数obj
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            table.render({
              elem: "#tableData",
              data: data.data.content,
              limit: 10,
                 width:1675,
              page: { layout: ['prev', 'page', 'next', 'skip'] },
              id: 'idTest',
              cols: [
                [{
                  checkbox: true,
                  fixed: true
                },
                {
                  field: 'appName',
                  title: '软件',
                  sort: true,
                  event: 'hover',
                  'unresize': true
                },
                {
                  field: 'equipmentName',
                  title: '服务器',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'ip',
                  title: 'IP',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'openTime',
                  title: '打开时间',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'openMode',
                  title: '打开方式',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'leaveTime',
                  title: '闲置时间',
                  sort: true,
                  templet: function (d) {
                    return d.leaveTime + '小时'
                  },
                  'unresize': true
                },
                {
                  field: 'operation',
                  title: '操作',
                  sort: true,
                  event: 'click',
                  toolbar: '#conntClick',
                  'unresize': true
                }
                ]
              ]
            });
          }
        });
      }
      if (data.index === 1) {
        $("#link-search-val").val("") // 切换回 我的分享时, 清除我的连接input里面的值
        active.getShareTable(userId)
      }
    });

    $("#link-search").click(function () { // 我的连接搜索按钮
      var searchValue = $("#link-search-val").val()
      $.ajax({ // 表格数据
        type: "POST",
        url: url + ":9006/AppConnection/appConnectionPageForUser/10/0/appName/desc",
        data: JSON.stringify({
          "userId": userId,
          "objValue": searchValue
        }), // id的参数obj
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          table.render({
            elem: "#tableData",
            data: data.data.content,
            limit: 10,
               width:1675,
            page: { layout: ['prev', 'page', 'next', 'skip'] },
            cols: [
              [{
                checkbox: true,
                fixed: true
              },
              {
                field: 'appName',
                title: '软件',
                sort: true,
                event: 'hover',
                'unresize': true
              },
              {
                field: 'equipmentName',
                title: '服务器',
                sort: true,
                'unresize': true
              },
              {
                field: 'ip',
                title: 'IP',
                sort: true,
                'unresize': true
              },
              {
                field: 'openTime',
                title: '打开时间',
                sort: true,
                'unresize': true
              },
              {
                field: 'openMode',
                title: '打开方式',
                sort: true,
                'unresize': true
              },
              {
                field: 'leaveTime',
                title: '闲置时间',
                sort: true,
                templet: function (d) {
                  return d.leaveTime + '小时'
                },
                'unresize': true
              },
              {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                toolbar: '#conntClick',
                'unresize': true
              }
              ]
            ]
          });
        }
      });
    })
    $("#link-search-val").on('keypress', function () { // 我的连接搜索按钮
      if (event.keyCode == "13") {
        var searchValue = $("#link-search-val").val()
        $.ajax({ // 表格数据
          type: "POST",
          url: url + ":9006/AppConnection/appConnectionPageForUser/10/0/appName/desc",
          data: JSON.stringify({
            "userId": userId,
            "objValue": searchValue
          }), // id的参数obj
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            table.render({
              elem: "#tableData",
              data: data.data.content,
              limit: 10,
                 width:1675,
              page: { layout: ['prev', 'page', 'next', 'skip'] },
              cols: [
                [{
                  checkbox: true,
                  fixed: true
                },
                {
                  field: 'appName',
                  title: '软件',
                  sort: true,
                  event: 'hover',
                  'unresize': true
                },
                {
                  field: 'equipmentName',
                  title: '服务器',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'ip',
                  title: 'IP',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'openTime',
                  title: '打开时间',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'openMode',
                  title: '打开方式',
                  sort: true,
                  'unresize': true
                },
                {
                  field: 'leaveTime',
                  title: '闲置时间',
                  sort: true,
                  templet: function (d) {
                    return d.leaveTime + '小时'
                  },
                  'unresize': true
                },
                {
                  field: 'operation',
                  title: '操作',
                  sort: true,
                  event: 'click',
                  toolbar: '#conntClick',
                  'unresize': true
                }
                ]
              ]
            });
          }
        });
      }
    })
    $("#share-search").click(function () { // 我的分享搜索按钮
      console.log(123)
      active.getShareTable(userId)
    })
    $("#share-search-val").on('keypress', function () { // 我的分享搜索按钮
      if (event.keyCode == "13") {
        active.getShareTable(userId)
      }
    })

    table.on('tool(tableData1)', function (obj) { // 我的分享点击操作列的按钮
      var connId = obj.data.connId;
      var shareId = getCookie("userId");
      var shareUserId = obj.data.shareUserId;
      if (obj.event === 'shareApp') {
        $.ajax({ // 获取左侧树状图
          type: 'GET',
          url: url + ":9006/AppConnection/departmentTree",
          success: function (data) {
            console.log(data)
            $("#treeList").html('')
            tree({
              elem: '#treeList',
              nodes: data.data,
              click: function (node) {
                console.log(node)
                orgId = node.id
                $("#shareBrand_sel").html("")
                var $select = $($(this)[0].elem).parents(".layui-form-select");
                $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='selectID']").val(node.id);
                $.ajax({
                  type: "POST",
                  url: url + ":9006/UserMonitorHistory/FindByOrgId",
                  data: JSON.stringify({
                    "orgId": orgId
                  }),
                  headers: {
                    "Content-Type": "application/json;charset=utf-8"
                  },
                  dataType: "json",
                  success: function (data) {
                    console.log(data)
                    var data = data.data;
                    var root = document.getElementById("shareBrand_sel");
                    console.log(root)
                    for (var i = 0; i < data.length; i++) {
                      var option = document.createElement("option");
                      option.setAttribute("dataid", data[i].id);
                      option.innerText = data[i].userName;
                      root.appendChild(option);
                      form.render("select");
                    }
                  }
                })
              }

            });
          }
        })
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['690px', '335px'],
          shade: 0.3,
          id: 'message1', //设定一个id，防止重复弹出
          btn: ['确定'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $("#shareAppDiaog"),
          yes: function (index) {
            layer.close(index);
            var optionsId = $("#shareChoose_sel option")
            var userIds = '';
            for (var i = 0; i < optionsId.length; i++) {
              userIds += "," + optionsId[i].getAttribute("dataid") // 将逗号拼接在前面
            }
            userIds = userIds.substring(1); // 截取 userIds 字符串最前面的逗号
            console.log(userIds)
            if (userIds == '') {
              layer.msg('请选择分享用户', {
                icon: 2,
                closeBtn: 1
              })
              return
            }
            $.ajax({ // 查询分享链接数据
              type: "POST",
              url: url + ':9006/AppShare/share',
              data: JSON.stringify({
                "userIds": userIds,
                "shareId": shareId,
                "appConnectionId": connId
              }),
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              success: function (data) {
                console.log(data)
                layer.msg(data.message)
                active.getShareTable(userId)
              }
            })
          },
          end: function () {
            $("#shareAppDiaog").css("display", "none");
            var ids = $("#shareChoose_sel option");
            var values = '';
            for (var i = 0; i < ids.length; i++) {
              values += ',' + ids[i].getAttribute("dataid")
            }
            $('#shareBrand_sel').html('')
            $('#shareChoose_sel').html('')
            values = values.substring(1);
            $(".addressee").val(values)
            console.log(values)
          }
        });
      }

      if (obj.event === 'openApp') {
        if (time2 == "") {
          time2 = setTimeout(function () {
            console.log(222)
            time2 = ''
          }, 1000)
          layer.load(0, { shade: [0.1] })
          $.ajax({
            type: "POST",
            url: url + ":9006/AppConnection/open",
            data: JSON.stringify({
              "connId": connId
            }), // 传id
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
              layer.close(layer.load(0, { shade: [0.1] }))
              var data = data.data;
              if (obj.data.openMode == '客户端') {
                var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
                form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
                form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
                form.appendTo('body').submit().remove();
              } else {
                window.open('https://' + data[4].substring(3, data[4].length) + ':8443/?authToken=' + data[3].substring(10, data[3].length) + '#' + data[2].substring(10, data[2].length))
              }
            }
          });
        }
      }

      if (obj.event === 'closeApp') {
        active.closeApp(obj.data.connId)

      }
      if (obj.event === 'emptyApp') {
        $.ajax({ // 查询分享链接数据
          type: "POST",
          url: url + ':9006/AppShare/clearShare',
          data: JSON.stringify({
            "shareId": shareId,
            "appConnectionId": connId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function (data) {
            layer.msg(data.message)
            active.getShareTable(userId)
          }
        })
      }
    });

    table.on('tool(tableData)', function (obj) { //我的连接 点击操作列的按钮
      var Id = obj.data.id;
      var shareUserId = obj.data.shareUserId;
      if (obj.event === 'openApp') {
        if (time3 == "") {
          time3 = setTimeout(function () {
            console.log(333)
            time3 = ''
          }, 1000)
          layer.load(0, { shade: [0.1] })
          $.ajax({
            type: "POST",
            url: url + ":9006/AppConnection/open",
            data: JSON.stringify({
              "connId": Id
            }), // 传id
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
              console.log(obj.data.openMode)
              console.log(data.data)
              layer.close(layer.load(0, { shade: [0.1] }))
              if (obj.data.openMode == '客户端') {
                var data = data.data;
                var form = $("<form></form>").attr("action", DOWNLOAD).attr("method", "post"); // 端口
                form.append($("<input></input>").attr("type", "hidden").attr("name", "fileName").attr("value", data[0]));
                form.append($("<input></input>").attr("type", "hidden").attr("name", "cont").attr("value", data[1]));
                form.appendTo('body').submit().remove();
              } else {
                var data = data.data;
                window.open('https://' + data[4].substring(3, data[4].length) + ':8443/?authToken=' + data[3].substring(10, data[3].length) + '#' + data[2].substring(10, data[2].length))
              }
            }
          });
        }
      }
      if (obj.event === 'closeApp') {
        var closeLoading = layer.load(0, {
          shade: [0.8, '#393D49']
        })
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          data: JSON.stringify({
            "ids": Id
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            layer.msg(data.message)
            $.ajax({ // 表格数据
              type: "POST",
              url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
              data: JSON.stringify({
                "userId": userId
              }), // id的参数obj
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              dataType: "json",
              success: function (data) {
                // console.log(data)
                table.render({
                  elem: "#tableData",
                  data: data.data.content,
                  limit: 10,
                     width:1675,
                  page: { layout: ['prev', 'page', 'next', 'skip'] },
                  cols: [
                    [{
                      checkbox: true,
                      fixed: true
                    },
                    {
                      field: 'appName',
                      title: '软件',
                      sort: true,
                      event: 'hover',
                      'unresize': true
                    },
                    {
                      field: 'equipmentName',
                      title: '服务器',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'ip',
                      title: 'IP',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'openTime',
                      title: '打开时间',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'openMode',
                      title: '打开方式',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'leaveTime',
                      title: '闲置时间',
                      sort: true,
                      templet: function (d) {
                        return d.leaveTime + '小时'
                      },
                      'unresize': true
                    },
                    {
                      field: 'operation',
                      title: '操作',
                      sort: true,
                      event: 'click',
                      toolbar: '#conntClick',
                      'unresize': true
                    }
                    ]
                  ]
                });
              }
            });
            layer.close(closeLoading);
          }
        });

      }
      if (obj.event === 'appInfo') {
        openDetail()
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/detail",
          data: JSON.stringify({
            "id": Id
          }), // id的参数obj
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            $(".userid").html('');
            $(".rankName").html('');
            $(".institutes").html('');
            $(".department").html('');
            $(".tel").html('');
            $(".phone").html('');
            $(".software").html('');
            $(".city").html('');
            $(".sysusername").html('')
            $(".server").html('')
            $(".port").html('')
            $(".opentime").html('')
            $(".leavetime").html('')
            if (data.code === 400) {
              layer.msg(data.message)
            } else {
              var data = data.data;
              $(".userid").html(data.userInfo.userName);
              $(".rankName").html(data.userInfo.rankName + "：");
              $(".institutes").html(data.userInfo.placesDepartment);
              $(".department").html(data.userInfo.department);
              $(".tel").html(data.userInfo.telephone);
              $(".phone").html(data.userInfo.phone);
              $(".software").html(data.userInfo.appName);
              $(".city").html(data.userInfo.ip);
              $(".sysusername").html(data.userInfo.sysUserName)
              $(".server").html(data.userInfo.equipmentName)
              $(".port").html(data.userInfo.port)
              $(".opentime").html(data.userInfo.openTime)
              $(".leavetime").html(data.userInfo.leaveTime)
              table.render({
                elem: "#detailTable",
                data: data.appModuleInfo,
                id: 'detailTable',
                limit: 10,
                cols: [
                  [{
                    checkbox: true
                  },
                  {
                    field: 'moduleName',
                    title: '模块',
                    unresize: true,
                    'unresize': true
                  },
                  {
                    field: 'handle',
                    title: 'Handle No.',
                    unresize: true,
                    'unresize': true
                  },
                  {
                    field: 'appOpentime',
                    title: '打开时间',
                    unresize: true,
                    'unresize': true,

                  },
                  {
                    field: 'openMode',
                    title: '打开方式',
                    sort: true,
                    'unresize': true
                  },
                  {
                    field: 'operation',
                    title: '操作',
                    unresize: true,
                    templet: "#releaseBtn",
                    event: 'click',
                    'unresize': true,
                    templet: function (d) {
                      return '<span class="layui-btn layui-btn-xs release" lay-event="release">释放</span>'
                    }
                  }
                  ]
                ],
                done: function (res, curr, count) { }
              });
            }
          }
        });
      }
    });

    //移到右边
    $(document).on('click', '.shareAdds', function () {
      var flag
      if (!$('#shareBrand_sel option').is(':selected')) {
        layer.alert('请选择移动的选项')
      } else if ($('#shareChoose_sel option').length == 0) {
        $('#shareBrand_sel option:selected').appendTo('#shareChoose_sel')
      } else {
        var arr = Array.prototype.slice.call($('#shareChoose_sel option'))
        for (var i = 0; i < arr.length; i++) {
          if ($('#shareBrand_sel option:selected').text() == arr[i].text) {
            flag = false
            layer.alert('不可以选择重复的选项!')
            return false
          } else {
            flag = true
          }
        }
        if (flag) {
          $('#shareBrand_sel option:selected').appendTo('#shareChoose_sel')
        }
      }
    });
    //移到左边
    $(document).on('click', '.shareRemoves', function () {
      if (!$("#shareChoose_sel option").is(":selected")) {
        layer.alert("请选择移动的选项")
      } else {
        $('#shareChoose_sel option:selected').appendTo('#shareBrand_sel');
      }
    });
    //全部移到右边
    $(document).on('click', '.shareAdd_alls', function () {
      $('#shareBrand_sel option').appendTo('#shareChoose_sel');
    });
    //全部移到左边
    $(document).on('click', '.shareRemove_alls', function () {
      $('#shareChoose_sel option').appendTo('#shareBrand_sel');
    });

    var active = {
      init: function () {
        this.loginPrivilege();
      },
      closeApp: function (Id) {
        var closeLoading = layer.load(0, {
          shade: [0.8, '#393D49']
        })
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          data: JSON.stringify({
            "ids": Id
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            layer.msg(data.message)
            active.getShareTable(userId)
            layer.close(closeLoading);
          }
        });
      },
      //封装一个函数 获取cookie
      getCookie: function (name) {
        var cookie = document.cookie;
        var arr = cookie.split("; "); //将字符串分割成数组
        for (var i = 0; i < arr.length; i++) {
          var arr1 = arr[i].split("=");
          if (arr1[0] == name) {
            return unescape(arr1[1]);
          }
        }
        return "GG"
      },
      //获取权限
      loginPrivilege: function () {
        var userId;
        var loginPageAccess = active.getCookie('loginPageAccess');
        if (loginPageAccess == 2) {
          userId = active.getCookie("userId2");
        } else if (loginPageAccess == 3) {
          userId = active.getCookie("userId3");
        }
        if (userId == "GG") {
          window.location.href = url + port + "/dist/views";
          layer.msg('请登录用户！', {
            icon: 2,
            closeBtn: 1
          })
        } else {
          $.ajax({
            url: url + ":9000/login/loginPrivilege",
            type: "post",
            async: true,
            data: {
              userId
            },
            success: function (data) {
              console.log(JSON.stringify(data));
              if (data.code == 200) {
                $('#userName').html(active.getCookie("userName"));
                if (data.data.length > 0) {
                  JSON1.list = data.data[0].children;
                  var json = JSON1;
                  var getTpl = menuTpl.innerHTML,
                    view = document.getElementById('menuBox');
                  laytpl(getTpl).render(json, function (html) {
                    view.innerHTML = html;
                  });
                  element.init();
                  $("#menuBox .layui-nav-child dd").on("click", function (params) {
                    sessionStorage.setItem("navItem", $(this).data("link"));
                    console.log(sessionStorage.getItem("navItem"))
                  })
                  $("dd.layui-this").parents(".layui-nav-item").addClass("layui-nav-itemed");
                  $("#menuBox .layui-nav-item > a").on("click", function (params) {
                    sessionStorage.setItem("navItem", $(this).data("link"));
                  })
                }
              } else {
                layer.msg(data.message, {
                  icon: 2,
                  closeBtn: 1
                })
              }
            },
            error: function (data) {
              console.log(data)
            }
          })
        }
      },
      //注销
      logout: function (userId, token) {
        $.ajax({
          url: url + ":9000/login/logout",
          type: "post",
          async: true,
          data: {
            userId,
            token
          },
          success: function (data) {
            console.log(JSON.stringify(data));
            if (data.code == 200 && data.data == 1) {
              window.location.href = url + port + "/dist/views";
            } else {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function (data) {
            console.log(data)
          }
        })
      },
      //修改密码弹框
      updataPassword: function () {
        layer.open({
          type: 1,
          title: "修改密码", //不显示标题栏
          closeBtn: 1,
          area: ['360px', '260px'],
          shade: 0.8,
          id: 'LAY_layuipro',
          btnAlign: 'c',
          moveType: 1,
          content: $('.updata-password'),
          success: function (layero) {
          },
          end: function (index, layero) {
            active.clearUpdata();
            $(".updata-password").hide()
          }
        });
      },
      //清空修改密码弹框数据
      clearUpdata: function () {
        $('.updata-password input[name=oldPassword]').val('');
        $('.updata-password input[name=newPassword]').val('');
        $('.updata-password input[name=verifyPassword]').val('');
      },
      //修改密码提交
      updataSubmit: function (data) {
        $.ajax({
          url: url + ":9000/login/userUpdatePassword",
          type: "post",
          async: true,
          data,
          success: function (data) {
            if (data.code == 200 && data.data == 1) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
              window.location.href = url + port + "/dist/views";
            } else {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function (data) {
            console.log(data)
          }
        })
      },
      getShareTable: function (userId) {
        var appName = $("#share-search-val").val()
        $.ajax({ // 分享分页
          type: "POST",
          url: url + ':9006/AppShare/pageInfo/10/0/datetime/desc',
          data: JSON.stringify({
            shareId: userId,
            appName: appName
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function (data) {
            console.log(data)
            table.render({ // 我的分享
              elem: '#tableData1',
              data: data.data.content,
                 width:1675,
              id: 'shareTable',
              page: {
                layout: ['prev', 'page', 'next', 'skip']
              },
              // method: 'post',
              cols: [
                [{
                  checkbox: true,
                  fixed: true
                }, {
                  field: 'datetime',
                  title: '时间',
                  sort: true,
                  'unresize': true
                }, {
                  field: 'app_name',
                  title: '软件',
                  sort: true,
                  'unresize': true
                }, {
                  field: 'openMode',
                  title: '打开方式',
                  sort: true,
                  'unresize': true
                }, {
                  field: 'type',
                  title: '状态',
                  sort: true,
                  templet: function (d) {
                    console.log(userName)
                    if (d.status == 1) {
                      return '无分享'
                    } else if (d.status == 2) {
                      return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                    } else if (d.status == 3) {
                      return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                    }
                  },
                  'unresize': true
                }, {
                  field: 'userName',
                  title: '用户',
                  sort: true,
                  templet: function (d) {
                    return d.realName
                  },
                  'unresize': true
                }, {
                  field: 'operation',
                  title: '操作',
                  sort: true,
                  event: 'click',
                  width: 280,
                  templet: function (d) {
                    if (d.shareUserId === userId) {
                      return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                    } else {
                      return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                    }
                  },
                  'unresize': true
                }]
              ]
            });
          }
        })
      }
    }

    //自定义验证规则
    form.verify({
      oldPass: [/(.+){6,12}$/, '密码必须6到12位'],
      newPass1: [/(.+){6,12}$/, '密码必须6到12位'],
      newPass2: [/(.+){6,12}$/, '密码必须6到12位'],
      newPass1: function (value, item) {
        if (value != $('input[name=verifyPassword]').val()) {
          return '两次密码不一致'
        }
      }
    });

    //监听修改密码提交
    form.on('submit(newUpdataPassword)', function (data) {
      data.field.userId = active.getCookie("userId");
      active.updataSubmit(data.field);
      return false
    });

    //注销
    $('body .signOut').on('click', function () {
      var userID;
      var loginPageAccess = active.getCookie('loginPageAccess');
      userID = active.getCookie("userId");
      active.logout(userID, active.getCookie('token3'));
      window.location.href = url + port + "/dist/views";
    });
    //修改密码
    $('#updataUserPassword').on('click', function () {
      active.updataPassword()
    });
    $('#userName').html(active.getCookie("realName")); // 显示页面用户名

    $.ajax({ // 收消息
      type: "POST",
      url: url + ':9006/Message/putMessage/' + userId,
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      success: function (data) {
        console.log(data)
        $(".layui-badge").html(data.data.length)
        if (data.code === 200) {
          var data = data.data;
          var messageInfo = '';
          for (var i = 0; i < data.length; i++) {
            messageInfo = `<div>
                                  ${data[0].datetime} 通知：${data[0].note}
                                </div>`
          }
          $("#msg-box-show").append(messageInfo)
        } else {
          console.log(data.message)
        }
      }
    })
    function setCookie(name, value, num) { // 设置cookie
      var val = escape(value); //对value进行编码
      var date = new Date();
      date.setDate(date.getDate() + num);
      document.cookie = `${name}=${val};expires=${date}`;
    }

    function removeCookie(name) { // 删除cookie
      setCookie(name, 123, -1);
    }

    function getCookie(name) { // 获取cookie
      var strcookie = document.cookie; //获取cookie字符串
      var arrcookie = strcookie.split("; "); //分割
      //遍历匹配
      for (var i = 0; i < arrcookie.length; i++) {
        var arr = arrcookie[i].split("=");
        if (arr[0] == name) {
          return arr[1];
        }
      }
      return "";
    }

    function openDetail() { // 打开详情页
      layer.open({
        type: 1,
        title: "详情", //不显示标题栏
        closeBtn: 1,
        area: ["95%", "520px"],
        shade: 0.3,
        id: "detail_layui", //设定一个id，防止重复弹出
        btn: ["确定"],
        btnAlign: "c",
        moveType: 1, //拖拽模式，0或者1
        content: $("#detailHtml"),
        end: function () {
          $("#detailHtml").hide();
        }
      });
    }

    var closeIds = '';
    table.on("checkbox(required)", function (obj) { // 选择我的连接页面多选按钮
      console.log(obj)
    });
    // table.on('checkbox(tableData)', function(data) {
    //   console.log(data)
    // })

    $("#link-close").click(function () { // 选择我的连接页面多选按钮
      var checkStatus = table.checkStatus('idTest'),
        data = checkStatus.data;
      for (var i = 0; i < data.length; i++) {
        closeIds += `,${data[i].id}`;
      }
      closeIds = closeIds.substring(1); // 去掉id串前面的逗号
      console.log(closeIds)
      if (closeIds != '') {
        var batchClose = layer.load(0, {
          shade: [0.8, '#393D49']
        })
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          data: JSON.stringify({
            "ids": closeIds
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            $.ajax({ // 我的连接 表格数据
              type: "POST",
              url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/appName/desc',
              data: JSON.stringify({
                "userId": userId
              }), // id的参数obj
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              dataType: "json",
              success: function (data) {
                console.log(data)
                table.render({
                  elem: "#tableData",
                  data: data.data.content,
                  limit: 10,
                     width:1675,
                  page: { layout: ['prev', 'page', 'next', 'skip'] },
                  id: 'idTest',
                  cols: [
                    [{
                      checkbox: true,
                      fixed: true
                    },
                    {
                      field: 'appName',
                      title: '软件',
                      sort: true,
                      event: 'hover',
                      'unresize': true
                    },
                    {
                      field: 'equipmentName',
                      title: '服务器',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'ip',
                      title: 'IP',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'openTime',
                      title: '打开时间',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'openMode',
                      title: '打开方式',
                      sort: true,
                      'unresize': true
                    },
                    {
                      field: 'leaveTime',
                      title: '闲置时间',
                      sort: true,
                      templet: function (d) {
                        return d.leaveTime + '小时'
                      },
                      'unresize': true
                    },
                    {
                      field: 'operation',
                      title: '操作',
                      sort: true,
                      event: 'click',
                      toolbar: '#conntClick',
                      'unresize': true
                    }
                    ]
                  ]
                });
              }
            });
            layer.close(batchClose); // 关闭加载层
          }
        });
        closeIds = '';
      } else {
        layer.msg('请选择要关闭的连接!');
        return false
      }
    })

    table.on('sort(tableData)', function (obj) { // 我的连接表格 排序
      console.log(obj.field);
      console.log(obj.type)
      $.ajax({ // 我的连接 表格数据
        type: "POST",
        url: url + ':9006/AppConnection/appConnectionPageForUser/10/0/' + obj.field + "/" + obj.type,
        data: JSON.stringify({
          "userId": userId
        }), // id的参数obj
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          table.render({
            elem: "#tableData",
            data: data.data.content,
            limit: 10,
               width:1675,
            page: { layout: ['prev', 'page', 'next', 'skip'] },
            id: 'idTest',
            cols: [
              [{
                checkbox: true,
                fixed: true
              },
              {
                field: 'appName',
                title: '软件',
                sort: true,
                event: 'hover',
                'unresize': true
              },
              {
                field: 'equipmentName',
                title: '服务器',
                sort: true,
                'unresize': true
              },
              {
                field: 'ip',
                title: 'IP',
                sort: true,
                'unresize': true
              },
              {
                field: 'openTime',
                title: '打开时间',
                sort: true,
                'unresize': true
              },
              {
                field: 'openMode',
                title: '打开方式',
                sort: true,
                'unresize': true
              },
              {
                field: 'leaveTime',
                title: '闲置时间',
                sort: true,
                templet: function (d) {
                  return d.leaveTime + '小时'
                },
                'unresize': true
              },
              {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                toolbar: '#conntClick',
                'unresize': true
              }
              ]
            ]
          });
        }
      });
    });

    table.on('sort(tableData1)', function (obj) { // 我的分享表格 排序
      console.log(obj.field);
      console.log(obj.type)
      $.ajax({ // 分享分页
        type: "POST",
        url: url + ':9006/AppShare/pageInfo/10/0/' + obj.field + "/" + obj.type,
        data: JSON.stringify({
          "shareId": userId
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          console.log(data)
          table.render({ // 我的分享
            elem: '#tableData1',
            data: data.data.content,
               width:1675,
            id: 'shareTable',
            // method: 'post',
            limit: 10,
            page: {
              layout: ['prev', 'page', 'next', 'skip']
            },
            cols: [
              [{
                checkbox: true,
                fixed: true
              }, {
                field: 'datetime',
                title: '时间',
                sort: true,
                'unresize': true
              }, {
                field: 'app_name',
                title: '软件',
                sort: true,
                'unresize': true
              }, {
                field: 'openMode',
                title: '打开方式',
                sort: true,
                'unresize': true
              }, {
                field: 'type',
                title: '状态',
                sort: true,
                templet: function (d) {
                  console.log(userName)
                  if (d.status == 1) {
                    return '无分享'
                  } else if (d.status == 2) {
                    return '<i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i><i class="layui-icon">&#xe602;</i>'
                  } else if (d.status == 3) {
                    return '<i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i><i class="layui-icon">&#xe603;</i>'
                  }
                },
                'unresize': true
              }, {
                field: 'userName',
                title: '用户',
                sort: true,
                templet: function (d) {
                  // console.log(d)
                  if (d.shareUserId === userId) {
                    return d.userName || '未分享'
                  } else {
                    return d.shareUserName
                  }
                },
                'unresize': true
              }, {
                field: 'operation',
                title: '操作',
                sort: true,
                event: 'click',
                width: 280,
                templet: function (d) {
                  if (d.shareUserId === userId) {
                    return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span><span class="layui-btn layui-btn-xs shareApp" lay-event="shareApp">分享</span><span class="layui-btn layui-btn-xs empty" lay-event="emptyApp">清空分享</span>'
                  } else {
                    return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span>'
                  }
                },
                'unresize': true
              }]
            ]
          });
        }
      })
    });
  });
})()
