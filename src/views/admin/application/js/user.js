vm = new Vue({
  el: '#app',
  data: function() {
    return {
      tips: false,
    }
  },
  mounted() {
    this.init()
    this.isTips()
  },
  methods: {
    downLoad() {
      var _this = this
      window.location.href = url + ':9000/login/downloadApp'
      this.$message('下载中')
      this.tips = false
    },
    handleClose() {
      this.tips = false
      sessionStorage.removeItem('isFirstLogin')
    },
    isTips() {
      var isFirstLogin = sessionStorage.getItem('isFirstLogin')
      if (isFirstLogin === 'true') {
        this.tips = true
      }
    },
    // 检查session
    init() {
      $.ajax({
        url: url + ":9006/AppConnection/appConnectionSession",
        type: "get",
        async: true,
        success: function (data) {
          console.log(data)
        }
      })
    }
  }
})
