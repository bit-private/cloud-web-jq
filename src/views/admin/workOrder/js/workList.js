
vm = new Vue({
    el: '#app',
    data: function () {
        return {
            submitSerch:'',
            currentPage:1,
            pageSize:10,
            tableDataTotal:0,
            tableData:[],
            dialogTitle:"",
            dialogVisible:false,
            details:'',
        }
    },
    methods: {
        resetSearch(){
            this.submitSerch=''
            this.currentPage=1
            this.init();
        },
        handleSizeChange(val) {
            this.currentPage=1
            this.pageSize=val
            this.init();
        },
        handleCurrentChange(val) {
            this.currentPage=val
            this.init();
        },
        showDetail(param,flag){
            let _that=this
            if(flag=='title'){
                this.dialogTitle='工单标题'
                _that.dialogVisible=true
                _that.details=param.title
            }else if(flag=='description'){
                this.dialogTitle='问题描述'
                _that.dialogVisible=true
                _that.details=param.describe
            }else if(flag=='opinion'){
                this.dialogTitle='处理意见'
                _that.dialogVisible=true
                _that.details=param.opinion
            }
        },
        searchTable(){
            this.init()
        },
        init(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9006/workOrder/getWorkOrderList',
                type: 'post',
                contentType: "application/json;charset=utf-8",
                async: false,
                data:JSON.stringify({pd:{ title:_that.submitSerch},currentPage:_that.currentPage,showCount:_that.pageSize}),
                headers: {
                    "token":  _that.getCookie("token")
                },
                dataType: 'json',
                success: function(data) {
                    _that.$set(_that,"tableData",data.data.content)
                    _that.tableDataTotal=data.data.pageCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
    },
    mounted() {
        this.init();
        var a =[ {user:[1,2,3]}, {type:[4,5,6]} ];
        var b ={};
        a.map(item=> {
            console.log(item,b)
            b={...item, ...b}
        });
        console.log(b);
    },
})
