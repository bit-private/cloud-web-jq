
vm = new Vue({
    el: '#app',
    data: function () {
        return {
            submitSerch:'',
            currentPage:1,
            pageSize:10,
            tableDataTotal:0,
            tableData:[],

            recordSubmitSerch:'',
            recordCurrentPage:1,
            recordPageSize:10,
            recordTableDataTotal:0,
            recordTableData:[],
            dialogTitle:"",
            dialogVisible:false,
            details:'',
            addWorkOrder:false,
            activeName:'first',

            submitPeople:'',
            problemType:'',
            softOrhard:'',
            title:'',
            describe:'',
            opinions:'',
            submitID:'',
            ownAuthorId:''
        }
    },
    methods: {
        resetSearch(){
            this.submitSerch=''
            this.currentPage=1
            this.init();
        },
        recordResetSearch(){
            this.recordSubmitSerch=''
            this.recordCurrentPage=1
            this.getRecord()
        },
        handleClick(tab, event){
            if(this.activeName=="first"){
               this.submitSerch=''
               this.currentPage=1
               this.pageSize=10
                this.init()
            }else {
                this.recordSubmitSerch=''
                this.recordCurrentPage=1
                this.recordPageSize=10
                this.getRecord()
            }
        },
        handleSizeChange(val) {
            this.currentPage=1
            this.pageSize=val
            this.init();
        },
        getOwntreeData() {

        },
        handleCurrentChange(val) {
            this.currentPage=val
            this.init();
        },
        recordSizeChange(val) {
            this.recordCurrentPage=1
            this.recordPageSize=val
            this.getRecord();
        },
        recordCurrentChange(val) {
            this.recordCurrentPage=val
            this.getRecord();
        },
        searchTable(){
            this.init()
        },
        recordSearchTable(){
            this.getRecord()
        },
        init(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9006/workOrder/getUntreatedWorkOrderList',
                type: 'post',
                contentType: "application/json;charset=utf-8",
                async: false,
                data:JSON.stringify({pd:{ title:_that.submitSerch,id:_that.getCookie('userId')},currentPage:_that.currentPage,showCount:_that.pageSize}),
                headers: {
                    "token":  _that.getCookie("token")
                },
                dataType: 'json',
                success: function(data) {
                    _that.$set(_that,"tableData",data.data.content)
                    _that.tableDataTotal=data.data.pageCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getRecord(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            console.log(1111)
            $.ajax({
                url: url+':9006/workOrder/getEndWorkOrderList',
                type: 'post',
                contentType: "application/json;charset=utf-8",
                async: false,
                data:JSON.stringify({pd:{ title:_that.recordSubmitSerch,id:_that.getCookie('userId')},currentPage:_that.recordCurrentPage,showCount:_that.recordPageSize}),
                headers: {
                    "token":  _that.getCookie("token")
                },
                dataType: 'json',
                success: function(data) {
                    _that.$set(_that,"recordTableData",data.data.content)
                    _that.recordTableDataTotal=data.data.pageCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        addWorkOrderFun(){
            this.addWorkOrder=true
        },
        cancel(){
            this.addWorkOrder=false
        },
        dialogClosed(){
          this.opinions=''
          this.submitID=''
        },
        showDetail (param,flag){
            let _that=this
            if(flag=='title'){
                this.dialogTitle='工单标题'
                _that.dialogVisible=true
                _that.details=param.title
            }else if(flag=='description'){
                this.dialogTitle='问题描述'
                _that.dialogVisible=true
                _that.details=param.describe
            }else if(flag=='opinion'){
                this.dialogTitle='处理意见'
                _that.dialogVisible=true
                _that.details=param.opinion
            }
        },
        open(row){
            this.addWorkOrder=true
             this.submitPeople=row.author
             this.problemType=row.problem_type
             this.softOrhard=row.equipment
             this.title=row.title
             this.describe=row.describe
            this.submitID=row.id
            this.ownAuthorId=row.authorId
        },
        submitForm(){
            let _that=this
            if(this.opinions=='' || String(this.opinions).replace(/\s+/g, "")==''){
                layer.msg("处理意见不能为空",{
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            $.ajax({
                url: url+':9006/workOrder/updateWorkOrderHandle',
                type: 'post',
                async: true,
                data:{opinion:_that.opinions,id:_that.submitID,authorId:_that.ownAuthorId},
                headers: {
                    "token":  _that.getCookie("token")
                },
                success: function(data) {
                    if(data.code==200){
                        layer.msg(data.message,{
                            icon: 1,
                            closeBtn: 1
                        });
                        _that.addWorkOrder=false
                        _that.currentPage=1
                        _that.init()
                    }else{
                        layer.msg(data.message,{
                            icon: 7,
                            closeBtn: 1
                        });
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })

        },
    },
    mounted() {
        this.init();
    },
})
