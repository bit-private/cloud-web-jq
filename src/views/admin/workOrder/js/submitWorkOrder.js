
vm = new Vue({
    el: '#app',
    data: function () {
        return {
            submitSerch:'',
            currentPage:1,
            pageSize:10,
            tableDataTotal:0,
            tableData:[],
            dialogTitle:"",
            dialogVisible:false,
            details:'',
            addWorkOrder:false,
            problemType:'' ,
            problemTypeList:[{name:'硬件问题',val:1},{name:'软件问题',val:2},{name:'其他',val:3}],
            softwareAndHardware:[],
            region:'',
            title:'',
            describe:'',
        }
    },
    methods: {
        changeSelect(){
              let _that=this
              $.ajax({
                  url: url+':9006/workOrder/getEquipmentName',
                  type: 'post',
                  async: false,
                  data: {type:this.problemType},
                  headers: {
                      "token":  _that.getCookie("token")
                  },
                  success: function(data) {
                    _that.softwareAndHardware=data.data.content
                    _that.region=''
                      if(_that.problemType==3 && data.data.content.length==0){
                          layer.msg('未授权受理人，请联系管理员', {
                              icon: 2,
                              closeBtn: 1
                          });
                      }
                  },
                  error: function(data) {
                      console.log(data)
                  }
              })
        },
        resetSearch(){
            this.submitSerch=''
            this.currentPage=1
            this.init();
        },
        handleSizeChange(val) {
            this.currentPage=1
            this.pageSize=val
            this.init();
        },
        handleCurrentChange(val) {
            this.currentPage=val
            this.init();
        },
        showDetail (param,flag){
            let _that=this
            if(flag=='title'){
                this.dialogTitle='工单标题'
                _that.dialogVisible=true
                _that.details=param.title
            }else if(flag=='description'){
                this.dialogTitle='问题描述'
                _that.dialogVisible=true
                _that.details=param.describe
            }else if(flag=='opinion'){
                this.dialogTitle='处理意见'
                _that.dialogVisible=true
                _that.details=param.opinion
            }
        },
        searchTable(){
            this.init()
        },
        init(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            $.ajax({
                url: url+':9006/workOrder/getSubmitWorkOrderList',
                type: 'post',
                contentType: "application/json;charset=utf-8",
                async: false,
                data:JSON.stringify({pd:{ title:_that.submitSerch,id:_that.getCookie('userId')},currentPage:_that.currentPage,showCount:_that.pageSize}),
                headers: {
                    "token":  _that.getCookie("token")
                },
                dataType: 'json',
                success: function(data) {
                    _that.$set(_that,"tableData",data.data.content)
                    _that.tableDataTotal=data.data.pageCount
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        addWorkOrderFun(){
            this.addWorkOrder=true
        },
        cancel(){
            this.addWorkOrder=false
        },
        dialogClosed(){
            this.problemType=''
            this.region=''
            this.title=''
            this.describe=''
        },
        open(row){
            let _that=this
            $.ajax({
                url: url+':9006/workOrder/updateWorkOrderStatus',
                type: 'post',
                async: true,
                headers: {
                    token: this.getCookie('token')
                },
                data: {id:row.id},
                success: function(data) {
                    if(data.code==200){
                        layer.msg('撤回成功', {
                            icon: 1,
                            closeBtn: 1
                        });
                        _that.currentPage=1
                        _that.init()
                    }else{
                        layer.msg(data.message, {
                            icon: 2,
                            closeBtn: 1
                        });
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        submitForm(){
            let _that=this
            let sysName=''
            let responsibleId=''
           if(this.problemType==''){
               layer.msg("请选择问题类型",{
                   icon: 7,
                   closeBtn: 1
               });
               return
           }
            if(this.problemType==1 && this.region==''){
                layer.msg("请选择设备名称",{
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            if(this.problemType==2 && this.region==''){
                layer.msg("请选择软件名称",{
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            if(_that.problemType==3 && _that.softwareAndHardware.length==0){
                layer.msg('未授权受理人，请联系管理员', {
                    icon: 2,
                    closeBtn: 1
                });
                return
            }
            if(this.title=='' || String(this.title).replace(/\s+/g, "")==''){
                layer.msg("请输入标题",{
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            if(this.describe=='' ||  String(this.describe).replace(/\s+/g, "")==''){
                layer.msg("请选择描述",{
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            if(this.problemType!=3){
                for(let i=0;i<this.softwareAndHardware.length;i++){
                    if(this.region==this.softwareAndHardware[i].id){
                        sysName=this.softwareAndHardware[i].name
                        responsibleId=this.softwareAndHardware[i].administrator
                    }
                }
            }
            let params={
                authorId:_that.getCookie('userId'),
                problemType:'',
                title:_that.title,
                describe:_that.describe,
                equipment:this.problemType==3?'':sysName,
                responsibleId:this.problemType==3?'':responsibleId,
            }
            if(this.problemType==1){
                params.problemType='硬件问题'
            }else if(this.problemType==2){
                params.problemType='软件问题'
            }else{
                params.problemType='其他'
            }
            $.ajax({
                url: url+':9006/workOrder/addWorkOrder',
                type: 'post',
                async: true,
                contentType: 'application/json;charset=utf-8',
                headers: {
                    token: this.getCookie('token')
                },
                data: JSON.stringify(params),
                success: function(data) {
                    if(data.code==200){
                        layer.msg('新建成功', {
                            icon: 1,
                            closeBtn: 1
                        });
                        _that.addWorkOrder=false
                        _that.currentPage=1
                        _that.init()
                    }else{
                        layer.msg(data.message, {
                            icon: 2,
                            closeBtn: 1
                        });
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
    },
    mounted() {
        this.init();
    },
})
