vm = new Vue({
  el: '#approval',
  data: function() {
    return {
      otherUnitsTree:[],
      otherSelectNodes:[],
      otherTreeIDs:[],
      otherTreeNames:[],
      otherNamesStr:'',
      poewr:true,
      ownSelectNodes:[],
      ownTree:[],
      ownTreeIDs:[],
      ownTreeNames:[],
      ownNamesStr:'',
      props: {
        children: 'children',
        label: 'name',
        id: 'id'
      },
      otherProps: {
        children: 'children',
        label: 'name',
        id: 'id'
      },
      ownradio:'',
      name:'',//用户名
      typeList:false,//申请类别
      typeList2:false,//申请类别
      typeList3:false,//申请类别
      projectName:"",
      value1:'',//开始时间
      value2:'',//结束时间
      state:'',//存储容量
      tips:'',//备注
      softwareType:'',//选中软件名称
      softwareTypeList:{//已添加软件类型

      },
      //审核通过拒绝需要的流程实例ID
      processInstanceId:'',
      //管理员搜索框
      inputSerch:'',
      //
      radio: '',
      deviceRadio: '',
      // 硬件分配完成后在显示确定按钮
      resources: false,
      // 待审核状态颜色
      typeStyle: '',
      // 审核记录文字颜色
      textStyle: '',
      // 默认显示的tab
      activeName: 'first',
      // activeName: 'dispose',
      // table数据
      tableData: [],
      tableDataPage:1,
      approvalPendingPage:1,
      approvalPendingTotal:0,
      recordPage:1,
      recordTotal:0,
      pendingNum:0,
      //审批记录
      approvalRecord:[],
      //领导端待审批
      approvalPending:[],
      // dialog显示
      centerDialogVisible: false,
      // 硬件设备dialog
      centerDialogVisible1: false,
      selfNum:0,
      groupLists:[],
      deviceLists:[],
      distribution:{},
      recouGroup:{},
      putDistribution:[],
      putRecouGroup:[],
      currentIndex:'',
      powerProcessInstanceId:'',
      depare:'',
      seeID:'',
      jfog:'',
      currentUserId:'',
      // 审批配置
      dispose: {
        value: '',
        options: [],
        value1: [],
        value1id: [],
        options1: [],
        op2:[],
        value2: [],
        value2id: [],
        options2: [],
        defaultProps: {
          children: 'children',
          label: 'name'
        },
        list: []
      },
    }
  },
  watch: {
    selectedData: function(newValue) {
      this.$nextTick(() => { this.dataArr = this.handleDataTransform(newValue, 'key', 'label'); });
    },
  },

  methods: {
    handleCheckChange2(key) {
      console.log(key)
      console.log(this.$refs[key+'b'])
      console.log(this.$refs[key+'b'][0].getCheckedKeys())
      console.log(this.$refs[key+'b'][0].getCheckedNodes())
      this.$set(this.dispose.op2[key], 'value2id', this.$refs[key+'b'][0].getCheckedKeys().join(','))
      // this.dispose.op2[key].value1id = this.$refs.tree.getCheckedKeys()
      let arr = []
      for (let i = 0; i < this.$refs[key+'b'][0].getCheckedNodes().length; i++) {
        if(!this.$refs[key+'b'][0].getCheckedNodes().children){
          arr.push(this.$refs[key+'b'][0].getCheckedNodes()[i].name)
        }
      }
      this.$set(this.dispose.op2[key], 'value2',arr)
      console.log(this.dispose.op2[key])
    },
    handleCheckChange(key) {
      console.log(key)
      console.log(this.$refs[key+'a'])
      console.log(this.$refs[key+'a'][0].getCheckedKeys())
      console.log(this.$refs[key+'a'][0].getCheckedNodes())
      this.$set(this.dispose.op2[key], 'value1id', this.$refs[key+'a'][0].getCheckedKeys().join(','))
      // this.dispose.op2[key].value1id = this.$refs.tree.getCheckedKeys()
      let arr = []
      for (let i = 0; i < this.$refs[key+'a'][0].getCheckedNodes().length; i++) {
        if(!this.$refs[key+'a'][0].getCheckedNodes().children){
          arr.push(this.$refs[key+'a'][0].getCheckedNodes()[i].name)
        }
      }
      this.$set(this.dispose.op2[key], 'value1',arr)
      console.log(this.dispose.op2[key])
    },
    handClick(key){
      console.log(key)
    },
    // 添加配置
    addDispose() {
      let _that = this
      console.log(this.dispose.options)
      console.log(this.dispose.value)
      if(this.dispose.value != ''){
        for (let i = 0; i < this.dispose.op2.length; i++) {
          if(this.dispose.op2[i].value == this.dispose.value){
            _that.$message({
              message:'已有该部门配置',
              type: 'error'
            });
          this.dispose.value = ''
            return
          }
        }
          let obj = {
          value:this.dispose.value,
          value1:'',
          value2:'',

        }
        this.dispose.op2.push(obj)
        this.dispose.value = ''
      }else{
        _that.$message({
          message:'请选择部门',
          type: 'error'
        });
      }
    },
    // 删除
    del(key){
      console.log(key)
      var _that = this
        let bmid= ''
        for (let i = 0; i < this.dispose.options.length; i++) {
          if(this.dispose.op2[key].value == this.dispose.options[i].name){
            bmid = this.dispose.options[i].id
          }        
        }
        let obj = {
          departmentId:bmid,
        }
      $.ajax({
        url:  url+':9001/examineAndApproveResource/deleteAdminOptions',
        type: 'post',
        headers: {
          "token":  _that.getCookie("token")
        },
        data:obj,
        success: function(data) {
          if(data.code == '200'){
            console.log(data)
            _that.$message({
              message: data.message,
              type: 'success'
            });
            _that.getDispose()
          }else{
            // _that.$message({
            //   message: data.message,
            //   type: 'error'
            // });
            _that.dispose.op2.splice(key,1)
          }
        }
      })
    },
    // 保存
    save(){
      let _that = this
      let arr = []
      for (let j = 0; j < this.dispose.op2.length; j++) {
          let bmid= ''
          for (let i = 0; i < this.dispose.options.length; i++) {
            if(this.dispose.op2[j].value == this.dispose.options[i].name){
              bmid = this.dispose.options[i].id
            }        
          }
          if(this.dispose.op2[j].value2.length == 0 || this.dispose.op2[j].value1.length == 0){
            _that.$message({
              message: '请完善审批人员信息',
              type: 'error'
            });
            return
          }else{
            let obj = {
              userId: this.getCookie("userId"),
              departmentId:bmid,
              secondStage:this.dispose.op2[j].value2id,
              firstStage:this.dispose.op2[j].value1id,
            }
          arr.push(obj)
          }
      }
      console.log(arr)
      $.ajax({
        url: url+':9001/examineAndApproveResource/adminOptions',
        type: 'post',
        async: false,
        contentType: "application/json;charset=utf-8",
        headers: {
          "token": _that.getCookie("token"),
          // "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        data: JSON.stringify(arr),
        success: function(data) {
          if(data.code == '200'){
            // console.log(data)
            _that.$message({
              message: data.message,
              type: 'success'
            });
            _that.getDispose()
          }else{
            _that.$message({
              message: data.message,
              type: 'error'
            });
          }
        },
        error: function(data) {

        }
      })
    },
    // 获取审批配置的回显
    getDispose() {
      var _that = this
      $.ajax({
        url: url+':9001/examineAndApproveResource/departmentAndOrgTree',
        type: 'post',
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          console.log(data)
          _that.dispose.options = data.data.department
          _that.dispose.options1 = data.data.tree
          _that.dispose.op2 = []
          if(data.data.entityList && data.data.entityList.length>0){
            for (let i = 0; i < data.data.entityList.length; i++) {
              let obj = {
                value:'',
                value1:data.data.entityList[i].firstStageNames.split(','),
                value1id:data.data.entityList[i].firstStage,
                value2:data.data.entityList[i].secondStageNames.split(','),
                value2id:data.data.entityList[i].secondStage,
              }
              for (let j = 0; j < data.data.department.length; j++) {
                if(data.data.entityList[i].departmentId == data.data.department[j].id){
                  obj.value = data.data.department[j].name
                }
              }
              _that.dispose.op2.push(obj)
            }
            console.log(_that.dispose.op2)
          }else{
            // _that.dispose.op2 = []
          }
        }
      })
    },
    ownClose(){
      this.distribution={}
      this.recouGroup={}
      this.putDistribution=[]
      this.putRecouGroup=[]
    },
    handleClose(){
      this.softwareTypeList={}
    },
    //获取待审批列表
    getApprovalPending(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: url+':9001/examineAndApproveResource/waitApproval',
        type: 'post',
        async: false,
        data: {currentUserId:userId,
          page:_that.approvalPendingPage,size:10,conditions:this.inputSerch},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          _that.$set(_that,"approvalPending",data.data.list)
          _that.$set(_that,"approvalPendingTotal",data.data.count)
          _that.$set(_that,"pendingNum",data.data.count)
          _that.$set(_that,"selfNum",data.data.count)
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //获取审批记录列表
    getApprovalRecord(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: url+':9001/examineAndApproveResource/myRecord',
        type: 'post',
        async: false,
        data: {currentUserId:userId,page:_that.recordPage,
          size:10,conditions:this.inputSerch},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          if(data.data!=null) {
            _that.$set(_that, "approvalRecord", data.data.list)
            _that.$set(_that, "recordTotal", data.data.count)
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    // 上级搜索按钮
    searchLeader() {
      if(this.activeName=='first'){
        this.approvalPendingPage=1
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=1
        this.getApprovalRecord()
      }
    },
    // 管理员搜索按钮
    searchAdmin() {
      if(this.activeName=='first'){
        this.approvalPendingPage=1
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=1
        this.getApprovalRecord()
      }
    },
    getCookie: function (name) {
      var cookie = document.cookie;
      var arr = cookie.split("; "); //将字符串分割成数组
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=");
        if (arr1[0] == name) {
          return unescape(arr1[1]);
        }
      }
      return "GG"
    },
    // 资源组单选按钮
    handleChange(val, val1) {
      console.log(val1)
      let _that=this
      this.recouGroup=val1;
      this.deviceRadio=''
      this.putDistribution=[]
      this.$set(this.putRecouGroup ,this.currentIndex, this.recouGroup)
      $.ajax({
        url: url+':9001/examineAndApproveResource/hardwareByGroupId',
        type: 'post',
        async: false,
        data: {groupId:val1.id,applicationId:this.seeID},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          console.log(data)
          _that.deviceLists=data.data
        },
        error: function(data) {

        }
      })
    },
    handleDeviceChange(val, val1){
      this.distribution=val1
      this.$set(this.putDistribution,this.currentIndex,this.distribution)
    },
    ruleCheck(){
        if(this.putDistribution.length==0&&this.putRecouGroup.length==0){
          this.$message({
            message: '您未选择任何设备',
            type: 'error'
          });
        }
        this.radio=''
        this.deviceRadio=''
    },
    chanrab(){
      this.radio=''
      this.deviceRadio=''
      this.deviceLists=[]
    },
    beforesureDistribution(){
      let _that=this;
      let params=[];
      console.log(_that.tableData)
      console.log(_that.putDistribution)
      for(let i=0;i<_that.tableData.length;i++){
        params.push({applicationId:_that.tableData[i].applicationId,
          authDate:_that.tableData[i].authDate,
          authType:_that.tableData[i].applyType==2?'org':'user',
          department:_that.tableData[i].department,
          groupId:_that.putRecouGroup[i]?_that.putRecouGroup[i].id:'',
          hardwareId:_that.putDistribution[i]?_that.putDistribution[i].id:_that.recouGroup.id,
          hardwareType: _that.putDistribution[i]?'single':'group',
          linkType:_that.tableData[i].linkType.value,
          userId:_that.tableData[i].applyType==2?_that.tableData[i].departmentId:_that.tableData[i].currentUserId,
          // userId:_that.tableData[i].currentUserId,
          // userName:_that.tableData[i].userName,
          userName:_that.tableData[i].applyType==2?_that.tableData[i].department:_that.tableData[i].userName,
        })
      }
      console.log(_that.tableData)
      $.ajax({
        url: url+':9002/authorization/saveAuthorizations',
        type: 'post',
        async: false,
        headers: {
          "token": _that.getCookie("token"),
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        data: JSON.stringify(params),
        success: function(data) {
           if(data.message=='授权成功'){
             _that.sureDistribution()
           }else{
             _that.$message({
               message: data.message,
               type: 'error'
             });
           }
        },
        error: function(data) {

        }
      })
    },
    sureDistribution(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
        $.ajax({
          url: url+':9001/examineAndApproveResource/auditBeg',
          type: 'post',
          async: false,
          data: {currentUserId:userId,whether:true,
            processInstanceId:this.powerProcessInstanceId},
          headers: {
            "token":  _that.getCookie("token")
          },
          success: function(data) {
            _that.$message({
              message: '授权成功',
              type: 'success'
            });
            this.centerDialogVisible=false
            this.distribution={}
            this.recouGroup={}
            this.putDistribution=[]
            this.putRecouGroup=[]
            _that.getApprovalPending();
          },
          error: function(data) {

          }
        })
    },
    // tab页切换
    tabsHandleClick(tab, event) {
      this.inputSerch=''
      this.activeName=tab.name
      console.log(this.activeName)
      console.log(tab)
      if(this.activeName=='first'){
        this.approvalPendingPage=1
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=1
        this.getApprovalRecord()
      }
    },
    // 分页改变
    currentChange(val) {
      console.log(val)
    },
    handleCurrentChange(val){
      if(this.activeName=='first'){
        this.approvalPendingPage=val
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=val
        this.getApprovalRecord()
      }
    },
    handlePendingChange(val){
      if(this.activeName=='first'){
        this.approvalPendingPage=val
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=val
        this.getApprovalRecord()
      }
    },
    // 审核回显
    open(val) {
      let _that=this;
      this.processInstanceId=val.row.processInstanceId
      this.typeList=false
      this.typeList2=false
      this.typeList3=false
      $.ajax({
        url:url+':9001/examineAndApproveResource/getUpdatePage',
        type: 'post',
        async: false,
        data: {processInstanceId:val.row.processInstanceId},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          _that.name=data.data.currentUserName;
          _that.ownradio=data.data.applyType;
          if(data.data.applyType==1){
            _that.typeList=true
          }else if(data.data.applyType==2){
            _that.typeList2=true
            _that.$set(_that,'ownSelectNodes',data.data.ids.split(','))
            _that.$set(_that,'ownNamesStr',data.data.names)
          }else if(data.data.applyType==3){
            _that.typeList3=true
            _that.$set(_that,'otherSelectNodes',data.data.ids.split(','))
            _that.$set(_that,'otherNamesStr',data.data.names)
          }
          _that.projectName=data.data.projectName;
          _that.depare=data.data.applyTypeValue;
          _that.value1=data.data.startTime;
          _that.value2=data.data.endTime;
          _that.state=data.data.capacity;
          _that.tips=data.data.remark;
          _that.softwareType=data.data.softwareValue;
          let arr=data.data.softwareValue?data.data.softwareValue.split(","):[];
          let arr1=data.data.softwareTypeKey?data.data.softwareTypeKey.split(","):[];
          let arr2=data.data.softwareNames?data.data.softwareNames.split(","):[];
          for(let i=0;i<arr.length;i++){
            _that.$set(_that.softwareTypeList,"index_"+i,{name:arr[i],softName:arr2[i]})
          }
        },
        error: function(data) {

        }
      })
    },
    //审核通过或拒绝
    submitApproval(adopt){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: url+':9001/examineAndApproveResource/auditBeg',
        type: 'post',
        async: false,
        data: {currentUserId:userId,processInstanceId:_that.processInstanceId,whether:adopt},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          _that.$message({
            message: '审核成功',
            type: 'success'
          });
          _that.getApprovalPending()
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    selfOpen(params){
      this.powerProcessInstanceId=params.row.processInstanceId
      this.currentUserId=params.row.currentUserId
      let _that=this
      $.ajax({
        url: url+':9001/examineAndApproveResource/authList',
        type: 'post',
        async: false,
        data:{processInstanceId:params.row.processInstanceId},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          _that.tableData=data.data
        },
        error: function(data) {

        }
      })
    },
    // 打开硬件设备弹框
    openEquipment(val) {
      console.log(val)
      this.currentIndex=val.$index;
      this.seeID=val.row.applicationId;
      let _that=this
      $.ajax({
        url: url+':9001/examineAndApproveResource/hardwareGroupList',
        type: 'post',
        async: false,
        data:{
          applicationId:val.row.applicationId
        },
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
        console.log(data)
          _that.groupLists=data.data
        },
        error: function(data) {

        }
      })
    },
    resetSearch(){
      this.inputSerch=''
      if(this.activeName=='first'){
        this.approvalPendingPage=1
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=1
        this.getApprovalRecord()
      }
    },
    tab () {//申请类型切换
      if (this.radio === 0) {
        this.typeList=false
        this.typeList2=false
        this.typeList3=false
      } else if(this.radio == 1) {
        this.typeList=true
        this.typeList2=false
        this.typeList3=false
      }else if(this.radio == 2) {
        this.typeList=false
        this.typeList2=true
        this.typeList3=false
      }else if(this.radio == 3) {
        this.typeList=false
        this.typeList2=false
        this.typeList3=true
      }
    },
    getOrgTreeAndName(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: url+':9001/examineAndApproveResource/orgTreeAndName',
        type: 'post',
        async: false,
        data: {currentUserId:userId},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          // _that.otherUnitsTree=data.data.orgTree
          // _that.ownTree=data.data.rankTree
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getPoewr(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: url+':9001/examineAndApproveResource/getUserInfo',
        type: 'post',
        async: false,
        data: {currentUserId:userId},
        headers: {
          "token":  _that.getCookie("token")
        },
        success: function(data) {
          console.log(data.data.professionalTitle)
         if(data.data.professionalTitle!=null){
           _that.poewr=true
           _that.jfog=data.data.professionalTitle
         }else {
           _that.poewr=false
         }
        },
        error: function(data) {
          console.log(data)
        }
      })
    }
  },
  mounted() {
    this.getPoewr()
    this.getApprovalPending();
    this.getOrgTreeAndName();
    this.getDispose()
    this.getApprovalRecord();
  },
})
