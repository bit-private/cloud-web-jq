vm = new Vue({
  el: "#app",
  data: function() {
    return {
      taskId: "",
      processInstanceId: "",
      reviewProgress: "",
      reviewProgressValue: "",
      paramsState: "",
      paramsStateValue: "",
      otherUnitsTree: [],
      otherSelectNodes: [],
      otherTreeIDs: [],
      otherTreeNames: [],
      otherNamesStr: "",

      ownSelectNodes: [],
      ownTree: [],
      ownTreeIDs: [],
      ownTreeNames: [],
      ownNamesStr: "",
      props: {
        children: "children",
        label: "name",
        id: "id"
      },
      otherProps: {
        children: "children",
        label: "name",
        id: "id"
      },
      isEdit: false,
      ownSelect: {},
      title: "",
      adoptNum: 0,
      recordNum: 0,
      tableDataPage: 1,
      tableDataTotal: 0,
      table1DataPage: 1,
      table1DataTotal: 0,
      table2DataPage: 1,
      table2DataTotal: 0,
      multipleSelectionRows: [],
      projectName: "",
      show1: true,
      show2: false,
      show3: false,
      activeName: "first",
      activeIndex: "1",
      activeIndex2: "1",
      search: "",
      submitSerch: "", //提交申请搜索
      adoptSerch: "", //已通过搜索
      recordSerch: "", //提交记录搜索
      typeList: false, //申请类别
      typeList2: false, //申请类别
      typeList3: false, //申请类别
      name: "", //用户名
      radio: 0, //单选框
      value1: "", //开始时间
      value2: "", //结束时间
      state: "", //存储容量
      showDialog: false, //弹框显隐
      tips: "", //备注
      softwareTypes: [], //软件类型下拉框
      softwareType: "", //选中软件名称
      softwareTypeList: {}, //已添加软件类型
      softwareEdition: [], //软件名称/类型
      tableData: [], //提交申请数据
      tableData1: [], //已通过数据
      tableData2: [], //提交记录数据
      ownBMID: "",
      isAdoptNum: false,
      isRecordNum: true,
      firstSubmit: false,
      allPeople: 0,
      wPeople: 0,
      branchName: "",
      branchID: "",
      branchNum: "",
      commitNum: 1,
      presentCheckNode: {},
      poewr: true,
      foreignOrgId: ""
    }
  },
  methods: {
    handleClick(val){
        if (this.activeName == "first") {
            this.submitSerch = ""
            this.table1DataPage = 1
            this.init()
        } else if (this.activeName == 'second') {
            this.adoptSerch = ""
            this.table1DataPage = 1
            this.getMyPass()
            this.adoptNum = 0
            this.isAdoptNum = false
        } else if (this.activeName == 'third') {
            this.recordSerch = ""
            this.table2DataPage = 1
            this.isRecordNum = false
            this.getSubmitRecord()
        }
    },
    getNum() {
      let _that = this
      $.ajax({
        url: url + ":9001/examineAndApproveResource/unreadCount ",
        type: "post",
        async: false,
        data: { currentUserId: this.getCookie("userId") },
        headers: {
          token: token
        },
        success: function(data) {
          if (data.data == 0) {
            _that.isAdoptNum = false
          } else {
            _that.isAdoptNum = true
          }
          _that.adoptNum = data.data
        }
      })
    },
    radioChange(val) {
      if (this.$refs.otherTree) {
        this.$refs.otherTree.setCheckedKeys([])
      }
      this.otherSelectNodes = []
      this.otherTreeIDs = []
      this.otherTreeNames = []
      this.otherNamesStr = ""
      this.ownSelectNodes = []
      this.ownTreeIDs = []
      this.ownTreeNames = []
      this.ownNamesStr = ""
      this.allPeople = 0
      this.wPeople = 0
      this.projectName = ""
    },
    changeStartTime(val) {
      this.value1 = val
      console.log(this.value1)
    },
    changeEndTime(val) {
      this.value2 = val
    },
    ownClosed() {
      this.projectName = ""
      this.ownSelectNodes = []
      this.otherSelectNodes = []
    },
    init() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/myApplyDetail",
        // url: 'http://10.1.1.37:9001/examineAndApproveResource/myApplyDetail',
        type: "post",
        async: false,
        data: { currentUserId: userId, page: _that.tableDataPage, size: 10, conditions: this.submitSerch },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          _that.$set(_that, "tableData", data.data.list)
          _that.tableDataTotal = data.data.count
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getMyPass() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/myPass",
        type: "post",
        async: false,
        data: { currentUserId: userId, page: _that.table1DataPage, size: 10, conditions: this.adoptSerch },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data != null) {
            _that.$set(_that, "tableData1", data.data.list)
            _that.table1DataTotal = data.data.count
            _that.adoptNum = data.data.count
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getMyRecord() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/myRecord",
        type: "post",
        async: false,
        data: { currentUserId: userId, page: _that.table2DataPage, size: 10, conditions: this.recordSerch },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data != null) {
            _that.$set(_that, "tableData2", data.data.list)
            _that.table2DataTotal = data.data.count
            _that.recordNum = data.data.count
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getSubmitRecord() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/submitRecord",
        type: "post",
        async: false,
        data: { currentUserId: userId, page: _that.table2DataPage, size: 10, conditions: this.recordSerch },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data != null) {
            _that.$set(_that, "tableData2", data.data.list)
            _that.table2DataTotal = data.data.count
            _that.recordNum = data.data.count
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getAddMessage() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/getPageByUserId",
        type: "post",
        async: false,
        data: { currentUserId: userId },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          _that.softwareTypes = data.data.appTypeList
          _that.name = data.data.realName
          _that.getSoftWareList(data.data.appTypeList)
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    getOrgTreeAndName() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/orgTreeAndName",
        type: "post",
        async: false,
        data: { currentUserId: userId },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          _that.otherUnitsTree = data.data.orgTree
          _that.ownBMID = data.data.orgId
          _that.dadnode(data.data.rankTree)
          _that.ownTree = data.data.rankTree
          _that.allPeople = data.data.orgUserCount
          _that.branchName = data.data.orgName
          _that.branchID = data.data.orgId
          _that.branchNum = data.data.orgUserCount
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    dadnode(params) {
      for (let i = 0; i < params.length; i++) {
        if (params[i].children && params[i].children.length == 0 && params[i].id != this.ownBMID) {
          params[i].disabled = true
        } else if (params[i].children && params[i].children.length > 0) {
          this.dadnode(params[i].children)
        }
      }
    },
    owndadnode(params) {
      for (let i = 0; i < this.otherUnitsTree.length; i++) {
        if (this.otherUnitsTree[i].children && this.otherUnitsTree[i].id != params) {
          this.$set(this.otherUnitsTree[i], "disabled", true)
          for (let j = 0; j < this.otherUnitsTree[i].children.length; j++) {
            this.$set(this.otherUnitsTree[i].children[j], "disabled", true)
          }
        }
      }
    },
    resetDao() {
      for (let i = 0; i < this.otherUnitsTree.length; i++) {
        if (this.otherUnitsTree[i].children) {
          this.$set(this.otherUnitsTree[i], "disabled", false)
          for (let j = 0; j < this.otherUnitsTree[i].children.length; j++) {
            this.$set(this.otherUnitsTree[i].children[j], "disabled", false)
          }
        } else {
          this.$set(this.otherUnitsTree[i], "disabled", false)
        }
      }
    },
    cutOut() {
      let userId = this.getCookie("userId") // 获取用户id
      let arr = []
      if (this.multipleSelectionRows.length == 0) {
        this.$message({
          message: "请选择软件申请 ",
          type: "error"
        })
        return
      }
      for (let i = 0; i < this.multipleSelectionRows.length; i++) {
        if (this.multipleSelectionRows[i].reviewProgress != 1) {
          layer.msg("已通过审核的申请，不可删除", {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        arr.push(this.multipleSelectionRows[i].processInstanceId)
      }
      let _that = this
      $.ajax({
        url: url + ":9001/examineAndApproveResource/deleteApply ",
        type: "post",
        async: false,
        data: { currentUserId: userId, processInstanceId: arr.join(",") },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data == true) {
            _that.$message({
              message: "删除成功",
              type: "success"
            })
            _that.init()
          } else {
            _that.$message({
              message: data.message,
              type: "error"
            })
            _that.init()
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    searchInput(params) {
      if (params == "submitList") {
        this.tableDataPage = 1
        this.init()
      } else if (params == "adoptList") {
        this.table1DataPage = 1
        this.getMyPass()
      } else if (params == "recordSerch") {
        this.table2DataPage = 1
        this.getMyRecord()
      }
    },
    handleCurrentChange(val) {
      if (this.activeName == "first") {
        // this.submitSerch=''
        this.tableDataPage = val
        this.init()
      } else if (this.activeName == 'second') {
        // this.adoptSerch='';
        this.table1DataPage = val
        this.getMyPass()
      } else if (this.activeName == 'third') {
        // this.recordSerch=''
        this.table2DataPage = val
        this.getMyRecord()
      }
    },
    handleSelectChangeLeft(rows) {
      this.multipleSelectionRows = rows
    },
    handleOpen(key, keyPath) {
      console.log(key, keyPath)
    },
    handleClose(key, keyPath) {
      console.log(key, keyPath)
    },
    handleSelect(val) {
      this.activeIndex = val
      if (val == 1) {
        this.submitSerch = ""
        this.table1DataPage = 1
        this.init()
      } else if (val == 2) {
        this.adoptSerch = ""
        this.table1DataPage = 1
        this.getMyPass()
        this.adoptNum = 0
        this.isAdoptNum = false
      } else if (val == 3) {
        this.recordSerch = ""
        this.table2DataPage = 1
        this.isRecordNum = false
        this.getMyRecord()
      }
    },
    handleEdit(index, row) {
      //催办
      console.log(index, row)
      let _that = this
      $.ajax({
        url: url + ":9001/examineAndApproveResource/urgeTransaction",
        type: "post",
        async: false,
        data: { processInstanceId: row.processInstanceId },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data == true) {
            _that.$message({
              message: "催办成功",
              type: "success"
            })
            _that.init()
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    handleDelete(index, row) {
      //撤回
      console.log(index, row)
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/revocationApply ",
        type: "post",
        async: false,
        data: { currentUserId: userId, processInstanceId: row.processInstanceId },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data == true) {
            _that.$message({
              message: "撤回成功",
              type: "success"
            })
            _that.init()
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    submit() {
      //切换
      this.show1 = true
      this.show2 = false
      this.show3 = false
      // console.log(this.show1)
    },
    adopt() {
      //切换
      this.show1 = false
      this.show2 = true
      this.show3 = false
    },
    record() {
      //切换
      this.show1 = false
      this.show2 = false
      this.show3 = true
    },
    x() {
      //清空搜索
      this.submitSerch = ""
      this.tableDataPage = 1
      this.init()
    },
    y() {
      //清空搜索
      this.adoptSerch = ""
      this.table1DataPage = 1
      this.getMyPass()
    },
    z() {
      //清空搜索
      this.recordSerch = ""
      this.table2DataPage = 1
      this.getMyRecord()
    },
    handleClose1(done) {
      this.$confirm("是否关闭当前页面?", "", {
        confirmButtonText: "是",
        cancelButtonText: "否"
        // type: 'warning'
      })
        .then(() => {
          done()
        })
        .catch(() => {})
    },
    closeBtn() {
      this.showDialog = false
    },
    newly(params) {
        console.log(params)
      //新建请求
      if (params === "edit") {
        this.isEdit = true
        let _that = this
        if (this.multipleSelectionRows.length == 1) {
          if (this.multipleSelectionRows[0].reviewProgress != 1) {
            layer.msg("已通过审核的申请，不可编辑", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
          _that.processInstanceId = _that.multipleSelectionRows[0].processInstanceId
          _that.taskId = _that.multipleSelectionRows[0].taskId
          _that.reviewProgress = _that.multipleSelectionRows[0].reviewProgress
          _that.reviewProgressValue = _that.multipleSelectionRows[0].reviewProgressValue
          _that.paramsState = _that.multipleSelectionRows[0].state
          _that.paramsStateValue = _that.multipleSelectionRows[0].stateValue
          _that.showDialog = true
          $.ajax({
            url: url + ":9001/examineAndApproveResource/getUpdatePage",
            type: "post",
            async: false,
            data: { processInstanceId: _that.multipleSelectionRows[0].processInstanceId },
            headers: {
              token: _that.getCookie("token")
            },
            success: function(data) {
              _that.title = "编辑"
              _that.name = data.data.currentUserName
              _that.radio = data.data.applyType
              if (data.data.applyType == 1) {
                _that.typeList = true
              } else if (data.data.applyType == 2) {
                _that.typeList2 = true
                _that.$set(_that, "ownSelectNodes", data.data.ids.split(","))
                _that.$set(_that, "ownNamesStr", data.data.names)
              } else if (data.data.applyType == 3) {
                _that.typeList3 = true
                _that.$set(_that, "otherSelectNodes", data.data.ids.split(","))
                _that.$set(_that, "otherNamesStr", data.data.names)
              }
              _that.projectName = data.data.projectName
              _that.value1 = data.data.startTime
              _that.value2 = data.data.endTime
              console.log(new Date(_that.value1.replace(/-/g, "/")).getTime())
              console.log(new Date(_that.value2.replace(/-/g, "/")).getTime())
              _that.state = data.data.capacity
              _that.tips = data.data.remark
              let arr = data.data.softwareValue.split(",")
              let arr1 = data.data.softwareIds.split(",")
              let arr2 = data.data.softwareTypeKey.split(",")
              for (let i = 0; i < arr.length; i++) {
                _that.$set(_that.softwareTypeList, "index_" + i, { name: arr[i], softName: arr1[i], select: arr2[i] })
              }
            },
            error: function(data) {}
          })
        } else if (this.multipleSelectionRows.length == 0) {
          layer.msg("请选择编辑项", {
            icon: 2,
            closeBtn: 1
          })
        } else {
          layer.msg("只能选择一项进行编辑", {
            icon: 2,
            closeBtn: 1
          })
        }
      } else {
        this.isEdit = false
        this.resetBtn()
        this.processInstanceId = ""
        this.taskId = ""
        this.reviewProgress = ""
        this.reviewProgressValue = ""
        this.paramsState = ""
        this.paramsStateValue = ""
        this.title = "新建"
        this.showDialog = true
      }
    },
    addSoftwareType() {
      //添加软件类型
      if (this.softwareType == "") {
        // this.$message("请选择软件类型")
        layer.msg("请选择软件类型名称", {
          icon: 2,
          closeBtn: 1
        })
        return
      }
      let obj = JSON.parse(JSON.stringify(this.softwareTypeList)) //深拷贝对象
      let name = ""
      for (let i = 0; i < this.softwareTypes.length; i++) {
        if (this.softwareTypes[i].value == this.softwareType) {
          name = this.softwareTypes[i].typeKey
        }
      }
      this.$set(this.softwareTypeList, this.transformation(obj), { name: this.softwareType, softName: "", select: name })
    },
    transformation: function(obj) {
      let arr = Object.keys(obj) //对象转化数组
      let lastIndex = ""
      let join = ""
      if (arr.length == 0) {
        join = "index_0"
      } else {
        lastIndex = arr[arr.length - 1].split("_")[1] //获取最后一项数字
        join = "index_" + (parseInt(lastIndex) + 1) //将要新增进对象的key值
      }
      return join
    },
    delSoftwareEdition(key) {
      //删除软件类型
      Vue.delete(this.softwareTypeList, key) //vue方法
    },
    resetBtn() {
      //重置
      if (this.isEdit) {
        this.$message({
          message: "不可重置",
          type: "error"
        })
        return
      }
      this.radio = 0
      this.typeList = false
      this.typeList2 = false
      this.typeList3 = false
      this.value1 = "" //开始时间
      this.value2 = "" //结束时间
      this.state = "" //存储容量
      this.softwareType = "" //软件类型
      this.softwareTypeList = {}
      this.tips = "" //备注
      this.taskId = ""

      if (this.$refs.otherTree) {
        this.$refs.otherTree.setCheckedKeys([])
      }
      this.otherSelectNodes = []
      this.otherTreeIDs = []
      this.otherTreeNames = []
      this.otherNamesStr = ""
      this.ownSelectNodes = []
      this.ownTreeIDs = []
      this.ownTreeNames = []
      this.ownNamesStr = ""
      this.allPeople = 0
      this.wPeople = 0
    },
    application() {
      //提交抓取数据
      let userId = this.getCookie("userId") // 获取用户id
      let str = ""
      let _that = this
      if (this.radio === 0) {
        layer.msg("请选择申请类别", {
          icon: 2,
          closeBtn: 1
        })
        return
      } else if (this.radio == 1) {
        str = "个人"
      } else if (this.radio == 2) {
        str = "组织单位"
      } else if (this.radio == 3) {
        str = "外协用户/单位"
      }
      let arr = []
      if (this.value1 == "" || this.value2 == "" || this.state == "" || this.value1 == null || this.value2 == null) {
        layer.msg("必填项不能为空", {
          icon: 2,
          closeBtn: 1
        })
        return
      }
      if ((this.radio == 3 && this.projectName == "") || (this.radio == 3 && this.projectName == null)) {
        layer.msg("必填项不能为空", {
          icon: 2,
          closeBtn: 1
        })
        return
      }
      if (this.otherTreeIDs.length == 0 && this.radio == 3) {
        layer.msg("请选择外协用户", {
          icon: 2,
          closeBtn: 1
        })
        return
      }
      if (this.state % 1 != 0 || this.state.replace(/\s+/g, "") == "" || this.state <= 0) {
        layer.msg("存储容量只能为大于0的整数", {
          icon: 2,
          closeBtn: 1
        })
        this.state = ""
        return
      }
      for (let i in this.softwareTypeList) {
        if (this.softwareTypeList[i].softName == null || this.softwareTypeList[i].softName == "") {
          layer.msg("软件名称不能为空", {
            icon: 2,
            closeBtn: 1
          })
          return
        }
      }
      let softwareTypeKey = []
      let softwareValue = []
      let softwareIds = []
      for (let i in this.softwareTypeList) {
        let obj = {}
        obj.name = this.softwareTypeList[i].name
        softwareValue.push(this.softwareTypeList[i].name)
        softwareIds.push(this.softwareTypeList[i].softName)
        obj.softName = this.softwareTypeList[i].softName
        arr.push(obj)
      }
      for (let i = 0; i < softwareValue.length; i++) {
        for (let j = 0; j < this.softwareTypes.length; j++) {
          if (softwareValue[i] == this.softwareTypes[j].value) {
            softwareTypeKey.push(this.softwareTypes[j].typeKey)
          }
        }
      }
      let params = {
        taskId: this.taskId, //任务id
        processInstanceId: this.processInstanceId, //工作流流程实例id
        currentUserId: userId, //申请用户id
        currentUserName: this.name, //用户名称
        applyType: this.radio, //申请类型
        projectName: this.projectName, //projectName
        applyTypeValue: str, //申请类型值
        ids: "", //类型选择后的组织单位id或外协用户的ids
        names: "", //类型选择后的组织单位name或外协用户的names
        startTime: this.value1, //开始时间
        endTime: this.value2, //结束时间
        capacity: this.state, //存储容量
        remark: this.tips, //备注信息
        softwareTypeKey: softwareTypeKey.join(","), //软件类型key(多个类型用英文逗号分隔)
        softwareValue: softwareValue.join(","), //软件类型value(多个类型用英文逗号分隔)
        softwareIds: softwareIds.join(","),
        updateTime: "", //操作时间(每次修改状态的时间)
        reviewProgress: this.reviewProgress, //审核进度(1提交审批, 2二级审批通过, 3一级审批通过, 4资源授权成功)
        reviewProgressValue: this.reviewProgressValue, //审核进度值
        state: this.paramsState, //审核状态(1审核通过, 2审核未通过, 3审核中, 4已催办)
        stateValue: this.paramsStateValue, //审核状态值
        ignore: this.commitNum,
        foreignOrgId: this.foreignOrgId
      }
      let startTime = new Date(params.startTime)
      let endTime = new Date(params.endTime)
      params.startTime = this.dateFtt("yyyy-MM-dd", startTime)
      // params.endTime=this.dateFtt("yyyy-MM-dd hh:mm:ss",endTime)
      params.endTime = this.dateFtt("yyyy-MM-dd", endTime)
      if (new Date(params.startTime.replace(/-/g, "/")).getTime() > new Date(params.endTime.replace(/-/g, "/")).getTime()) {
        layer.msg("结束时间不能小于开始时间", {
          icon: 2,
          closeBtn: 1
        })
        return
      }
      if (this.radio == "2") {
        params.ids = this.branchID
        params.names = this.branchName
      } else if (this.radio == "3") {
        params.ids = JSON.parse(JSON.stringify(this.otherTreeIDs)).join(",")
        params.names = JSON.parse(JSON.stringify(this.otherTreeNames)).join(",")
      }
      let selfUrl = ""
      if (this.isEdit) {
        selfUrl = "updateApply"
      } else {
        selfUrl = "submitApproveData"
      }
      this.firstSubmit = true
      $.ajax({
        url: url + ":9001/examineAndApproveResource/" + selfUrl,
        type: "post",
        async: false,
        data: params,
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data == true) {
            _that.$message({
              message: "提交成功",
              type: "success"
            })
            _that.commitNum = 1
            _that.showDialog = false
            _that.init()
          } else if (data.data == "修改成功") {
            _that.$message({
              message: "修改成功",
              type: "success"
            })
            _that.showDialog = false
            _that.commitNum = 1
            _that.init()
          } else {
            if (data.message.indexOf("是否继续") != -1) {
              _that
                .$confirm(data.message, {
                  confirmButtonText: "是",
                  cancelButtonText: "否",
                  type: "warning"
                })
                .then(() => {
                  _that.commitNum = 2
                  _that.application()
                })
                .catch(() => {
                  _that.commitNum = 3
                  _that.application()
                })
            } else {
              _that.commitNum = 1
              _that.$message({
                message: data.message,
                type: "error"
              })
            }
          }
          setTimeout(function() {
            _that.firstSubmit = false
          }, 1000)
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    buW(s) {
      return s < 10 ? "0" + s : s
    },
    dateFtt(fmt, date) {
      //author: meizz
      var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        S: date.getMilliseconds() //毫秒
      }
      if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length))
      for (var k in o) if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length))
      return fmt
    },
    tab() {
      //申请类型切换
      if (this.radio === 0) {
        this.typeList = false
        this.typeList2 = false
        this.typeList3 = false
      } else if (this.radio == 1) {
        this.typeList = true
        this.typeList2 = false
        this.typeList3 = false
      } else if (this.radio == 2) {
        this.typeList = false
        this.typeList2 = true
        this.typeList3 = false
      } else if (this.radio == 3) {
        this.typeList = false
        this.typeList2 = false
        this.typeList3 = true
      }
    },
    getCookie: function(name) {
      var cookie = document.cookie
      var arr = cookie.split("; ") //将字符串分割成数组
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=")
        if (arr1[0] == name) {
          return unescape(arr1[1])
        }
      }
      return "GG"
    },
    checkChange() {
      // let nodeNameArr = []
      // let nodeKeyArr = [];
      // console.log(this.$refs.otherTree)
      // this.$refs.otherTree.getCheckedNodes().map(item => {
      //     if (!item.children) {
      //         nodeNameArr.push(item.name)
      //         nodeKeyArr.push(item.id)
      //     }
      // });
      // this.wPeople=nodeKeyArr.length
      // this.otherTreeIDs=nodeKeyArr
      // this.otherTreeNames=nodeNameArr
      // let nodeStr=JSON.parse(JSON.stringify(nodeNameArr))
      // this.otherNamesStr=nodeStr.join(',')
    },
    handleCheckChange(data, checked, indeterminate) {
      let nodeNameArr = []
      let nodeKeyArr = []
      let isHasNoPeopleUnit = []
      let allCheckID = ""
      this.$refs.otherTree.getCheckedNodes().map(item => {
        isHasNoPeopleUnit.push(item.id)
        if (!item.children) {
          nodeNameArr.push(item.name)
          nodeKeyArr.push(item.id)
        }
      })
      this.wPeople = nodeKeyArr.length
      this.otherTreeIDs = nodeKeyArr
      this.otherTreeNames = nodeNameArr
      let nodeStr = JSON.parse(JSON.stringify(nodeNameArr))
      this.otherNamesStr = nodeStr.join(",")
      if (this.$refs.otherTree.getHalfCheckedKeys().length == 0) {
        this.$refs.otherTree.getCheckedNodes().map(item => {
          if (item.children) {
            allCheckID = item.id
            this.otherNamesStr = item.name
          }
        })
      }
      this.foreignOrgId = allCheckID
      if (data.children && nodeNameArr.length != 0) {
        this.owndadnode(data.id)
      } else if (nodeNameArr.length != 0) {
        if (this.$refs.otherTree.getHalfCheckedKeys().length == 1) {
          this.owndadnode(this.$refs.otherTree.getHalfCheckedKeys()[0])
        }
      } else if (nodeNameArr.length == 0 && isHasNoPeopleUnit.length == 0) {
        this.resetDao()
      } else if (nodeNameArr.length == 0 && isHasNoPeopleUnit.length == 1) {
        console.log(isHasNoPeopleUnit)
        this.owndadnode(isHasNoPeopleUnit[0])
      }
      // let nodeNameArr = []
      // let nodeKeyArr = [];
      // console.log(this.$refs.otherTree)
      // this.$refs.otherTree.getCheckedNodes().map(item => {
      //     if (!item.children) {
      //         nodeNameArr.push(item.name)
      //         nodeKeyArr.push(item.id)
      //     }
      // });
      // this.otherTreeIDs=nodeKeyArr
      // this.otherTreeNames=nodeNameArr
      // let nodeStr=JSON.parse(JSON.stringify(nodeNameArr))
      // this.otherNamesStr=nodeStr.join(',')
    },
    checkOwn() {
      let nodeNameArr = []
      let nodeKeyArr = []
      this.$refs.myOwnTree.getCheckedNodes().map(item => {
        if (!item.children || item.children == 0) {
          nodeNameArr.push(item.name)
          nodeKeyArr.push(item.id)
        }
      })
      console.log(this.$refs.myOwnTree.getHalfCheckedKeys())
      this.ownTreeIDs = nodeKeyArr
      // this.$set(this,'ownTreeIDs',nodeKeyArr)
      this.ownTreeNames = nodeNameArr
      let nodeStr = JSON.parse(JSON.stringify(nodeNameArr))
      this.ownNamesStr = nodeStr.join(",")
    },
    handleOwnChange() {
      // let nodeNameArr = []
      // let nodeKeyArr = [];
      // console.log(this.$refs.myOwnTree.getCheckedNodes(),111111)
      // this.$refs.myOwnTree.getCheckedNodes().map(item => {
      //     if (!item.children || item.children==0) {
      //         nodeNameArr.push(item.name)
      //         nodeKeyArr.push(item.id)
      //     }
      // });
      // this.ownTreeIDs=nodeKeyArr
      // // this.$set(this,'ownTreeIDs',nodeKeyArr)
      // this.ownTreeNames=nodeNameArr
      // let nodeStr=JSON.parse(JSON.stringify(nodeNameArr))
      // this.ownNamesStr=nodeStr.join(',')
    },
    getSoftWareList(params) {
      let _that = this
      for (let i = 0; i < params.length; i++) {
        $.ajax({
          url: url + ":9001/examineAndApproveResource/softwareList",
          type: "post",
          async: false,
          data: { typeKey: params[i].typeKey },
          headers: {
            token: _that.getCookie("token")
          },
          success: function(data) {
            _that.$set(_that.ownSelect, params[i].typeKey, data.data)
          },
          error: function(data) {
            console.log(data)
          }
        })
      }
      console.log(_that.ownSelect)
    },
    getPoewr() {
      let _that = this
      let userId = this.getCookie("userId") // 获取用户id
      $.ajax({
        url: url + ":9001/examineAndApproveResource/getUserInfo",
        type: "post",
        async: false,
        data: { currentUserId: userId },
        headers: {
          token: _that.getCookie("token")
        },
        success: function(data) {
          if (data.data.orgName == "外协") {
            _that.poewr = false
          } else {
            _that.poewr = true
          }
        },
        error: function(data) {
          console.log(data)
        }
      })
    }
  },
  computed: {
    //搜索过滤数组
    submitData: function() {
      //提交申请
      return this.tableData.filter(item => {
        return item.user.indexOf(this.submitSerch) !== -1
      })
    },
    adoptData: function() {
      //已通过
      return this.tableData1.filter(item => {
        return item.user.indexOf(this.adoptSerch) !== -1
      })
    },
    recordData: function() {
      //提交记录
      return this.tableData2.filter(item => {
        return item.user.indexOf(this.recordSerch) !== -1
      })
    }
  },
  mounted() {
    this.init()
    this.getMyPass()
    this.getMyRecord()
    this.getAddMessage()
    this.getOrgTreeAndName()
    this.getPoewr()
    this.getNum()
  }
})

// var a = document.getElementsByClassName('el-table_1_column_11')
// var b = Array.from(a)
// console.log(b)
// b[0].innerHTML = '操作'
