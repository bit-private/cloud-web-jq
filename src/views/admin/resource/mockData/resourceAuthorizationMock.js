
Mock.mock('/mock/table', 'post', {
    code: 0,
    msg: "",
    count: 1000,
    "data|15": [{
        /* 'name': '@cname', //中文名称
        'age|1-100': 100, //100以内随机整数
        'birthday': '@date("yyyy-MM-dd")', //日期
        'city': '@city(true)', //中国城市 */
        'id|1-100': 100,
        'userName': '@cname',
        'sex|1': ['男', '女'],
        'city': '@city(true)',
        'sign': '@string',
        'experience|1-1000': 1000,
        // 'score|1-100': 100,
        // 'classify|1': ['模特', '老师'],
        // 'wealth|1-10000': 10000,
        // 'time': '@date("yyyy-MM-dd HH:mm:ss")',
        // 'info': '@string',
    }]
});
