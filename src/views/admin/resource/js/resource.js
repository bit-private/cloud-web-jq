vm = new Vue({
  el: '#app',
  data: function() {
    return {
      newBuild: false,
      activeName: 'user',
      activeName1: 'group',
      trees: [],
      resTree: [],
      defaultProps: {
        label: 'name',
      },
      checkId: [],
      radio: '',
      hardware: {},
      // 硬件需要的参数
      systemName: '',
      linkName: '',
      applicationId:'',

      autoNode: false,
      search: '',
      date: '2099-01-01',
      tableData: [],
      dataInfo: [],
      department: '',
    }
  },
  mounted() {
    this.init()
  },
  methods: {
    // init
    init() {
      // this.getUserTree()
    },
    // 搜索
    handleSearch(val) {
      console.log(this)
      console.log(window)
      var _this = this
      $.ajax({
        url: url + ':9002/authorization/getApplicationList',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        data: {searchName: _this.search},
        success: function(data) {
          console.log(data)
          _this.tableData = data.data
        },
        error: function(err) {
          console.log(err)
        }
      })
    },
    // 删除搜索
    handleClearSearch() {
      this.search = ''
      this.handleSearch()
    },
    // 获取单节点
    getDNode(osType) {
      var _this = this
      $.ajax({
        url: url + ':9002/authorization/getHardwareTree',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        data: {osType: osType},
        success: function(data) {
          console.log(data)
          _this.resTree = data.data
        },
        error: function(err) {
          console.log(err)
        }
      })
    },
    // 获取资源组
    getHardwareGroupTree(osType, linkType,applicationId ) {
      var _this = this
      $.ajax({
        url: url + ':9002/authorization/getHardwareGroupTree',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        data: {osType: osType, linkType: linkType,applicationId :applicationId },
        success: function(data) {
          if(data.code==200){
            _this.resTree = data.data
          }else{
            layer.msg(data.message, {
              icon: 2,
              closeBtn:1
            });
            // _this.radio = ''
          }
        },
        error: function(err) {
          console.log(err)
        }
      })

    },
    //
    getcheckAppPanConfig(id) {
      var _this = this
      $.ajax({
        url: url + ':9006/paramConfig/checkAppPanConfig',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        data: {applicationId: id},
        success: function(data) {
          if(data.code==200){

          }else{
            layer.msg(data.message, {
              icon: 2,
              closeBtn:1
            });
            // _this.radio = ''
          }
        },
        error: function(err) {
          console.log(err)
        }
      })

    },
    // 选择软件
    handleSoftware(val) {
      if(this.checkId != '') {
        this.systemName = val.systemName
        this.linkName = val.linkName
        this.applicationId  = val.id
        console.log(val)
        this.getHardwareGroupTree(this.systemName, this.linkName,this.applicationId)
        console.log(val)
        this.getcheckAppPanConfig(val.id)
      }else {
        this.$message.error('请先选择用户或者部门!');
        this.radio = ''
      }
      console.log(this.systemName, this.linkName)
    },
    // 获取软件
    getApplicationList() {
      var _this = this
      $.ajax({
        url: url + ':9002/authorization/getApplicationList',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        success: function(data) {
          console.log(data)
          _this.tableData = data.data
        },
        error: function(err) {
          console.log(err)
        }
      })
    },
    // 选择部门节点
    handleNodeClick(data) {
      this.checkId = []
      if(data.flag >= 3) {
        this.checkId.push(data)
      }else {
        this.$message.error('只可以选择科级以下的部门!');
      }
      console.log(this.checkId)
    },
    // 获取用户
    handleNode(vcheckedNodes, checkedKeys) {
      this.checkId = []
      console.log(vcheckedNodes, checkedKeys)
      vcheckedNodes.pid == '0' ? this.department = vcheckedNodes.id : this.department = vcheckedNodes.pid
      // this.department = vcheckedNodes.name
      let res = this.$refs.tree.getCheckedNodes()
      console.log(res)
      res.forEach((item) => {
        if(item.flag == 2) {
          this.checkId.push(item)
        }
      })
      console.log(this.checkId)
    },
    // 获取部门树
    getDeparTree() {
      var _this = this
      $.ajax({
        url: url + ':9002/authorization/getOrgTree',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        success: function(data) {
          console.log(data)
          _this.trees = data.data
        },
        error: function(err) {
          console.log(err)
        }
      })
    },
    // 获取用户树
    getUserTree() {
      var _this = this
      $.ajax({
        url: url + ':9002/authorization/getOrgAndUserTree',
        type: 'post',
        headers: {
          "token": getCookie("token")
        },
        success: function(data) {
          console.log(data)
          _this.trees = data.data
        },
        error: function(err) {
          console.log(err)
        }
      })
    },
    // 选择类型
    handleDNode(data) {
      this.hardware = []
      this.hardware = data
    },
    // 选择资源组
    handleGroup(data) {
      console.log(data)
      this.hardware = []
      this.hardware = data
    },
    // 切换硬件标签
    handleClickTab(tab) {
      console.log(tab)
      this.hardware = []
      tab.name == 'group' ? this.getHardwareGroupTree(this.systemName, this.linkName,this.applicationId) : this.getDNode(this.systemName)
    },
    // 标签页切换
    handleClick(tab, event) {
      console.log(this.checkId)
      this.checkId = []
      tab.name == 'user' ? this.getUserTree() : this.getDeparTree()
    },
    // 关闭dialog
    handleClose() {
      this.systemName = ''
      this.linkName = ''
      this.applicationId = ''
      this.trees = []
      this.resTree = []
      this.checkId = []
      this.dataInfo = []
      this.radio = ''
      this.hardware = {}
      this.search = ''
      this.activeName = 'user',
      this.activeName1 = 'group',
      this.newBuild = false
      // this.dataInfo = []
      // this.checkId = []
      // this.radio = ''
    },
    saveBtn() {
      if(this.radio.id=='' || this.radio.id==undefined){
        this.$message.error('请选择应用软件');
        return
      }
      if(this.hardware.id=='' || this.hardware.id==undefined){
        this.$message.error('请选择硬件资源');
        return
      }
      layer.load(0, {shade: [0.1]})
      var _this = this
      var userInfo = [...new Set(this.checkId)]
      this.dataInfo = []
      console.log(userInfo)
      console.log(this.radio.id==undefined)
      userInfo.forEach((item) => {
        var obj = {
          applicationId: this.radio.id,
          authDate: this.date,
          authType: this.activeName,
          department: this.department,
          groupId: this.hardware.id,
          hardwareId: this.hardware.children ? this.hardware.id : this.hardware.id,
          hardwareType: this.hardware.children ? this.activeName1 : 'single',
          linkType: this.radio.linkName,
          userId: "",
          userName: ""
        }
        obj.userName = item.name
        obj.userId = item.id
        // obj.department = item.name
        this.dataInfo.push(obj)
      })
      $.ajax({
        url: url + ':9002/authorization/saveAuthorizations',
        type: 'post',
        contentType: "application/json;charset=UTF-8",
        headers: {
          token: this.getCookie('token')
        },
        data: JSON.stringify(this.dataInfo),
        success: function(data) {
          layer.close(layer.load(0, {shade: [0.1]}))
          _this.$message(data.message);
          if(data.code == 200) {
            _this.checkId = []
            _this.handleClose()
            window.reTable.tableList1();
            window.reTable.getTableListPage1();
          }
        },
        error: function(err) {
          console.log(err)
        }
      })
    },
    getCookie(name) {
      var cookie = document.cookie;
      // console.log(cookie);
      var arr = cookie.split("; "); //将字符串分割成数组
      // console.log(arr);
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=");
        if (arr1[0] == name) {
          return unescape(arr1[1]);
        }
      }
      return "GG"
    }
  }
})
