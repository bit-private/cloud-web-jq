;(function() {
  // console.log(url);
  const baseURL_SON1 = url + ":9001/HardwareResource"
  const baseURL_SON2 = url + ":9001/HardwareGroupResource"

  //组数据标识
  //新建组变量
  var tableListData = []
  var newTableListData = []
  var newTableListData_SON = []
  var checkedTrue = []
  //table显示数据条数
  var tableDateNum = 0
  //编辑组变量
  var editTableListData = []
  var editNewTableListData = []
  var editNewTableListData_SON = []
  var editCheckedTrue = []
  var editExistingData
  //搜索
  var where
  var where1
  var editdkof = []
  //是否是搜索
  var ifSearch = false
  //状态提示弹框
  var layerTipFail
  var layerTipSuccess
  //类型table联动变量
  //类型变量
  var typeOneVariable = ""
  var typeTwoVariable = ""
  //编辑id
  var editId = ""
  //标识编辑联动
  var flagFirst = true

  //判断id输入框
  $(".rgb input:first").focus()
  $(".rgb input").keyup(function() {
    var input = $(".rgb input")
    if ($(this).val().length == $(this).attr("maxlength")) {
      input.eq(input.index($(this)) + 1).focus()
    }
  })
  $("#icon-footer3").hide()
  $("#icon-footer4").hide()
  //JavaScript代码区域
  layui.use(["element", "laydate", "tree", "table", "layer", "form", "laypage", "upload"], function() {
    var element = layui.element,
      laydate = layui.laydate,
      table = layui.table,
      layer = layui.layer,
      laypage = layui.laypage,
      upload = layui.upload,
      tree = layui.tree,
      form = layui.form

    var active = {
      //初始化
      init: function() {
        this.tableList1()
        this.tableList2()
        this.getTableListPage1()
        this.getTableListPage2()

        // this.tableList3();
        // this.tableList4();
      },
      //第一个表格1
      tableList1: function(pageNum, where, mode, sortString) {
        $.ajax({
          url: baseURL_SON1 + "/page",
          // url:"http://10.1.3.150:9001/HardwareResource/page",
          type: "post",
          async: true,
          data: { page: pageNum == undefined ? 1 : pageNum, size: 10, name: where == undefined ? "" : where, mode: mode == undefined ? "" : mode, sortString: sortString == undefined ? "" : sortString },
          success: function(data) {
            // console.log(JSON.stringify(data))
            if (data.code == 200) {
              table.render({
                elem: "#hardwareList",
                cols: [
                  [
                    { type: "checkbox" },
                    { field: "usingState", title: "状态", templet: "#barDemo1" },
                    { field: "NAME", title: "名称", sort: "true", style: "cursor:pointer", event: "demo", templet: "#mouseHov1" },
                    { field: "intranetIP", title: "IP（内网）" },
                    { field: "extranetIP", title: "IP（外网）" },
                    { field: "extranetMACIP", title: "MAC地址（外网）" },
                    { field: "systemName", title: "系统" },
                    { field: "typeName", title: "主机类型" },
                    //修改
                    { field: "desktopType", title: "桌面类型" },
                    { field: "realName", title: "设备管理员" },
                    { field: "descri", title: "描述" },
                    { field: "", title: "操作", align: "center", toolbar: "#barDemo" }
                  ]
                ],
                data: data.data.content
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //第二个表格2
      tableList2: function(pageNum, where, mode, sortString) {
        $.ajax({
          url: baseURL_SON2 + "/page",
          // url:"http://10.1.3.150:9001/HardwareGroupResource/page",
          type: "post",
          async: true,
          data: { page: pageNum == undefined ? 1 : pageNum, size: 10, name: where1 == undefined ? "" : where1, mode: mode == undefined ? "" : mode, sortString: sortString == undefined ? "" : sortString },
          success: function(data) {
            // console.log(JSON.stringify(data));
            if (data.code == 200) {
              table.render({
                elem: "#hardwareGroupList",
                cols: [
                  [
                    { type: "checkbox" },
                    { field: "groupName", title: "组名", sort: "true", event: "demo1", style: "cursor:pointer", templet: "#mouseHov2" },
                    { field: "value", title: "类型" },
                    { field: "systemName", title: "系统" },
                    { field: "descri", title: "描述" },
                    { field: "softwareName", title: "应用软件" },
                    { field: "", title: "操作", align: "center", toolbar: "#xiangqing" }
                  ]
                ],
                data: data.data.content
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //第三个表格3
      tableList3: function(data) {
        // console.log(JSON.stringify(data));
        table.render({
          elem: "#addGroup",
          cols: [
            [
              { type: "checkbox" },
              { field: "name", width: 80, title: "名称", sort: true },
              { field: "extranetip", width: 100, title: "IP（外网）" },
              { field: "systemName", width: 100, title: "系统" },
              { field: "typeName", width: 120, title: "类型", sort: true },
              { field: "groupName", title: "归属组", width: 120, templet: "#barDemo2" }
            ]
          ],
          data: data,
          limit: tableDateNum
        })

        // if(data.length==0){
        //     $('#old').hide()
        // }else{
        //     $('#old').show()
        // }
        // form.render();
      },
      tableList1w: function(data) {
        // console.log(JSON.stringify(data));
        // table.render({
        //   elem: '#demoiii'
        //   , cols: [[
        //     { type: 'checkbox' }
        //     , { field: 'name', width: 80, title: '名称', sort: true }
        //     , { field: 'extranetip', width: 100, title: 'IP（外网）' }
        //     , { field: 'systemName', width: 100, title: '系统' }
        //     , { field: 'typeName', width: 120, title: '类型', sort: true }
        //     , { field: 'groupName', title: '归属组', width: 120, templet: '#barDemo2' }
        //   ]]
        //   , data: data
        //   , limit: tableDateNum
        // });
        // table.render({
        //         elem: '#demoiii'
        //         , cols: [[
        //           , { field: 'name', title: '名称', sort: 'true', style: 'cursor:pointer'}
        //           , { field: 'extranetip', title: 'IP（内网）' }
        //           , { field: 'extranetip', title: 'MAC地址（外网）' }
        //           , { field: 'systemName', title: '系统' }
        //           , { field: 'typeName', title: '桌面类型' }
        //         ]]
        //         , data: data
        //       });
        // if(data.length==0){
        //     $('#old').hide()
        // }else{
        //     $('#old').show()
        // }
        // form.render();
      },
      //第四个表格4
      tableList4: function(data) {
        // console.log(JSON.stringify(data));
        table.render({
          elem: "#newAddGroup",
          cols: [
            [
              { type: "checkbox", LAY_CHECKED: true },
              { field: "name", width: 80, title: "名称", sort: true },
              { field: "extranetip", width: 100, title: "IP（外网）" },
              { field: "systemName", width: 100, title: "系统" },
              { field: "typeName", width: 120, title: "类型", sort: true },
              { field: "groupName", title: "归属组", width: 120, templet: "#barDemo2" }
            ]
          ],
          data: data,
          limit: tableDateNum
        })
        checkedTrue = []
        for (var i = 0; i < data.length; i++) {
          checkedTrue.push(data[i].id)
        }
        // console.log(checkedTrue);
        // if(data.length==0){
        //     $('#new').hide()
        // }else{
        //     $('#new').show()
        // }
      },
      // //第五个表格5
      tableList5: function(data) {
        table.render({
          elem: "#editGroup",
          cols: [
            [
              { type: "checkbox" },
              { field: "name", width: 80, title: "名称", sort: true },
              { field: "extranetip", width: 100, title: "IP（外网）" },
              { field: "systemName", width: 100, title: "系统" },
              { field: "typeName", width: 120, title: "类型", sort: true },
              { field: "groupName", title: "归属组", width: 120, templet: "#barDemo2" }
            ]
          ],
          data: data,
          limit: tableDateNum
        })

        // if(data.length==0){
        //     $('#editold').hide()
        // }else{
        //     $('#editold').show()
        // }
      },
      // //第六个表格6
      tableList6: function(data) {
        table.render({
          elem: "#editAddGroup",
          cols: [
            [
              { type: "checkbox", LAY_CHECKED: true },
              { field: "name", width: 80, title: "名称", sort: true },
              { field: "extranetip", width: 100, title: "IP（外网）" },
              { field: "systemName", width: 100, title: "系统" },
              { field: "typeName", width: 120, title: "类型", sort: true },
              { field: "groupName", title: "归属组", width: 120, templet: "#barDemo2" }
            ]
          ],
          data: data,
          limit: tableDateNum
        })
        editCheckedTrue = []
        for (var i = 0; i < data.length; i++) {
          editCheckedTrue.push(data[i].id)
        }
        // console.log(editCheckedTrue);
        // if(data.length==0){
        //     $('#editnew').hide()
        // }else{
        //     $('#editnew').show()
        // }
      },
      //新增设备弹框
      newBuildEquipment: function() {
        layer.closeAll("loading")
        //示范一个公告层
        var index = layer.open({
          type: 1,
          title: ["新增设备", "font-size:20px;font-weight:900"], //不显示标题栏
          closeBtn: 1,
          area: ["800px", "700px"],
          shade: 0.8,
          id: "LAY_layuipro", //设定一个id，防止重复弹出
          // ,resize: false
          // ,btn: ['确定']
          btnAlign: "c",
          moveType: 1, //拖拽模式，0或者1
          content: $(".new-equipment"),
          success: function(layero) {
            //取消按钮
            $(".cancel").on("click", function() {
              //关闭所有弹框
              layer.close(index)
            })
            // $("").on('click',function () {
            //
            // })
          },
          end: function(index, layero) {
            //清空弹框数据
            active.clearData()
            $(".new-equipment").hide()
          }
        })
      },
      //新增设备提交
      newBuildEquipmentSub: function(obj) {
        // console.log(JSON.stringify(obj.field))
        $.ajax({
          url: baseURL_SON1 + "/saveHardware",
          // url:"http://10.1.3.150:9001/HardwareResource/saveHardware",
          type: "post",
          async: true,
          data: obj.field,
          success: function(data) {
            layer.closeAll("loading") //关闭加载层
            // console.log(JSON.stringify(data));
            if (data.code == 200) {
              if (data.data > 0) {
                layer.closeAll() //关闭所有层
                layer.msg(data.message, {
                  icon: 1,
                  // time: 2000, //20s后自动关闭
                  // btn: ['确定'],
                  closeBtn: 1
                })
                active.tableList1()
                active.getTableListPage1()
              } else {
                layer.msg(data.message, {
                  icon: 2,
                  closeBtn: 1
                })
              }
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading") //关闭加载层
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },

      //获取硬件添加页面数据
      getHardWareAddData: function() {
        var that = this
        $.ajax({
          url: baseURL_SON1 + "/addDataShow",
          type: "post",
          async: true,
          success: function(data) {
            // console.log(JSON.stringify(data))
            if (data.code == 200) {
              var optionString = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              if (data.data.typeList) {
                //判断
                for (let i = 0; i < data.data.typeList.length; i++) {
                  //遍历，动态赋值
                  optionString += '<option grade="' + data.data.typeList[i].id + '" value="' + data.data.typeList[i].typeKey + '" parentId="' + data.data.typeList[i].parentId + '"'
                  optionString += ">" + data.data.typeList[i].value + "</option>" //动态添加数据
                }
                $(".new-equipment select[name=hardwareTypeId]").empty() //清空子节点
                $(".new-equipment select[name=hardwareTypeId]").append(optionString) // 添加子节点
              }

              // var osTypeKeyString = "<option grade=\'请选择类型\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
              // if(data.data.osTypeList){                   //判断
              //     for(let i=0; i<data.data.osTypeList.length; i++){ //遍历，动态赋值
              //         osTypeKeyString +="<option grade=\""+data.data.osTypeList[i].id+"\" value=\""+data.data.osTypeList[i].typeKey+"\" parentId=\""+data.data.osTypeList[i].parentId+"\"";
              //         osTypeKeyString += ">"+data.data.osTypeList[i].value+"</option>";  //动态添加数据
              //     }
              //     $('.new-equipment select[name=osTypeKey]').empty();//清空子节点
              //     $(".new-equipment select[name=osTypeKey]").append(osTypeKeyString);  // 添加子节点
              // }

              var optionBrandAssets = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              if (data.data.brandList) {
                for (let i = 0; i < data.data.brandList.length; i++) {
                  //遍历，动态赋值
                  optionBrandAssets += '<option grade="' + data.data.brandList[i].id + '" value="' + data.data.brandList[i].typeKey + '" parentId="' + data.data.brandList[i].parentId + '"'
                  optionBrandAssets += ">" + data.data.brandList[i].value + "</option>" //动态添加数据
                }
                $(".new-equipment select[name=brandAssets]").empty() //清空子节点
                $(".new-equipment select[name=brandAssets]").append(optionBrandAssets) // 添加子节点
              }

              var administrator = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              if (data.data.administratorList) {
                //判断
                for (let i = 0; i < data.data.administratorList.length; i++) {
                  //遍历，动态赋值
                  administrator += '<option grade="' + data.data.administratorList[i].id + '" value="' + data.data.administratorList[i].id + '" parentId="' + data.data.administratorList[i].id + '"'
                  administrator += ">" + data.data.administratorList[i].realName + "</option>" //动态添加数据
                }
                $(".new-equipment select[name=administrator]").empty() //清空子节点
                $(".new-equipment select[name=administrator]").append(administrator) // 添加子节点
              }

              $("#classtree").empty() //清空子节点
              tree({
                elem: "#classtree",
                nodes: data.data.orgList,
                click: function(node) {
                  console.log(node)
                  var $select = $($(this)[0].elem).parents(".layui-form-select")
                  $select
                    .removeClass("layui-form-selected")
                    .find(".layui-select-title span")
                    .html(node.name)
                    .end()
                    .find("input:hidden[name='assetPropertyRightUnit']")
                    .val(node.id)
                }
              })
              form.render("select", "newEquipment")
              // that.newBuildEquipment()
              active.newBuildEquipment()
            }
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //编辑设备弹框
      editBuildEquipment: function() {
        layer.closeAll("loading")
        laydate.render({
          elem: "#editassetPurchaseDate",
          type: "datetime"
        })
        //示范一个公告层
        var index = layer.open({
          type: 1,
          title: ["编辑设备", "font-size:20px;font-weight:900"], //不显示标题栏
          closeBtn: 1,
          area: ["800px", "700px"],
          shade: 0.8,
          id: "LAY_layuipro", //设定一个id，防止重复弹出
          // ,resize: false
          // ,btn: ['确定']
          btnAlign: "c",
          moveType: 1, //拖拽模式，0或者1
          content: $(".edit-equipment"),
          success: function(layero) {
            form.render()
            $(".cancel").on("click", function() {
              //关闭所有弹框
              layer.close(index)
            })
          },
          end: function(index, layero) {
            $(".edit-equipment").hide()
          }
        })
      },
      //编辑设备提交
      editBuildEquipmentSub: function(obj) {
        // console.log(JSON.stringify(obj.field));
        $.ajax({
          url: baseURL_SON1 + "/updateHardware",
          // url:"http://10.1.3.150:9001/HardwareResource/updateHardware",
          type: "post",
          async: true,
          // headers:{"Content-Type":"application/json;charset=UTF-8"},
          data: obj.field,
          success: function(data) {
            layer.closeAll("loading") //关闭加载层
            // console.log(JSON.stringify(data));
            if (data.code == 200) {
              if (data.data > 0) {
                layer.closeAll() //关闭所有层
                layer.msg(data.message, {
                  icon: 1,
                  // time: 2000, //20s后自动关闭
                  // btn: ['确定'],
                  closeBtn: 1
                })
                active.tableList1()
                active.getTableListPage1()
              } else {
                layer.msg(data.message, {
                  icon: 2,
                  closeBtn: 1
                })
              }
            }
            // console.log(JSON.stringify(data));
            else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
            // console.log(data.msg)
          },
          error: function(data) {
            layer.closeAll("loading") //关闭加载层
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //编辑设备数据回显
      updataRetrieval: function(id) {
        $.ajax({
          url: baseURL_SON1 + "/updateDataShow",
          // url:"http://10.1.3.150:9001/HardwareResource/updateDataShow",
          type: "post",
          async: true,
          data: { id },
          success: function(data) {
            // console.log(JSON.stringify(data.data));
            if (data.code == 200) {
              $(".edit-equipment input[name=id]").val(data.data.hardware.id)
              $(".edit-equipment input[name=name]").val(data.data.hardware.name)

              var optionString = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              for (var i = 0; i < data.data.typeList.length; i++) {
                //遍历，动态赋值
                optionString += '<option grade="' + data.data.typeList[i].id + '" value="' + data.data.typeList[i].typeKey + '" parentId="' + data.data.typeList[i].parentId + '"'
                optionString += ">" + data.data.typeList[i].value + "</option>" //动态添加数据
              }
              $(".edit-equipment select[name=hardwareTypeId]").empty() //清空子节点
              $(".edit-equipment select[name=hardwareTypeId]").append(optionString) // 添加子节点
              $(".edit-equipment select[name=hardwareTypeId]").val(data.data.hardware.hardwareTypeId)
              // var osTypeKeyString = "<option grade=\'请选择类型\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
              // if(data.data.osTypeList){                   //判断
              //     for(let i=0; i<data.data.osTypeList.length; i++){ //遍历，动态赋值
              //         osTypeKeyString +="<option grade=\""+data.data.osTypeList[i].id+"\" value=\""+data.data.osTypeList[i].typeKey+"\" parentId=\""+data.data.osTypeList[i].parentId+"\"";
              //         osTypeKeyString += ">"+data.data.osTypeList[i].value+"</option>";  //动态添加数据
              //     }
              //     $('.edit-equipment select[name=osTypeKey]').empty();//清空子节点
              //     $(".edit-equipment select[name=osTypeKey]").append(osTypeKeyString);  // 添加子节点
              // }
              var memberMangerString = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              for (let i = 0; i < data.data.administratorList.length; i++) {
                //遍历，动态赋值
                memberMangerString += '<option grade="' + data.data.administratorList[i].id + '" value="' + data.data.administratorList[i].id + '" parentId="' + data.data.administratorList[i].id + '"'
                memberMangerString += ">" + data.data.administratorList[i].realName + "</option>" //动态添加数据
              }
              $(".edit-equipment select[name=administrator]").empty() //清空子节点
              $(".edit-equipment select[name=administrator]").append(memberMangerString) // 添加子节点
              $(".edit-equipment select[name=administrator]").val(data.data.hardware.administrator)

              $(".edit-equipment input[name=extranetIP]").val(data.data.hardware.extranetIP)
              $(".edit-equipment input[name=intranetIP]").val(data.data.hardware.intranetIP)
              $(".edit-equipment input[name=extranetMACIP]").val(data.data.hardware.extranetMACIP)

              $(".edit-equipment select[name=osTypeKey]").val(data.data.hardware.osTypeKey)

              $(".edit-equipment textarea[name=descri]").val(data.data.hardware.descri)
              $(".edit-equipment input[name=systemName]").val(data.data.hardware.systemName)
              $(".edit-equipment input[name=modelCPU]").val(data.data.hardware.modelCPU)
              $(".edit-equipment input[name=numberCPU]").val(data.data.hardware.numberCPU)
              $(".edit-equipment input[name=memoryCapacity]").val(data.data.hardware.memoryCapacity)
              $(".edit-equipment input[name=hardDriveCapacity]").val(data.data.hardware.hardDriveCapacity)
              $(".edit-equipment input[name=figureCardType]").val(data.data.hardware.figureCardType)
              $(".edit-equipment input[name=figureCardNumber]").val(data.data.hardware.figureCardNumber)
              $(".edit-equipment input[name=desktopType]").val(data.data.hardware.desktopType)

              var optionBrandAssets = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              if (data.data.brandList) {
                for (let i = 0; i < data.data.brandList.length; i++) {
                  //遍历，动态赋值
                  optionBrandAssets += '<option grade="' + data.data.brandList[i].id + '" value="' + data.data.brandList[i].typeKey + '" parentId="' + data.data.brandList[i].parentId + '"'
                  optionBrandAssets += ">" + data.data.brandList[i].value + "</option>" //动态添加数据
                }
                $(".edit-equipment select[name=brandAssets]").empty() //清空子节点
                $(".edit-equipment select[name=brandAssets]").append(optionBrandAssets) // 添加子节点
              }

              $(".edit-equipment select[name=brandAssets]").val(data.data.hardware.brandAssets)
              $(".edit-equipment input[name=assetTypes]").val(data.data.hardware.assetTypes)
              $(".edit-equipment input[name=assetSerialNumber]").val(data.data.hardware.assetSerialNumber)
              $(".edit-equipment input[name=assetPurchaseDate]").val(data.data.hardware.assetPurchaseDate)
              $(".edit-equipment input[name=assetNumber]").val(data.data.hardware.assetNumber)
              $(".edit-equipment select[name=assetWarrantyDate]").val(data.data.hardware.assetWarrantyDate)
              $("#classtree1").empty() //清空子节点
              tree({
                elem: "#classtree1",
                nodes: data.data.orgList,
                click: function(node) {
                  console.log(node)
                  console.log($(this)[0].elem)
                  console.log($($(this)[0].elem).parents(".layui-form-select"))
                  var $select = $($(this)[0].elem).parents(".layui-form-select")
                  $select
                    .removeClass("layui-form-selected")
                    .find(".layui-select-title span")
                    .html(node.name)
                    .end()
                    .find("input:hidden[name='assetPropertyRightUnit']")
                    .val(node.id)
                }
              })
              if (data.data.hardware.orgName == "") {
                $("#classtree1")
                  .parents(".layui-form-select")
                  .removeClass("layui-form-selected")
                  .find(".layui-select-title span")
                  .html("选择单位")
                  .end()
                  .find("input:hidden[name='assetPropertyRightUnit']")
                  .val(data.data.hardware.assetPropertyRightUnit)
              } else {
                $("#classtree1")
                  .parents(".layui-form-select")
                  .removeClass("layui-form-selected")
                  .find(".layui-select-title span")
                  .html(data.data.hardware.orgName)
                  .end()
                  .find("input:hidden[name='assetPropertyRightUnit']")
                  .val(data.data.hardware.assetPropertyRightUnit)
              }

              // $('.edit-equipment input[name=assetPropertyRightUnit]').val(data.data.hardware.assetPropertyRightUnit);
              active.editBuildEquipment()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
            // console.log(JSON.stringify(data));
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //编辑设备验证外网IP
      scriptData: function(extranetIP, osTypeKey) {
        // console.log(extranetIP+"---------------------"+osTypeKey);
        // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
        $.ajax({
          url: baseURL_SON1 + "/scriptData",
          type: "post",
          async: true,
          data: { extranetIP, osTypeKey },
          success: function(data) {
            console.log(data,'scriptData')
            layer.closeAll("loading") //关闭加载层
            // console.log(JSON.stringify(data));
            if (data.code == 200) {
              $(".edit-equipment input[name=systemName]").val(data.data.system)
              $(".edit-equipment input[name=modelCPU]").val(data.data.cpuModel)
              $(".edit-equipment input[name=numberCPU]").val(data.data.cpuNumber)
              $(".edit-equipment input[name=memoryCapacity]").val(data.data.memory)
              $(".edit-equipment input[name=hardDriveCapacity]").val(data.data.diskSize)
              $(".edit-equipment input[name=figureCardType]").val(data.data.nvidiaType)
              $(".edit-equipment input[name=figureCardNumber]").val(data.data.nvidiaNumber)
              $(".edit-equipment input[name=desktopType]").val(data.data.desktopType)
              $(".edit-equipment input[name=name]").val(data.data.name)
              $(".edit-equipment input[name=extranetMACIP]").val(data.data.macAddress)
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
            // console.log(JSON.stringify(data));
          },
          error: function(data) {
            layer.closeAll("loading") //关闭加载层
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //新建组弹框
      newBuildGroup: function() {
        layer.closeAll("loading")
        formSelects.selects({
          name: "select2",
          el: "select[name=applicationId]",
          show: "#select-result2",
          model: "select",
          filter: "applicationId",
          init: [],
          reset: true
        })
        var index = layer.open({
          type: 1,
          title: ["新建组", "font-size:20px;font-weight:900"], //不显示标题栏
          closeBtn: 1,
          // ,area:['850px','700px']
          area: ["1270px", "800px"],
          shade: 0.8,
          id: "LAY_layuipro", //设定一个id，防止重复弹出
          // ,resize: false
          // ,btn: ['确定']
          btnAlign: "c",
          moveType: 1, //拖拽模式，0或者1
          content: $(".new-groups"),
          success: function(layero) {
            //取消按钮
            $(".cancel").on("click", function() {
              //关闭所有弹框
              layer.close(index)
            })
          },
          end: function(index, layero) {
            active.clearGroupData()
            tableListData = ""
            newTableListData = []
            newTableListData_SON = []
            // checkedTrue = [];
            var newArr = []
            active.tableList4(newArr)
            // $('#new').hide();
            $(".new-groups").hide()
            // console.log(tableListData);
            // console.log(newTableListData);
          }
        })
      },
      //新建组提交
      newAddGroup: function(obj) {
        var applicationId = []
        for (let i = 0; i < formSelects.array("select2").length; i++) {
          applicationId.push(formSelects.array("select2")[i].val)
        }
        obj.applicationId = applicationId.join(",")
        $.ajax({
          url: baseURL_SON2 + "/addHardwareGroup",
          // url:"http://10.1.3.150:9001/HardwareGroupResource/save",
          type: "post",
          async: true,
          // headers:{"Content-Type":"application/json;charset=UTF-8"},
          data: obj,
          success: function(data) {
            // layer.closeAll(); //关闭所有层
            layer.closeAll("loading") //关闭加载层
            console.log(JSON.stringify(data))
            if (data.code == 200) {
              // $('.new-groups input[name=groupName]').val('');
              // $('.new-groups select[name=hardwareGroupTypesId]').val('');
              // $('.new-groups textarea[name=descri]').val('');
              // //清空ip框
              // $('.new-groups .ip1').val('');
              // $('.new-groups .ip2').val('');
              // $('.new-groups .ip3').val('');
              // $('.new-groups .ip4').val('');
              // $('.new-groups .num').val('');

              layer.closeAll()
              // active.addDataShow();
              active.tableList2()
              active.getTableListPage2()
              layer.msg(data.message, {
                icon: 1,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading") //关闭加载层
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //新建组table数据显示
      addDataShow: function(conditions, typeKey, cb) {
        $.ajax({
          url: baseURL_SON2 + "/dataOfAddingThePage",
          // url:"http://10.1.3.150:9001/HardwareGroupResource/dataOfAddingThePage",
          type: "post",
          async: true,
          data: { conditions, typeKey },
          success: function(data) {
            // console.log(JSON.stringify(data))
            if (data.code == 200) {
              newTableListData = []
              active.tableList4(newTableListData)
              if (conditions) {
                // console.log(data.data);
                if (data.data.hardwareList != 0) {
                  newTableListData_SON = data.data.hardwareList
                  // console.log(newTableListData_SON)
                  active.tableList3(newTableListData_SON)
                  // var newArr = []
                  // active.tableList4(newArr);
                } else {
                  layer.msg(data.message, {
                    icon: 2,
                    colseBtn: 1
                  })
                }
              } else {
                tableListData = data.data.hardwareList
                tableDateNum = data.data.hardwareList.length
                // console.log(tableListData);
                active.tableList3(tableListData)
                var optionString = "<option grade='请选择软件分类系统'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
                if (data.data.typeList) {
                  //判断
                  for (var i = 0; i < data.data.typeList.length; i++) {
                    //遍历，动态赋值
                    optionString += '<option grade="' + data.data.typeList[i].id + '" value="' + data.data.typeList[i].typeKey + '"'
                    optionString += ">" + data.data.typeList[i].value + "</option>" //动态添加数据
                  }
                  $(".new-groups select[name=hardwareGroupTypesId]").empty() //清空子节点
                  $(".new-groups select[name=hardwareGroupTypesId]").append(optionString) // 添加子节点
                }

                // var optionTwoString = "<option grade=\'请选择\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                var optionTwoString = ""
                if (data.data.application) {
                  //判断
                  for (var i = 0; i < data.data.application.length; i++) {
                    //遍历，动态赋值
                    optionTwoString += '<option grade="' + data.data.application[i].id + '" value="' + data.data.application[i].id + '"'
                    optionTwoString += ">" + data.data.application[i].softwareName + "/" + data.data.application[i].softwareVersion + "</option>" //动态添加数据
                  }
                  $(".new-groups select[name=applicationId]").empty() //清空子节点
                  $(".new-groups select[name=applicationId]").append(optionTwoString) // 添加子节点
                }

                $(".new-groups select[name=hardwareGroupTypesId]").val(data.data.typeKey)
                console.log($(".new-groups select[name=applicationId]").val() == "")
                console.log($(".new-groups select[name=applicationId]").val())
                $(".new-groups select[name=applicationId]").val("")
                form.render()

                formSelects.selects({
                  name: "select2",
                  el: "select[name=applicationId]",
                  show: "#select-result2",
                  model: "select",
                  filter: "applicationId",
                  reset: true
                })
                $(".new-groups select[name=applicationId]").val("")
                console.log($(".new-groups select[name=applicationId]").val(""))
                if (cb) {
                  cb()
                }

                // var newArr = []
                // active.tableList4(newArr);
                // active.typeList(cb);
              }
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //编辑组弹框
      editBuildGroup: function() {
        layer.closeAll("loading")
        var index = layer.open({
          type: 1,
          title: ["编辑组", "font-size:20px;font-weight:900"], //不显示标题栏
          closeBtn: 1,
          area: ["1270px", "800px"],
          shade: 0.8,
          id: "LAY_layuipro", //设定一个id，防止重复弹出
          // ,resize: false
          // ,btn: ['确定']
          btnAlign: "c",
          moveType: 1, //拖拽模式，0或者1
          content: $(".edit-groups"),
          success: function(layero) {
            console.log(editdkof)
            formSelects.selects({
              name: "select3",
              el: "select[name=applicationIde]",
              show: "#select-result2",
              model: "select",
              filter: "applicationIde",
              init: editdkof,
              reset: true
            })
            //取消按钮
            $(".cancel").on("click", function() {
              //关闭所有弹框
              layer.close(index)
            })
            // form.render()
          },
          end: function(index, layero) {
            editTableListData = ""
            editNewTableListData = []
            ifSearch = false
            flagFirst = true
            $(".edit-groups").hide()
          }
        })
      },
      //编辑组提交
      update: function(obj) {
        console.log(obj)
        var applicationId = []
        for (let i = 0; i < formSelects.array("select3").length; i++) {
          applicationId.push(formSelects.array("select3")[i].val)
        }
        obj.applicationId = applicationId.join(",")
        $.ajax({
          url: baseURL_SON2 + "/updateHardwareGroup",
          // url:"http://10.1.3.150:9001/HardwareGroupResource/save",
          type: "post",
          async: true,
          // headers:{"Content-Type":"application/json;charset=UTF-8"},
          data: obj,
          success: function(data) {
            // layer.closeAll(); //关闭所有层
            console.log(JSON.stringify(data))
            layer.closeAll("loading") //关闭加载层
            if (data.code == 200) {
              // $('.new-groups input[name=id]').val('');
              // $('.new-groups input[name=groupName]').val('');
              // $('.new-groups select[name=hardwareGroupTypesId]').val('');
              // $('.new-groups textarea[name=descri]').val('');
              layer.closeAll()
              active.tableList2()
              active.getTableListPage2()
              layer.msg(data.message, {
                icon: 1,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
            // console.log(data.msg)
          },
          error: function(data) {
            layer.closeAll("loading") //关闭加载层
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //编辑硬件组数据显示
      updateDataShow: function(id, conditions, typeKey, cb) {
        $.ajax({
          url: baseURL_SON2 + "/updatePageData",
          // url:"http://10.1.3.150:9001/HardwareGroupResource/updatePageData",
          type: "post",
          async: true,
          data: { id, conditions, typeKey },
          success: function(data) {
            // console.log(JSON.stringify(data));
            if (data.code == 200) {
              var optionString = "<option grade='请选择类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              if (data.data.typeList) {
                //判断
                for (var i = 0; i < data.data.typeList.length; i++) {
                  //遍历，动态赋值
                  optionString += '<option grade="' + data.data.typeList[i].id + '" value="' + data.data.typeList[i].typeKey + '"'
                  optionString += ">" + data.data.typeList[i].value + "</option>" //动态添加数据
                }
                $(".edit-groups select[name=hardwareGroupTypesId]").empty() //清空子节点
                $(".edit-groups select[name=hardwareGroupTypesId]").append(optionString) // 添加子节点
              }

              // var optionTwoString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
              var optionTwoString = ""
              if (data.data.application) {
                //判断
                for (var i = 0; i < data.data.application.length; i++) {
                  //遍历，动态赋值
                  optionTwoString += '<option grade="' + data.data.application[i].id + '" value="' + data.data.application[i].id + '"'
                  optionTwoString += ">" + data.data.application[i].softwareName + "/" + data.data.application[i].softwareVersion + "</option>" //动态添加数据
                }
                $(".edit-groups select[name=applicationIde]").empty() //清空子节点
                $(".edit-groups select[name=applicationIde]").append(optionTwoString) // 添加子节点
              }

              $(".edit-groups input[name=groupId]").val(data.data.hardwardGroup.id)
              $(".edit-groups input[name=groupName]").val(data.data.hardwardGroup.groupName)
              if (flagFirst) {
                $(".edit-groups select[name=hardwareGroupTypesId]").val(data.data.hardwardGroup.hardwareGroupTypesId)
              } else {
                $(".edit-groups select[name=hardwareGroupTypesId]").val(data.data.typeKey)
              }

              editdkof = data.data.hardwardGroup.applicationId.split(",")
              $(".edit-groups textarea[name=descri]").val(data.data.hardwardGroup.descri)
              form.render()
              formSelects.selects({
                name: "select3",
                el: "select[name=applicationIde]",
                show: "#select-result2",
                model: "select",
                filter: "applicationIde",
                init: editdkof,
                reset: true
              })
              // if(data.data.hardwardGroup.addFlag==1){
              editExistingData = data.data.hardwardGroup.ids
              if (conditions) {
                for(var i = 0; i < editNewTableListData.length; i ++) {
                  for(var j = 0; j < data.data.hardwareList.length; j ++) {
                    if(editNewTableListData[i].id == data.data.hardwareList[j].id) {
                      data.data.hardwareList.splice(j, 1)
                      j --
                    }
                  }
                }
                // for (var i = 0; i < data.data.hardwareList.length; i++) {
                //   for (var j = 0; j < editNewTableListData.length; j++) {
                //     if (data.data.hardwareList[i].id == editNewTableListData[j].id) {
                //       data.data.hardwareList.splice(i, 1)
                      
                //     }
                //   }
                // }
                editNewTableListData_SON = data.data.hardwareList
                console.log(data.data.hardwareList)
                console.log(editNewTableListData)
                active.tableList5(editNewTableListData_SON)
                // var newArr = []
                // active.tableList6(newArr);
              } else {
                // console.log(editExistingData.length);
                editNewTableListData = []
                tableDateNum = data.data.hardwareList.length
                for (var i = 0; i < editExistingData.length; i++) {
                  for (var j = 0; j < data.data.hardwareList.length; j++) {
                    if (editExistingData[i] == data.data.hardwareList[j].id) {
                      editNewTableListData.push(data.data.hardwareList[j])
                      data.data.hardwareList.splice(j, 1)
                      j--
                      // break;
                    }
                  }
                }
                editTableListData = data.data.hardwareList
                // console.log(editTableListData)
                active.tableList5(editTableListData)
                active.tableList6(editNewTableListData)
                if (cb) {
                  cb()
                }
              }
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }

            // active.editBuildGroup();
          },
          error: function(data) {
            window.location.href = "../../error/error.html"
            console.log(data)
          }
        })
      },
      updateDataShow1: function(id, conditions, typeKey, cb) {
        console.log(id)
        $.ajax({
          url: baseURL_SON2 + "/updatePageData",
          // url:"http://10.1.3.98:9001/HardwareGroupResource/updatePageData",
          type: "post",
          async: true,
          data: { id, conditions, typeKey },
          success: function(data) {
            console.log(data,'ziyuanjiedian')
            // console.log(data.data);
            // console.log(data.data.hardwareList);
            if (data.code == 200) {
              let str = `${data.data.hardwardGroup.groupName}资源组节点服务器信息`
              // let arr = data.data.hardwareList
              $("#xiangqing2>p").html(str)
              // // tableList1w(arr)
              // let str1 = ''
              // for (let i = 0; i< arr.length; i++) {

              //   str1 +=  `<tr class='xqtr'>
              //       <td>${arr[i].name}</td>
              //       <td>${arr[i].extranetip}</td>
              //       <td>${arr[i].extranetip}</td>
              //       <td>${arr[i].systemName}</td>
              //       <td>${arr[i].systemName}</td>
              //   </tr>`
              //   }
              //   // console.log(str1)
              //   $('.xiangqingtable>tbody').html(str1)
              //   $('.xqtbody>.xqtr').on('click',function(){
              //     // console.log($(this))
              //     $(this).addClass('colors').siblings().removeClass('colors')
              //   })
            }
          },
          error: function(data) {
            window.location.href = "../../error/error.html"
            console.log(data)
          }
        })
        $.ajax({
          url: baseURL_SON2 + "/details",
          // url:"http://10.1.3.98:9001/HardwareGroupResource/details",
          type: "post",
          async: true,
          data: { id },
          success: function(data) {
            console.log(data)
            // console.log(data.data);
            if (data.code == 200) {
              let arr = data.data
              let str1 = ""
              for (let i = 0; i < arr.length; i++) {
                str1 += `<tr class='xqtr'>
                    <td>${arr[i].name}</td>
                    <td>${arr[i].extranetip}</td>
                    <td>${arr[i].extranetmacip}</td>
                    <td>${arr[i].systemName}</td>
                    <td>${arr[i].desktopType}</td>
                </tr>`
              }
              // console.log(str1)
              $(".xiangqingtable>tbody").html(str1)
              $(".xqtbody>.xqtr").on("click", function() {
                // console.log($(this))
                $(this)
                  .addClass("colors")
                  .siblings()
                  .removeClass("colors")
              })
            }
          },
          error: function(data) {
            window.location.href = "../../error/error.html"
            console.log(data)
          }
        })
      },
      // 删除硬件
      delHardware: function(ids) {
        $.ajax({
          url: baseURL_SON1 + "/deleteHardware",
          type: "post",
          async: true,
          data: { ids },
          success: function(data) {
            // that.treeList(data.data);
            console.log(JSON.stringify(data))
            layer.closeAll("loading")
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              active.tableList1()
              active.getTableListPage1()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            // window.location.href = "../../error/error.html";
            // console.log(data.msg)
          }
        })
      },
      //删除硬件组
      delHardwareGroup: function(ids) {
        $.ajax({
          url: baseURL_SON2 + "/deleteHardwareGroup",
          type: "post",
          async: true,
          data: { ids },
          success: function(data) {
            // that.treeList(data.data);
            console.log(JSON.stringify(data))
            layer.closeAll("loading")
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              active.tableList2()
              active.getTableListPage2()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            // window.location.href = "../../error/error.html";
            // console.log(data.msg)
          }
        })
      },
      //更新按钮
      updataHardware(ids) {
        $.ajax({
          url: baseURL_SON1 + "/renewalData",
          // url:"http://10.1.3.150:9001/HardwareResource/renewalData",
          type: "post",
          async: true,
          data: { ids },
          success: function(data) {
            // that.treeList(data.data);
            // console.log(JSON.stringify(data));
            layer.closeAll("loading")
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              active.tableList2()
              active.getTableListPage2()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            window.location.href = "../../error/error.html"
            // console.log(data.msg)
          }
        })
      },
      //获取表格1的页码
      getTableListPage1: function(where, mode, sortString) {
        $.ajax({
          url: baseURL_SON1 + "/page",
          // url:"http://10.1.3.150:9001/HardwareResource/page",
          type: "post",
          async: true,
          data: { page: 1, size: 10, name: where == undefined ? "" : where, mode: mode == undefined ? "" : mode, sortString: sortString == undefined ? "" : sortString },
          success: function(data) {
            if (data.code == 200) {
              laypage.render({
                elem: "hardwareListPage",
                count: data.data.pageCount,
                layout: ["prev", "page", "next", "skip"],
                limit: 10,
                jump: function(obj, first) {
                  //obj包含了当前分页的所有参数，比如：
                  // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                  // console.log(obj.limit); //得到每页显示的条数
                  // console.log(first);
                  //首次不执行
                  if (!first) {
                    active.tableList1(obj.curr, where, mode, sortString)
                    //do something
                  }
                }
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //获取表格2的页码
      getTableListPage2: function(where, mode, sortString) {
        $.ajax({
          url: baseURL_SON2 + "/page",
          // url:"http://10.1.3.150:9001/HardwareGroupResource/page",
          type: "post",
          async: true,
          data: { page: 1, size: 10, name: where1 == undefined ? "" : where1, mode: mode == undefined ? "" : mode, sortString: sortString == undefined ? "" : sortString },
          success: function(data) {
            if (data.code == 200) {
              laypage.render({
                elem: "hardwareGroupListPage",
                count: data.data.pageCount,
                layout: ["prev", "page", "next", "skip"],
                limit: 10,
                jump: function(obj, first) {
                  //obj包含了当前分页的所有参数，比如：
                  // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                  // console.log(obj.limit); //得到每页显示的条数
                  // console.log(first);
                  //首次不执行
                  if (!first) {
                    active.tableList2(obj.curr, where1, mode, sortString)
                    //do something
                  }
                }
              })
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //启用停用按钮
      startAndStop: function(id, usingState) {
        var that = this
        $.ajax({
          url: baseURL_SON1 + "/updateState",
          type: "post",
          async: true,
          data: { id, usingState },
          success: function(data) {
            // console.log(JSON.stringify(data));
            layer.closeAll("loading")
            if (data.code == 200) {
              if (data.data > 0) {
                that.tableList1()
                that.getTableListPage1()
              } else {
                layer.msg(data.message, {
                  icon: 2,
                  closeBtn: 1
                })
              }
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            // window.location.href = "../../error/error.html";
            console.log(data)
          }
        })
      },
      //清空新建设备框数据
      clearData: function() {
        $(".new-equipment input[name=name]").val("")
        $(".new-equipment input[name=extranetIP]").val("")
        $(".new-equipment input[name=intranetIP]").val("")
        $(".new-equipment input[name=extranetMACIP]").val("")
        $(".new-equipment select[name=osTypeKey]").val("")
        $(".new-equipment textarea[name=descri]").val("")
        $(".new-equipment select[name=brandAssets]").val("")
        $(".new-equipment input[name=assetTypes]").val("")
        $(".new-equipment input[name=assetSerialNumber]").val("")
        $(".new-equipment input[name=assetPurchaseDate]").val("")
        $(".new-equipment input[name=assetNumber]").val("")
        $(".new-equipment select[name=assetWarrantyDate]").val("")
        $(".new-equipment input[name=assetPropertyRightUnit]").val("")
        $("#classtree")
          .parents(".layui-form-select")
          .removeClass("layui-form-selected")
          .find(".layui-select-title span")
          .html("选择单位")
          .end()
          .find("input:hidden[name='assetPropertyRightUnit']")
          .val("")
      },
      //清空新建组弹框数据
      clearGroupData: function() {
        $(".new-groups input[name=groupName]").val("")
        $(".new-groups select[name=hardwareGroupTypesId]").val("")
        $(".new-groups textarea[name=descri]").val("")
        //清空ip框
        $(".new-groups .ip1").val("")
        $(".new-groups .ip2").val("")
        $(".new-groups .ip3").val("")
        $(".new-groups .ip4").val("")
        $(".new-groups .num").val("")
      },
      //时间前面加0
      add0: function(m) {
        return m < 10 ? "0" + m : m
      },
      //时间戳转换
      timeFormat: function(time) {
        var time = new Date(time)
        var year = time.getFullYear()
        var month = time.getMonth() + 1
        var date = time.getDate()
        var hours = time.getHours()
        var minutes = time.getMinutes()
        var seconds = time.getSeconds()
        return year + "-" + active.add0(month) + "-" + active.add0(date) + " " + active.add0(hours) + ":" + active.add0(minutes) + ":" + active.add0(seconds)
      }
    }
    active.init()
    //排序
    table.on("sort(demo)", function(obj) {
      //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
      console.log(obj.field) //当前排序的字段名
      console.log(obj.type) //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
      console.log(this) //当前排序的 th 对象

      active.tableList1(0, where, obj.type, obj.field)
      active.getTableListPage1(where, obj.type, obj.field)
      //尽管我们的 table 自带排序功能，但并没有请求服务端。
      //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
      // table.reload('testTable', { //testTable是表格容器id
      //     initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
      //     ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
      //         field: obj.field //排序字段
      //         ,order: obj.type //排序方式
      //     }
      // });
    })
    table.on("sort(demo1)", function(obj) {
      //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
      console.log(obj.field) //当前排序的字段名
      console.log(obj.type) //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
      console.log(this) //当前排序的 th 对象

      active.tableList2(0, where1, obj.type, obj.field)
      active.getTableListPage2(where1, obj.type, obj.field)
      //尽管我们的 table 自带排序功能，但并没有请求服务端。
      //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
      // table.reload('testTable', { //testTable是表格容器id
      //     initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
      //     ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
      //         field: obj.field //排序字段
      //         ,order: obj.type //排序方式
      //     }
      // });
    })
    form.verify({
      ip: function(value, item) {
        //value：表单的值、item：表单的DOM对象
        if (value) {
          console.log(value)
          if (!new RegExp("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$").test(value)) {
            return "请输入正确格式的ip"
          }
        }
      },
      mac: function(value, item) {
        if (value) {
          if (!new RegExp("^[A-Fa-f0-9][A-Fa-f0-9]-|:[A-Fa-f0-9][A-Fa-f0-9]-|:[A-Fa-f0-9][A-Fa-f0-9]-|:[A-Fa-f0-9][A-Fa-f0-9]-|:[A-Fa-f0-9][A-Fa-f0-9]-|:[A-Fa-f0-9][A-Fa-f0-9]$").test(value)) {
            return "请输入正确格式的mac,00:00:00:00:00:00或者00-00-00-00-00-00"
          }
        }
      }
    })
    //新增设备提交
    form.on("submit(newEquipment)", function(obj) {
      layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
      obj.field.orgName = $("#classtree")
        .parents(".layui-form-select")
        .removeClass("layui-form-selected")
        .find(".layui-select-title span")
        .html()
      if (obj.field.orgName == "选择单位") {
        obj.field.orgName = ""
      }
      active.newBuildEquipmentSub(obj)
      console.log(obj,'obj')
      return false
    })
    //编辑设备提交
    form.on("submit(editEquipment)", function(obj) {
      // console.log(typeof obj.field);
      // console.log(JSON.stringify(obj.field))
      // console.log($('#classtree1').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html());
      obj.field.orgName = $("#classtree1")
        .parents(".layui-form-select")
        .removeClass("layui-form-selected")
        .find(".layui-select-title span")
        .html()
      if (obj.field.orgName == "选择单位") {
        obj.field.orgName = ""
      }
      layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
      active.editBuildEquipmentSub(obj)
      // $.ajax({
      //     url:baseURL_SON1 + "/updateHardware",
      //     type:'post',
      //     async:true,
      //     // headers:{"Content-Type":"application/json;charset=UTF-8"},
      //     data:obj.field,
      //     success:function(data){
      //         layer.closeAll('loading'); //关闭加载层
      //         // console.log(JSON.stringify(data));
      //         if(data.code==200){
      //             if(data.data>0){
      //                 layer.closeAll(); //关闭所有层
      //                 layer.msg(data.msg, {
      //                     icon:1,
      //                     // time: 2000, //20s后自动关闭
      //                     // btn: ['确定'],
      //                     closeBtn:1
      //                 });
      //                 active.tableList1();
      //                 active.getTableListPage1();
      //             }
      //             else{
      //                 layer.msg(data.msg,{
      //                     icon:2,
      //                     closeBtn:1
      //                 })
      //             }
      //         }
      //         // console.log(JSON.stringify(data));
      //         else if(data.code==400){
      //             layer.msg(data.msg,{
      //                 icon:2,
      //                 closeBtn:1
      //             })
      //         }
      //         // console.log(data.msg)
      //     },
      //     error:function(data){
      //         layer.closeAll('loading'); //关闭加载层
      // window.location.href = "../../error/error.html";
      //         console.log(data)
      //     }
      //
      // })

      // layer.alert(JSON.stringify(obj.field), {
      //     title: '最终的提交信息'
      // })
      return false
    })
    //新增组提交
    form.on("submit(newGroup)", function(obj) {
      if ($(".new-groups .ip1").val() && $(".new-groups .ip2").val() && $(".new-groups .ip3").val() && $(".new-groups .ip4").val() && $(".new-groups .num").val()) {
        obj.field.startIP = $(".new-groups .ip1").val() + "." + $(".new-groups .ip2").val() + "." + $(".new-groups .ip3").val() + "." + $(".new-groups .ip4").val()
        obj.field.nodeNum = $(".new-groups .num").val()
        obj.field.addFlag = 2
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        console.log(obj.field, 22222)
        // var applicationId=[]
        // for(let i=0;i<formSelects.array('select1').length;i++){
        //   applicationId.push(formSelects.array('select1')[i].val);
        // }
        // console.log(applicationId)
        active.newAddGroup(obj.field)
      } else {
        if (checkedTrue.length == 0) {
          layer.msg("请添加硬件设备", {
            icon: 2,
            closeBtn: 1
          })
        } else {
          obj.field.hardwareIds = checkedTrue.toString()
          obj.field.addFlag = 1
          layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
          active.newAddGroup(obj.field)
        }
      }

      // layer.alert(JSON.stringify(obj.field), {
      //     title: '最终的提交信息'
      // })
      return false
    })
    //编辑组提交
    form.on("submit(editNewGroup)", function(obj) {
      obj.field.hardwareIds = editCheckedTrue.toString()
      obj.field.addFlag = 1
      layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
      active.update(obj.field)
      return false
    })
    //监听表格复选框选择
    //监听新建组
    table.on("checkbox(addGroup)", function(obj) {
      if (obj.type == "all") {
        let isAll = 0
        let isName = 0
        let isOnly = 0
        for (let i = 0; i < tableListData.length; i++) {
          if (tableListData[i].usingState == 2) {
            $("#old .layui-form-checked").click()
            layer.msg("设备列表中含有已停用设备", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
        }
        for (let i = 0; i < tableListData.length; i++) {
          if (tableListData[i].systemName.indexOf("Server") == -1) {
            isAll++
          }
          if (tableListData[i].systemName.indexOf("Server") != -1 && tableListData[i].groupName != "") {
            isName++
          }
        }
        for (let i = 0; i < newTableListData.length; i++) {
          if (newTableListData[i].systemName.indexOf("Server") != -1) {
            isOnly++
          }
        }
        if (isAll != tableListData.length || isName != 0 || (isOnly != 0 && isAll != 0)) {
          $("#old .layui-form-checked").click()
          layer.msg("WindowsServer禁止加入其它系统类型", {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        // console.log(obj.type);
        if (newTableListData_SON.length != 0) {
          for (var k = 0; k < newTableListData_SON.length; k++) {
            newTableListData.push(newTableListData_SON[k])
            for (var t = 0; t < tableListData.length; t++) {
              if (tableListData[t].id == newTableListData_SON[k].id) {
                tableListData.splice(t, 1)
                break
              }
            }
            newTableListData_SON.splice(k, 1)
            k--
          }
          active.tableList3(tableListData)
          active.tableList4(newTableListData)
        } else {
          for (var q = 0; q < tableListData.length; q++) {
            newTableListData.push(tableListData[q])
            tableListData.splice(q, 1)
            q--
          }
          active.tableList3(tableListData)
          active.tableList4(newTableListData)
        }
      } else if (obj.checked) {
        if (obj.data.usingState == 2) {
          $("#old .layui-form-checked").click()
          layer.msg("设备已停用", {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        for (let i = 0; i < newTableListData.length; i++) {
          if (newTableListData[i].systemName.indexOf("Server") != -1 && obj.data.systemName.indexOf("Server") == -1) {
            $("#old .layui-form-checked").click()
            layer.msg("其它系统类型禁止加入WindowsServer", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
        }
        for (let i = 0; i < newTableListData.length; i++) {
          if (newTableListData[i].systemName.indexOf("Server") == -1 && obj.data.systemName.indexOf("Server") != -1) {
            $("#old .layui-form-checked").click()
            layer.msg("WindowsServer禁止加入其它系统类型", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
        }
        if (obj.data.groupName != "" && obj.data.systemName.indexOf("Server") != -1) {
          $("#old .layui-form-checked").click()
          let meaaage = obj.data.name + "设备已存在归属组" + obj.data.groupName + "中，请更换添加设备"
          layer.msg(meaaage, {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        // checkedTrue.push(obj.data.id);
        if (newTableListData_SON.length != 0) {
          for (var w = 0; w < newTableListData_SON.length; w++) {
            if (obj.data.id == newTableListData_SON[w].id) {
              newTableListData.push(newTableListData_SON[w])
              for (var s = 0; s < tableListData.length; s++) {
                if (tableListData[s].id == newTableListData_SON[w].id) {
                  tableListData.splice(s, 1)
                  break
                }
              }
              newTableListData_SON.splice(w, 1)
              active.tableList3(newTableListData_SON)
              if (newTableListData_SON.length == 0) {
                active.tableList3(tableListData)
              }
              active.tableList4(newTableListData)
              break
            }
          }
        } else {
          for (var j = 0; j < tableListData.length; j++) {
            if (obj.data.id == tableListData[j].id) {
              newTableListData.push(tableListData[j])
              tableListData.splice(j, 1)
              active.tableList3(tableListData)
              active.tableList4(newTableListData)
              break
            }
          }
        }
      }
    })
    table.on("checkbox(newAddGroup)", function(obj) {
      if (obj.type == "all") {
        // console.log(obj.type);
        for (var i = 0; i < newTableListData.length; i++) {
          tableListData.push(newTableListData[i])
        }
        newTableListData = []
        active.tableList3(tableListData)
        active.tableList4(newTableListData)
      } else if (!obj.checked) {
        for (var j = 0; j < newTableListData.length; j++) {
          if (obj.data.id == newTableListData[j].id) {
            tableListData.push(newTableListData[j])
            newTableListData.splice(j, 1)
            active.tableList3(tableListData)
            active.tableList4(newTableListData)
            // form.render();
            break
          }
        }
      }
      // if(!)
    })
    //监听编辑组
    table.on("checkbox(editGroup)", function(obj) {
      if (obj.type == "all") {
        let isAll = 0
        let isName = 0
        let isOnly = 0
        for (let i = 0; i < editTableListData.length; i++) {
          if (editTableListData[i].systemName.indexOf("Server") == -1) {
            isAll++
          }
          if (editTableListData[i].systemName.indexOf("Server") != -1 && editTableListData[i].groupName != "") {
            isName++
          }
        }
        for (let i = 0; i < editNewTableListData.length; i++) {
          if (editNewTableListData[i].systemName.indexOf("Server") != -1) {
            isOnly++
          }
        }
        console.log(isOnly != 0 && isAll != 0)
        console.log(isOnly)
        console.log(isAll != 0)
        if (isAll != editTableListData.length || isName != 0 || (isOnly != 0 && isAll != 0)) {
          $("#editold .layui-form-checked").click()
          layer.msg("WindowsServer禁止加入其它系统类型", {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        for (let i = 0; i < tableListData.length; i++) {
          if (tableListData[i].usingState == 2) {
            $("#editold .layui-form-checked").click()
            layer.msg("设备列表中含有已停用设备", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
        }
        // console.log(obj.type);
        if (editNewTableListData_SON.length != 0) {
          for (var k = 0; k < editNewTableListData_SON.length; k++) {
            editNewTableListData.push(editNewTableListData_SON[k])
            for (var t = 0; t < editTableListData.length; t++) {
              if (editTableListData[t].id == editNewTableListData_SON[k].id) {
                editTableListData.splice(t, 1)
                break
              }
            }
            editNewTableListData_SON.splice(k, 1)
            k--
          }
          active.tableList5(editTableListData)
          active.tableList6(editNewTableListData)
        } else {
          // console.log(11111)
          for (var q = 0; q < editTableListData.length; q++) {
            editNewTableListData.push(editTableListData[q])
            editTableListData.splice(q, 1)
            q--
          }
          active.tableList5(editTableListData)
          active.tableList6(editNewTableListData)
        }
      } else if (obj.checked) {
        if (obj.data.usingState == 2) {
          $("#old .layui-form-checked").click()
          layer.msg("设备已停用", {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        for (let i = 0; i < editNewTableListData.length; i++) {
          if (editNewTableListData[i].systemName.indexOf("Server") != -1 && obj.data.systemName.indexOf("Server") == -1) {
            $("#editold .layui-form-checked").click()
            layer.msg("其它系统类型禁止加入WindowsServer", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
        }
        for (let i = 0; i < editNewTableListData.length; i++) {
          if (editNewTableListData[i].systemName.indexOf("Server") == -1 && obj.data.systemName.indexOf("Server") != -1) {
            $("#editold .layui-form-checked").click()
            layer.msg("WindowsServer禁止加入其它系统类型", {
              icon: 2,
              closeBtn: 1
            })
            return
          }
        }
        if (obj.data.groupName != "" && obj.data.systemName.indexOf("Server") != -1) {
          $("#editold .layui-form-checked").click()
          let meaaage = obj.data.name + "设备已存在归属组" + obj.data.groupName + "中，请更换添加设备"
          layer.msg(meaaage, {
            icon: 2,
            closeBtn: 1
          })
          return
        }
        // checkedTrue.push(obj.data.id);
        if (editNewTableListData_SON.length != 0) {
          for (var w = 0; w < editNewTableListData_SON.length; w++) {
            if (obj.data.id == editNewTableListData_SON[w].id) {
              editNewTableListData.push(editNewTableListData_SON[w])
              for (var s = 0; s < editTableListData.length; s++) {
                if (editTableListData[s].id == editNewTableListData_SON[w].id) {
                  editTableListData.splice(s, 1)
                  break
                }
              }
              editNewTableListData_SON.splice(w, 1)
              active.tableList5(editNewTableListData_SON)
              if (editNewTableListData_SON.length == 0) {
                active.tableList5(editTableListData)
              }
              active.tableList6(editNewTableListData)
              break
            }
          }
        } else {
          for (var j = 0; j < editTableListData.length; j++) {
            if (obj.data.id == editTableListData[j].id) {
              editNewTableListData.push(editTableListData[j])
              editTableListData.splice(j, 1)
              active.tableList5(editTableListData)
              active.tableList6(editNewTableListData)
              break
            }
          }
        }
      }
    })
    table.on("checkbox(editAddGroup)", function(obj) {
      if (obj.type == "all") {
        // console.log(obj.type);
        for (var i = 0; i < editNewTableListData.length; i++) {
          editTableListData.push(editNewTableListData[i])
        }
        editNewTableListData = []
        active.tableList5(editTableListData)
        active.tableList6(editNewTableListData)
      } else if (!obj.checked) {
        for (var j = 0; j < editNewTableListData.length; j++) {
          if (obj.data.id == editNewTableListData[j].id) {
            editTableListData.push(editNewTableListData[j])
            editNewTableListData.splice(j, 1)
            active.tableList5(editTableListData)
            active.tableList6(editNewTableListData)
            // form.render();
            break
          }
        }
      }
      // if(!)
    })
    //监听工具条
    table.on("tool(demo)", function(obj) {
      // var data = obj.data;
      if (obj.event === "start") {
        //启用
        // console.log(obj.data.usingState)
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.startAndStop(obj.data.id, obj.data.usingState)
      } else if (obj.event === "stop") {
        //停用
        // console.log(obj.data.id)
        // console.log(obj.data.usingState)
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.startAndStop(obj.data.id, obj.data.usingState)
      } else if (obj.event === "demo") {
        //编辑设备
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.updataRetrieval(obj.data.id)
      }
      // else if(obj.event==='demo1'){
      //     //编辑组
      //     layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
      //     active.updateDataShow(obj.data.id)
      //     // layer.open({
      //     //     type: 1
      //     //     ,title: "新建组" //不显示标题栏
      //     //     ,closeBtn: 1
      //     //     ,area:['800px','700px']
      //     //     ,shade: 0.8
      //     //     ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
      //     //     // ,resize: false
      //     //     // ,btn: ['确定']
      //     //     ,btnAlign: 'c'
      //     //     ,moveType: 1 //拖拽模式，0或者1
      //     //     ,content:$('.edit-groups')
      //     //     // ,success: function(layero){
      //     //     //     var btn = layero.find('.layui-layer-btn');
      //     //     //     // btn.find('.layui-layer-btn0').attr({
      //     //     //     //     href: 'http://www.layui.com/'
      //     //     //     //     ,target: '_blank'
      //     //     //     // });
      //     //     //     btn.find('.layui-layer-btn0').click(function(){
      //     //     //         // alert(1111111111)
      //     //     //     });
      //     //     // }
      //     //     ,end: function(index, layero){
      //     //         $(".edit-groups").hide()
      //     //     }
      //     // });
      // }
    })
    table.on("tool(demo1)", function(obj) {
      if (obj.event === "demo1") {
        //编辑组
        editId = obj.data.id
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.updateDataShow(obj.data.id, "", true, active.editBuildGroup)
        // layer.open({
        //     type: 1
        //     ,title: "新建组" //不显示标题栏
        //     ,closeBtn: 1
        //     ,area:['800px','700px']
        //     ,shade: 0.8
        //     ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
        //     // ,resize: false
        //     // ,btn: ['确定']
        //     ,btnAlign: 'c'
        //     ,moveType: 1 //拖拽模式，0或者1
        //     ,content:$('.edit-groups')
        //     // ,success: function(layero){
        //     //     var btn = layero.find('.layui-layer-btn');
        //     //     // btn.find('.layui-layer-btn0').attr({
        //     //     //     href: 'http://www.layui.com/'
        //     //     //     ,target: '_blank'
        //     //     // });
        //     //     btn.find('.layui-layer-btn0').click(function(){
        //     //         // alert(1111111111)
        //     //     });
        //     // }
        //     ,end: function(index, layero){
        //         $(".edit-groups").hide()
        //     }
        // });
      } else if (obj.event === "detail") {
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ["1065px", "500px"],
          shade: 0.3,
          id: "lunbo_layui", //设定一个id，防止重复弹出
          //btn: ['发送'],
          btnAlign: "c",
          moveType: 1, //拖拽模式，0或者1
          content: $("#xiangqing2")
          // success: function (layero) {
          //   form.on('submit(test2)', function (data) {
          //     alert("yangzheng")
          //     layer.msg('也可以这样', {
          //       time: 2000, //20s后自动关闭
          //       //btn: ['明白了', '知道了']
          //     });
          //     return false;
          //   });
          //   var btn = layero.find('.layui-layer-btn');
          //   btn.find('.layui-layer-btn0').on('click', function (params) { });
          //   return false;
          // }
        })
        $(".layui-layer-setwin>.layui-layer-close1").on("click", function() {
          // console.log(1111)
          $("#xiangqing2").css("display", "none")
        })
        editId = obj.data.id
        // layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层

        active.updateDataShow1(obj.data.id, "", true, active.editBuildGroup)

        // console.log($(this))
        // console.log($(this).parent)
        // console.log(XMLHttpRequest)
      }
    })

    //新增设备
    $(".new-build-equipment").click(function() {
      laydate.render({
        elem: "#assetPurchaseDate",
        type: "datetime"
      })
      layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
      active.getHardWareAddData()
      // active.newBuildEquipment();
    })
    //编辑设备
    $(".edit-build-equipment").click(function() {
      var checkStatus = table.checkStatus("hardwareList")
      if (checkStatus.data.length == 1) {
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.updataRetrieval(checkStatus.data[0].id)
        // active.editBuildEquipment()
      } else if (checkStatus.data.length > 1) {
        layer.msg("请选择一个服务器", { icon: 2, closeBtn: 1 })
      } else {
        layer.msg("请选择服务器", { icon: 2, closeBtn: 1 })
      }
      // active.editBuildEquipment()
    })
    //根据ip外网验证
    $(".edit-equipment .verificationIP").on("click", function() {
      if ($.trim($(".edit-equipment input[name=extranetIP]").val()) != "" && $(".edit-equipment select[name=osTypeKey]").val() != "") {
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.scriptData($.trim($(".edit-equipment input[name=extranetIP]").val()), $(".edit-equipment select[name=osTypeKey]").val())
      } else {
        layer.msg("请输入ip外网和操作系统", {
          icon: 2,
          closeBtn: 1
        })
      }
    })
    //新建组
    $(".new-build-group").click(function() {
      layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
      active.addDataShow("", "", active.newBuildGroup)
    })
    //编辑组
    $(".edit-build-group").click(function() {
      var checkStatus = table.checkStatus("hardwareGroupList")
      if (checkStatus.data.length == 1) {
        // console.log(checkStatus.data[0].id)
        editId = checkStatus.data[0].id
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.updateDataShow(checkStatus.data[0].id, "", true, active.editBuildGroup)
      } else if (checkStatus.data.length > 1) {
        layer.msg("请选择一个硬件组", { icon: 2, closeBtn: 1 })
      } else {
        layer.msg("请选择硬件组", { icon: 2, closeBtn: 1 })
      }
      // active.editBuildGroup()
    })
    //更新硬件
    $(".updata-hardware").on("click", function() {
      var checkStatus = table.checkStatus("hardwareList")

      let checkArray = []
      for (let i = 0; i < checkStatus.data.length; i++) {
        checkArray.push(checkStatus.data[i].id)
      }
      // console.log(checkArray)
      if (checkArray.length > 0) {
        layer.confirm(
          "您确定要更新吗？",
          {
            btn: ["确定", "取消"] //按钮
            //
          },
          function() {
            layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
            active.updataHardware(checkArray.toString())
          },
          function() {
            layer.msg("您取消了更新", {
              icon: 2,
              // time: 2000, //20s后自动关闭
              // btn: ['确定'],
              closeBtn: 1
            })
          }
        )
      } else {
        layer.msg("请选择服务器", { icon: 2, closeBtn: 1 })
      }
    })
    //删除硬件
    $(".del-hardware").on("click", function() {
      var checkStatus = table.checkStatus("hardwareList")

      let checkArray = []
      for (let i = 0; i < checkStatus.data.length; i++) {
        checkArray.push(checkStatus.data[i].id)
      }
      // console.log(checkArray)
      if (checkArray.length > 0) {
        layer.confirm(
          "您确定要删除吗？",
          {
            btn: ["确定", "取消"] //按钮
            //
          },
          function() {
            layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
            active.delHardware(checkArray.toString())
          },
          function() {
            layer.msg("您取消了删除", {
              icon: 2,
              // time: 2000, //20s后自动关闭
              // btn: ['确定'],
              closeBtn: 1
            })
          }
        )
      } else {
        layer.msg("请选择服务器", { icon: 2, closeBtn: 1 })
      }

      // if(checkStatus.data.length==1){
      //     layer.confirm('您确定要删除吗？', {
      //         btn: ['确定','取消'],//按钮
      //         //
      //     }, function(){
      //         console.log(checkStatus.data[0].id);
      //         active.delHardware(checkStatus.data[0].id);
      //
      //     }, function(){
      //         layer.msg('您取消了删除', {
      //             icon:2,
      //             // time: 2000, //20s后自动关闭
      //             // btn: ['确定'],
      //             closeBtn:1
      //         });
      //     });
      //     // console.log(checkStatus.data[0].id)
      // }
      // else if(checkStatus.data.length>1){
      //     layer.msg('请选择一个用户',{icon:2,closeBtn:1});
      // }
      // else{
      //     layer.msg('请选择用户',{icon:2,closeBtn:1});
      // }
    })
    //删除硬件组
    $(".del-hardwareGroup").on("click", function() {
      var checkStatus = table.checkStatus("hardwareGroupList")
      let checkArray = []
      for (let i = 0; i < checkStatus.data.length; i++) {
        checkArray.push(checkStatus.data[i].id)
      }
      console.log(checkArray)
      if (checkArray.length > 0) {
        layer.confirm(
          "您确定要删除吗？",
          {
            btn: ["确定", "取消"] //按钮
            //
          },
          function() {
            layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
            active.delHardwareGroup(checkArray.toString())
          },
          function() {
            layer.msg("您取消了删除", {
              icon: 2,
              // time: 2000, //20s后自动关闭
              // btn: ['确定'],
              closeBtn: 1
            })
          }
        )
      } else {
        layer.msg("请选择硬件组", { icon: 2, closeBtn: 1 })
      }
      // if(checkStatus.data.length==1){
      //     layer.confirm('您确定要删除吗？', {
      //         btn: ['确定','取消'],//按钮
      //         //
      //     }, function(){
      //         console.log(checkStatus.data[0].id);
      //         active.delHardwareGroup(checkStatus.data[0].id);
      //
      //     }, function(){
      //         layer.msg('您取消了删除', {
      //             icon:2,
      //             // time: 2000, //20s后自动关闭
      //             // btn: ['确定'],
      //             closeBtn:1
      //         });
      //     });
      //     // console.log(checkStatus.data[0].id)
      // }
      // else if(checkStatus.data.length>1){
      //     layer.msg('请选择一个用户',{icon:2,closeBtn:1});
      // }
      // else{
      //     layer.msg('请选择用户',{icon:2,closeBtn:1});
      // }
    })
    //下载硬件导入模板
    $(".downloadTemplate").on("click", function() {
      window.open(baseURL_SON1 + "/downloadTemplate")
    })
    //下载硬件组导入模块
    $(".downloadTemplateGroup").on("click", function() {
      window.open(baseURL_SON2 + "/downloadTemplate")
    })
    //新建硬件组类型table联动
    //监听软件联动版本
    form.on("select(typeLinkage)", function(data) {
      // console.log(1111);
      // console.log(data.value)
      typeOneVariable = data.value
      active.addDataShow("", data.value)
    })
    form.on("select(editTypeLinkage)", function(data) {
      // console.log(1111);
      // console.log(data.value)
      flagFirst = false
      typeTwoVariable = data.value
      active.updateDataShow(editId, "", data.value)
    })
    //上传导入
    //硬件
    upload.render({
      elem: ".ImportHardWare",
      url: baseURL_SON1 + "/importData",
      // url:'http://10.1.3.150:9001/HardwareResource/importData.do',
      method: "post",
      accept: "file",
      exts: "xls|xlsx",
      before: function(obj) {
        //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        // console.log(JSON.stringify(obj));
        layer.load(0, { shade: [0.1] }) //上传loading
      },
      done: function(res) {
        // console.log(res);
        if (res.code == 200) {
          active.tableList1()
          active.getTableListPage1()
          layer.closeAll("loading")
          layer.msg("上传成功", { icon: 1, closeBtn: 1 })
        } else if (res.code == 400) {
          layer.closeAll("loading")
          layer.msg(res.message, { icon: 2, closeBtn: 1 })
        }

        // alert("上传成功")
        //上传完毕回调
      },
      error: function(res) {
        console.log(res)
        layer.closeAll("loading")
        alert("上传失败")
        //请求异常回调
      }
    })
    //硬件组
    upload.render({
      elem: ".ImportHardWareGroup",
      url: baseURL_SON2 + "/importData",
      method: "post",
      accept: "file",
      exts: "xls|xlsx",
      before: function(obj) {
        //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        // console.log(JSON.stringify(obj))
        layer.load(0, { shade: [0.1] }) //上传loading
      },
      done: function(res) {
        if (res.code == 200) {
          active.tableList2()
          active.getTableListPage2()
          layer.closeAll("loading")
          layer.msg("上传成功", { icon: 1, closeBtn: 1 })
        } else if (res.code == 400) {
          layer.closeAll("loading")
          layer.msg(res.message, { icon: 2, closeBtn: 1 })
        }
        // console.log(res)
        // alert("上传成功")
        //上传完毕回调
      },
      error: function(res) {
        layer.closeAll("loading")
        alert("上传失败")
        //请求异常回调
      }
    })
    //导出
    //硬件
    $(".exportHardWare").on("click", function() {
      window.open(baseURL_SON1 + "/exportHardwareMessage")
      // layer.load(0, {shade: [0.1]}); //上传loading
      // active.exportHardWare()
    })
    //硬件组
    $(".exportHardWareGroup").on("click", function() {
      window.open(baseURL_SON2 + "/exportHardwareMessage")
      // layer.load(0, {shade: [0.1]}); //上传loading
      // active.exportHardWareGroup()
    })
    $(".xiangqing").on("click", function() {
      console.log(111111111111)
      console.log($(this))
    })
    //搜索1
    $("#btn-footer1").on("click", function() {
      // console.log(this);
      if (!$("#index-footer1").val()) {
        // console.log(11111)
        // layer.tips('不能为空！！！', $("#index-footer1"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
      } else {
        where = $("#index-footer1").val()
        active.tableList1(0, where)
        active.getTableListPage1(where)
        // active.tableList1(0,$("#index-footer1").val());
        // active.getTableListPage1($("#index-footer1").val());
      }
    })
    $("#index-footer1").on("keypress", function() {
      // console.log(this);
      if (event.keyCode == "13") {
        if (!$("#index-footer1").val()) {
          // console.log(11111)
          // layer.tips('不能为空！！！', $("#index-footer1"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
        } else {
          where = $("#index-footer1").val()
          active.tableList1(0, where)
          active.getTableListPage1(where)
          // active.tableList1(0,$("#index-footer1").val());
          // active.getTableListPage1($("#index-footer1").val());
        }
      }
    })
    $("#btn-footer2").on("click", function() {
      // console.log(this);
      if (!$("#index-footer2").val()) {
        // console.log(11111)
        // layer.tips('不能为空！！！', $("#index-footer2"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
      } else {
        where1 = $("#index-footer2").val()
        active.tableList2(0, where1)
        active.getTableListPage2(where1)
        // active.tableList2(0,$("#index-footer2").val());
        // active.getTableListPage2($("#index-footer2").val());
      }
    })
    $("#index-footer2").on("keypress", function() {
      // console.log(this);
      if (event.keyCode == "13") {
        if (!$("#index-footer2").val()) {
          // console.log(11111)
          // layer.tips('不能为空！！！', $("#index-footer2"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
        } else {
          where1 = $("#index-footer2").val()
          active.tableList2(0, where1)
          active.getTableListPage2(where1)
          // active.tableList2(0,$("#index-footer2").val());
          // active.getTableListPage2($("#index-footer2").val());
        }
      }
    })
    $("#btn-footer3").on("click", function() {
      if (!$("#index-footer3").val()) {
        // // layer.tips('不能为空！！！', $("#index-footer3"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
        active.tableList3(tableListData)
        return false
      } else {
        // searchOne = $("#index-footer3").val()
        active.addDataShow($("#index-footer3").val(), typeOneVariable)
        $("#index-footer3").val("")
        return false
      }
    })
    $("#index-footer3").on("keypress", function() {
      if (event.keyCode == "13") {
        if (!$("#index-footer3").val()) {
          // // layer.tips('不能为空！！！', $("#index-footer3"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
          active.tableList3(tableListData)
          return false
        } else {
          // searchOne = $("#index-footer3").val()
          active.addDataShow($("#index-footer3").val(), typeOneVariable)
          $("#index-footer3").val("")
          return false
        }
      }
    })
    $("#btn-footer4").on("click", function() {
      ifSearch = true
      if (!$("#index-footer4").val()) {
        // // layer.tips('不能为空！！！', $("#index-footer4"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
        active.tableList5(editTableListData)
        return false
      } else {
        // active.updataBuildGroup($("#index-footer4").val());
        active.updateDataShow(editId, $("#index-footer4").val(), typeTwoVariable)
        $("#index-footer4").val("")
        return false
      }
    })
    $("#index-footer4").on("keypress", function() {
      if (event.keyCode == "13") {
        ifSearch = true
        if (!$("#index-footer4").val()) {
          // // layer.tips('不能为空！！！', $("#index-footer4"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
          active.tableList5(editTableListData)
          return false
        } else {
          // active.updataBuildGroup($("#index-footer4").val());
          active.updateDataShow(editId, $("#index-footer4").val(), typeTwoVariable)
          $("#index-footer4").val("")
          return false
        }
      }
    })
    //键盘抬起重新加载
    $("#index-footer1").on("keyup", function() {
      if (!$("#index-footer1").val()) {
        where = ""
        active.tableList1()
        active.getTableListPage1()
      }
    })
    $("#icon-footer1").on("click", function() {
      where = ""
      $(this)
        .prev()
        .val("")
      active.tableList1()
      active.getTableListPage1()
    })
    $("#index-footer2").on("keyup", function() {
      if (!$("#index-footer2").val()) {
        where1 = ""
        active.tableList2()
        active.getTableListPage2()
      }
    })
    $("#icon-footer2").on("click", function() {
      where1 = ""
      $(this)
        .prev()
        .val("")
      active.tableList2()
      active.getTableListPage2()
    })
    //状态指示
    $("body")
      .on("mouseover", ".fail", function() {
        layerTipFail = layer.tips("停用", $(this), {
          tips: [2, "#FF5722"],
          tipsMore: false
          // time:0,
          // closeBtn:1
        })
      })
      .on("mouseout", ".fail", function() {
        layer.close(layerTipFail)
      })
    $("body")
      .on("mouseover", ".success", function() {
        layerTipSuccess = layer.tips("启用", $(this), {
          tips: [2, "#FF5722"],
          tipsMore: false
          // time:0,
          // closeBtn:1
        })
      })
      .on("mouseout", ".success", function() {
        layer.close(layerTipSuccess)
      })
    //暂时没用
    $(".demoTable .layui-btn").on("click", function() {
      var type = $(this).data("type")
      active[type] ? active[type].call(this) : ""
    })
    //清空IP输入框
    $(".NAME").on("click", function() {
      $(".new-groups .ip1").val("")
      $(".new-groups .ip2").val("")
      $(".new-groups .ip3").val("")
      $(".new-groups .ip4").val("")
      $(".new-groups .num").val("")
    })
    //下拉树操作
    $(".selecttree .downpanel")
      .on("click", ".layui-select-title", function(e) {
        $(".selecttree .layui-form-select")
          .not($(this).parents(".layui-form-select"))
          .removeClass("layui-form-selected")
        $(this)
          .parents(".downpanel")
          .toggleClass("layui-form-selected")
        layui.stope(e)
      })
      .on("click", "dl i", function(e) {
        layui.stope(e)
      })
    $(document).on("click", function(e) {
      $(".selecttree .layui-form-select").removeClass("layui-form-selected")
    })
  })
})()
