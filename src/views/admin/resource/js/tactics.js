vm = new Vue({
  el: '#app',
  data: function () {
    return {
      tableRowID: '',
    //   options:[],
    keys:'',
      showDetails: {
        selVal: '',
        showInfo: false,
        dataInfo: {
          aplicationAndDaemon: {
            hostName: "",
            ip: "",
            port: '',
            softwareName: "",
            softwareVersion: "",
          },
          
        }
      },
      count: 1,
      showDialog: false,//弹框显隐
      title: {//头部数据
        name: '',
        ban: '',
        jiqi: '',
        IP: '',
        duan: ''
      },
      servers: [],//设置服务器组服务器
      server: [],//服务器选项
      tacticsName: ['预留', '借阅', '限制借阅数量', '禁止借阅', '限定借阅期限', '授权使用', '授权使用所有模块', '禁止使用', '禁止使用所有模块', '设置某功能模块许可证空闲时间', '设置所有功能模块许可证空闲时间'],
      props: {
        children: 'children',
        label: 'name',
        id: 'id'
      },
      templateName: '',
      fileArr: [],//配置文件下拉选择
      templetArr: [],//模板下拉选择
      rules: {
        name: [{ min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' }]
      },
      formData: {
        file: '',//配置文件名称
        templet: '',//模板名称
        newTemplet: '',//新建模板名称
        tactic: '',//策略名称
        modular: [],
        type: [{ name: '用户', type: 0 }, { name: '用户组', type: 1 }, { name: '服务器', type: 2 }, { name: '服务器组', type: 3 }],
        user: {
          index_0: { name: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] },
        },
        serverGroup: {
          index_0: { name: '', member: [] },
        },
        tactics: [],
        reserveTactics: { //预留

        },
        borrowingTactics: {//借阅

        },
        limitNumBorrowingsTactics: {//限制借阅数量

        },
        noBorrowingTactics: {//禁止借阅

        },
        limitPeriodBorrowingsTactics: {//限定借阅期限

        },
        authorizedUseTactics: {//授权使用

        },
        authorizedUseAllModulesTactics: {//授权使用所有模块

        },
        prohibitionOfUseTactics: {//禁止使用

        },
        prohibitionOfUseAllModulesTactics: {//禁止使用所有模块

        },
        setFreeTimeTactics: {//设置某功能模块许可证空闲时间

        },
        setAllFreeTimeTactics: {//设置所有功能模块许可证空闲时间

        },
        limitTheNumber: {//限定许可证使用数

        },
      }
    }
  },

  methods: {
    removal(obj,obj1){
      // console.log(obj)
      // console.log(obj1)
      let that = this
      let str =''
      // str += obj1.name
      // str += obj1.modular
      // str += obj1.num
      if(obj1.name==null){
        str+=''
      }else{
        str += obj1.name
      }
      if(obj1.modular==null){
        str+=''
      }else{
        str += obj1.modular
      }
      if(obj1.num==null){
        str+=''
      }else{
        str += obj1.num
      }
      if(obj1.type === 0){
        str += obj1.treeNodeName
      }else if(obj1.type == 1){
        str += obj1.tacticsUsers
      }else if(obj1.type == 2){
        // console.log(obj1.tacticsServer)
        str += obj1.tacticsServer
      }else if(obj1.type == 3){
        str += obj1.tacticsServers
      }else if(obj1.type == null){
        str += ''
      }
      // console.log(str)
      let a = 0
      for (const i in obj) {
        let str1 =''
        if(obj[i].name==null){
          str1+=''
        }else{
          str1 += obj[i].name
        }
        if(obj[i].modular==null){
          str1+=''
        }else{
          str1 += obj[i].modular
        }
        if(obj[i].num==null){
          str1+=''
        }else{
          str1 += obj[i].num
        }
        // str1 += obj[i].modular
        // str1 += obj[i].num
        if(obj[i].type === 0){
          str1 += obj[i].treeNodeName
        }else if(obj[i].type == 1){
          str1 += obj[i].tacticsUsers
        }else if(obj[i].type == 2){
          console.log(obj[i].tacticsServer)
          str1 += obj[i].tacticsServer
        }else if(obj[i].type == 3){
          str1 += obj[i].tacticsServers
        }else if(obj[i].type == null){
          str1 += ''
        }
        // console.log(str1)
        if(str == str1){
          a++
        }
      }
      if(a>=2){
        // that.$message('您已经配置了相同的策略')
        // that.$message({
        //   showClose: true,
        //   message: '您已经配置了相同的策略',
        //   type: 'warning'
        // });
            layer.msg('您已经配置了相同的策略', {
              icon: 7,
              closeBtn:1
          });
      }
    },
    infoData(val) {
      console.log(val)
      var _this = this
      $.ajax({
        url: url + ':9001/licenseServiceDetails/getDefaultTemplateList/' + val,
        type: "post",
        async: true,
        success: function (data) {
          console.log(data)
          _this.showDetails.dataInfo = data.data
        }
      });
    },
    handleClose(done) {
      // this.$confirm('是否关闭当前页面?', '', {
      //   confirmButtonText: '是',
      //   cancelButtonText: '否',
      //   // type: 'warning'
      // }).then(() => {
      //   this.closeBtn()
      // }).catch(() => {
      // });
      var vConfirm = confirm("是否关闭当前页面?")
      if(vConfirm == true) {
        this.closeBtn()
      }
    },
    closeBtn () {
      this.showDialog = false
    },
    openDialog () {
      let _that = this
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getOrgTree",
        type: "post",
        async: false,
        success: function (data) {
          if (data.code == 200) {
            okotree = data
            // _that.options = data.data
            console.log(data)
            console.log(okotree)
            // console.log(_that.objkey(_that.options))
            _that.$set(_that.formData.user, 'index_0', { name: '', id: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] })
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    // 重置信息
    resetBtn () {
      console.log(url)
      console.log(port)
      this.formData.user = { index_0: { name: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] } }
      this.formData.serverGroup = { index_0: { name: '', member: [] } }
      this.formData.reserveTactics = {}
      this.formData.borrowingTactics = {}
      this.formData.limitNumBorrowingsTactics = {}
      this.formData.noBorrowingTactics = {}
      this.formData.limitPeriodBorrowingsTactics = {}
      this.formData.authorizedUseTactics = {}
      this.formData.authorizedUseAllModulesTactics = {}
      this.formData.prohibitionOfUseTactics = {}
      this.formData.prohibitionOfUseAllModulesTactics = {}
      this.formData.setFreeTimeTactics = {}
      this.formData.setAllFreeTimeTactics = {}
      this.formData.limitTheNumber = {}
      this.formData.tactic=''
    },
    // 删除模板
    deleteBtn (val) {
      let _this = this
      $.ajax({
        url: url + ":9001/licenseServiceDetails/deleteTemplateById/" + val,
        type: "post",
        async: true,
        success: function (data) {
          if (data.code == 200) {
            _this.handleFlieChange(_this.showDetails.selVal)
            _this.formData.templet = ''
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    init (rowID) {
      this.getCL()
      console.log(rowID)
      let _that = this
      // rowID = '8d4d3bc9e21d447ea0e3d19835461536'
      this.tableRowID = rowID;
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getApplicationAndDaemon/" + rowID,
        type: "post",
        async: true,
        success: function (data) {
          if (data.code == 200) {
            if (data.data) {
              _that.title.name = data.data.softwareName
              _that.title.ban = data.data.softwareVersion
              _that.title.jiqi = data.data.hostName
              _that.title.IP = data.data.ip
              _that.title.duan = data.data.port
              _that.fileArr = data.data.daemonList
              _that.getSeverGroup();
              if (data.data && data.data.daemonList.length > 0) {
                _that.formData.file = data.data.daemonList[0].id
                _that.handleFlieChange(data.data.daemonList[0].id)
                let obj = {};
                // obj = _that.fileArr.find((item) => {
                // // obj = _that.fileArr.filter((item) => {
                //   console.log(item)
                //   return item.id === data.data.daemonList[0].id;
                // });
                for( item in _that.fileArr) {
                  if(_that.fileArr[item].id === data.data.daemonList[0].id) {
                    obj = _that.fileArr[item]
                  }
                }
                _that.getModuleList(obj.name);
                _that.openDialog();

              }
            }
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    getCL () {
      let _that = this
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getStrategyCategory",
        type: "post",
        async: true,
        success: function (data) {
          if (data.code == 200) {
            data.data.splice(8,4)
            _that.formData.tactics = data.data
            console.log(data)
            // console.log(_that.formData.tactics)
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    getModuleList (name) {
      let _that = this
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getModuleList",
        type: "post",
        dateType: 'json',
        contentType: "application/json;charset-UTF-8",
        data: JSON.stringify({ licServiceId: this.tableRowID, daemonName: name }),
        async: true,
        success: function (data) {
          if (data.code == 200) {
              console.log(data)
            _that.$set(_that.formData, 'modular', data.data)
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    getSeverGroup () {
      let _that = this
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getHardwareList",
        type: "post",
        async: true,
        success: function (data) {
          if (data.code == 200) {
            _that.server = data.data
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    handleFlieChange (selVal) {
      let _that = this
      _that.formData.templet=''
      _that.templateName=''
      _that.formData.user = { index_0: { name: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] } }
      _that.formData.serverGroup = { index_0: { name: '', member: [] } }
      _that.formData.reserveTactics = {}
      _that.formData.borrowingTactics = {}
      _that.formData.limitNumBorrowingsTactics = {}
      _that.formData.noBorrowingTactics = {}
      _that.formData.limitPeriodBorrowingsTactics = {}
      _that.formData.authorizedUseTactics = {}
      _that.formData.authorizedUseAllModulesTactics = {}
      _that.formData.prohibitionOfUseTactics = {}
      _that.formData.prohibitionOfUseAllModulesTactics = {}
      _that.formData.setFreeTimeTactics = {}
      _that.formData.setAllFreeTimeTactics = {}
      _that.formData.limitTheNumber = {}
      _that.showDetails.selVal = selVal
      _that.formData.tactic=''
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getTemplateList",
        type: "post",
        dateType: 'json',
        contentType: "application/json;charset-UTF-8",
        data: JSON.stringify({ 'licenseServiceId': this.tableRowID, 'permitFileId': selVal }),
        async: true,
        success: function (data) {
          if (data.code == 200) {
            _that.templetArr = data.data
            let obj = {};
            // obj = _that.fileArr.find((item) => {
            // // obj = _that.fileArr.filter((item) => {
            //   return item.id === selVal;
            // });
            console.log(selVal)
            for (item in _that.fileArr) {
              if(_that.fileArr[item].id === selVal) {
                obj = _that.fileArr[item]
              }
            }

            _that.getModuleList(obj.name);
            if (data.data.length > 0) {
              _that.formData.templet = data.data[0].id
              _that.handleTempleChange(data.data[0].id)
            }
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    handleTempleChange (selVal) {
      let obj = {};
      let _that = this
      let num = 0;
      let severNum = 0
      // obj = this.templetArr.find((item) => {
      // // obj = this.templetArr.filter((item) => {
      //   return item.id === selVal;
      // });
      for( item in this.templetArr) {
        if(this.templetArr[item].id === selVal) {
          obj = this.templetArr[item]
        }
      }
      if(obj){
        console.log(obj)
        this.templateName = obj.name
      }
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getTemplateById/" + selVal,
        type: "post",
        async: true,
        success: function (data) {
          if (data.code == 200) {
              console.log(data)
            _that.$set(_that.formData, 'user', {})
            _that.$set(_that.formData.user, 'index_0', { name: '', id: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] })
            data.data.templateUserGroupList.map((item) => {
              _that.$set(_that.formData.user, 'index_' + num,
                { name: item.userGroupName, id: item.id, member: [], treeNodeName: item.userNames, selectNodes: item.userIds.split(","), tree: [okotree.data] })
              num++;
            })
            //清空策略名称数据
            _that.formData.borrowingTactics = {}
            _that.formData.reserveTactics = {}
            _that.formData.limitNumBorrowingsTactics = {}
            _that.formData.noBorrowingTactics = {}
            _that.formData.limitPeriodBorrowingsTactics = {}
            _that.formData.authorizedUseTactics = {}
            _that.formData.authorizedUseAllModulesTactics = {}
            _that.formData.prohibitionOfUseTactics = {}
            _that.formData.prohibitionOfUseAllModulesTactics = {}
            _that.formData.setFreeTimeTactics = {}
            _that.formData.setAllFreeTimeTactics = {}
            _that.formData.limitTheNumber = {}
            _that.formData.tactic=''
            data.data.templateStrategyCategoryList.map(item => {
              switch (item.categoryName) {
                case '借阅':
                  _that.echo(item, 'borrowingTactics')
                  break
                case '预留':
                  _that.echo(item, 'reserveTactics')
                  break
                case '限定借阅数量':
                  _that.echo(item, 'limitNumBorrowingsTactics')
                  break
                case '禁止借阅':
                  _that.echo(item, 'noBorrowingTactics')
                  break
                case '限定借阅期限':
                  _that.echo(item, 'limitPeriodBorrowingsTactics')
                  break
                case '授权使用':
                  _that.echo(item, 'authorizedUseTactics')
                  break
                case '授权使用所有模块':
                  _that.echo(item, 'authorizedUseAllModulesTactics')
                  break
                case '禁止使用':
                  _that.echo(item, 'prohibitionOfUseTactics')
                  break
                case '禁止使用所有模块':
                  _that.echo(item, 'prohibitionOfUseAllModulesTactics')
                  break
                case '设置超时时间':
                  _that.echo(item, 'setFreeTimeTactics')
                  break
                case '设置所有模块超时时间':
                  _that.echo(item, 'setAllFreeTimeTactics')
                  break
                case '限定许可使用数量':
                  _that.echo(item, 'limitTheNumber')
                  break
              }
            });
            _that.$set(_that.formData, 'serverGroup', {})
            //服务器组在切换配置文件名称，模板名称时不知道为什么会取到策略名称里面的模块的值，并且显示里面内容显示错误
            //所以在100毫秒后重置一下
            setTimeout(function () {
              _that.$set(_that.formData.serverGroup, 'index_' + severNum,
                { name: "", id: "", member: [] })
              data.data.templateHardwareList.map((item) => {
                _that.$set(_that.formData.serverGroup, 'index_' + severNum,
                  { name: item.hardwareGroupName, id: item.id, member: item.hardwareIds.split(",") })
                severNum++;
              })
              console.log(_that.formData,333)
            }, 100)

          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    handleTempleChange1 (selVal) {
      // let obj = {};
      let _that = this
      let num = 0;
      let severNum = 0
      // obj = this.templetArr.find((item) => {
      //   return item.id === selVal;
      // });
      // console.log(obj)
      // this.templateName = obj.name
      $.ajax({
        url: url + ":9001/licenseServiceDetails/getTemplateById/" + selVal,
        type: "post",
        async: true,
        success: function (data) {
          if (data.code == 200) {
              console.log(data)
            _that.$set(_that.formData, 'user', {})
            _that.$set(_that.formData.user, 'index_0', { name: '', id: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] })
            data.data.templateUserGroupList.map((item) => {
              _that.$set(_that.formData.user, 'index_' + num,
                { name: item.userGroupName, id: item.id, member: [], treeNodeName: item.userNames, selectNodes: item.userIds.split(","), tree: [okotree.data] })
              num++;
            })
            //清空策略名称数据
            _that.formData.borrowingTactics = {}
            _that.formData.reserveTactics = {}
            _that.formData.limitNumBorrowingsTactics = {}
            _that.formData.noBorrowingTactics = {}
            _that.formData.limitPeriodBorrowingsTactics = {}
            _that.formData.authorizedUseTactics = {}
            _that.formData.authorizedUseAllModulesTactics = {}
            _that.formData.prohibitionOfUseTactics = {}
            _that.formData.prohibitionOfUseAllModulesTactics = {}
            _that.formData.setFreeTimeTactics = {}
            _that.formData.setAllFreeTimeTactics = {}
            _that.formData.limitTheNumber = {}
            _that.formData.tactic=''
            data.data.templateStrategyCategoryList.map(item => {
              switch (item.categoryName) {
                // case '借阅':
                //   _that.echo(item, 'borrowingTactics')
                //   break
                case '预留':
                  _that.echo(item, 'reserveTactics')
                  break
                // case '限制借阅数量':
                //   _that.echo(item, 'limitNumBorrowingsTactics')
                //   break
                // case '禁止借阅':
                //   _that.echo(item, 'noBorrowingTactics')
                //   break
                // case '限定借阅期限':
                //   _that.echo(item, 'limitPeriodBorrowingsTactics')
                //   break
                case '授权使用':
                  _that.echo(item, 'authorizedUseTactics')
                  break
                case '授权使用所有模块':
                  _that.echo(item, 'authorizedUseAllModulesTactics')
                  break
                case '禁止使用':
                  _that.echo(item, 'prohibitionOfUseTactics')
                  break
                case '禁止使用所有模块':
                  _that.echo(item, 'prohibitionOfUseAllModulesTactics')
                  break
                case '设置某功能模块许可证空闲时间':
                  _that.echo(item, 'setFreeTimeTactics')
                  break
                case '设置所有功能模块许可证空闲时间':
                  _that.echo(item, 'setAllFreeTimeTactics')
                  break
                case '限定许可证使用数':
                  _that.echo(item, 'limitTheNumber')
                  break
              }
            });
            _that.$set(_that.formData, 'serverGroup', {})
            //服务器组在切换配置文件名称，模板名称时不知道为什么会取到策略名称里面的模块的值，并且显示里面内容显示错误
            //所以在100毫秒后重置一下
            setTimeout(function () {
              _that.$set(_that.formData.serverGroup, 'index_' + severNum,
                { name: "", id: "", member: [] })
              data.data.templateHardwareList.map((item) => {
                _that.$set(_that.formData.serverGroup, 'index_' + severNum,
                  { name: item.hardwareGroupName, id: item.id, member: item.hardwareIds.split(",") })
                severNum++;
              })
            }, 100)
          }
          else if (data.code == 400) {
            layer.msg(data.message, {
              icon: 2,
              closeBtn: 1
            });
          }
        },
        error: function (data) {
          window.location.href = "../../error/error.html";
        }
      });
    },
    echo (item, params) {
      let _that = this
      let obj = {
        id: item.categoryId,
        name: item.categoryName,
        member: '',
        modular: item.moduleId,
        type: item.type,
        num: item.parameterValue,
        show1: false,
        show2: false,
        show3: false,
        show4: false,
        tacticsServer: [],
        tacticsUsers: [],
        tacticsUsersId: item.typeValue,
        tacticsServers: [],
        tacticsUser: [],
        tree: [okotree.data],
        treeNodeName: [],
      }
      if (item.type == 0) {
        obj.show1 = true;
        obj.tacticsUser = item.typeValueName
        obj.selectNodes = [item.typeValue]
        obj.treeNodeName = item.typeValueName

      } else if (item.type == 1) {
        obj.show3 = true;
        obj.tacticsUsers = item.typeValueName
      } else if (item.type == 2) {
        obj.show2 = true;
        obj.tacticsServer = item.typeValue
      } else if (item.type == 3) {
        obj.show4 = true;
        obj.tacticsServers = item.typeValueName
      }
      this.$set(_that.formData[params], _that.transformation(_that.formData[params]), obj)
    },
    transformation: function (obj) {
      let arr = Object.keys(obj)//对象转化数组
      let lastIndex = ''
      let join = ''
      if (arr.length == 0) {
        join = 'index_0'
      } else {
        lastIndex = arr[arr.length - 1].split("_")[1]//获取最后一项数字
        join = 'index_' + (parseInt(lastIndex) + 1)//将要新增进对象的key值
      }
      return join
    },
    handleCheckChange (key) {
      let nodeNameArr = []
      let nodeKeyArr = [];
      this.$refs[key][0].getCheckedNodes().map(item => {
        // console.log(item.children)
        // console.log(item.type)
        if (item.children.length === 0 && item.type) {
          nodeNameArr.push(item.name)
          nodeKeyArr.push(item.id)
        }
      });
      // console.log(nodeNameArr)
      // console.log(nodeKeyArr)
      this.$set(this.formData.user[key], 'treeNodeName', nodeNameArr.join(','))
      this.$set(this.formData.user[key], 'selectNodes', nodeKeyArr)
    },
    handleCheck(obj){
        // console.log(obj.id)
        this.$refs[this.keys][0].setCheckedKeys([obj.id]);
    },
    handleCheckChange1 (obj,key,obj1) {
      
      // console.log(obj)
      let nodeNameArr = []
      let selectArrs =[]
      let nodeKeyArr = [];
      let key1 = key.substring(0, key.length - 1)
      this.keys = key
    //   console.log( this.$refs[key][0].getCheckedKeys()[this.$refs[key][0].getCheckedKeys().length-1])
    //   console.log( this.$refs[key][0])
      // console.log( this.$refs[key][0].getCurrentKey())
      // console.log( this.$refs[key][0].getCheckedNodes()[this.$refs[key][0].getCheckedNodes().length-1].name)
      this.$refs[key][0].getCheckedNodes().map(item => {
        if (item.children.length !== 0) {
            // console.log(item)
            this.$set(item, 'disabled', true)
        }else if (item.children==null){
            // console.log(item.id)
            selectArrs.unshift(item.id)
        }
      });
      if (this.$refs[key][0].getCheckedNodes().length > 0) {
        nodeNameArr.push(this.$refs[key][0].getCheckedNodes()[this.$refs[key][0].getCheckedNodes().length - 1].name)
        nodeKeyArr.push(this.$refs[key][0].getCheckedKeys()[this.$refs[key][0].getCheckedKeys().length-1])
      } else {
        nodeNameArr = []
      }
      // console.log(nodeKeyArr)
      this.$set(obj[key1], 'treeNodeName', nodeNameArr.join(','))
      this.$set(obj[key1],'selectNodes',nodeKeyArr)
    //   this.$refs[key][0].setCheckedKeys([]);
    this.removal(obj,obj1)
    },
    handleNodeClick (data) {
       
    
    },
    addUser () {
      let obj = JSON.parse(JSON.stringify(this.formData.user))//深拷贝对象
      this.$set(this.formData.user, this.transformation(obj), { name: '', member: [], treeNodeName: '', selectNodes: [], tree: [okotree.data] })
    },
    addServerGroup () {
      let obj = JSON.parse(JSON.stringify(this.formData.serverGroup))//深拷贝对象
      this.$set(this.formData.serverGroup, this.transformation(obj), { name: '', member: [] })
    },
    addTacticsName () {
      if (this.formData.tactic == "") {
        // this.$message("请选择策略名称")
        layer.msg("请选择策略名称", {
              icon: 2,
              closeBtn:1
          });
        return
      }
      if (this.formData.tactic == "预留") {
        let obj = JSON.parse(JSON.stringify(this.formData.reserveTactics))//深拷贝对象
        this.$set(this.formData.reserveTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[0].id })
      } else if (this.formData.tactic == "借阅") {
        let obj = JSON.parse(JSON.stringify(this.formData.borrowingTactics))//深拷贝对象
        this.$set(this.formData.borrowingTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[8].id })
      } else if (this.formData.tactic == "限定借阅数量") {
        let obj = JSON.parse(JSON.stringify(this.formData.limitNumBorrowingsTactics))//深拷贝对象
        this.$set(this.formData.limitNumBorrowingsTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[10].id })
      } else if (this.formData.tactic == "禁止借阅") {
        let obj = JSON.parse(JSON.stringify(this.formData.noBorrowingTactics))//深拷贝对象
        this.$set(this.formData.noBorrowingTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[11].id })
      } else if (this.formData.tactic == "限定借阅期限") {
        let obj = JSON.parse(JSON.stringify(this.formData.limitPeriodBorrowingsTactics))//深拷贝对象
        this.$set(this.formData.limitPeriodBorrowingsTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[9].id })
      } else if (this.formData.tactic == "授权使用") {
        let obj = JSON.parse(JSON.stringify(this.formData.authorizedUseTactics))//深拷贝对象
        this.$set(this.formData.authorizedUseTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[1].id })
      } else if (this.formData.tactic == "授权使用所有模块") {
        let obj = JSON.parse(JSON.stringify(this.formData.authorizedUseAllModulesTactics))//深拷贝对象
        this.$set(this.formData.authorizedUseAllModulesTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[2].id })
      } else if (this.formData.tactic == "禁止使用") {
        let obj = JSON.parse(JSON.stringify(this.formData.prohibitionOfUseTactics))//深拷贝对象
        this.$set(this.formData.prohibitionOfUseTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[3].id })
      } else if (this.formData.tactic == "禁止使用所有模块") {
        let obj = JSON.parse(JSON.stringify(this.formData.prohibitionOfUseAllModulesTactics))//深拷贝对象
        this.$set(this.formData.prohibitionOfUseAllModulesTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[4].id })
      } else if (this.formData.tactic == "设置超时时间") {
        let obj = JSON.parse(JSON.stringify(this.formData.setFreeTimeTactics))//深拷贝对象
        this.$set(this.formData.setFreeTimeTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[5].id })
      } else if (this.formData.tactic == "设置所有模块超时时间") {
        let obj = JSON.parse(JSON.stringify(this.formData.setAllFreeTimeTactics))//深拷贝对象
        this.$set(this.formData.setAllFreeTimeTactics, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[6].id })
      } else if (this.formData.tactic == "限定许可使用数量") {
        let obj = JSON.parse(JSON.stringify(this.formData.limitTheNumber))//深拷贝对象
        this.$set(this.formData.limitTheNumber, this.transformation(obj), { name: this.formData.tactic, member: [], modular: '', type: '', num: '', show1: false, show2: false, show3: false, show4: false, tacticsServer: '', tacticsUsers: '', tacticsServers: '', tacticsUser: '', tree: [okotree.data], id: this.formData.tactics[7].id })
      }

    },
    userShow (obj) {
      obj.show1 = false
      obj.show2 = false
      obj.show3 = false
      obj.show4 = false
      if (obj.type == 0) {
        obj.show1 = true
      } else if (obj.type == 2) {
        obj.show2 = true
      } else if (obj.type == 1) {
        obj.show3 = true
      } else if (obj.type == 3) {
        obj.show4 = true
      } else if (obj.type == '— — 类型 — —') {
        return
      }
    },
    delUser (key) {
      Vue.delete(this.formData.user, key);//vue方法
    },
    delServerGroup (key) {
      Vue.delete(this.formData.serverGroup, key);//vue方法
    },
    delTacticsName (obj, key) {
      Vue.delete(obj, key);//vue方法
    },
    application () {
      const reg = /^[0-9a-zA-Z]*$/
      let UserGroupList = []//用户组
      let HardwareList = []//服务器组
      let StrategyCategoryList = []//策略
      //验证用户组名
      for (const i in this.formData.user) {
        if(this.formData.user[i].name==''&&this.formData.user[i].selectNodes.length === 0){
          continue;
        }else{
          let arr = []
          if (!reg.test(this.formData.user[i].name)) {
            // this.$message.error('用户组名称只能为英文字母');
            layer.msg("用户组名称只能为英文字母", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          if(this.formData.user[i].name===''){
            // this.$message.error('请输入用户组名称');
            layer.msg("请输入用户组名称", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          if (this.formData.user[i].selectNodes.length === 0) {
            // this.$message.error('请选择至少一个用户');
            layer.msg("请选择至少一个用户", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          for (let j = 0; j < this.formData.user[i].selectNodes.length; j++) {
            arr.push(this.formData.user[i].selectNodes[j])
          }
          let obj = {
            userGroupName: this.formData.user[i].name,
            userIds: arr.join(",")
          }
          UserGroupList.push(obj)
        }
      }
      //验证服务器组
      for (const i in this.formData.serverGroup) {
        if(this.formData.serverGroup[i].name==''&&this.formData.serverGroup[i].member.length === 0){
          continue;
        }else{
          let arr = []
          if (!reg.test(this.formData.serverGroup[i].name)) {
            // this.$message.error('服务器组名称只能为英文字母');
            layer.msg("服务器组名称只能为英文字母", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          if(this.formData.serverGroup[i].name===''){
            // this.$message.error('请输入服务器组名称');
            layer.msg("请输入服务器组名称", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          if (this.formData.serverGroup[i].member.length === 0) {
            // this.$message.error('请选择至少一个服务器');
            layer.msg("请选择至少一个服务器", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          for (let j = 0; j < this.formData.serverGroup[i].member.length; j++) {
            arr.push(this.formData.serverGroup[i].member[j])
          }
          let obj = {
            hardwareGroupName: this.formData.serverGroup[i].name,
            hardwareIds: arr.join(",")
          }
          HardwareList.push(obj)
        }
      }
      //验证策略信息是否完整
      // this.fors1(this.formData.reserveTactics,StrategyCategoryList)预留
      for (const i in this.formData.reserveTactics) {
        // console.log(this.formData.reserveTactics[i])
        let str = ''
        for (const key in this.formData.modular) {
          if (this.formData.modular[key].id == this.formData.reserveTactics[i].modular) {
            str = this.formData.modular[key].name
          }
        }
        if (this.formData.reserveTactics[i].modular == '' || this.formData.reserveTactics[i].type == null || this.formData.reserveTactics[i].num == '') {
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1
        });
          return;
        }
        let objs = {}
        if (this.formData.reserveTactics[i].type === 0) {
          if (this.formData.reserveTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1
          });

            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValue: this.formData.reserveTactics[i].selectNodes[0],
            parameterValue: this.formData.reserveTactics[i].num
          }
        } else if (this.formData.reserveTactics[i].type == 1) {
          if (this.formData.reserveTactics[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValueName: this.formData.reserveTactics[i].tacticsUsers,
            parameterValue: this.formData.reserveTactics[i].num
          }
        } else if (this.formData.reserveTactics[i].type == 2) {
          if (this.formData.reserveTactics[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValue: this.formData.reserveTactics[i].tacticsServer,
            parameterValue: this.formData.reserveTactics[i].num
          }
        } else if (this.formData.reserveTactics[i].type == 3) {
          if (this.formData.reserveTactics[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValueName: this.formData.reserveTactics[i].tacticsServers,
            parameterValue: this.formData.reserveTactics[i].num
          }
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors2(this.formData.authorizedUseTactics,StrategyCategoryList)授权使用
      for (const i in this.formData.authorizedUseTactics) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.authorizedUseTactics[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.authorizedUseTactics[i].modular == '' || this.formData.authorizedUseTactics[i].type == null) {
          // //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.authorizedUseTactics[i].type === 0) {
          console.log(this.formData.authorizedUseTactics[i])
          if (this.formData.authorizedUseTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValue: this.formData.authorizedUseTactics[i].selectNodes[0],
            parameterValue: null
          }
        } else if (this.formData.authorizedUseTactics[i].type == 1) {
          if (this.formData.authorizedUseTactics[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValueName: this.formData.authorizedUseTactics[i].tacticsUsers,
            parameterValue: null
          }
        } else if (this.formData.authorizedUseTactics[i].type == 2) {
          if (this.formData.authorizedUseTactics[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValue: this.formData.authorizedUseTactics[i].tacticsServer,
            parameterValue: null
          }
        } else if (this.formData.authorizedUseTactics[i].type == 3) {
          if (this.formData.authorizedUseTactics[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValueName: this.formData.authorizedUseTactics[i].tacticsServers,
            parameterValue: null
          }
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors4(this.formData.authorizedUseAllModulesTactics,StrategyCategoryList)授权使用所有模块
      for (const i in this.formData.authorizedUseAllModulesTactics) {
        if (this.formData.authorizedUseAllModulesTactics[i].type == null) {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.authorizedUseAllModulesTactics[i].type === 0) {
          if (this.formData.authorizedUseAllModulesTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
            categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
            moduleId: null,
            type: this.formData.authorizedUseAllModulesTactics[i].type,
            typeValue: this.formData.authorizedUseAllModulesTactics[i].selectNodes[0],
            parameterValue: null
          }
          }else if (this.formData.authorizedUseAllModulesTactics[i].type == 1) {
            if (this.formData.authorizedUseAllModulesTactics[i].tacticsUsers.length == 0) {
              // this.$alert('请选择用户组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择用户组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
              categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.authorizedUseAllModulesTactics[i].type,
              typeValueName: this.formData.authorizedUseAllModulesTactics[i].tacticsUsers,
              parameterValue:null
            }
          } else if (this.formData.authorizedUseAllModulesTactics[i].type == 2) {
            if (this.formData.authorizedUseAllModulesTactics[i].tacticsServer.length == 0) {
              // this.$alert('请选择服务器', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
              categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.authorizedUseAllModulesTactics[i].type,
              typeValue: this.formData.authorizedUseAllModulesTactics[i].tacticsServer,
              parameterValue: null
            }
          } else if (this.formData.authorizedUseAllModulesTactics[i].type == 3) {
            if (this.formData.authorizedUseAllModulesTactics[i].tacticsServers.length == 0) {
              // this.$alert('请选择服务器组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
              categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.authorizedUseAllModulesTactics[i].type,
              typeValueName: this.formData.authorizedUseAllModulesTactics[i].tacticsServers,
              parameterValue: null
            }
          }
        
        StrategyCategoryList.push(objs)
      }
      // this.fors2(this.formData.prohibitionOfUseTactics,StrategyCategoryList)禁止使用
      for (const i in this.formData.prohibitionOfUseTactics) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.prohibitionOfUseTactics[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.prohibitionOfUseTactics[i].modular == '' || this.formData.prohibitionOfUseTactics[i].type == null) {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.prohibitionOfUseTactics[i].type === 0) {
          if (this.formData.prohibitionOfUseTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValue: this.formData.prohibitionOfUseTactics[i].selectNodes[0],
            parameterValue: null
          }
        } else if (this.formData.prohibitionOfUseTactics[i].type == 1) {
          if (this.formData.prohibitionOfUseTactics[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValueName: this.formData.prohibitionOfUseTactics[i].tacticsUsers,
            parameterValue: null
          }
        } else if (this.formData.prohibitionOfUseTactics[i].type == 2) {
          if (this.formData.prohibitionOfUseTactics[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValue: this.formData.prohibitionOfUseTactics[i].tacticsServer,
            parameterValue: null
          }
        } else if (this.formData.prohibitionOfUseTactics[i].type == 3) {
          if (this.formData.prohibitionOfUseTactics[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValueName: this.formData.prohibitionOfUseTactics[i].tacticsServers,
            parameterValue: null
          }
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors4(this.formData.prohibitionOfUseAllModulesTactics,StrategyCategoryList)禁止使用所有模块
      for (const i in this.formData.prohibitionOfUseAllModulesTactics) {
        if (this.formData.prohibitionOfUseAllModulesTactics[i].type == null) {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.prohibitionOfUseAllModulesTactics[i].type === 0) {
          if (this.formData.prohibitionOfUseAllModulesTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
            categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
            moduleId: null,
            type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
            typeValue: this.formData.prohibitionOfUseAllModulesTactics[i].selectNodes[0],
            parameterValue: null
          }
          }else if (this.formData.prohibitionOfUseAllModulesTactics[i].type == 1) {
            if (this.formData.prohibitionOfUseAllModulesTactics[i].tacticsUsers.length == 0) {
              // this.$alert('请选择用户组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择用户组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
              categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
              typeValueName: this.formData.prohibitionOfUseAllModulesTactics[i].tacticsUsers,
              parameterValue:null
            }
          } else if (this.formData.prohibitionOfUseAllModulesTactics[i].type == 2) {
            if (this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServer.length == 0) {
              // this.$alert('请选择服务器', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
              categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
              typeValue: this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServer,
              parameterValue: null
            }
          } else if (this.formData.prohibitionOfUseAllModulesTactics[i].type == 3) {
            if (this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServers.length == 0) {
              // this.$alert('请选择服务器组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
              categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
              typeValueName: this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServers,
              parameterValue: null
            }
          }
        
        StrategyCategoryList.push(objs)
      }
      // this.fors3(this.formData.setFreeTimeTactics,StrategyCategoryList)设置超时时间
      for (const i in this.formData.setFreeTimeTactics) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.setFreeTimeTactics[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.setFreeTimeTactics[i].modular == '' || this.formData.setFreeTimeTactics[i].num == '') {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        objs = {
          categoryName: this.formData.setFreeTimeTactics[i].name,
          categoryId: this.formData.setFreeTimeTactics[i].id,
          moduleId: this.formData.setFreeTimeTactics[i].modular,
          moduleName:str,
          type: null,
          typeValue: null,
          parameterValue: this.formData.setFreeTimeTactics[i].num
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors5(this.formData.setAllFreeTimeTactics,StrategyCategoryList)设置所有模块超时时间
      for (const i in this.formData.setAllFreeTimeTactics) {
        if (this.formData.setAllFreeTimeTactics[i].num == '') {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        objs = {
          categoryName: this.formData.setAllFreeTimeTactics[i].name,
          categoryId: this.formData.setAllFreeTimeTactics[i].id,
          moduleId: null,
          type: null,
          typeValue: null,
          parameterValue: this.formData.setAllFreeTimeTactics[i].num
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors1(this.formData.limitTheNumber,StrategyCategoryList)限定许可使用数量
      for (const i in this.formData.limitTheNumber) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.limitTheNumber[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.limitTheNumber[i].modular == '' || this.formData.limitTheNumber[i].type == null || this.formData.limitTheNumber[i].num == '') {
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.limitTheNumber[i].type === 0) {
          if (this.formData.limitTheNumber[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValue: this.formData.limitTheNumber[i].selectNodes[0],
            parameterValue: this.formData.limitTheNumber[i].num
          }
        } else if (this.formData.limitTheNumber[i].type == 1) {
          if (this.formData.limitTheNumber[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValueName: this.formData.limitTheNumber[i].tacticsUsers,
            parameterValue: this.formData.limitTheNumber[i].num
          }
        } else if (this.formData.limitTheNumber[i].type == 2) {
          if (this.formData.limitTheNumber[i].tacticsServer.length == 0) {
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValue: this.formData.limitTheNumber[i].tacticsServer,
            parameterValue: this.formData.limitTheNumber[i].num
          }
        } else if (this.formData.limitTheNumber[i].type == 3) {
          if (this.formData.limitTheNumber[i].tacticsServers.length == 0) {
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValueName: this.formData.limitTheNumber[i].tacticsServers,
            parameterValue: this.formData.limitTheNumber[i].num
          }
        }
        StrategyCategoryList.push(objs)
      }
      let data = {}
      if (this.templateName == '') {
        this.$prompt('请输入模板名称', '命名', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
        }).then(({ value }) => {
          // this.$message({
          //   type: 'success',
          //   message: '模板名称: ' + value
          // });
          layer.msg("模板名称:"+value, {
            icon: 1,
            closeBtn:1              
        });
          data = {
            id: this.formData.templet,
            name: value,
            templateUserGroupList: UserGroupList,
            templateHardwareList: HardwareList,
            templateStrategyCategoryList: StrategyCategoryList,
            licenseServiceId: this.tableRowID,
            permitFileId: this.formData.file
          }
          console.log(data)
          layer.load(0, {shade: [0.1]})
          let id = this.formData.file
          let that = this
           $.ajax({
          url: url + ":9001/StrategyApply/apply",
          type: "post",
          dateType: 'json',
          contentType: "application/json;charset-UTF-8",
          data: JSON.stringify(data),
          async: true,
          success: function (data) {
            //   console.log(data)
            layer.close(layer.load(0, {shade: [0.1]}))
            if (data.code == 200) {
                console.log(data)
                // that.$message(data.message)
                layer.msg(data.message, {
                  icon: 1,
                  closeBtn:1              
              });
                that.handleFlieChange(id)
              }else{
                // that.$message(data.message)
                layer.msg(data.message, {
                  icon: 3,
                  closeBtn:1              
              });
            }
          },
          error: function (data) {
            window.location.href = "../../error/error.html";
            console.log(data)
          }
        });
      // this.init (this.tableRowID)
          }).catch(() => {
            // this.$message({
            //   type: 'info',
            //   message: '取消输入'
            // });
            layer.msg("取消输入", {
              icon: 7,
              closeBtn:1              
          });
          });
        } else {
          data = {
            id: this.formData.templet,
            name: this.templateName,
            templateUserGroupList: UserGroupList,
            templateHardwareList: HardwareList,
            templateStrategyCategoryList: StrategyCategoryList,
            licenseServiceId: this.tableRowID,
            permitFileId: this.formData.file
          }
          console.log(data)
          layer.load(0, {shade: [0.1]})
          let id = this.formData.file
          let that = this
          $.ajax({
            url: url + ":9001/StrategyApply/apply",
            type: "post",
            dateType: 'json',
            contentType: "application/json;charset-UTF-8",
            data: JSON.stringify(data),
            async: true,
            success: function (data) {
              layer.close(layer.load(0, {shade: [0.1]}))
              // that.$message(data.message)
              if (data.code == 200) {
                console.log(data)
                  layer.msg(data.message, {
                    icon: 1,
                    closeBtn:1              
                });
                that.handleFlieChange(id)
              }else{
                // that.$message(data.message)
                layer.msg(data.message, {
                  icon: 3,
                  closeBtn:1              
              });
              }
            },
            error: function (data) {
              window.location.href = "../../error/error.html";
              console.log(data)
            }
          });
          // this.init (this.tableRowID)
        }
    },
    save () {
      const reg = /^[0-9a-zA-Z]*$/
      let UserGroupList = []
      let HardwareList = []
      let StrategyCategoryList = []
      for (const i in this.formData.user) {
        if(this.formData.user[i].name==''&&this.formData.user[i].selectNodes.length === 0){
          continue;
        }else{
          let arr = []
          if (!reg.test(this.formData.user[i].name)) {
            this.$message.error('用户组名称只能为英文字母');
            return;
          }
          if(this.formData.user[i].name===''){
            this.$message.error('请输入用户组名称');
            return;
          }
          if (this.formData.user[i].selectNodes.length === 0) {
            this.$message.error('请选择至少一个用户');
            return;
          }
          for (let j = 0; j < this.formData.user[i].selectNodes.length; j++) {
            arr.push(this.formData.user[i].selectNodes[j])
          }
          let obj = {
            userGroupName: this.formData.user[i].name,
            userIds: arr.join(",")
          }
          UserGroupList.push(obj)
        }
      }
      for (const i in this.formData.serverGroup) {
        if(this.formData.serverGroup[i].name==''&&this.formData.serverGroup[i].member.length === 0){
          continue;
        }else{
          let arr = []
          if (!reg.test(this.formData.serverGroup[i].name)) {
            this.$message.error('服务器组名称只能为英文字母');
            return;
          }
          if(this.formData.serverGroup[i].name===''){
            this.$message.error('请输入服务器组名称');
            return;
          }
          if (this.formData.serverGroup[i].member.length === 0) {
            this.$message.error('请选择至少一个服务器');
            return;
          }
          for (let j = 0; j < this.formData.serverGroup[i].member.length; j++) {
            arr.push(this.formData.serverGroup[i].member[j])
          }
          let obj = {
            hardwareGroupName: this.formData.serverGroup[i].name,
            hardwareIds: arr.join(",")
          }
          HardwareList.push(obj)
        }
      }
            //验证策略信息是否完整
      // this.fors1(this.formData.reserveTactics,StrategyCategoryList)预留
      for (const i in this.formData.reserveTactics) {
        // console.log(this.formData.reserveTactics[i])
        let str = ''
        for (const key in this.formData.modular) {
          if (this.formData.modular[key].id == this.formData.reserveTactics[i].modular) {
            str = this.formData.modular[key].name
          }
        }
        if (this.formData.reserveTactics[i].modular == '' || this.formData.reserveTactics[i].type == null || this.formData.reserveTactics[i].num == '') {
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1
        });
          return;
        }
        let objs = {}
        if (this.formData.reserveTactics[i].type === 0) {
          if (this.formData.reserveTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1
          });

            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValue: this.formData.reserveTactics[i].selectNodes[0],
            parameterValue: this.formData.reserveTactics[i].num
          }
        } else if (this.formData.reserveTactics[i].type == 1) {
          if (this.formData.reserveTactics[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1
          });
            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValueName: this.formData.reserveTactics[i].tacticsUsers,
            parameterValue: this.formData.reserveTactics[i].num
          }
        } else if (this.formData.reserveTactics[i].type == 2) {
          if (this.formData.reserveTactics[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValue: this.formData.reserveTactics[i].tacticsServer,
            parameterValue: this.formData.reserveTactics[i].num
          }
        } else if (this.formData.reserveTactics[i].type == 3) {
          if (this.formData.reserveTactics[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.reserveTactics[i].name,
            categoryId: this.formData.reserveTactics[i].id,
            moduleId: this.formData.reserveTactics[i].modular,
            moduleName:str,
            type: this.formData.reserveTactics[i].type,
            typeValueName: this.formData.reserveTactics[i].tacticsServers,
            parameterValue: this.formData.reserveTactics[i].num
          }
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors2(this.formData.authorizedUseTactics,StrategyCategoryList)授权使用
      for (const i in this.formData.authorizedUseTactics) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.authorizedUseTactics[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.authorizedUseTactics[i].modular == '' || this.formData.authorizedUseTactics[i].type == null) {
          // //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.authorizedUseTactics[i].type === 0) {
          console.log(this.formData.authorizedUseTactics[i])
          if (this.formData.authorizedUseTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValue: this.formData.authorizedUseTactics[i].selectNodes[0],
            parameterValue: null
          }
        } else if (this.formData.authorizedUseTactics[i].type == 1) {
          if (this.formData.authorizedUseTactics[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValueName: this.formData.authorizedUseTactics[i].tacticsUsers,
            parameterValue: null
          }
        } else if (this.formData.authorizedUseTactics[i].type == 2) {
          if (this.formData.authorizedUseTactics[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValue: this.formData.authorizedUseTactics[i].tacticsServer,
            parameterValue: null
          }
        } else if (this.formData.authorizedUseTactics[i].type == 3) {
          if (this.formData.authorizedUseTactics[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseTactics[i].name,
            categoryId: this.formData.authorizedUseTactics[i].id,
            moduleId: this.formData.authorizedUseTactics[i].modular,
            moduleName:str,
            type: this.formData.authorizedUseTactics[i].type,
            typeValueName: this.formData.authorizedUseTactics[i].tacticsServers,
            parameterValue: null
          }
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors4(this.formData.authorizedUseAllModulesTactics,StrategyCategoryList)授权使用所有模块
      for (const i in this.formData.authorizedUseAllModulesTactics) {
        if (this.formData.authorizedUseAllModulesTactics[i].type == null) {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.authorizedUseAllModulesTactics[i].type === 0) {
          if (this.formData.authorizedUseAllModulesTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
            categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
            moduleId: null,
            type: this.formData.authorizedUseAllModulesTactics[i].type,
            typeValue: this.formData.authorizedUseAllModulesTactics[i].selectNodes[0],
            parameterValue: null
          }
          }else if (this.formData.authorizedUseAllModulesTactics[i].type == 1) {
            if (this.formData.authorizedUseAllModulesTactics[i].tacticsUsers.length == 0) {
              // this.$alert('请选择用户组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择用户组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
              categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.authorizedUseAllModulesTactics[i].type,
              typeValueName: this.formData.authorizedUseAllModulesTactics[i].tacticsUsers,
              parameterValue:null
            }
          } else if (this.formData.authorizedUseAllModulesTactics[i].type == 2) {
            if (this.formData.authorizedUseAllModulesTactics[i].tacticsServer.length == 0) {
              // this.$alert('请选择服务器', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
              categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.authorizedUseAllModulesTactics[i].type,
              typeValue: this.formData.authorizedUseAllModulesTactics[i].tacticsServer,
              parameterValue: null
            }
          } else if (this.formData.authorizedUseAllModulesTactics[i].type == 3) {
            if (this.formData.authorizedUseAllModulesTactics[i].tacticsServers.length == 0) {
              // this.$alert('请选择服务器组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.authorizedUseAllModulesTactics[i].name,
              categoryId: this.formData.authorizedUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.authorizedUseAllModulesTactics[i].type,
              typeValueName: this.formData.authorizedUseAllModulesTactics[i].tacticsServers,
              parameterValue: null
            }
          }
        
        StrategyCategoryList.push(objs)
      }
      // this.fors2(this.formData.prohibitionOfUseTactics,StrategyCategoryList)禁止使用
      for (const i in this.formData.prohibitionOfUseTactics) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.prohibitionOfUseTactics[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.prohibitionOfUseTactics[i].modular == '' || this.formData.prohibitionOfUseTactics[i].type == null) {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.prohibitionOfUseTactics[i].type === 0) {
          if (this.formData.prohibitionOfUseTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValue: this.formData.prohibitionOfUseTactics[i].selectNodes[0],
            parameterValue: null
          }
        } else if (this.formData.prohibitionOfUseTactics[i].type == 1) {
          if (this.formData.prohibitionOfUseTactics[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValueName: this.formData.prohibitionOfUseTactics[i].tacticsUsers,
            parameterValue: null
          }
        } else if (this.formData.prohibitionOfUseTactics[i].type == 2) {
          if (this.formData.prohibitionOfUseTactics[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValue: this.formData.prohibitionOfUseTactics[i].tacticsServer,
            parameterValue: null
          }
        } else if (this.formData.prohibitionOfUseTactics[i].type == 3) {
          if (this.formData.prohibitionOfUseTactics[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseTactics[i].name,
            categoryId: this.formData.prohibitionOfUseTactics[i].id,
            moduleId: this.formData.prohibitionOfUseTactics[i].modular,
            moduleName:str,
            type: this.formData.prohibitionOfUseTactics[i].type,
            typeValueName: this.formData.prohibitionOfUseTactics[i].tacticsServers,
            parameterValue: null
          }
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors4(this.formData.prohibitionOfUseAllModulesTactics,StrategyCategoryList)禁止使用所有模块
      for (const i in this.formData.prohibitionOfUseAllModulesTactics) {
        if (this.formData.prohibitionOfUseAllModulesTactics[i].type == null) {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.prohibitionOfUseAllModulesTactics[i].type === 0) {
          if (this.formData.prohibitionOfUseAllModulesTactics[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
            categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
            moduleId: null,
            type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
            typeValue: this.formData.prohibitionOfUseAllModulesTactics[i].selectNodes[0],
            parameterValue: null
          }
          }else if (this.formData.prohibitionOfUseAllModulesTactics[i].type == 1) {
            if (this.formData.prohibitionOfUseAllModulesTactics[i].tacticsUsers.length == 0) {
              // this.$alert('请选择用户组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择用户组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
              categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
              typeValueName: this.formData.prohibitionOfUseAllModulesTactics[i].tacticsUsers,
              parameterValue:null
            }
          } else if (this.formData.prohibitionOfUseAllModulesTactics[i].type == 2) {
            if (this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServer.length == 0) {
              // this.$alert('请选择服务器', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
              categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
              typeValue: this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServer,
              parameterValue: null
            }
          } else if (this.formData.prohibitionOfUseAllModulesTactics[i].type == 3) {
            if (this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServers.length == 0) {
              // this.$alert('请选择服务器组', '', {
              //   confirmButtonText: '确定',
              //   callback: action => {
              //     this.$message({
              //       type: 'info',
              //       message: `错误: 策略信息不完善`
              //     });
              //   }
              // });
              layer.msg("请选择服务器组", {
                icon: 2,
                closeBtn:1              
            });
              return;
            }
            objs = {
              categoryName: this.formData.prohibitionOfUseAllModulesTactics[i].name,
              categoryId: this.formData.prohibitionOfUseAllModulesTactics[i].id,
              moduleId: null,
              type: this.formData.prohibitionOfUseAllModulesTactics[i].type,
              typeValueName: this.formData.prohibitionOfUseAllModulesTactics[i].tacticsServers,
              parameterValue: null
            }
          }
        
        StrategyCategoryList.push(objs)
      }
      // this.fors3(this.formData.setFreeTimeTactics,StrategyCategoryList)设置超时时间
      for (const i in this.formData.setFreeTimeTactics) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.setFreeTimeTactics[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.setFreeTimeTactics[i].modular == '' || this.formData.setFreeTimeTactics[i].num == '') {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        objs = {
          categoryName: this.formData.setFreeTimeTactics[i].name,
          categoryId: this.formData.setFreeTimeTactics[i].id,
          moduleId: this.formData.setFreeTimeTactics[i].modular,
          moduleName:str,
          type: null,
          typeValue: null,
          parameterValue: this.formData.setFreeTimeTactics[i].num
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors5(this.formData.setAllFreeTimeTactics,StrategyCategoryList)设置所有模块超时时间
      for (const i in this.formData.setAllFreeTimeTactics) {
        if (this.formData.setAllFreeTimeTactics[i].num == '') {
          //console.log("1111")
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        objs = {
          categoryName: this.formData.setAllFreeTimeTactics[i].name,
          categoryId: this.formData.setAllFreeTimeTactics[i].id,
          moduleId: null,
          type: null,
          typeValue: null,
          parameterValue: this.formData.setAllFreeTimeTactics[i].num
        }
        StrategyCategoryList.push(objs)
      }
      // this.fors1(this.formData.limitTheNumber,StrategyCategoryList)限定许可使用数量
      for (const i in this.formData.limitTheNumber) {
        let str = ''
          for (const key in this.formData.modular) {
            if (this.formData.modular[key].id == this.formData.limitTheNumber[i].modular) {
              str = this.formData.modular[key].name
            }
          }
        if (this.formData.limitTheNumber[i].modular == '' || this.formData.limitTheNumber[i].type == null || this.formData.limitTheNumber[i].num == '') {
          // this.$alert('请完善策略信息', '策略信息不完整', {
          //   confirmButtonText: '确定',
          //   callback: action => {
          //     this.$message({
          //       type: 'info',
          //       message: `错误: 策略信息不完善`
          //     });
          //   }
          // });
          layer.msg("策略信息不完整", {
            icon: 2,
            closeBtn:1              
        });
          return;
        }
        let objs = {}
        if (this.formData.limitTheNumber[i].type === 0) {
          if (this.formData.limitTheNumber[i].selectNodes.length == 0) {
            // this.$alert('请选择用户', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValue: this.formData.limitTheNumber[i].selectNodes[0],
            parameterValue: this.formData.limitTheNumber[i].num
          }
        } else if (this.formData.limitTheNumber[i].type == 1) {
          if (this.formData.limitTheNumber[i].tacticsUsers.length == 0) {
            // this.$alert('请选择用户组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择用户组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValueName: this.formData.limitTheNumber[i].tacticsUsers,
            parameterValue: this.formData.limitTheNumber[i].num
          }
        } else if (this.formData.limitTheNumber[i].type == 2) {
          if (this.formData.limitTheNumber[i].tacticsServer.length == 0) {
            // this.$alert('请选择服务器', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValue: this.formData.limitTheNumber[i].tacticsServer,
            parameterValue: this.formData.limitTheNumber[i].num
          }
        } else if (this.formData.limitTheNumber[i].type == 3) {
          if (this.formData.limitTheNumber[i].tacticsServers.length == 0) {
            // this.$alert('请选择服务器组', '', {
            //   confirmButtonText: '确定',
            //   callback: action => {
            //     this.$message({
            //       type: 'info',
            //       message: `错误: 策略信息不完善`
            //     });
            //   }
            // });
            layer.msg("请选择服务器组", {
              icon: 2,
              closeBtn:1              
          });
            return;
          }
          objs = {
            categoryName: this.formData.limitTheNumber[i].name,
            categoryId: this.formData.limitTheNumber[i].id,
            moduleId: this.formData.limitTheNumber[i].modular,
            moduleName:str,
            type: this.formData.limitTheNumber[i].type,
            typeValueName: this.formData.limitTheNumber[i].tacticsServers,
            parameterValue: this.formData.limitTheNumber[i].num
          }
        }
        StrategyCategoryList.push(objs)
      }
      this.$prompt('请输入模板名称', '命名', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
      }).then(({ value }) => {
        console.log(UserGroupList)
        console.log(HardwareList)
        console.log(StrategyCategoryList)
        let data = {
          id: this.formData.templet,
          name: value,
          templateUserGroupList: UserGroupList,
          templateHardwareList: HardwareList,
          templateStrategyCategoryList: StrategyCategoryList,
          licenseServiceId: this.tableRowID,
          permitFileId: this.formData.file

        }
        console.log(data)
        layer.load(0, {shade: [0.1]})
        let id = this.formData.file
        let that = this
        $.ajax({
          url: url + ":9001/licenseServiceDetails/saveTemplate",
          type: "post",
          dateType: 'json',
          contentType: "application/json;charset-UTF-8",
          data: JSON.stringify(data),
          async: true,
          success: function (data) {
            //   console.log(data)
            layer.close(layer.load(0, {shade: [0.1]}))
            if (data.code == 200) {
              console.log(data)
              // that.$message(data.message)
              layer.msg(data.message, {
                icon: 1,
                closeBtn:1              
            });
              that.handleFlieChange(id)
            }else{
              // that.$message(data.message)
              layer.msg(data.message, {
                icon: 3,
                closeBtn:1              
            });
            }
          },
          error: function (data) {
            window.location.href = "../../error/error.html";
            console.log(data)
          }
        });
        // this.init (this.tableRowID)
      }).catch(() => {
        // this.$message({
        //   type: 'info',
        //   message: '取消输入'
        // });
        layer.msg('取消输入', {
          icon: 7,
          closeBtn:1              
      });
      });

    }
  },
})