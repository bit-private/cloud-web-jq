//资源授权管理

//时间戳的处理
layui.laytpl.toDateString = function (d, format) {

    // var date = new Date(d || new Date())
    if (d == null) {
        return ""
    }
    var date = new Date(d)

        , ymd = [
            this.digit(date.getFullYear(), 4)
            , this.digit(date.getMonth() + 1)
            , this.digit(date.getDate())
        ]
        , hms = [
            this.digit(date.getHours())
            , this.digit(date.getMinutes())
            , this.digit(date.getSeconds())
        ];

    format = format || 'yyyy-MM-dd HH:mm:ss';

    return format.replace(/yyyy/g, ymd[0])
        .replace(/MM/g, ymd[1])
        .replace(/dd/g, ymd[2])
        .replace(/HH/g, hms[0])
        .replace(/mm/g, hms[1])
        .replace(/ss/g, hms[2]);
};

//数字前置补零
layui.laytpl.digit = function (num, length, end) {
    var str = '';
    num = String(num);
    length = length || 2;
    for (var i = num.length; i < length; i++) {
        str += '0';
    }
    return num < Math.pow(10, length) ? str + (num | 0) : num;
};
(function () {
    const baseURL_SON = url + ":9002/authorization";
    //const baseURL_SON = "http://localhost:9001/authorization";

    //页面变量

    //判断是用户还是部门
    var newOrEdit;  //1是新建 2是编辑
    //用户所在科室
    var department = "";
    //新建用户表格数据
    var userChecked;
    //已选用户或单位展示数据
    var overUserDepartment = [];
    //判断是部门还是用户
    var authType = "";
    //判断是组还是具体设备
    var hardwareType = "";
    //判断资源组有没有设备
    var groupEquipmentFlag;
    //添加的软件
    var addSoftware = "";
    //新建按资源组分类设备
    var groupEquipment = [];
    //新建按资源组分类设备ID
    var groupEquipmentID = "";
    //新建按类型分类设备
    var classEquipment = [];
    //新建按类型分类设备ID
    // var classEquipmentID = "";
    //单选按类型分类设备
    // var radioclassEquipment = [];
    //已选设备
    var overEquipment = [];
    var overEquipmentSon = 1;
    //新建编辑运行系统
    var osType = "";
    var groupId = "";
    //新建编辑所属的桌面类型
    var linkType = "";
    //编辑功能
    //编辑id
    var userOrEquipmentId = "";
    //搜索
    var searchName;


    layui.use(['element', 'table', 'form', 'laydate', 'upload', 'layer', 'laypage', 'tree'], function () {
        var element = layui.element
            , table = layui.table
            , form = layui.form
            , upload = layui.upload
            , layer = layui.layer
            , laypage = layui.laypage
            , tree = layui.tree
            , laydate = layui.laydate
            , date = new Date();

        //echarts
        $(document).ready(function () {
            active.getAuthRelationList();
        })


        var param = {
            departmentId: '',
            userId: ''
        }
        var active = {
            //初始化
            init: function (params) {
                this.tableList1();
                this.getTableListPage1();
                // this.getAuthRelationList();
            },
            //获取表格
            tableList1: function (pageNum, searchName, mode, sortString) {
                // console.log(pageNum,searchName,mode,sortString)
                $.ajax({
                    url: baseURL_SON + "/getAuthorizationPage",
                    // url:"http://10.1.3.27:9002/authorization/getAuthorizationPage",
                    type: "post",
                    async: true,
                    headers: {
                        "token": getCookie("token")
                    },
                    data: { page: pageNum == undefined ? 1 : pageNum, size: 10, searchName: searchName == undefined ? '' : searchName, mode: mode == undefined ? '' : mode, sortString: sortString == undefined ? '' : sortString, departmentId: param.departmentId, userId: param.userId },
                    success: function (data) {
                        console.log(data,'获取表格')
                        if (data.code == 200) {
                            table.render({
                                elem: '#resourceList'
                                , cols: [[
                                    { type: 'checkbox' }
                                    , { field: 'realName', title: '用户', event: 'editUserOrEquipment', templet: '#barDemo1', style: 'cursor: pointer;', sort: 'true' }
                                    , { field: 'orgName', title: '院所' }
                                    , { field: 'department', title: '科室' }
                                    , { field: 'applicationName', title: '软件', sort: 'true' }
                                    , { field: 'version', title: '版本', sort: 'true' }
                                    , { field: 'hardwareName', title: '设备/组', templet: '#barDemo2', style: 'cursor: pointer;', sort: 'true', event: 'demo' }
                                    , { field: 'systemName', title: '运行系统' }
                                    // ,{field:'authDate', title: '过期时间',sort:true,templet: '<div>{{ layui.laytpl.toDateString(d.authDate) }}</div>'}
                                    // ,{field:'authDate', title: '过期时间',sort:true,templet: '#barDemo3'}
                                    , { field: 'authDate', title: '过期时间', sort: true }
                                ]]
                                , data: data.data.content
                            });
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //获取表格1的页码
            getTableListPage1: function (searchName, mode, sortString) {
                $.ajax({
                    url: baseURL_SON + "/getAuthorizationPage",
                    // url:"http://10.1.3.152:9002/authorization/getAuthorizationPage",
                    type: "post",
                    async: true,
                    headers: {
                        "token": getCookie("token")
                    },
                    data: { page: 1, size: 10, searchName: searchName == undefined ? '' : searchName, mode: mode == undefined ? '' : mode, sortString: sortString == undefined ? '' : sortString, departmentId: param.departmentId, userId: param.userId },
                    success: function (data) {
                        if (data.code == 200) {
                            laypage.render({
                                elem: "resourcePage",
                                count: data.data.pageCount
                                , layout: ['prev', 'page', 'next', 'skip']
                                , limit: 10
                                , jump: function (obj, first) {
                                    //obj包含了当前分页的所有参数，比如：
                                    // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                    // console.log(obj.limit); //得到每页显示的条数
                                    // console.log(first);
                                    //首次不执行
                                    if (!first) {
                                        active.tableList1(obj.curr, searchName, mode, sortString);
                                        //do something
                                    }
                                }
                            })
                        }
                        else if (data.code == 400) {
                            // console.log(2222222222222)
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //新建弹框
            newlyBuild: function () {
                layer.closeAll('loading'); //关闭加载层
                layer.open({
                    type: 1
                    , title: [""]
                    , closeBtn: 1
                    , area: ['680px', '650px']
                    , shade: 0.8
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.layui-content-main')
                    // ,success: function(layero){
                    //     var btn = layero.find('.layui-layer-btn');
                    //     // btn.find('.layui-layer-btn0').attr({
                    //     //     href: 'http://www.layui.com/'
                    //     //     ,target: '_blank'
                    //     // });
                    //     btn.find('.layui-layer-btn0').click(function(){
                    //         // alert(1111111111)
                    //     });
                    // }
                    , end: function (index, layero) {
                        active.clearData();
                        $(".layui-content-main").hide()
                    }
                });
            },
            //获取机构列表     获取新建用户、部门树
            getOrgList: function () {
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                $.ajax({
                    url: baseURL_SON + "/getOrgList",
                    type: "post",
                    async: true,
                    headers: {
                        "token": getCookie("token")
                    },
                    success: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        // console.log(JSON.stringify(data.data));
                        if (data.code == 200) {
                            active.treeListUserDepartment(data.data)
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // console.log(JSON.stringify(data));
                    },
                    // error:function(data){
                    //     layer.closeAll('loading'); //关闭加载层
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //新建用户、部门树渲染
            treeListUserDepartment: function (nodes) {
                // console.log(nodes)
                $('#demo1').empty();
                $('#demo2').empty();
                tree({
                    elem: '#demo1'
                    , nodes
                    , click: function (node) {
                        console.log(node)
                        department = node.name;
                        // groupId = node.id;
                        active.getUserListByOrgId(node.id);
                        // console.log(node) //node即为当前点击的节点数据
                    }
                });
                tree({
                    elem: '#demo2'
                    , nodes
                    , click: function (node) {
                        console.log(node);
                        if (node.flag < 3) {
                            layer.msg('只能选择科级和以下职级', {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        else {
                            department = node.name;
                            authType = "org";
                            overUserDepartment = [];
                            var obj = {}
                            obj.id = node.id;
                            obj.userName = node.name;
                            overUserDepartment.push(obj);
                            // console.log(overUserDepartment);
                            active.overUserDepartment(overUserDepartment);
                        }

                        // console.log(node.id) //node即为当前点击的节点数据
                    }
                });
            },
            //根据机构查询具体人数
            getUserListByOrgId: function (orgId) {
                // console.log(orgId);
                $.ajax({
                    url: baseURL_SON + "/getUserListByOrgId",
                    type: "post",
                    async: true,
                    data: { orgId },
                    headers: {
                        "token": getCookie("token")
                    },
                    success: function (data) {
                        // layer.closeAll('loading'); //关闭加载层
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            userChecked = data.data;
                            active.tab1Date(userChecked)
                            // table.render({
                            //     elem: '#tab1'
                            //     ,cols: [[
                            //         {type:'checkbox'}
                            //         ,{field:'realName', title: '用户'}
                            //     ]]
                            //     ,data:userChecked
                            // });
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // console.log(JSON.stringify(data));
                    },
                    // error:function(data){
                    //     // layer.closeAll('loading'); //关闭加载层
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //tab1数据表格
            tab1Date: function (data) {
                table.render({
                    elem: '#tab1'
                    , cols: [[
                        { type: 'checkbox' }
                        , { field: 'realName', title: '用户' }
                    ]]
                    , data
                });
            },
            //已选用户或部门表格
            overUserDepartment: function (data) {
                table.render({
                    elem: '#tab2'
                    , cols: [[
                        { type: 'checkbox', LAY_CHECKED: true }
                        , { field: 'realName', title: '已选' }
                    ]],
                    data,
                });
            },
            //资源组表格
            resourceGroupTable: function (data) {
                if (newOrEdit == 1) {
                    if (groupEquipmentFlag == 1) {
                        console.log(JSON.stringify(data))
                        table.render({
                            elem: '#tab3'
                            , cols: [[
                                { field: 'name', title: '设备名' }
                            ]],
                            data,
                        });
                    }
                    else {
                        // console.log(JSON.stringify(data));
                        // table.render({
                        //     elem: '#tab3'
                        //     ,cols: [[
                        //         {type:'checkbox'}
                        //         ,{field:'name', title: '设备名'}
                        //     ]],
                        //     data,
                        // });
                        table.render({
                            elem: '#tab3'
                            , cols: [[
                                { title: '选择', toolbar: "#radioTpl1", width: 80, event: 'radiodemo1' }
                                , { field: 'name', title: '设备名', sort: true }
                            ]],
                            data,
                        });
                    }
                }
                else if (newOrEdit == 2) {
                    if (groupEquipmentFlag == 1) {
                        table.render({
                            elem: '#tab7'
                            , cols: [[
                                { field: 'name', title: '设备名' }
                            ]],
                            data,
                        });
                    }
                    else {
                        // table.render({
                        //     elem: '#tab7'
                        //     ,cols: [[
                        //         {type:'checkbox'}
                        //         ,{field:'name', title: '设备名'}
                        //     ]],
                        //     data,
                        // });
                        table.render({
                            elem: '#tab7'
                            , cols: [[
                                { title: '选择', toolbar: "#radioTpl2", width: 80, event: 'radiodemo2' }
                                , { field: 'name', title: '设备名', sort: true }
                            ]],
                            data,
                        });
                    }
                }

            },
            //类型表格
            classTable: function (data) {
                if (newOrEdit == 1) {
                    // table.render({
                    //     elem: '#tab4'
                    //     ,cols: [[
                    //         {type:'checkbox'}
                    //         ,{field:'name', title: '设备名'}
                    //     ]],
                    //     data,
                    // });
                    table.render({
                        elem: '#tab4'
                        , cols: [[
                            { title: '选择', toolbar: "#radioTpl1", width: 80, event: 'radiodemo1' }
                            , { field: 'name', title: '设备名', sort: true }
                        ]],
                        data,
                    });
                }
                else if (newOrEdit == 2) {
                    // table.render({
                    //     elem: '#tab8'
                    //     ,cols: [[
                    //         {type:'checkbox'}
                    //         ,{field:'name', title: '设备名'}
                    //     ]],
                    //     data,
                    // });
                    table.render({
                        elem: '#tab8'
                        , cols: [[
                            { title: '选择', toolbar: "#radioTpl2", width: 80, event: 'radiodemo2' }
                            , { field: 'name', title: '设备名', sort: true }
                        ]],
                        data,
                    });
                }
            },
            //已选设备
            overEquipment: function (data) {
                if (newOrEdit == 1) {
                    table.render({
                        elem: '#tab5'
                        , cols: [[
                            { type: 'checkbox', LAY_CHECKED: true }
                            , { field: 'name', title: '已选' }
                        ]],
                        data,
                    });
                }
                else if (newOrEdit == 2) {
                    table.render({
                        elem: '#tab9'
                        , cols: [[
                            { type: 'checkbox', LAY_CHECKED: true }
                            , { field: 'name', title: '已选' }
                        ]],
                        data,
                    });
                }
            },
            //获取添加软件列表
            getApplication: function (page, searchName) {
                $.ajax({
                    url: baseURL_SON + "/getApplicationList",
                    // url:"http://10.1.3.152:9002/authorization/getApplicationPage",
                    type: "post",
                    async: true,
                    headers: {
                        "token": getCookie("token")
                    },
                    data: { page: page == undefined ? 1 : page, size: 10, searchName: searchName == undefined ? "" : searchName },
                    success: function (data) {
                        console.log(data)
                        // layer.closeAll('loading'); //关闭加载层
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            if (newOrEdit == 1) {
                                table.render({
                                    elem: '#addSoftware'
                                    , cols: [[
                                        { title: '选择', toolbar: "#radioTpl", width: 80, event: 'radiodemo' }
                                        , { field: 'softwareName', width: 110, title: '名称', sort: true }
                                        , { field: 'softwareVersionName', width: 110, title: '版本', sort: true }
                                        , { field: 'applicationClassficationName', width: 120, title: '分类' }
                                        , { field: 'linkName', width: 100, title: '桌面类型' }
                                        , { field: 'systemName', title: '运行系统', width: 130 }
                                    ]],
                                    data: data.data
                                });
                            }
                            else if (newOrEdit == 2) {
                                table.render({
                                    elem: '#editSoftware'
                                    , cols: [[
                                        { title: '选择', toolbar: "#radioTpl", width: 80, event: 'radiodemo' }
                                        , { field: 'softwareName', width: 110, title: '名称' }
                                        , { field: 'softwareVersionName', width: 110, title: '版本', sort: true }
                                        , { field: 'applicationClassficationName', width: 120, title: '分类' }
                                        , { field: 'linkName', width: 100, title: '桌面类型' }
                                        , { field: 'systemName', title: '运行系统', width: 130 }
                                    ]],
                                    data: data.data
                                });
                            }
                            // active.treeListUserDepartment(data.data)
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // console.log(JSON.stringify(data));
                    },
                    // error:function(data){
                    //     // layer.closeAll('loading'); //关闭加载层
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //获取添加软件列表分页
            getApplicationPage: function (page, searchName) {
                $.ajax({
                    url: baseURL_SON + "/getApplicationList",
                    type: "post",
                    async: true,
                    headers: {
                        "token": getCookie("token")
                    },
                    data: { page: 1, size: 10, searchName: searchName == undefined ? "" : searchName },
                    success: function (data) {
                        if (data.code == 200) {
                            if (newOrEdit == 1) {
                                laypage.render({
                                    elem: "addSoftwarePage",
                                    count: data.data.pageCount
                                    , layout: ['prev', 'page', 'next', 'skip']
                                    , limit: 10
                                    , jump: function (obj, first) {
                                        //obj包含了当前分页的所有参数，比如：
                                        // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                        // console.log(obj.limit); //得到每页显示的条数
                                        // console.log(first);
                                        //首次不执行
                                        if (!first) {
                                            active.getApplication(obj.curr, searchName);
                                            //do something
                                        }
                                    }
                                })
                            }
                            else if (newOrEdit == 2) {
                                laypage.render({
                                    elem: "editSoftwarePage",
                                    count: data.data.pageCount
                                    , layout: ['prev', 'page', 'next', 'skip']
                                    , limit: 10
                                    , jump: function (obj, first) {
                                        //obj包含了当前分页的所有参数，比如：
                                        // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                        // console.log(obj.limit); //得到每页显示的条数
                                        // console.log(first);
                                        //首次不执行
                                        if (!first) {
                                            active.getApplication(obj.curr, searchName);
                                            //do something
                                        }
                                    }
                                })
                            }

                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //获取资源组
            getHardwareGroupList: function (osType, linkType) {
                // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                $.ajax({
                    url: baseURL_SON + "/getHardwareGroupTree",
                    // url:"http://10.1.3.152:9002/authorization/getHardwareGroupList",
                    type: "post",
                    async: true,
                    data: { osType, linkType },
                    headers: {
                        "token": getCookie("token")
                    },
                    success: function (data) {
                        // layer.closeAll('loading'); //关闭加载层
                        console.log(data, '资源组');
                        if (data.code == 200) {
                            if (data.data == '') {
                                active.resourceGroupTable([]);
                            }
                            if (newOrEdit == 1) {
                                $('#demo3').empty();
                                tree({
                                    elem: '#demo3'
                                    , nodes: data.data
                                    , click: function (node) {
                                        groupEquipmentID = node.id;
                                        if (node.children) {
                                            hardwareType = "group";
                                            overEquipment = [];
                                            overEquipment.push(node);
                                            // console.log(overEquipment);
                                            active.overEquipment(overEquipment);
                                            if (node.children.length <= 0) {
                                                console.log(node.id)
                                                groupEquipmentFlag = 1;
                                                groupEquipment = [];
                                                groupEquipment.push(node);
                                                active.resourceGroupTable(groupEquipment);
                                            }
                                            else {
                                                groupEquipmentFlag = 2;
                                                groupEquipment = node.children;
                                                // console.log(JSON.stringify(groupEquipment))
                                                active.resourceGroupTable(node.children);
                                            }

                                        }
                                        console.log(node) //node即为当前点击的节点数据
                                    }
                                });
                            }
                            else if (newOrEdit == 2) {
                                $('#demo5').empty();
                                tree({
                                    elem: '#demo5'
                                    , nodes: data.data
                                    , click: function (node) {
                                        groupEquipmentID = node.id;
                                        if (node.children) {
                                            // console.log(node);
                                            hardwareType = "group";
                                            overEquipment = [];
                                            overEquipment.push(node);
                                            active.overEquipment(overEquipment);
                                            if (node.children.length <= 0) {
                                                groupEquipmentFlag = 1;
                                                groupEquipment = [];
                                                groupEquipment.push(node);
                                                active.resourceGroupTable(groupEquipment);
                                            }
                                            else {
                                                groupEquipmentFlag = 2;
                                                groupEquipment = node.children;
                                                active.resourceGroupTable(node.children);
                                            }

                                        }
                                        // console.log(node) //node即为当前点击的节点数据
                                    }
                                });
                            }
                            // active.treeListUserDepartment(data.data)
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // console.log(JSON.stringify(data));
                    },
                    // error:function(data){
                    //     // layer.closeAll('loading'); //关闭加载层
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            // 获取类型
            getHardwareTree: function (osType, linkType) {
                // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                $.ajax({
                    url: baseURL_SON + "/getHardwareTree",
                    type: "post",
                    async: true,
                    headers: {
                        "token": getCookie("token")
                    },
                    data: { osType, linkType },
                    success: function (data) {
                        // layer.closeAll('loading'); //关闭加载层
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            if (newOrEdit == 1) {
                                $('#demo4').empty();
                                tree({
                                    elem: '#demo4'
                                    , nodes: data.data
                                    , click: function (node) {
                                        // classEquipmentID = node.id;
                                        groupEquipmentID = "";
                                        if (node.children) {
                                            if (node.children.length > 0) {
                                                // hardwareType = "group";
                                                //类型组变量
                                                // overEquipment = [];
                                                // overEquipment.push(node);
                                                // active.overEquipment(overEquipment);
                                                classEquipment = node.children;
                                                active.classTable(node.children);
                                                // console.log(JSON.stringify(node.children))
                                            }
                                            else {
                                                active.classTable();
                                            }
                                        }

                                        console.log(node) //node即为当前点击的节点数据
                                    }
                                });
                            }
                            else if (newOrEdit == 2) {
                                $('#demo6').empty();
                                tree({
                                    elem: '#demo6'
                                    , nodes: data.data
                                    , click: function (node) {
                                        // classEquipmentID = node.id;
                                        // console.log(node.id)
                                        groupEquipmentID = "";
                                        // if(node.children.length>0){
                                        if (node.children.length > 0) {
                                            // console.log(node.id);
                                            // hardwareType = "group";
                                            //类型组变量
                                            // overEquipment = [];
                                            // overEquipment.push(node);
                                            // active.overEquipment(overEquipment);
                                            classEquipment = node.children;
                                            active.classTable(node.children);
                                            // console.log(JSON.stringify(node.children))
                                        }
                                        else {
                                            active.classTable();
                                        }
                                        // console.log(node) //node即为当前点击的节点数据
                                    }
                                });
                            }
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // console.log(JSON.stringify(data));
                    },
                    // error:function(data){
                    //     // layer.closeAll('loading'); //关闭加载层
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //新建、编辑提交
            newlyBuildSubmission: function (List) {
                // console.log(JSON.stringify(List));
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                if (newOrEdit == 1) {
                    $.ajax({
                        url: baseURL_SON + "/saveAuthorizations",
                        type: "post",
                        async: true,
                        headers: {
                            "Content-Type": "application/json;charset=UTF-8",
                            "token": getCookie("token")
                        },
                        // data:JSON.stringify(List),
                        data: JSON.stringify(List),
                        success: function (data) {
                            layer.closeAll('loading'); //关闭加载层
                            // console.log(JSON.stringify(data));
                            if (data.code == 200) {
                                layer.closeAll(); //关闭所有层
                                layer.msg(data.message, {
                                    icon: 1,
                                    closeBtn: 1
                                })
                                active.tableList1();
                                active.getTableListPage1();
                                active.getAuthRelationList();
                            }
                            else if (data.code == 400) {
                                layer.msg(data.message, {
                                    icon: 2,
                                    closeBtn: 1
                                })
                            }
                            // console.log(JSON.stringify(data));
                        },
                        // error:function(data){
                        //     layer.closeAll('loading'); //关闭加载层
                        //     window.location.href = "../../error/error.html";
                        //     console.log(data)
                        // }
                    });
                }
                else if (newOrEdit == 2) {
                    // console.log(JSON.stringify(List))
                    $.ajax({
                        // url:"http://10.1.3.152:9002/authorization/updateAuthorization",
                        url: baseURL_SON + "/updateAuthorization",
                        // url:baseURL_SON + "/updateAuthorizations",
                        type: "post",
                        async: true,
                        headers: {
                            "Content-Type": "application/json;charset=UTF-8",
                            "token": getCookie("token")
                        },
                        // headers:{"Content-Type":"application/x-www-form-urlencoded;charset=UTF-8"},
                        data: JSON.stringify(List),
                        // data:List,
                        // data:{id:"b48c1fc1-0c5f-4c3d-83d8-ba461d493183",hardwareType:"single",userId:"d627e5d4-1327-4184-b8be-05e1c9f80296",userName:"abc5",department:"北京月新时代",authType:"user",applicationId:"d39f6bf4-a567-4a57-bd6b-f2abd4faf27e",groupId:"",authDate:"2099-01-01",hardwareId:"8b67188c-0d54-4a84-8ecb-df415a71864c"},
                        success: function (data) {
                            layer.closeAll('loading'); //关闭加载层
                            console.log(data, 'updateAuthorization');
                            if (data.code == 200) {
                                layer.closeAll(); //关闭所有层
                                layer.msg(data.message, {
                                    icon: 1,
                                    closeBtn: 1
                                })
                                active.tableList1();
                                active.getTableListPage1();
                                active.getAuthRelationList();
                            }
                            else if (data.code == 400) {
                                layer.msg(data.message, {
                                    icon: 2,
                                    closeBtn: 1
                                })
                            }
                            // console.log(JSON.stringify(data));
                        },
                        // error:function(data){
                        //     layer.closeAll('loading'); //关闭加载层
                        //     // window.location.href = "../../error/error.html";
                        //     console.log(data)
                        // }
                    });
                }
            },
            //编辑弹框
            editBuild: function () {
                // console.log(111111111)
                layer.closeAll('loading'); //关闭加载层
                layer.open({
                    type: 1
                    , title: [''] //不显示标题栏
                    , closeBtn: 1
                    , tipsMore: false
                    , area: ['680px', '650px']
                    , shade: 0.8
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.layui-content-edit')
                    , success: function (layero) {

                    }
                    , end: function (index, layero) {
                        active.clearData();
                        $(".layui-content-edit").hide()
                    }
                });
            },
            //编辑用户或部门显示
            showUserOrEquipment: function (data) {
                // console.log(JSON.stringify(data));
                table.render({
                    elem: '#tab6'
                    , cols: [[
                        { field: 'realName', title: '已选' }
                    ]]
                    , data,
                });
            },
            // 删除
            delResource: function (ids) {
                $.ajax({
                    url: baseURL_SON + "/deleteAuthorizations",
                    type: "post",
                    async: true,

                    data: { ids },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        layer.closeAll('loading'); //关闭加载层
                        if (data.code == 200) {
                            layer.msg('删除成功', {
                                icon: 1,
                                closeBtn: 1
                            });
                            active.tableList1();
                            active.getTableListPage1();
                            active.getAuthRelationList();
                        }
                        else if (data.code == 400) {
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     layer.closeAll('loading'); //关闭加载层
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //获取关系图接口
            getAuthRelationList: function (searchName) {
                $.ajax({
                    url: baseURL_SON + "/getAuthRelationList",
                    type: "post",
                    async: true,
                    data: { searchName },
                    headers: {
                        "token": getCookie("token")
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            active.getEcharts(data.data);
                        }
                        else if (data.code == 400) {
                            // console.log(333333333333)
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //echarts关系图
            getEcharts: function (JSON) {
                //	app.title = '力引导布局';
                var myChart = echarts.init(document.getElementById('main'));
                var graph = JSON;
                var categories = [];
                categories[0] = "user";
                categories[1] = "application";
                categories[2] = "hardware";
                // for (var i = 0; i < 3; i++) {
                //     categories[i] = {
                //         name: '类型' + i
                //     };
                // }
                graph.nodes.forEach(function (node) {
                    node.itemStyle = null;//
                    node.symbolSize = node.size;//强制指定节点的大小
                    // Use random x, y
                    node.x = node.y = null;
                    node.draggable = true;
                });
                option = {
                    // title: {
                    //     text: 'demo',//文本标题
                    //     subtext: 'Default layout',//副标题
                    //     top: 'bottom',//上下位置
                    //     left: 'right'//左右位置
                    // },
                    tooltip: {
                        //      formatter: function (params, ticket, callback) {//回调函数
                        //           var str = appendPath(graph.nodes[params.dataIndex].id);
                        //           document.getElementById("div1").innerHTML = str;
                        //           return str;//
                        //       }
                    },
                    legend: [{
                        // selectedMode: 'single',
                        // x:'center',
                        // y:'center',
                        data: categories.map(function (a) {//显示策略
                            return a.name;
                        })
                    }],
                    animation: false,//是否开启动画
                    series: [
                        {
                            name: '',
                            type: 'graph',
                            layout: 'force',
                            // center: [50,50],
                            data: graph.nodes,//节点数据
                            links: graph.links,//节点边数据
                            categories: categories,//策略
                            roam: true,//是否开启滚轮缩放和拖拽漫游，默认为false（关闭），其他有效输入为true（开启），'scale'（仅开启滚轮缩放），'move'（仅开启拖拽漫游）
                            label: {
                                normal: {
                                    show: 'false',
                                    position: 'right'
                                }
                            },
                            slient: false,//是否响应点击事件，为false的时候就是响应
                            force: {
                                repulsion: 100
                            }
                        }
                    ]
                };
                myChart.setOption(option);
                function appendPath(id) {
                    var str = id;
                    var links = graph.links;
                    var i = 0;
                    var map = {};
                    for (i = 0; i < links.length; i++) {
                        map[links[i].target] = links[i].source;
                    }
                    while (true) {
                        if (map[id] == undefined) {
                            break;
                        }
                        str = map[id] + "->" + str;
                        id = map[id];
                    }
                    return str;
                }
            },
            //清空新建编辑数据
            clearData() {
                active.resourceGroupTable();
                active.classTable();
                active.overEquipment();
                //已选用户或部门表格
                active.overUserDepartment();
                //已选设备
                active.overEquipment();
                userChecked = "";
                //用户所在科室
                department = "";
                //已选用户或单位展示数据
                overUserDepartment = [];
                //判断是部门还是用户
                authType = "";
                //判断是组还是具体设备
                hardwareType = "";
                //判断资源组有没有设备
                groupEquipmentFlag = "";
                //添加的软件
                addSoftware = "";
                //新建按资源组分类设备
                groupEquipment = [];
                //新建按类型分类设备
                classEquipment = [];
                //已选设备
                overEquipment = [];
                //新建资源组ID
                groupEquipmentID = "";
                //新建编辑运行系统
                osType = "";
                $('.layui-software').hide();
                $('.layui-equipment').hide();
                $('.layui-user').show();
                active.tab1Date();
                // $('#test2')
                //日期范围
                laydate.render({
                    elem: '#test2'
                    , value: '2099-01-01'
                    , isInitValue: true //是否允许填充初始值，默认为 true
                    , min: 'date'
                    // ,value: ""
                    // ,range: true
                });
                laydate.render({
                    elem: '#test3'
                    , value: '2099-01-01'
                    , isInitValue: true //是否允许填充初始值，默认为 true
                    , min: 'date'
                    // ,value: ""
                    // ,range: true
                });
                // $("input[name=forever]").attr("checked",'false');//全选
                // $('input[name=forever]').checked(false);
                // form.render();

            },
            getTree: function (val) {
                $.ajax({
                    url: url + ':9000/organization/getOrgTreeAndIncludeUsers',
                    //   url: 'http://10.1.3.27:9000/organization/getOrgTreeAndIncludeUsers',
                    type: 'get',
                    success: function (data) {
                        console.log(data)
                        layui.tree({
                            elem: '#treeList', //指定元素
                            nodes: data.data,
                            spread: true,
                            click: function (node) {
                                console.log(node)
                                if (node.children) {
                                    param.userId = ''
                                    param.departmentId = node.id
                                    active.tableList1()
                                    active.getTableListPage1()
                                } else {
                                    param.userId = node.id
                                    param.departmentId = ''
                                    active.tableList1()
                                    active.getTableListPage1()
                                }
                            }
                        })
                    }
                })
                $('body').on('mousedown', '.layui-tree a', function () {
                    $('.layui-tree a').css('background', '')
                    $(this).css('background', 'rgb(18, 160, 255)')
                })
            }
        };
        active.init();
        active.getTree()
        window.reTable = active
        //排序
        $('#treeList').html('')
        table.on('sort(demo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            console.log(obj.field); //当前排序的字段名
            console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            console.log(this); //当前排序的 th 对象

            active.tableList1(0, searchName, obj.type, obj.field);
            active.getTableListPage1(searchName, obj.type, obj.field);
            //尽管我们的 table 自带排序功能，但并没有请求服务端。
            //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
            // table.reload('testTable', { //testTable是表格容器id
            //     initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
            //     ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
            //         field: obj.field //排序字段
            //         ,order: obj.type //排序方式
            //     }
            // });
        });
        //监听新建单选框
        table.on('tool(demo)', function (obj) {
            if (obj.event == 'radiodemo') {
                // addSoftware.push(obj.data);
                // console.log(obj.data);
                addSoftware = obj.data.id;
                osType = obj.data.systemName;
                linkType = obj.data.linkName;
                // console.log(JSON.stringify(addSoftware)); //选中行的相关数据
            }
            //监听编辑
            else if (obj.event == 'editUserOrEquipment') {
                // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                // console.log(JSON.stringify(obj.data.id));
                newOrEdit = 2;
                userOrEquipmentId = obj.data.id
                overUserDepartment = [];
                overUserDepartment.push(obj.data);
                //显示编辑的用户或部门
                active.showUserOrEquipment(overUserDepartment);
                //编辑弹框
                // active.editBuild();
            }

            if (obj.event == 'demo') {
                console.log(obj, 'obj')
                $.ajax({
                    url: url + ":9001/HardwareGroupResource/details",
                    type: "post",
                    async: true,
                    data: { id: obj.data.hardwareId },
                    success: function (data) {
                        console.log(data, "data")
                        $("#shebeidialog>p").html(obj.data.hardwareName + "资源组节点服务器信息")
                        console.log(data.data);
                          if (data.code == 200) {
                            let arr = data.data
                            let str1 = ""
                            for (let i = 0; i < arr.length; i++) {
                              str1 += `<tr class='xqtr'>
                                  <td>${arr[i].name}</td>
                                  <td>${arr[i].extranetip}</td>
                                  <td>${arr[i].extranetmacip}</td>
                                  <td>${arr[i].systemName}</td>
                                  <td>${arr[i].desktopType}</td>
                              </tr>`
                            }
                            // console.log(str1)
                            $(".shebeitable>tbody").html(str1)
                          
                          }
                    },
                    error: function(data) {
                      window.location.href = "../../error/error.html"
                      console.log(data)
                    }
                })
                if(obj.data.hardwareType=="single"){
                }else if(obj.data.hardwareType=="group"){
                layer.open({
                    type: 1,
                    title: "详情", //不显示标题栏
                    closeBtn: 1,
                    area: ["1065px", "500px"],
                    shade: 0.3,
                    id: "lunbo_layui", //设定一个id，防止重复弹出
                    //btn: ['发送'],
                    btnAlign: "c",
                    moveType: 1, //拖拽模式，0或者1
                    content: $("#shebeidialog")
                })
            }
            }
        });
        //监听多选框
        table.on('checkbox(tab1)', function (obj) {
            // console.log(obj)
            if (obj.type == "all") {
                if (obj.checked) {
                    authType = "user";
                    overUserDepartment = [];
                    for (let i = 0; i < userChecked.length; i++) {
                        var obj = userChecked[i];
                        overUserDepartment.push(obj)
                    }
                    // overUserDepartment = userChecked;
                    // console.log(userChecked);
                    active.overUserDepartment(overUserDepartment);
                }
                else {
                    overUserDepartment = [];
                    active.overUserDepartment(overUserDepartment);
                }
            }
            else {
                if (obj.checked) {
                    authType = "user";
                    var obj = obj.data;
                    overUserDepartment.push(obj);
                    active.overUserDepartment(overUserDepartment);
                }
                else {
                    for (let i = 0; i < overUserDepartment.length; i++) {
                        if (overUserDepartment[i].id == obj.data.id) {
                            // console.log(userChecked);
                            overUserDepartment.splice(i, 1);
                            // console.log(userChecked);
                            break;
                        }
                    }

                    active.overUserDepartment(overUserDepartment);
                }
                // console.log(userChecked);
            }
        });
        //监听用户部门已选
        table.on('checkbox(tab2)', function (obj) {
            if (obj.type == "all") {
                if (!obj.checked) {
                    overUserDepartment = [];
                    active.overUserDepartment(overUserDepartment);
                }
            }
            else {
                if (!obj.checked) {
                    for (let i = 0; i < overUserDepartment.length; i++) {
                        if (overUserDepartment[i].id == obj.data.id) {
                            overUserDepartment.splice(i, 1);
                            break;
                        }
                    }
                    active.overUserDepartment(overUserDepartment);
                }
            }
        });
        //监听资源组设备

        // 监听单选框
        form.on("radio(radiodemo1)", function (obj) {
            hardwareType = "single";
            overEquipment = [];
            var obj = {
                name: this.value,
                id: this.id
            }
            overEquipment.push(obj);
            // console.log(overEquipment)
            active.overEquipment(overEquipment);
            // console.log(obj.value);
            // console.log(obj);
            // console.log(this.name);
            // layer.tips(this.value+" "+this.name+":"+obj.elem.checked,obj.othis);
        });
        form.on("radio(radiodemo2)", function (obj) {
            hardwareType = "single";
            overEquipment = [];
            var obj = {
                name: this.value,
                id: this.id
            }
            overEquipment.push(obj);
            console.log(overEquipment)
            active.overEquipment(overEquipment);
            // console.log(obj.value);
            // console.log(obj);
            // console.log(this.name);
            // layer.tips(this.value+" "+this.name+":"+obj.elem.checked,obj.othis);
        });

        //新建按钮
        $('.new-build').on('click', function () {
            console.log(vm)
            vm.newBuild = true
            vm.date = '2099-01-01'
            vm.getUserTree()
            vm.getApplicationList()
            // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            // newOrEdit = 1;
            // //获取机构列表
            // active.getOrgList();
            // active.newlyBuild()
        });
        //编辑按钮
        $('.edit-build').on('click', function () {

            var checkStatus = table.checkStatus('resourceList');
            if (checkStatus.data.length == 1) {
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                // console.log(checkStatus.data[0]);
                userOrEquipmentId = checkStatus.data[0].id
                newOrEdit = 2;
                //弹框显示
                overUserDepartment = [];
                overUserDepartment.push(checkStatus.data[0]);
                // console.log(JSON.stringify(overUserDepartment));
                //显示编辑的用户或部门
                active.showUserOrEquipment(overUserDepartment);
                // //获取软件列表
                // active.getApplication();
                // //获取软件列表分页
                active.getApplicationPage();
                // //获取资源组
                active.getHardwareGroupList();
                // //获取类型
                // active.getHardwareTree();
                //编辑弹框
                active.editBuild();
                // console.log(checkStatus.data[0].id)
                // active.updateDataShow(checkStatus.data[0].id)
            }
            else if (checkStatus.data.length > 1) {
                layer.msg('请选择一个用户', { icon: 2, closeBtn: 1 });
            }
            else {
                layer.msg('请选择用户', { icon: 2, closeBtn: 1 });
            }

        });


        //删除
        $(".del-resource").on("click", function () {
            var checkStatus = table.checkStatus('resourceList');
            let checkArray = [];
            for (let i = 0; i < checkStatus.data.length; i++) {
                checkArray.push(checkStatus.data[i].id);
            }
            console.log(checkArray)
            if (checkArray.length > 0) {
                layer.confirm('您确定要删除吗？', {
                    btn: ['确定', '取消'],//按钮
                    //
                }, function () {
                    layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                    active.delResource(checkArray.toString());
                }, function () {
                    layer.msg('您取消了删除', {
                        icon: 2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn: 1
                    });
                });
            }
            else {
                layer.msg('请选择用户', { icon: 2, closeBtn: 1 });
            }

            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         console.log(checkStatus.data[0].id)
            //         active.delResource(checkStatus.data[0].id);
            //
            //     }, function(){
            //         layer.msg('您取消了删除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            //     // console.log(checkStatus.data[0].id)
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个用户',{icon:2});
            // }
            // else{
            //     layer.msg('请选择用户',{icon:2});
            // }
        });

        //下载导入模板
        $(".downloadTemplate").on('click', function () {
            window.open(baseURL_SON + '/downloadTemplate');
        });
        //上传导入
        upload.render({
            elem: ".importAuthorization",
            url: baseURL_SON + '/importAuthorization',
            method: 'post',
            accept: 'file',
            exts: 'xls|xlsx',
            before: function (obj) { //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                console.log(JSON.stringify(obj))
                layer.load(0, { shade: [0.1] }); //上传loading
            },
            done: function (res) {
                console.log(res)
                if (res.code == 200) {
                    active.tableList1();
                    active.getTableListPage1();
                    layer.closeAll('loading');
                    layer.msg('上传成功', { icon: 1, closeBtn: 1 });

                }
                else {
                    layer.closeAll('loading');
                    layer.msg('上传失败', { icon: 2, closeBtn: 1 });
                }

                // alert("上传成功")
                //上传完毕回调
            },
            error: function (res) {
                console.log(res);
                layer.closeAll('loading');
                alert("上传失败")
                //请求异常回调
            }
        });
        //导出功能
        $(".exportAuthorization").on('click', function () {
            window.open(baseURL_SON + '/exportAuthorization');
        });

        //搜索1
        $("#btn-footer1").on("click", function () {
            // console.log(this);
            if (!$("#index-footer1").val()) {
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer1"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else {
                searchName = $("#index-footer1").val()
                active.tableList1(0, searchName);
                active.getTableListPage1(searchName);
                // active.tableList1(0,$("#index-footer1").val());
                // active.getTableListPage1($("#index-footer1").val());
            }

        });
        $("#index-footer1").on("keypress", function () {
            // console.log(this);
            if (event.keyCode == "13") {
                if (!$("#index-footer1").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer1"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    searchName = $("#index-footer1").val()
                    active.tableList1(0, searchName);
                    active.getTableListPage1(searchName);
                    // active.tableList1(0,$("#index-footer1").val());
                    // active.getTableListPage1($("#index-footer1").val());
                }
            }
        });
        //键盘抬起重新加载1
        $("#index-footer1").on("keyup", function () {
            if (!$("#index-footer1").val()) {
                searchName = "";
                active.tableList1();
                active.getTableListPage1();
            }
        });
        $("#icon-footer1").on("click", function () {
            $(this).prev().val('');
            searchName = "";
            active.tableList1();
            active.getTableListPage1();

        });
        //搜索2
        $("#btn-footer2").on("click", function () {
            // console.log(this);
            if (!$("#index-footer2").val()) {
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer2"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else {
                active.getApplication(0, $("#index-footer2").val());
                active.getApplicationPage($("#index-footer2").val());
            }

        });
        $("#index-footer2").on("keypress", function () {
            // console.log(this);
            if (event.keyCode == "13") {
                if (!$("#index-footer2").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer2"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    active.getApplication(0, $("#index-footer2").val());
                    active.getApplicationPage($("#index-footer2").val());
                }
            }
        });
        //键盘抬起重新加载2
        $("#index-footer2").on("keyup", function () {
            if (!$("#index-footer2").val()) {
                active.getApplication();
                active.getApplicationPage();
            }
        });
        $("#icon-footer2").on("click", function () {
            $(this).prev().val('');
            active.getApplication();
            active.getApplicationPage();

        });
        //搜索3
        $("#btn-footer3").on("click", function () {
            // console.log(this);
            if (!$("#index-footer3").val()) {
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer3"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else {
                active.getApplication(0, $("#index-footer3").val());
                active.getApplicationPage($("#index-footer3").val());
            }

        });
        $("#index-footer3").on("keypress", function () {
            // console.log(this);
            if (event.keyCode == "13") {
                if (!$("#index-footer3").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer3"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
                }
                else {
                    active.getApplication(0, $("#index-footer3").val());
                    active.getApplicationPage($("#index-footer3").val());
                }
            }
        });
        //键盘抬起重新加载3
        $("#index-footer3").on("keyup", function () {
            if (!$("#index-footer3").val()) {
                active.getApplication();
                active.getApplicationPage();
            }
        });
        $("#icon-footer3").on("click", function () {
            $(this).prev().val('');
            active.getApplication();
            active.getApplicationPage();

        });
        //搜索4
        $("#btn-footer4").on("click", function () {
            // console.log(this);
            if (!$("#index-footer4").val()) {
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer3"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else {
                active.getAuthRelationList($("#index-footer4").val());
            }

        });
        $("#index-footer4").on("keypress", function () {
            // console.log(this);
            if (event.keyCode == "13") {
                if (!$("#index-footer4").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer3"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    active.getAuthRelationList($("#index-footer4").val());
                }
            }
        });
        //键盘抬起重新加载4
        $("#index-footer4").on("keyup", function () {
            if (!$("#index-footer4").val()) {
                active.getAuthRelationList();
            }
        });
        $("#icon-footer4").on("click", function () {
            $(this).prev().val('');
            active.getAuthRelationList();
        });
        //日期范围
        laydate.render({
            elem: '#test2'
            , value: '2099-01-01'
            , isInitValue: true //是否允许填充初始值，默认为 true
            , min: 'date'
            // ,min: new Date()
            // ,range: true
        });
        laydate.render({
            elem: '#test3'
            , value: '2099-01-01'
            , isInitValue: true //是否允许填充初始值，默认为 true
            , min: 'date'
            // ,min: new Date()
            // ,range: true
        });




        //新建
        //选择用户和部门页面的下一页
        $(".layui-user-next").on('click', function () {
            if (overUserDepartment.length <= 0) {
                layer.msg('请选择用户或者部门', {
                    icon: 2,
                    closeBtn: 1
                })
            }
            else {
                //获取添加软件列表
                active.getApplication();
                //获取添加软件列表分页
                active.getApplicationPage();
                $(".layui-content-main .layui-user").hide();
                $(".layui-content-main .layui-software").show()
            }
            // $(".layui-user").css("display","none");
            // $(".layui-software").css("display","block")
        });
        //添加软件页面的下一页
        $(".layui-software-next").on('click', function () {
            // console.log(addSoftware);
            if (addSoftware != "" && $('#test2').val()) {
                //获取资源组
                active.getHardwareGroupTree(osType, linkType);
                //获取类型
                active.getHardwareTree(osType, linkType);
                $(".layui-content-main .layui-software").hide();
                $(".layui-content-main .layui-equipment").show();
            }
            else {
                layer.msg('请选择软件和时间', {
                    icon: 2,
                    closeBtn: 1
                })
            }


            // $(".layui-software").css("display","none");
            // $(".layui-equipment").css("display","block");
        });
        //新建授权提交
        $('.newCompleteOver').on('click', function () {
            if (overEquipment.length > 0) {
                var submitData = [];
                // var obj = {}
                if (authType == "user") {
                    for (let i = 0; i < overUserDepartment.length; i++) {
                        // console.log(overUserDepartment)
                        if (hardwareType == "single") {
                            // for(let v of overEquipment){
                            var obj = {};
                            obj.hardwareType = "single";
                            obj.authType = "user";
                            obj.userId = overUserDepartment[i].id;
                            obj.userName = overUserDepartment[i].userName;
                            obj.department = department;
                            obj.applicationId = addSoftware;
                            obj.authDate = $('#test2').val();
                            obj.groupId = groupEquipmentID;
                            obj.linkType = linkType;
                            // obj.hardwareId = v.id;
                            // obj.hardwareName = v.hardwareName;
                            obj.hardwareId = overEquipment[0].id;
                            obj.hardwareName = overEquipment[0].hardwareName;
                            submitData.push(obj);
                            // }
                        }
                        else {
                            var obj = {};
                            obj.authType = "user";
                            obj.userId = overUserDepartment[i].id;
                            obj.userName = overUserDepartment[i].userName;
                            obj.department = department;
                            obj.applicationId = addSoftware;
                            obj.authDate = $('#test2').val();
                            obj.groupId = groupEquipmentID;
                            obj.linkType = linkType;
                            obj.hardwareType = "group";
                            obj.hardwareId = overEquipment[0].id;
                            obj.hardwareName = overEquipment[0].hardwareName;
                            submitData.push(obj);
                        }
                    }
                }
                else {
                    if (hardwareType == "group") {
                        var obj = {};
                        obj.authType = "org";
                        obj.userId = overUserDepartment[0].id;
                        obj.userName = overUserDepartment[0].userName;
                        obj.department = department;
                        obj.applicationId = addSoftware;
                        obj.authDate = $('#test2').val();
                        obj.groupId = groupEquipmentID;
                        obj.linkType = linkType;
                        obj.hardwareId = overEquipment[0].id;
                        obj.hardwareName = overEquipment[0].hardwareName;
                        obj.hardwareType = "group";
                        submitData.push(obj);
                    }
                    else {
                        // for(let j of overEquipment){
                        var obj = {};
                        obj.hardwareType = "single";
                        obj.authType = "org";
                        obj.userId = overUserDepartment[0].id;
                        obj.userName = overUserDepartment[0].userName;
                        obj.department = department;
                        obj.applicationId = addSoftware;
                        obj.authDate = $('#test2').val();
                        obj.groupId = groupEquipmentID;
                        obj.linkType = linkType;
                        // obj.hardwareId = j.id;
                        // obj.hardwareName = j.hardwareName;
                        obj.hardwareId = overEquipment[0].id;
                        obj.hardwareName = overEquipment[0].hardwareName;
                        submitData.push(obj);
                        // }
                    }

                }
                //新建授权提交
                active.newlyBuildSubmission(submitData);
                // active.newlyBuildSubmission(obj);
            }
            else {
                layer.msg('请选择设备', {
                    icon: 2,
                    closeBtn: 1
                })
            }
        });

        $(".layui-software-previous").on('click', function () {
            $(".layui-content-main .layui-software").hide();
            $(".layui-content-main .layui-user").show();
        });

        $(".layui-equipment-previous").on('click', function () {
            $(".layui-content-main .layui-equipment").hide();
            $(".layui-content-main .layui-software").show();
        });

        //编辑
        //选择用户和部门页面的下一页
        $(".layui-user-next-edit").on('click', function () {
            //获取软件列表
            active.getApplication();
            //获取软件列表分页
            active.getApplicationPage();
            $(".layui-content-edit .layui-user").hide();
            $(".layui-content-edit .layui-software").show()
        });
        //添加软件页面的下一页
        $(".layui-software-next-edit").on('click', function () {

            if (addSoftware != "" && $('#test3').val()) {
                //获取资源组
                active.getHardwareGroupList(osType, linkType);
                //获取类型
                active.getHardwareTree(osType, linkType);
                $(".layui-content-edit .layui-software").hide();
                $(".layui-content-edit .layui-equipment").show();
            }
            else {
                layer.msg('请选择软件和时间', {
                    icon: 2,
                    closeBtn: 1
                })
            }

            // $(".layui-content-edit .layui-software").hide();
            // $(".layui-content-edit .layui-equipment").show();
        });
        //编辑授权提交
        $('.editCompleteOver').on('click', function () {
            // console.log(JSON.stringify(overUserDepartment))
            // overUserDepartment
            if (overEquipment.length > 0) {
                // var submitData = [];
                var obj = {};
                if (hardwareType == "group") {
                    // console.log(JSON.stringify(overEquipment))
                    obj.id = userOrEquipmentId;
                    obj.userId = overUserDepartment[0].userId;
                    obj.userName = overUserDepartment[0].userName;
                    obj.department = overUserDepartment[0].department;
                    obj.authType = overUserDepartment[0].authType;
                    obj.applicationId = addSoftware;
                    obj.authDate = $('#test3').val();
                    obj.groupId = groupEquipmentID;
                    obj.hardwareId = overEquipment[0].id;
                    obj.hardwareName = overEquipment[0].hardwareName;
                    obj.hardwareType = "group";
                    // submitData.push(obj);
                }
                else {
                    // console.log(111111)
                    // console.log(JSON.stringify(overUserDepartment))
                    // for(let j of overEquipment){
                    //     var obj = {};
                    obj.id = userOrEquipmentId;
                    obj.hardwareType = "single";
                    obj.userId = overUserDepartment[0].userId;
                    obj.userName = overUserDepartment[0].userName;
                    obj.department = overUserDepartment[0].department;
                    obj.authType = overUserDepartment[0].authType;
                    obj.applicationId = addSoftware;
                    obj.groupId = groupEquipmentID;
                    obj.authDate = $('#test3').val();
                    // obj.hardwareId = j.id;
                    // obj.hardwareName = j.hardwareName;
                    obj.hardwareId = overEquipment[0].id;
                    obj.hardwareName = overEquipment[0].hardwareName;
                    // submitData.push(obj);
                    // }
                }


                //新建授权提交
                active.newlyBuildSubmission(obj);
                // active.newlyBuildSubmission(submitData);
            }
            else {
                layer.msg('请选择设备', {
                    icon: 2,
                    closeBtn: 1
                })
            }
        });

        $(".layui-software-previous-edit").on('click', function () {
            $(".layui-content-edit .layui-software").hide();
            $(".layui-content-edit .layui-user").show();
        });

        $(".layui-equipment-previous-edit").on('click', function () {
            $(".layui-content-edit .layui-equipment").hide();
            $(".layui-content-edit .layui-software").show();
        });
        //清空类型
        $('body .ResourcesGroup').on('click', function () {
            active.classTable();
        });
        //清空资源组
        $('body .Class').on('click', function () {
            active.resourceGroupTable();
        });
        //选中下拉树颜色变化
        $("body .departmentColor").on("mousedown", ".layui-tree a", function () {
            // $(".layui-tree a").css('background','none');
            //             // $(this).css('background','red');
            $(".layui-tree a").css('background', 'none');
            $(this).css('background', '#1E9FFF');
        });
        $('.setSelected').on('click', function () {
            overEquipmentSon = 2;
        });

    });


})();
