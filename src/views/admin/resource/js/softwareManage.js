// 软件资源管理

(function () {
    const baseURL_SON1 = url + ":9001/application";
    const baseURL_SON2 = url + ":9001";

    //const baseURL_SON1 = "http://localhost:9002/application";
    //const baseURL_SON2 = "http://localhost:9002";

    var application = "";
    //搜索
    var searchName;
    //tip层变量
    var indexTip1;
    var indexTip2;

    // layui.config({
    //     base: './js/formSelects.js' //此处路径请自行处理, 可以使用绝对路径
    // }).extend({
    //     formSelects: 'formSelects'
    // });


    //JavaScript代码区域
    layui.use(['element', 'laydate', 'table', 'layer', 'form', 'laypage', 'upload', 'tree'], function () {
        var element = layui.element
            // ,layedit = layui.layedit
            ,
            laydate = layui.laydate,
            table = layui.table,
            layer = layui.layer,
            form = layui.form,
            upload = layui.upload,
            tree = layui.tree,
            laypage = layui.laypage;
            formSelects = layui.formSelects;
        form.render();

        // http://localhost:9001/
        var active = {
            //初始化
            init: function () {
                this.tableList();
                this.getTableListPage();
                this.getSysname()
                // this.getQueryOs();
                // this.getQueryAcf();
                // this.getIconList();
            },
            //表格1
            tableList: function (page, searchName, mode, sortString) {
                // console.log(page+"---"+searchName)
                $.ajax({
                    url: baseURL_SON1 + "/getApplicationsPage",
                    // url:"http://10.1.3.152:9001/application/getApplicationsPage",
                    type: "post",
                    async: true,
                    // data:{page:pageNum==undefined? 0:pageNum,size:10,name:where==undefined? '':where},
                    data: {
                        page: page == undefined ? 0 : page,
                        size: 10,
                        searchName: searchName == undefined ? '' : searchName,
                        mode: mode == undefined ? '' : mode,
                        sortString: sortString == undefined ? '' : sortString
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            table.render({
                                elem: '#softwareList',
                                cols: [
                                    [{
                                        type: 'checkbox'
                                    }, {
                                        field: 'softwareName',
                                        title: '名称',
                                        event: 'demo',
                                        style: 'cursor: pointer;',
                                        templet: '#mouseHov',
                                        sort: 'true'
                                    }, {
                                        field: 'versionName',
                                        title: '版本',
                                        sort: 'true'
                                    }, {
                                        field: 'applicationClassficationName',
                                        title: '分类'
                                    }, {
                                        field: 'systemName',
                                        title: '运行系统'
                                    }, {
                                        field: 'licenseType',
                                        title: '许可模式'
                                    }, {
                                        field: 'desktopType',
                                        title: '桌面类型'
                                    }, {
                                        field: 'loadStrategyType',
                                        title: '负载策略'
                                    }, {
                                        field: 'sysIds',
                                        title: '系统用户'
                                    }, {
                                        field: 'realName',
                                        title: '应用管理员'
                                    }, {
                                        field: 'orgName',
                                        title: '所属单位'
                                    }]
                                ],
                                data: data.data.content
                            });
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    error: function (data) {
                        console.log(data)
                    }
                });
            },

            // ----------------------------------
            getSysname: function (cb) {
                $.ajax({
                    url: url + ":9000/user/querySystemUserName",
                    type: "post",
                    async: true,
                    success: function (data) {
                        console.log(data, '系统用户');
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (cb) {
                            // console.log(1111111111111)
                            var optionString = "<option grade=\'请选择一级属性\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].id + "\"";
                                    optionString += ">" + data.data[i].systemUser + "</option>"; //动态添加数据
                                }
                                $('select[name=sysId]').empty(); //清空子节点
                                // $('select[name=editsysIds]').empty();//清空子节点
                                $("select[name=sysId]").append(optionString); // 添加子节点
                                console.log(optionString)
                                // $("select[name=editsysIds]").append(optionString);  // 添加子节点
                            }
                            form.render();
                        } else {
                            // var optionString = "<option grade=\'请选择一级属性\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            var optionString = "";
                            if (data.data) { //判断
                                // vm.$data.systemName = data.data
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].id + "\"";
                                    optionString += ">" + data.data[i].systemUser + "</option>"; //动态添加数据
                                }
                                $('select[name=sysIds]').empty(); //清空子节点
                                $('select[name=editsysIds]').empty(); //清空子节点
                                $("select[name=sysIds]").append(optionString); // 添加子节点
                                $("select[name=editsysIds]").append(optionString); // 添加子节点
                                console.log(optionString)
                            }
                            form.render();
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message);
                    // }
                });
            },


            //新增弹框
            newBuildSoftWare: function () {
                layer.closeAll('loading');
                formSelects.selects({
                    name: "select2",
                    el: "select[name=editsysIds]",
                    show: "#select-result2",
                    model: "select",
                    filter: "sysIds",
                    init: [],
                    reset: true
                  })
                var index = layer.open({
                    type: 1,
                    title: ["新增应用", 'font-size:20px;font-weight:900'] //不显示标题栏
                    ,
                    closeBtn: 1,
                    area: ['800px', '650px'],
                    shade: 0.8,
                    id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,
                    btnAlign: 'c',
                    moveType: 1 //拖拽模式，0或者1
                    ,
                    content: $('.new-application'),
                    success: function (layero) {
                        //取消按钮
                        $(".cancel").on("click", function () {
                            //关闭所有弹框
                            layer.close(index);
                        });

                    },
                    end: function (index, layero) {
                        active.clearData();
                        $(".new-application").css("display", "none");
                    }
                });
            },
            //新增提交
            newBuildSoftWareSub: function (obj) {
                $.ajax({
                    url: baseURL_SON1 + "/saveApplication",
                    type: 'post',
                    async: true,
                    data: obj.field,
                    success: function (data) {
                        layer.closeAll(); //关闭所有层
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            })
                            active.tableList();
                            active.getTableListPage()
                        }
                        // that.tableList1()
                        else {
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }
                        // console.log(data.message)
                    },
                    error: function (data) {

                        console.log(data)
                    }

                });
            },
            //编辑弹框
            editBuildSoftWare: function () {
                layer.closeAll('loading');
                var index = layer.open({
                    type: 1,
                    title: ["编辑应用", 'font-size:20px;font-weight:900'] //不显示标题栏
                    ,
                    closeBtn: 1,
                    area: ['800px', '650px'],
                    shade: 0.8,
                    id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,
                    btnAlign: 'c',
                    moveType: 1 //拖拽模式，0或者1
                    ,
                    content: $('.edit-application'),
                    success: function (layero) {
                        form.render();
                        $(".cancel").on("click", function () {
                            //关闭所有弹框
                            layer.close(index);
                        });
                        $('.edit-application input[name=directory]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false)
                        $('.edit-application input[name=startCommand]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false)
                        $('.edit-application .rm').css("display", "block");
                        // if($('.edit-application .lianjie .layui-this').html().toLowerCase()=="rdp"||$('.edit-application .lianjie .layui-this').html().toLowerCase()=="horizon"||$('.edit-application .lianjie .layui-this').html().toLowerCase()=="citrix"){
                        //     $('.edit-application input[name=directory]').removeAttr("lay-verify").attr("placeholder","非必填").addClass('dis').attr('disabled',true);
                        //     $('.edit-application input[name=startCommand]').removeAttr("lay-verify").attr("placeholder","非必填").addClass('dis').attr('disabled',true);
                        //     $('.edit-application .rm').css("display","none");
                        // }
                        // else{
                        //     $('.edit-application input[name=directory]').attr('lay-verify','required').attr("placeholder","必填").removeClass('dis').attr('disabled',false)
                        //     $('.edit-application input[name=startCommand]').attr('lay-verify','required').attr("placeholder","必填").removeClass('dis').attr('disabled',false)
                        //     $('.edit-application .rm').css("display","block");
                        // }
                    },
                    end: function (index, layero) {
                        // $('.edit-application input[name=startCommand]').attr('lay-verify','required').attr("placeholder","").val('');
                        // $('.edit-application input[name=directory]').attr('lay-verify','required').attr("placeholder","").val('');
                        // $('.edit-application .rm').css("display","block");
                        $('.edit-application input[name=directory]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
                        $('.edit-application input[name=startCommand]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
                        $('.edit-application .rm').css("display", "block")
                        $(".edit-application").hide()
                    }
                });
            },
            //编辑提交
            editBuildSoftWareSub: function (obj) {
                // console.log(JSON.stringify(obj.field));
                $.ajax({
                    url: baseURL_SON1 + "/updateApplication",
                    type: 'post',
                    async: true,
                    data: obj.field,
                    success: function (data) {
                        layer.closeAll(); //关闭所有层
                        // console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            })
                            active.tableList();
                            active.getTableListPage()
                        }
                        // that.tableList1()
                        else {
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }
                        // console.log(data.message)
                    },
                    error: function (data) {

                        console.log(data)
                    }

                });
            },
            //删除软件
            delSoftware: function (ids) {
                $.ajax({
                    url: baseURL_SON1 + "/deleteBatch",
                    type: "post",
                    async: true,
                    data: {
                        ids
                    },
                    success: function (data) {
                        // that.treeList(data.data);
                        // console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        if (data.code == 200) {
                            layer.msg('删除成功', {
                                icon: 1,
                                closeBtn: 1
                            });
                            active.tableList();
                            active.getTableListPage()
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    error: function (data) {

                        // console.log(data.message)
                    }
                });
            },
            //获取表格1的页码
            getTableListPage: function (searchName, mode, sortString) {
                $.ajax({
                    url: baseURL_SON1 + "/getApplicationsPage",
                    type: "post",
                    async: true,
                    data: {
                        page: 0,
                        size: 10,
                        searchName: searchName == undefined ? '' : searchName,
                        mode: mode == undefined ? '' : mode,
                        sortString: sortString == undefined ? '' : sortString
                    },
                    success: function (data) {
                        if (data.code == 200) {
                            laypage.render({
                                elem: "softwarePage",
                                count: data.data.pageCount,
                                layout: ['prev', 'page', 'next', 'skip'],
                                limit: 10,
                                jump: function (obj, first) {
                                    //obj包含了当前分页的所有参数，比如：
                                    // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                    // console.log(obj.limit); //得到每页显示的条数
                                    // console.log(first);
                                    //首次不执行
                                    if (!first) {
                                        active.tableList(obj.curr, searchName, mode, sortString);
                                        //do something
                                    }
                                }
                            })
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    error: function (data) {

                        console.log(data)
                    }
                });
            },
            //编辑数据回显
            updataRetrieval: function (applicationId) {
                // console.log(id)
                $.ajax({
                    url: baseURL_SON1 + "/queryApplication",
                    type: "post",
                    async: true,
                    data: {
                        applicationId
                    },
                    success: function (data) {
                        if (data.code == 200) {
                            application = "";
                            console.log(JSON.stringify(data));
                            $('.edit-application input[name=id]').val(data.data.id);
                            $('.edit-application input[name=softwareName]').val(data.data.softwareName);
                            $('.edit-application input[name=softwareVersion]').val(data.data.softwareVersion);
                            $('.edit-application select[name=applicationClassficationId]').val(data.data.applicationClassficationId);
                            $('.edit-application select[name=systemId]').val(data.data.systemId);
                            $('.edit-application select[name=licenseId]').val(data.data.licenseId);
                            $('.edit-application select[name=loadStrategyId]').val(data.data.loadStrategyId);
                            $('.edit-application select[name=linkId]').val(data.data.linkId);
                            $('.edit-application select[name=iconId]').val(data.data.iconId);
                            $('.edit-application input[name=directory]').val(data.data.directory);
                            $('.edit-application input[name=startCommand]').val(data.data.startCommand);
                            $('.edit-application input[name=vendor]').val(data.data.vendor);
                            $('.edit-application input[name=buyDate]').val(data.data.buyDate);
                            $('.edit-application input[name=assetNumber]').val(data.data.assetNumber);
                            $('.edit-application select[name=administrator]').val(data.data.administrator);
                            $('.edit-application select[name=organizationId]').val(data.data.organizationId);
                            $('#classtree1').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html(data.data.orgName).end().find("input:hidden[name='organizationId']").val(data.data.organizationId);
                            active.editBuildSoftWare()
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    error: function (data) {
                        application = "";

                        console.log(data);
                    }
                })

            },
            //获取软件分类
            getQueryAcf: function (nam, cb) {
                $.ajax({
                    // url:baseURL_SON2 + "/acf/queryAcf",
                    url: baseURL_SON1 + "/getDictionaryList",
                    type: "post",
                    async: true,
                    data: {
                        typeKey: 'app_type'
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data.data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].typeKey + "\" state=\"" + data.data[i].state + "\"";
                                    optionString += ">" + data.data[i].value + "</option>"; //动态添加数据
                                }
                                if (nam) {
                                    $('select[name=applicationClassficationId]').empty(); //清空子节点
                                    $("select[name=applicationClassficationId]").append(optionString); // 添加子节点
                                } else {
                                    $('select[name=queryAcf]').empty(); //清空子节点
                                    $("select[name=queryAcf]").append(optionString); // 添加子节点
                                }

                            }
                            form.render();
                            if (cb) {
                                // console.log(cb);
                                cb('systemId', active.getlicensePattern)
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    error: function (data) {
                        layer.closeAll('loading');

                        console.log(data.message)
                    }
                });
            },
            //获取操作系统
            getQueryOs: function (nam, cb) {
                $.ajax({
                    // url:baseURL_SON2 + "/os/queryOs",
                    url: baseURL_SON1 + "/getDictionaryList",
                    type: "post",
                    async: true,
                    data: {
                        typeKey: 'os_type'
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data.data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择操作系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].typeKey + "\" deleteFlag=\"" + data.data[i].deleteFlag + "\"";
                                    optionString += ">" + data.data[i].value + "</option>"; //动态添加数据
                                }
                                if (nam) {
                                    $('select[name=systemId]').empty(); //清空子节点
                                    $("select[name=systemId]").append(optionString); // 添加子节点
                                } else {
                                    $('select[name=queryOs]').empty(); //清空子节点
                                    $("select[name=queryOs]").append(optionString); // 添加子节点
                                }

                            }
                            form.render();
                            if (cb) {
                                // console.log(cb);
                                cb(active.getloadLeveling)
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // if(nam){
                        //     form.render("select",'newSoftWareForm');
                        // }else{
                        //     form.render("select",'softWareForm');
                        // }
                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            //获取许可类型
            getlicensePattern: function (cb) {
                $.ajax({
                    url: baseURL_SON1 + "/getDictionaryList",
                    type: "post",
                    async: true,
                    data: {
                        typeKey: 'license_pattern'
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].typeKey + "\" id=\"" + data.data[i].id + "\" parentId=\"" + data.data[i].parentId + "\"";
                                    optionString += ">" + data.data[i].value + "</option>"; //动态添加数据
                                }

                                $('select[name=licenseId]').empty(); //清空子节点
                                $("select[name=licenseId]").append(optionString); // 添加子节点

                            }
                            form.render();
                            if (cb) {
                                // console.log(cb);
                                cb(active.getAppConnectionType)
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // form.render("select",'newSoftWareForm');
                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            //获取负载类型
            getloadLeveling: function (cb) {
                $.ajax({
                    url: baseURL_SON1 + "/getDictionaryList",
                    type: "post",
                    async: true,
                    data: {
                        typeKey: 'load_leveling'
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].typeKey + "\" typeKey=\"" + data.data[i].typeKey + "\" parentId=\"" + data.data[i].parentId + "\"";
                                    optionString += ">" + data.data[i].value + "</option>"; //动态添加数据
                                }

                                $('select[name=loadStrategyId]').empty(); //清空子节点
                                $("select[name=loadStrategyId]").append(optionString); // 添加子节点

                            }
                            form.render();
                            if (cb) {
                                // console.log(cb);
                                cb(active.getIconList)
                                cb(active.getOwnManger)
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // form.render("select",'newSoftWareForm');
                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            //获取软件管理员
            getOwnManger: function (cb) {
                $.ajax({
                    url: baseURL_SON1 + "/findAdministratorList",
                    type: "post",
                    async: true,
                    data: {
                        applicationId: ''
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.administratorList.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data.administratorList[i].id + "\" value=\"" + data.data.administratorList[i].id + "\" typeKey=\"" + data.data.administratorList[i].id + "\" parentId=\"" + data.data.administratorList[i].id + "\"";
                                    optionString += ">" + data.data.administratorList[i].realName + "</option>"; //动态添加数据
                                }

                                $('select[name=administrator]').empty(); //清空子节点
                                $("select[name=administrator]").append(optionString); // 添加子节点

                            }
                            form.render();
                            if (cb) {
                                // console.log(cb);
                                // cb(active.getIconList)
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // form.render("select",'newSoftWareForm');
                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            //获取连接方式
            getAppConnectionType: function (cb) {
                $.ajax({
                    url: baseURL_SON1 + "/getDictionaryList",
                    type: "post",
                    async: true,
                    data: {
                        typeKey: 'app_connection_type'
                    },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].typeKey + "\" typeKey=\"" + data.data[i].typeKey + "\" parentId=\"" + data.data[i].parentId + "\"";
                                    optionString += ">" + data.data[i].value + "</option>"; //动态添加数据
                                }

                                $('select[name=linkId]').empty(); //清空子节点
                                $("select[name=linkId]").append(optionString); // 添加子节点

                            }
                            form.render();
                            // if(cb && application){
                            //     cb('iconId',active.updataRetrieval)
                            // }else if(cb){
                            //     // cb('iconId',active.newBuildSoftWare)
                            //     cb('iconId',active.getOrgList)
                            // }
                            cb('iconId', active.getOrgList)
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // form.render("select",'newSoftWareForm');
                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            //获取软件图标列表
            getIconList: function (nam, cb) {
                $.ajax({
                    url: baseURL_SON2 + "/icon/getIconList",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (data.code == 200) {
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) { //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].id + "\"";
                                    optionString += ">" + data.data[i].name + "</option>"; //动态添加数据
                                }
                                if (nam) {
                                    $('select[name=iconId]').empty(); //清空子节点
                                    $("select[name=iconId]").append(optionString); // 添加子节点
                                } else {
                                    $('select[name=IconList]').empty(); //清空子节点
                                    $("select[name=IconList]").append(optionString); // 添加子节点
                                }
                            }
                            form.render();

                            if (cb && application) {
                                cb(active.updataRetrieval)
                            } else if (cb) {
                                cb(active.newBuildSoftWare)
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                        // if(nam){
                        //     form.render("select",'newSoftWareForm');
                        // }else{
                        //     form.render("select",'softWareForm');
                        // }

                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            //获取产权单位
            getOrgList: function (cb) {
                $.ajax({
                    url: baseURL_SON2 + "/licenseService/getOrgList",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";

                        if (data.code == 200) {
                            if (application) {
                                if (data.data.length > 0) {
                                    $('#classtree1').empty();
                                    tree({
                                        elem: "#classtree1",
                                        nodes: data.data,
                                        click: function (node) {
                                            // console.log(node);
                                            var $select = $($(this)[0].elem).parents(".layui-form-select");
                                            $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='organizationId']").val(node.id);
                                        }
                                    });
                                }
                                cb(application)
                            } else {
                                if (data.data.length > 0) {
                                    $('#classtree').empty();
                                    tree({
                                        elem: "#classtree",
                                        nodes: data.data,
                                        click: function (node) {
                                            console.log(node);
                                            var $select = $($(this)[0].elem).parents(".layui-form-select");
                                            $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='organizationId']").val(node.id);
                                        }
                                    });
                                }
                                cb()
                            }
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }


                        // var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                        // if(data.data){                   //判断
                        //     for(var i=0; i<data.data.length; i++){ //遍历，动态赋值
                        //         optionString +="<option grade=\""+data.data[i].id+"\" value=\""+data.data[i].id+"\"";
                        //         optionString += ">"+data.data[i].name+"</option>";  //动态添加数据
                        //     }
                        //
                        //     $('select[name=organizationId]').empty();//清空子节点
                        //     $("select[name=organizationId]").append(optionString);  // 添加子节点
                        // }
                        form.render();
                        // if(cb){
                        //     // console.log(cb);
                        //     cb(application)
                        // }


                    },
                    error: function (data) {

                        console.log(data.message)
                    }
                });
            },
            // //增加操作系统
            // addQueryOs:function (name) {
            //     $.ajax({
            //         url:baseURL_SON2 + "/os/addOs",
            //         type:"post",
            //         async:true,
            //         data:{name},
            //         success:function(data){
            //             console.log(data);
            //             layer.closeAll('loading');
            //             if(data.code==200){
            //                 layer.msg('添加成功', {
            //                     icon: 1,
            //                     closeBtn:1
            //                 });
            //                 active.getQueryOs()
            //                 $('input[name=operatingSystem]').val('');
            //             }
            //             else {
            //                 layer.msg(data.message,{
            //                     icon:2,
            //                     closeBtn:1
            //                 })
            //                 $('input[name=operatingSystem]').val('');
            //             }
            //         },
            //         error:function(data){
            //             
            //             console.log(data.message)
            //         }
            //     });
            // },
            // //增加软件分类
            // addQueryAcf:function (name) {
            //     $.ajax({
            //         url:baseURL_SON2 + "/acf/addAcf",
            //         type:"post",
            //         async:true,
            //         data:{name},
            //         success:function(data){
            //             // console.log(data);
            //             layer.closeAll('loading');
            //             if(data.code==200){
            //                 layer.msg('添加成功', {
            //                     icon: 1,
            //                     closeBtn:1
            //                 });
            //                 active.getQueryAcf();
            //                 $('input[name=softwareClass]').val('');
            //             }
            //
            //             else {
            //                 layer.msg(data.message, {
            //                     icon: 2,
            //                     closeBtn:1
            //                 });
            //                 $('input[name=softwareClass]').val('');
            //             }
            //         },
            //         error:function(data){
            //             
            //             console.log(data.message)
            //         }
            //     });
            // },
            // //删除操作系统
            // delQueryOs:function (ids) {
            //     $.ajax({
            //         url:baseURL_SON2 + "/os/deleteOs",
            //         type:"post",
            //         async:true,
            //         data:{ids},
            //         success:function(data){
            //             console.log(data);
            //             layer.closeAll('loading');
            //             if(data.code==200){
            //                 layer.msg('删除成功', {
            //                     icon: 1,
            //                     closeBtn:1
            //                 });
            //                 active.getQueryOs();
            //             }
            //             else {
            //                 layer.msg(data.message, {
            //                     icon: 2,
            //                     closeBtn:1
            //                 });
            //                 active.getQueryOs();
            //             }
            //         },
            //         error:function(data){
            //             
            //             console.log(data.message)
            //         }
            //     });
            // },
            // //删除软件分类
            // delQueryAcf:function (ids) {
            //     $.ajax({
            //         url:baseURL_SON2 + "/acf/deleteAcf",
            //         type:"post",
            //         async:true,
            //         data:{ids},
            //         success:function(data){
            //             console.log(data);
            //             layer.closeAll('loading');
            //             if(data.code==200){
            //                 layer.msg('删除成功', {
            //                     icon: 1,
            //                     closeBtn:1
            //                 });
            //                 active.getQueryAcf();
            //             }
            //             else {
            //                 layer.msg(data.message, {
            //                     icon: 2,
            //                     closeBtn:1
            //                 });
            //                 active.getQueryAcf();
            //             }
            //         },
            //         error:function(data){
            //             
            //             console.log(data.message)
            //         }
            //     });
            // },
            // //删除软件图标
            // deleteImg:function (id) {
            //     $.ajax({
            //         url:baseURL_SON2 + "/icon/deleteImg",
            //         type:"post",
            //         async:true,
            //         data:{id},
            //         success:function(data){
            //             // console.log(data);
            //             layer.closeAll('loading');
            //             if(data.code==200){
            //                 layer.msg('删除成功', {
            //                     icon: 1,
            //                     closeBtn:1
            //                 });
            //                 active.getIconList();
            //             }
            //             else {
            //                 layer.msg(data.message, {
            //                     icon: 2,
            //                     closeBtn:1
            //                 });
            //                 active.getIconList();
            //             }
            //         },
            //         error:function(data){
            //             
            //             console.log(data.message)
            //         }
            //     });
            // },
            //清空新建框数据
            clearData: function () {
                $('.new-application input[name=softwareName]').val('');
                $('.new-application input[name=softwareVersion]').val('');
                // $('.new-application input[name=directory]').val('');
                // $('.new-application input[name=startCommand]').val('');
                $('.new-application input[name=vendor]').val('');
                $('.new-application input[name=buyDate]').val('');
                $('.new-application input[name=assetNumber]').val('');
                $('#classtree').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html('选择单位').end().find("input:hidden[name='organizationId']").val('');
                // $('.new-application input[name=startCommand]').attr('lay-verify','required').attr("placeholder","").val('');
                // $('.new-application input[name=directory]').attr('lay-verify','required').attr("placeholder","").val('');
                $('.new-application input[name=directory]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
                $('.new-application input[name=startCommand]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
                $('.new-application .rm').css("display", "block")

            },
            //时间前面加0
            add0: function (m) {
                return m < 10 ? '0' + m : m
            },
            //时间戳转换
            timeFormat: function (time) {
                var time = new Date(time);
                var year = time.getFullYear();
                var month = time.getMonth() + 1;
                var date = time.getDate();
                var hours = time.getHours();
                var minutes = time.getMinutes();
                var seconds = time.getSeconds();
                return year + '-' + active.add0(month) + '-' + active.add0(date) + ' ' + active.add0(hours) + ':' + active.add0(minutes) + ':' + active.add0(seconds);
            },
        };
        active.init();
        //排序
        table.on('sort(demo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            console.log(obj.field); //当前排序的字段名
            console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            console.log(this); //当前排序的 th 对象

            active.tableList(0, searchName, obj.type, obj.field);
            active.getTableListPage(searchName, obj.type, obj.field);
            //尽管我们的 table 自带排序功能，但并没有请求服务端。
            //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
            // table.reload('testTable', { //testTable是表格容器id
            //     initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
            //     ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
            //         field: obj.field //排序字段
            //         ,order: obj.type //排序方式
            //     }
            // });
        });
        form.on('select(connectionMode)', function (data) {
            $('.new-application input[name=directory]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
            $('.new-application input[name=startCommand]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
            $('.new-application .rm').css("display", "block");
            // if($('.new-application .lianjie .layui-this').html().toLowerCase()=="rdp"||$('.new-application .lianjie .layui-this').html().toLowerCase()=="horizon"||$('.new-application .lianjie .layui-this').html().toLowerCase()=="citrix"){
            //     $('.new-application input[name=directory]').removeAttr("lay-verify").attr("placeholder","非必填").addClass('dis').attr('disabled',true).val('');
            //     $('.new-application input[name=startCommand]').removeAttr("lay-verify").attr("placeholder","非必填").addClass('dis').attr('disabled',true).val('');
            //     $('.new-application .rm').css("display","none");
            // }
            // else{
            //     $('.new-application input[name=directory]').attr('lay-verify','required').attr("placeholder","必填").removeClass('dis').attr('disabled',false).val('');
            //     $('.new-application input[name=startCommand]').attr('lay-verify','required').attr("placeholder","必填").removeClass('dis').attr('disabled',false).val('');
            //     $('.new-application .rm').css("display","block");
            // }

        });
        form.on('select(editconnectionMode)', function (data) {
            $('.edit-application input[name=directory]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
            $('.edit-application input[name=startCommand]').attr('lay-verify', 'required').attr("placeholder", "必填").removeClass('dis').attr('disabled', false).val('');
            $('.edit-application .rm').css("display", "block");
            // if($('.edit-application .lianjie .layui-this').html().toLowerCase()=="rdp"||$('.edit-application .lianjie .layui-this').html().toLowerCase()=="horizon"||$('.edit-application .lianjie .layui-this').html().toLowerCase()=="citrix"){
            //     $('.edit-application input[name=directory]').removeAttr("lay-verify").attr("placeholder","非必填").addClass('dis').attr('disabled',true).val('');
            //     $('.edit-application input[name=startCommand]').removeAttr("lay-verify").attr("placeholder","非必填").addClass('dis').attr('disabled',true).val('');
            //     $('.edit-application .rm').css("display","none");
            // }
            // else{
            //     $('.edit-application input[name=directory]').attr('lay-verify','required').attr("placeholder","必填").removeClass('dis').attr('disabled',false).val('');
            //     $('.edit-application input[name=startCommand]').attr('lay-verify','required').attr("placeholder","必填").removeClass('dis').attr('disabled',false).val('');
            //     $('.edit-application .rm').css("display","block");
            // }
        })
        //监听表格复选框选择
        table.on('checkbox(demo)', function (obj) {
            console.log(obj)
        });
        //监听工具条
        table.on('tool(demo)', function (obj) {
            // console.log(obj.data);
            // var data = obj.data;
            // if(obj.event === 'detail'){
            //     layer.msg('ID：'+ data.id + ' 的查看操作');
            // } else if(obj.event === 'del'){
            //     layer.confirm('真的删除行么', function(index){
            //         obj.del();
            //         layer.close(index);
            //     });
            // } else if(obj.event === 'edit'){
            //     layer.alert('编辑行：<br>'+ JSON.stringify(data))
            // };
            //
            if (obj.event === 'demo') {
                laydate.render({
                    elem: '#buyDate1',
                    type: 'datetime'
                });
                layer.load(0, {
                    shade: [0.1]
                }); //0代表加载的风格，支持0-2//loading层
                application = obj.data.applicationId;
                active.getQueryAcf('applicationClassficationId', active.getQueryOs);
                // active.editBuildSoftWare();
            }

        });
        //新增提交
        form.on('submit(newSoftware)', function (obj) {
            layer.load(0, {
                shade: [0.1]
            }); //0代表加载的风格，支持0-2//loading层
            console.log(JSON.stringify(obj.field));
            active.newBuildSoftWareSub(obj);
            // $.ajax({
            //     url:baseURL_SON1 + "/saveApplication",
            //     type:'post',
            //     async:true,
            //     data:obj.field,
            //     success:function(data){
            //         layer.closeAll(); //关闭所有层
            //         // console.log(JSON.stringify(data));
            //         if(data.code==200){
            //             layer.msg(data.message, {
            //                 icon:1,
            //                 // time: 2000, //20s后自动关闭
            //                 // btn: ['确定'],
            //                 closeBtn:1
            //             })
            //             active.tableList();
            //             active.getTableListPage()
            //         }
            //             // that.tableList1()
            //         else {
            //             layer.msg(data.message, {
            //                 icon:2,
            //                 // time: 2000, //20s后自动关闭
            //                 // btn: ['确定'],
            //                 closeBtn:1
            //             });
            //         }
            //             // console.log(data.message)
            //     },
            //     error:function(data){
            //         
            //         console.log(data)
            //     }
            //
            // });

            // layer.alert(JSON.stringify(obj.field), {
            //     title: '最终的提交信息'
            // })
            return false;
        });
        //编辑提交
        form.on('submit(editSoftware)', function (obj) {
            layer.load(0, {
                shade: [0.1]
            }); //0代表加载的风格，支持0-2//loading层
            // console.log(JSON.stringify(obj.field))
            active.editBuildSoftWareSub(obj)
            // $.ajax({
            //     url:baseURL_SON1 + "/updateApplication",
            //     type:'post',
            //     async:true,
            //     data:obj.field,
            //     success:function(data){
            //         layer.closeAll(); //关闭所有层
            //         // console.log(JSON.stringify(data));
            //         if(data.code==200){
            //             layer.msg(data.message, {
            //                 icon:1,
            //                 // time: 2000, //20s后自动关闭
            //                 // btn: ['确定'],
            //                 closeBtn:1
            //             })
            //             active.tableList();
            //             active.getTableListPage()
            //         }
            //         // that.tableList1()
            //         else {
            //             layer.msg(data.message, {
            //                 icon:2,
            //                 // time: 2000, //20s后自动关闭
            //                 // btn: ['确定'],
            //                 closeBtn:1
            //             });
            //         }
            //         // console.log(data.message)
            //     },
            //     error:function(data){
            //         
            //         console.log(data)
            //     }
            //
            // });

            // layer.alert(JSON.stringify(obj.field), {
            //     title: '最终的提交信息'
            // })
            return false;
        });

        //新增应用
        $(".new-build-application").click(function () {
            laydate.render({
                elem: '#buyDate',
                type: 'datetime'
            });
            layer.load(0, {
                shade: [0.1]
            }); //0代表加载的风格，支持0-2//loading层
            active.getQueryAcf('applicationClassficationId', active.getQueryOs);
            // active.getQueryAcf('applicationClassficationId');
            // active.getQueryOs('systemId');
            // active.getlicensePattern();
            // active.getloadLeveling();
            // active.getAppConnectionType();
            // active.getIconList('iconId');
            //示范一个公告层
            // active.newBuildSoftWare()
        });
        //编辑应用
        $(".edit-build-application").click(function () {
            var checkStatus = table.checkStatus('softwareList');
            if (checkStatus.data.length == 1) {
                laydate.render({
                    elem: '#buyDate1',
                    type: 'datetime'
                });
                layer.load(0, {
                    shade: [0.1]
                }); //0代表加载的风格，支持0-2//loading层
                application = checkStatus.data[0].applicationId;
                active.getQueryAcf('applicationClassficationId', active.getQueryOs);
                // active.getQueryOs('systemId');
                // active.getlicensePattern();
                // active.getloadLeveling();
                // active.getAppConnectionType();
                // active.getIconList('iconId');
                //示范一个公告层
                // active.updataRetrieval(checkStatus.data[0].applicationId)

            } else if (checkStatus.data.length > 1) {
                layer.msg('请选择一个软件', {
                    icon: 2
                });
            } else {
                layer.msg('请选择软件', {
                    icon: 2
                });
            }

        });
        //删除
        $(".del-software").on("click", function () {
            var checkStatus = table.checkStatus('softwareList');
            let checkArray = [];
            for (let i = 0; i < checkStatus.data.length; i++) {
                checkArray.push(checkStatus.data[i].applicationId);
            }
            // console.log(checkArray)
            if (checkArray.length > 0) {
                layer.confirm('您确定要删除吗？', {
                    btn: ['确定', '取消'], //按钮
                    //
                }, function () {
                    layer.load(0, {
                        shade: [0.1]
                    }); //0代表加载的风格，支持0-2//loading层
                    active.delSoftware(checkArray.toString());
                }, function () {
                    layer.msg('您取消了删除', {
                        icon: 2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn: 1
                    });
                });
            } else {
                layer.msg('请选择软件', {
                    icon: 2,
                    closeBtn: 1
                });
            }
            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         // console.log(checkStatus.data[0].applicationId);
            //         active.delSoftware(checkStatus.data[0].applicationId);
            //
            //     }, function(){
            //         layer.msg('您取消了删除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            //     // console.log(checkStatus.data[0].id)
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个用户',{icon:2});
            // }
            // else{
            //     layer.msg('请选择用户',{icon:2});
            // }
        });
        // //增加操作系统
        // $(".addQueryOs").on('click',function(){
        //     layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
        //     active.addQueryOs($.trim($('input[name=operatingSystem]').val()))
        // });
        // //增加软件分类
        // $(".addQueryAcf").on('click',function(){
        //     layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
        //     active.addQueryAcf($.trim($('input[name=softwareClass]').val()))
        // });
        // //删除操作系统
        // $(".delQueryOs").on('click',function(){
        //     console.log($('select[name=queryOs]').val())
        //     if($.trim($('select[name=queryOs]').val())==""){
        //         layer.msg('请选择操作系统', {
        //             icon: 0,
        //             closeBtn:1
        //         });
        //     }else{
        //         layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
        //         active.delQueryOs($('select[name=queryOs]').val())
        //     }
        //
        // });
        // //删除软件分类
        // $(".delQueryAcf").on('click',function(){
        //     console.log($('select[name=queryAcf]').val())
        //     if($.trim($('select[name=queryAcf]').val())==""){
        //         layer.msg('请选择软件分类', {
        //             icon: 0,
        //             closeBtn:1
        //         });
        //     }else{
        //         layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
        //         active.delQueryAcf($('select[name=queryAcf]').val())
        //     }
        // });
        // //删除软件图标
        // $(".deleteImg").on('click',function(){
        //     console.log($('select[name=IconList]').val())
        //     if($.trim($('select[name=IconList]').val())==""){
        //         layer.msg('请选择软件分类', {
        //             icon: 0,
        //             closeBtn:1
        //         });
        //     }else{
        //         layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
        //         active.deleteImg($('select[name=IconList]').val())
        //     }
        // });
        //下载导入模板
        $(".download-import-template").on('click', function () {
            window.open(baseURL_SON1 + '/downloadTemplate');
        });
        //上传导入
        upload.render({
            elem: ".importApplication",
            url: baseURL_SON1 + '/importApplication',
            method: 'post',
            accept: 'file',
            exts: 'xls|xlsx',
            before: function (obj) { //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                console.log(JSON.stringify(obj));
                layer.load(0, {
                    shade: [0.1]
                }); //上传loading
            },
            done: function (res) {
                console.log(res);
                if (res.code == 200) {
                    active.tableList();
                    active.getTableListPage();
                    layer.closeAll('loading');
                    layer.msg('上传成功', {
                        icon: 1,
                        closeBtn: 1
                    });
                } else if (res.code == 400) {
                    layer.closeAll('loading');
                    layer.msg(res.message, {
                        icon: 2,
                        closeBtn: 1
                    });
                }

                // alert("上传成功")
                //上传完毕回调
            },
            error: function (res) {
                console.log(res);
                layer.closeAll('loading');
                alert("上传失败")
                //请求异常回调
            }
        });
        // //上传图标
        // upload.render({
        //     elem:".uploadImg",
        //     url:baseURL_SON2 + '/icon/uploadImg',
        //     // url:'http://localhost:9001/icon/uploadImg',
        //     method:'post',
        //     accept:'images',
        //     // data:{
        //     //     name1:$('input[name=uploadImg]').val()
        //     // },
        //     // exts:'xls|xlsx',
        //     before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        //         this.data={name:$('input[name=uploadImg]').val()}
        //         layer.load(0, {shade: [0.1]}); //上传loading
        //
        //     },
        //     done: function(res){
        //         // console.log(3)
        //         // console.log(res)
        //         if(res.code==200){
        //             layer.closeAll('loading');
        //             layer.msg('上传成功',{icon:1,closeBtn:1});
        //             $('input[name=uploadImg]').val('')
        //             active.getIconList()
        //         }
        //         else if(res.code==400){
        //             layer.closeAll('loading');
        //             layer.msg(res.message,{icon:2,closeBtn:1});
        //             active.getIconList()
        //         }
        //
        //         // alert("上传成功")
        //         //上传完毕回调
        //     },
        //     error: function(res){
        //         console.log(res);
        //         layer.closeAll('loading');
        //         alert("上传失败")
        //         //请求异常回调
        //     }
        // });
        //导出功能
        $(".exportApplication").on('click', function () {
            window.open(baseURL_SON1 + '/exportApplication');
        });
        //搜索
        $("#btn-footer1").on("click", function () {
            // console.log(this);
            if (!$("#index-footer1").val()) {
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer1"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            } else {
                searchName = $("#index-footer1").val()
                active.tableList(0, searchName)
                active.getTableListPage(searchName)
                // active.tableList(0,$("#index-footer1").val())
                // active.getTableListPage($("#index-footer1").val())
            }

        });
        $("#index-footer1").on("keypress", function () {
            // console.log(this);
            if (event.keyCode == "13") {
                if (!$("#index-footer1").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer1"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    searchName = $("#index-footer1").val()
                    active.tableList(0, searchName)
                    active.getTableListPage(searchName)
                    // active.tableList(0,$("#index-footer1").val())
                    // active.getTableListPage($("#index-footer1").val())
                }
            }
        });
        //键盘抬起重新加载
        $("#index-footer1").on("keyup", function () {
            if (!$("#index-footer1").val()) {
                searchName = "";
                active.tableList();
                active.getTableListPage();
            }
        });
        $("#icon-footer1").on("click", function () {
            $(this).prev().val('');
            searchName = "";
            active.tableList();
            active.getTableListPage();

        });
        //tips层
        $('.edit-operation input[name=operatingSystem]').on('focus', function () {
            layer.tips('例如:windows </br>' +
                'linux', $('.edit-operation input[name=operatingSystem]'), {
                tips: [2, '#FF5722'],
                tipsMore: false,
                closeBtn: 1
            });
        });
        //软件目录提示
        $('.new-application input[name=directory]').on('mouseover', function () {
            indexTip1 = layer.tips('例如:/pb/openworks', $('.new-application input[name=directory]'), {
                tips: [2, '#FF5722'],
                tipsMore: false,
                time: 0
            });
        });
        $('.edit-application input[name=directory]').on('mouseover', function () {
            indexTip1 = layer.tips('例如:/pb/openworks', $('.edit-application input[name=directory]'), {
                tips: [2, '#FF5722'],
                tipsMore: false,
                time: 0
            });
        });
        //关闭tip层
        $('input[name=directory]').on('mouseout', function () {
            layer.close(indexTip1)
        });
        //启动命令提示
        $('.new-application input[name=startCommand]').on('mouseover', function () {
            indexTip2 = layer.tips('例如:jason ‘/apps/jason/Jason’\n' +
                '        owstart \'source /tmp/.lgclogin;startow\'', $('.new-application input[name=startCommand]'), {
                tips: [2, '#FF5722'],
                tipsMore: false,
                time: 0
            });
        });
        $('.edit-application input[name=startCommand]').on('mouseover', function () {
            indexTip2 = layer.tips('例如:jason ‘/apps/jason/Jason’\n' +
                '        owstart \'source /tmp/.lgclogin;startow\'', $('.edit-application input[name=startCommand]'), {
                tips: [2, '#FF5722'],
                tipsMore: false,
                time: 0
            });
        });
        //关闭tip层
        $('input[name=startCommand]').on('mouseout', function () {
            layer.close(indexTip2)
        });
        //下拉树操作
        $(".selecttree .downpanel").on("click", ".layui-select-title", function (e) {
            $(".selecttree .layui-form-select").not($(this).parents(".layui-form-select")).removeClass("layui-form-selected");
            $(this).parents(".downpanel").toggleClass("layui-form-selected");
            layui.stope(e);
        }).on("click", "dl i", function (e) {
            layui.stope(e);
        });
        $(document).on("click", function (e) {
            $(".selecttree .layui-form-select").removeClass("layui-form-selected");
        });

    });
})()