// 许可证资源管理
okotree={data:[]};


(function () {
    const baseURL_SON = url + ":9001/licenseService";
    const baseURL_SON1 = url + ":9001/application";
    const baseURL_SON2 = url + ":9001/module";
    const baseURL_SON3 = url + ":9001/moduleUsage";
    const baseURL_SON4 = url + ":9001/appConnection";
    const baseURL_SON5 = url + ":9006/AppConnection";


      //  const baseURL_SON = "http://localhost:9002/licenseService";
     //   const baseURL_SON1 = "http://localhost:9002/application";
     //  const baseURL_SON2 = "http://localhost:9002/module";
     //  const baseURL_SON3 = "http://localhost:9002/moduleUsage";
     //   const baseURL_SON4 = "http://localhost:9002/appConnection";

    //模块使用详情变量
    var applicationId = "";

    //打开桌面变量
    var userId="";
    var appId="";

    // 点击模块获取applicationId
    var moduleApplicationId = ""


    //表格3的传参
    var moduleName = "";
    var daemonName = "";
    //搜索
    var searchName;
    //状态指示
    var layerTipFail;
    var layerTipSuccess;


    layui.laytpl.toDateString = function(d, format){
        // var date = new Date(d || new Date())
        if(!d){
            return ""
        }
        // var date = new Date(d || new Date())
        var date = new Date(d)
            ,ymd = [
            this.digit(date.getFullYear(), 4)
            ,this.digit(date.getMonth() + 1)
            ,this.digit(date.getDate())
        ]
            ,hms = [
            this.digit(date.getHours())
            ,this.digit(date.getMinutes())
            ,this.digit(date.getSeconds())
        ];

        format = format || 'yyyy-MM-dd HH:mm:ss';

        return format.replace(/yyyy/g, ymd[0])
            .replace(/MM/g, ymd[1])
            .replace(/dd/g, ymd[2])
            .replace(/HH/g, hms[0])
            .replace(/mm/g, hms[1])
            .replace(/ss/g, hms[2]);
    };

//数字前置补零
    layui.laytpl.digit = function(num, length, end){
        var str = '';
        num = String(num);
        length = length || 2;
        for(var i = num.length; i < length; i++){
            str += '0';
        }
        return num < Math.pow(10, length) ? str + (num|0) : num;
    };



    //JavaScript代码区域
    layui.use(['element','table','laypage','layer','upload','form'], function() {
        var element = layui.element
            ,table = layui.table
            ,laypage = layui.laypage
            ,upload = layui.upload
            ,form = layui.form
            ,layer = layui.layer;
        
        var active = {
            //初始化
            init:function () {
                this.tableList1();
                // this.tableList2();
                // this.tableList3();
                this.getTableListPage1();
            },
            //表格1
            tableList1:function (page,searchName,mode,sortString) {
                $.ajax({
                    url:baseURL_SON + "/getLicServicePage",
                    type:"post",
                    async:true,
                    data:{page:page==undefined? 1:page,size:10,searchName:searchName==undefined? '':searchName,mode:mode==undefined? '':mode,sortString:sortString==undefined? '':sortString},
                    success:function(data){
                        // console.log(JSON.stringify(data));
                        if(data.code==200){
                            table.render({
                                elem: '#licenceList'
                                ,cols: [[
                                    {type:'checkbox'}
                                    ,{field:'status', title: '状态',toolbar:'#barDemo'}
                                    ,{field:'softwareName', title: '软件',event:'demo',style:'cursor:pointer',templet: '#mouseHov1',sort:'true'}
                                    ,{field:'versionName', title: '版本',sort:'true'}
                                    ,{field:'authNumber', title: '平台软件授权用户数',sort:'true'}
                                    ,{field:'hostName', title: 'Lic服务器',event:'demo1',style:'cursor:pointer',templet: '#mouseHov2',sort:'true'}
                                    ,{field:'moduleNumber', title: '模块数',sort:'true'}
                                    // ,{field:'authDate', title: '授权时间',sort:'true',templet: '<div>{{ layui.laytpl.toDateString(d.authDate) }}</div>'}
                                    ,{field:'authDate', title: '授权时间',sort:'true'}
                                    ,{field:'', title: '策略配置操作', align:'center', toolbar: '#tactics'}
                                ]]
                                ,data:data.data.content
                            });
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        window.location.href = "../../error/error.html";
                        console.log(data)
                    }
                });
            },
            //获取表格1分页
            getTableListPage1:function (searchName,mode,sortString) {
                $.ajax({
                    url:baseURL_SON + "/getLicServicePage",
                    type:"post",
                    async:true,
                    data:{page:1,size:10,searchName:searchName==undefined? '':searchName,mode:mode==undefined? '':mode,sortString:sortString==undefined? '':sortString},
                    success:function(data){
                        if(data.code==200){
                            laypage.render({
                                elem:"licencePage",
                                count:data.data.pageCount
                                ,layout: ['prev', 'page', 'next', 'skip']
                                ,limit:10
                                ,jump: function(obj, first){
                                    //obj包含了当前分页的所有参数，比如：
                                    // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                    // console.log(obj.limit); //得到每页显示的条数
                                    // console.log(first);
                                    //首次不执行
                                    if(!first){
                                        active.tableList1(obj.curr,searchName,mode,sortString);
                                        //do something
                                    }
                                }
                            })
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        window.location.href = "../../error/error.html";
                        console.log(data)
                    }
                });
            },
            //表格2
            tableList2:function (page,licServiceId,cb) {
                $.ajax({
                    url:baseURL_SON2 + "/getModulePage",
                    // url:"http://10.1.3.98:9001/module/getModulePage",
                    type:"post",
                    async:true,
                    data:{page:page==undefined? 0:page,size:10,licServiceId: licServiceId },
                    success:function (data) {
                        // console.log(JSON.stringify(data));
                        if(data.code==200){
                            table.render({
                                elem: '#test1'
                                // ,url:'/mock/table1'
                                // ,method:'post'
                                ,cols: [[
                                    {field:'name', title: '模块',width:'200',event:'demo2',style:'cursor:pointer'}
                                    ,{field:'daemonName', title: '守护程序', width:'90'}
                                    ,{field:'useAndTotal', title: '占用/总数', width:'100'}
                                    ,{field:'useRate', title: '使用率', width:'340',templet:'#barDemo2'}
                                ]],
                                data:data.data.content,
                            });
                            // if(cb)
                            //     cb(0,softwareId,versionId);
                                // active.getTableListPage2(0,softwareId,versionId);
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    }
                })
            },
            //获取表格2分页
            getTableListPage2:function (page,licServiceId) {
                $.ajax({
                    url:baseURL_SON2 + "/getModulePage",
                    // url:"http://10.1.3.98:9001/module/getModulePage",
                    type:"post",
                    async:true,
                    data:{page:page==undefined? 0:page,size:10,licServiceId: licServiceId},
                    success:function(data){
                        if(data.code==200){
                            laypage.render({
                                elem:"testPage1",
                                count:data.data.pageCount
                                ,layout: ['prev', 'page', 'next', 'skip']
                                ,limit:10
                                ,jump: function(obj, first){
                                    //obj包含了当前分页的所有参数，比如：
                                    // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                    // console.log(obj.limit); //得到每页显示的条数
                                    // console.log(first);
                                    //首次不执行
                                    if(!first){
                                        active.tableList2(obj.curr,licServiceId);
                                        //do something
                                    }
                                }
                            });
                            //打开软件详情弹框
                            active.softwareDetails();
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    }
                });
            },
            //表格3
            tableList3:function (page,applicationId,moduleName,daemonName,licServiceId) {
                // console.log(page);
                // console.log(applicationId);
                // console.log(moduleName);
                // console.log(daemonName);
                $.ajax({
                    url:baseURL_SON3 + "/getModuleUsagePage",
                    // url:"http://10.1.3.98:9001/moduleUsage/getModuleUsagePage",
                    type:"post",
                    async:true,
                    data:{page:page==undefined? 0:page,size:10,applicationId,moduleName,daemonName, licServiceId: licServiceId},
                    success:function (data) {
                        console.log(JSON.stringify(data));
                        if(data.code==200){
                            table.render({
                                elem: '#test2',
                                cols: [[
                                    {type:'checkbox', width:'40'}
                                    ,{field:'status', title: '状态', templet:'#barDemo1',width:'100'}
                                    ,{field:'realName', title: '用户', width:'100'}
                                    ,{field:'orgName', title: '科室', width:'100'}
                                    // ,{field:'openDate', title: '打开时间', width:'210',templet: '<div>{{ layui.laytpl.toDateString(d.openDate) }}</div>'}
                                    ,{field:'openDate', title: '打开时间', width:'210'}
                                    ,{field:'handle', title: 'Handel', width:'100'}
                                    ,{field:'deskTop', title: '桌面', width:'100'}
                                ]],
                                data:data.data.content,
                            });
                            $('.software-details-right .tabhid').show();
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function (data) {
                        window.location.href = "../../error/error.html";
                        console.log(data);
                    }
                })
            },
            //获取表格3分页
            getTableListPage3:function (applicationId,moduleName,daemonName, licServiceId) {
                $.ajax({
                    url:baseURL_SON3 + "/getModuleUsagePage",
                    // url:"http://10.1.3.98:9001/moduleUsage/getModuleUsagePage",
                    type:"post",
                    async:true,
                    data:{page:0,size:10,applicationId,moduleName,daemonName, licServiceId: licServiceId},
                    success:function(data){
                        // console.log(JSON.stringify(data))
                        if(data.code==200){
                            laypage.render({
                                elem:"testPage2",
                                count:data.data.pageCount
                                ,layout: ['prev', 'page', 'next', 'skip']
                                ,limit:10
                                ,jump: function(obj, first){
                                    //obj包含了当前分页的所有参数，比如：
                                    // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                    // console.log(obj.limit); //得到每页显示的条数
                                    // console.log(first);
                                    //首次不执行
                                    if(!first){
                                        active.tableList3(obj.curr,applicationId,moduleName,daemonName);
                                        //do something
                                    }
                                }
                            })
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        window.location.href = "../../error/error.html";
                        console.log(data)
                    }
                });
            },
            //新建许可弹框
            newBuildLicence:function () {
                layer.closeAll('loading');
                var index = layer.open({
                    type: 1
                    ,title: ["新增许可服务",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,area:['800px','380px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.new-service')
                    ,success: function(layero){

                        //取消按钮
                        $(".cancel").on("click",function () {
                            //关闭所有弹框
                            layer.close(index);
                        });
                    }
                    ,end: function(index, layero){
                        active.clearData();
                        $(".new-service").hide();
                    }
                });
            },
            //编辑许可弹框
            editBuildLicence:function (data) {
                layer.closeAll('loading');
                $('.edit-service input[name=id]').val(data.id);
                $('.edit-service input[name=softwareName]').val(data.softwareName);
                $('.edit-service input[name=softwareVersion]').val(data.softwareVersion);
                $('.edit-service input[name=hostName]').val(data.hostName);
                $('.edit-service input[name=ip]').val(data.ip);
                $('.edit-service input[name=mac]').val(data.mac);
                $('.edit-service input[name=encryption]').val(data.encryption);
                $('.edit-service input[name=port]').val(data.port);
                $('.edit-service input[name=programDirectory]').val(data.programDirectory);
                $('.edit-service input[name=permit]').val(data.permit);
                $('.edit-service input[name=startCommand]').val(data.startCommand);

                $('.edit-service .other').empty();//清空子节点
                for(let i = 0; i < data.daemonList.length;i++){
                    $('.edit-service .other').append('<div class="fle">\n' +
                        '                <div class="layui-form-item clearfix">\n' +
                        '                    <label class="layui-form-label">守护程序</label>\n' +
                        '                    <div class="layui-input-inline">\n' +
                        '                        <input name="daemonPath" value='+data.daemonList[i].daemonPath+' class="layui-input dis" type="text" placeholder="" autocomplete="off" lay-verify="required" disabled>\n' +
                        '                    </div>\n' +
                        '                    <b class="star fl">*</b>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="fle">\n' +
                        '                <div class="layui-form-item clearfix">\n' +
                        '                    <label class="layui-form-label">配置文件</label>\n' +
                        '                    <div class="layui-input-inline">\n' +
                        '                        <input name="configPath" value='+data.daemonList[i].configPath+' class="layui-input dis" type="text" placeholder="" autocomplete="off" lay-verify="required" disabled>\n' +
                        '                    </div>\n' +
                        '                    <b class="star fl">*</b>\n' +
                        '                </div>\n' +
                        '            </div>')
                    // $('.edit-service .other').append('<div class="layui-form-item clearfix">\n' +
                    //     '                <label class="layui-form-label">守护程序</label>\n' +
                    //     '                <div class="layui-input-inline">\n' +
                    //     '                    <input name="daemonPath" value='+data.daemonList[i].daemonPath+' class="layui-input dis" type="text" placeholder="" autocomplete="off" lay-verify="required" disabled>\n' +
                    //     '                </div>\n' +
                    //     '                <b class="star fl">*</b>\n' +
                    //     '            </div>\n' +
                    //     '            <div class="layui-form-item clearfix">\n' +
                    //     '                <label class="layui-form-label">配置文件</label>\n' +
                    //     '                <div class="layui-input-inline">\n' +
                    //     '                    <input name="configPath" value='+data.daemonList[i].configPath+' class="layui-input dis" type="text" placeholder="" autocomplete="off" lay-verify="required" disabled>\n' +
                    //     '                </div>\n' +
                    //     '                <b class="star fl">*</b>\n' +
                    //     '            </div>')
                }
                var index = layer.open({
                    type: 1
                    ,title: ["编辑许可服务",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,area:['800px','500px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.edit-service')
                    ,success: function(layero){
                        form.render()
                        //取消按钮
                        $(".cancel").on("click",function () {
                            //关闭所有弹框
                            layer.close(index);
                        })
                    }
                    ,end: function(index, layero){
                        $(".edit-service").hide()
                    }
                });
            },
            //删除许可
            delLicence:function (ids) {
                $.ajax({
                    url:baseURL_SON + "/deleteBatch",
                    type:"post",
                    async:true,
                    data:{ids},
                    success:function(data){
                        // console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        if(data.code==200){
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn:1
                            });
                            active.tableList1();
                            active.getTableListPage1();
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        layer.closeAll('loading');
                        window.location.href = "../../error/error.html";
                        // console.log(data.message)
                    }
                });
            },
            //获取软件列表
            querySoftwareAll:function (cb) {
                $.ajax({
                    url:baseURL_SON1 + "/querySoftwareAll",
                    type:"post",
                    async:true,
                    success:function(data){
                        // console.log(data);
                        layer.closeAll('loading');
                        if(data.code==200){
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if(data.data){                   //判断
                                for(var i=0; i<data.data.length; i++){ //遍历，动态赋值
                                    optionString +="<option grade=\""+data.data[i].softwareName+"\" value=\""+data.data[i].softwareName+"\"";
                                    optionString += ">"+data.data[i].softwareName+"</option>";  //动态添加数据
                                }
                                $('select[name=softwareName]').empty();//清空子节点
                                $("select[name=softwareName]").append(optionString);  // 添加子节点
                            }
                            form.render('select','newService');
                            if(cb){
                                cb()
                            }
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        window.location.href = "../../error/error.html";
                        console.log(data.message)
                    }
                });
            },
            //软件版本联动
            querySoftVersionList:function (softwareName) {
                $.ajax({
                    url:baseURL_SON1 + "/querySoftVersionList",
                    type:"post",
                    async:true,
                    data:{softwareName},
                    success:function(data){
                        // console.log(data);
                        if(data.code==200){
                            var optionString = "<option grade=\'请选择软件分类系统\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if(data.data){                   //判断
                                for(var i=0; i<data.data.length; i++){ //遍历，动态赋值
                                    optionString +="<option grade=\""+data.data[i].softwareVersion+"\" value=\""+data.data[i].softwareVersion+"\"";
                                    optionString += ">"+data.data[i].softwareVersion+"</option>";  //动态添加数据
                                }
                                $('select[name=softwareVersion]').empty();//清空子节点
                                $("select[name=softwareVersion]").append(optionString);  // 添加子节点
                            }
                            form.render('select','newService');
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        window.location.href = "../../error/error.html";
                        console.log(data.message)
                    }
                });
            },
            //编辑许可服务回显
            getLicenseServiceInfo:function (id,cb) {
                $.ajax({
                    url:baseURL_SON + "/getLicenseServiceInfo",
                    type:"post",
                    async:true,
                    data:{id},
                    success:function(data){
                      layer.closeAll('loading');
                        // console.log(JSON.stringify(data));
                        if(data.code==200){
                            appId=data.data.applicationId;
                            cb(data.data);
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                    },
                    error:function(data){
                        window.location.href = "../../error/error.html";
                        console.log(data.message);
                    }
                });
            },
            //软件详情弹框数据
            softwareDetailsData:function (data) {
                console.log(data);
                $('.software-details .softwareName').html(data.softwareName);
                $('.software-details .softwareVersion').html(data.softwareVersion);
                $('.software-details .authDate').html(data.authDate);

                var daemonListData = "";
                for(let i=0;i<data.daemonList.length;i++){
                    daemonListData+='<span style="padding: 0px 5px">'+data.daemonList[i].name+'</span>'
                }
                $('.software-details .daemonList').html(daemonListData);

                $('.software-details .hostName').html(data.hostName);
                $('.software-details .ip').html(data.ip);
                $('.software-details .mac').html(data.mac);
                $('.software-details .encryption').html(data.encryption);
                $('.software-details .port').html(data.port);
                $('.software-details .moduleNumber').html(data.moduleNumber);
                $('.software-details .authNumber').html(data.authNumber);

                applicationId = data.id

                moduleApplicationId = data.applicationId

                active.tableList2(0,applicationId);
                active.getTableListPage2(0,applicationId);


            },
            //软件详情弹框
            softwareDetails:function () {
                layer.closeAll('loading');
                layer.open({
                    type: 1
                    // ,title: false //不显示标题栏
                    // ,skin:'winning-class'//自定义样式winning-class
                    ,title: ["软件许可详情",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,shade: 0.8
                    // ,area:['1550px','650px']
                    ,area:['85%','650px']
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.software-details')
                    ,success: function(layero){

                    }
                    ,end: function(index, layero){
                        $(".software-details").hide();
                        $(".software-details-right .tabhid").hide();
                        $(".software-details-right .name").html('');
                        $(".software-details-right .daemonName").html('');
                        $(".software-details-right .moduleTotal").html('');
                        $(".software-details-right .useNumber").html('');

                    }
                });
            },
            //打开链接
            open:function (userId,appId,systemUserId) {
                $.ajax({
                    // url:baseURL_SON4 + "/open",
                    url:baseURL_SON5 + "/open",
                    type:"post",
                    async:true,
                    headers: {
                        "Content-Type": "application/json;charset=utf-8"
                    },
                    data:JSON.stringify({userId,appId,systemUserId}),
                    // data:JSON.stringify({userId:userId,appId:appId,systemUserId:systemUserId}),
                    success:function(data){
                        console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        // console.log()
                        console.log(data.data[1])
                        if(data.data[1]){
                            active.downLoadVnc(data.data[0],data.data[1]);
                        }
                        else{
                            layer.msg('网络错误',{
                                icon:2,
                                closeBtn:1
                            })
                        }

                    },
                    error:function(data){
                        // console.log(111111111111111)
                        window.location.href = "../../error/error.html";
                        console.log(data)
                    }
                });
            },
            //下载连接
            downLoadVnc:function (fileName,cont) {
                layer.closeAll('loading'); //关闭加载层

                var formSubmit = '<form action="'+baseURL_SON5 + "/downLoadVnc"+'" method="post" id="aa" hidden>\n' +
                    '        <input type="hidden" name="fileName" value="'+fileName+'">\n' +
                    '        <input type="hidden" name="cont" value="'+cont+'">\n' +
                    '    </form>';
                $('body').append(formSubmit);
                // console.log($('body').append(formSubmit));
                // formSubmit.appendTo($('body'))
                $('#aa').submit().remove();

            },
            //关闭连接
            close:function (userId,appId) {
                $.ajax({
                    url:baseURL_SON4 + "/close",
                    type:"post",
                    async:true,
                    data:{userId,appId},
                    success:function(data){
                        layer.closeAll('loading'); //关闭加载层
                        console.log(JSON.stringify(data))
                    },
                    error:function(data){
                        layer.closeAll('loading');
                        window.location.href = "../../error/error.html";
                        console.log(data)
                    }
                });
            },
            //释放许可
            closeLicenseProcess :function (id) {
                $.ajax({
                    url:baseURL_SON + "/closeLicenseProcess ",
                    type:"post",
                    async:true,
                    data:{id},
                    success:function(data){
                        layer.closeAll('loading'); //关闭加载层
                        if(data.code==200){
                            layer.msg(data.message,{
                                icon:1,
                                closeBtn:1
                            })
                            active.tableList3(0,applicationId,moduleName,daemonName);
                            active.getTableListPage3(applicationId,moduleName,daemonName);
                        }
                        else if(data.code==400){
                            layer.msg(data.message, {
                                icon:2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                        }
                        console.log(JSON.stringify(data))
                    },
                    error:function(data){
                        layer.closeAll('loading'); //关闭加载层
                        window.location.href = "../../error/error.html";
                        console.log(data)
                    }
                });
            },
            //清空新建许可弹框数据
            clearData:function () {
                $('.new-service select[name=softwareVersionId]').val('');
                $('.new-service input[name=ip]').val('');
                $('.new-service input[name=encryption]').val('');
                $('.new-service input[name=port]').val('');
                $('.new-service input[name=programDirectory]').val('');
                $('.new-service input[name=permit]').val('');
                $('.new-service input[name=startCommand]').val('');
            },
            //时间前面加0
            add0:function (m){return m<10?'0'+m:m },
            //时间戳转换
            timeFormat:function (time) {
                var time = new Date(time);
                var year = time.getFullYear();
                var month = time.getMonth()+1;
                var date = time.getDate();
                var hours = time.getHours();
                var minutes = time.getMinutes();
                var seconds = time.getSeconds();
                return year+'-'+active.add0(month)+'-'+active.add0(date)+' '+active.add0(hours)+':'+active.add0(minutes)+':'+active.add0(seconds);
            },
            // 获取许可详情
            getStrategyInfo: function (val) {
              $.ajax({
                // url: 'http://10.1.1.229:9001/licenseServiceDetails/getDefaultTemplateList/' + val,
                url: url +  ':9001/licenseServiceDetails/getDefaultTemplateList/' + val,
                type: 'post',
                async: true,
                success: function(data) {
                  if(data.code == 200) {
                    vm._data.showDetails.dataInfo = data.data
                    vm._data.showDetails.showInfo = true
                  }
                },
                error: function(err) {
                  console.log(err)
                }
              })
            }
        }

        active.init();

        //排序
        table.on('sort(demo)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            console.log(obj.field); //当前排序的字段名
            console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            console.log(this); //当前排序的 th 对象

            active.tableList1(0,searchName,obj.type,obj.field);
            active.getTableListPage1(searchName,obj.type,obj.field);
            //尽管我们的 table 自带排序功能，但并没有请求服务端。
            //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
            // table.reload('testTable', { //testTable是表格容器id
            //     initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
            //     ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
            //         field: obj.field //排序字段
            //         ,order: obj.type //排序方式
            //     }
            // });
        });
        //监听软件联动版本
        form.on('select(softwareName)', function (data) {
           // console.log(data.value);
            active.querySoftVersionList(data.value);
            // if(data.elem.name == 'areaId') {//选择一级
            //     loadBlock(data);//加载二级
            // }
        });
        //新建许可服务提交
        form.on('submit(newServiceSub)', function(obj){
            layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层

            $.ajax({
                url:baseURL_SON + "/saveLicenseService",
                type:'post',
                async:true,
                data:obj.field,
                success:function(data){
                    layer.closeAll('loading'); //关闭加载层
                    console.log(JSON.stringify(data));
                    if(data.code==200){
                        layer.closeAll(); //关闭所有层
                        layer.msg(data.message, {
                            icon:1,
                            // time: 2000, //20s后自动关闭
                            // btn: ['确定'],
                            closeBtn:1
                        })
                        active.tableList1();
                        active.getTableListPage1()
                    }
                    // that.tableList1()
                    else if(data.code==400){
                        layer.msg(data.message, {
                            icon:2,
                            // time: 2000, //20s后自动关闭
                            // btn: ['确定'],
                            closeBtn:1
                        });
                    }
                    // console.log(data.message)
                },
                error:function(data){
                    layer.closeAll('loading'); //关闭加载层
                    window.location.href = "../../error/error.html";
                    console.log(data)
                }

            });

            // layer.alert(JSON.stringify(obj.field), {
            //     title: '最终的提交信息'
            // })
            return false;
        });
        //编辑许可服务提交
        form.on('submit(editServiceSub)', function(obj){
            // layer.alert(JSON.stringify(obj.field), {
            //     title: '最终的提交信息'
            // })
            var data = {
                id:obj.field.id,
                softwareName:obj.field.softwareName,
                softwareVersion:obj.field.softwareVersion,
                ip:obj.field.ip,
                encryption:obj.field.encryption,
                port:obj.field.port,
                programDirectory:obj.field.programDirectory,
                permit:obj.field.permit,
                startCommand:obj.field.startCommand
            };
            // console.log(data)
            layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层

            $.ajax({
                url:baseURL_SON + "/updateLicenseService ",
                type:'post',
                async:true,
                data:data,
                success:function(data){
                    console.log(JSON.stringify(data));
                    layer.closeAll('loading'); //关闭加载层
                    if(data.code==200){
                        layer.closeAll(); //关闭所有层
                        layer.msg(data.message, {
                            icon:1,
                            // time: 2000, //20s后自动关闭
                            // btn: ['确定'],
                            closeBtn:1
                        })
                        active.tableList1();
                        active.getTableListPage1()
                    }
                    // that.tableList1()
                    else if(data.code==400){
                        layer.msg(data.message, {
                            icon:2,
                            // time: 2000, //20s后自动关闭
                            // btn: ['确定'],
                            closeBtn:1
                        });
                    }
                    // console.log(data.message)
                },
                error:function(data){
                    layer.closeAll('loading'); //关闭加载层
                    window.location.href = "../../error/error.html";
                    console.log(data)
                }

            });


            return false;
        });
        //监听表格复选框选择
        table.on('checkbox(demo)', function(obj){
            console.log(obj)
        });
        //监听工具条
        table.on('tool(demo)', function(obj){
          if(obj.event === 'infoBtn') {
            var idInfo = obj.data.id
            // active.getStrategyInfo(idInfo)
            vm.infoData(idInfo)
            vm._data.showDetails.showInfo=true
          }
            if(obj.event==="demo"){
                // console.log(JSON.stringify(obj.data));
                // applicationId = obj.data.id;
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                active.getLicenseServiceInfo(obj.data.id,active.softwareDetailsData)
                // console.log(obj.data.id);
            }
            else if(obj.event==="demo1"){
                // console.log(obj.data.id)
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                active.getLicenseServiceInfo(obj.data.id,active.editBuildLicence)
                // $('.software-details-right').css("display","block");
                // active.editBuildLicence()
            }else if(obj.event==="demo2"){
                // console.log(11111)
                // console.log(JSON.stringify(obj.data));

                $('.software-details-right .name').html(obj.data.name);
                $('.software-details-right .daemonName').html(obj.data.daemonName);
                $('.software-details-right .moduleTotal').html(obj.data.moduleTotal);
                $('.software-details-right .useNumber').html(obj.data.useNumber);

                moduleName = obj.data.name;
                daemonName = obj.data.daemonName;
                console.log(obj)
                active.tableList3(0,moduleApplicationId,obj.data.name,obj.data.daemonName, obj.data.licServiceId);
                active.getTableListPage3(moduleApplicationId,obj.data.name,obj.data.daemonName, obj.data.licServiceId);

              }else if(obj.event=== 'edit'){
                vm.init(obj.data.id)
                vm._data.showDialog=true
            }



        });
        //新建许可服务
        $(".new-build-service").click(function(){
            //示范一个公告层
            layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            active.querySoftwareAll(active.newBuildLicence);
            // active.newBuildLicence();
        });
        //编辑许可服务
        $(".edit-build-service").click(function(){
            var checkStatus = table.checkStatus('licenceList');
            if(checkStatus.data.length==1){
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                active.getLicenseServiceInfo(checkStatus.data[0].id,active.editBuildLicence)
            }
            else if(checkStatus.data.length>1){
                layer.msg('请选择一个服务',{icon:2});
            }
            else{
                layer.msg('请选择服务',{icon:2});
            }
            //示范一个公告层
            // active.editBuildLicence()

        });
        //删除
        $(".del-licence").on("click",function(){
            var checkStatus = table.checkStatus('licenceList');
            let checkArray = [];
            for(let i=0;i<checkStatus.data.length;i++){
                checkArray.push(checkStatus.data[i].id);
            }
            // console.log(checkArray);
            if(checkArray.length>0){
                layer.confirm('您确定要删除吗？', {
                    btn: ['确定','取消'],//按钮
                    //
                }, function(){
                    layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                    active.delLicence(checkArray.toString());
                }, function(){
                    layer.msg('您取消了删除', {
                        icon:2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn:1
                    });
                });
            }
            else{
                layer.msg('请选择服务',{icon:2,closeBtn:1});
            }

            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         // console.log(checkStatus.data[0].applicationId)
            //         active.delLicence(checkStatus.data[0].id);
            //
            //     }, function(){
            //         layer.msg('您取消了删除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            //     // console.log(checkStatus.data[0].id)
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个用户',{icon:2});
            // }
            // else{
            //     layer.msg('请选择用户',{icon:2});
            // }
        });
        //下载导入模板
        $(".downloadTemplate").on('click',function(){
            window.open(baseURL_SON + '/downloadTemplate');
        });
        //上传导入
        upload.render({
            elem:".importLicenseService",
            url:baseURL_SON + '/importLicenseService',
            method:'post',
            accept:'file',
            exts:'xls|xlsx',
            before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                console.log(JSON.stringify(obj))
                layer.load(0, {shade: [0.1]}); //上传loading
            },
            done: function(res){
                console.log(res)
                if(res.code==200){
                    active.tableList1();
                    active.getTableListPage1();
                    layer.closeAll('loading');
                    layer.msg('上传成功',{icon:1,closeBtn:1});


                }
                else if(res.code==400){
                    layer.closeAll('loading');
                    layer.msg(res.message,{icon:2,closeBtn:1});
                }

                // alert("上传成功")
                //上传完毕回调
            },
            error: function(res){
                console.log(res);
                layer.closeAll('loading');
                alert("上传失败")
                //请求异常回调
            }
        });
        //导出功能
        $(".exportLicenseService").on('click',function(){
            window.open(baseURL_SON + '/exportLicenseService');
        });




        //打开桌面
        $('#openDesktop').on('click',function () {
            var checkStatus = table.checkStatus('test2');
            // console.log(JSON.stringify(checkStatus));
            if(checkStatus.data.length==1){
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                active.open(checkStatus.data[0].userId,appId,checkStatus.data[0].sysUserId)
                // active.open(5,7)
            }
            else if(checkStatus.data.length>1){
                layer.msg('请选择一个许可桌面',{icon:2,closeBtn:1});
            }
            else{
                layer.msg('请选择许可桌面',{icon:2,closeBtn:1});
            }
        });
        //释放许可
        $('#releaseLicense').on('click',function () {
            var checkStatus = table.checkStatus('test2');
            // console.log(JSON.stringify(checkStatus));
            if(checkStatus.data.length==1){
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                active.closeLicenseProcess (checkStatus.data[0].id)
            }
            else if(checkStatus.data.length>1){
                layer.msg('请选择一个许可桌面',{icon:2,closeBtn:1});
            }
            else{
                layer.msg('请选择许可桌面',{icon:2,closeBtn:1});
            }
        });
        //关闭桌面
        $('#closeDesktop').on('click',function () {
            var checkStatus = table.checkStatus('test2');
            // console.log(JSON.stringify(checkStatus));
            if(checkStatus.data.length==1){
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                active.close(checkStatus.data[0].userId,appId)
            }
            else if(checkStatus.data.length>1){
                layer.msg('请选择一个许可桌面',{icon:2,closeBtn:1});
            }
            else{
                layer.msg('请选择许可桌面',{icon:2,closeBtn:1});
            }
        });



        //搜索
        $("#btn-footer1").on("click", function() {
            // console.log($("#index-footer1").val());
            if(!$("#index-footer1").val()){
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer1"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else{
                searchName = $("#index-footer1").val()
                active.tableList1(0,searchName)
                active.getTableListPage1(searchName)
                // active.tableList1(0,$("#index-footer1").val())
                // active.getTableListPage1($("#index-footer1").val())
            }

        });
        $("#index-footer1").on("keypress", function() {
            // console.log($("#index-footer1").val());
            if(event.keyCode == "13") {
                if (!$("#index-footer1").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer1"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    searchName = $("#index-footer1").val()
                    active.tableList1(0, searchName)
                    active.getTableListPage1(searchName)
                    // active.tableList1(0,$("#index-footer1").val())
                    // active.getTableListPage1($("#index-footer1").val())
                }
            }
        });
        //键盘抬起重新加载
        $("#index-footer1").on("keyup",function(){
            if(!$("#index-footer1").val()){
                searchName = "";
                active.tableList1();
                active.getTableListPage1();
            }
        });
        $("#icon-footer1").on("click",function(){
            $(this).prev().val('');
            searchName = "";
            active.tableList1();
            active.getTableListPage1();

        });
        //状态指示
        //状态指示
        $('body').on('mouseover','.fail',function () {

            layerTipFail = layer.tips('停用', $(this), {
                tips: [2,'#FF5722'],
                tipsMore: false,
                // time:0,
                // closeBtn:1
            })
        }).on('mouseout','.fail',function () {
            layer.close(layerTipFail);
        });
        $('body').on('mouseover','.success',function () {
            layerTipSuccess = layer.tips('启用', $(this), {
                tips: [2,'#FF5722'],
                tipsMore: false,
                // time:0,
                // closeBtn:1
            })
        }).on('mouseout','.success',function () {
            layer.close(layerTipSuccess);
        });
        // $('body').on("click",".add",function(){
        //     $(this).parent().parent().append("<div class=\"layui-form-item clearfix\">\n" +
        //         "                <label class=\"layui-form-label\"></label>\n" +
        //         "                <div class=\"layui-input-inline\">\n" +
        //         "                    <input name=\"title\" class=\"layui-input\" type=\"text\" placeholder=\"\" autocomplete=\"off\" lay-verify=\"required\">\n" +
        //         "                </div>\n" +
        //         "                <b class=\"add fl\">+</b>\n" +
        //         "            </div>\n" +
        //         "            <div class=\"layui-form-item\">\n" +
        //         "                <label class=\"layui-form-label\"></label>\n" +
        //         "                <div class=\"layui-input-inline\">\n" +
        //         "                    <input name=\"title\" class=\"layui-input\" type=\"text\" placeholder=\"\" autocomplete=\"off\" lay-verify=\"required\">\n" +
        //         "                </div>\n" +
        //         "            </div>");
        //     $(this).hide()
        // })

    });
})()