vm = new Vue({
  el: '#approval',
  data: function() {
    return {
      otherUnitsTree:[],
      otherSelectNodes:[],
      otherTreeIDs:[],
      otherTreeNames:[],
      otherNamesStr:'',

      ownSelectNodes:[],
      ownTree:[],
      ownTreeIDs:[],
      ownTreeNames:[],
      ownNamesStr:'',
      props: {
        children: 'children',
        label: 'name',
        id: 'id'
      },
      otherProps: {
        children: 'children',
        label: 'name',
        id: 'id'
      },
      ownradio:'',
      name:'',//用户名
      typeList:false,//申请类别
      typeList2:false,//申请类别
      typeList3:false,//申请类别
      projectName:"",
      value1:'',//开始时间
      value2:'',//结束时间
      state:'',//存储容量
      tips:'',//备注
      softwareType:'',//选中软件名称
      softwareTypeList:{//已添加软件类型

      },
      //审核通过拒绝需要的流程实例ID
      processInstanceId:'',
      //管理员搜索框
      inputSerch:'',
      //
      radio: '',
      deviceRadio: '',
      // 硬件分配完成后在显示确定按钮
      resources: false,
      // 待审核状态颜色
      typeStyle: '',
      // 审核记录文字颜色
      textStyle: '',
      // 默认显示的tab
      activeName: 'first',
      // table数据
      tableData: [],
      tableDataPage:0,
      approvalPendingPage:0,
      approvalPendingTotal:0,
      recordPage:0,
      recordTotal:0,
      pendingNum:0,
      //审批记录
      approvalRecord:[],
      //领导端待审批
      approvalPending:[],
      // dialog显示
      centerDialogVisible: false,
      // 硬件设备dialog
      centerDialogVisible1: false,
      selfNum:0,
      groupLists:[],
      deviceLists:[],
      distribution:{}
    }
  },

  methods: {
    ownClose(){
      this.distribution=''
    },
    handleClose(){
      this.softwareTypeList={}
    },
    //获取待审批列表
    getApprovalPending(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/waitApproval',
        type: 'post',
        async: false,
        data: {currentUserId:'374a164406544137994d6e7f3bdbedaa',
          page:_that.approvalPendingPage,size:10,conditions:this.inputSerch},
        success: function(data) {
          _that.$set(_that,"approvalPending",data.data.list)
          _that.$set(_that,"approvalPendingTotal",data.data.count)
          _that.$set(_that,"pendingNum",data.data.count)
          _that.$set(_that,"selfNum",data.data.count)
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    //获取审批记录列表
    getApprovalRecord(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/myApplyDetail',
        type: 'post',
        async: false,
        data: {currentUserId:'374a164406544137994d6e7f3bdbedaa',page:_that.tableDataPage,
          size:10,conditions:this.inputSerch},
        success: function(data) {
          _that.$set(_that,"approvalRecord",data.data.list)
          _that.$set(_that,"recordTotal",data.data.count)
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    // 上级搜索按钮
    searchLeader() {
      if(this.activeName=='first'){
        this.approvalPendingPage=0
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=0
        this.getApprovalRecord()
      }
    },
    // 管理员搜索按钮
    searchAdmin() {
      if(this.activeName=='first'){
        this.approvalPendingPage=0
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=0
        this.getApprovalRecord()
      }
    },
    getCookie: function (name) {
      var cookie = document.cookie;
      var arr = cookie.split("; "); //将字符串分割成数组
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=");
        if (arr1[0] == name) {
          return unescape(arr1[1]);
        }
      }
      return "GG"
    },
    // 资源组单选按钮
    handleChange(val, val1) {
      let _that=this
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/hardwareByGroupId',
        type: 'post',
        async: false,
        data: {groupId:val1},
        success: function(data) {
          console.log(data)
          _that.deviceLists=data.data
        },
        error: function(data) {

        }
      })
    },
    handleDeviceChange(val, val1){
      this.distribution=val1
    },
    sureDistribution(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/auditBeg',
        type: 'post',
        async: false,
        data: {currentUserId:'374a164406544137994d6e7f3bdbedaa',whether:true,
          processInstanceId:this.tableData[0].processInstanceId,hardwareId:this.distribution.id,hardwareName:this.distribution.name},
        success: function(data) {
          _that.$message({
            message: '授权成功',
            type: 'success'
          });
          _that.getApprovalPending();
        },
        error: function(data) {

        }
      })
    },
    // tab页切换
    tabsHandleClick(tab, event) {
      this.inputSerch=''
      if(this.activeName=='first'){
        this.approvalPendingPage=0
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=0
        this.getApprovalRecord()
      }
    },
    // 分页改变
    currentChange(val) {
      console.log(val)
    },
    handleCurrentChange(val){
      if(this.activeName=='first'){
        this.approvalPendingPage=val
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=val
        this.getApprovalRecord()
      }
    },
    handlePendingChange(val){
      if(this.activeName=='first'){
        this.approvalPendingPage=val
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=val
        this.getApprovalRecord()
      }
    },
    // 审核回显
    open(val) {
      let _that=this;
      this.processInstanceId=val.row.processInstanceId
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/getUpdatePage',
        type: 'post',
        async: false,
        data: {processInstanceId:val.row.processInstanceId},
        success: function(data) {
          _that.name=data.data.currentUserName;
          _that.ownradio=data.data.applyType;
          if(data.data.applyType==1){
            _that.typeList=true
          }else if(data.data.applyType==2){
            _that.typeList2=true
            _that.$set(_that,'ownSelectNodes',data.data.ids.split(','))
            _that.$set(_that,'ownNamesStr',data.data.names)
          }else if(data.data.applyType==3){
            _that.typeList3=true
            _that.$set(_that,'otherSelectNodes',data.data.ids.split(','))
            _that.$set(_that,'otherNamesStr',data.data.names)
          }
          _that.projectName=data.data.projectName;
          _that.value1=data.data.startTime;
          _that.value2=data.data.endTime;
          _that.state=data.data.capacity;
          _that.tips=data.data.remark;
          let arr=data.data.softwareValue.split(",");
          let arr1=data.data.softwareTypeKey.split(",");
          let arr2=data.data.softwareNames.split(",");
          for(let i=0;i<arr.length;i++){
            _that.$set(_that.softwareTypeList,"index_"+i,{name:arr[i],softName:arr2[i]})
          }
        },
        error: function(data) {

        }
      })
    },
    //审核通过或拒绝
    submitApproval(adopt){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/auditBeg',
        type: 'post',
        async: false,
        data: {currentUserId:'374a164406544137994d6e7f3bdbedaa',processInstanceId:_that.processInstanceId,whether:adopt},
        success: function(data) {
          _that.$message({
            message: '审核成功',
            type: 'success'
          });
          _that.getApprovalPending()
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    selfOpen(params){
      console.log(params)
      this.tableData=[params.row]
    },
    // 打开硬件设备弹框
    openEquipment(val) {
      let _that=this
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/hardwareGroupList',
        type: 'post',
        async: false,
        success: function(data) {
        console.log(data)
          _that.groupLists=data.data
        },
        error: function(data) {

        }
      })
    },
    resetSearch(){
      this.inputSerch=''
      if(this.activeName=='first'){
        this.approvalPendingPage=0
        this.getApprovalPending()
      }else if(this.activeName=='second'){
        this.recordPage=0
        this.getApprovalRecord()
      }
    },
    tab () {//申请类型切换
      if (this.radio === 0) {
        this.typeList=false
        this.typeList2=false
        this.typeList3=false
      } else if(this.radio == 1) {
        this.typeList=true
        this.typeList2=false
        this.typeList3=false
      }else if(this.radio == 2) {
        this.typeList=false
        this.typeList2=true
        this.typeList3=false
      }else if(this.radio == 3) {
        this.typeList=false
        this.typeList2=false
        this.typeList3=true
      }
    },
    getOrgTreeAndName(){
      let _that=this
      let userId = this.getCookie("userId"); // 获取用户id
      $.ajax({
        url: 'http://10.1.3.150:9001/examineAndApproveResource/orgTreeAndName',
        type: 'post',
        async: false,
        data: {currentUserId:'374a164406544137994d6e7f3bdbedaa'},
        success: function(data) {
          _that.otherUnitsTree=data.data.orgTree
          _that.ownTree=data.data.rankTree
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
  },
  mounted() {
    this.getApprovalPending();
    this.getApprovalRecord();
    this.getOrgTreeAndName();
  },
})
