
vm = new Vue({
    el: '#app',
    data: function () {
        return {
            ruleForm:{
                showTitle:1,
                noticeName:'',
                noticeDesc:'',
            },
            loginTitle:'',
            submitSerch:'',//搜索框
            showDialog: false,//弹框显隐
            tableData:[],
            tableDataPage:0,
            tableDataTotal:0,
            title:'新建',
            dialogDisabled:false,
            multipleSelectionRows:[],
            rules: {
                noticeName: [
                    {required: true, message: '请输入活动名称', trigger: 'blur'},
                ],
            },
            fileList:[],
        }
    },
    methods: {
        ownClosed(){
            this.dialogDisabled=false
            this.ruleForm.showTitle=1
            this.ruleForm.noticeName=''
            this.ruleForm.noticeDesc=''
        },
        init(){
            let _that=this
            let userId = this.getCookie("userId"); // 获取用户id
            // $.ajax({
            //   url: url+':9001/examineAndApproveResource/myApplyDetail',
            //   type: 'post',
            //   async: false,
            //   data: {currentUserId:userId,
            //     page:_that.tableDataPage,size:10,conditions:this.submitSerch},
            //   success: function(data) {
            let  data= {data:{list:[{
                        num:'1',
                        title:'啊啊啊啊啊',
                        time:'2019-01-01',
                    }],count:1}}
            _that.$set(_that,"tableData",data.data.list)
            _that.tableDataTotal=data.data.count
            //   },
            //   error: function(data) {
            //     console.log(data)
            //   }
            // })
        },
        sureCutOut(){
            let userId = this.getCookie("userId"); // 获取用户id
            let arr=[]
            for(let i=0;i<this.multipleSelectionRows.length;i++){
                arr.push(this.multipleSelectionRows[i].processInstanceId)
            }
            let _that=this
            $.ajax({
                url: url+':9001/examineAndApproveResource/revocationApply ',
                type: 'post',
                async: false,
                data: {currentUserId:userId,processInstanceId :arr.join(',')},
                success: function(data) {
                    if(data.data==true){
                        _that.$message({
                            message: '删除成功',
                            type: 'success'
                        });
                        _that.init();
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        },
        searchInput(params){
            this.tableDataPage=0
            this.init();
        },
        handleCurrentChange(val){
            this.tableDataPage=val
            this.init()
        },
        handleSelectChangeLeft(rows){
            this.multipleSelectionRows=rows;
        },
        handleOpen(key, keyPath) {
            console.log(key, keyPath);
        },
        handleClose(key, keyPath) {
            console.log(key, keyPath);
        },
        handleEdit(index, row) {
            this.title='详情'
            this.dialogDisabled=true
            let _that=this;
            // $.ajax({
            //   url: url+':9001/examineAndApproveResource/urgeTransaction',
            //   type: 'post',
            //   async: false,
            //   data: {processInstanceId :row.processInstanceId},
            //   success: function(data) {
            let data={data:{
                    num:1,
                    name:'lalla',
                    type:'23244'
                }}

            _that.showDialog=true
            _that.ruleForm.showTitle=data.data.num
            _that.ruleForm.noticeName=data.data.name
            _that.ruleForm.noticeDesc=data.data.type

            //   },
            //   error: function(data) {
            //     console.log(data)
            //   }
            // })
        },
        x(){//清空搜索
            this.submitSerch = ''
            this.tableDataPage=0
            this.init();
        },
        cutOut(done) {
            let _that=this
            if(this.multipleSelectionRows.length==0){
                this.$message({
                    message: '请至少选择一项删除',
                    type: 'error'
                });
                return;
            }
            this.$confirm('您确定要删除?', '删除', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                // type: 'warning'
            }).then(() => {
                _that.sureCutOut();
            }).catch(() => {
            });
        },
        closeBtn () {
            this.showDialog = false
        },
        newly(params){//新建请求
            if(params==='edit'){
                this.isEdit=true
                let _that=this
                console.log(this.multipleSelectionRows)
                if(this.multipleSelectionRows.length==1){
                    this.title="编辑"
                    _that.showDialog = true
                    // $.ajax({
                    //   url: url+':9001/examineAndApproveResource/getUpdatePage',
                    //   type: 'post',
                    //   async: false,
                    //   data: {processInstanceId:_that.multipleSelectionRows[0].processInstanceId},
                    //   success: function(data) {
                    let data={
                        data:{
                            num:1,
                            name:'lalla',
                            type:'23244'
                        }
                    }
                    _that.ruleForm.showTitle=data.data.num
                    _that.ruleForm.noticeName=data.data.name
                    _that.ruleForm.noticeDesc=data.data.type
                    // },
                    // error: function(data) {
                    //
                    // }
                    // })
                }else{
                    layer.msg("只能选择一项进行编辑", {
                        icon: 2,
                        closeBtn:1
                    });
                }
            }else{
                this.ruleForm.showTitle=1
                this.ruleForm.noticeName=''
                this.ruleForm.noticeDesc=''
                this.title="新建"
                this.showDialog = true
            }
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        submitForm(formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    alert('submit!');
                    let _that=this
                    let userId = this.getCookie("userId"); // 获取用户id
                    // $.ajax({
                    //   url: url+':9001/examineAndApproveResource/myApplyDetail',
                    //   type: 'post',
                    //   async: false,
                    //   data: {currentUserId:userId,
                    //     page:_that.tableDataPage,size:10,conditions:this.submitSerch},
                    //   success: function(data) {
                    let  data= {data:{list:[{
                                num:'1',
                                title:'啊啊啊啊啊',
                                time:'2019-01-01',
                            }],count:1}}
                    _that.$set(_that,"tableData",data.data.list)
                    _that.tableDataTotal=data.data.count
                    //   },
                    //   error: function(data) {
                    //     console.log(data)
                    //   }
                    // })
                } else {
                    return false;
                }
            });
        },
        submitUpload() {
            this.$refs.upload.submit();
        },
        handleRemove(file, fileList) {
            console.log(file, fileList);
        },
        handlePreview(file) {
            console.log(file);
        }
    },
    computed: {},
    mounted() {
        this.init();
    },
})
