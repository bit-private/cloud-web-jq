//角色与权限管理
var delRoleId=''
var ownsArr=[]
vm = new Vue({
  el: '#app',
  data: function () {
      return {
          dialogTitle:'新增用户',
          dialogVisible:false,
          idArr:[],
          data:[],
          userNames:'',
          userIds:'',
          RoleId:'',
          defaultProps: {
            children: 'children',
            label: 'name'
          }
      }
  },
  methods: {
    closeDialog(){
      this.dialogVisible = false
    },
    handleCheckChange(data, checked, indeterminate) {
      console.log(data, checked, indeterminate);
      console.log(this.$refs.tree.getCheckedNodes());
      let arr = []
      let arr1 = []
      for (let i = 0; i < this.$refs.tree.getCheckedNodes().length; i++) {
        if(this.$refs.tree.getCheckedNodes()[i].flag){

        }else{
          arr.push(this.$refs.tree.getCheckedNodes()[i].name)
          arr1.push(this.$refs.tree.getCheckedNodes()[i].id)
        }
      }
      this.userNames = arr.join(',')
      this.userIds = arr1.join(',')
    },
    addusers(){
      let _that = this
      console.log(this.RoleId)
      console.log(this.userIds)
      if(this.userIds == ''){
        layer.msg('请选择要添加的用户', {
          icon: 3,
          // time: 2000, //20s后自动关闭
          // btn: ['确定'],
          closeBtn: 1
        })
        return
      }
      $.ajax({
        url: url + ':9000/privilege/userRoleAdd',
        // url:"http://10.1.2.251:9000/privilege/queryPrivilege",
        type: 'post',
        headers:{
          'Content-Type':'application/json;charset=utf8'
        },
        async: true,
        data:JSON.stringify({roleId:this.RoleId,userIds:this.userIds}) ,
        success: function(data) {
          console.log(data)
          if (data.code == 200) {
            _that.dialogVisible = false
            _that.userNames=''
            _that.userIds=''
            // console.log(data.data[0]);
            for(let i=0;i< $('.layui-form-radio').length;i++){
              ownsArr.push($($('.layui-form-radio')[i]).hasClass('layui-form-radioed'))
            }
            active.getRole()
            active.findPrivilegeByRoleId(delRoleId)
            active.getRoles()
            // console.log(JSON.stringify(data));

          } else {
            layer.msg(data.message, {
              icon: 3,
              // time: 2000, //20s后自动关闭
              // btn: ['确定'],
              closeBtn: 1
            })
          }
        },
        // error: function(data) {
        //   window.location.href = '../../error/error.html'
        //   console.log(data.message)
        // }
      })
    }
  },
  computed: {},
  mounted() {
  },

})

//ztree树状

//layui
;(function() {
 
  const baseURL_SON = url + ':9000/privilege'
  // var delRoleId = ''
  var delIds = []
  //JavaScript代码区域
  layui.use(['element', 'form', 'layer'], function() {
    var element = layui.element,
      form = layui.form,
      layer = layui.layer,
      tree = layui.tree

    //获取选中的节点
    $('.determine').on('click', function() {
      if ($('.person').html() == '') {
        layer.msg('请选择角色', {
          icon: 2,
          closeBtn: 1
        })
      } else {
        var menuIds = []
        for (let i = 0; i < xtree1.GetChecked().length; i++) {
          menuIds.push(xtree1.GetChecked()[i].value)
          menuIds.push(xtree1.GetParent(xtree1.GetChecked()[i].value).value)
          console.log(xtree1.GetChecked()[i].value)
          console.log(xtree1.GetParent(xtree1.GetChecked()[i].value).value)
        }

        //数组去重
        // menuIds = [...new Set(menuIds)];
        menuIds = active.unique(menuIds)
        // console.log(menuIds)
        active.editPrivilege(delRoleId, menuIds.toString())
      }
    })
    var active = {
      // 数组去重
      unique: function(arr) {
        if (!Array.isArray(arr)) {
          console.log('type error!')
          return
        }
        var array = []
        for (var i = 0; i < arr.length; i++) {
          if (array.indexOf(arr[i]) === -1) {
            array.push(arr[i])
          }
        }
        return array
      },
      //初始化
      init: function() {
        //获取角色和人数
        this.getRole()
        this.menuTree()
      },
      //获取权限树
      menuTree: function() {
        $.ajax({
          url: baseURL_SON + '/menuTree',
          type: 'post',
          async: true,
          success: function(data) {
            console.log(data.data);
            if (data.code == 200 && data.message == '查询成功') {
              // console.log(data.data[0]);
              active.renderAuthorityTree(data)
              // console.log(JSON.stringify(data));
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            }
          },
          // error: function(data) {
          //   window.location.href = '../../error/error.html'
          //   console.log(data.message)
          // }
        })
      },
      //编辑回显权限树
      findPrivilegeByRoleId: function(roleId) {
        $.ajax({
          url: baseURL_SON + '/queryPrivilege',
          // url:"http://10.1.2.251:9000/privilege/queryPrivilege",
          type: 'post',
          async: true,
          data: { roleId },
          success: function(data) {
            if (data.code == 200) {
              // console.log(data.data[0]);

              active.renderAuthorityTree(data)
              // console.log(JSON.stringify(data));
            } else if(data.message=='参数不能为空'){
              return
            }else{
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            }
          },
          // error: function(data) {
          //   window.location.href = '../../error/error.html'
          //   console.log(data.message)
          // }
        })
      },
      //权限树的渲染
      renderAuthorityTree: function(data) {
        // console.log(data)
        var json = []
        for (let i = 0; i < data.data.length; i++) {
          var json_children = []
          json_children.push(data.data[i])
          // console.log(JSON.stringify(json_children));
          json.push(json_children)
          // json = data.data[i];
        }
        // console.log(json);

        xtree1 = new layuiXtree({
          elem: 'xtree1', //(必填) 放置xtree的容器，样式参照 .xtree_contianer
          form: form, //(必填) layui 的 from
          data: json[0] //(必填) json数据
        })
      },
      treeList:function (params) {
        $('#demo').empty();
        tree({
            elem: '#demo' //传入元素选择器
            ,nodes:params
            ,click: function(node) {
                editDepID = node.id;
                editDepPID = node.pid;
                // active.tableList(0,node.id);
                // active.getPage(node.id);
                orgId=node.id
                console.log(node) //node即为当前点击的节点数据
            }
        });
    },
      //用户树的渲染
      renderAuthorityTrees: function(data) {
        console.log(data)
        var json = []
        for (let i = 0; i < data.data.length; i++) {
          var json_children = []
          json_children.push(data.data[i])
          // console.log(JSON.stringify(json_children));
          json.push(json_children)
          // json = data.data[i];
        }
        console.log(json);

        xtree2 = new layuiXtree({
          elem: 'xtree2', //(必填) 放置xtree的容器，样式参照 .xtree_contianer
          form: form, //(必填) layui 的 from
          data: json[0] //(必填) json数据
        })
      },
      //提交编辑权限树
      editPrivilege: function(roleId, menuIds) {
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        $.ajax({
          url: baseURL_SON + '/editPrivilege',
          type: 'post',
          async: true,
          data: { roleId, menuIds },
          success: function(data) {
            layer.closeAll('loading') //关闭加载层
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
              active.findPrivilegeByRoleId(roleId)
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll('loading') //关闭加载层
            // window.location.href = '../../error/error.html'
            console.log(data.message)
          }
        })
      },
      //获取角色和人数
      getRole: function() {
        $.ajax({
          url: baseURL_SON + '/numberAndCharacters',
          type: 'post',
          async: true,
          success: function(data) {
            // that.treeList(data.data);
            // console.log(JSON.stringify(data));
            if(data.data.length==0){
              delRoleId=''
            }else{
              
              delRoleId = data.data[0].id
            }
            // vm.$data.RoleId = data.data[0].id
            console.log(data)
            if (data.code == 200 && data.message == '查询成功') {
              var radioRole = ''
              for (let i = 0; i < data.data.length; i++) {
                radioRole +=
                  '<div class="radioRole">\n' +
                  '              <input type="radio" name="sex" value="' +
                  data.data[i].id +
                  '" title="' +
                  data.data[i].roleName +
                  '">\n' +
                  '              <b>' +
                  data.data[i].countUser +
                  '</b>\n' +
                  '        </div>'
              }
              $('.roleShow').empty()
              $('.roleShow').append(radioRole)
              var rolenametext=''
              if(ownsArr.indexOf(true)==-1){
                if(data.data.length==0){
                  delRoleId = ''
                vm.$data.RoleId = ''
                }else{
                  delRoleId = data.data[0].id
                  vm.$data.RoleId = data.data[0].id
                  $('.main-right .main-btn2 p span').text(data.data[0].roleName)
                }
              }else{
                let index=ownsArr.indexOf(true)
                delRoleId = data.data[index].id
                vm.$data.RoleId =data.data[index].id
                rolenametext=data.data[index].roleName
              }
              // delRoleId = data.data[0].id
              active.findPrivilegeByRoleId(delRoleId)
             
              form.render()
              active.clickRoleName()
              active.getRoles()
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            }
            if(ownsArr.indexOf(true)==-1){
              $('input:radio').eq(0).attr('checked', 'true')
            }else{
              let index=ownsArr.indexOf(true)
              $('input:radio').eq(index).attr('checked', 'true')
            }
            ownsArr=[]
            form.render()
          }
        })
      },
      //获取用户
      getRoles: function() {
        // console.log(delRoleId,2222)
        $.ajax({
          url: baseURL_SON + '/users/'+delRoleId,
          type: 'get',
          async: true,
          success: function(data){
            // that.treeList(data.data);
            console.log(data);
            // active.findPrivilegeByRoleId(data.data[0].id)
            if (data.code == 200 && data.message == '查询成功') {
              var radioRole = ''
              
              if(data.length==0){

              }else{
                for (let i = 0; i < data.data.length; i++) {
                  radioRole +=
                    '<div class="radioRoles" title="'+data.data[i].detail+'">\n'+
                    '      <input type="checkbox" value="'+
                    data.data[i].id +
                    '" title="' +
                    data.data[i].detail +'" lay-skin="primary">\n'+
                    '</div>'
                }
              }
              
              // for (let i = 0; i < data.data.length; i++) {
              //   radioRole +=
              //     '<div class="radioRole">\n' +
              //     '              <input type="radio" name="sex" value="' +
              //     data.data[i].id +
              //     '" title="' +
              //     data.data[i].detail +
              //     '">\n'
              //     '        </div>'
              // }
              // console.log(radioRole)
              $('.roleShows').empty()
              $('.roleShows').append(radioRole)
              delIds = []
              // form.render()
              // form.render()
              // active.clickRoleName()
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            }
            // $('input:radio').eq(0).attr('checked', 'true')
            form.render()
          }
        })
      },
      //查询具体人数
      // getUserNameByRoleId: function(roleId, DOM) {
      //   $.ajax({
      //     url: baseURL_SON + '/queryTheRoleUserName',
      //     type: 'post',
      //     async: true,
      //     data: { roleId },
      //     success: function(data) {
      //       if (data.code == 200 && data.message == '查询成功') {
      //         layer.tips(data.data[0].userName, DOM, {
      //           tips: [2, '#FF5722'],
      //           tipsMore: false,
      //           time: 0,
      //           closeBtn: 1
      //         })
      //       } else {
      //         layer.msg(data.message, {
      //           icon: 3,
      //           // time: 2000, //20s后自动关闭
      //           // btn: ['确定'],
      //           closeBtn: 1
      //         })
      //       }
      //     },
      //     // error: function(data) {
      //     //   window.location.href = '../../error/error.html'
      //     //   console.log(data.message)
      //     // }
      //   })
      // },

      //新建角色弹框
      newBuildUser: function(params) {
        let index = layer.open({
          type: 1,
          title: ['新建角色', 'font-size:20px;font-weight:900'], //不显示标题栏
          closeBtn: 1,
          area: ['350px', '180px'],
          shade: 0.8,
          id: 'LAY_layuipro', //设定一个id，防止重复弹出
          // ,resize: false
          // ,btn: ['确定']
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('.new-role'),
          success: function(layero) {
            $('.cancel').on('click', function() {
              $('.new-role input[name=roleName]').val('')
              //关闭所有弹框
              layer.close(index)
            })
          },
          end: function(index, layero) {
            $('.new-role').hide()
            $('.new-role input[name=roleName]').val('')
          }
        })
      },
      // 增加用户
      newBuildUsers: function(params) {
        console.log(vm)
        
        vm.$data.dialogVisible = true
        vm.$data.userNames = ''
        console.log('111')
        vm.$data.userIds = ''
        console.log(url)
        $.ajax({
          url: url + ':9000/organization/getOrganizationAndUsers/'+delRoleId,
          type: 'get',
          async: true,
          success: function(data) {
            console.log(data);
            let arr=[]
            // active.treeList(data.data)
            vm.$data.data = data.data
            data.data.forEach(m=>{
               if(m.children){
                 m.children.forEach(mChile=>{
                   arr.push(mChile.id)
                 })
               }
            })
            vm.$data.idArr = arr
            console.log(arr)
          }
        })
        // let index = layer.open({
        //   type: 1,
        //   title: ['新增用户', 'font-size:20px;font-weight:900'], //不显示标题栏
        //   closeBtn: 1,
        //   area: ['350px', '580px'],
        //   shade: 0.8,
        //   id: 'LAY_layuipros', //设定一个id，防止重复弹出
        //   // ,resize: false
        //   // ,btn: ['确定']
        //   btnAlign: 'c',
        //   moveType: 1, //拖拽模式，0或者1
        //   content: $('.new-roles'),
        //   success: function(layero) {
        //     $('.cancels').on('click', function() {
        //       //关闭所有弹框
        //       layer.close(index)
        //     })
        //   },
        //   end: function(index, layero) {
        //     $('.new-roles').hide()
        //   }
        // })
      },
      //新建角色提交
      newAddRole: function(data) {
        $.ajax({
          url: baseURL_SON + '/newAddRole',
          type: 'post',
          async: true,
          data,
          success: function(data) {
            // that.treeList(data.data);
            // console.log(JSON.stringify(data));
            layer.closeAll('loading') //关闭加载层
            if (data.code == 200) {
              $('.new-role input[name=roleName]').val('')
              layer.msg(data.message, {
                icon: 1,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
              active.getRole()
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll('loading') //关闭加载层
            // window.location.href = '../../error/error.html'
            console.log(data.message)
          }
        })
      },
      //点击人数显示具体人员
      clickRoleName: function() {
        //tips层
        $(document).ready(function() {
          $('.roleShow b').on('click', function(e) {
            // console.log($(this).prev().prev().val())
            active.getUserNameByRoleId(
              $(this)
                .prev()
                .prev()
                .val(),
              $(this)
            )
          })
        })
      },
      //删除角色
      deleteRole: function(roleId) {
        $.ajax({
          url: baseURL_SON + '/deleteRole',
          // url:"http://10.1.2.251:9000/privilege/deleteRole",
          type: 'post',
          async: true,
          data: { roleId },
          success: function(data) {
            // that.treeList(data.data);
            console.log(JSON.stringify(data))
            layer.closeAll('loading') //关闭加载层
            if (data.code == 200 && data.data == 1) {
              layer.msg(data.message, {
                icon: 1,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
              $('.main-right .main-btn2 p span').text('')
              active.getRole()
              active.menuTree()
              active.getRoles()
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
              // $('.main-right .main-btn2 p span').text('');
              // active.getRole();
              // active.menuTree();
            }
          },
          error: function(data) {
            layer.closeAll('loading') //关闭加载层
            // window.location.href = '../../error/error.html'
            console.log(data.message)
          }
        })
      },
      //删除角色
      deleteRoles: function(roleId) {
        $.ajax({
          url: baseURL_SON + '/userRoleDelete',
          // url:"http://10.1.2.251:9000/privilege/deleteRole",
          type: 'delete',
          async: true,
          headers:{
            'Content-Type':'application/json;charset=utf8'
          },
          data:JSON.stringify({roleId:delRoleId,userIds:roleId.join(',')}),
          success: function(data) {
            // that.treeList(data.data);
            console.log(data)
            layer.closeAll('loading') //关闭加载层
            if (data.code == 200 && data.data == 1) {
              layer.msg(data.message, {
                icon: 1,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
              for(let i=0;i< $('.layui-form-radio').length;i++){
                ownsArr.push($($('.layui-form-radio')[i]).hasClass('layui-form-radioed'))
              }
              active.getRole()
              active.findPrivilegeByRoleId(delRoleId)
              active.getRoles()
            } else {
              layer.msg(data.message, {
                icon: 3,
                // time: 2000, //20s后自动关闭
                // btn: ['确定'],
                closeBtn: 1
              })
              // $('.main-right .main-btn2 p span').text('');
              // active.getRole();
              // active.menuTree();
            }
          },
          error: function(data) {
            layer.closeAll('loading') //关闭加载层
            // window.location.href = '../../error/error.html'
            console.log(data.message)
          }
        })
      }
    }
    active.init()
    window.active=active
    //编辑权限
    //新建角色提交
    form.on('submit(formDemo)', function(data) {
      layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
      console.log(JSON.stringify(data.field))
      active.newAddRole(data.field)
      return false
    })

    //监听单选
    form.on('radio()', function(data) {
      console.log(data)
      $('.main-right .main-btn2 p span').text(data.elem.title)
      delRoleId = data.value
      vm.$data.RoleId = data.value
      active.getRoles()
      active.findPrivilegeByRoleId(delRoleId)
    })
    form.on('checkbox()', function(data) {
      console.log(data.value)
      if(delIds.indexOf(data.value) == -1){
        delIds.push(data.value)
      }else{
        delIds.splice(delIds.indexOf(data.value),1)
      }
      console.log(delIds)
      // $('.main-right .main-btn2 p span').text(data.elem.title)
      // delRoleId = data.value
      // vm.$data.RoleId = data.value
      // console.log(delRoleId)
      // active.getRoles()
      // active.findPrivilegeByRoleId(delRoleId)
    })
    //新建角色弹框
    $('.new-build-role').click(function() {
      active.newBuildUser()
    })
    //新增用户弹框
    $('.new-build-roles').click(function() {
      active.newBuildUsers()
    })
    //删除
    $('.del-role').on('click', function() {
      if (delRoleId) {
        layer.confirm(
          '您确定要删除吗？',
          {
            btn: ['确定', '取消']
          },
          function() {
            layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
            active.deleteRole(delRoleId)
          },
          function() {
            layer.msg('您取消了删除', {
              icon: 2,
              closeBtn: 1
            })
          }
        )
      } else {
        layer.msg('请选择你要删除的角色', {
          icon: 2,
          closeBtn: 1
        })
      }
    })
    $('.del-roles').on('click', function() {
      if (delIds.length>0) {
        layer.confirm(
          '您确定要删除吗？',
          {
            btn: ['确定', '取消']
          },
          function() {
            layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
            active.deleteRoles(delIds)
          },
          function() {
            layer.msg('您取消了删除', {
              icon: 2,
              closeBtn: 1
            })
          }
        )
      } else {
        layer.msg('请选择你要删除的角色', {
          icon: 2,
          closeBtn: 1
        })
      }
    })

    //搜索
    $('.search-btn').on('click', function(params) {
      layer.tips('不能为空！！！', $('#search-id'), { tips: [3, '#FF5722'] }) //在元素的事件回调体中，follow直接赋予this即可
    })
  })
})()
