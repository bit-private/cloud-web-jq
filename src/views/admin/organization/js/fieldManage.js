/*
    * When I wrote this,only God and I understood what I was doing
    * Now,God only knows
    * 写这段代码的时候，只有上帝和我知道它是干嘛的。
    * 现在，只有上帝知道
    * */

//域管理

(function () {
    const baseURL_SON = url + ":9000/domain";
    //编辑域回显ID
    var domainID = "";
    //新建系统用户信息
    // var newUserMessage = "";
    var pageFlag;
    //需要还原的列表数据
    var systemUser = [];
    //需要还原用户的id
    var systemUserIds = [];
    //搜索
    var where;

    //JavaScript代码区域
    layui.use(['element','form','table','layer','laypage','upload'], function() {
        var element = layui.element
            ,form = layui.form
            ,table = layui.table
            ,layer = layui.layer
            ,upload = layui.upload
            ,laypage = layui.laypage;

        form.render();


        var active={
            //初始化
            init:function () {
                // console.log($(this)[0]);
                // console.log(this);
                this.tableList1();
                this.tableList2();
                this.getTableListPage2();
            },
            //表格1
            tableList1:function () {
              $.ajax({
                url: baseURL_SON + '/queryDomain',
                type: 'post',
                success: function(data) {
                  console.log(data)
                  table.render({
                    elem: '#fieldTable',
                    data:data.data,
                    cols: [[
                      {type:'checkbox'}
                      ,{field:'name', title: '域名',event:'editBuildField',style:'cursor:pointer',templet: '#mouseHov'}
                      ,{field:'domainType', title: '类别'}
                      ,{field:'ip', title: '主机IP地址'}
                      ,{field:'follow_ip', title: '从机IP地址'}
                      ,{field:'description', title: '描述'}
                    ]],
                    response: {
                      statusName: 'code' //数据状态的字段名称，默认：code
                      ,statusCode: 200 //成功的状态码，默认：0
                      ,msgName: 'message' //状态信息的字段名称，默认：msg
                      // ,countName: 'total' //数据总数的字段名称，默认：count
                      ,dataName: 'data' //数据列表的字段名称，默认：data
                    }
                  });
                },
                error: function(err) {
                  console.log(err)
                }
              })
            },
            //表格2
            tableList2:function (pageNum,where,sort,column) {
                $.ajax({
                    url:baseURL_SON + "/pagingQuerySystemUser",
                    type:"post",
                    async:true,
                    data:{page:pageNum==undefined? 0:pageNum,size:10,search:where==undefined? '':where,sort:sort==undefined? '':sort,column:column==undefined? '':column},
                    success:function(data){
                        // console.log(JSON.stringify(data));
                        table.render({
                            elem: '#sysUser'
                            ,cols:  [[ //标题栏
                                {type:'checkbox',  rowspan: 2}
                                ,{field: 'systemUser', title: '用户名',  rowspan: 2,sort:true} //rowspan即纵向跨越的单元格数
                                ,{align: 'NISfield', title: 'NIS域', colspan: 3} //colspan即横跨的单元格数，这种情况下不用设置field和width
                                ,{align: 'ADfield', title: 'AD域', colspan: 2} //colspan即横跨的单元格数，这种情况下不用设置field和width
                                // ,{field: 'ADfield', title: 'AD域',rowspan: 2 ,toolbar: '#barDemo1'} //colspan即横跨的单元格数，这种情况下不用设置field和width
                                // ,{field: 'pwdFlag', title: '确认密码',  rowspan: 2,align: 'center', toolbar: '#barDemo3'}
                                ,{field: 'descri', title: '描述',  rowspan: 2}
                            ], [
                                {field: 'nisUserId', title: '用户id',sort:true}
                                ,{field: 'nisGroupId', title: '组id',sort:true}
                                ,{field: 'nisUserContents', title: '用户目录',sort:true}
                                ,{field: 'adGroupId', title: '是否存在',toolbar: '#barDemo1'}
                                ,{field: 'adUserContents', title: '确认密码', toolbar: '#barDemo3'}
                            ]]
                            ,data:data.data.content
                        });
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });

            },
            //表格3，需要还原的用户
            tableList3:function (search) {
                $.ajax({
                    url:baseURL_SON + "/queryRemoveSystemUser",
                    // url:"http://10.1.2.251:9000/domain/queryRemoveSystemUser",
                    type:"post",
                    async:true,
                    data:{search:search==undefined? '':search},
                    success:function(data){
                        console.log(data)
                        systemUserIds = [];
                        systemUser = data.data;
                        if(data.code == 200) {
                            table.render({
                                elem: '#reduction_user',
                                limit: data.data.length,
                                cols:[[
                                        {type:'checkbox'},
                                        {field:'systemUserName',title: '用户名',width:'290'}
                                    ]],
                                data:data.data
                            });
                        }else {
                            layer.msg(data.message, {
                                icon:1,
                                closeBtn:1
                            });
                        }
                        if(!search){
                            active.reductionUser()
                        }

                    },
                })

            },
            //新建域弹框
            newBuildField:function (params) {
                layer.closeAll('loading');
                var index = layer.open({
                    type: 1
                    ,title: ["新建域",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,area:['380px','650px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.new-field')
                    ,success: function(layero){
                        //取消按钮
                        $(".cancel").on("click",function () {
                            //关闭所有弹框
                            layer.close(index);
                        });
                    }
                    ,end: function(index, layero){
                        active.clearNewFieldData();
                        $(".new-field").hide()
                    }
                });
            },
            //新建域提交
            addDomain:function (data) {
                $.ajax({
                    url:baseURL_SON + "/addDomain",
                    type:"post",
                    async:true,
                    data:data.field,
                    success:function(data){
                        // console.log(JSON.stringify(data));
                        if(data.data==1){
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon:1,
                                closeBtn:1
                            });
                            active.tableList1()
                        }else{
                            layer.msg(data.message, {
                                icon:2,
                                closeBtn:1
                            });
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //编辑域弹框
            editBuildField:function (params) {
                layer.closeAll('loading');
                var index = layer.open({
                    type: 1
                    ,title: ["编辑域",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,area:['380px','650px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.edit-field')
                    ,success: function(layero){
                        //取消按钮
                        $(".cancel").on("click",function () {
                            //关闭弹框
                            layer.close(index);
                        });
                    }
                    ,end: function(index, layero){
                        $(".edit-field").hide()
                    }
                });
            },
            //编辑域提交
            updateDomain:function (data) {
                console.log(JSON.stringify(data.field));
                $.ajax({
                    url:baseURL_SON + "/updateDomain",
                    type:"post",
                    async:true,
                    data:data.field,
                    success:function(data){
                        console.log(JSON.stringify(data));
                        if(data.code==200){
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon:1,
                                closeBtn:1
                            });
                            active.tableList1()
                        }else{
                            layer.msg(data.message, {
                                icon:2,
                                closeBtn:1
                            });
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //删除域
            delField:function (domainIds) {
                $.ajax({
                    url:baseURL_SON + "/deleteDomain",
                    type:"post",
                    async:true,
                    data:{domainIds},
                    success:function(data){
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        if(data.code==200 && data.data>0){
                            layer.msg('删除成功', {
                                icon: 1,
                                closeBtn:1
                            });
                            active.tableList1();
                        }
                        else{
                            layer.msg('删除失败', {
                                icon:3,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     layer.closeAll('loading');
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            //删除域用户
            delFieldUser:function (systemUserIds) {
                $.ajax({
                    url:baseURL_SON + "/deleteSystemUser",
                    type:"post",
                    async:true,
                    data:{systemUserIds},
                    success:function(data){
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        if(data.code==200 && data.data>0){
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn:1
                            });
                            active.tableList2();
                            active.getTableListPage2()
                        }
                        else{
                            layer.msg(data.message, {
                                icon:3,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     layer.closeAll('loading');
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            //移除域用户
            removeFieldUser:function (systemUserIds) {
                // console.log(ids);
                $.ajax({
                    url:baseURL_SON + "/removeSystemUser",
                    type:"post",
                    async:true,
                    data:{systemUserIds},
                    success:function(data){
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        if(data.code==200 && data.data>0){
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn:1
                            });
                            active.tableList2();
                            active.getTableListPage2()
                        }
                        else{
                            layer.msg(data.message, {
                                icon:3,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     layer.closeAll('loading');
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            //新增用户弹框
            newBuildUser:function () {
                layer.closeAll('loading');
                layer.open({
                    type: 1
                    ,title: ["新增用户",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,area:['1100px','450px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.new-user')
                    ,success: function(layero){
                        form.render();
                    //     var btn = layero.find('.layui-layer-btn');
                    //     // btn.find('.layui-layer-btn0').attr({
                    //     //     href: 'http://www.layui.com/'
                    //     //     ,target: '_blank'
                    //     // });
                    //     btn.find('.layui-layer-btn0').click(function(){
                    //         // alert(1111111111)
                    //     });
                    }
                    ,end: function(index, layero){
                        active.clearNewUserData();
                        $(".new-user").hide()
                    }
                });
            },
            //新增用户提交
            addSystemUser:function (data) {
                layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                $.ajax({
                    url:baseURL_SON + "/addSystemUser",
                    type:"post",
                    async:true,
                    data:data.field,
                    success:function(data){
                        console.log(JSON.stringify(data));
                        layer.closeAll('loading');
                        if(data.data==1){
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon:1,
                                closeBtn:1
                            });
                            active.tableList2();
                            active.getTableListPage2();
                        }else{
                            layer.msg(data.message, {
                                icon:2,
                                closeBtn:1
                            });
                        }

                    },
                    // error:function(data){
                    //     layer.closeAll('loading');
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //还原系统用户弹框
            reductionUser:function () {
                layer.closeAll('loading');
                layer.open({
                    type: 1
                    ,title: ["还原用户",'font-size:20px;font-weight:900'] //不显示标题栏
                    ,closeBtn: 1
                    ,area:['350px','550px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.reduction-user')
                    ,success: function(layero){
                        //     var btn = layero.find('.layui-layer-btn');
                        //     // btn.find('.layui-layer-btn0').attr({
                        //     //     href: 'http://www.layui.com/'
                        //     //     ,target: '_blank'
                        //     // });
                        //     btn.find('.layui-layer-btn0').click(function(){
                        //         // alert(1111111111)
                        //     });
                    }
                    ,end: function(index, layero){
                        $("#index-footer3").val('')
                        $(".reduction-user").hide()
                    }
                });
            },
            //还原系统用户提交
            subReductionUser:function (systemUserIds) {
                console.log(systemUserIds);
                $.ajax({
                    url:baseURL_SON + "/restoreRemoveSystemUser",
                    // url:"http://10.1.2.251:9000/domain/restoreRemoveSystemUser",
                    type:"post",
                    async:true,
                    data:{systemUserIds},
                    success:function(data){
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if(data.code==200 && data.data>0){
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn:1
                            });
                            active.tableList2();
                            active.getTableListPage2()
                        }
                        else{
                            layer.closeAll('loading');
                            layer.msg(data.message, {
                                icon:3,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn:1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     layer.closeAll('loading');
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            //确认密码
            confirmPass:function (systemUserId) {
                layer.closeAll('loading');
                // console.log(sysId)
                $('input[name=systemUserId]').val(systemUserId);
                $('input[name=newPassword]').val('');
                $('input[name=twicePassword]').val('');
                // console.log(sysId)
                var index = layer.open({
                    type: 1
                    ,title: "确认密码" //不显示标题栏
                    ,closeBtn: 1
                    ,area:['350px','230px']
                    ,shade: 0.8
                    ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.confirm-pass')
                    ,success: function(layero){

                    }
                    ,end: function(index, layero){
                        $(".confirm-pass").hide()
                    }
                });
            }, //确认密码

            //修改密码
            changePassword:function (systemUserId) {
                console.log(111)
                layer.closeAll('loading');
                // console.log(sysId)
                $('input[name=systemUserId]').val(systemUserId);
                $('input[name=newPassword]').val('');
                $('input[name=twicePassword]').val('');
                // console.log(sysId)
                var index = layer.open({
                    type: 1
                    ,title: "修改密码" //不显示标题栏
                    ,closeBtn: 1
                    ,area:['350px','230px']
                    ,shade: 0.8
                    ,id: 'kors' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    ,btnAlign: 'c'
                    ,moveType: 1 //拖拽模式，0或者1
                    ,content:$('.confirm-pass1')
                    ,success: function(layero){
                        $('input[name=AtwicePassword]').val('')
                        $('input[name=AnewPassword]').val('')
                    }
                    ,end: function(index, layero){
                        $(".confirm-pass1").hide()
                    }
                });
            },
            //获取表格2的页码
            getTableListPage2:function (where,sort,column) {
                $.ajax({
                    url:baseURL_SON + "/pagingQuerySystemUser",
                    type:"post",
                    async:true,
                    data:{page:0,size:10,search:where==undefined? '':where,sort:sort==undefined? '':sort,column:column==undefined? '':column},
                    success:function(data){
                        laypage.render({
                            elem:"sysUserPage",
                            count:data.data.pageCount
                            ,layout: ['prev', 'page', 'next', 'skip']
                            // ,limit:10
                            ,jump: function(obj, first){
                                //obj包含了当前分页的所有参数，比如：
                                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                // console.log(obj.limit); //得到每页显示的条数
                                // console.log(first);
                                //首次不执行
                                if(!first){
                                    active.tableList2(obj.curr,where,sort,column);
                                    //do something
                                }
                            }
                        })
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //获取域类别
            getDomainTypeList:function (cb) {
                $.ajax({
                    url:baseURL_SON + "/queryDomainType",
                    type:"post",
                    async:true,
                    success:function(data){
                        // console.log(JSON.stringify(data));
                        var optionString = "<option grade=\'请选择一级属性\' selected = \'selected\' class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                        if(data.data){                   //判断
                            for(var i=0; i<data.data.length; i++){ //遍历，动态赋值
                                optionString +="<option grade=\""+data.data[i].id+"\" value=\""+data.data[i].typeKey+"\"";
                                optionString += ">"+data.data[i].value+"</option>";  //动态添加数据
                            }

                            $('select[name=domainType]').empty();//清空子节点
                            $("select[name=domainType]").append(optionString);  // 添加子节点
                        }

                        form.render();
                        // form.render("select",'newField');
                        if(cb){
                            cb(domainID);
                            domainID = "";
                        }else{
                            active.newBuildField()
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //编辑域信息回显
            findDomainById:function (domainId) {
                $.ajax({
                    url:baseURL_SON + "/echoDomain",
                    type:"post",
                    async:true,
                    data:{domainId},
                    success:function(data){
                        // console.log(JSON.stringify(data));
                        $('.edit-field input[name=id]').val(data.data.id);
                        $('.edit-field input[name=name]').val(data.data.name);
                        $('.edit-field select[name=domainType]').val(data.data.domainType);
                        $('.edit-field input[name=ip]').val(data.data.ip);
                        $('.edit-field input[name=userName]').val(data.data.userName);
                        $('.edit-field input[name=password]').val(data.data.password);
                        $('.edit-field input[name=followIp]').val(data.data.followIp);
                        $('.edit-field textarea[name=description]').val(data.data.description);
                        form.render();
                        active.editBuildField();
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //验证系统用户唯一性
            verifySystemUserSole:function (systemUserId) {
                $.ajax({
                    url:baseURL_SON + "/verifySystemUserSole",
                    type:"post",
                    async:true,
                    data:{systemUserId},
                    success:function(data){

                        console.log(JSON.stringify(data));
                        if(data.code==200 && data.data>0){
                            pageFlag = data.data;
                            if(data.data==1){
                                //都可以创建
                                $('.new-user input[name=nisUserId]').attr('lay-verify','required');
                                $('.new-user input[name=nisGroupId]').attr('lay-verify','required');
                                $('.new-user input[name=nisUserContents]').attr('lay-verify','required');
                                $('.middle b').attr('hidden',false);
                            }
                            else if(data.data==2){
                                //ad可以创建
                                $('.new-user input[name=nisUserId]').val('');
                                $('.new-user input[name=nisGroupId]').val('');
                                $('.new-user input[name=nisUserContents]').val('');

                                $('.new-user input[name=nisUserId]').attr('disabled',true);
                                $('.new-user input[name=nisGroupId]').attr('disabled',true);
                                $('.new-user input[name=nisUserContents]').attr('disabled',true);

                                $('.new-user input[name=nisUserId]').addClass('dis');
                                $('.new-user input[name=nisGroupId]').addClass('dis');
                                $('.new-user input[name=nisUserContents]').addClass('dis');

                                $('.middle b').attr('hidden',true);
                            }
                            else if(data.data==3){
                                //nis可以创建
                                $('.new-user input[name=nisUserId]').attr('lay-verify','required');
                                $('.new-user input[name=nisGroupId]').attr('lay-verify','required');
                                $('.new-user input[name=nisUserContents]').attr('lay-verify','required');
                                $('.middle b').attr('hidden',false);

                                $('.new-user input[name=adGroupId]').val('');
                                $('.new-user input[name=adUserContents]').val('');
                                $('.new-user input[name=adGroupId]').addClass('dis');
                                $('.new-user input[name=adUserContents]').addClass('dis');
                                $('.new-user input[name=adGroupId]').attr('disabled',true);
                                $('.new-user input[name=adUserContents]').attr('disabled',true);
                            }

                        }
                        else{
                            $(".new-user input[name=systemUserId]").attr("placeholder","该用户已被注册！");
                            $('.new-user input[name=systemUserId]').val('');
                            active.clearDis();
                            layer.msg('该用户已被注册！',{
                                icon:2,
                                closeBtn:1
                            })
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //同步系统用户
            synchronousSystemUser:function () {
                $.ajax({
                    url:baseURL_SON + "/synchronousSystemUser",
                    type:"post",
                    async:true,
                    data:{},
                    success:function(data){
                        layer.closeAll();
                        // console.log(JSON.stringify(data));
                        if(data.code==200&&data.data==1){
                            layer.msg(data.message,{
                                icon:1,
                                closeBtn:1
                            })
                            active.tableList2();
                            active.getTableListPage2();
                        }else{
                            layer.msg(data.message, {
                                icon:2,
                                closeBtn:1
                            });
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //清空新建域弹框数据
            clearNewFieldData:function () {
                $('.new-field input[name=name]').val('');
                $('.new-field select[name=domainType]').val('');
                $('.new-field input[name=ip]').val('');
                $('.new-field input[name=userName]').val('');
                $('.new-field input[name=password]').val('');
                $('.new-field input[name=followIp]').val('');
                $('.new-field textarea[name=description]').val('');
            },
            //清空新建系统用户弹框数据
            clearNewUserData:function () {
                $(".new-user input[name=systemUserId]").attr("placeholder","");
                $('.new-user input[name=systemUserId]').val('');
                $('.new-user input[name=password]').val('');
                $('.new-user input[name=password1]').val('');
                $('.new-user input[name=nisUserId]').val('');
                $('.new-user input[name=nisGroupId]').val('');
                $('.new-user input[name=nisUserContents]').val('');
                $('.new-user input[name=adGroupId]').val('');
                $('.new-user input[name=adUserContents]').val('');
                $('.new-user textarea[name=descri]').val('');
                $('.new-user input[name=nisUserId]').attr('lay-verify','required');
                $('.new-user input[name=nisGroupId]').attr('lay-verify','required');
                $('.new-user input[name=nisUserContents]').attr('lay-verify','required');
            },
            //用户输入框事件
            //清空不可改变
            clearDis:function () {
                $('.new-user input[name=password]').val('');
                $('.new-user input[name=password1]').val('');
                $('.new-user input[name=nisUserId]').val('');
                $('.new-user input[name=nisGroupId]').val('');
                $('.new-user input[name=nisUserContents]').val('');
                // $('.new-user input[name=adGroupId]').val('');
                // $('.new-user input[name=adUserContents]').val('');
                $('.new-user textarea[name=descri]').val('');

                $('.new-user input[name=password]').attr('disabled',true);
                $('.new-user input[name=password1]').attr('disabled',true);
                $('.new-user input[name=nisUserId]').attr('disabled',true);
                $('.new-user input[name=nisGroupId]').attr('disabled',true);
                $('.new-user input[name=nisUserContents]').attr('disabled',true);
                // $('.new-user input[name=adGroupId]').attr('disabled',true);
                // $('.new-user input[name=adUserContents]').attr('disabled',true);
                $('.new-user textarea[name=descri]').attr('disabled',true);

                $('.new-user input[name=password]').addClass('dis');
                $('.new-user input[name=password1]').addClass('dis');
                $('.new-user input[name=nisUserId]').addClass('dis');
                $('.new-user input[name=nisGroupId]').addClass('dis');
                $('.new-user input[name=nisUserContents]').addClass('dis');
                // $('.new-user input[name=adGroupId]').addClass('dis');
                // $('.new-user input[name=adUserContents]').addClass('dis');
                $('.new-user textarea[name=descri]').addClass('dis');


                $('.new-user input[name=nisUserId]').removeAttr('lay-verify');
                $('.new-user input[name=nisGroupId]').removeAttr('lay-verify');
                $('.new-user input[name=nisUserContents]').removeAttr('lay-verify');
                $('.middle b').attr('hidden',true);
            },
            //可以操作
            operation:function () {
                $('.new-user input[name=password]').attr('disabled',false);
                $('.new-user input[name=password1]').attr('disabled',false);
                $('.new-user input[name=nisUserId]').attr('disabled',false);
                $('.new-user input[name=nisGroupId]').attr('disabled',false);
                $('.new-user input[name=nisUserContents]').attr('disabled',false);
                // $('.new-user input[name=adGroupId]').attr('disabled',false);
                // $('.new-user input[name=adUserContents]').attr('disabled',false);
                $('.new-user textarea[name=descri]').attr('disabled',false);

                $('.new-user input[name=password]').removeClass('dis');
                $('.new-user input[name=password1]').removeClass('dis');
                $('.new-user input[name=nisUserId]').removeClass('dis');
                $('.new-user input[name=nisGroupId]').removeClass('dis');
                $('.new-user input[name=nisUserContents]').removeClass('dis');
                // $('.new-user input[name=adGroupId]').removeClass('dis');
                // $('.new-user input[name=adUserContents]').removeClass('dis');
                $('.new-user textarea[name=descri]').removeClass('dis');
            },
        }
        active.init();
        //排序
        table.on('sort(demo)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            console.log(obj.field); //当前排序的字段名
            console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            console.log(this); //当前排序的 th 对象

            active.tableList(0,where,obj.type,obj.field);
            active.getTableListPage2(where,obj.type,obj.field);
            //尽管我们的 table 自带排序功能，但并没有请求服务端。
            //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
            // table.reload('testTable', { //testTable是表格容器id
            //     initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
            //     ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
            //         field: obj.field //排序字段
            //         ,order: obj.type //排序方式
            //     }
            // });
        });
        //自定义验证规则
        form.verify({
            // title: function(value){
            //     if(value.length < 5){
            //         return '标题至少得5个字符啊';
            //     }
            // }
            pass: [/(.+){6,12}$/, '密码必须6到12位']
            ,pass1: [/(.+){6,12}$/, '密码必须6到12位']
            ,pass2: [/(.+){6,12}$/, '密码必须6到12位']
            ,pass3: [/(.+){6,12}$/, '密码必须6到12位']
            ,pass4: [/(.+){6,12}$/, '密码必须6到12位']
            ,pass8: [/(.+){6,12}$/, '密码必须6到12位']
            ,pass: function(value){
                console.log(value)
                console.log($('input[name=twicePassword]').val())
                if(value != $('input[name=twicePassword]').val()){
                    return '两次密码不一致'
                }
            }
            ,pass9: function(value){
                console.log(value)
                console.log($('input[name=AtwicePassword]').val())
                if(value != $('input[name=AtwicePassword]').val()){
                    return '两次密码不一致'
                }
            }
            ,pass3:function (value) {
                if(value != $('input[name=password1]').val()){
                    return '两次密码不一致'
                }
            }
            ,ADpass:function (value) {
                if(!new RegExp("(?!^\\\\d+$)(?!^[a-zA-Z]+$)(?!^[_#@]+$).{7,20}").test(value)){
                    return '密码是数字，字母，符号组成';
                }
            }
            ,ip: function(value, item){ //value：表单的值、item：表单的DOM对象
                if(value){
                    if(!new RegExp("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$").test(value)){
                        return '请输入正确格式的ip';
                    }
                }

            }
        });

        table.on('checkbox(reductionUser)', function(obj){
            if(obj.type=="all"){
                if(obj.checked){
                    for(let i = 0; i < systemUser.length; i++){
                        systemUserIds.push(systemUser[i].id)
                    }
                }
                else{
                    systemUserIds = [];
                }
                // console.log(11111111)
                // console.log(obj.checked); //当前是否选中状态
                console.log(systemUserIds); //选中行的相关数据
            }
            else if(obj.type=="one"){
                if(obj.checked){
                    systemUserIds.push(obj.data.id)
                }
                else{
                    for(let i=0;i<systemUserIds.length;i++){
                        if(obj.data.id==systemUserIds[i]){
                            systemUserIds.splice(i,1)
                        }
                    }
                }
                // console.log(22222222)
                // console.log(obj.checked); //当前是否选中状态
                console.log(systemUserIds); //选中行的相关数据
            }
            // console.log(obj.data); //选中行的相关数据
            // console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one
        });
        //监听提交
        //确定密码
        form.on('submit(confirmPass)', function(data){
            layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            $.ajax({
                url:baseURL_SON + "/confirmPassword",
                type:"post",
                async:true,
                data:data.field,
                success:function(data){
                    console.log(JSON.stringify(data));
                    if(data.data==1){
                        layer.closeAll();//关闭所有层
                        $('input[name=newPassword]').val('');
                        $('input[name=twicePassword]').val('');
                        active.tableList2();
                        active.getTableListPage2();
                        layer.msg(data.message, {
                            icon:1,
                            closeBtn:1
                        });
                    }else{
                        layer.closeAll('loading');
                        layer.msg(data.message, {
                            icon:2,
                            closeBtn:1
                        });
                    }

                },
                error:function(data){
                    layer.closeAll('loading');
                    // window.location.href = "../../error/error.html";
                    console.log(data.message)
                }
            });
            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        form.on('submit(confirmPass1)', function(data){
            layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            console.log(data.field)
            data.field.newPassword=data.field.AnewPassword
            data.field.twicePassword=data.field.AtwicePassword
            $.ajax({
                url:baseURL_SON + "/confirmPassword",
                type:"post",
                async:true,
                data:data.field,
                success:function(data){
                    console.log(JSON.stringify(data));
                    if(data.data==1){
                        layer.closeAll();//关闭所有层
                        $('input[name=AnewPassword]').val('');
                        $('input[name=AtwicePassword]').val('');
                        active.tableList2();
                        active.getTableListPage2();
                        layer.msg(data.message, {
                            icon:1,
                            closeBtn:1
                        });
                    }else{
                        layer.closeAll('loading');
                        layer.msg(data.message, {
                            icon:2,
                            closeBtn:1
                        });
                    }

                },
                error:function(data){
                    layer.closeAll('loading');
                    // window.location.href = "../../error/error.html";
                    console.log(data.message)
                }
            });
            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        //新建域
        form.on('submit(newDomain)', function(data){
            active.addDomain(data);
            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        //编辑域
        form.on('submit(editDomain)', function(data){
            // console.log(JSON.stringify(data.field))
            active.updateDomain(data);
            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        //新建系统用户
        form.on('submit(newUser)', function(data){
            // console.log(JSON.stringify(data.field));
            // newUserMessage = data;
            // active.verifySystemUserSole($.trim($('.new-user input[name=systemUserId]').val()),active.addSystemUser);
            // layer.msg(JSON.stringify(data.field));
            data.field.pageFlag = pageFlag;
            active.addSystemUser(data);
            return false;
        });
        //新建域弹框
        $(".new-build-field").click(function(){
            //示范一个公告层
            layer.load(0, {shade: [0.1]}); //上传loading
            active.getDomainTypeList()
        });
        //编辑域弹框
        $(".edit-build-field").click(function(){
            var checkStatus = table.checkStatus('fieldTable');
            if(checkStatus.data.length==1){
                // console.log(checkStatus.data[0].id)
                layer.load(0, {shade: [0.1]}); //上传loading
                domainID = checkStatus.data[0].id;
                active.getDomainTypeList(active.findDomainById)

            }
            else if(checkStatus.data.length>1){
                layer.msg('请选择一个域',{icon:2});
            }
            else{
                layer.msg('请选择域',{icon:2});
            }
            //示范一个公告层
            // active.editBuildField()
        });
        //新增用户
        $(".new-build-user").click(function(){
            layer.load(0, {shade: [0.1]}); //上传loading
            active.clearDis()
            //示范一个公告层
            active.newBuildUser()
        });
        //删除域
        $(".del-class").on("click",function(){
            var checkStatus = table.checkStatus('fieldTable');
            let checkArray = []
            for(let i=0;i<checkStatus.data.length;i++){
                checkArray.push(checkStatus.data[i].id);
            }
            console.log(checkArray)
            if(checkArray.length>0){
                layer.confirm('您确定要删除吗？', {
                    btn: ['确定','取消'],//按钮
                    //
                }, function(){
                    layer.load(0, {shade: [0.1]}); //上传loading
                    active.delField(checkArray.toString());
                }, function(){
                    layer.msg('您取消了删除', {
                        icon:2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn:1
                    });
                });
            }
            else{
                layer.msg('请选择域',{icon:2,closeBtn:1});
            }




            // console.log(checkStatus.data[0].id)
            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         active.delField(checkStatus.data[0].id)
            //
            //     }, function(){
            //         layer.msg('您取消了删除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            //     // console.log(checkStatus.data[0].id)
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个域',{icon:2});
            // }
            // else{
            //     layer.msg('请选择域',{icon:2});
            // }
        });
        //删除用户
        $(".del-user").on("click",function(){
            var checkStatus = table.checkStatus('sysUser');
            let checkArray = []
            for(let i=0;i<checkStatus.data.length;i++){
                checkArray.push(checkStatus.data[i].id);
            }
            console.log(checkArray);
            if(checkArray.length>0){
                layer.confirm('您确定要删除吗？', {
                    btn: ['确定','取消'],//按钮
                    //
                }, function(){
                    layer.confirm('执行此操作将删除该用户下的所有数据！', {
                        icon:0,
                        btn: ['确定','取消'],//按钮
                        //
                    },function(){
                        layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                        active.delFieldUser(checkArray.toString())
                    },function(){
                        layer.msg('您取消了删除', {
                            icon:2,
                            // time: 2000, //20s后自动关闭
                            // btn: ['确定'],
                            closeBtn:1
                        });
                    });

                }, function(){
                    layer.msg('您取消了删除', {
                        icon:2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn:1
                    });
                });
            }
            else{
                layer.msg('请选择用户',{icon:2,closeBtn:1});
            }
            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         layer.confirm('执行此操作将删除该用户下的所有数据！', {
            //             icon:0,
            //             btn: ['确定','取消'],//按钮
            //             //
            //         },function(){
            //             layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            //             active.delFieldUser(checkStatus.data[0].id)
            //         },function(){
            //             layer.msg('您取消了删除', {
            //                 icon:2,
            //                 // time: 2000, //20s后自动关闭
            //                 // btn: ['确定'],
            //                 closeBtn:1
            //             });
            //         });
            //
            //
            //     }, function(){
            //         layer.msg('您取消了删除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个用户',{icon:2});
            // }
            // else{
            //     layer.msg('请选择用户',{icon:2});
            // }
        });
        //移除用户
        $(".remove-user").on("click",function(){
            var checkStatus = table.checkStatus('sysUser');
            // console.log(JSON.stringify(checkStatus.data));
            let checkArray = []
            for(let i=0;i<checkStatus.data.length;i++){
                checkArray.push(checkStatus.data[i].id);
            }
            console.log(checkArray);
            if(checkArray.length>0){
                layer.confirm('您确定要移除吗？', {
                    btn: ['确定','取消'],//按钮
                    //
                }, function(){
                    layer.load(0, {shade: [0.1]}); //上传loading
                    active.removeFieldUser(checkArray.toString())
                }, function(){
                    layer.msg('您取消了移除', {
                        icon:2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn:1
                    });
                });
            }
            else{
                layer.msg('请选择用户',{icon:2,closeBtn:1});
            }

            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要移除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         active.removeFieldUser(checkStatus.data[0].id)
            //     }, function(){
            //         layer.msg('您取消了移除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            //     // console.log(checkStatus.data[0].id)
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个用户',{icon:2});
            // }
            // else{
            //     layer.msg('请选择用户',{icon:2});
            // }
        });
        //还原用户弹框
        $('.reductionUser').on('click',function () {
            layer.load(0, {shade: [0.1]}); //上传loading
            active.tableList3()
            // active.reductionUser()
           // console.log("你自己心里没点B数吗？")
        });
        //还原用户提交
        $('.subReductionUser').on('click',function () {
            layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            active.subReductionUser(systemUserIds.toString())
            // console.log(741741)
        });
        //下载导入模板
        $(".downloadTemplate").on('click',function(){
            window.open(baseURL_SON + '/downloadSystemUserTemplate');
        });
        //同步系统用户
        $(".synchronousSystemUser").click(function(){
            //示范一个公告层
            layer.confirm('您确定同步系统用户？', {
                btn: ['确定','取消'],//按钮
                //
            }, function(){
                // layer.closeAll();
                layer.load(0, {shade: [0.1]}); //上传loading
                active.synchronousSystemUser()
            }, function(){
                layer.msg('您取消了同步系统用户', {
                    icon:2,
                    // time: 2000, //20s后自动关闭
                    // btn: ['确定'],
                    closeBtn:1
                });
            });

        });
        //导入
        upload.render({
            elem:".ImportFieldUser",
            url:baseURL_SON + '/importSystemUser',
            method:'post',
            accept:'file',
            exts:'xls|xlsx',
            before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                // console.log(JSON.stringify(obj));
                layer.load(0, {shade: [0.1]}); //上传loading
            },
            done: function(res){
                if(res.code==200 && res.data==1 && res.message != ""){
                    active.tableList2();
                    active.getTableListPage2();
                    layer.closeAll('loading');
                    layer.msg(res.message,{icon:1,closeBtn:1});
                }
                // else if(res.code==200 && res.data==1 && res.message != ""){
                //     layer.closeAll('loading');
                //     layer.msg(res.message,{icon:0,closeBtn:1});
                // }
                else{
                    layer.closeAll('loading');
                    layer.msg('上传失败',{icon:2,closeBtn:1});
                }
                console.log(res)
                // alert("上传成功")
                //上传完毕回调
            },
            error: function(){
                layer.closeAll('loading');
                // window.location.href = "../../error/error.html";
                alert("上传失败")
                //请求异常回调
            }
        });
        //导出
        $(".exportSystemUserData").on('click',function(){
            window.open(baseURL_SON + '/exportSystemUserData');
        });
        //验证用户唯一性
        $('.new-user input[name=systemUserId]').on('blur',function () {
            if($.trim($('.new-user input[name=systemUserId]').val())){
                active.verifySystemUserSole($.trim($('.new-user input[name=systemUserId]').val()))
            }

        });
        //监听工具条
        table.on('tool(demo)', function(obj){
            var data = obj.data;
            // console.log(data);
            if(obj.event==='editBuildField'){
                // console.log(data.id);
                layer.load(0, {shade: [0.1]}); //上传loading
                domainID = data.id;
                active.getDomainTypeList(active.findDomainById)
            }
            else if(obj.event === 'changePassword'){
                //示范一个公告层
                active.changePassword(data.id)
            }
            else if(obj.event === 'confirmPass'){
                //示范一个公告层
                active.confirmPass(data.id)
            }
        });
        //搜索
        $("#btn-footer2").on("click", function() {
            // console.log(this);
            if(!$("#index-footer2").val()){
                // console.log(11111)
                // // layer.tips('不能为空！！！', $("#index-footer2"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else{
                where = $("#index-footer2").val()
                active.tableList2(0,where);
                active.getTableListPage2(where);
                // active.tableList2(0,$("#index-footer2").val());
                // active.getTableListPage2($("#index-footer2").val());
            }

        });
        $("#index-footer2").on("keypress", function() {
            // console.log(this);
            if(event.keyCode == "13") {
                if (!$("#index-footer2").val()) {
                    // console.log(11111)
                    // // layer.tips('不能为空！！！', $("#index-footer2"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    where = $("#index-footer2").val()
                    active.tableList2(0, where);
                    active.getTableListPage2(where);
                    // active.tableList2(0,$("#index-footer2").val());
                    // active.getTableListPage2($("#index-footer2").val());
                }
            }
        });
        $("#btn-footer3").on("click", function() {
            // console.log(this);
            if(!$("#index-footer3").val()){
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer3"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else{
                active.tableList3($("#index-footer3").val());
            }

        });
        //键盘抬起重新加载
        $("#index-footer2").on("keyup",function(){
            if(!$("#index-footer2").val()){
                where = "";
                active.tableList2();
                active.getTableListPage2();
            }
        });
        $("#icon-footer2").on("click",function(){
            $(this).prev().val('');
            where = "";
            active.tableList2();
            active.getTableListPage2();

        });
        $("#index-footer3").on("keyup",function(){
            if(!$("#index-footer3").val()){
                active.tableList3();
            }
        });
        $("#icon-footer3").on("click",function(){
            $(this).prev().val('');
            active.tableList3();

        });
        //监听用户输入框事件
        $('.new-user input[name=systemUserId]').on('input',function(){
            if($.trim($('.new-user input[name=systemUserId]').val())){
                active.operation()
                // $('.new-user input[name=password]').attr('disabled',false);
            }
            else{
                active.clearDis()
                // $('.new-user input[name=password]').attr('disabled',true);
            }
        });
        //重置按钮
        $('.Reset').on('click',function () {
            active.clearDis();
        })

    });

})()