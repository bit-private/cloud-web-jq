//用户管理
vm = new Vue({
    el: '#app',
    data: function () {
        return {
            dialogTitle: '新建用户',
            dialogVisible: false,
            options: [],
            userList: [],
            userName: "",//用户名
            name: "",//姓名
            sendName: '',//
            email: '',//邮箱
            companyId: '',//单位ID
            companyName: '',//单位名字
            power:['普通用户'],//职权
            systemUser: [],//系统用户
            systemUserLabel: [],//系统用户
            systemUserLabelSrting: '',
            AD: '',//AD域
            position: "",//职位
            newpositionvalue: '',
            newposition: '',
            IP: '',//IP地址
            mac: '',//mac地址
            tel: '',//电话
            mobile: '',//手机
            sex: '',//性别
            checkedDomain: [],//同步域
            roleList: [],//职权列表
            professionalTitleList: [],//职位列表
            systemName: [],//系统用户列表
            emailAddress: '',//邮箱后缀地址
            domainList: [],//同步域列表
            treeData: [],
            defaultProps: {
                children: 'children',
                label: 'name'
            },
            isShow: false,
            otherSelectNodes: [],
            visiblePopover: false,
            loading: false,
            IsAc: true,
            editUserID: '',
            disabled:false,
            powertext:'' ,
            roleList:[],
        }
    },
    methods: {
        closeDialog() {
            this.userName = ""//用户名
            this.name = ""//姓名
            this.sendName = ""//姓名
            this.email = ''//邮箱
            this.companyId = ''//单位ID
            this.companyName = '' //单位名字
            this.power = ['普通用户']//职权
            this.powertext=''
            this.systemUser = [] //系统用户
            this.systemUserLabel = [] //系统用户
            this.systemUserLabelSrting = ''
            this.AD = '' //AD域
            this.position = "" //职位
            this.newpositionvalue = '',
                this.newposition = '',
                this.IP = ''//IP地址
            this.mac = ''//mac地址
            this.tel = ''//电话
            this.mobile = ''//手机
            this.sex = ''//性别
            this.checkedDomain = []//同步域
            this.userList = []
            this.options = []
        },
        selcetChange() {
            this.systemUserLabel = []
            for (let i = 0; i < this.systemName.length; i++) {
                for (let j = 0; j < this.systemUser.length; j++) {
                    if (this.systemName[i].id == this.systemUser[j]) {
                        this.systemUserLabel.push(this.systemName[i].systemUser)
                        this.systemUserLabelSrting = this.systemUserLabel.join('，')
                    }
                }
            }
        },
        submitAdd() {
            if(this.IP!='' && this.IP!=null){
                var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
                if(!reg.test(this.IP)){
                    layer.msg("请输入正确的IP地址", {
                        icon: 2,
                        closeBtn:1
                    });
                    return
                }
            }
            if(this.mac!='' && this.mac!=null){
                if(!new RegExp("[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}").test(this.mac)&&!new RegExp("[a-fA-F0-9]{2}-[a-fA-F0-9]{2}-[a-fA-F0-9]{2}-[a-fA-F0-9]{2}-[a-fA-F0-9]{2}-[a-fA-F0-9]{2}").test(this.mac)&&!new RegExp("[a-fA-F0-9]{2}[a-fA-F0-9]{2}[a-fA-F0-9]{2}[a-fA-F0-9]{2}[a-fA-F0-9]{2}[a-fA-F0-9]{2}").test(this.mac)){
                            layer.msg("请输入正确的MAC地址", {
                                icon: 2,
                                closeBtn:1
                            });
                            return
                }
            }
            if(this.tel){
                if(!new RegExp("^[0-9]{11}$").test(this.tel)){
                    layer.msg("请输入正确的电话号码", {
                        icon: 2,
                        closeBtn:1
                    });
                    return
                }
            }
            if(this.name!=''){
                if(!new RegExp("^[\u4e00-\u9fa5]+$").test(this.name)){
                    layer.msg("姓名为汉字", {
                        icon: 2,
                        closeBtn:1
                    });
                    return
                }
            }
            if(this.name=='' || this.userName=='' || this.email==''
                || this.power.length==0 || this.company=='' || this.companyName==''){
                layer.msg("必填项不能为空", {
                    icon: 2,
                    closeBtn:1
                });
                return
            }
            let urlLast = ""
            let _that = this
            let param = {}
            if (this.dialogTitle == '新建用户') {
                _that.disabled=false
                
                urlLast = ":9000/user/newAddUser"
                for (var i = 0; i < _that.professionalTitleList.length; i++) {
                    console.log(_that.professionalTitleList,_that.position,'_that.position')
                    if (_that.professionalTitleList[i].name == _that.position) {
                        _that.newpositionvalue = _that.professionalTitleList[i].value
                    }
                }


                var roleid=[]
                for (let i = 0; i < _that.roleList.length; i++) {
                    // roleArr.push(data.data.roleList[i].id)
                    if(_that.power.join()==_that.roleList[i].roleName){
                        roleid.push(_that.roleList[i].id)
                    }

                }
               
                param = {
                    userName: _that.userName,
                    realName: _that.name,
                    email: _that.email,
                    orgId: _that.companyId,
                    roleIds:roleid.join(),
                    sysIds: _that.systemUser.join(','),
                    professionalTitle: _that.newpositionvalue,
                    ipAddr: _that.IP,
                    macAddr: _that.mac,
                    sex: _that.sex,
                    telephone: _that.tel,
                    phone: _that.mobile,
                    domainIds: _that.checkedDomain.join(','),
                    adName: _that.AD
                }
            } else {
                _that.disabled=true
                urlLast = ":9000/user/updateUser"
                // console.log(_that.position, '_that.position修改')
                for (var i = 0; i < _that.professionalTitleList.length; i++) {
                    console.log(_that.professionalTitleList[i],_that.position,'_that.position')
                    if (_that.professionalTitleList[i].name == _that.position) {
                        // console.log(_that.professionalTitleList[i].id,_that.position,'_that.position')
                        _that.newpositionvalue = _that.professionalTitleList[i].value
                    }
                }
                // console.log(_that.newpositionvalue, '_that.newpositionvalue')
                param = {
                    id: this.editUserID,
                    userName: _that.userName,
                    realName: _that.name,
                    email: _that.email,
                    orgId: _that.companyId,
                    editroleIds: _that.power.join(','),
                    editsysIds: _that.systemUser.join(','),
                    roleIds: _that.power.join(','),
                    sysIds: _that.systemUser.join(','),

                    professionalTitle: _that.newpositionvalue,

                    ipAddr: _that.IP,
                    macAddr: _that.mac,
                    sex: _that.sex,
                    telephone: _that.tel,
                    phone: _that.mobile,
                    domainIds: _that.checkedDomain.join(','),
                    adName: _that.AD
                }
                // console.log(param, 'param')
            }
            layer.load(0, { shade: [0.1] });
            $.ajax({
                url: url + urlLast,
                type: "post",
                async: true,
                data: param,
                success: function (data) {
                    console.log(data,'data')
                    if (data.code == 200) {
                        layer.closeAll();
                        _that.dialogVisible = false
                        active.getTreeList();
                        active.tableList();
                        active.getPage();
                        active.afternewAddUser(data.data.user);
                        layer.msg(data.message, {
                            icon: 1,
                            closeBtn: 1
                        });
                    } else {
                        layer.msg(data.message, {
                            icon: 2,
                            closeBtn: 1
                        });
                    }

                },
                error: function (data) {
                    // window.location.href = "../../error/error.html";
                    // console.log(data.message)
                }
            });
        },
        showTree() {
            this.isShow = !this.isShow
        },
        handleCheckChange(data, checked, indeterminate) {
            console.log(data, checked, indeterminate)
            // console.log(this.$refs.otherTree.getCurrentNode().children)
            // if(!this.$refs.otherTree.getCurrentNode().children){
            //     this.companyId=this.$refs.otherTree.getCurrentNode().id
            //     this.companyName=this.$refs.otherTree.getCurrentNode().name
            //     this.visiblePopover=false
            // }
            this.companyId = data.id
            this.companyName = data.name
            this.visiblePopover = false
        },
        changeOUserName() {
            console.log(111)
            this.email = this.userName + this.emailAddress
        },
        getEdit(id) {
            active.getRanks()
            let _that = this
            $.ajax({
                url: url + ":9000/user/echoUser",
                type: "post",
                async: true,
                data: { userId: id },
                success: function (data) {
                    if (data.code == 200) {
                        _that.dialogTitle = '编辑用户'
                        _that.dialogVisible = true
                        console.log(data,'data')
                        _that.disabled=true
                        _that.userName = data.data.user.userName
                        _that.name = data.data.user.realName
                        _that.sendName = data.data.user.realName
                        _that.email = data.data.user.email
                        _that.companyName = data.data.user.orgName
                        _that.otherSelectNodes = [data.data.user.orgId]
                        _that.companyId = data.data.user.orgId
                        _that.AD = data.data.user.adName
                        let roleArr = []
                        let textArr=[]
                        for (let i = 0; i < data.data.roleList.length; i++) {
                            roleArr.push(data.data.roleList[i].id)
                            textArr.push(data.data.roleList[i].roleName)
                        }
                        _that.roleList=data.data.roleList
                        _that.power = roleArr
                        _that.powertext=textArr.toString()
                        let sysArr = []
                        for (let i = 0; i < data.data.sysList.length; i++) {
                            sysArr.push(data.data.sysList[i].id)
                        }
                        
                        for (var i = 0; i < _that.professionalTitleList.length; i++) {
                            console.log(_that.professionalTitleList[i].value,data.data.user.professionalTitle,'data.data.user.professionalTitle')
                            if (_that.professionalTitleList[i].value == data.data.user.professionalTitle) {
                                _that.newposition = _that.professionalTitleList[i].name
                                break
                                // _that.newpositionvalue=_that.professionalTitleList[i].value
                                console.log(_that.newposition, ' _that.newposition')
                            }
                        }
                        

                        _that.systemUser = sysArr
                        _that.position = _that.newposition  //
                        _that.IP = data.data.user.ipAddr
                        _that.mac = data.data.user.macAddr
                        _that.sex = data.data.user.sex
                        _that.tel = data.data.user.telephone
                        _that.mobile = data.data.user.phone

                        _that.systemUserLabel = []
                        for (let i = 0; i < _that.systemName.length; i++) {
                            for (let j = 0; j < _that.systemUser.length; j++) {
                                if (_that.systemName[i].id == _that.systemUser[j]) {
                                    _that.systemUserLabel.push(_that.systemName[i].systemUser)
                                    _that.systemUserLabelSrting = _that.systemUserLabel.join('，')
                                }
                            }
                        }
                        // console.log(_that.position, ' _that.position293')
                    } else {
                        layer.msg(data.message, {
                            icon: 2,
                            closeBtn: 1
                        });
                    }
                },
            })
        },
        positionChange(){
// console.log(this.position,"1111111")
        },
        getUserName(query) {
            if (query !== '') {
                let reg = /^[a-z0-9A-Z\\-_]+$/
                let param = {}
                let _that = this
                this.loading = true
                if (reg.test(query)) {
                    param.searchUserName = query
                } else {
                    param.searchRealName = query
                }
                $.ajax({
                    url: url + ":9000/user/newUserSearch",
                    type: "post",
                    async: true,
                    data: param,
                    success: function (data) {
                        if (data.code == 200) {
                            _that.options = data.data
                            _that.loading = false
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            });
                        }
                    },
                    error: function (data) {

                    }
                });
            } else {
                this.options = [];
            }
        },
        getRealName(query) {
            if (query !== '') {
                let reg = /^[a-z0-9A-Z\\-_]+$/
                let param = {}
                let _that = this
                this.loading = true
                if (reg.test(query)) {
                    param.searchUserName = query
                } else {
                    param.searchRealName = query
                }
                $.ajax({
                    url: url + ":9000/user/newUserSearch",
                    type: "post",
                    async: true,
                    data: param,
                    success: function (data) {
                        if (data.code == 200) {
                            _that.userList = data.data
                            _that.loading = false
                        } else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            });
                        }
                    },
                    error: function (data) {

                    }
                });
            } else {
                this.options = [];
            }
        },
        changeUserName() {
            for (let i = 0; i < this.options.length; i++) {
                if (this.userName == this.options[i].userName) {
                    this.name = this.options[i].realName
                    this.sendName = this.options[i].realName
                    this.AD = this.options[i].adName
                    this.email = this.options[i].email
                }
            }
        },
        changeRealName() {
            for (let i = 0; i < this.userList.length; i++) {
                if (this.sendName == this.userList[i].userName) {
                    this.userName = this.userList[i].userName
                    this.name = this.userList[i].realName
                    this.AD = this.userList[i].adName
                    this.email = this.userList[i].email
                }
            }
        },
    },
    computed: {},
    mounted() {

    },

})

//时间戳的处理
layui.laytpl.toDateString = function (d, format) {
    if (d == null) {
        return ""
    }
    var date = new Date(d)

        , ymd = [
            this.digit(date.getFullYear(), 4)
            , this.digit(date.getMonth() + 1)
            , this.digit(date.getDate())
        ]
        , hms = [
            this.digit(date.getHours())
            , this.digit(date.getMinutes())
            , this.digit(date.getSeconds())
        ];

    format = format || 'yyyy-MM-dd HH:mm:ss';

    return format.replace(/yyyy/g, ymd[0])
        .replace(/MM/g, ymd[1])
        .replace(/dd/g, ymd[2])
        .replace(/HH/g, hms[0])
        .replace(/mm/g, hms[1])
        .replace(/ss/g, hms[2]);
};

//数字前置补零
layui.laytpl.digit = function (num, length, end) {
    var str = '';
    num = String(num);
    length = length || 2;
    for (var i = num.length; i < length; i++) {
        str += '0';
    }
    return num < Math.pow(10, length) ? str + (num | 0) : num;
};

(function () {
    console.log(url);
    const baseURL_SON = url + ':9000/user';
    const baseURL_SON1 = url + ':9000/domain';
    const baseURL_SON2 = url + ':9001';
    // const baseURL_SON = 'http://10.1.2.251:9000/user';
    // const baseURL_SON1 = 'http://10.1.2.251:9000/domain';
    var orgId;
    //判断有没有根节点
    var rootNode = true;
    //判断新建部门还是编辑部门
    var judge = 1;
    //部门领导变量
    var DepartmentLeaderVariableLeft;
    var DepartmentLeaderVariableRight = [];
    var flag;
    //监听编辑职级提交
    var addRank = true;
    //监听删除职级ID
    var checkFlag = [];
    //监听新建用户多选框id
    var newUserCheckboxId = [];
    //编辑用户ID
    var editUserID = "";
    //编辑部门的ID
    var editDepID = "";
    //编辑部门PID
    var editDepPID = "";

    //判断是否验证用户的唯一性
    var isNoUser = false;
    //邮箱变量
    var Emailvariable = "";


    //记录页数
    var pageNum = 0;
    //搜索
    var where;

    // layui.config({
    //     base: '/views/admin/organization/js/' //文件所在的目录
    // }).extend({ //设定模块别名
    //     treeselect: 'treeselect'
    // });

    //JavaScript代码区域
    layui.use(['element', 'table', 'form', 'layer', 'upload', 'tree', 'jquery', 'laypage'], function () {
        var element = layui.element
            , table = layui.table
            , form = layui.form
            , layer = layui.layer
            , upload = layui.upload
            , tree = layui.tree
            , laypage = layui.laypage
            , $ = layui.jquery;

        var active = {
            //初始化
            init: function () {
                var that = this;
                this.getTreeList();//获取树状数据
                // this.getTreeList1();//获取下拉树状数据
                this.tableList();//获取表格
                this.getPage();//获取页数
            },

            //获取树状数据
            getTreeList: function () {
                $.ajax({
                    url: baseURL_SON + "/organizationTreeNumber",
                    type: "post",
                    async: true,
                    success: function (data) {
                        vm.$data.treeData = data.data
                        active.treeList(data.data);
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";

                    // }
                });
            },
            //获取部门下拉树数据
            getTreeList1: function (params) {
                $.ajax({
                    url: baseURL_SON + "/organizationTree",
                    type: "post",
                    async: true,
                    success: function (data) {
                        if (data.data.length <= 0) {
                            rootNode = false;
                        } else {
                            rootNode = true;
                        }
                        if (params) {
                            active.departmentalLleadershipAdd(params, data.data);
                            if (judge == 1) {
                                $('.new-dep .person-add').show();
                            }
                            else if (judge == 2) {
                                $('.edit-dep .person-add').show();
                            }
                        } else {
                            active.departmentTree(data.data);

                            if (judge == 1) {
                                active.getSysname(active.newBuildDepartment);
                            }
                            else if (judge == 2) {
                                active.getSysname(active.findOrganizationById);
                            }
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            getIsAc: function (params) {
                $.ajax({
                    url: url + ":9000/user/newUserModel",
                    type: "post",
                    async: true,
                    success: function (data) {
                        console.log(data.data)
                        if (data.data == 'B') {
                            vm.$data.IsAc = true
                        } else {
                            vm.$data.IsAc = false
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data)
                    // }
                });
            },
            //获取邮箱后缀
            getEamilSuffix: function () {
                $.ajax({
                    url: baseURL_SON + "/queryEmail",
                    type: "post",
                    async: true,
                    success: function (data) {
                        console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            Emailvariable = data.data.access;
                            vm.$data.emailAddress = data.data.access;
                            if (judge == 3) {
                                // active.newBuildUser()
                            }
                            else if (judge == 4) {
                                // active.editBuildUser(editUserID);
                                vm.getEdit(editUserID)
                                console.log(vm)
                            }
                        }
                        else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";

                    // }
                })

            },
            //树状图
            treeList: function (params) {
                $('#demo').empty();
                tree({
                    elem: '#demo' //传入元素选择器
                    , nodes: params
                    , click: function (node) {
                        editDepID = node.id;
                        editDepPID = node.pid;
                        // if(node.flag==3){
                        active.tableList(0, node.id);
                        active.getPage(node.id);
                        orgId = node.id
                        console.log(node) //node即为当前点击的节点数据
                    }
                });
            },
            //表格
            tableList: function (pageNum, orgId, where, sort, column) {
                $.ajax({
                    url: baseURL_SON + "/pagingQueryUser",
                    type: "post",
                    async: true,
                    data: { page: pageNum == undefined ? 0 : pageNum, size: 10, orgId, search: where == undefined ? '' : where, sort: sort == undefined ? '' : sort, column: column == undefined ? '' : column },
                    success: function (data) {
                        table.render({
                            elem: '#userTable'
                            , cols:
                                [[
                                    { type: 'checkbox' }
                                    , { field: 'userName', title: '用户名', sort: true, style: 'cursor: pointer;', event: 'demo', templet: '#mouseHov' }
                                    , { field: 'realName', title: '姓名', sort: true, }
                                    , { field: 'email', title: '邮件' }
                                    , { field: 'roleName', title: '职权', sort: true } //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                    // ,{field:'lastLoginTime', title: '最后登录时间',sort:true,templet: '<div>{{ layui.laytpl.toDateString(d.lastLoginTime) }}</div>'}
                                    , { field: 'lastLoginTime', title: '最后登录时间', sort: true, }
                                    , { field: 'sysName', title: '系统用户' }
                                    , { field: 'ipAddr', title: 'IP地址' }
                                    , { field: 'macAddr', title: 'MAC地址' }
                                ]]
                            , data: data.data.content
                        });

                        // console.log(data.data);
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                })
            },
            //新建用户弹框
            newBuildUser: function (params) {
                
                layer.closeAll('loading'); //关闭加载层
                formSelects.selects({
                    name: 'select1',
                    el: 'select[name=roleIds]',
                    show: '#select-result',
                    model: 'select',
                    filter: 'roleIds',
                    reset: true
                });
                formSelects.selects({
                    name: 'select2',
                    el: 'select[name=sysIds]',
                    show: '#select-result2',
                    model: 'select',
                    filter: 'sysIds',
                    reset: true
                });
                // formSelects.selects({
                //     name: 'select3',
                //     el: 'select[name=professionalTitle]',
                //     show: '#select-result3',
                //     model: 'select',
                //     filter: 'professionalTitle',
                //     reset: true
                // });
                layer.open({
                    type: 1
                    , title: ["新建用户", 'font-size:20px;font-weight:900'] //不显示标题栏
                    , closeBtn: 1
                    , area: ['800px', '650px']
                    , shade: 0.8
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.new-user')
                    , success: function (layero) {
                        isNoUser = false;
                    }
                    , end: function (index, layero) {
                        $(".new-user input[name=loginPageAccess]").attr('checked', false);
                        // form.render();
                        $(".new-user .layui-form-checkbox").removeClass('layui-form-checked');
                        active.clearNewUser();
                        $(".new-user").hide();
                        newUserCheckboxId = []
                    }
                });
            },
            //验证用户名唯一性
            verifyUserNameSole: function (userName) {
                $.ajax({
                    url: baseURL_SON + "/verifyUserNameSole",
                    type: "post",
                    async: true,
                    data: { userName },
                    success: function (data) {
                        if (data.data == 1 && data.code == 200) {
                            isNoUser = true;
                        } else if (data.code == 400) {
                            isNoUser = true;
                            // console.log(1)
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            });
                        } else {
                            isNoUser = false;
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    // }
                });
            },
            //新建用户提交
            newAddUser: function (data) {
                var params = data
                layer.load(0, { shade: [0.1] });
                $.ajax({
                    url: baseURL_SON + "/newAddUser",
                    type: "post",
                    async: true,
                    data,
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.code == 200 && data.data.success == 1) {
                            layer.closeAll();
                            active.getTreeList();
                            active.tableList();
                            active.getPage();
                            active.afternewAddUser(data.data.user);
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn: 1
                            });
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }

                    },
                    error: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        // window.location.href = "../../error/error.html";
                        // console.log(data.message)
                    }
                });
            },
            //新建用户提交
            afternewAddUser: function (data) {
                $.ajax({
                    url: baseURL_SON2 + "/examineAndApproveResource/newAddUser",
                    type: "post",
                    async: true,
                    data,
                    success: function (data) {

                    },
                    error: function (data) {
                        // layer.closeAll('loading'); //关闭加载层
                        // window.location.href = "../../error/error.html";
                        // console.log(data.message)
                    }
                });
            },
            //编辑用户弹框
            editBuildUser: function (userId) {
                layer.closeAll('loading'); //关闭加载层
                $.ajax({
                    url: baseURL_SON + "/echoUser",
                    // url:"http://10.1.2.251:9000/user/echoUser",
                    type: "post",
                    async: true,
                    data: { userId },
                    success: function (data) {
                        if (data.code == 200) {
                            $(".edit-user input[name=id]").val(data.data.user.id);
                            $(".edit-user input[name=userName]").val(data.data.user.userName);
                            $(".edit-user input[name=realName]").val(data.data.user.realName);
                            // $(".edit-user input[name=email]").val(data.data.user.userName+Emailvariable);
                            $(".edit-user input[name=email]").val(data.data.user.email);
                            $('#classtree3').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html(data.data.user.orgName).end().find("input:hidden[name=orgId]").val(data.data.user.orgId);
                            if (data.data.user.loginPageAccess == 1 || data.data.user.loginPageAccess == 3) {
                                // $(".edit-user input[name=loginPageAccess]").val('1');
                                // $(".edit-user input[name=loginPageAccess]").addClass('checked',true);
                                // console.log(1111111111111)
                                $(".edit-user input[name=loginPageAccess]").attr('checked', true);
                                form.render();
                                $(".edit-user .layui-form-checkbox").addClass('layui-form-checked');
                            }
                            // console.log(JSON.stringify(data.data))
                            var ARRAY1 = [];
                            for (let i = 0; i < data.data.roleList.length; i++) {
                                ARRAY1.push(data.data.roleList[i].id)
                            }
                            var ARRAY2 = [];
                            for (let i = 0; i < data.data.sysList.length; i++) {
                                ARRAY2.push(data.data.sysList[i].id)
                            }
                            var ARRAY3 = [];
                            ARRAY3.push(data.data.user.professionalTitle)
                            $(".edit-user input[name=ipAddr]").val(data.data.user.ipAddr);
                            $(".edit-user input[name=macAddr]").val(data.data.user.macAddr);
                            $(".edit-user input[name=sex]").val(data.data.user.sex);
                            $(".edit-user input[name=telephone]").val(data.data.user.telephone);
                            $(".edit-user input[name=phone]").val(data.data.user.phone);
                            // console.log(ARRAY1)
                            // console.log(ARRAY2)
                            layer.open({
                                type: 1
                                , title: ["编辑用户", 'font-size:20px;font-weight:900'] //不显示标题栏
                                , closeBtn: 1
                                , area: ['800px', '600px']
                                , shade: 0.8
                                , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                                // ,resize: false
                                // ,btn: ['确定']
                                , btnAlign: 'c'
                                , moveType: 1 //拖拽模式，0或者1
                                , content: $('.edit-user')
                                , success: function (layero) {
                                    formSelects.selects({
                                        name: 'select3',
                                        el: 'select[name=editroleIds]',
                                        show: '#select-result3',
                                        model: 'select',
                                        filter: 'roleIds',
                                        init: ARRAY1,
                                        reset: true,
                                    });
                                    formSelects.selects({
                                        name: 'select4',
                                        el: 'select[name=editsysIds]',
                                        show: '#select-result4',
                                        model: 'select',
                                        filter: 'sysIds',
                                        init: ARRAY2,
                                        reset: true
                                    });
                                    formSelects.selects({
                                        name: 'select5',
                                        el: 'select[name=editprofessionalTitle]',
                                        show: '#select-result5',
                                        model: 'select',
                                        filter: 'professionalTitle',
                                        init: ARRAY3,
                                        reset: true
                                    });
                                }
                                , end: function (index, layero) {
                                    // console.log(22222222222)
                                    $(".edit-user input[name=loginPageAccess]").attr('checked', false);
                                    // form.render();
                                    $(".edit-user .layui-form-checkbox").removeClass('layui-form-checked');
                                    $(".edit-user").hide()
                                }
                            });

                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                })

            },
            //编辑用户提交
            editAddUser: function (data) {
                console.log('1123', data);
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                $.ajax({
                    url: baseURL_SON + "/updateUser",
                    type: "post",
                    async: true,
                    data,
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.data == 1 && data.code == 200) {
                            layer.closeAll();
                            active.getTreeList();
                            active.tableList();
                            // active.tableList(pageNum);
                            active.getPage();
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }

                    },
                    error: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        // window.location.href = "../../error/error.html";
                        console.log(data.message)
                    }
                });
            },
            //新建部门弹框
            newBuildDepartment: function (params) {
                layer.closeAll('loading'); //关闭加载层
                layer.open({
                    type: 1
                    , title: ["新建部门", 'font-size:20px;font-weight:900'] //不显示标题栏
                    , closeBtn: 1
                    , shade: 0.8
                    , area: ['800px', '600px']
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.new-dep')
                    , success: function (layero) {
                        // console.log(rootNode)
                        if (!rootNode) {
                            // console.log(22222222222)
                            $('.new-dep .selecttree').parent().hide();
                            $('.new-dep .systemUser').parent().hide();
                            $('.new-dep .CompanyEmail').show();
                            $('.new-dep input[name=companyEmail]').attr('lay-verify', 'required');
                        }
                        else {
                            // console.log(11111111111111)
                            $('.new-dep .selecttree').parent().show();
                            $('.new-dep .systemUser').parent().show();
                            $('.new-dep .CompanyEmail').hide();
                            $('.new-dep input[name=companyEmail]').removeAttr('lay-verify');
                        }
                    }
                    , end: function (index, layero) {
                        active.clearNewDep()
                        $(".new-dep").hide()
                    }
                });
            },
            //新建部门提交
            newAddOrganization: function (data) {
                console.log(JSON.stringify(data));
                $.ajax({
                    url: baseURL_SON + "/newAddOrganization",
                    type: "post",
                    async: true,
                    data,
                    success: function (data) {
                        // that.treeList(data.data);
                        // console.log(JSON.stringify(data));
                        if (data.data == 1 && data.code == 200) {
                            $('.new-dep input[name=name]').val('');
                            // $('.new-dep select[name=rank]').val('');
                            $('.new-dep input[name=leader]').val('');
                            $('.new-dep input[name=leaderUser]').val('');
                            $('.new-dep input[name=parentId]').val('');
                            $('.new-dep input[name=dutyUser]').val('');
                            $('.new-dep input[name=duty]').val('');
                            // $('.new-dep select[name=sysId]').val('');
                            $('.new-dep input[name=companyEmail]').val('');
                            active.getTreeList();
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }

                    },
                    error: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        // window.location.href = "../../error/error.html";
                        console.log(data.message)
                    }
                });
            },
            //编辑部门弹框
            editBuildDepartment: function (params) {
                layer.closeAll('loading'); //关闭加载层
                layer.open({
                    type: 1
                    , title: ["编辑部门", 'font-size:20px;font-weight:900'] //不显示标题栏
                    , closeBtn: 1
                    , shade: 0.8
                    , area: ['800px', '600px']
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.edit-dep')
                    , success: function (layero) {
                        if (editDepPID == 0) {
                            $('.edit-dep .selecttree').parent().hide();
                            $('.edit-dep .systemUser').parent().hide();
                            $('.edit-dep .CompanyEmail').show();
                            $('.edit-dep input[name=companyEmail]').attr('lay-verify', 'required');
                        }
                        else {
                            $('.edit-dep .selecttree').parent().show();
                            $('.edit-dep .systemUser').parent().show();
                            $('.edit-dep .CompanyEmail').hide();
                            $('.edit-dep input[name=companyEmail]').removeAttr('lay-verify');
                        }
                    }
                    , end: function (index, layero) {
                        // editDepID = "";
                        $('.edit-dep .person-add').hide()
                        $(".edit-dep").hide()
                    }
                });
            },
            //编辑部门数据回显
            findOrganizationById: function () {
                // console.log(editDepID);
                $.ajax({
                    url: baseURL_SON + "/echoOrganization",
                    // url:"http://10.1.2.251:9000/user/echoOrganization",
                    type: "post",
                    async: true,
                    data: { orgId: editDepID },
                    success: function (data) {
                        console.log(data);
                        if (data.code == 200 && data.data.length > 0) {
                            $('.edit-dep input[name=id]').val(data.data[0].id);
                            $('.edit-dep input[name=name]').val(data.data[0].name);
                            $('.edit-dep select[name=rank]').val(data.data[0].rank);
                            $('.edit-dep input[name=leader]').val(data.data[0].leader);
                            $('.edit-dep input[name=leaderUser]').val(data.data[0].leaderName);
                            $('.edit-dep input[name=parentId]').val(data.data[0].parentId);
                            $('.edit-dep input[name=dutyUser]').val(data.data[0].dutyUser);
                            $('.edit-dep input[name=duty]').val(data.data[0].dutyUserName);
                            $('.edit-dep select[name=sysId]').val(data.data[0].sysId);
                            $('.edit-dep input[name=companyEmail]').val(data.data[0].companyEmail);
                            $('#classtree1').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html(data.data[0].parentName).end().find("input:hidden[name='assetPropertyRightUnit']").val(data.data[0].parentId);
                            form.render()
                            // console.log(JSON.stringify(data));
                            active.editBuildDepartment();
                        }
                        else {
                            layer.msg(data.message, {
                                icon: 2,
                                closeBtn: 1
                            })
                            layer.closeAll('loading'); //关闭加载层
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            //编辑部门提交
            editAddOrganization: function (data) {
                console.log(JSON.stringify(data));
                $.ajax({
                    url: baseURL_SON + "/updateOrganization",
                    type: "post",
                    async: true,
                    data,
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.data == 1 && data.code == 200) {
                            active.getTreeList();
                            active.tableList();//获取表格
                            active.getPage();//获取页数
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn: 1
                            });
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }

                    },
                    error: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        // window.location.href = "../../error/error.html";
                        console.log(data.message)
                    }
                });
            },
            //删除部门
            delOrganization: function (orgId) {
                $.ajax({
                    url: baseURL_SON + "/deleteOrganization",
                    type: "post",
                    async: true,
                    data: { orgId },
                    success: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        console.log(JSON.stringify(data));
                        if (data.code == 200 && data.data == 1) {
                            layer.closeAll(); //关闭所有层
                            active.getTreeList();
                            active.tableList();
                            active.getPage();
                            editDepID = "";
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn: 1
                            });
                        }
                        else {
                            layer.msg(data.message, {
                                icon: 3,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            console.log(data.message)
                        }
                    },
                    error: function (data) {
                        layer.closeAll('loading'); //关闭加载层
                        // window.location.href = "../../error/error.html";
                        console.log(data.message)
                    }
                });
            },
            //删除用户
            delUser: function (userIds) {
                $.ajax({
                    url: baseURL_SON + "/deleteUser",
                    // url:"http://10.1.2.251:9000/user/deleteUser",
                    type: "post",
                    async: true,
                    data: userIds,
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.code == 200 && data.data > 0) {
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn: 1
                            });
                            active.getTreeList();
                            active.tableList();
                            active.getPage()
                        }
                        else {
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            afterdelUser: function (userIds) {
                console.log(userIds, 111)
                $.ajax({
                    url: baseURL_SON2 + "/examineAndApproveResource/deleteActUser",
                    // url:"http://10.1.2.251:9000/user/deleteUser",
                    type: "post",
                    async: true,
                    data: { userId: userIds },
                    success: function (data) {

                    },
                    error: function (data) {
                        window.location.href = "../../error/error.html";
                        // console.log(data.message)
                    }
                });
            },
            //编辑职级弹框
            editRank: function (params) {
                layer.closeAll('loading'); //关闭加载层
                layer.open({
                    type: 1
                    , title: ["编辑职级", 'font-size:20px;font-weight:900'] //不显示标题栏
                    , closeBtn: 1
                    , area: ['350px', '430px']
                    , shade: 0.8
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    // ,shadeClose: true//点击阴影关闭遮罩
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.edit-class')
                    , success: function (layero) {

                    }
                    , end: function (index, layero) {
                        $(".edit-class").hide()
                    }
                });
            },
            //根据上下级获取职级
            findRankSize: function (size) {
                $.ajax({
                    url: baseURL_SON + "/queryTheRankLevelSize",
                    type: "post",
                    async: true,
                    data: { size },
                    success: function (data) {
                        // that.treeList(data.data);
                        // console.log(JSON.stringify(data));
                        if (data.code == 200 && data.data) {
                            $('.edit-class input[name=upDownRank]').val(data.data);
                            active.getRankList();
                            // layer.msg('删除成功', {
                            //     icon: 1,
                            //     closeBtn:1
                            // });
                            // active.tableList();
                            // active.getPage()
                        }
                        else {
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            // console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //编辑职级提交
            newAddRank: function (data) {
                $.ajax({
                    url: baseURL_SON + "/newAddRank",
                    type: "post",
                    async: true,
                    data,
                    success: function (data) {

                        console.log(JSON.stringify(data));

                        if (data.data == 1 && data.code == 200) {
                            addRank = false;
                            // active.getRankList();
                            active.findRankSize($('.edit-class input[name=size]:checked').val());
                            $('.edit-class input[name=rankName]').val('');
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }
                        else {
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //删除职级
            deleteRank: function (rankIds) {
                // console.log(rankIds)
                $.ajax({
                    url: baseURL_SON + "/deleteRank",
                    type: "post",
                    async: true,
                    data: { rankIds },
                    success: function (data) {
                        // that.treeList(data.data);
                        // console.log(JSON.stringify(data));
                        if (data.code == 200 && data.data == 1) {
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                closeBtn: 1
                            });
                            checkFlag = [];
                            active.getRankList();
                        }
                        else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            console.log(data.message)
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //更改密码弹框
            updataPass: function (data) {
                layer.closeAll('loading'); //关闭加载层
                $('.newPassword').val('');
                $('.notarizePassword').val('');
                // console.log(data.id);
                $('.updata-pass .layui-userName span').html(data.userName);
                $('.updata-pass input[name=userId]').val(data.id);
                var index = layer.open({
                    type: 1
                    , title: ["更改密码", 'font-size:20px;font-weight:900'] //不显示标题栏
                    , closeBtn: 1
                    , area: ["350px", "300px"]
                    , shade: 0.8
                    , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    // ,shadeClose: true//点击阴影关闭遮罩
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('.updata-pass')
                    , success: function (layero) {

                    }
                    , end: function (index, layero) {
                        $(".updata-pass").hide()
                    }
                });
            },
            //更改密码提交
            updatePassword: function (data) {
                console.log(JSON.stringify(data.field));
                $.ajax({
                    url: baseURL_SON + "/updatePassword",
                    type: "post",
                    async: true,
                    // data:{userId:data.id,newPassword:$('.newPassword').val(),notarizePassword:$('.notarizePassword').val()},
                    data: data.field,
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.data == 1 && data.code == 200) {
                            $('.updata-pass input[name=newPassword]').val('');
                            $('.updata-pass input[name=twicePassword]').val('');
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            //解锁功能
            unlockPadss: function (userId) {
                // console.log(userId);
                $.ajax({
                    url: baseURL_SON + "/deblockUser",
                    // url: "http://10.1.2.251:9000/user/deblockUser",
                    type: "post",
                    async: true,
                    data: { userId },
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            active.tableList()
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            active.tableList()
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                });
            },
            // 冻结功能
            FrozenMethod: function (userId) {
                $.ajax({
                    url: baseURL_SON + "/lockUser",
                    // url: "http://10.1.2.251:9000/user/deblockUser",
                    type: "post",
                    async: true,
                    data: { userId },
                    success: function (data) {
                        // that.treeList(data.data);
                        console.log(JSON.stringify(data));
                        if (data.code == 200) {
                            layer.closeAll();
                            layer.msg(data.message, {
                                icon: 1,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            active.tableList()
                        } else {
                            layer.closeAll('loading'); //关闭加载层
                            layer.msg(data.message, {
                                icon: 2,
                                // time: 2000, //20s后自动关闭
                                // btn: ['确定'],
                                closeBtn: 1
                            });
                            active.tableList()
                        }

                    }
                });
            },
            //获取角色
            getRole: function () {
                $.ajax({
                    url: baseURL_SON + "/queryRole",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(JSON.stringify(data.data));
                        // var optionString = "<option grade=\'请选择一级属性\' selected = \'selected\' class=\"layui-select-tips\" value=\"\">请选择</option>";
                        var optionString = "";
                        if (data.data) {                   //判断
                            vm.$data.roleList = data.data
                            for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].id + "\"";
                                optionString += ">" + data.data[i].roleName + "</option>";  //动态添加数据
                            }

                            $('select[name=roleIds]').empty();//清空子节点
                            $('select[name=editroleIds]').empty();//清空子节点
                            $("select[name=roleIds]").append(optionString);  // 添加子节点
                            $("select[name=editroleIds]").append(optionString);  // 添加子节点
                        }

                        // that.newBuildUser()
                        active.getSysname()
                        // form.render("select",'new_user');
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //获取系统用户
            getSysname: function (cb) {
                $.ajax({
                    url: baseURL_SON + "/querySystemUserName",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(data);
                        // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                        if (cb) {
                            // console.log(1111111111111)
                            var optionString = "<option grade=\'请选择一级属性\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) {                   //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].id + "\"";
                                    optionString += ">" + data.data[i].systemUser + "</option>";  //动态添加数据
                                }
                                $('select[name=sysId]').empty();//清空子节点
                                // $('select[name=editsysIds]').empty();//清空子节点
                                $("select[name=sysId]").append(optionString);  // 添加子节点
                                console.log(optionString)
                                // $("select[name=editsysIds]").append(optionString);  // 添加子节点
                            }
                            form.render();
                            cb()
                        }
                        else {
                            // var optionString = "<option grade=\'请选择一级属性\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            var optionString = "";
                            if (data.data) {                   //判断
                                vm.$data.systemName = data.data
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].id + "\"";
                                    optionString += ">" + data.data[i].systemUser + "</option>";  //动态添加数据
                                }
                                $('select[name=sysIds]').empty();//清空子节点
                                $('select[name=editsysIds]').empty();//清空子节点
                                $("select[name=sysIds]").append(optionString);  // 添加子节点
                                $("select[name=editsysIds]").append(optionString);  // 添加子节点
                                console.log(optionString)
                            }
                            form.render();

                            if (judge == 3) {
                                active.getDomainList(active.getEamilSuffix);
                                // active.newBuildUser();
                            }
                            else if (judge == 4)
                                active.getEamilSuffix();
                            // active.editBuildUser(editUserID)
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message);
                    // }
                });
            },
            //获取职级
            getRanks: function () {
                $.ajax({
                    url: baseURL_SON + "/professionalTitleList",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(JSON.stringify(data.data));
                        vm.$data.professionalTitleList = data.data

                        var optionString = "<option grade=\'请选择\' selected = \'selected\' class=\"layui-select-tips\" value=\"\">请选择</option>";
                        // var optionString = "";
                        if (data.data) {                   //判断
                            for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                optionString += "<option grade=\"" + data.data[i].name + "\" value=\"" + data.data[i].name + "\"";
                                optionString += ">" + data.data[i].name + "</option>";  //动态添加数据
                                // console.log(optionString, 'optionString')
                            }

                            $('select[name=professionalTitle]').empty();//清空子节点
                            $('select[name=editprofessionalTitle]').empty();//清空子节点
                            $("select[name=professionalTitle]").append(optionString);  // 添加子节点
                            $("select[name=editprofessionalTitle]").append(optionString);  // 添加子节点
                        }

                        // that.newBuildUser()
                        // active.getSysname()
                        // form.render("select",'new_user');
                    },
                    error: function (data) {
                        // window.location.href = "../../error/error.html";
                        // console.log(data.message)
                    }
                });
            },
            //获取同步域
            getDomainList: function (cb) {
                $.ajax({
                    url: baseURL_SON1 + "/queryDomain",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(JSON.stringify(data.data));
                        if (data.code == 200 && data.message == "查询成功") {
                            vm.$data.domainList = data.data
                            var synchronizationRank = "";
                            for (let i = 0; i < data.data.length; i++) {
                                synchronizationRank += '<input type="checkbox" name="domainIds" title="' + data.data[i].name + '" lay-skin="primary" value="' + data.data[i].id + '" lay-filter="rankCheck">'
                            }
                            $('.new-user .many-check').empty();
                            $('.new-user .many-check').append(synchronizationRank);
                        }
                        form.render();
                        cb()
                        // active.newBuildUser();
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                });
            },
            //获取职级列表
            getRankList: function (cb) {
                $.ajax({
                    url: baseURL_SON + "/queryRank",
                    type: "post",
                    async: true,
                    success: function (data) {
                        // console.log(JSON.stringify(data))
                        if (cb) {
                            // console.log(JSON.stringify(data));
                            var optionString = "<option grade=\'请选择一级属性\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            if (data.data) {                   //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    optionString += "<option grade=\"" + data.data[i].id + "\" value=\"" + data.data[i].typeKey + "\"";
                                    optionString += ">" + data.data[i].name + "</option>";  //动态添加数据
                                    // optionChecked+='<input type="checkbox" name="" title="'+data.data[i].name+'" lay-skin="primary"  lay-filter="check" value="'+data.data[i].typeKey+'">'
                                }
                                $('select[name=rank]').empty();//清空子节点
                                $("select[name=rank]").append(optionString);  // 添加子节点
                            }
                            form.render();
                            cb()
                        } else {
                            // console.log(JSON.stringify(data));
                            // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
                            // var optionString = "<option grade=\'请选择一级属性\'  selected = \'selected\'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>";
                            var optionChecked = "";
                            if (data.data) {
                                // console.log(JSON.stringify(data.data));
                                //判断
                                for (var i = 0; i < data.data.length; i++) { //遍历，动态赋值
                                    // optionString +="<option grade=\""+data.data[i].id+"\" value=\""+data.data[i].id+"\"";
                                    // optionString += ">"+data.data[i].name+"</option>";  //动态添加数据
                                    optionChecked += '<input type="checkbox" name="" title="' + data.data[i].name + '" lay-skin="primary"  lay-filter="check" value="' + data.data[i].typeKey + '">'
                                }
                                // $('.edit-class select[name=parentId]').empty();//清空子节点
                                // $(".edit-class select[name=parentId]").append(optionString);  // 添加子节点
                                $('.optionChecked').empty();//清空子节点
                                $(".optionChecked").append(optionChecked);  // 添加子节点
                            }
                            form.render();
                            // form.render('select','rank');
                            if (addRank)
                                active.editRank()
                        }

                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                })
            },
            //获取页数
            getPage: function (orgId, where, sort, column) {
                $.ajax({
                    url: baseURL_SON + "/pagingQueryUser",
                    type: "post",
                    async: true,
                    data: { page: 0, size: 10, orgId, search: where == undefined ? '' : where, sort: sort == undefined ? '' : sort, column: column == undefined ? '' : column },
                    success: function (data) {
                        laypage.render({
                            elem: "laypage",
                            count: data.data.pageCount
                            , layout: ['prev', 'page', 'next', 'skip']
                            , jump: function (obj, first) {
                                //首次不执行
                                if (!first) {
                                    // pageNum = obj.curr-1;
                                    active.tableList(obj.curr, orgId, where, sort, column);
                                    //do something
                                }
                            }
                        })
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     // console.log(data.message)
                    // }
                })

            },
            //获取新建部门主管单位下拉树
            departmentTree: function (param) {
                // console.log(param)
                if (judge == 1) {
                    $('#classtree').empty();//清空子节点
                    tree({
                        elem: "#classtree",
                        nodes: param,
                        click: function (node) {
                            // console.log(node);
                            var $select = $($(this)[0].elem).parents(".layui-form-select");
                            // $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='parentId']").val(node.pid);
                            $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='parentId']").val(node.id);
                            //新建部门获取用户
                        }
                    });
                }
                else if (judge == 2) {
                    $('#classtree1').empty();//清空子节点
                    tree({
                        elem: "#classtree1",
                        nodes: param,
                        click: function (node) {
                            // console.log(node);
                            var $select = $($(this)[0].elem).parents(".layui-form-select");
                            // $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='parentId']").val(node.pid);
                            $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='parentId']").val(node.id);
                            //新建部门获取用户
                        }
                    });
                }
                else if (judge == 3) {
                    //新建用户单位下拉树
                    $('#classtree2').empty();//清空子节点
                    tree({
                        elem: "#classtree2",
                        nodes: param,
                        click: function (node) {
                            console.log(node);
                            var $select = $($(this)[0].elem).parents(".layui-form-select");
                            // $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='parentId']").val(node.pid);
                            $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='orgId']").val(node.id);
                            //新建部门获取用户
                        }
                    });
                    active.getRole()
                    active.getRanks()
                }
                else if (judge == 4) {
                    //新建用户单位下拉树
                    $('#classtree3').empty();//清空子节点
                    tree({
                        elem: "#classtree3",
                        nodes: param,
                        click: function (node) {
                            console.log(node);
                            var $select = $($(this)[0].elem).parents(".layui-form-select");
                            // $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='parentId']").val(node.pid);
                            $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='orgId']").val(node.id);
                            //新建部门获取用户
                        }
                    });
                    active.getRole()
                    active.getRanks()
                }

            },
            //获取新建部门部门领导添加数据
            departmentalLleadershipAdd: function (params, nodes) {
                $(`#${params}`).empty();//清空子节点
                tree({
                    elem: `#${params}`,
                    nodes,
                    click: function (node) {
                        // console.log(node);
                        active.findUserByOrgId(node.id);
                        var $select = $($(this)[0].elem).parents(".layui-form-select");
                        $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='selectID']").val(node.id);
                    }
                });
            },
            //根据组织机构ID查询用户
            findUserByOrgId: function (id) {
                $.ajax({
                    url: baseURL_SON + "/queryRealName",
                    type: "post",
                    async: true,
                    data: { orgId: id },
                    success: function (data) {
                        // console.log(JSON.stringify(data));
                        if (data.data) {
                            DepartmentLeaderVariableLeft = data.data;
                            DepartmentLeaderVariableRight = [];
                            $('.rightData').empty();//清空子节点
                            active.renderLeft(DepartmentLeaderVariableLeft);
                        }
                    },
                    // error:function(data){
                    //     window.location.href = "../../error/error.html";
                    //     console.log(data.message)
                    // }
                })
            },
            //渲染左侧用户
            renderLeft: function (data) {
                var optionString;
                for (var i = 0; i < data.length; i++) {
                    optionString += '<option value="' + data[i].id + '">' + data[i].realName + '</option>'
                }
                $('.leftData').empty();//清空子节点
                $(".leftData").append(optionString);  // 添加子节点
            },
            //渲染右侧用户
            renderRight: function (data) {
                var optionString;
                for (var i = 0; i < data.length; i++) {
                    optionString += '<option value="' + data[i].id + '">' + data[i].realName + '</option>'
                }
                $('.rightData').empty();//清空子节点
                $(".rightData").append(optionString);// 添加子节点
            },
            //清空新建用户数据
            clearNewUser: function () {
                $('.new-user input[name=userName]').val('');
                $('.new-user input[name=realName]').val('');
                $('.new-user input[name=email]').val('');
                $('.new-user input[name=ipAddr]').val('');
                $('.new-user input[name=macAddr]').val('');
                $('.new-user input[name=telephone]').val('');
                $('.new-user input[name=phone]').val('');
                $('#classtree2').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html("选择单位").end().find("input:hidden[name='orgId']").val(0);
            },
            //清空新建部门数据
            clearNewDep: function () {
                $('.new-dep input[name=name]').val('');
                $('.new-dep select[name=rank]').val('');
                $('.new-dep input[name=leader]').val('');
                $('.new-dep input[name=leaderUser]').val('');
                $('.new-dep input[name=dutyUser]').val('');
                $('.new-dep input[name=duty]').val('');
                $('.new-dep select[name=sysId]').val('');
                $('.new-dep input[name=companyEmail]').val('');
                $('#classtree').parents(".layui-form-select").removeClass("layui-form-selected").find(".layui-select-title span").html("选择单位").end().find("input:hidden[name='parentId']").val(0);
                $('.new-dep .person-add').hide()
            }
        };
        active.init();
        window.active = active
        //排序
        table.on('sort(demo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            console.log(obj.field); //当前排序的字段名
            console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            console.log(this); //当前排序的 th 对象

            active.tableList(0, orgId, where, obj.type, obj.field);
            active.getPage(orgId, where, obj.type, obj.field);
        });

        //右移
        $('.new-dep .oneRight').on('click', function () {
            // console.log($('.leftData').val());
            if ($('.new-dep .leftData').val()) {
                if ($('.new-dep .leftData').val().length == 1) {
                    if (DepartmentLeaderVariableRight.length > 0) {
                        DepartmentLeaderVariableLeft.push(DepartmentLeaderVariableRight[0]);
                        for (var i = 0; i < DepartmentLeaderVariableLeft.length; i++) {
                            if ($('.new-dep .leftData').val() == DepartmentLeaderVariableLeft[i].id) {
                                DepartmentLeaderVariableRight.splice(0, 1, DepartmentLeaderVariableLeft[i]);
                                DepartmentLeaderVariableLeft.splice(i, 1);
                                break;
                            }
                        }
                        active.renderLeft(DepartmentLeaderVariableLeft);
                        active.renderRight(DepartmentLeaderVariableRight);
                    }
                    else {
                        for (var j = 0; j < DepartmentLeaderVariableLeft.length; j++) {
                            if ($('.new-dep .leftData').val() == DepartmentLeaderVariableLeft[j].id) {
                                DepartmentLeaderVariableRight.push(DepartmentLeaderVariableLeft[j]);
                                DepartmentLeaderVariableLeft.splice(j, 1);
                                break;
                            }
                        }
                        active.renderLeft(DepartmentLeaderVariableLeft);
                        active.renderRight(DepartmentLeaderVariableRight);
                    }

                } else {
                    layer.msg('该用户单选', { icon: 2, closeBtn: 1 });
                }
            } else {
                layer.msg('请选择一个用户', { icon: 2, closeBtn: 1 });
            }

        });
        $('.edit-dep .oneRight').on('click', function () {
            // console.log($('.leftData').val());
            if ($('.edit-dep .leftData').val()) {
                if ($('.edit-dep .leftData').val().length == 1) {
                    if (DepartmentLeaderVariableRight.length > 0) {
                        DepartmentLeaderVariableLeft.push(DepartmentLeaderVariableRight[0]);
                        for (var i = 0; i < DepartmentLeaderVariableLeft.length; i++) {
                            if ($('.edit-dep .leftData').val() == DepartmentLeaderVariableLeft[i].id) {
                                DepartmentLeaderVariableRight.splice(0, 1, DepartmentLeaderVariableLeft[i]);
                                DepartmentLeaderVariableLeft.splice(i, 1);
                                break;
                            }
                        }
                        active.renderLeft(DepartmentLeaderVariableLeft);
                        active.renderRight(DepartmentLeaderVariableRight);
                    }
                    else {
                        for (var j = 0; j < DepartmentLeaderVariableLeft.length; j++) {
                            if ($('.edit-dep .leftData').val() == DepartmentLeaderVariableLeft[j].id) {
                                DepartmentLeaderVariableRight.push(DepartmentLeaderVariableLeft[j]);
                                DepartmentLeaderVariableLeft.splice(j, 1);
                                break;
                            }
                        }
                        active.renderLeft(DepartmentLeaderVariableLeft);
                        active.renderRight(DepartmentLeaderVariableRight);
                    }

                } else {
                    layer.msg('该用户单选', { icon: 2, closeBtn: 1 });
                }
            }
            else {
                layer.msg('请选择一个用户', { icon: 2, closeBtn: 1 });
            }

        });
        //左移
        $('.new-dep .oneLeft').on('click', function () {
            if (DepartmentLeaderVariableRight.length > 0) {
                DepartmentLeaderVariableLeft.push(DepartmentLeaderVariableRight[0]);
                DepartmentLeaderVariableRight = [];
                active.renderLeft(DepartmentLeaderVariableLeft);
                active.renderRight(DepartmentLeaderVariableRight);
            }
        });
        $('.edit-dep .oneLeft').on('click', function () {
            if (DepartmentLeaderVariableRight.length > 0) {
                DepartmentLeaderVariableLeft.push(DepartmentLeaderVariableRight[0]);
                DepartmentLeaderVariableRight = [];
                active.renderLeft(DepartmentLeaderVariableLeft);
                active.renderRight(DepartmentLeaderVariableRight);
            }
        });
        //确定按钮
        $('.new-dep .determineAdministrators').on('click', function () {
            if (DepartmentLeaderVariableRight.length > 0) {
                if (flag == 1) {
                    $('.new-dep input[name=leader]').val(DepartmentLeaderVariableRight[0].id);
                    $('.new-dep input[name=leaderUser]').val(DepartmentLeaderVariableRight[0].realName);
                    $('.new-dep .person-add').hide()
                }
                else {
                    $('.new-dep input[name=dutyUser]').val(DepartmentLeaderVariableRight[0].id);
                    $('.new-dep input[name=duty]').val(DepartmentLeaderVariableRight[0].realName);
                    $('.new-dep .person-add').hide()
                }
            }
            else {
                layer.msg('请选择一个用户', { icon: 2, closeBtn: 1 });
            }

        });
        $('.edit-dep .determineAdministrators').on('click', function () {
            if (DepartmentLeaderVariableRight.length > 0) {
                if (flag == 1) {
                    $('.edit-dep input[name=leader]').val(DepartmentLeaderVariableRight[0].id);
                    $('.edit-dep input[name=leaderUser]').val(DepartmentLeaderVariableRight[0].realName);
                    $('.edit-dep .person-add').hide()
                }
                else {
                    $('.edit-dep input[name=dutyUser]').val(DepartmentLeaderVariableRight[0].id);
                    $('.edit-dep input[name=duty]').val(DepartmentLeaderVariableRight[0].realName);
                    $('.edit-dep .person-add').hide()
                }
            }
            else {
                layer.msg('请选择一个用户', { icon: 2, closeBtn: 1 });
            }

        });

        // form.on('select(usersName)', function(data){
        //     console.log(data.elem); //得到select原始DOM对象
        //     console.log(data.value); //得到被选中的值
        //     console.log(data.othis); //得到美化后的DOM对象
        // });
        //监听多选框
        form.on('checkbox(rankCheck)', function (obj) {
            if (obj.elem.checked) {
                newUserCheckboxId.push(obj.value)
            }
            else {
                for (let i = 0; i < newUserCheckboxId.length; i++) {
                    if (obj.value == newUserCheckboxId[i]) {
                        newUserCheckboxId.splice(i, 1);
                        break;
                    }
                }
            }
            // console.log(newUserCheckboxId)
        });
        //监听编辑职级单选框
        form.on('radio(rank)', function (data) {
            console.log(data.elem); //得到radio原始DOM对象
            console.log(data.value); //被点击的radio的value值
            active.findRankSize(data.value)
        });
        //监听表格
        table.on('tool(demo)', function (obj) {
            // console.log(obj);
            //示范一个公告层
            if (obj.event === "demo") {
                // console.log(obj.data.id);
                // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                judge = 4;
                editUserID = obj.data.id;
                vm.$data.editUserID = obj.data.id;
                active.getTreeList1();
                // active.editBuildUser(obj.data)
            }

        });
        //自定义验证规则
        form.verify({
            pass: [/(.+){6,12}$/, '密码必须6到12位']
            , pass1: [/(.+){6,12}$/, '密码必须6到12位']
            , pass: function (value, item) {
                if (value != $('input[name=twicePassword]').val()) {
                    return '两次密码不一致'
                }
            }
            , FullName: function (value, item) {
                if (!new RegExp("^[\u4e00-\u9fa5]+$").test(value)) {
                    return "姓名为汉字"
                }
            },
            // nameUser:function (value,item) {
            //     if(!new RegExp("^[a-zA-Z][a-zA-Z0-9_]{0,19}$").test(value)){
            //         return "请输入正确的用户名格式"
            //     }
            // },
            ip: function (value, item) { //value：表单的值、item：表单的DOM对象
                if (value) {
                    if (!new RegExp("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$").test(value)) {
                        return '请输入正确格式的ip';
                    }
                }
            },
            tel: function (value, item) {
                if (value) {
                    if (!new RegExp("^[0-9]{11}$").test(value)) {
                        return '请输入正确的电话号码'
                    }
                }

            },
            mac: function (value, item) {
                if (value) {
                    if (!new RegExp("[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}").test(value)) {
                        return '请输入正确MAC'
                    }
                }

            }
        });
        //监听新建用户提交
        form.on('submit(newUser)', function (data) {
            if (!isNoUser) {
                layer.msg('用户名重复，请更换！', {
                    icon: 2,
                    closeBtn: 1
                })
            }
            else {
                if (data.field.orgId == 0 || (formSelects.array('select1').length == 0 && data.field.loginPageAccess != 1)) {
                    // console.log(111)
                    if (data.field.orgId == 0 || formSelects.array('select3').length == 0) {
                        layer.msg('必填项不能为空', { icon: 2, closeBtn: 1 })
                    }
                    else {
                        layer.msg('必填项不能为空!', { icon: 2, closeBtn: 1 })
                    }


                }
                else {
                    var roleIds = [];
                    var sysIds = [];
                    var professionalTitle = [];
                    for (let i = 0; i < formSelects.array('select1').length; i++) {
                        roleIds.push(formSelects.array('select1')[i].val);
                    }
                    for (let j = 0; j < formSelects.array('select2').length; j++) {
                        sysIds.push(formSelects.array('select2')[j].val);
                    }
                    for (let k = 0; k < formSelects.array('select3').length; k++) {
                        professionalTitle.push(formSelects.array('select3')[k].val);
                    }

                    data.field.roleIds = roleIds.toString();
                    data.field.sysIds = sysIds.toString();
                    // data.field.professionalTitle = professionalTitle.toString();
                    data.field.professionalTitle = $('.dsf').val()
                    data.field.domainIds = newUserCheckboxId.toString();

                    active.newAddUser(data.field);
                    // active.afternewAddUser(data.field);

                    newUserCheckboxId = []
                }
            }
            return false;
        });
        //监听编辑用户提交
        form.on('submit(editUser)', function (data) {
            // console.log(data)
            // if(data.field.orgId == 0 || !$('.edit-user .layui-form-checkbox').hasClass('layui-form-checked')){
            //     if(data.field.orgId == 0 || formSelects.array('select3').length==0){
            //       console.log(data.field.orgId,formSelects.array('select3').length)
            //         layer.msg('必填项不能为空',{icon:2,closeBtn:1})
            //     }
            //     else{
            //         layer.msg('必填项不能为空!',{icon:2,closeBtn:1})
            //         console.log(data.field.orgId,formSelects.array('select3').length)
            //     }
            // }
            if (data.field.orgId == '' || data.field.realName == '' || data.field.editroleIds == '') {
                layer.msg('必填项不能为空!', { icon: 2, closeBtn: 1 })
            }
            else {
                var roleIds = [];
                var sysIds = [];
                for (let i = 0; i < formSelects.array('select3').length; i++) {
                    roleIds.push(formSelects.array('select3')[i].val);
                }
                for (let j = 0; j < formSelects.array('select4').length; j++) {
                    sysIds.push(formSelects.array('select4')[j].val);
                }
                data.field.roleIds = roleIds.toString();
                data.field.sysIds = sysIds.toString();
                if ($('.edit-user .layui-form-checkbox').hasClass('layui-form-checked')) {
                    data.field.loginPageAccess = 1
                }
                active.editAddUser(data.field);
            }
            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        //监听新建部门
        form.on('submit(newDep)', function (data) {
            if (rootNode) {
                if (data.field.parentId == 0) {
                    layer.msg('必填项不能为空', { icon: 5 })
                } else {
                    layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                    active.newAddOrganization(data.field);
                }
            }
            else {
                data.field.parentId = 0;
                data.field.sysId = "";
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                active.newAddOrganization(data.field);
            }

            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        //监听编辑部门
        form.on('submit(editDep)', function (data) {
            if (editDepPID != 0) {
                if (data.field.parentId == 0) {
                    layer.msg('必填项不能为空', { icon: 5 })
                } else {
                    layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                    active.editAddOrganization(data.field);
                }
            }
            else {
                data.field.parentId = 0;
                data.field.sysId = "";
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                active.editAddOrganization(data.field);
            }
            // layer.msg(JSON.stringify(data.field));
            return false;
        });
        //监听删除部门
        form.on('submit(delDep)', function (data) {
            layer.confirm('您确定要删除吗？', {
                btn: ['确定', '取消'],//按钮
                //
            }, function () {
                layer.confirm('执行此操作将删除部门下所有用户信息！', {
                    icon: 0,
                    btn: ['确定', '取消'],//按钮
                    //
                }, function () {
                    layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                    active.delOrganization(data.field.id);
                }, function () {
                    layer.msg('您取消了删除', {
                        icon: 2,
                        // time: 2000, //20s后自动关闭
                        // btn: ['确定'],
                        closeBtn: 1
                    });
                });

            }, function () {
                layer.msg('您取消了删除', {
                    icon: 2,
                    // time: 2000, //20s后自动关闭
                    // btn: ['确定'],
                    closeBtn: 1
                });
            });

            // layer.msg(JSON.stringify(data.field.id));
            return false;
        });
        //监听编辑职级
        form.on('submit(rank)', function (data) {
            // console.log(JSON.stringify(data.field))
            active.newAddRank(data.field);
            return false;
        });
        //监听删除职级选框
        form.on('checkbox(check)', function (data) {
            // console.log(data);
            if (data.elem.checked) {
                checkFlag.push(data.value);
            }
            else {
                for (let i = 0; i < checkFlag.length; i++) {
                    if (data.value == checkFlag[i]) {
                        checkFlag.splice(i, 1);
                        break;
                    }
                }
            }
            // console.log(checkFlag)
            // console.log(data);
            // console.log(data.elem.checked);
            // console.log(data.value)
        });
        //监听删除职级提交
        form.on('submit(check)', function (data) {
            if (checkFlag.length > 0) {
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                active.deleteRank(checkFlag.toString());
            }
            else {
                layer.msg('请选择要删除的职级', { icon: 0, closeBtn: 1 })
            }
            return false;
        });
        //监听提交更改密码
        form.on('submit(determineUpPass)', function (data) {
            layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
            active.updatePassword(data);
            return false;
        });
        //新建用户弹框
        $(".new-build-person").click(function () {
            // form.render("select",'new_user');
            // form.render()

            // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
            judge = 3;
            active.getIsAc();
            active.getTreeList1();
            vm.$data.dialogVisible = true
            vm.$data.dialogTitle = '新建用户'
            vm.$data.disabled=false
            // active.getRole()
            //示范一个公告层
            // active.newBuildUser()
        });
        //编辑用户弹框
        $(".edit-build-person").click(function () {

            var checkStatus = table.checkStatus('userTable');
            if (checkStatus.data.length == 1) {
                // layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
                judge = 4;
                editUserID = checkStatus.data[0].id
                vm.$data.editUserID = checkStatus.data[0].id
                active.getIsAc();
                active.getTreeList1();
                // active.editBuildUser(checkStatus.data[0].id)
                // console.log(checkStatus.data[0].id)
            }
            else if (checkStatus.data.length > 1) {
                layer.msg('请选择一个用户', { icon: 2 });
            }
            else {
                layer.msg('请选择用户', { icon: 2 });
            }
            // console.log(checkStatus.data) //获取选中行的数据
            // console.log(checkStatus.data.length) //获取选中行数量，可作为是否有选中行的条件

        });
        //新建部门弹框
        $(".new-build-department").click(function () {
            //示范一个公告层
            layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
            judge = 1;
            active.getRankList(active.getTreeList1);
            // active.newBuildDepartment()
        });
        //编辑部门弹框
        $(".edit-build-department").click(function () {
            judge = 2;
            if (editDepID) {
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                //示范一个公告层
                active.getRankList(active.getTreeList1);
            }
            else {
                layer.msg('请选择您要编辑的部门', {
                    icon: 2,
                    closeBtn: 1
                })
            }
        });
        //编辑职级
        $(".edit-build-rank").click(function () {
            layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
            addRank = true;
            active.findRankSize($('.edit-class input[name=size]:checked').val());
            // active.getRankList();
        });
        //更改密码
        $(".updata-build-pass").click(function () {
            var checkStatus = table.checkStatus('userTable');
            if (checkStatus.data.length == 1) {
                // console.log(checkStatus.data[0])
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                active.updataPass(checkStatus.data[0])
            }
            else if (checkStatus.data.length > 1) {
                layer.msg('请选择一个用户', { icon: 2 });
            }
            else {
                layer.msg('请选择用户', { icon: 2 });
            }

        });
        //解锁按钮
        $(".Unlock").click(function () {
            var checkStatus = table.checkStatus('userTable');
            if (checkStatus.data.length == 1) {
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                // console.log(checkStatus.data[0].id)
                active.unlockPadss(checkStatus.data[0].id)
            }
            else if (checkStatus.data.length > 1) {
                layer.msg('请选择一个用户', { icon: 2 });
            }
            else {
                layer.msg('请选择用户', { icon: 2 });
            }

        });
        // 冻结按钮
        $(".Frozen").click(function () {
            var checkStatus = table.checkStatus('userTable');
            if (checkStatus.data.length == 1) {
                layer.load(0, { shade: [0.1] }); //0代表加载的风格，支持0-2//loading层
                active.FrozenMethod(checkStatus.data[0].id)
            }
            else if (checkStatus.data.length > 1) {
                layer.msg('请选择一个用户', { icon: 2 });
            }
            else {
                layer.msg('请选择用户', { icon: 2 });
            }
        });
        //删除用户
        $(".del-user").on("click", function () {
            var checkStatus = table.checkStatus('userTable');
            let checkArray = [];
            for (let i = 0; i < checkStatus.data.length; i++) {
                checkArray.push(checkStatus.data[i].id);
            }
            console.log(111)
            if (checkArray.length > 0) {
                let index = layer.open({
                    type: 1
                    , title: ["信息", 'font-size:20px;font-weight:900'] //不显示标题栏
                    , closeBtn: 1
                    , area: ['350px', '200px']
                    , shade: 0.8
                    , id: 'fjifd' //设定一个id，防止重复弹出
                    // ,resize: false
                    // ,btn: ['确定']
                    , btnAlign: 'c'
                    , moveType: 1 //拖拽模式，0或者1
                    , content: $('#opensid')
                    , success: function (layero) {
                        $("#noDel").on("click", function () {
                            //关闭所有弹框
                            layer.close(index);
                            $('#opensid').hide()
                        });
                        $("#sureDel").on("click", function () {
                            var isDelCould = $('#isDEl').prop("checked") == true ? "1" : "0"
                            console.log(checkArray.toString(), isDelCould)
                            layer.close(index);
                            $('#opensid').hide()
                            active.delUser({ "userIds": checkArray.toString(), "clearData": isDelCould });
                            active.afterdelUser(checkArray.toString());
                            if (orgId) {
                                active.tableList(0, orgId);
                                active.getPage(orgId)
                            }
                            else {
                                active.tableList(0);
                                active.getPage()
                            }
                        });
                    }
                    , end: function (index, layero) {
                        $(".new-role").hide()
                        $("#isDEl").prop("checked", false);
                        $("#isDEl").next().removeClass('layui-form-checked')
                    }
                });
            } else {
                layer.msg('请选择用户！', { icon: 2, closeBtn: 1 });
            }
            // if(checkArray.length>0){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮

            // }, function(){
            //     active.delUser(checkArray.toString());
            //     active.afterdelUser(checkArray.toString());
            //     if(orgId){
            //         active.tableList(0,orgId);
            //         active.getPage(orgId)
            //     }
            //     else{
            //         active.tableList(0);
            //         active.getPage()
            //     }
            //
            // }, function(){
            //     layer.msg('您取消了删除', {
            //         icon:2,
            //         time: 2000, //20s后自动关闭
            //         btn: ['确定'],
            // closeBtn:1
            // });
            // });
            // }
            // else{
            //     layer.msg('请选择用户！',{icon:2,closeBtn:1});
            // }




            // if(checkStatus.data.length==1){
            //     layer.confirm('您确定要删除吗？', {
            //         btn: ['确定','取消'],//按钮
            //         //
            //     }, function(){
            //         console.log(checkStatus.data[0].id);
            //         active.delUser(checkStatus.data[0].id);
            //
            //     }, function(){
            //         layer.msg('您取消了删除', {
            //             icon:2,
            //             // time: 2000, //20s后自动关闭
            //             // btn: ['确定'],
            //             closeBtn:1
            //         });
            //     });
            //     // console.log(checkStatus.data[0].id)
            // }
            // else if(checkStatus.data.length>1){
            //     layer.msg('请选择一个用户',{icon:2});
            // }
            // else{
            //     layer.msg('请选择用户',{icon:2});
            // }
        });
        //下载硬件导入模板
        $(".downloadTemplate").on('click', function () {
            window.open(baseURL_SON + '/downloadUserTemplate');
        });
        //上传导入
        upload.render({
            elem: ".Import",
            url: baseURL_SON + '/importUser',
            method: 'post',
            accept: 'file',
            exts: 'xls|xlsx',
            // headers:"multipart/form-data",
            before: function (obj) {
                layer.load(0, { shade: [0.1] }); //上传loading
                console.log('接口地址：' + this.url);
                console.log(this.item)
            },
            done: function (res) {
                console.log(res);
                layer.closeAll('loading');
                if (res.code == 200 && res.data > 0) {
                    active.getTreeList();
                    active.tableList();
                    active.getPage();
                    layer.closeAll('loading');
                    layer.msg(res.message, { icon: 1, closeBtn: 1 });
                }
                else {
                    layer.closeAll('loading');
                    layer.msg(res.message, { icon: 2, closeBtn: 1 });
                }
                // alert("上传成功")
                //上传完毕回调
            },
            error: function () {
                layer.closeAll('loading');
                alert("上传失败")
                //请求异常回调
            }
        });
        //导出
        $(".exportApplication").on('click', function () {
            window.open(baseURL_SON + '/exportUser');
        });
        //加号按钮
        $(".departmentalLleadershipAdd").on("click", function () {
            // $('.person-add').css("display","block")
            flag = 1;
            if (judge == 1) {
                $('.new-dep .leftData').empty();
                $('.new-dep .rightData').empty();
                active.getTreeList1('treeList3');
            }
            else if (judge == 2) {
                $('.edit-dep .leftData').empty();
                $('.edit-dep .rightData').empty();
                active.getTreeList1('treeList2');
            }
            // $('.person-add').show()
        });
        $(".leadershipAdd").on("click", function () {
            // $('.person-add').css("display","block")
            flag = 2;
            if (judge == 1) {
                $('.new-dep .leftData').empty();
                $('.new-dep .rightData').empty();
                active.getTreeList1('treeList3');
            }
            else if (judge == 2) {
                $('.edit-dep .leftData').empty();
                $('.edit-dep .rightData').empty();
                active.getTreeList1('treeList2');
            }

            // $('.person-add').show()
        });
        //删除弹框
        $(".del-dep").on("click", function () {

            layer.confirm('您确定要删除吗？', {
                btn: ['确定', '取消'],//按钮
                //
            }, function () {
                layer.msg('删除成功', {
                    icon: 1,
                    closeBtn: 1
                });
            }, function () {
                layer.msg('您取消了删除', {
                    icon: 2,
                    // time: 2000, //20s后自动关闭
                    // btn: ['确定'],
                    closeBtn: 1
                });
            });
        });
        $('.new-user input[name=userName]').on('blur', function () {
            active.verifyUserNameSole($.trim($(this).val()))
        });
        //新建用户和邮箱联动
        $('.new-user input[name=userName]').on('input', function () {
            $('.new-user input[name=email]').val($.trim($('.new-user input[name=userName]').val()) + Emailvariable)
            if ($.trim($('.new-user input[name=userName]').val()) == "") {
                $('.new-user input[name=email]').val('')
            }

        });
        //键盘抬起重新加载
        // $("#index-footer1").on("keyup",function(){
        $("#index-footer1").on("keyup", function () {
            if (!$("#index-footer1").val()) {
                where = "";
                active.tableList(0, orgId);
                active.getPage(orgId);
            }
        });
        $('#icon-footer1').on('click', function () {
            where = "";
            $(this).prev().val('');
            active.tableList(0, orgId);
            active.getPage(orgId);
            // console.log($(this).prev())
        })
        //搜索
        $("#btn-footer1").on("click", function () {
            // console.log(this);
            if (!$("#index-footer1").val()) {
                // console.log(11111)
                // layer.tips('不能为空！！！', $("#index-footer1"), { tips: [3, '#FF5722'] }); //在元素的事件回调体中，follow直接赋予this即可
            }
            else {
                where = $("#index-footer1").val()
                active.tableList(0, orgId, where)
                active.getPage(orgId, where)
                // active.tableList(0,orgId,$("#index-footer1").val())
                // active.getPage(orgId,$("#index-footer1").val())
            }

        });
        //搜索
        $("#index-footer1").on("keypress", function () {
            // console.log(this);
            if (event.keyCode == "13") {
                if (!$("#index-footer1").val()) {
                    // console.log(11111)
                    // layer.tips('不能为空！！！', $("#index-footer1"), {tips: [3, '#FF5722']}); //在元素的事件回调体中，follow直接赋予this即可
                } else {
                    where = $("#index-footer1").val()
                    active.tableList(0, orgId, where)
                    active.getPage(orgId, where)
                    // active.tableList(0,orgId,$("#index-footer1").val())
                    // active.getPage(orgId,$("#index-footer1").val())
                }
            }
        });
        //下拉树操作
        $(".selecttree .downpanel").on("click", ".layui-select-title", function (e) {
            $(".selecttree .layui-form-select").not($(this).parents(".layui-form-select")).removeClass("layui-form-selected");
            $(this).parents(".downpanel").toggleClass("layui-form-selected");
            layui.stope(e);
        }).on("click", "dl i", function (e) {
            layui.stope(e);
        });
        $(document).on("click", function (e) {
            $(".selecttree .layui-form-select").removeClass("layui-form-selected");
        });

        //选中下拉树颜色变化
        // $("body .left-nav").on("mousedown",".layui-tree a",function(){
        $("body").on("mousedown", ".layui-tree a", function () {
            // $(".layui-tree a").css('color','black');
            // $(this).css('color','red');
            $(".layui-tree a").css('background', 'none');
            $(this).css('background', '#1E9FFF');
        })

    });

})();