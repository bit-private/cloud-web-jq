
Mock.mock('/mock/table', 'post', {
    code: 0,
    msg: "",
    count: 1000,
    "totalPages": 2,
    "data|15": [{
        /* 'name': '@cname', //中文名称
        'age|1-100': 100, //100以内随机整数
        'birthday': '@date("yyyy-MM-dd")', //日期
        'city': '@city(true)', //中国城市 */
        'userName': '@cname',
        'fullName|1': ['男', '女'],
        'mail': '@city(true)',
        'role': '@string',
        'lastTime|1-1000': 1000,
        'systemUser|1-100': 100,
        'IPaddress|1': ['模特', '老师'],
        'MACaddress|1-10000': 10000
    }]
});
