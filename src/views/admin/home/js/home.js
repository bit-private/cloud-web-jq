(function () {
  //JavaScript代码区域
  
  layui.use(['jquery', 'element', 'form', 'layedit', 'laydate', "tree", "table", "layer"], function () {
    var element = layui.element,
      tree = layui.tree,
      table = layui.table,
      jquery = layui.jquery,
      layer = layui.layer,
      form = layui.form;

    var listFn = {
      diaogShow: function (params) {
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['40%', '400px'],
          shade: 0.3,
          id: 'message', //设定一个id，防止重复弹出
          btn: ['确定'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#messageDiaog'),
          end: function (params) {
            $('#messageDiaog').hide()
          }
        });
      },
      departmentList: function (params) {
        tree({
          elem: '#treeList',
          nodes: [{ //节点数据
            name: '节点A',
            spread: true,
            children: [{
              name: '节点A1'
            }]
          }, {
            name: '节点B',
            children: [{
              name: '节点B1',
              alias: 'bb' //可选
                ,
              id: '123' //可选
            }, {
              name: '节点B2'
            }]
          }],
          click: function (node) {
            console.log(node) //node即为当前点击的节点数据
          }
        })
      },
    }

    // 接口路径与端口
    // var url = 'http://10.1.2.121:9006';  // 佳豪 - 06
    // const CONNECT = url + ':9006/EquipmentMonitor/totalPerformance'; // 连接管理 - 佳豪
    const CONNECT = url + ':9006/HomePage/hardwareReport'; // 连接管理 - 佳豪
    const SOFTWARE = url + ':9001/application/getApplicationInfo'; // 软件数据 - 书军 - 01
    const USERINFO = url + ':9006/UserMonitor/adminViewerInfo'; // 用户数据 - 佳豪
    const EQUIPMENT = url + ':9001/HardwareResource/informatioManagement'; //设备数据 - 杨然9001

    $.ajax({ // 首页仪表盘数据
      type: "POST",
      url: CONNECT,
      // data:{"operatingSystemId": "liunx"},
      data: JSON.stringify({}),
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      success: function (data) {
        // console.log(data)
        var myChart = echarts.init(document.getElementById('main'));
        var myChart1 = echarts.init(document.getElementById('main1'));
        var myChart2 = echarts.init(document.getElementById('main2'));
        var data = data.data;
        option = {
          tooltip: {
            formatter: "{a} <br/>{b} : {c}"
          },
          series: [{
            //类型
            type: 'gauge',
            min: 0,
            max: 100,
            //半径
            //radius: 180,
            //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
            startAngle: 180,
            //结束角度。
            endAngle: 0,
            center: ['50%', '70%'],
            //仪表盘轴线相关配置。
            axisLine: {
              show: true,
              // 属性lineStyle控制线条样式
              lineStyle: {
                width: 40,
                color: [
                  [(data.cpu / 100), '#00CFB5'], // 进度条进度
                  [1, '#2FACEB']
                ]
              }
            },
            //分隔线样式。
            splitLine: {
              show: true,
              length: 0,
            },
            //刻度样式。
            axisTick: {
              show: true
            },
            //刻度标签。
            axisLabel: {
              show: true,
              fontSize: 12,
              "distance": -20
            },
            //仪表盘指针。
            pointer: {
              //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
              show: true,
              //指针长度
              length: '110%',
              width: 2,
            },
            //仪表盘标题。
            title: {
              show: true,
              offsetCenter: [0, '-25%'], // x, y，单位px
              textStyle: {
                color: '#000',
                fontSize: 14
              }
            },
            //仪表盘详情，用于显示数据。
            detail: {
              show: true,
              offsetCenter: [0, 0],
              formatter: '{value}%',
              textStyle: {
                color: '#000',
                fontSize: 14
              }
            },
            splitNumber: 1,
            data: [{
              value: data.cpu,
              name: 'cpu',
              textStyle: {
                fontSize: 16
              }
            }]
          }]
        };

        myChart.setOption(option);

        option1 = {
          tooltip: {
            formatter: "{a} <br/>{b} : {c}"
          },
          series: [{
            //类型
            type: 'gauge',
            min: 0,
            max: 100,
            //半径
            //radius: 180,
            //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
            startAngle: 180,
            //结束角度。
            endAngle: 0,
            center: ['50%', '70%'],
            //仪表盘轴线相关配置。
            axisLine: {
              show: true,
              // 属性lineStyle控制线条样式
              lineStyle: {
                width: 40,
                color: [
                  [(data.gpu / 100), '#00CFB5'], // 进度条进度
                  [1, '#2FACEB']
                ]
              }
            },
            //分隔线样式。
            splitLine: {
              show: true,
              length: 0,
            },
            //刻度样式。
            axisTick: {
              show: true,
            },
            //刻度标签。
            axisLabel: {
              show: true,
              fontSize: 12,
              "distance": -20
            },
            //仪表盘指针。
            pointer: {
              //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
              show: true,
              //指针长度
              length: '110%',
              width: 2,
            },
            //仪表盘标题。
            title: {
              show: true,
              offsetCenter: [0, '-25%'], // x, y，单位px
              textStyle: {
                color: '#000',
                fontSize: 14
              }
            },
            //仪表盘详情，用于显示数据。
            detail: {
              show: true,
              offsetCenter: [0, 0],
              formatter: '{value}%',
              textStyle: {
                color: '#000',
                fontSize: 14
              }
            },
            splitNumber: 1,
            data: [{
              value: data.gpu,
              name: 'gpu',
              textStyle: {
                fontSize: 16
              }
            }]
          }]
        };

        myChart1.setOption(option1);

        option2 = {
          tooltip: {
            formatter: "{a} <br/>{b} : {c}"
          },
          series: [{
            //类型
            type: 'gauge',
            min: 0,
            max: 100,
            //半径
            //radius: 180,
            //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
            startAngle: 180,
            //结束角度。
            endAngle: 0,
            center: ['50%', '70%'],
            //仪表盘轴线相关配置。
            axisLine: {
              show: true,
              // 属性lineStyle控制线条样式
              lineStyle: {
                width: 40,
                color: [
                  [(data.men / 100), '#00CFB5'], // 进度条进度
                  [1, '#2FACEB']
                ]
              }
            },
            //分隔线样式。
            splitLine: {
              show: true,
              length: 0,
            },
            //刻度样式。
            axisTick: {
              show: true,
            },
            //刻度标签。
            axisLabel: {
              show: true,
              fontSize: 12,
              "distance": -20
            },
            //仪表盘指针。
            pointer: {
              //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
              show: true,
              //指针长度
              length: '110%',
              width: 2,
            },
            //仪表盘标题。
            title: {
              show: true,
              offsetCenter: [0, '-25%'], // x, y，单位px
              textStyle: {
                color: '#000',
                fontSize: 14
              }
            },
            //仪表盘详情，用于显示数据。
            detail: {
              show: true,
              offsetCenter: [0, 0],
              formatter: '{value}%',
              textStyle: {
                color: '#000',
                fontSize: 14
              }
            },
            splitNumber: 1,
            data: [{
              value: data.men,
              name: '内存',
              textStyle: {
                fontSize: 16
              }
            }]
          }]
        };

        myChart2.setOption(option2);

      }
    })


    $.ajax({ // 软件信息数据
      type: 'POST',
      url: SOFTWARE,
      data: {},
      success: function (data) {
        // console.log(data)
        var data = data.data;
        $("#applCount").text(data.applCount);
        // $("#moduleCount").text(data.moduleCount)
        $("#applUseCount").text(data.applUseCount)
      }
    })

    $.ajax({ // 设备信息数据
      type: 'POST',
      url: EQUIPMENT,
      data: {},
      success: function (data) {
        console.log(data)
        var data = data.data;
        $("#hardwareCount").text(data.hardwareCount)
        $("#startCount").text(data.startCount)
        $("#warning").text(data.warning)
        $("#willExpire").text(data.willExpire)
        $("#failureCount").text(data.failureCount)
        $("#disableCount").text(data.disableCount)
        $("#outWarranty").text(data.outWarranty)
      }
    })

    $.ajax({ // 用户信息数据
      type: 'GET',
      url: USERINFO,
      // url: 'http://10.1.1.213:9006/UserMonitor/adminViewerInfo',
      data: {},
      success: function (data) {
        console.log(data)
        if (data.code === 400) {
          // layer.msg(data.data.error)
        }
        var data = data.data;
        $("#userCount").text(data.userCount)
        $("#onLineUserCount").text(data.onLineUserCount)
        $("#onLineUserTimeCount").text(toHourMinute(parseInt(data.onLineUserTimeCount)))
        $("#maxTimeDepartment").text(data.departmentName + '(' + toHourMinute(parseInt(data.departmentTime)) + ')')
        $("#maxTimeUserStr").text(data.userName + '(' + toHourMinute(parseInt(data.userTime)) + ')')
      }
    })

    // 将分钟数量转换为小时和分钟字符串
    function toHourMinute(minutes) {
      return (Math.floor(minutes / 60) + "小时" + (minutes % 60) + "分");
    }

    var autoRefresh;
    var t = 300000;
    autoRefresh = setInterval(function () { // 自动刷新
      $.ajax({ // 首页仪表盘数据
        type: "POST",
        url: CONNECT,
        // data:{"operatingSystemId": "liunx"},
        data: JSON.stringify({}),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          var myChart = echarts.init(document.getElementById('main'));
          var myChart1 = echarts.init(document.getElementById('main1'));
          var myChart2 = echarts.init(document.getElementById('main2'));
          var data = data.data;
          option = {
            tooltip: {
              formatter: "{a} <br/>{b} : {c}"
            },
            series: [{
              //类型
              type: 'gauge',
              min: 0,
              max: 100,
              //半径
              //radius: 180,
              //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
              startAngle: 180,
              //结束角度。
              endAngle: 0,
              center: ['50%', '70%'],
              //仪表盘轴线相关配置。
              axisLine: {
                show: true,
                // 属性lineStyle控制线条样式
                lineStyle: {
                  width: 40,
                  color: [
                    [(data.cpu / 100), '#00CFB5'], // 进度条进度
                    [1, '#2FACEB']
                  ]
                }
              },
              //分隔线样式。
              splitLine: {
                show: true,
                length: 0,
              },
              //刻度样式。
              axisTick: {
                show: true
              },
              //刻度标签。
              axisLabel: {
                show: true,
                fontSize: 12,
                "distance": -20
              },
              //仪表盘指针。
              pointer: {
                //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
                show: true,
                //指针长度
                length: '110%',
                width: 2,
              },
              //仪表盘标题。
              title: {
                show: true,
                offsetCenter: [0, '-25%'], // x, y，单位px
                textStyle: {
                  color: '#000',
                  fontSize: 14
                }
              },
              //仪表盘详情，用于显示数据。
              detail: {
                show: true,
                offsetCenter: [0, 0],
                formatter: '{value}%',
                textStyle: {
                  color: '#000',
                  fontSize: 14
                }
              },
              splitNumber: 1,
              data: [{
                value: data.cpu,
                name: 'cpu',
                textStyle: {
                  fontSize: 16
                }
              }]
            }]
          };

          myChart.setOption(option);

          option1 = {
            tooltip: {
              formatter: "{a} <br/>{b} : {c}"
            },
            series: [{
              //类型
              type: 'gauge',
              min: 0,
              max: 100,
              //半径
              //radius: 180,
              //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
              startAngle: 180,
              //结束角度。
              endAngle: 0,
              center: ['50%', '70%'],
              //仪表盘轴线相关配置。
              axisLine: {
                show: true,
                // 属性lineStyle控制线条样式
                lineStyle: {
                  width: 40,
                  color: [
                    [(data.gpu / 100), '#00CFB5'], // 进度条进度
                    [1, '#2FACEB']
                  ]
                }
              },
              //分隔线样式。
              splitLine: {
                show: true,
                length: 0,
              },
              //刻度样式。
              axisTick: {
                show: true,
              },
              //刻度标签。
              axisLabel: {
                show: true,
                fontSize: 12,
                "distance": -20
              },
              //仪表盘指针。
              pointer: {
                //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
                show: true,
                //指针长度
                length: '110%',
                width: 2,
              },
              //仪表盘标题。
              title: {
                show: true,
                offsetCenter: [0, '-25%'], // x, y，单位px
                textStyle: {
                  color: '#000',
                  fontSize: 14
                }
              },
              //仪表盘详情，用于显示数据。
              detail: {
                show: true,
                offsetCenter: [0, 0],
                formatter: '{value}%',
                textStyle: {
                  color: '#000',
                  fontSize: 14
                }
              },
              splitNumber: 1,
              data: [{
                value: data.gpu,
                name: 'gpu',
                textStyle: {
                  fontSize: 16
                }
              }]
            }]
          };

          myChart1.setOption(option1);

          option2 = {
            tooltip: {
              formatter: "{a} <br/>{b} : {c}"
            },
            series: [{
              //类型
              type: 'gauge',
              min: 0,
              max: 100,
              //半径
              //radius: 180,
              //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
              startAngle: 180,
              //结束角度。
              endAngle: 0,
              center: ['50%', '70%'],
              //仪表盘轴线相关配置。
              axisLine: {
                show: true,
                // 属性lineStyle控制线条样式
                lineStyle: {
                  width: 40,
                  color: [
                    [(data.men / 100), '#00CFB5'], // 进度条进度
                    [1, '#2FACEB']
                  ]
                }
              },
              //分隔线样式。
              splitLine: {
                show: true,
                length: 0,
              },
              //刻度样式。
              axisTick: {
                show: true,
              },
              //刻度标签。
              axisLabel: {
                show: true,
                fontSize: 12,
                "distance": -20
              },
              //仪表盘指针。
              pointer: {
                //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
                show: true,
                //指针长度
                length: '110%',
                width: 2,
              },
              //仪表盘标题。
              title: {
                show: true,
                offsetCenter: [0, '-25%'], // x, y，单位px
                textStyle: {
                  color: '#000',
                  fontSize: 14
                }
              },
              //仪表盘详情，用于显示数据。
              detail: {
                show: true,
                offsetCenter: [0, 0],
                formatter: '{value}%',
                textStyle: {
                  color: '#000',
                  fontSize: 14
                }
              },
              splitNumber: 1,
              data: [{
                value: data.men,
                name: '内存',
                textStyle: {
                  fontSize: 16
                }
              }]
            }]
          };

          myChart2.setOption(option2);

        }
      })

      $.ajax({ // 软件信息数据
        type: 'POST',
        url: SOFTWARE,
        data: {},
        success: function (data) {
          // console.log(data)
          var data = data.data;
          $("#applCount").text(data.applCount);
          // $("#moduleCount").text(data.moduleCount)
          $("#applUseCount").text(data.applUseCount)
        }
      })

      $.ajax({ // 设备信息数据
        type: 'POST',
        url: EQUIPMENT,
        data: {},
        success: function (data) {
          // console.log(data)
          var data = data.data;
          $("#hardwareCount").text(data.hardwareCount)
          $("#startCount").text(data.startCount)
          $("#warning").text(data.warning)
          $("#willExpire").text(data.willExpire)
          $("#failureCount").text(data.failureCount)
        }
      })

      $.ajax({ // 用户信息数据
        type: 'GET',
        url: USERINFO,
        data: {},
        success: function (data) {
          console.log(data)
          if (data.code === 400) {
            // layer.msg(data.data.error)
          }
          var data = data.data;
          $("#userCount").text(data.userCount)
          $("#onLineUserCount").text(data.onLineUserCount)
          $("#onLineUserTimeCount").text(toHourMinute(parseInt(data.onLineUserTimeCount)))
          $("#maxTimeDepartment").text(data.departmentName + '(' + toHourMinute(parseInt(data.departmentTime)) + ')')
          $("#maxTimeUserStr").text(data.userName + '(' + toHourMinute(parseInt(data.userTime)) + ')')
        }
      })
    }, t)



    function getCookie(name) { // 获取cookie 公共函数
      var strcookie = document.cookie; //获取cookie字符串
      var arrcookie = strcookie.split("; "); //分割
      //遍历匹配
      for (var i = 0; i < arrcookie.length; i++) {
        var arr = arrcookie[i].split("=");
        if (arr[0] == name) {
          return arr[1];
        }
      }
      return "";
    }

    // //zbw添加，判断登录之后默认选中状态是首页。
    window.onload = function () {
      $('.home').parent().addClass('layui-this');
      $('.home').parent().siblings().children("dl").children("dd").removeClass('layui-this')
      loads()
    }
    var userId = getCookie('userId')

    function loads() {
      $("#sendMessage").on("click", function () { // 点击发送消息
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['50%', '400px'],
          shade: 0.3,
          id: 'message', //设定一个id，防止重复弹出
          btn: ['确定'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#messageDiaog'),
          yes: function (index) {
            layer.close(index);
            var optionsId = $("#choose_sel option")
            var putUserIds = '';
            var note = $(".content").val()
            var liveTime = $(".liveTime").val() || 30

            var re = /^[1-9]+[0-9]*]*$/; // 正则验证是否是正确天数
            if (!re.test(liveTime)) {
              layer.msg("请输入正确的消息显示时间");
              return false;
            }

            for (var i = 0; i < optionsId.length; i++) {
              putUserIds += "," + optionsId[i].getAttribute("dataid") // 将逗号拼接在前面
              // putUserIds.push(optionsId[i].value)
            }
            putUserIds = putUserIds.substring(1); // 截取 putUserIds 字符串最前面的逗号
            console.log(putUserIds)
            if (note == '') {
              layer.msg("请输入要发送的内容!")
              return false
            } else {
              // console.log(putUserIds, userId, optionsId)
              // console.log("接收人id是"+putUserIds,"分享者id是"+userId)
              $.ajax({ // 发送消息
                type: "POST",
                url: url + ':9006/Message/sendMessage',
                // url: 'http://10.1.1.213:9006/Message/sendMessage',
                data: JSON.stringify({
                  "putUserIds": putUserIds,
                  "userId": userId,
                  "note": note,
                  "liveTime": liveTime
                }),
                headers: {
                  "Content-Type": "application/json;charset=utf-8"
                },
                success: function (data) {
                  console.log(data);
                  layer.msg(data.message);
                  $(".addressee").val('');
                  $(".content").val('');
                  $(".liveTime").val('');
                }
              })
            }

          },
          end: function (params) {
            $('#messageDiaog').hide()
          }
        });

        if ($('.layui-badge').html() > 0) {
          $.ajax({ // 按userid收消息
            type: "POST",
            url: url + ':9006/Message/updateState/' + userId,
            // url: 'http://10.1.1.213:9006/Message/updateState/' + userId,
            // data: JSON.stringify({ "userId": userId, "searchInfo": searchInfo }),
            headers: {
              "Content-Type": "application/json;charset=utf-8"
            },
            success: function (data) {
              // console.log(data)
              $('.layui-badge').html('0')
            }
          })
        }
      })

      $(".add-user").on("click", function () { // 点击加号按钮
        $("#brand_sel").html('') // 每次点击的时候都 清空brand_sel中的内容
        $("#choose_sel").html('') // 每次点击的时候都 清空choose_sel中的内容
        layer.open({
          type: 1,
          title: "详情", //不显示标题栏
          closeBtn: 1,
          area: ['800px', '335px'],
          shade: 0.3,
          id: 'message1', //设定一个id，防止重复弹出
          btn: ['确定'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $("#userAddDiaog"),
          end: function () {
            // $("#userAddDiaog").hidie();
            $("#userAddDiaog").css("display", "none");
            // $(".addressee").val($("#choose_sel").text())
            // console.log($("#choose_sel").text())
            var ids = $("#choose_sel option");
            var values = '';
            for (var i = 0; i < ids.length; i++) {
              values += ',' + ids[i].text
            }
            values = values.substring(1);
            $(".addressee").val(values)
            console.log(values)
          }
        });
        $("#treeList1").html('')

        $.ajax({ // 获取左侧树状图
          type: 'GET',
          url: url + ":9006/AppConnection/departmentTree",
          success: function (data) {
            console.log(data)
            var data = data.data;
            tree({
              elem: '#treeList1',
              nodes: data,
              click: function (node) {
                console.log(node)
                if (node.pid == 0) {
                  orgId = ''
                } else {
                  orgId = node.id
                }
                $("#brand_sel").html("")
                var $select = $($(this)[0].elem).parents(".layui-form-select");
                $select.removeClass("layui-form-selected").find(".layui-select-title span").html(node.name).end().find("input:hidden[name='selectID']").val(node.id);
                $.ajax({
                  type: "POST",
                  url: url + ":9006/UserMonitorHistory/FindByOrgId",
                  data: JSON.stringify({
                    "orgId": orgId
                  }),
                  headers: {
                    "Content-Type": "application/json;charset=utf-8"
                  },
                  dataType: "json",
                  success: function (data) {
                    console.log(data)
                    var data = data.data;
                    var root = document.getElementById("brand_sel");
                    for (var i = 0; i < data.length; i++) {
                      var option = document.createElement("option");
                      // option.setAttribute("value", data[i].groupName);
                      // option.setAttribute("title", data[i].id);
                      option.setAttribute("dataid", data[i].id);
                      option.innerText = data[i].userName;
                      root.appendChild(option);
                      form.render("select");
                      // console.log(data[i].groupName)
                    }
                  }
                })
              }

            });
          }
        })
      })

      var searchInfo = ''
      $("#massage-btn").click(function () { // 点击搜索按钮
        searchInfo = $("#massage-input").val();
        $(".tips-box").html('');
        console.log(searchInfo)
        $.ajax({ // 点击搜索按钮,查询消息
          type: "POST",
          url: url + ':9006/Message/messageHistory',
          data: JSON.stringify({
            "userId": userId,
            "searchInfo": searchInfo
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function (data) {
            console.log(data)
            if (data.code === 200) {
              var data = data.data;
              var messageInfo = '';
              for (var i = 0; i < data.length; i++) {
                console.log(data[i])
                messageInfo += 
                '<div class="tips-list"><div>'+data[i].datetime+'</div><p>通知：'+
                    data[i].note+'</p></div><br/>'
              }
              // console.log(messageInfo)
              $(".tips-box").append(messageInfo)
            } else {
              layer.msg(data)
            }
          }
        })
      })

      element.on('tab(tab)', function (data) { //选项卡切换
        $(".tips-box").html('');
        if (data.index === 1) {
          $.ajax({ // 切换选项卡到发送记录,查询消息
            type: "POST",
            url: url + ':9006/Message/messageHistory',
            data: JSON.stringify({
              "userId": userId,
              "searchInfo": searchInfo
            }),
            headers: {
              "Content-Type": "application/json;charset=utf-8"
            },
            success: function (data) {
              console.log(data)
              if (data.code === 200) {
                var data = data.data;
                var messageInfo = '';
                for (var i = 0; i < data.length; i++) {
                  console.log(data[i])
                  messageInfo += 
                  '<div class="tips-list"><div>'+data[i].datetime+'</div><p>通知：'+
                    data[i].note+'</p></div><br/>'
                }
                // console.log(messageInfo)
                $(".tips-box").append(messageInfo)
              } else {
                layer.msg(data)
              }
            }
          })
        }
        if (data.index === 2) {
          $.ajax({ // 切换选项卡到发送记录,查询消息
            type: "POST",
            url: url + ':9006/Message/putMessageHistory',
            // url: 'http://10.1.1.213:9006/Message/putMessageHistory',
            data: JSON.stringify({
              "userId": userId,
              "searchInfo": searchInfo
            }),
            headers: {
              "Content-Type": "application/json;charset=utf-8"
            },
            success: function (data) {
              console.log(data)
              if (data.code === 200) {
                var data = data.data;
                var messageInfo = '';
                for (var i = 0; i < data.length; i++) {
                  console.log(data[i])
                  messageInfo +=
                  '<div class="tips-list"><div>'+data[i].datetime+'</div><p>通知：'+
                  data[i].note+'</p></div><br/>'
                }
                // console.log(messageInfo)
                $(".tips-box").append(messageInfo)
              }
            }
          })
        }
      });
      // var userId = getCookie("userId");

      console.log(userId)
      $.ajax({ // 按userid收消息
        type: "POST",
        url: url + ':9006/Message/putMessage/' + userId,
        // url: 'http://10.1.1.213:9006/Message/putMessage/' + userId,
        // data: JSON.stringify({ "userId": userId, "searchInfo": searchInfo }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function (data) {
          console.log(data)
          // active.setCookie('news', data.data.length)
          $('.layui-badge').html(data.data.length)
          if (data.data.length === 0) {
            $(".notice-box").remove();
          }
          if (data.code === 200) {
            var data = data.data;
            var messageInfo = '';
            for (var i = 0; i < data.length; i++) {
              console.log(data[i])
              messageInfo =
              '<div class="notice-list"><div>通知：</div><p class="notice-content">'+
              data[0].note+'</p><div class="notice-time">'+
                data[0].datetime+'</div></div>'
            }
            // console.log(messageInfo)
            $(".notice-box").append(messageInfo)
          } else {
            // layer.msg(data)
            console.log(data)
          }
        }
      })

      //移到右边
      $(document).on('click', '.adds', function () {
        if (!$("#brand_sel option").is(":selected")) {
          layer.alert("请选择移动的选项")
        } else {
          $('#brand_sel option:selected').appendTo('#choose_sel');
        }
      });
      //移到左边
      $(document).on('click', '.removes', function () {
        if (!$("#choose_sel option").is(":selected")) {
          layer.alert("请选择移动的选项")
        } else {
          $('#choose_sel option:selected').appendTo('#brand_sel');
        }
      });
      //双击选项
      $(document).on('dblclick', '#brand_sel', function () {
        $("option:selected", this).appendTo('#choose_sel');
      });
      //双击选项
      $(document).on('dblclick', '#choose_sel', function () {
        $("option:selected", this).appendTo('#brand_sel');
      });
      //全部移到右边
      $(document).on('click', '.add_alls', function () {
        $('#brand_sel option').appendTo('#choose_sel');
      });
      //全部移到左边
      $(document).on('click', '.remove_alls', function () {
        $('#choose_sel option').appendTo('#brand_sel');
      });


      function getCookie(name) { // 获取cookie 公共函数
        var strcookie = document.cookie; //获取cookie字符串
        var arrcookie = strcookie.split("; "); //分割
        //遍历匹配
        for (var i = 0; i < arrcookie.length; i++) {
          var arr = arrcookie[i].split("=");
          if (arr[0] == name) {
            return arr[1];
          }
        }
        return "";
      }


    }

  });
})()