vm = new Vue({
  el: '#home',
  data: function() {
    return{
      // 用户信息
      userData: {},
      // 许可证按钮
      button: [],
      // 许可信息,
      tableData: [],
      // 许可占用率
      linceValue: [],
      // 软件信息
      applicationData: [],
      // 软件使用率
      applicetionValue: [],
    }
  },
  mounted() {
    this.init()
  },
  methods: {
    // init
    init() {
      this.getUserData()
      this.getApplication()
      this.getButtonData()
      this.defaultData()
    },
    // 获取用户信息
    getUserData() {
      var _this = this
      $.ajax({
        // url: url + ':9006/HomePage/userInfo',
        url: url + ':9006/HomePage/userInfo',
        type: "get",
        async: true,
        success: function (data) {
          _this.userData = data.data
        }
      });
    },
    
    // 获取许可按钮
    getButtonData() {
      var _this = this
      $.ajax({
        // url: url + ':9006/HomePage/authorizationAppInfo',
        url: url + ':9006/HomePage/authorizationAppInfo',
        type: 'get',
        async: true,
        success: function(data) {
          _this.button = data.data
          _this.handleLince(data.data[0].id)
        }
      })
    },
    // 获取许可证信息
    handleLince(val) {
      var _this = this
      $.ajax({
        url: url + ':9006/HomePage/moduleInfo',
        type: 'post',
        data: {applicationId: val},
        asunc: true,
        success: function(data) {
          console.log(data)
          _this.tableData = data.data
          _this.linceValue = []
          for(var i = 0; i < data.data.length; i ++) {
            if(data.data[i].useRate == null){
              _this.linceValue.push('left:0%')
            }else{
              _this.linceValue.push('left:'+data.data[i].useRate+'%')
            }
          }
          console.log(_this.linceValue)
        }
      })
    },
    // 默认许可证信息
    defaultData(val) {
      console.log(val)
      var _this = this
      $.ajax({
        url: url + ':9006/HomePage/moduleInfo',
        type: 'post',
        data: {applicationId: val},
        asunc: true,
        success: function(data) {
          _this.tableData = data.data
        }
      })
    },
    // 获取软件信息
    getApplication() {
      var _this = this
      $.ajax({
        url: url + ':9006/HomePage/applicationReport',
        type: 'post',
        async: true,
        success: function(data) {
          console.log(data)
          _this.applicationData = data.data
          for( var i = 0; i < data.data.length; i ++) {
            _this.applicetionValue.push(parseInt(data.data[i].count / data.data[0].count * 100))
          }
          console.log(_this.applicetionValue)
        }
      })
    }
  }
})
