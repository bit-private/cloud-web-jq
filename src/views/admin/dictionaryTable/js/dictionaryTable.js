;(function() {
  //路径
  const baseURL_SON = url + ":9001/dictionary"
  const baseURL_SON1 = url + ":9001"
  //弹框
  var indexLayer

  layui.use(["element", "layer", "form", "upload"], function() {
    var element = layui.element,
      layer = layui.layer,
      upload = layui.upload,
      form = layui.form
    form.render()

    var active = {
      //初始化
      init: function() {
        //获取字典表数据
        this.getMessage()
        //获取软件图标数据
        this.getIconList()
        // 查询职位
        // this.getPosition()
      },
      //渲染页面
      Rendering: function(params, name, name1) {
          console.log(params)
        name = "<option grade='请选择域类型'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
        if (name1 == "positionList" && params) {
          for (var i = 0; i < params.length; i++) {
            //遍历，动态赋值
            name += '<option value="' + params[i].id + '" typeKey="' + params[i].typeKey + '"'
            name += ">" + params[i].name + "</option>"
          }
          $("select[name='" + name1 + "']").empty() //清空子节点
          $("select[name='" + name1 + "']").append(name) // 添加子节点
        } else {
          for (var i = 0; i < params.length; i++) {
            //遍历，动态赋值
            name += '<option value="' + params[i].id + '" typeKey="' + params[i].typeKey + '"'
            name += ">" + params[i].value + "</option>"
          }
          $("select[name='" + name1 + "']").empty() //清空子节点
          $("select[name='" + name1 + "']").append(name) // 添加子节点
        }
        // if(params){
        //     for(var i=0; i<params.length; i++){
        //         //遍历，动态赋值
        //         name +="<option value=\""+params[i].id+"\" typeKey=\""+params[i].typeKey+"\"";
        //         name += ">"+params[i].value+"</option>";
        //     }
        //     $('select[name=\''+name1+'\']').empty();//清空子节点
        //     $('select[name=\''+name1+'\']').append(name);  // 添加子节点
        // }
      },
      //获取信息
      getMessage: function() {
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        $.ajax({
          url: baseURL_SON + "/dataList",
          type: "post",
          async: true,
          success: function(data) {
            console.log(data)
            layer.closeAll("loading")
            if (data.code == 200) {
              active.Rendering(data.data.domainTypeList, "domainTypeList", "domainTypeList")
              active.Rendering(data.data.osTypeList, "osTypeList", "osTypeList")
              active.Rendering(data.data.appTypeList, "appTypeList", "appTypeList")
              active.Rendering(data.data.licensePatternList, "licensePatternList", "licensePatternList")
              active.Rendering(data.data.hardwareTypeList, "hardwareTypeList", "hardwareTypeList")
              active.Rendering(data.data.hardwareBrandList, "hardwareBrandList", "hardwareBrandList")
              active.Rendering(data.data.appConnectionTypeList, "appConnectionTypeList", "appConnectionTypeList")
              active.Rendering(data.data.loadLevelingList, "loadLevelingList", "loadLevelingList")
              active.Rendering(data.data.positionTypeList, "positionList", "positionList")
              form.render()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          }
        })
      },
      // // 获取职位
      // getPosition: function() {
      //     layer.load(0, {shade: [0.1]}); //0代表加载的风格，支持0-2//loading层
      //     $.ajax({
      //         url:url + ":9000/user/professionalTitleList",
      //         type:"post",
      //         async:true,
      //         success:function(data){
      //             console.log(data);
      //             layer.closeAll('loading');
      //             if(data.code==200){
      //                 active.Rendering(data.data,'positionList','positionList');
      //                 form.render();
      //             }
      //             else if(data.code==400){
      //                 layer.msg(data.message,{
      //                     icon:2,
      //                     closeBtn:1
      //                 })
      //             }
      //         }
      //     });
      // },
      //增加信息
      addMessage: function(name, typeKey, value) {
        $.ajax({
          url: baseURL_SON + "/saveDictionary",
          // url:"http://10.1.3.150:9001/dictionary/saveDictionary",
          type: "post",
          async: true,
          data: { name, typeKey, value },
          success: function(data) {
            console.log(JSON.stringify(data))
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              active.getMessage()
            //   avtive.getPosition()
              form.render()
            } else if (data.code == 400) {
              layer.closeAll("loading")
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            window.location.href = "../../error/error.html"
            console.log(data.message)
          }
        })
      },
      addmess: function(name, type, msg, level) {
        if ($.trim($("input[name='" + name + "']").val())) {
          layer.load(0, { shade: [0.1] })
          active.addMessage($.trim($("input[name='" + name + "']").val()), type, level)
          $("input[name='" + name + "']").val("")
          $("input[name='level']").val('')
        } else {
          layer.msg(msg, {
            icon: 2,
            closeBtn: 1
          })
        }
      },
      //删除信息
      rmMessage: function(id) {
        $.ajax({
          url: baseURL_SON + "/deleteDictionary",
          // url:"http://10.1.3.150:9001/dictionary/deleteDictionary",
          type: "post",
          async: true,
          data: { id },
          success: function(data) {
            console.log(JSON.stringify(data))
            if (data.code == 200) {
              layer.msg(data.message, {
                icon: 1,
                closeBtn: 1
              })
              active.getMessage()
            //   avtive.getPosition()
              form.render()
            } else if (data.code == 400) {
              layer.closeAll("loading")
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            window.location.href = "../../error/error.html"
            console.log(data.message)
          }
        })
      },
      rmmess: function(name, msg) {
        if ($.trim($("select[name='" + name + "']").val())) {
          console.log(name)
          layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
          active.rmMessage($.trim($("select[name='" + name + "']").val()))
          $("input[select='" + name + "']").val("")
        } else {
          layer.msg(msg, {
            icon: 2,
            closeBtn: 1
          })
        }
      },

      //获取软件图标
      getIconList: function() {
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        $.ajax({
          url: baseURL_SON1 + "/icon/getIconList",
          type: "post",
          async: true,
          success: function(data) {
            // console.log(data);
            // var optionString = "<option grade=\'请选择一级属性\'>--请选择一级属性--</option>";
            layer.closeAll("loading")
            if (data.code == 200) {
              // console.log(data)
              var optionString = "<option grade='请选择软件分类系统'  selected = 'selected'  class=\"layui-select-tips\" placeholder=\"请选择\"></option>"
              if (data.data) {
                //判断
                // console.log(data.data)
                for (var i = 0; i < data.data.length; i++) {
                  //遍历，动态赋值
                  optionString += '<option grade="' + data.data[i].id + '" value="' + data.data[i].id + '"'
                  optionString += ">" + data.data[i].name + "</option>" //动态添加数据
                }
                $("select[name=uploadImg]").empty() //清空子节点
                $("select[name=uploadImg]").append(optionString) // 添加子节点
              }
              form.render()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
            }
          }
        })
      },
      //删除软件图标
      deleteImg: function(id) {
        $.ajax({
          url: baseURL_SON1 + "/icon/deleteImg",
          type: "post",
          async: true,
          data: { id },
          success: function(data) {
            // console.log(data);
            layer.closeAll("loading")
            if (data.code == 200) {
              layer.msg("删除成功", {
                icon: 1,
                closeBtn: 1
              })
              active.getIconList()
            } else if (data.code == 400) {
              layer.msg(data.message, {
                icon: 2,
                closeBtn: 1
              })
              active.getIconList()
            }
          },
          error: function(data) {
            layer.closeAll("loading")
            window.location.href = "../../error/error.html"
            console.log(data.message)
          }
        })
      }
    }
    active.init()

    //增加域类型
    $(".addDomainTypeList").on("click", function() {
      active.addmess("domainTypeList", "domain_type", "请输入域类型")
    })
    //增加操作系统
    $(".addOsTypeList").on("click", function() {
      active.addmess("osTypeList", "os_type", "请输入操作系统")
    })
    //增加软件分类
    $(".addAppTypeList").on("click", function() {
      active.addmess("appTypeList", "app_type", "请输入软件类型")
    })
    //增加许可分类
    $(".addLicensePatternList").on("click", function() {
      active.addmess("licensePatternList", "license_pattern", "请输入许可类型")
    })
    //增加资源类型
    $(".addHardwareTypeList").on("click", function() {
      active.addmess("hardwareTypeList", "hardware_type", "请输入资源类型")
    })
    //增加品牌
    $(".addHardwareBrandList").on("click", function() {
      active.addmess("hardwareBrandList", "hardware_brand", "请输入品牌")
    })
    //增加连接方式
    $(".addAppConnectionTypeList").on("click", function() {
      active.addmess("appConnectionTypeList", "app_connection_type", "请输入连接方式")
    })
    //增加负载策略
    $(".addLoadLevelingList").on("click", function() {
      active.addmess("loadLevelingList", "load_leveling", "请输入负载策略")
    })
    // 增加职位
    $(".addPositionList").on("click", function() {
      active.addmess("positionList", "position_type", "请输入职位", $.trim($("input[name='level']").val()))
    })
    //上传图标
    upload.render({
      elem: ".addUploadImg",
      url: baseURL_SON1 + "/icon/uploadImg",
      method: "post",
      accept: "images",
      before: function(obj) {
        //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        console.log(obj, this)
        this.data = { name: $("input[name=uploadImg]").val() }
        layer.load(0, { shade: [0.1] }) //上传loading
        this.token = getCookie("token")
      },
      done: function(res) {
        // console.log(3)
        // console.log(res)
        if (res.code == 200) {
          layer.closeAll("loading")
          layer.msg("上传成功", { icon: 1, closeBtn: 1 })
          $("input[name=uploadImg]").val("")
          active.getIconList()
        } else if (res.code == 400) {
          layer.closeAll("loading")
          layer.msg(res.message, { icon: 2, closeBtn: 1 })
          active.getIconList()
        }

        // alert("上传成功")
        //上传完毕回调
      },
      error: function(res) {
        console.log(getCookie("token"))
        layer.closeAll("loading")
        alert("上传失败")
        //请求异常回调
      }
    })

    //删除域类型
    $(".rmDomainTypeList").on("click", function() {
      active.rmmess("domainTypeList", "请选择要删除的域类型")
    })
    //删除操作系统
    $(".rmOsTypeList").on("click", function() {
      active.rmmess("osTypeList", "请选择要删除的操作系统")
    })
    //删除软件类型
    $(".rmAppTypeList").on("click", function() {
      active.rmmess("appTypeList", "请选择要删除的软件类型")
    })
    //删除许可分类
    $(".rmLicensePatternList").on("click", function() {
      active.rmmess("licensePatternList", "请选择要删除的许可分类")
    })
    //删除资源类型
    $(".rmHardwareTypeList").on("click", function() {
      active.rmmess("hardwareTypeList", "请选择要删除的资源类型")
    })
    //删除品牌
    $(".rmHardwareBrandList").on("click", function() {
      active.rmmess("hardwareBrandList", "请选择要删除的品牌")
    })
    //删除连接方式
    $(".rmAppConnectionTypeList").on("click", function() {
      active.rmmess("appConnectionTypeList", "请选择要删除的连接方式")
    })
    //删除负载策略
    $(".rmLoadLevelingList").on("click", function() {
      active.rmmess("loadLevelingList", "请选择要删除的负载策略")
    })
    //删除职位
    $(".rmPositionList").on("click", function() {
      active.rmmess("positionList", "请选择要删除的职位")
    })
    //删除软件图标
    $(".rmUploadImg").on("click", function() {
      // console.log($('select[name=uploadImg]').val())
      if ($.trim($("select[name=uploadImg]").val()) == "") {
        layer.msg("请选择要删除的软件图标", {
          icon: 2,
          closeBtn: 1
        })
      } else {
        layer.load(0, { shade: [0.1] }) //0代表加载的风格，支持0-2//loading层
        active.deleteImg($("select[name=uploadImg]").val())
      }
    })
    //tip提示层
    // $('input[name=osTypeList]').on('focus',function () {
    //     layer.tips('例如:windows </br>' +
    //         'linux', $('input[name=osTypeList]'), {
    //         tips: [2,'#FF5722'],
    //         tipsMore: false,
    //         closeBtn:1,
    //         time:0
    //     });
    // });
    $("input[name=osTypeList]").on("mouseover", function() {
      indexLayer = layer.tips("提示 填写操作系统，例如：RedHat 6；Centos 6；等", $("input[name=osTypeList]"), {
        tips: [2, "#FF5722"],
        tipsMore: false,
        closeBtn: 1,
        time: 0
      })
    })
    $("input[name=osTypeList]").on("mouseout", function() {
      layer.close(indexLayer)
    })
  })
})()
