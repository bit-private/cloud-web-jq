vm = new Vue({
    el: '#app',
    data: function () {
        return {
            options: [{
                value: 'nfs',
                label: 'nfs'
            }, {
                value: 'cifs',
                label: 'cifs'
            }],
            systemUserDefaultPassword: '',
            userDefaultPasswordkey: '',
            userDefaultPassword: "",
            systemUserDefaultPasswordkey: '',
            radio: 1,
            disabled: false,
            failNumkey: '',
            failNum: '',
            unlockH: '',
            unlockkey: '',
            unlockM: '',
            overtimekey: '',
            overtimeH: '',
            overtimeM: '',
            automatickey: '',
            automaticH: '',
            automaticM: '',
            CPUalertkey: '',
            CPUalert: '',
            GPUalertkey: '',
            GPUalert: '',
            memoryalertkey: '',
            memoryalert: '',
            dataMager: 'no',
            dataMagerInputs: false,
            IP: '',
            path: '',
            space: '',
            IPalertkey: '',
            pathalertkey: '',
            spacealertkey: '',
            dataMageralertkey: '',

            suffix: '',
            sizeLimitation: '',
            softwareType: '',
            softwareTypes: [],
            softwareTypeList: {},
            suffixKey: '',
            sizeLimitationKey: '',


            Approval: false,
            workOrder: false,
            severError: false,
            overload: false,
            continuous: '',
            messageDelivery: '',
            messageReceiver: [],
            softwareAlarm: '',
            sendMessage: '',
            messageReceiverList: [{ value: '1', label: '设备管理员' }, { value: '0', label: '系统管理员' }],

            Approvalalert: "",
            workOrderalert: "",
            severErroralert: "",
            overloadalert: "",
            continuousalert: '',
            messageDeliveryalert: '',
            messageReceiveralert: '',
            softwareAlarmalert: '',
            sendMessagealert: '',
        }
    },
    methods: {
        addSoftwareType() {//添加软件类型
            if (this.softwareType == "") {
                layer.msg("请选择软件", {
                    icon: 2,
                    closeBtn: 1
                });
                return
            }
            for (let key in this.softwareTypeList) {
                if (this.softwareTypeList[key].name == this.softwareType) {
                    layer.msg("同一种软件只能添加一次", {
                        icon: 2,
                        closeBtn: 1
                    });
                    return
                }
            }
            let obj = JSON.parse(JSON.stringify(this.softwareTypeList))//深拷贝对象
            let name = ''
            for (let i = 0; i < this.softwareTypes.length; i++) {
                if (this.softwareTypes[i].name == this.softwareType) {
                    name = this.softwareTypes[i].id
                }
            }
            this.$set(this.softwareTypeList, this.transformation(obj), { name: this.softwareType, softName: '', select: name, IP: "", path: '', space: "", disabled: false, agreement: '' })
        console.log(this.softwareTypeList,'this.softwareTypeList')
        },
        transformation: function (obj) {
            let arr = Object.keys(obj)//对象转化数组
            let lastIndex = ''
            let join = ''
            if (arr.length == 0) {
                join = 'index_0'
            } else {
                lastIndex = arr[arr.length - 1].split("_")[1]//获取最后一项数字
                join = 'index_' + (parseInt(lastIndex) + 1)//将要新增进对象的key值
            }
            return join
        },
        delSoftwareEdition(key) {//删除软件类型
            Vue.delete(this.softwareTypeList, key);//vue方法
        },
        init() {
            this.getdata()
        },
        getdataListadc() {
            let _that = this
            $.ajax({
                url: url + ':9006/paramConfig/getAppList',
                type: "post",
                headers: {
                    "token": token,
                },
                success: function (data) {
                    _that.softwareTypes = data.data
                }
            });
        },
        getdata() {
            this.$set(this, 'softwareTypeList', {})
            let _that = this
            $.ajax({
                url: url + ':9006/paramConfig/list',
                type: "get",
                // contentType: "application/json;charset=utf-8",
                headers: {
                    "token": token,
                },
                dataType: 'json',
                // data: {},
                success: function (data) {
                    console.log(data)
                    let arr = data.data
                    console.log(data.data, 'data.data')
                    _that.failNumkey = arr.errorCount.typeKey
                    _that.failNum = arr.errorCount.value
                    _that.systemUserDefaultPasswordkey = arr.systemUserDefaultPassword.typeKey
                    _that.userDefaultPasswordkey = arr.userDefaultPassword.typeKey
                    _that.unlockkey = arr.unlockTime.typeKey
                    _that.unlockH = Math.floor(parseInt(arr.unlockTime.value) / 60)
                    _that.unlockM = Math.floor(parseInt(arr.unlockTime.value) % 60)
                    _that.overtimekey = arr.logoutTime.typeKey
                    _that.overtimeH = Math.floor(parseInt(arr.logoutTime.value) / 60)
                    _that.overtimeM = Math.floor(parseInt(arr.logoutTime.value) % 60)
                    _that.automatickey = arr.idleTimeCloseTokinaga.typeKey
                    _that.automaticH = Math.floor(parseInt(arr.idleTimeCloseTokinaga.value) / 60)
                    _that.automaticM = Math.floor(parseInt(arr.idleTimeCloseTokinaga.value) % 60)
                    _that.closeskey = arr.idleTimeClose.typeKey
                    _that.radio = arr.idleTimeClose.value
                    _that.CPUalertkey = arr.cpu.typeKey
                    _that.CPUalert = arr.cpu.value
                    _that.GPUalertkey = arr.gpu.typeKey
                    _that.GPUalert = arr.gpu.value
                    _that.memoryalertkey = arr.ram.typeKey
                    _that.memoryalert = arr.ram.value
                    _that.userDefaultPassword = arr.userDefaultPassword.value
                    _that.systemUserDefaultPassword = arr.systemUserDefaultPassword.value
                    _that.workOrder = arr.workOrderReminder.value == 0 ? false : true
                    _that.workOrderalert = arr.workOrderReminder.typeKey

                    _that.Approval = arr.resourceApprovalReminder.value == 0 ? false : true
                    _that.Approvalalert = arr.resourceApprovalReminder.typeKey

                    _that.severError = arr.serverNodeFaultReminder.value == 0 ? false : true
                    _that.severErroralert = arr.serverNodeFaultReminder.typeKey

                    _that.continuous = arr.faultAlarmDuration.value
                    _that.continuousalert = arr.faultAlarmDuration.typeKey

                    _that.messageDelivery = arr.faultSendInterval.value
                    _that.messageDeliveryalert = arr.faultSendInterval.typeKey

                    if (arr.faultMessageReceiver.value == 2) {
                        _that.messageReceiver = ['0', '1']
                    } else {
                        _that.messageReceiver = [arr.faultMessageReceiver.value]
                    }
                    console.log(_that.messageReceiver)
                    // _that.messageReceiver=arr.faultMessageReceiver.value
                    _that.messageReceiveralert = arr.faultMessageReceiver.typeKey

                    _that.overload = arr.softwareServerOverloadReminder.value == 0 ? false : true
                    _that.overloadalert = arr.softwareServerOverloadReminder.typeKey

                    _that.softwareAlarm = arr.overloadAlarmDuration.value
                    _that.softwareAlarmalert = arr.overloadAlarmDuration.typeKey

                    _that.sendMessage = arr.overloadSendInterval.value
                    _that.sendMessagealert = arr.overloadSendInterval.typeKey
                    // _that.dataMager=arr.panOpen.value==1?'yes':'no'
                    // _that.dataMagerInputs=arr.panOpen.value==1?false:true
                    // _that.IP=arr.panStorageServerIp.value
                    // _that.path=arr.panStoragePath.value
                    // _that.space=arr.panSpaceSize.value
                    //
                    // _that.dataMageralertkey=arr.panOpen.typeKey
                    // _that.IPalertkey=arr.panStorageServerIp.typeKey
                    // _that.pathalertkey=arr.panStoragePath.typeKey
                    // _that.spacealertkey=arr.panSpaceSize.typeKey
                    _that.suffix = arr.panNotAllowedType.value
                    _that.suffixKey = arr.panNotAllowedType.typeKey
                    _that.sizeLimitation = arr.panDownloadLimit.value
                    _that.sizeLimitationKey = arr.panDownloadLimit.typeKey

                    for (let i = 0; i < arr.panConfigInfo.length; i++) {
                        _that.$set(_that.softwareTypeList, "index_" + i, { name: arr.panConfigInfo[i].applicationName, softName: arr.panConfigInfo[i].id, select: arr.panConfigInfo[i].applicationId, IP: arr.panConfigInfo[i].storageServerIp, path: arr.panConfigInfo[i].storagePath, space: arr.panConfigInfo[i].spaceSize, disabled: true, agreement: arr.panConfigInfo[i].protocolType })
                    }
                }
            });
        },
        closes() {
            if (this.radio == 1) {
                this.disabled = false
            } else if (this.radio == 0) {
                this.disabled = true
            }

        },
        closeDataMager() {
            if (this.dataMager == 'yes') {
                this.dataMagerInputs = false
            } else if (this.dataMager == 'no') {
                this.dataMagerInputs = true
            }
        },
        save() {
            let _that = this
            let a = true
            if(this.userDefaultPassword.match(/^[A-Za-z0-9]+$/g)){
            }else{
                layer.msg('密码不能包含中文，空格', {
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            if (this.radio == 1) {
                a = true
                if (_that.failNum === '' || _that.unlockH === '' || _that.unlockM === '' || _that.overtimeH === '' || _that.overtimeM === '' || _that.automaticH === '' || _that.automaticM === '' || _that.CPUalert === '' || _that.GPUalert === '' || _that.memoryalert === '') {
                    layer.msg('请完善信息', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            } else if (this.radio === 0) {
                a = false
                if (_that.failNum === '' || _that.unlockH === '' || _that.unlockM === '' || _that.overtimeH === '' || _that.overtimeM === '' || _that.CPUalert === '' || _that.GPUalert === '' || _that.memoryalert === '') {
                    layer.msg('请完善信息', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.failNum.length > 0) {
                if (this.failNum < 1 || this.failNum % 1 !== 0 || this.failNum > 10 || String(this.failNum).replace(/\s+/g, "") == '') {
                    layer.msg('登录允许密码连续错误次数应为1-10之的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.unlockH.length > 0) {
                if (this.unlockH < 0 || this.unlockH % 1 !== 0 || this.unlockH > 24 || String(this.unlockH).replace(/\s+/g, "") == '') {
                    layer.msg('自动解锁时间的小时数应为0-24之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.unlockM.length > 0) {
                if (this.unlockM < 1 || this.unlockM > 59 || this.unlockM % 1 !== 0 || String(this.unlockM).replace(/\s+/g, "") == '') {
                    layer.msg('自动解锁时间的分钟数应为1-60之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }

            if (this.overtimeH.length > 0) {
                if (this.overtimeH < 0 || this.overtimeH % 1 !== 0 || this.overtimeH > 24 || String(this.overtimeH).replace(/\s+/g, "") == '') {
                    layer.msg('自动退出的超时时间的小时数应为0-24之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }

            if (this.overtimeM.length > 0) {
                if (this.overtimeM < 1 || this.overtimeM > 59 || this.overtimeM % 1 !== 0) {
                    layer.msg('自动退出的超时时间的分钟数应为1-60之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.automaticH.length > 0 && this.radio == 1) {
                if (this.automaticH < 1 || this.automaticH % 1 !== 0 || this.automaticH > 48 || String(this.automaticH).replace(/\s+/g, "") == '') {
                    layer.msg('自动关闭的监控时长的小时数应为1-48之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.automaticM.length > 0) {
                if (this.automaticM < 1 || this.automaticM > 59 || this.automaticM % 1 !== 0) {
                    layer.msg('自动关闭的监控时长的分钟数应为1-60之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.CPUalert.length > 0) {
                if (this.CPUalert < 1 || this.CPUalert > 100 || this.CPUalert % 1 !== 0 || String(this.CPUalert).replace(/\s+/g, "") == '') {
                    layer.msg('CPU报警阈值应为1-100之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.GPUalert.length > 0) {
                if (this.GPUalert < 1 || this.GPUalert > 100 || this.GPUalert % 1 !== 0 || String(this.GPUalert).replace(/\s+/g, "") == '') {
                    layer.msg('GPU报警阈值应为1-100之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }

            if (this.memoryalert.length > 0) {
                if (this.memoryalert < 1 || this.memoryalert > 100 || this.memoryalert % 1 !== 0 || String(this.memoryalert).replace(/\s+/g, "") == '') {
                    layer.msg('内存报警阈值应为1-100之间的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }

            if (this.sizeLimitation != '') {
                if (this.sizeLimitation < 0 || this.sizeLimitation % 1 != 0) {
                    layer.msg('下载文件大小限制应为大等于0的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            for (let key in this.softwareTypeList) {
                console.log(this.softwareTypeList[key])
                var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
                if (this.softwareTypeList[key].IP == '') {
                    layer.msg("请填写存储服务器IP", {
                        icon: 2,
                        closeBtn: 1
                    });
                    return
                }
                console.log(!reg.test(this.softwareTypeList[key].IP))
                if (!reg.test(this.softwareTypeList[key].IP)) {
                    layer.msg("存储服务器IP格式不正确", {
                        icon: 2,
                        closeBtn: 1
                    });
                    return
                }
                if (this.softwareTypeList[key].path == '') {
                    layer.msg("请填写存储路径", {
                        icon: 2,
                        closeBtn: 1
                    });
                    return
                }
                if (this.softwareTypeList[key].path.substring(0, 1) != '/') {
                    layer.msg('存储路径格式不正确', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
                if (this.softwareTypeList[key].agreement == '') {
                    layer.msg("请选择协议", {
                        icon: 2,
                        closeBtn: 1
                    });
                    return
                }
                if (this.softwareTypeList[key].space == '') {
                    layer.msg("空间大小不能为空", {
                        icon: 2,
                        closeBtn: 1
                    });
                    return
                }
                console.log(this.softwareTypeList[key].space)
                if (this.softwareTypeList[key].space <= 0 || this.softwareTypeList[key].space % 1 !== 0 || String(this.softwareTypeList[key].space).replace(/\s+/g, "") == '') {
                    layer.msg('空间大小应为大于0的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.dataMager == 'yes') {
                if (_that.IP == '' || _that.path == '' || _that.space == '') {
                    layer.msg('请完善信息', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.severError) {
                if (this.continuous < 1 || this.continuous % 1 !== 0 || String(this.continuous).replace(/\s+/g, "") == '') {
                    layer.msg('持续报警时间应为大于0的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
                if (this.messageDelivery < 1 || this.messageDelivery % 1 !== 0 || String(this.messageDelivery).replace(/\s+/g, "") == '') {
                    layer.msg('短信发送间隔应为大于0的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
                if (this.messageReceiver.length == 0) {
                    layer.msg('请选择短信接收人', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            if (this.overload) {
                if (this.softwareAlarm < 1 || this.softwareAlarm % 1 !== 0 || String(this.softwareAlarm).replace(/\s+/g, "") == '') {
                    layer.msg('持续报警时间应为大于0的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
                if (this.sendMessage < 1 || this.sendMessage % 1 !== 0 || String(this.sendMessage).replace(/\s+/g, "") == '') {
                    layer.msg('短信发送间隔应为大于0的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    return
                }
            }
            var layerLoad = layer.load(0, { shade: false })
            let changeArr = []
            for (let key in this.softwareTypeList) {
                changeArr.push({
                    'applicationId': this.softwareTypeList[key].select, 'storageServerIp': this.softwareTypeList[key].IP,
                    'storagePath': this.softwareTypeList[key].path, "spaceSize": this.softwareTypeList[key].space,
                    'protocolType': this.softwareTypeList[key].agreement, 'id': this.softwareTypeList[key].softName
                })
            }
            console.log(changeArr)
            let data = [
                {
                    typeKey: this.failNumkey,
                    value: this.failNum
                },
                {
                    typeKey: this.userDefaultPasswordkey,
                    value: this.userDefaultPassword
                },
                {
                    typeKey: this.systemUserDefaultPasswordkey,
                    value: this.systemUserDefaultPassword
                },
                {
                    typeKey: this.unlockkey,
                    value: parseInt(this.unlockH * 60) + parseInt(this.unlockM),
                },
                {
                    typeKey: this.overtimekey,
                    value: parseInt(this.overtimeH * 60) + parseInt(this.overtimeM),
                },
                {
                    typeKey: this.automatickey,
                    value: parseInt(this.automaticH * 60) + parseInt(this.automaticM)
                },
                {
                    typeKey: this.closeskey,
                    value: this.radio
                },
                {
                    typeKey: this.CPUalertkey,
                    value: this.CPUalert
                },
                {
                    typeKey: this.GPUalertkey,
                    value: this.GPUalert
                },
                {
                    typeKey: this.memoryalertkey,
                    value: this.memoryalert
                },
                {
                    typeKey: this.suffixKey,
                    value: this.suffix
                },
                {
                    typeKey: this.sizeLimitationKey,
                    value: this.sizeLimitation
                },
                {
                    typeKey: "panConfigInfo",
                    value: changeArr
                },
                {
                    typeKey: this.Approvalalert,
                    value: this.Approval == true ? '1' : '0'
                },
                {
                    typeKey: this.workOrderalert,
                    value: this.workOrder == true ? '1' : '0'
                },
                {
                    typeKey: this.severErroralert,
                    value: this.severError == true ? '1' : '0'
                },
                {
                    typeKey: this.overloadalert,
                    value: this.overload == true ? '1' : '0'
                },
                {
                    typeKey: this.continuousalert,
                    value: this.continuous
                },
                {
                    typeKey: this.messageDeliveryalert,
                    value: this.messageDelivery
                },
                {
                    typeKey: this.messageReceiveralert,
                    value: this.messageReceiver.length == 2 ? '2' : this.messageReceiver[0]
                },
                {
                    typeKey: this.softwareAlarmalert,
                    value: this.softwareAlarm
                },
                {
                    typeKey: this.sendMessagealert,
                    value: this.sendMessage
                }
            ]
            var _this=this
            $.ajax({
                url: url + ':9006/paramConfig/edit',
                type: "POST",
                contentType: "application/json;charset=utf-8",
                headers: {
                    "token": token,
                },
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (data) {
                    layer.close(layerLoad)
                    if (data.code == 200) {
                        _this.getdata()
                        _this.getdataListadc()
                        _this.softwareType=''
                        layer.msg(data.message, {
                            icon: 1,
                            closeBtn: 1
                        });
                        _that.getdata()
                    } else {
                        layer.msg(data.message, {
                            icon: 7,
                            closeBtn: 1
                        });
                    }
                }
            });

        },
        unlockChange() {
            // if(this.unlockM.length>0){
            //     if (this.unlockM<1||this.unlockM>59||this.unlockM%1!==0) {
            //      layer.msg('请输入0-60之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.unlockM = ''
            //  }
            // }
        },
        overtimeChange() {
            // if(this.overtimeM.length>0){
            //     if (this.overtimeM<1||this.overtimeM>59||this.overtimeM%1!==0) {
            //      layer.msg('请输入0-60之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.overtimeM = ''
            //  }
            // }
        },
        automaticChange() {
            // if(this.automaticM.length>0){
            //     if (this.automaticM<1||this.automaticM>59||this.automaticM%1!==0) {
            //      layer.msg('请输入0-60之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.automaticM = ''
            //  }
            // }
        },
        CPUChange() {
            // if(this.CPUalert.length>0){
            //     if (this.CPUalert<0||this.CPUalert>100||this.CPUalert%1!==0) {
            //      layer.msg('请输入0-100之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.CPUalert = ''
            //  }
            // }
        },
        GPUChange() {
            // if(this.GPUalert.length>0){
            //     if (this.GPUalert<0||this.GPUalert>100||this.GPUalert%1!==0) {
            //      layer.msg('请输入0-100之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.GPUalert = ''
            //  }
            // }
        },
        memoryChange() {
            // if(this.memoryalert.length>0){
            //     if (this.memoryalert<0||this.memoryalert>100||this.memoryalert%1!==0) {
            //      layer.msg('请输入0-100之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.memoryalert = ''
            //  }
            // }
        },
        IPChange(param, key) {
            var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
            if (!reg.test(param)) {
                layer.msg('请输入正确的IP', {
                    icon: 7,
                    closeBtn: 1
                });
                // this.IP=''
                // this.softwareTypeList[key].IP=''

            }
        },
        pathChange(param, key) {
            if (param.substring(0, 1) != '/') {
                layer.msg('请填写正确路径', {
                    icon: 7,
                    closeBtn: 1
                });
            }
        },
        sizeChange(param) {
            if (param == '') {
                return
            }
            if (param < 0 || param % 1 != 0 || param.replace(/\s+/g, "") == '') {
                layer.msg('请输入大于0的整数', {
                    icon: 7,
                    closeBtn: 1
                });
            }
        },
        spaceChange(param, key) {
            if (param <= 0 || param % 1 !== 0 || param.replace(/\s+/g, "") == '') {
                layer.msg('空间大小应为大于0的整数', {
                    icon: 7,
                    closeBtn: 1
                });
            }
        },
        failTime() {
            if (this.failNum.length > 0) {
                if (this.failNum < 1 || this.failNum % 1 !== 0 || this.failNum > 10) {
                    layer.msg('请输入0-10之的整数', {
                        icon: 7,
                        closeBtn: 1
                    });
                    this.failNum = ''
                }
            }
        },
        unlockHour() {
            // if(this.unlockH.length>0){
            //     if (this.unlockH<0||this.unlockH%1!==0 ||this.unlockH >24) {
            //      layer.msg('请输入0-24之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.unlockH = ''
            //  }
            // }
        },
        overtimeHour() {
            // if(this.overtimeH.length>0){
            //     if (this.overtimeH<0||this.overtimeH%1!==0 || this.overtimeH>24) {
            //      layer.msg('请输入0-24之间的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.overtimeH = ''
            //  }
            // }
        },
        automaticHour() {
            // if(this.automaticH.length>0){
            //     if (this.automaticH<0||this.automaticH%1!==0 || this.automaticH>48) {
            //      layer.msg('请输入0-48的整数', {
            //          icon: 7,
            //          closeBtn:1
            //      });
            //      this.automaticH = ''
            //  }
            // }
        },

    },
    computed: {},
    mounted() {
        this.getdataListadc()
        this.init()
    },
})
