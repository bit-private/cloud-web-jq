
vm = new Vue({
  el: '#app',
  data: function () {
    return {
      ruleForm:{
        showTitle:1,
        tipsTitle: 0,
        noticeName:'',
        noticeDesc:'',
      },
      
      datas:{},
      loginTitle:'',
      submitSerch:'',//搜索框
      showDialog: false,//弹框显隐
      tableData:[],
      tableDataPage:1,
      tableDataTotal:0,
      title:'新建',
      dialogDisabled:false,
      multipleSelectionRows:[],
      rules: {
        noticeName: [
          {required: true, message: '请输入活动名称', trigger: 'blur'},
        ],
      }
    }
  },
  methods: {
    ownClosed(){
      this.dialogDisabled=false
      this.ruleForm.showTitle=1
      this.ruleForm.tipsTitle=0
      this.ruleForm.noticeName=''
      this.ruleForm.noticeDesc=''
    },
    getNotice() {
      var _that = this
      let data = {
        currentPage:_that.tableDataPage,
        showCount:10,
        pd:{
            title:""
        }
      }
      $.ajax({
        url: url + ':9006/notification/getList',
        type: "post",
        contentType: "application/json;charset=utf-8",
        //  headers: {
        //   "token": token,
        //  },
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data) {
          console.log(data)
          _that.tableData = data.data.content
          _that.tableDataTotal = data.data.pageCount
          _that.getNoticefirst()
          // _that.userData = data.data
        }
      });
    },
    getNoticefirst() {
      var _that = this
      $.ajax({
        url: url + ':9006/notification/getDefault',
        type: "post",
         headers: {
          "token": token,
         },
        dataType: 'json',
        success: function (data) {
          console.log(data)
          if(data.data!=null){
            _that.loginTitle = data.data.title
          }else{
            _that.loginTitle=''
          }
        }
      });
    },
    init(){
      let _that=this
      _that.getNotice()
      _that.getNoticefirst()

      let userId = this.getCookie("userId"); // 获取用户id
      // $.ajax({
      //   url: url+':9001/examineAndApproveResource/myApplyDetail',
      //   type: 'post',
      //   async: false,
      //   data: {currentUserId:userId,
      //     page:_that.tableDataPage,size:10,conditions:this.submitSerch},
      //   success: function(data) {
        //  let  data= {data:{list:[{
        //     num:'1',
        //     title:'啊啊啊啊啊',
        //     time:'2019-01-01',
        //   }],count:1}}
        //   _that.$set(_that,"tableData",data.data.list)
        //   _that.tableDataTotal=data.data.count
      //   },
      //   error: function(data) {
      //     console.log(data)
      //   }
      // })
    },
    sureCutOut(){
      // let userId = this.getCookie("userId"); // 获取用户id
      let arr=[]
      console.log(this.multipleSelectionRows)
      for(let i=0;i<this.multipleSelectionRows.length;i++){
        arr.push(this.multipleSelectionRows[i].id)
      }
      let _that=this
      console.log(arr)
      $.ajax({
        url: url+':9006/notification/delete ',
        type: 'post',
        async: false,
        headers: {
          "token": token,
          // "Content-Type": "application/json;charset=utf-8"
         },
        data: {ids :arr.join(',')},
        success: function(data) {
            _that.$message({
              message: '删除成功',
              type: 'success'
            });
            _that.getNotice()
        },
        error: function(data) {
          console.log(data)
        }
      })
    },
    searchInput(params){
      var _that = this
      let data = {
        currentPage:1,
        showCount:10,
        pd:{
            title:_that.submitSerch
        }
      }
      $.ajax({
        url: url + ':9006/notification/getList',
        type: "post",
        contentType: "application/json;charset=utf-8",
         headers: {
          "token": token,
         },
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data) {
          console.log(data)
          _that.tableData = data.data.content
          console.log(_that.tableData)
          _that.tableDataTotal = data.data.pageCount
        }
      });
    },
    handleCurrentChange(val){
      let _that = this
      console.log(val)
        this.tableDataPage=val
        this.getNotice()
        
    },
    handleSelectChangeLeft(rows){
      this.multipleSelectionRows=rows;
    },
    handleOpen(key, keyPath) {
      console.log(key, keyPath);
    },
    handleClose(key, keyPath) {
      console.log(key, keyPath);
    },
    handleEdit(index, row) {
      console.log(row)
      console.log(index)
      this.title='详情'
      this.dialogDisabled=true
      let _that=this;
         let data={data:{
             num:row.state,
             name:row.title,
             type:row.content,
             stress:row.stress
           }}
           console.log(data.data)
        _that.showDialog=true
        _that.ruleForm.showTitle=data.data.num
        _that.ruleForm.noticeName=data.data.name
        _that.ruleForm.noticeDesc=data.data.type
        _that.ruleForm.tipsTitle=data.data.stress
    },
    x(){//清空搜索
      this.submitSerch = ''
      this.tableDataPage=1
      this.init();
    },
    cutOut(done) {
      let _that=this
      if(this.multipleSelectionRows.length==0){
        this.$message({
          message: '请至少选择一项删除',
          type: 'error'
        });
        return;
      }
      this.$confirm('您确定要删除?', '删除', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        // type: 'warning'
      }).then(() => {
        _that.sureCutOut();
      }).catch(() => {
      });
    },
    closeBtn () {
      this.showDialog = false
    },
    newly(params){//新建请求
      if(params==='edit'){
        this.isEdit=true
        let _that=this
        console.log(this.multipleSelectionRows)
        // _that.datas = this.multipleSelectionRows[0]
        if(this.multipleSelectionRows.length==1){
          this.title="编辑"
          _that.showDialog = true
              let data={
                data:{
                  num:this.multipleSelectionRows[0].state,
                  stress: this.multipleSelectionRows[0].stress,
                  name:this.multipleSelectionRows[0].title,
                  type:this.multipleSelectionRows[0].content,
                  id:this.multipleSelectionRows[0].id,
                }
              }

              _that.datas={
                id:data.data.id,
                num:_that.ruleForm.showTitle,
                stress: _that.ruleForm.tipsTitle,
                name:_that.ruleForm.noticeName,
                type:_that.ruleForm.noticeDesc
              }
              
              _that.ruleForm.showTitle=data.data.num
              _that.ruleForm.tipsTitle=data.data.stress
              _that.ruleForm.noticeName=data.data.name
              _that.ruleForm.noticeDesc=data.data.type
        }else{
          layer.msg("只能选择一项进行编辑", {
            icon: 2,
            closeBtn:1
          });
        }
      }else{
        this.ruleForm.noticeName=''
        this.ruleForm.noticeDesc=''
        this.title="新建"
        this.showDialog = true
      }
    },
    getCookie: function (name) {
      var cookie = document.cookie;
      var arr = cookie.split("; "); //将字符串分割成数组
      for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("=");
        if (arr1[0] == name) {
          return unescape(arr1[1]);
        }
      }
      return "GG"
    },
    submitForm(formName) {
      if(this.ruleForm.noticeName == ''){
          layer.msg("请输入通知标题", {
            icon: 7,
            closeBtn:1
        });
      }else if(this.ruleForm.noticeDesc == ''){
        layer.msg("请输入通知内容", {
          icon: 7,
          closeBtn:1
      });
      }else{

      
        if (this.title == '编辑') {
          var _that = this
          _that.datas.num=_that.ruleForm.showTitle
          _that.datas.name=_that.ruleForm.noticeName
          _that.datas.type=_that.ruleForm.noticeDesc
          console.log(_that.datas)
          let data = {
            id:_that.datas.id,
            title:_that.datas.name,
            content:_that.datas.type,
            state:_that.datas.num,
            stress: _that.ruleForm.tipsTitle
          }
          console.log(data)
          $.ajax({
            // url: url + ':9006/HomePage/userInfo',
            url: url + ':9006/notification/edit',
            type: "post",
             headers: {
              "token": token,
              "Content-Type": "application/json;charset=utf-8"
             },
            dataType: 'json',
            data: JSON.stringify(data),
            success: function (data) {
              console.log(data)
              _that.showDialog=false
              _that.getNotice()
            }
          });
        } else  if (this.title == '新建') {
          var _that = this

          let data = {
            title:_that.ruleForm.noticeName,
            content:_that.ruleForm.noticeDesc,
            state:_that.ruleForm.showTitle,
            stress: _that.ruleForm.tipsTitle
          }
          console.log(data)
          $.ajax({
            // url: url + ':9006/HomePage/userInfo',
            url: url + ':9006/notification/add',
            type: "post",
            contentType: "application/json;charset=utf-8",
             headers: {
              "token": token,
              // "Content-Type": "application/json;charset=utf-8"
             },
            dataType: 'json',
            data: JSON.stringify(data),
            success: function (data) {
              console.log(data)
              _that.showDialog=false
              _that.getNotice()
            }
          });
        }else if(this.title == '详情'){
          this.showDialog=false
        }
      }
    },
  },
  computed: {},
  mounted() {
    this.init();
  },
})
