
vm = new Vue({
    el: '#app',
    data: function () {
        return {
            ruleForm:{
                title:'',
                explain:'',
                id:'',
                oldPath:''
            },
            uploaddata:{
                title:'',
                explain:'',
            },
            headers:{
                'token':token
            },
            urll:url+':9006/userManual/add',
            urlll:url+':9006/userManual/edit',
            loginTitle:'',
            submitSerch:'',//搜索框
            showDialog: false,//弹框显隐
            showDialog1: false,//弹框显隐
            tableData:[],
            tableDataPage:0,
            tableDataTotal:0,
            datas:{},
            title:'新建',
            dialogDisabled:false,
            multipleSelectionRows:[],
            rules: {
                title: [
                    {required: true, message: '请输入活动名称', trigger: 'blur'},
                ],
            },
            fileList:[],
            hasOwnFlie:''
        }
    },
    methods: {
        beforeAvatarUpload(file) {
            const isLt2M = file.size / 1024 / 1024 < 20;
            if (!isLt2M) {
              this.$message.error('上传文件大小不能超过 20MB!');
            }
            return isLt2M;
          },
        ownClosed(){
            this.dialogDisabled=false
            this.ruleForm.title=''
            this.ruleForm.explain=''
            this.uploaddata.title=''
            this.uploaddata.explain=''
            this.ruleForm.id=''
            this.fileList=[]
        },
        handleExceed(files, fileList) {
            this.$message.warning(`一次只能上传一个文件`);
        },
        getmanual() {
            var _that = this
            let data = {
              currentPage:_that.tableDataPage,
              showCount:10,
              pd:{
                  title:""
              }
            }
            console.log(data)
            $.ajax({
              url: url + ':9006/userManual/getList',
              type: "post",
              contentType: "application/json;charset=utf-8",
               headers: {
                   "token":  _that.getCookie("token")
               },
              dataType: 'json',
              data: JSON.stringify(data),
              success: function (data) {
                console.log(data)
                _that.tableData = data.data.content
                console.log(_that.tableData)
                _that.tableDataTotal = data.data.pageCount
              }
            });
          },
        init(){
            let _that=this
            _that.getmanual()
        },
        sureCutOut(){
            console.log(this.multipleSelectionRows)
            // let userId = this.getCookie("userId"); // 获取用户id
            let arr=[]
            for(let i=0;i<this.multipleSelectionRows.length;i++){
                arr.push(this.multipleSelectionRows[i].id)
            }
            let _that=this
            $.ajax({
                url: url+':9006/userManual/delete ',
                type: 'post',
                async: true,
                headers: {
                    "token":  _that.getCookie("token")
                  // "Content-Type": "application/json;charset=utf-8"
                 },
                data: {ids :arr.join(',')},
                success: function(data) {
                    _that.$message({
                      message: '删除成功',
                      type: 'success'
                    });
                    _that.getmanual()
                },
                error: function(data) {
                  console.log(data)
                }
              })
        },
        searchInput(params){
            var _that = this
            let data = {
              currentPage:1,
              showCount:10,
              pd:{
                  title:_that.submitSerch
              }
            }
            $.ajax({
              url: url + ':9006/userManual/getList',
              type: "post",
              contentType: "application/json;charset=utf-8",
               headers: {
                   "token":  _that.getCookie("token")
               },
              dataType: 'json',
              data: JSON.stringify(data),
              success: function (data) {
                console.log(data)
                _that.tableData = data.data.content
                console.log(_that.tableData)
                _that.tableDataTotal = data.data.pageCount
              }
            });
        },
        handleCurrentChange(val){
            this.tableDataPage=val
            this.init()
        },
        handleSelectChangeLeft(rows){
            this.multipleSelectionRows=rows;
        },
        handleOpen(key, keyPath) {
            console.log(key, keyPath);
        },
        handleClose(key, keyPath) {
            console.log(key, keyPath);
        },
        x(){//清空搜索
            this.submitSerch = ''
            this.tableDataPage=0
            this.init();
        },
        cutOut(done) {
            let _that=this
            if(this.multipleSelectionRows.length==0){
                this.$message({
                    message: '请至少选择一项删除',
                    type: 'error'
                });
                return;
            }
            this.$confirm('您确定要删除?', '删除', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
            }).then(() => {
                _that.sureCutOut();
            }).catch(() => {
            });
        },
        closeBtn () {
            this.showDialog = false
            this.showDialog1 = false
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        newly(params){//新建请求
            if(params==='edit'){
                this.isEdit=true
                let _that=this
                    if(this.multipleSelectionRows.length==1){
                    this.title="编辑"
                    _that.showDialog1 = true
                        let data={
                            data:{
                            num:this.multipleSelectionRows[0].state,
                            name:this.multipleSelectionRows[0].title,
                            type:this.multipleSelectionRows[0].explain,
                            id:this.multipleSelectionRows[0].id,
                            }
                        }
                       _that.hasOwnFlie=this.multipleSelectionRows[0].fileName
                        _that.datas={
                            id:data.data.id,
                            name:_that.ruleForm.title,
                            type:_that.ruleForm.explain,
                        }
                        _that.ruleForm.id=data.data.id
                        _that.ruleForm.title=data.data.name
                        _that.ruleForm.explain=data.data.type
                }else{
                    layer.msg("只能选择一项进行编辑", {
                        icon: 2,
                        closeBtn:1
                    });
                }
            }else{
                this.uploaddata.title=''
                this.uploaddata.explain=''
                this.title="新建"
                this.showDialog = true
            }
        },
        getCookie: function (name) {
            var cookie = document.cookie;
            var arr = cookie.split("; "); //将字符串分割成数组
            for (var i = 0; i < arr.length; i++) {
                var arr1 = arr[i].split("=");
                if (arr1[0] == name) {
                    return unescape(arr1[1]);
                }
            }
            return "GG"
        },
        submitForm(formName) {
            // console.log(formName)
            // console.log(this.fileList)
            if(this.uploaddata.title == ''){
                layer.msg("请输入手册标题", {
                    icon: 7,
                    closeBtn:1
                });
            }
            else if (this.fileList.length === 0){
                layer.msg("请选择上传文件", {
                    icon: 7,
                    closeBtn:1
                });
            }
            else if (this.uploaddata.explain == ''){
                layer.msg("请输入手册说明", {
                    icon: 7,
                    closeBtn:1
                });
            }else{
                 var _that = this
                    _that.submitUpload()
                    
            }
        },
        submitForm1(formName) {
            if(this.ruleForm.title == ''){
                layer.msg("请输入手册标题", {
                    icon: 7,
                    closeBtn:1
                });
                return
            }
            // else if (this.fileList.length === 0){
            //     layer.msg("请选择上传文件", {
            //         icon: 7,
            //         closeBtn:1
            //     });
            // }
            else if (this.ruleForm.explain == ''){
                layer.msg("请输入手册说明", {
                    icon: 7,
                    closeBtn:1
                });
                return
            }else{
                    var _that = this
                    _that.submitUpload1()
            }
                
        },
        success(response, file, fileList){
            if(response.code==200){
                layer.msg("新建成功", {
                    icon: 1,
                    closeBtn:1
                });
                console.log(1222)
                this.showDialog = false
                this.getmanual()
            }else{
                layer.msg(response.message, {
                    icon: 1,
                    closeBtn:1
                });
            }
        },
        success1(){
            console.log(333)
            layer.msg("编辑成功", {
                icon: 1,
                closeBtn:1
            });
            this.showDialog1 = false
                    this.getmanual()
        },
        submitUpload1() {
             if(this.ruleForm.oldPath!=''){
                 this.$refs.uploads.submit();
             }else{
                 let _that=this
                 $.ajax({
                     url: url+':9006/userManual/edit',
                     type: 'post',
                     async: true,
                     data: this.ruleForm,
                     headers: {
                         "token":  this.getCookie("token")
                     },
                     success: function(data) {
                         if(data.code==200){
                             layer.msg("编辑成功", {
                                 icon: 1,
                                 closeBtn:1
                             });
                             _that.showDialog1 = false
                             _that.getmanual()
                         }else{
                             layer.msg(data.message, {
                                 icon: 1,
                                 closeBtn:1
                             });
                         }
                     },
                     error: function(data) {
                         console.log(data)
                     }
                 })
             }

        },
        submitUpload() {
            this.$refs.upload.submit();
        },
        handleRemove(file, fileList) {
            console.log(file, fileList);
        },
        handlePreview(file , fileList) {
            console.log(file , fileList)
            console.log(file, fileList,22222);
        },
        fileUpload(file){
            // console.log(file)
        },
        changeesdfs(file, fileList){
            this.ruleForm.oldPath=fileList==0?'':this.multipleSelectionRows[0].path
        },
        addChange(file, fileList){
           this.fileList=fileList
        },
    },
    computed: {},
    mounted() {
        this.init();
    },
})
