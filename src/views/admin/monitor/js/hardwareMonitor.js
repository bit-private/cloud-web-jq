;(function() {
  //JavaScript代码区域
  layui.use(["element", "form", "table", "layer", "laypage"], function() {
    var element = layui.element,
      table = layui.table,
      form = layui.form,
      laypage = layui.laypage,
      layer = layui.layer
    var gdfdf=''
    var eqquipmentName=''

    $(".hardBox").click(function(e) {
      // 点击服务器图片显示信息
      var fault = e.target.getAttribute("data-fault-id")
      var obj = e.target.getAttribute("data-equipmentId")
      gdfdf=e.target.getAttribute("data-equipmentId")
      var ip=e.target.getAttribute("data-ip")
      var odfsdesktopType=e.target.getAttribute("data-desktopType")
      if (e.target.nodeName.toLowerCase() == "img" && fault == 1) {
        // var url = 'http://10.1.1.234:9006'
        $.ajax({
          type: "POST",
          url: url + ":9006/EquipmentMonitor/equParentConn",
          data: JSON.stringify({
            equipmentId: obj
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            // var targetIp = data.data[0].extranetip
            var targetIp = ip
            var desktopType = odfsdesktopType
            var data = data.data
            openDetail([targetIp, desktopType])
            tableRender(data)
          }
        })
      }
    })

    function openDetail(params) {
      // 点击打开详情
      layer.open({
        type: 1,
        title: "详情", //不显示标题栏
        closeBtn: 1,
        area: ["95%", "640px"],
        shade: 0.3,
        id: "detail_layui", //设定一个id，防止重复弹出
        btn: ["确定"],
        btnAlign: "c",
        moveType: 1, //拖拽模式，0或者1
        content: $("#detailHtml"),
        success: function(layero, index) {
          $("#reboot").click(function() {
            $.ajax({
              type: "post",
              url: url + ":9006/EquipmentMonitor/restartEquipment",
              data: { ip: params[0], desktopType: params[1] },
              success: function(data) {
                layer.msg(data.message)
              }
            })
          })
        },
        end: function(params) {
          $("#reboot").unbind();
          $("#detailHtml").hide()
        }
      })
    }

    var state = $(".state")

    function tableRender(data) {
      table.render({
        elem: "#detailTable",
        data: data,
        limit: 10,
        page: { layout: ["prev", "page", "next", "skip"] },
        id: "state",
        cols: [
          [
            // { field: "state", title: "活动",templet: '#barDemo1'},
            {
              field: "type",
              title: "状态",
              width: 60,
              templet: function(d) {
                if (d.state == "1") {
                  // 如果活动状态为1 渲染绿色
                  return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                }
                if (d.state == "0") {
                  // 如果活动状态为2 渲染灰色
                  return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                }
                if (d.state === "") {
                  return "<i></i>"
                }
              }
            },
            {
              field: "type",
              title: "连接状态",
              templet: function(d) {
                if (d.connectionStatus == "1") {
                  // 如果活动状态为1 渲染绿色
                  return '<div class="connected">已连接</div>'
                }
                if (d.connectionStatus == "0") {
                  // 如果活动状态为1 渲染绿色
                  return '<div class="noConnected">未连接</div>'
                }
                if (d.connectionStatus == null) {
                  // 如果活动状态为1 渲染绿色
                  return "<div></div>"
                }
              },
              sort: true,
              unresize: true
            },
            {
              field: "user_name",
              title: "用户名",
              templet: '<div><span title="{{d.user_name}}">{{d.user_name || ""}}</span></div>'
            },
            {
              field: "system_user_id",
              title: "系统用户",
              templet: '<div><span title="{{d.system_user_id}}">{{d.system_user_id || ""}}</span></div>'
            },
            {
              field: "real_name",
              title: "姓名",
              templet: '<div><span title="{{d.real_name}}">{{d.real_name || ""}}</span></div>'
            },
            {
              field: "parent_department_name",
              title: "院所",
              templet: '<div><span title="{{d.parent_department_name}}">{{d.parent_department_name || ""}}</span></div>'
            },
            {
              field: "department_name",
              title: "科室",
              templet: '<div><span title="{{d.department_name}}">{{d.department_name || ""}}</span></div>'
            },
            {
              field: "app_name",
              title: "使用软件",
              templet: '<div><span title="{{d.app_name}}">{{d.app_name || ""}}</span></div>'
            },
            {
              field: "port",
              title: "端口",
              templet: '<div><span title="{{d.port}}">{{d.port || ""}}</span></div>'
            },
            {
              field: "open_time",
              title: "打开时间",
              templet: function(d) {
                var date = new Date(d.open_time) //时间戳如果是10位 *1000, 如果是13位不用 * 1000
                Y = date.getFullYear() + "-"
                M = (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) + "-"
                D = date.getDate() + " "
                h = date.getHours() + ":"
                m = date.getMinutes() + ":"
                s = date.getSeconds()
                return Y + M + D + h + m + s
              }
            },
            {
              field: "time_count",
              title: "工作时长(分钟)",
              templet: '<div><span title="{{d.time_count}}">{{d.time_count || ""}}</span></div>'
            },
            {
              field: "openType",
              title: "打开方式",
              templet: '<div><span title="{{d.openType}}">{{d.openType}}</span></div>'
            },
            {
              field: "screens",
              title: "操作",
              event: "open",
              templet: function(d) {
                if (d.desktopType == "平台") {
                  return '<span class="layui-btn layui-btn-xs openApp" lay-event="openApp">打开</span><span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span>'
                } else {
                  return '<span class="layui-btn layui-btn-xs closeApp" lay-event="closeApp">关闭</span>'
                }
              }
            }
          ]
        ],
        done: function(res, curr, count) {

          for (var i = 0; i < res.data.length; i++) {
            if (res.data[i].state == 1) {
              this.addClass("state-1")
            }
          }
        }.bind(state)
      })
    }

    table.on("tool(detailTable)", function(params) {
      var data = params.data
      if (params.event === "openApp") {
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/open",
          data: JSON.stringify({
            connId: data.id
          }), // 传用户id和软件id
          // data: JSON.stringify({ "userId": 5, "appId": 7 }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function(data) {
            var data = data.data
            var form = $("<form></form>")
              .attr("action", url + ":9006/AppConnection/downLoadVnc")
              .attr("method", "post")
            form.append(
              $("<input></input>")
                .attr("type", "hidden")
                .attr("name", "fileName")
                .attr("value", data[0])
            )
            form.append(
              $("<input></input>")
                .attr("type", "hidden")
                .attr("name", "cont")
                .attr("value", data[1])
            )
            form
              .appendTo("body")
              .submit()
              .remove()
          }
        })
      }
      if (params.event === "closeApp") {
        $.ajax({
          type: "POST",
          url: url + ":9006/AppConnection/close",
          data: JSON.stringify({
            ids: data.id,
            openType: data.openType,
            serverIp: data.ip,
            desktopType: data.desktopType,
            closesCommand: data.closesCommand
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function(data) {
            layer.msg(data.message)
            $.ajax({
              type: "POST",
              url: url + ":9006/EquipmentMonitor/equParentConn",
              data: JSON.stringify({
                equipmentId: gdfdf
              }),
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              success: function(data) {
                console.log(data)
                tableRender(data.data)
              }
            })
            // $("#detailHtml").hide()
          }
        })
      }
    })

    let cpualert = ""
    let gpualert = ""
    let ramalert = ""
    $.ajax({
      // 获取报警阀值
      type: "GET",
      url: url + ":9006/EquipmentMonitor/getThresholdValue",
      headers: {
        token: token
      },
      success: function(data) {
        cpualert = data.data.cpu
        gpualert = data.data.gpu
        ramalert = data.data.ram
      }
    })

    // 硬件监控-下拉菜单
    $.ajax({
      // 获取资源池数据
      type: "GET",
      url: url + ":9006/HardwareGroup/DeleteFlag",
      success: function(data) {
        var data = data.data
        var root = document.getElementById("resources")
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement("option")
          // option.setAttribute("value", data[i].groupName);
          option.setAttribute("title", data[i].id)
          option.innerText = data[i].groupName
          root.appendChild(option)
          form.render("select")
        }
      }
    })

    $.ajax({
      // 获取操作系统数据
      type: "GET",
      url: url + ":9006/EquipmentMonitor/OperatingSystems",
      // url: 'http://10.1.1.213:9006/EquipmentMonitor/OperatingSystems',
      success: function(data) {
        var root = document.getElementById("system")
        var data = data.data
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement("option")
          // option.setAttribute("value", data[i].groupName);
          option.setAttribute("title", data[i].typeKey)
          option.innerText = data[i]
          root.appendChild(option)
          form.render("select")
        }
      }
    })

    $.ajax({
      // 获取设备类型数据
      type: "GET",
      url: url + ":9006/EquipmentMonitor/HardwareTypes",
      success: function(data) {
        var root = document.getElementById("equipment")
        var data = data.data
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement("option")
          // option.setAttribute("value", data[i].groupName);
          option.setAttribute("title", data[i].typeKey)
          option.innerText = data[i].value
          root.appendChild(option)
          form.render("select")
        }
      }
    })

    var active = {
      getLaypage: function(val, type) {
        laypage.render({
          elem: "laypage",
          count: val,
          limit: 40,
          jump: function(obj, first) {
            $.ajax({
              type: "POST",
              url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40" + "/" + (obj.curr - 1) + "/" + type,
              // data: JSON.stringify({}),
              data: JSON.stringify({
                resourcePoolId: resourcesId,
                operatingSystemId: systemId,
                equipmentType: equipmentId,
                equipmentName: eqquipmentName
              }),
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              success: function(data) {
                var data = data.data.content
                var ul = document.getElementById("ul")
                ul.innerHTML = ""
                $('#ul').html("");
                for (var i = 0; i < data.length; i++) {
                  var li = document.createElement("li")
                  var image = document.createElement("img")
                  var p = document.createElement("p")
                  var state = document.createElement("img")
                  var hasConnect = document.createElement("div")
                  image.setAttribute("class", "hardImg")
                  // image.setAttribute("src", "./img/u2957.png")
                  if (data[i].operatingSystemId == "Linux") {
                    image.setAttribute("src", "./img/linux.png")
                  } else {
                    image.setAttribute("src", "./img/windows.png")
                  }
                  image.setAttribute("data-cpu", data[i].cpu)
                  image.setAttribute("data-gpu", data[i].gpu)
                  image.setAttribute("data-memory", data[i].memory)
                  image.setAttribute("data-fault-id", data[i].faultId)
                  image.setAttribute("data-equipmentId", data[i].equipmentId)
                  image.setAttribute("data-connectionCount", data[i].connectionCount)
                  image.setAttribute("data-userCount", data[i].userCount)
                  image.setAttribute("data-id", data[i].id)
                  image.setAttribute("data-desktopType", data[i].desktopType)
                  image.setAttribute("data-swap", data[i].swap)
                  image.setAttribute("data-operatingSystemId", data[i].operatingSystemId)
                  image.setAttribute("data-ip", data[i].ip)
                  p.setAttribute("class", "serverName")
                  p.innerText = data[i].equipmentName
                  if (data[i].status == 1) {
                    hasConnect.setAttribute("class", "userFlag")
                  }
                  if (data[i].faultId == 1) {
                    state.setAttribute("class", "hardFlag")
                    state.setAttribute("src", "./img/u2961.png")
                    if (data[i].cpu > cpualert || data[i].gpu > gpualert || data[i].memory > ramalert) {
                      state.setAttribute("class", "hardFlag")
                      state.setAttribute("src", "./img/u3060.png")
                    }
                  } else if (data[i].faultId == 3) {
                    state.setAttribute("class", "hardFlag")
                    state.setAttribute("src", "./img/u3000.png")
                  } else {
                    state.setAttribute("class", "hardFlag")
                    state.setAttribute("src", "./img/u3051.png")
                  }
                  li.setAttribute("class", "layui-col-sm2")
                  li.setAttribute("data-id", data[i].id)
                  li.appendChild(image)
                  li.appendChild(state)
                  li.appendChild(hasConnect)
                  li.appendChild(p)
                  ul.appendChild(li)

                  $(".hardImg").on("mouseenter", function(params) {
                    // 鼠标悬浮显示信息
                    //  console.log(this.getAttribute("data-cpu"),this.getAttribute("data-gpu"),this.getAttribute("data-memory"))
                    // var cpu = this.getAttribute("data-cpu");
                    var cpu = this.getAttribute("data-cpu") >= cpualert ? "<span style='color:red'>CPU：" + this.getAttribute("data-cpu") + "%" + "</span>" : "<span >CPU：" + this.getAttribute("data-cpu") + "%" + "</span>"
                    var gpu = this.getAttribute("data-gpu") >= gpualert ? "<span style='color:red'>GPU：" + this.getAttribute("data-gpu") + "%" + "</span>" : "<span >GPU：" + this.getAttribute("data-gpu") + "%" + "</span>"
                    var memory = this.getAttribute("data-memory") >= ramalert ? "<span style='color:red'>内存：" + this.getAttribute("data-memory") + "%" + "</span>" : "<span >内存：" + this.getAttribute("data-memory") + "%" + "</span>"
                    var userCount = this.getAttribute("data-userCount")
                    var connectionCount = this.getAttribute("data-connectionCount")
                    var swap = "<span style='color:red'>SWAP： 已占用</span>"
                    var operatingSystemId = this.getAttribute("data-operatingSystemId")
                    var ip = this.getAttribute("data-ip")

                    if (this.getAttribute("data-fault-id") == 1 && this.getAttribute("data-swap") == 1) {
                      var htmlTpl = "用户数：" + userCount + "<br/>连接数：" + connectionCount + "<br/>" + cpu + "<br/>" + gpu + "<br/>" + memory + "<br/>" + swap + "<br/>" + ip
                      layer.tips(htmlTpl, $(this), {
                        tips: [2, "#78BA32"]
                      })
                    } else if (this.getAttribute("data-fault-id") == 1 && this.getAttribute("data-swap") == 0) {
                      var htmlTpl = "用户数：" + userCount + "<br/>连接数：" + connectionCount + "<br/>" + cpu + "<br/>" + gpu + "<br/>" + memory + "<br/>" + "<br/>" + ip
                      layer.tips(htmlTpl, $(this), {
                        tips: [2, "#78BA32"]
                      })
                    } else if (this.getAttribute("data-fault-id") == 1 && this.getAttribute("data-swap") == 3) {
                      if (this.getAttribute("data-operatingSystemId") == "Windows") {
                        var htmlTpl = "用户数：" + userCount + "<br/>连接数：" + connectionCount + "<br/>" + cpu + "<br/>" + gpu + "<br/>" + memory + "<br/>" + ip
                      } else {
                        var htmlTpl = "用户数：" + userCount + "<br/>连接数：" + connectionCount + "<br/>" + cpu + "<br/>" + gpu + "<br/>" + memory + "<br/>" + ip
                      }
                      layer.tips(htmlTpl, $(this), {
                        tips: [2, "#78BA32"]
                      })
                    } else if (this.getAttribute("data-fault-id") == 2) {
                      // 服务器不可用
                      var htmlTpl = "服务器无法连接" + "<br/>" + ip
                      layer.tips(htmlTpl, $(this), {
                        tips: [2, "#78BA32"]
                      })
                    } else if (this.getAttribute("data-fault-id") == 3) {
                      // 服务器已停用
                      var htmlTpl = "服务器已停用" + "<br/>" + ip
                      layer.tips(htmlTpl, $(this), {
                        tips: [2, "#78BA32"]
                      })
                    }
                  })
                }
              }
            })
          }
        })
      },
      getDashboard: function() {
        $.ajax({
          // 右上方仪表盘数据
          type: "POST",
          url: url + ":9006/EquipmentMonitor/totalPerformance",
          // data:{"operatingSystemId": "liunx"},
          data: JSON.stringify({
            resourcePoolId: resourcesId,
            operatingSystemId: systemId,
            equipmentType: equipmentId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            var myChart = echarts.init(document.getElementById("main"))
            var myChart1 = echarts.init(document.getElementById("main1"))
            var myChart2 = echarts.init(document.getElementById("main2"))
            var data = data.data
            option = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}"
              },
              series: [
                {
                  //类型
                  type: "gauge",
                  min: 0,
                  max: 100,
                  //半径
                  //radius: 180,
                  //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
                  startAngle: 180,
                  //结束角度。
                  endAngle: 0,
                  center: ["50%", "70%"],
                  //仪表盘轴线相关配置。
                  axisLine: {
                    show: true,
                    // 属性lineStyle控制线条样式
                    lineStyle: {
                      width: 20,
                      color: [
                        [data.cpu / 100, "#36ce9b"], // 进度条进度
                        [1, "#576d85"]
                      ]
                    }
                  },
                  //分隔线样式。
                  splitLine: {
                    show: true,
                    length: 0
                  },
                  //刻度样式。
                  axisTick: {
                    show: false
                  },
                  //刻度标签。
                  axisLabel: {
                    show: true,
                    fontSize: 12,
                    distance: -20
                  },
                  //仪表盘指针。
                  pointer: {
                    //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
                    show: false,
                    //指针长度
                    length: "90%",
                    width: 0
                  },
                  //仪表盘标题。
                  title: {
                    show: true,
                    offsetCenter: [0, "-25%"], // x, y，单位px
                    textStyle: {
                      color: "#36ce9b",
                      fontSize: 12
                    }
                  },
                  //仪表盘详情，用于显示数据。
                  detail: {
                    show: true,
                    offsetCenter: [0, 0],
                    formatter: "{value}%",
                    textStyle: {
                      fontSize: 12
                    }
                  },
                  splitNumber: 1,
                  data: [
                    {
                      value: data.cpu,
                      name: "cpu",
                      textStyle: {
                        fontSize: 16
                      }
                    }
                  ]
                }
              ]
            }

            myChart.setOption(option)

            option1 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}"
              },
              series: [
                {
                  //类型
                  type: "gauge",
                  min: 0,
                  max: 100,
                  //半径
                  //radius: 180,
                  //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
                  startAngle: 180,
                  //结束角度。
                  endAngle: 0,
                  center: ["50%", "70%"],
                  //仪表盘轴线相关配置。
                  axisLine: {
                    show: true,
                    // 属性lineStyle控制线条样式
                    lineStyle: {
                      width: 20,
                      color: [
                        [data.gpu / 100, "#36ce9b"], // 进度条进度
                        [1, "#576d85"]
                      ]
                    }
                  },
                  //分隔线样式。
                  splitLine: {
                    show: true,
                    length: 0
                  },
                  //刻度样式。
                  axisTick: {
                    show: false
                  },
                  //刻度标签。
                  axisLabel: {
                    show: true,
                    fontSize: 12,
                    distance: -20
                  },
                  //仪表盘指针。
                  pointer: {
                    //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
                    show: false,
                    //指针长度
                    length: "90%",
                    width: 0
                  },
                  //仪表盘标题。
                  title: {
                    show: true,
                    offsetCenter: [0, "-25%"], // x, y，单位px
                    textStyle: {
                      color: "#36ce9b",
                      fontSize: 12
                    }
                  },
                  //仪表盘详情，用于显示数据。
                  detail: {
                    show: true,
                    offsetCenter: [0, 0],
                    formatter: "{value}%",
                    textStyle: {
                      fontSize: 12
                    }
                  },
                  splitNumber: 1,
                  data: [
                    {
                      value: data.gpu,
                      name: "gpu",
                      textStyle: {
                        fontSize: 16
                      }
                    }
                  ]
                }
              ]
            }

            myChart1.setOption(option1)

            option2 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}"
              },
              series: [
                {
                  //类型
                  type: "gauge",
                  min: 0,
                  max: 100,
                  //半径
                  //radius: 180,
                  //起始角度。圆心 正右手侧为0度，正上方为90度，正左手侧为180度。
                  startAngle: 180,
                  //结束角度。
                  endAngle: 0,
                  center: ["50%", "70%"],
                  //仪表盘轴线相关配置。
                  axisLine: {
                    show: true,
                    // 属性lineStyle控制线条样式
                    lineStyle: {
                      width: 20,
                      color: [
                        [data.men / 100, "#36ce9b"], // 进度条进度
                        [1, "#576d85"]
                      ]
                    }
                  },
                  //分隔线样式。
                  splitLine: {
                    show: true,
                    length: 0
                  },
                  //刻度样式。
                  axisTick: {
                    show: false
                  },
                  //刻度标签。
                  axisLabel: {
                    show: true,
                    fontSize: 12,
                    distance: -20
                  },
                  //仪表盘指针。
                  pointer: {
                    //这个show属性好像有问题，因为在这次开发中，需要去掉指正，我设置false的时候，还是显示指针，估计是BUG吧，我用的echarts-3.2.3；希望改进。最终，我把width属性设置为0，成功搞定！
                    show: false,
                    //指针长度
                    length: "90%",
                    width: 0
                  },
                  //仪表盘标题。
                  title: {
                    show: true,
                    offsetCenter: [0, "-25%"], // x, y，单位px
                    textStyle: {
                      color: "#36ce9b",
                      fontSize: 12
                    }
                  },
                  //仪表盘详情，用于显示数据。
                  detail: {
                    show: true,
                    offsetCenter: [0, 0],
                    formatter: "{value}%",
                    textStyle: {
                      fontSize: 12
                    }
                  },
                  splitNumber: 1,
                  data: [
                    {
                      value: data.men,
                      name: "内存",
                      textStyle: {
                        fontSize: 16
                      }
                    }
                  ]
                }
              ]
            }

            myChart2.setOption(option2)
          }
        })
      }
    }
    var valParams = {
      type: 'equipment_name'
    }

    $.ajax({
      // 默认按设备名显示服务器
      type: "POST",
      url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
      data: JSON.stringify({}),
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      success: function(data) {
        active.getLaypage(data.data.totalElements, valParams.type)
      }
    })
    active.getDashboard()

    var IntervalName = setInterval(function() {
      // 每3秒查询一下服务器
      $.ajax({
        // 默认按设备名显示服务器
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
        data: JSON.stringify({}),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()
    }, 6000)

    var resourcesId = "",
      systemId = "",
      equipmentId = ""
    var screenInterval
    form.on("select(resources)", function(data) {
      // 资源池
      resourcesId = data.elem[data.elem.selectedIndex].title
      $("#system").val("")
      $("#equipment").val("")
      systemId = ""
      equipmentId = ""
      form.render("select")

      clearInterval(IntervalName) // 清除进入页面的默认定时器
      clearInterval(radioInterval) // 每次点击排序的按钮都先清空一次定时器
      clearInterval(screenInterval) // 每次点击排序的按钮都先清空一次定时器
      clearInterval(searchInterval) // 停止搜索定时器
      $.ajax({
        // 默认按设备名显示服务器
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
        data: JSON.stringify({
          resourcePoolId: resourcesId,
          operatingSystemId: systemId,
          equipmentType: equipmentId
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()

      screenInterval = setInterval(function(resourcesId,systemId,equipmentId) {
        $.ajax({
          // 默认按设备名显示服务器
          type: "POST",
          url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
          data: JSON.stringify({
            resourcePoolId: resourcesId,
            operatingSystemId: systemId,
            equipmentType: equipmentId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            active.getLaypage(data.data.totalElements, valParams.type)
          }
        })
        active.getDashboard()
      }, 6000)
    })

    form.on("select(system)", function(data) {
      // 系统
      // systemId = data.elem[data.elem.selectedIndex].title
      systemId = data.value
      $("#resources").val("")
      $("#equipment").val("")
      resourcesId = ""
      equipmentId = ""
      form.render("select")

      clearInterval(IntervalName) // 清除进入页面的默认定时器
      clearInterval(radioInterval) // 每次点击排序的按钮都先清空一次定时器
      clearInterval(screenInterval)
      clearInterval(searchInterval) // 停止搜索定时器
      $.ajax({
        // 默认按设备名显示服务器
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
        data: JSON.stringify({
          resourcePoolId: resourcesId,
          operatingSystemId: systemId,
          equipmentType: equipmentId
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()

      screenInterval = setInterval(function() {
        $.ajax({
          // 默认按设备名显示服务器
          type: "POST",
          url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
          data: JSON.stringify({
            resourcePoolId: resourcesId,
            operatingSystemId: systemId,
            equipmentType: equipmentId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            active.getLaypage(data.data.totalElements, valParams.type)
          }
        })
        active.getDashboard()
      }, 6000)
    })

    form.on("select(equipment)", function(data) {
      // 设备类型
      equipmentId = data.elem[data.elem.selectedIndex].title
      $("#system").val("")
      $("#resources").val("")
      resourcesId = ""
      systemId = ""
      form.render("select")

      clearInterval(IntervalName) // 清除进入页面的默认定时器
      clearInterval(radioInterval) // 每次点击排序的按钮都先清空一次定时器
      clearInterval(screenInterval)
      clearInterval(searchInterval) // 停止搜索定时器
      $.ajax({
        // 默认按设备名显示服务器
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
        data: JSON.stringify({
          resourcePoolId: resourcesId,
          operatingSystemId: systemId,
          equipmentType: equipmentId
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()

      screenInterval = setInterval(function() {
        $.ajax({
          // 默认按设备名显示服务器
          type: "POST",
          url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/40/0/" + valParams.type,
          data: JSON.stringify({
            resourcePoolId: resourcesId,
            operatingSystemId: systemId,
            equipmentType: equipmentId
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            active.getLaypage(data.data.totalElements, valParams.type)
          }
        })
        active.getDashboard()
      }, 6000)
    })

    var radioInterval
    form.on("radio(type)", function(data3) {
      // 服务器列表分页
      eqquipmentName = $("#monitor-hardware-monitor-input").val()
      clearInterval(IntervalName) // 清除进入页面的默认定时器
      clearInterval(radioInterval) // 每次点击排序的按钮都先清空一次定时器
      clearInterval(screenInterval)
      clearInterval(searchInterval) // 停止搜索定时器
      valParams.type = data3.value
      var type = data3.value,
        size = 40,
        page = 0
      $.ajax({
        // 服务器列表数据
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/" + type,
        data: JSON.stringify({
          resourcePoolId: resourcesId,
          operatingSystemId: systemId,
          equipmentType: equipmentId,
          equipmentName: eqquipmentName
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()

      radioInterval = setInterval(function() {
        // 每3秒查询一下服务器
        $.ajax({
          // 服务器列表数据
          type: "POST",
          url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/" + type,
          data: JSON.stringify({
            resourcePoolId: resourcesId,
            operatingSystemId: systemId,
            equipmentType: equipmentId,
            equipmentName: eqquipmentName
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            active.getLaypage(data.data.totalElements, valParams.type)
          }
        })

        active.getDashboard()
      }, 6000)
    })

    var searchInterval
    $("#monitor-hardware-monitor-btn").click(function() {
      // 点击搜索按钮查询
      clearInterval(IntervalName) // 清除进入页面的默认定时器
      clearInterval(radioInterval) // 每次点击排序的按钮都先清空一次定时器
      clearInterval(screenInterval) // 停止筛选定时器
      clearInterval(searchInterval) // 停止搜索定时器
      var size = 40,
        page = 0
       eqquipmentName = $("#monitor-hardware-monitor-input").val()
      $.ajax({
        // 搜索按钮 - 服务器列表数据
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/cpu",
        data: JSON.stringify({
          equipmentName: eqquipmentName
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()

      searchInterval = setInterval(function() {
        $.ajax({
          // 搜索按钮 - 服务器列表数据
          type: "POST",
          url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/cpu",
          data: JSON.stringify({
            equipmentName: eqquipmentName
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            active.getLaypage(data.data.totalElements, valParams.type)
          }
        })
        active.getDashboard()
      }, 6000)
    })
    $("#monitor-hardware-monitor-input").on("keypress", function() {
      // 点击搜索按钮查询
      if (event.keyCode == "13") {
        clearInterval(IntervalName) // 清除进入页面的默认定时器
        clearInterval(radioInterval) // 每次点击排序的按钮都先清空一次定时器
        clearInterval(screenInterval) // 停止筛选定时器
        clearInterval(searchInterval) // 停止搜索定时器
        var size = 40,
          page = 0
         eqquipmentName = $("#monitor-hardware-monitor-input").val()
        $.ajax({
          // 搜索按钮 - 服务器列表数据
          type: "POST",
          url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/cpu",
          data: JSON.stringify({
            equipmentName: eqquipmentName
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            active.getLaypage(data.data.totalElements, valParams.type)
          }
        })
        active.getDashboard()

        searchInterval = setInterval(function() {
          $.ajax({
            // 搜索按钮 - 服务器列表数据
            type: "POST",
            url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/cpu",
            data: JSON.stringify({
              equipmentName: eqquipmentName
            }),
            headers: {
              "Content-Type": "application/json;charset=utf-8"
            },
            success: function(data) {
              active.getLaypage(data.data.totalElements, valParams.type)
            }
          })
          active.getDashboard()
        }, 6000)
      }
    })

    $("#monitor-hardware-monitor-icon").click(function() {
      // 点击清空按钮
      var size = 40,
        page = 0
      $("#monitor-hardware-monitor-input").val("")
      $.ajax({
        // 搜索按钮 - 服务器列表数据
        type: "POST",
        url: url + ":9006/EquipmentMonitor/pageEquipmentMonitor/" + size + "/" + page + "/cpu",
        data: JSON.stringify({
          equipmentName: ""
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          active.getLaypage(data.data.totalElements, valParams.type)
        }
      })
      active.getDashboard()
    })

    $("body").on("mouseenter", ".success", function(params) {
      layer.tips("活动中", $(this), {
        tips: [2, "#78BA32"]
      })
    })
    $("body").on("mouseenter", ".fail", function(params) {
      layer.tips("未活动", $(this), {
        tips: [2, "#78BA32"]
      })
    })
  })
})()
