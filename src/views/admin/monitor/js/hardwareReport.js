
(function () {
  //JavaScript代码区域
  layui.use(['jquery', 'element', 'form', 'layedit', 'laydate', "table", "layer"], function () {
    var element = layui.element,
      table = layui.table,
      jquery = layui.jquery,
      layer = layui.layer,
      laydate = layui.laydate,
      form = layui.form;

    // :9006/EquipmentMonitorHistory/ProjectCountByTimeType/   仪表盘
    // :9006/EquipmentMonitorHistory/TimeTypeData/  线图
    var params = {
      day: '1',
      resourcePoolId: '',
      operatingSystemId: '',
      equipmentType: '',
      stateTime: '',
      endTime: ''
    }
    var active = {
      getLineGraph: function(day, resourcePoolId, operatingSystemId, equipmentType) {
        $.ajax({ // 最近一周数据
          type: 'POST',
          url: url + ':9006/EquipmentMonitorHistory/TimeTypeData/' + day,
          data: JSON.stringify({resourcePoolId, operatingSystemId, equipmentType}),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            if (data.code == 200) {
              var data = data.data;
              var xTime = [],
                cpu = [],
                gpu = [],
                memory = [],
                connect = [],
                mouseTime = [],
                mouseCpu = [],
                mouseGpu = [],
                mouseMemory = [],
                mouseConnect = [];
              for (var i = 0; i < data.lineData.length; i++) {
                xTime.push(data.lineData[i].datetime)
                cpu.push(data.lineData[i].cpu)
                gpu.push(data.lineData[i].gpu)
                memory.push(data.lineData[i].memory)
                connect.push(data.lineData[i].connectionNumber)
              }
              for (var k = 0; k < data.mouseData.length; k++) {
                mouseTime.push(data.mouseData[k].datetime)
                mouseCpu.push(data.mouseData[k].cpu)
                mouseGpu.push(data.mouseData[k].gpu)
                mouseMemory.push(data.mouseData[k].memory)
                mouseConnect.push(data.mouseData[k].connectionNumber)
              }
              // console.log(mouseTime, mouseCpu, mouseGpu, mouseMemory)
              var myChartLine = echarts.init(document.getElementById('mainLine'));
              optionLine = {
                tooltip: {
                  trigger: 'item',
                  formatter: function (params, ticket, callback) {
                    // console.log(data.mouseData)
                    var htmlStr = '';
                    for (var i = 0; i < params.length; i++) {
                      var param = params[i];
                      var xName = param.name; //x轴的名称
                      var seriesName = param.seriesName; //图例名称
                      var value = param.value; //y轴值
                      var color = param.color; //图例颜色

                      if (i === 0) {
                        htmlStr += xName + '<br/>'; //x轴的名称
                      }
                      htmlStr += '<div>';
                      //为了保证和原来的效果一样，这里自己实现了一个点的效果
                      htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>';
                    }
                    for (var k = 0; k < data.mouseData.length; k++) {
                      // console.log(data.mouseData[k])
                      var mouse = data.mouseData[k];
                      var cpus = mouse.cpu;
                      var gpus = mouse.gpu;
                      var memorys = mouse.memory;
                      var times = mouse.datetime;
                      var connects = mouse.connectionNumber;
                      if (times === params.name) {
                        htmlStr += mouse.equipmentName + ' ' + 'cpu' + '：' + cpus + '%' + ' ' + 'gpu' + '：' + gpus + '%' + ' ' + 'memory' + '：' + memorys + '%' + ' ' + '连接数' + '：' + connects + '<br/>';
                        htmlStr += '</div>';
                      }
                      // console.log(times, xName, params.name)
                    }
                    return htmlStr;
                  }
                },
                legend: {
                  data: ['cpu', 'gpu', 'memory', '连接数']
                },
                grid: {
                  left: '3%',
                  right: '4%',
                  bottom: '3%',
                  containLabel: true
                },
                toolbox: {
                  feature: {
                    // saveAsImage: {}  // 是否显示保存图片 
                  }
                },
                xAxis: {
                  type: 'category',
                  // boundaryGap: false,
                  data: xTime
                },
                yAxis: {
                  type: 'value'
                },
                series: [{
                    name: 'cpu',
                    type: 'line',
                    data: cpu,
                    color: 'red'
                  },
                  {
                    name: 'gpu',
                    type: 'line',
                    data: gpu,
                    color: 'blue'
                  },
                  {
                    name: 'memory',
                    type: 'line',
                    data: memory,
                    color: 'green'
                  },
                  {
                    name: '连接数',
                    type: 'line',
                    data: connect,
                    color: 'yellow'
                  }
                ]
              };
              myChartLine.setOption(optionLine)
            } else {
              layer.msg(data.message)
            }
          }
        })
      },
      getDashboard: function(day, resourcePoolId, operatingSystemId, equipmentType){
        $.ajax({ // 默认最近一天数据 cpu gpu 内存
          type: 'POST',
          url: url + ':9006/EquipmentMonitorHistory/ProjectCountByTimeType/' + day,
          data: JSON.stringify({resourcePoolId, operatingSystemId, equipmentType}),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            var data = data.data;
            option = { // cpu
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'cpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.cpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.cpu, //cpu下方数值
                  name: 'cpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart.setOption(option);
    
            option1 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'gpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.gpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.gpu, //cpu下方数值
                  name: 'gpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart1.setOption(option1);
    
            option2 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: '内存',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.memory / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.memory, //cpu下方数值
                  name: '内存',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart2.setOption(option2);
          }
        })
      }
    }

    $.ajax({ // 获取资源池数据
      type: 'GET',
      url: url + ':9006/HardwareGroup/DeleteFlag',
      success: function (data) {
        console.log(data)
        var data = data.data;
        var root = document.getElementById("resources");
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement("option");
          // option.setAttribute("value", data[i].groupName);
          option.setAttribute("title", data[i].id);
          option.innerText = data[i].groupName;
          root.appendChild(option);
          form.render('select');
          // console.log(data[i].groupName)
        }
      }
    })

    $.ajax({ // 获取操作系统数据
      type: 'GET',
      url: url + ':9006/EquipmentMonitor/OperatingSystems',
      success: function (data) {
        console.log(data)
        var data = data.data
        var root = document.getElementById("system");
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement("option");
          // option.setAttribute("value", data[i].groupName);
          option.setAttribute("title", data[i]);
          option.innerText = data[i];
          root.appendChild(option);
          form.render('select');
        }
      }
    })

    $.ajax({ // 获取设备类型数据
      type: 'GET',
      url: url + ':9006/EquipmentMonitor/HardwareTypes',
      success: function (data) {
        console.log(data)
        var data = data.data
        var root = document.getElementById("equipment");
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement("option");
          // option.setAttribute("value", data[i].groupName);
          option.setAttribute("title", data[i].typeKey);
          option.innerText = data[i].value;
          root.appendChild(option);
          form.render('select');

        }
      }
    })

    var myChart = echarts.init(document.getElementById('main'));
    var myChart1 = echarts.init(document.getElementById('main1'));
    var myChart2 = echarts.init(document.getElementById('main2'));

    active.getLineGraph(params.day)
    active.getDashboard(params.day)

    var resourcesId = '',
      systemId = '',
      equipmentId = '',
      eqquipmentName = '';
    form.on('select(resources)', function (data) { // 资源池
      params.resourcesId = data.elem[data.elem.selectedIndex].title
      active.getLineGraph(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
      active.getDashboard(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
    });

    form.on('select(system)', function (data) { // 系统
      params.operatingSystemId = data.elem[data.elem.selectedIndex].title
      active.getLineGraph(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
      active.getDashboard(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
    });

    form.on('select(equipment)', function (data) { // 设备类型
      params.equipmentType = data.elem[data.elem.selectedIndex].title
      active.getLineGraph(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
      active.getDashboard(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
    });

    $("#monitor-hardware-report-input").change(function () {
      $("#system").val("");
      $("#equipment").val("");
      $("#resources").val("");
      systemId = '';
      equipmentId = '';
      resourcesId = '';
      form.render('select');
    })

    var dataTypes
    form.on('radio(dateType)', function (data) { // 单选按钮 选择时间
      // console.log(data.value)
      var stateTime = $("#test1").val('')
      var endTime = $("#test2").val('')
      data.value == '1' ? params.day = '1' : data.value == '2' ? params.day = '7' : params.day = '30'
      active.getLineGraph(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
      active.getDashboard(params.day, params.resourcesId, params.operatingSystemId, params.equipmentType)
    });

    laydate.render({
      elem: '#test1' //指定元素
    });

    laydate.render({
      elem: '#test2' //指定元素
    });

    $("#monitor-hardware-report-btn").click(function () { // 点击搜索按钮
      var eqquipmentName = $("#monitor-hardware-report-input").val();
      $.ajax({ // 默认最近一天数据
        type: 'POST',
        url: url + ':9006/EquipmentMonitorHistory/DayData',
        data: JSON.stringify({
          "resourcePoolId": resourcesId,
          "operatingSystemId": systemId,
          "equipmentType": equipmentId,
          "equipmentName": eqquipmentName
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          var data = data.data;
          var xTime = [],
            cpu = [],
            gpu = [],
            connect = [],
            memory = [],
            mouseTime = [],
            mouseCpu = [],
            mouseGpu = [],
            mouseMemory = [],
            mouseConnect = [];
          for (var i = 0; i < data.lineData.length; i++) {
            xTime.push(data.lineData[i].datetime)
            cpu.push(data.lineData[i].cpu)
            gpu.push(data.lineData[i].gpu)
            memory.push(data.lineData[i].memory)
            connect.push(data.lineData[i].connectionNumber)
          }
          for (var k = 0; k < data.mouseData.length; k++) {
            mouseTime.push(data.mouseData[k].datetime)
            mouseCpu.push(data.mouseData[k].cpu)
            mouseGpu.push(data.mouseData[k].gpu)
            mouseMemory.push(data.mouseData[k].memory)
            mouseConnect.push(data.mouseData[k].connectionNumber)
          }
          // console.log(mouseTime, mouseCpu, mouseGpu, mouseMemory)
          var myChartLine = echarts.init(document.getElementById('mainLine'));
          optionLine = {
            tooltip: {
              // trigger: 'axis',
              trigger: 'item',
              formatter: function (params, ticket, callback) {
                // console.log(data.mouseData)
                var htmlStr = '';
                for (var i = 0; i < params.length; i++) {
                  var param = params[i];
                  var xName = param.name; //x轴的名称
                  var seriesName = param.seriesName; //图例名称
                  var value = param.value; //y轴值
                  var color = param.color; //图例颜色

                  if (i === 0) {
                    htmlStr += xName + '<br/>'; //x轴的名称
                  }
                  htmlStr += '<div>';
                  //为了保证和原来的效果一样，这里自己实现了一个点的效果
                  htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>';

                  //圆点后面显示的文本
                  // htmlStr += seriesName + '：' + value + '%';

                  // htmlStr += '</div>';
                }
                for (var k = 0; k < data.mouseData.length; k++) {
                  // console.log(data.mouseData[k])
                  var mouse = data.mouseData[k];
                  var cpus = mouse.cpu;
                  var gpus = mouse.gpu;
                  var memorys = mouse.memory;
                  var times = mouse.datetime;
                  var connects = mouse.connectionNumber;
                  if (times === params.name) {
                    htmlStr += mouse.equipmentName + ' ' + 'cpu' + '：' + cpus + '%' + ' ' + 'gpu' + '：' + gpus + '%' + ' ' + 'memory' + '：' + memorys + '%' + ' ' + '连接数' + '：' + connects + '<br/>';
                    htmlStr += '</div>';
                  }
                  // console.log(times, xName, params.name)
                }
                return htmlStr;
              }

            },
            legend: {
              data: ['cpu', 'gpu', 'memory', '连接数'],
            },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true
            },
            toolbox: {
              feature: {
                // saveAsImage: {}  // 是否显示保存图片 
              }
            },
            xAxis: {
              type: 'category',
              // boundaryGap: false,  // 是否顶头
              data: xTime,
              axisLabel: {
                interval: 0,
                rotate: 40
              },
            },
            yAxis: {
              type: 'value'
            },
            series: [{
                name: 'cpu',
                type: 'line',
                data: cpu,
                color: 'red'
              },
              {
                name: 'gpu',
                type: 'line',
                data: gpu,
                color: 'blue'
              },
              {
                name: 'memory',
                type: 'line',
                data: memory,
                color: 'green'
              },
              {
                name: '连接数',
                type: 'line',
                data: connect,
                color: 'yellow'
              }
            ]
          };
          myChartLine.setOption(optionLine)
        }
      })
      $.ajax({ // 默认最近一天数据
        type: 'POST',
        url: url + ':9006/EquipmentMonitorHistory/ProjectCountByTimeType/day',
        data: JSON.stringify({
          "resourcePoolId": resourcesId,
          "operatingSystemId": systemId,
          "equipmentType": equipmentId,
          "equipmentName": eqquipmentName
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          if (data.code == 200) {
            var data = data.data;
            option = { // cpu
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'cpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.cpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.cpu, //cpu下方数值
                  name: 'cpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart.setOption(option);

            option1 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'gpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.gpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.gpu, //cpu下方数值
                  name: 'gpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart1.setOption(option1);

            option2 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: '内存',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.memory / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.memory, //cpu下方数值
                  name: '内存',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart2.setOption(option2);
          } else {
            layer.msg(data.message)
          }
        }
      })
    })
    $("#monitor-hardware-report-input").on('keypress',function () { // 点击搜索按钮
      if(event.keyCode == "13") {
        var eqquipmentName = $("#monitor-hardware-report-input").val();
        $.ajax({ // 默认最近一天数据
          type: 'POST',
          url: url + ':9006/EquipmentMonitorHistory/DayData',
          data: JSON.stringify({
            "resourcePoolId": resourcesId,
            "operatingSystemId": systemId,
            "equipmentType": equipmentId,
            "equipmentName": eqquipmentName
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            var data = data.data;
            var xTime = [],
                cpu = [],
                gpu = [],
                connect = [],
                memory = [],
                mouseTime = [],
                mouseCpu = [],
                mouseGpu = [],
                mouseMemory = [],
                mouseConnect = [];
            for (var i = 0; i < data.lineData.length; i++) {
              xTime.push(data.lineData[i].datetime)
              cpu.push(data.lineData[i].cpu)
              gpu.push(data.lineData[i].gpu)
              memory.push(data.lineData[i].memory)
              connect.push(data.lineData[i].connectionNumber)
            }
            for (var k = 0; k < data.mouseData.length; k++) {
              mouseTime.push(data.mouseData[k].datetime)
              mouseCpu.push(data.mouseData[k].cpu)
              mouseGpu.push(data.mouseData[k].gpu)
              mouseMemory.push(data.mouseData[k].memory)
              mouseConnect.push(data.mouseData[k].connectionNumber)
            }
            // console.log(mouseTime, mouseCpu, mouseGpu, mouseMemory)
            var myChartLine = echarts.init(document.getElementById('mainLine'));
            optionLine = {
              tooltip: {
                // trigger: 'axis',
                trigger: 'item',
                formatter: function (params, ticket, callback) {
                  // console.log(data.mouseData)
                  var htmlStr = '';
                  for (var i = 0; i < params.length; i++) {
                    var param = params[i];
                    var xName = param.name; //x轴的名称
                    var seriesName = param.seriesName; //图例名称
                    var value = param.value; //y轴值
                    var color = param.color; //图例颜色

                    if (i === 0) {
                      htmlStr += xName + '<br/>'; //x轴的名称
                    }
                    htmlStr += '<div>';
                    //为了保证和原来的效果一样，这里自己实现了一个点的效果
                    htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>';

                    //圆点后面显示的文本
                    // htmlStr += seriesName + '：' + value + '%';

                    // htmlStr += '</div>';
                  }
                  for (var k = 0; k < data.mouseData.length; k++) {
                    // console.log(data.mouseData[k])
                    var mouse = data.mouseData[k];
                    var cpus = mouse.cpu;
                    var gpus = mouse.gpu;
                    var memorys = mouse.memory;
                    var times = mouse.datetime;
                    var connects = mouse.connectionNumber;
                    if (times === params.name) {
                      htmlStr += mouse.equipmentName + ' ' + 'cpu' + '：' + cpus + '%' + ' ' + 'gpu' + '：' + gpus + '%' + ' ' + 'memory' + '：' + memorys + '%' + ' ' + '连接数' + '：' + connects + '<br/>';
                      htmlStr += '</div>';
                    }
                    // console.log(times, xName, params.name)
                  }
                  return htmlStr;
                }

              },
              legend: {
                data: ['cpu', 'gpu', 'memory', '连接数'],
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              toolbox: {
                feature: {
                  // saveAsImage: {}  // 是否显示保存图片
                }
              },
              xAxis: {
                type: 'category',
                // boundaryGap: false,  // 是否顶头
                data: xTime,
                axisLabel: {
                  interval: 0,
                  rotate: 40
                },
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                name: 'cpu',
                type: 'line',
                data: cpu,
                color: 'red'
              },
                {
                  name: 'gpu',
                  type: 'line',
                  data: gpu,
                  color: 'blue'
                },
                {
                  name: 'memory',
                  type: 'line',
                  data: memory,
                  color: 'green'
                },
                {
                  name: '连接数',
                  type: 'line',
                  data: connect,
                  color: 'yellow'
                }
              ]
            };
            myChartLine.setOption(optionLine)
          }
        })
        $.ajax({ // 默认最近一天数据
          type: 'POST',
          url: url + ':9006/EquipmentMonitorHistory/ProjectCountByTimeType/day',
          data: JSON.stringify({
            "resourcePoolId": resourcesId,
            "operatingSystemId": systemId,
            "equipmentType": equipmentId,
            "equipmentName": eqquipmentName
          }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          dataType: "json",
          success: function (data) {
            console.log(data)
            if (data.code == 200) {
              var data = data.data;
              option = { // cpu
                tooltip: {
                  formatter: "{a} <br/>{b} : {c}%"
                },
                series: [{
                  name: 'cpu',
                  type: 'gauge',
                  min: 0,
                  max: 100,
                  startAngle: 180,
                  endAngle: 0,
                  axisLine: {
                    show: true,
                    lineStyle: {
                      width: 20,
                      color: [
                        [(data.cpu / 100), '#36ce9b'],
                        [1, '#576d85']
                      ]
                    }
                  },
                  splitLine: {
                    show: true,
                    length: 0,
                  },
                  axisTick: {
                    show: false,
                  },
                  axisLabel: {
                    show: true,
                    fontSize: 12,
                    "distance": -20
                  },
                  pointer: {
                    show: false,
                    length: '90%',
                    width: 0,
                  },
                  title: {
                    show: true,
                    offsetCenter: [0, '-25%'],
                    textStyle: {
                      color: '#36ce9b',
                      fontSize: 12
                    }
                  },
                  detail: {
                    show: true,
                    offsetCenter: [0, 0],
                    formatter: '{value}%',
                    textStyle: {
                      fontSize: 12
                    }
                  },
                  splitNumber: 1,
                  data: [{
                    value: data.cpu, //cpu下方数值
                    name: 'cpu',
                    textStyle: {
                      fontSize: 16
                    }
                  }]
                }]
              };
              myChart.setOption(option);

              option1 = {
                tooltip: {
                  formatter: "{a} <br/>{b} : {c}%"
                },
                series: [{
                  name: 'gpu',
                  type: 'gauge',
                  min: 0,
                  max: 100,
                  startAngle: 180,
                  endAngle: 0,
                  axisLine: {
                    show: true,
                    lineStyle: {
                      width: 20,
                      color: [
                        [(data.gpu / 100), '#36ce9b'],
                        [1, '#576d85']
                      ]
                    }
                  },
                  splitLine: {
                    show: true,
                    length: 0,
                  },
                  axisTick: {
                    show: false,
                  },
                  axisLabel: {
                    show: true,
                    fontSize: 12,
                    "distance": -20
                  },
                  pointer: {
                    show: false,
                    length: '90%',
                    width: 0,
                  },
                  title: {
                    show: true,
                    offsetCenter: [0, '-25%'],
                    textStyle: {
                      color: '#36ce9b',
                      fontSize: 12
                    }
                  },
                  detail: {
                    show: true,
                    offsetCenter: [0, 0],
                    formatter: '{value}%',
                    textStyle: {
                      fontSize: 12
                    }
                  },
                  splitNumber: 1,
                  data: [{
                    value: data.gpu, //cpu下方数值
                    name: 'gpu',
                    textStyle: {
                      fontSize: 16
                    }
                  }]
                }]
              };
              myChart1.setOption(option1);

              option2 = {
                tooltip: {
                  formatter: "{a} <br/>{b} : {c}%"
                },
                series: [{
                  name: '内存',
                  type: 'gauge',
                  min: 0,
                  max: 100,
                  startAngle: 180,
                  endAngle: 0,
                  axisLine: {
                    show: true,
                    lineStyle: {
                      width: 20,
                      color: [
                        [(data.memory / 100), '#36ce9b'],
                        [1, '#576d85']
                      ]
                    }
                  },
                  splitLine: {
                    show: true,
                    length: 0,
                  },
                  axisTick: {
                    show: false,
                  },
                  axisLabel: {
                    show: true,
                    fontSize: 12,
                    "distance": -20
                  },
                  pointer: {
                    show: false,
                    length: '90%',
                    width: 0,
                  },
                  title: {
                    show: true,
                    offsetCenter: [0, '-25%'],
                    textStyle: {
                      color: '#36ce9b',
                      fontSize: 12
                    }
                  },
                  detail: {
                    show: true,
                    offsetCenter: [0, 0],
                    formatter: '{value}%',
                    textStyle: {
                      fontSize: 12
                    }
                  },
                  splitNumber: 1,
                  data: [{
                    value: data.memory, //cpu下方数值
                    name: '内存',
                    textStyle: {
                      fontSize: 16
                    }
                  }]
                }]
              };
              myChart2.setOption(option2);
            } else {
              layer.msg(data.message)
            }
          }
        })
      }
    })

    $("#monitor-hardware-report-icon").click(function () {
      // console.log(123)
      $("#monitor-hardware-report-input").val('');
      $.ajax({ // 默认最近一天数据
        type: 'POST',
        url: url + ':9006/EquipmentMonitorHistory/DayData',
        data: JSON.stringify({
          "equipmentName": ''
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          var data = data.data;
          var xTime = [],
            cpu = [],
            gpu = [],
            connect = [],
            memory = [],
            mouseTime = [],
            mouseCpu = [],
            mouseGpu = [],
            mouseMemory = [],
            mouseConnect = [];
          for (var i = 0; i < data.lineData.length; i++) {
            xTime.push(data.lineData[i].datetime)
            cpu.push(data.lineData[i].cpu)
            gpu.push(data.lineData[i].gpu)
            memory.push(data.lineData[i].memory)
            connect.push(data.lineData[i].connectionNumber)
          }
          for (var k = 0; k < data.mouseData.length; k++) {
            mouseTime.push(data.mouseData[k].datetime)
            mouseCpu.push(data.mouseData[k].cpu)
            mouseGpu.push(data.mouseData[k].gpu)
            mouseMemory.push(data.mouseData[k].memory)
            mouseConnect.push(data.mouseData[k].connectionNumber)
          }
          // console.log(mouseTime, mouseCpu, mouseGpu, mouseMemory)
          var myChartLine = echarts.init(document.getElementById('mainLine'));
          optionLine = {
            tooltip: {
              // trigger: 'axis',
              trigger: 'item',
              formatter: function (params, ticket, callback) {
                // console.log(data.mouseData)
                var htmlStr = '';
                for (var i = 0; i < params.length; i++) {
                  var param = params[i];
                  var xName = param.name; //x轴的名称
                  var seriesName = param.seriesName; //图例名称
                  var value = param.value; //y轴值
                  var color = param.color; //图例颜色

                  if (i === 0) {
                    htmlStr += xName + '<br/>'; //x轴的名称
                  }
                  htmlStr += '<div>';
                  //为了保证和原来的效果一样，这里自己实现了一个点的效果
                  htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>';

                  //圆点后面显示的文本
                  // htmlStr += seriesName + '：' + value + '%';

                  // htmlStr += '</div>';
                }
                for (var k = 0; k < data.mouseData.length; k++) {
                  // console.log(data.mouseData[k])
                  var mouse = data.mouseData[k];
                  var cpus = mouse.cpu;
                  var gpus = mouse.gpu;
                  var memorys = mouse.memory;
                  var times = mouse.datetime;
                  var connects = mouse.connectionNumber;
                  if (times === params.name) {
                    htmlStr += mouse.equipmentName + ' ' + 'cpu' + '：' + cpus + '%' + ' ' + 'gpu' + '：' + gpus + '%' + ' ' + 'memory' + '：' + memorys + '%' + ' ' + '连接数' + '：' + connects + '<br/>';
                    htmlStr += '</div>';
                  }
                  // console.log(times, xName, params.name)
                }
                return htmlStr;
              }

            },
            legend: {
              data: ['cpu', 'gpu', 'memory', '连接数'],
            },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true
            },
            toolbox: {
              feature: {
                // saveAsImage: {}  // 是否显示保存图片 
              }
            },
            xAxis: {
              type: 'category',
              // boundaryGap: false,  // 是否顶头
              data: xTime,
              axisLabel: {
                interval: 0,
                rotate: 40
              },
            },
            yAxis: {
              type: 'value'
            },
            series: [{
                name: 'cpu',
                type: 'line',
                data: cpu,
                color: 'red'
              },
              {
                name: 'gpu',
                type: 'line',
                data: gpu,
                color: 'blue'
              },
              {
                name: 'memory',
                type: 'line',
                data: memory,
                color: 'green'
              },
              {
                name: '连接数',
                type: 'line',
                data: connect,
                color: 'yellow'
              }
            ]
          };
          myChartLine.setOption(optionLine)
        }
      })
      $.ajax({ // 默认最近一天数据
        type: 'POST',
        url: url + ':9006/EquipmentMonitorHistory/ProjectCountByTimeType/day',
        data: JSON.stringify({
          
          "equipmentName": ''
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          if (data.code == 200) {
            var data = data.data;
            option = { // cpu
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'cpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.cpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.cpu, //cpu下方数值
                  name: 'cpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart.setOption(option);

            option1 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'gpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.gpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.gpu, //cpu下方数值
                  name: 'gpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart1.setOption(option1);

            option2 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: '内存',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.memory / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.memory, //cpu下方数值
                  name: '内存',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart2.setOption(option2);
          } else {
            layer.msg(data.message)
          }
        }
      })
    })

    $(".hardwareReportQuery").click(function () { // 点击查询按钮
      // console.log(resourcesId, systemId, equipmentId, dataTypes)
      var stateTime = $("#test1").val()
      var endTime = $("#test2").val()
      var eqquipmentName = $("#monitor-hardware-report-input").val();
      if (stateTime == '' || endTime == '') {
        layer.msg('请输入正确的查询时间');
        return false
      }
      $.ajax({ // 自定义时间
        type: 'POST',
        url: url + ':9006/EquipmentMonitorHistory/DIYTime/' + stateTime + '/' + endTime,
        // url:  'http://10.1.1.213:9006/EquipmentMonitorHistory/DIYTime/'+stateTime+'/'+endTime,
        data: JSON.stringify({
          "resourcePoolId": resourcesId,
          "operatingSystemId": systemId,
          "equipmentType": equipmentId,
          "equipmentName": eqquipmentName
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          console.log(data)
          if (data.code == 200) {
            var data = data.data;
            var xTime = [],
              cpu = [],
              gpu = [],
              memory = [],
              connect = [],
              mouseTime = [],
              mouseCpu = [],
              mouseGpu = [],
              mouseMemory = [],
              mouseConnect = [];
            for (var i = 0; i < data.lineData.length; i++) {
              xTime.push(data.lineData[i].datetime)
              cpu.push(data.lineData[i].cpu)
              gpu.push(data.lineData[i].gpu)
              memory.push(data.lineData[i].memory)
              connect.push(data.lineData[i].connectionNumber)
            }
            for (var k = 0; k < data.mouseData.length; k++) {
              mouseTime.push(data.mouseData[k].datetime)
              mouseCpu.push(data.mouseData[k].cpu)
              mouseGpu.push(data.mouseData[k].gpu)
              mouseMemory.push(data.mouseData[k].memory)
              mouseConnect.push(data.mouseData[k].connectionNumber)
            }
            // console.log(mouseTime, mouseCpu, mouseGpu, mouseMemory)
            var myChartLine = echarts.init(document.getElementById('mainLine'));
            optionLine = {
              tooltip: {
                trigger: 'item',
                formatter: function (params, ticket, callback) {
                  // console.log(data.mouseData)
                  var htmlStr = '';
                  for (var i = 0; i < params.length; i++) {
                    var param = params[i];
                    var xName = param.name; //x轴的名称
                    var seriesName = param.seriesName; //图例名称
                    var value = param.value; //y轴值
                    var color = param.color; //图例颜色

                    if (i === 0) {
                      htmlStr += xName + '<br/>'; //x轴的名称
                    }
                    htmlStr += '<div>';
                    //为了保证和原来的效果一样，这里自己实现了一个点的效果
                    htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>';

                    //圆点后面显示的文本
                    // htmlStr += seriesName + '：' + value + '%';

                    // htmlStr += '</div>';
                  }
                  for (var k = 0; k < data.mouseData.length; k++) {
                    // console.log(data.mouseData[k])
                    var mouse = data.mouseData[k];
                    var cpus = mouse.cpu;
                    var gpus = mouse.gpu;
                    var memorys = mouse.memory;
                    var times = mouse.datetime;
                    var connects = mouse.connectionNumber;
                    if (times === params.name) {
                      htmlStr += mouse.equipmentName + ' ' + 'cpu' + '：' + cpus + '%' + ' ' + 'gpu' + '：' + gpus + '%' + ' ' + 'memory' + '：' + memorys + '%' + ' ' + '连接数' + '：' + connects + '<br/>';
                      htmlStr += '</div>';
                    }
                    // console.log(times, xName, params.name)
                  }
                  return htmlStr;
                }
              },
              legend: {
                data: ['cpu', 'gpu', 'memory', '连接数']
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              toolbox: {
                feature: {
                  // saveAsImage: {}  // 是否显示保存图片 
                }
              },
              xAxis: {
                type: 'category',
                // boundaryGap: false,
                data: xTime
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                  name: 'cpu',
                  type: 'line',
                  data: cpu,
                  color: 'red'
                },
                {
                  name: 'gpu',
                  type: 'line',
                  data: gpu,
                  color: 'blue'
                },
                {
                  name: 'memory',
                  type: 'line',
                  data: memory,
                  color: 'green'
                },
                {
                  name: '连接数',
                  type: 'line',
                  data: connect,
                  color: 'yellow'
                }
              ]
            };
            myChartLine.setOption(optionLine)
          } else {
            layer.msg(data.message)
          }
        }
      })

      $.ajax({ // 仪表盘数据
        type: 'POST',
        url: url + ':9006/EquipmentMonitorHistory/ProjectCountByDIYTime/' + stateTime + '/' + endTime,
        data: JSON.stringify({
          "resourcePoolId": resourcesId,
          "operatingSystemId": systemId,
          "equipmentType": equipmentId,
          "equipmentName": eqquipmentName
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        dataType: "json",
        success: function (data) {
          // console.log(data)
          if (data.code == 200) {
            var data = data.data;
            option = { // cpu
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'cpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.cpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.cpu, //cpu下方数值
                  name: 'cpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart.setOption(option);

            option1 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: 'gpu',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.gpu / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.gpu, //cpu下方数值
                  name: 'gpu',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart1.setOption(option1);

            option2 = {
              tooltip: {
                formatter: "{a} <br/>{b} : {c}%"
              },
              series: [{
                name: '内存',
                type: 'gauge',
                min: 0,
                max: 100,
                startAngle: 180,
                endAngle: 0,
                axisLine: {
                  show: true,
                  lineStyle: {
                    width: 20,
                    color: [
                      [(data.memory / 100), '#36ce9b'],
                      [1, '#576d85']
                    ]
                  }
                },
                splitLine: {
                  show: true,
                  length: 0,
                },
                axisTick: {
                  show: false,
                },
                axisLabel: {
                  show: true,
                  fontSize: 12,
                  "distance": -20
                },
                pointer: {
                  show: false,
                  length: '90%',
                  width: 0,
                },
                title: {
                  show: true,
                  offsetCenter: [0, '-25%'],
                  textStyle: {
                    color: '#36ce9b',
                    fontSize: 12
                  }
                },
                detail: {
                  show: true,
                  offsetCenter: [0, 0],
                  formatter: '{value}%',
                  textStyle: {
                    fontSize: 12
                  }
                },
                splitNumber: 1,
                data: [{
                  value: data.memory, //cpu下方数值
                  name: '内存',
                  textStyle: {
                    fontSize: 16
                  }
                }]
              }]
            };
            myChart2.setOption(option2);
          } else {
            layer.msg(data.message)
          }
        }
      })
    })

    $(".hardwareReportExport").click(function () { // 点击导出按钮
      var stateTime = $("#test1").val()
      var endTime = $("#test2").val()
      if (stateTime != '' || endTime != '') {
        downloadFileByForm(url + ":9006/EquipmentMonitorHistory/export", "diy")
        return false
      }
      switch (dataTypes) { //折线数据
        case '1': //导出天
          downloadFileByForm(url + ":9006/EquipmentMonitorHistory/export", "day")
          break;
        case '2': //导出星期
          downloadFileByForm(url + ":9006/EquipmentMonitorHistory/export", "week")
          break;
        case '3': //导出月
          downloadFileByForm(url + ":9006/EquipmentMonitorHistory/export", "month")
          break
          // case '4':   //导出自定义
          //   downloadFileByForm(url + ":9006/EquipmentMonitorHistory/export", "diy")
          // break
      }
    })

    function downloadFileByForm(url, queryType) { //导出按钮 公共函数
      console.log("ajaxDownloadSynchronized");
      // var fileName = appType;
      var stateTime = $("#test1").val()
      var endTime = $("#test2").val()
      var form = $("<form></form>").attr("action", url).attr("method", "get");
      form.append($("<input></input>").attr("type", "hidden").attr("name", "queryType").attr("value", queryType));
      form.append($("<input></input>").attr("type", "hidden").attr("name", "resourcePoolId").attr("value", resourcesId));
      form.append($("<input></input>").attr("type", "hidden").attr("name", "operatingSystemId").attr("value", systemId));
      form.append($("<input></input>").attr("type", "hidden").attr("name", "equipmentType").attr("value", equipmentId));
      form.append($("<input></input>").attr("type", "hidden").attr("name", "startTime").attr("value", stateTime)); //传递自定义开始时间参数
      form.append($("<input></input>").attr("type", "hidden").attr("name", "endTime").attr("value", endTime)); //传递自定义结束时间参数
      form.appendTo('body').submit().remove();
    }
  });
})()