vm = new Vue({
    el: '#app',
    data: function () {
        return {
            form: {
                id: '',
                intranetIp: '',
                name: '',
                describe: '',
                manufacturer: "",
                assetNumber: "",
                assetPropertyRightUnit: "",
                administrator: "",
                dialogtitle: "添加",
                assetPurchaseDate: "",
                multipleSelectionRows: [],
                options: [],
                searchoption: [],
                searchbeforeval: ''
            },
            sourceCopy: [],
            assetPropertyRightUnitid: '',
            assetPropertyRightUnitname: '',
            administratorid: '',
            draughtingId: "",
            orders: ["ascending", "descending"],
            currentPage: 1,
            size: 5,
            startTime: "",
            endTime: "",
            total: 0,
            dialogFormVisible: false,
            searchVal: "",
            searchdate: '',
            tableData: [],
            showPageData: [],
            selectindex: '',
            ids: [],
            //下拉树
            mineStatus: "", //产权单位
            treedata: [],
            defaultProps: {
                children: "children",
                label: "name"
            },
        }
    },
    methods: {
        // 下拉树选择器
        handleCheckChange(data, checked, node) {
            let res = this.$refs.tree.getCheckedNodes(false, true); //true，1. 是否只是叶子节点 2.选择的时候不包含父节点）
            if (checked) {
                this.$refs.tree.setCheckedNodes([data]);
            }
            let arrLabel = [];
            // console.log(res, 'res')
            res.forEach(item => {
                if (arrLabel.length === 0) {
                    arrLabel.push(item.name);
                } else {
                    arrLabel.length === 0;
                }
            });
            this.mineStatus = arrLabel;
            // console.log(arrLabel, 'arrLabel')
        },
        // 获取绘图仪信息
        init() {
            var _that = this
            $.ajax({
                url: url + ':9001/draughtingInstrument/page',
                type: 'GET',
                headers: {
                    "token": token,
                    "Content-Type": "application/json;charset=utf-8"
                },
                success: function (data) {
                    console.log(data, "获取绘图仪信息")
                    // _that.$set(_that,'showPageData',data.data)
                    _that.showPageData = data.data
                    // _that.$set(_that,'form.searchoption',data.data)
                    _that.form.searchoption = data.data
                },
            });
        },
        selectHandler(name) {},
        handleChange(value) {},
        // 添加
        addmapping() {
            this.form.dialogtitle = "添加"
            this.dialogFormVisible = true
            this.cleardata()
        },
        cleardata() {
            this.form.intranetIp = ''
            this.form.name = ''
            this.form.describe = ''
            this.form.manufacturer = ''
            this.form.assetNumber = ''
            this.form.intranetIp = ''
            this.form.assetPropertyRightUnit = ''
            this.form.administrator = ''
            this.form.assetPurchaseDate = ''
            this.mineStatus = ''
        },

        objDeepCopy(source) { //遍历产权单位获取id值
            var that = this
            var sourceCopy = source instanceof Array ? [] : {};
            for (var item in source) {
                sourceCopy[item] =
                    typeof source[item] === "object" ? that.objDeepCopy(source[item]) : source[item];
            }
            if (sourceCopy.id) {
                that.sourceCopy.push({
                    id: sourceCopy.id,
                    name: sourceCopy.name
                })
            }
        },
        editdata(index) {
            this.dialogFormVisible = true
            this.form.id = this.showPageData[index].id
            this.form.intranetIp = this.showPageData[index].intranetIp
            this.form.name = this.showPageData[index].name
            this.form.describe = this.showPageData[index].describe
            this.form.manufacturer = this.showPageData[index].manufacturer
            this.form.assetNumber = this.showPageData[index].assetNumber
            this.form.intranetIp = this.showPageData[index].intranetIp
            // this.form.administrator = this.showPageData[index].administrator
            // this.mineStatus = this.showPageData[index].assetPropertyRightUnit.split(",")
            this.mineStatus = this.assetPropertyRightUnitname.split(",")
            this.form.assetPurchaseDate = this.showPageData[index].assetPurchaseDate
        },

        // 编辑按钮、单击图片
        editmapping(e, index) {
            var _index = index;
            this.form.dialogtitle = "编辑"
            if (this.selectindex !== '') {
                index = this.selectindex
            }
            if (e.target.nodeName === 'IMG') {
                // 产权单位name展示
                this.sourceCopy.forEach(item => {
                    if (item.id == this.showPageData[_index].assetPropertyRightUnit || item.name == this.showPageData[_index].assetPropertyRightUnit) {
                        this.assetPropertyRightUnitname = item.name
                    }
                })
                //绘图管理员name展示
                for (var i = 0; i < this.form.options.length; i++) {
                    if (this.form.options[i].id == this.showPageData[_index].administrator) {
                        this.form.administrator = this.form.options[i].realName
                        break
                        // console.log(this.showPageData[_index].administrator,this.form.administrator,'123445566')
                    }
                }
                this.editdata(_index)
            } else if (this.form.multipleSelectionRows.length !== 1) {
                layer.msg("请选择一项进行编辑", {
                    icon: 7,
                    closeBtn: 1
                });
            } else {
                //绘图管理员name展示
                for (var i = 0; i < this.form.options.length; i++) {
                    if (this.form.options[i].id == this.showPageData[index].administrator) {
                        this.form.administrator = this.form.options[i].realName
                    }
                }
                // 产权单位name展示
                this.sourceCopy.forEach(item => {
                    if (item.id == this.showPageData[index].assetPropertyRightUnit || item.name == this.showPageData[index].assetPropertyRightUnit) {
                        this.assetPropertyRightUnitname = item.name
                    }
                })
                this.editdata(index)
            }
        },
        // 删除按钮
        deltetmapping() {
            var _this = this;
            if (this.form.multipleSelectionRows.length === 0) {
                layer.msg("请至少选择一项删除", {
                    icon: 7,
                    closeBtn: 1
                });
                return
            }
            this.$confirm('您确定要删除?', '删除', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
            }).then(() => {
                for (var i = 0; i < this.form.multipleSelectionRows.length; i++) {
                    if (this.ids.indexOf(this.showPageData[this.form.multipleSelectionRows[i]].id) === -1) {
                        this.ids.push(this.showPageData[this.form.multipleSelectionRows[i]].id)
                    } else {
                        this.ids.splice(this.ids.indexOf(this.showPageData[this.form.multipleSelectionRows[i]].id), 1)
                    }
                    //    console.log(_this.ids ,'uuuuuuu') 
                }
                $.ajax({
                    url: url + ':9001/draughtingInstrument/deleteList',
                    type: 'POST',
                    contentType: "application/json",
                    headers: {
                        "token": token,
                    },
                    dataType: 'json',
                    data: JSON.stringify(_this.ids),
                    success: function (data) {
                        // console.log(data, '删除')
                        if (data.code == 200) {
                            _this.init()
                            layer.msg(data.message, {
                                icon: 6,
                                closeBtn: 1
                            });
                        } else {
                            layer.msg(data.message, {
                                icon: 7,
                                closeBtn: 1
                            });
                        }

                    },
                });
            }).catch(() => {});
        },
        // dialog确认提交
        onSubmit() {
            if (this.form.intranetIp == '' || this.form.name == '' || this.form.administrator == '') {
                layer.msg('必填项不能为空', {
                    icon: 7,
                    closeBtn: 1
                });
            }
            // 添加
            
            if (this.form.dialogtitle === "添加") {
                var resDate = null;
                if (this.form.assetPurchaseDate == '') {} else {
                    var d = new Date(this.form.assetPurchaseDate); //日期格式转换
                    resDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                }

                // 绘图管理员id转换
                for (var i = 0; i < this.form.options.length; i++) {
                    if (this.form.options[i].realName == this.form.administrator) {
                        this.administratorid = this.form.options[i].id
                    }
                }
                // 产权单位id转换
                this.sourceCopy.forEach(item => {
                    // console.log(item.name,this.mineStatus.join(),'this.mineStatus')
                    if (item.name == this.mineStatus) {
                        this.assetPropertyRightUnit = item.id
                    }
                })

                var newdata = {
                    id: this.form.id,
                    intranetIp: this.form.intranetIp,
                    name: this.form.name,
                    describe: this.form.describe,
                    manufacturer: this.form.manufacturer,
                    assetNumber: this.form.assetNumber,
                    assetPropertyRightUnit: this.assetPropertyRightUnit == undefined ? '' : this.assetPropertyRightUnit,
                    administrator: this.administratorid,
                    assetPurchaseDate: resDate
                }
                this.dialogFormVisible = false;
                console.log(newdata, 'newdata')
                var _this = this;
                $.ajax({
                    url: url + ':9001/draughtingInstrument/saveDraughtingInstrument',
                    type: 'POST',
                    contentType: "application/x-www-form-urlencoded",
                    headers: {
                        "token": token,
                    },
                    dataType: 'json',
                    data: newdata,
                    success: function (data) {
                        // console.log(data, '添加')
                        if (data.code == 200) {
                            _this.init()
                            _this.showplotdata()
                            layer.msg(data.message, {
                                icon: 6,
                                closeBtn: 1
                            });
                        } else {
                            console.log(data)
                            layer.msg(data.message, {
                                icon: 7,
                                closeBtn: 1
                            });
                        }
                    },
                });
            }
            // 编辑
            if (this.form.dialogtitle === "编辑") {
                var resDate = null;
                this.dialogFormVisible = false;
                var d = new Date(this.form.assetPurchaseDate);
                    resDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                //绘图仪id转换
                for (var i = 0; i < this.form.options.length; i++) {
                    if (this.form.options[i].realName == this.form.administrator) {
                        this.administratorid = this.form.options[i].id
                    }
                }
                // 产权单位id转换
                this.sourceCopy.forEach(item => {
                    // console.log(item.name,this.mineStatus.join(),'this.mineStatus')
                    if (item.name == this.mineStatus) {
                        this.assetPropertyRightUnit = item.id
                    }
                })
                var updatedata = {
                    id: this.form.id,
                    intranetIp: this.form.intranetIp,
                    name: this.form.name,
                    describe: this.form.describe,
                    manufacturer: this.form.manufacturer,
                    assetNumber: this.form.assetNumber,
                    assetPropertyRightUnit: this.assetPropertyRightUnit == undefined ? '' : this.assetPropertyRightUnit,
                    administrator: this.administratorid,
                    assetPurchaseDate: resDate
                }
                var _this = this
                // console.log(updatedata)
                $.ajax({
                    url: url + ':9001/draughtingInstrument/updateDraughtingInstrument',
                    type: 'POST',
                    contentType: "application/x-www-form-urlencoded",
                    headers: {
                        "token": token,
                    },
                    dataType: 'json',
                    data: updatedata,
                    success: function (data) {
                        // console.log(data, '编辑')
                        if (data.code == 200) {
                            _this.init()
                            _this.showplotdata()
                            layer.msg(data.message, {
                                icon: 6,
                                closeBtn: 1
                            });
                        } else {
                            console.log(data)
                            layer.msg(data.message, {
                                icon: 7,
                                closeBtn: 1
                            });
                        }
                    },
                });
            }
        },
        // 绘图仪信息多选框选定
        handlerChanger(index, id) {
            console.log(index, id, 'indexid')
            if (this.form.multipleSelectionRows.indexOf(index) === -1) {
                this.form.multipleSelectionRows.push(index)
                this.selectindex = index;
            } else {
                this.form.multipleSelectionRows.splice(this.form.multipleSelectionRows.indexOf(index), 1)
            }
        },
        closebtn() {
            this.dialogFormVisible = false;
        },

        // 绘图仪产权单位回显
        showplotdata(id) {
            var _that = this
            $.ajax({
                url: url + ':9001/draughtingInstrument/updateMessage',
                type: 'POST',
                contentType: "application/x-www-form-urlencoded",
                headers: {
                    "token": token,
                },
                dataType: 'json',
                // data:JSON.stringify(newdata),
                data: {
                    id
                },
                success: function (data) {
                    console.log(data, '绘图仪产权单位回显')
                    _that.treedata = data.data.orgList //产权单位   
                    _that.form.options = data.data.administrators //administrators
                    _that.objDeepCopy(_that.treedata) //深度遍历
                },
            });
        },

        //绘图仪使用情况 下方表格
        showtable() {
            $.ajax({
                url: url + ':9001/draughtingInstrument/monitorListPage',
                type: 'POST',
                contentType: "application/x-www-form-urlencoded",
                headers: {
                    "token": token,
                },
                dataType: 'json',
                // data:JSON.stringify(newdata),
                data: aaa,
                success: function (data) {
                    console.log(data, '绘图仪使用情况')
                },
            });
            this.init()
        },
        // 搜索
        selectHandler(value) {},
        // 表格数据
        getTableData(val) {
            var _this = this
            console.log(this.form.searchbeforeval)
            this.showPageData.forEach(item => {
                if (item.name == this.form.searchbeforeval) {
                    this.draughtingId = item.id
                }
            })
            $.ajax({
                url: url + ':9001/draughtingInstrument/monitorListPage',
                type: "post",
                contentType: "application/x-www-form-urlencoded",
                headers: {
                    "token": token,
                },
                data: {
                    draughtingId: this.draughtingId,
                    page: val || _this.currentPage,
                    size: _this.size,
                    conditions: _this.searchVal,
                    startTime: _this.startTime,
                    endTime: _this.endTime,
                },
                success: function (data) {
                    _this.tableData = data.data.content
                    _this.total = data.data.pageCount
                    // console.log(data,"getTableData")
                }
            })
        },
        // 导出报表
        handleExport() {
            window.open(url + ":9001/draughtingInstrument/export")
        },
        // 排序
        handleSort(column, prop, order) {
            this.sortString = column.prop
            if (column.order == "ascending") {
                this.sortMode = "asc"
            } else {
                this.sortMode = "desc"
            }
            this.getTableData()
        },
        // 分页
        handleCurrentChange(val) {
            this.currentPage = val
            this.getTableData(val)
        },
        // 查询按钮 日期+搜索框
        handleSearchDate() {
            // console.log(this.searchdate, this.searchVal)
            if (this.searchdate != '' && this.searchdate != null) {
                this.startTime = this.searchdate[0] + " 00:00:00"
                this.endTime = this.searchdate[1] + " 23:59:59"
            } else {
                this.startTime = ''
                this.endTime = ''
            }
            this.getTableData()
        },
        // 搜索按钮
        handleSearch() {
            this.getTableData()
        }
    },
    mounted() {
        var _this = this
        this.init()
        this.getTableData()
        this.showplotdata()
        setInterval(() => {
            _this.getTableData()
        }, 60000)

    },

})