(function() {
  //JavaScript代码区域
  layui.use(['jquery', 'element', 'form', 'tree', 'layedit', 'laydate', 'table', 'layer'], function() {
    var element = layui.element,
      table = layui.table,
      $ = layui.jquery,
      layer = layui.layer,
      tree = layui.tree,
      laydate = layui.laydate,
      form = layui.form;

    var params = {
      day: '7',
      userId: '',
      departmentId: '',
      stateTime: '',
      endTime: '',
      depType: 'userName',
      sort: 'desc',
      departmentName: ''
    };

    var active = {
      getTimeTypeData: function(day, userId, departmentId, departmentName) {
        $.ajax({
          //页面打开默认显示最近一周数据
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/TimeTypeData/' + day,
          data: JSON.stringify({ userId: userId, departmentId: departmentId, departmentName: departmentName }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            var data = data.data;
            var xName = [],
              users = [],
              avgTime = [],
              userNumber = [];
            for (var i = 0; i < data.length; i++) {
              xName.push(data[i].datetime);
              users.push(data[i].users);
              avgTime.push(data[i].avgTime);
              userNumber.push(data[i].userNumber);
            }
            myChart.clear()
            option = {
              title: {
                // text: '折线图堆叠'
              },
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  // 坐标轴指示器，坐标轴触发有效
                  type: 'line' // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params) {
                  // console.log(params)
                  var tar = params[0].dataIndex,
                    tar1 = params[1];
                  return '用户名:' + data[tar].users + '<br/>' + tar1.seriesName + ':' + tar1.value;
                }
              },
              grid: {
                top:10,
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              toolbox: {
                feature: {
                  // saveAsImage: {}  // 是否显示保存图片
                }
              },
              xAxis: {
                type: 'category',
                boundaryGap: false,
                data: xName
              },
              yAxis: {
                type: 'value',
                minInterval: 1
              },
              series: [
                {
                  name: '使用人数',
                  color: '#9ACD32',
                  type: 'line',
                  smooth: true,
                  data: userNumber
                },
                {
                  name: '平均工作时间(分钟)',
                  color: '#CD9B1D',
                  type: 'line',
                  smooth: true,
                  data: avgTime
                }
              ]
            };
            myChart.setOption(option);
          }
        });
      },
      getTableDataByTimeType: function(day, depType, sort, userId, departmentId, departmentName) {
        $.ajax({
          //页面打开默认显示最近一周数据
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/TableDataByTimeType/10/0/' + depType + '/' + sort,
          data: JSON.stringify({ timeType: day, userId: userId, departmentId: departmentId, departmentName: departmentName }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            var data = data.data.content;
            table.render({
              elem: '#userTable',
              data: data,
              id: 'userTable',
              page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['prev', 'page', 'next', 'skip']
              },
              // width: 900,
              cols: [
                [
                  {
                    field: 'userName',
                    title: '用户',
                    sort: true,
                    unresize: true
                  },
                  {
                    field: 'parentDepartmentName',
                    title: '院所',
                    sort: true,
                    unresize: true
                  },
                  {
                    field: 'departmentName',
                    title: '科室',
                    sort: true,
                    unresize: true
                  },
                  {
                    field: 'loginCount',
                    title: '登录次数',
                    sort: true,
                    event: 'loginCount',
                    unresize: true
                  }, //修改
                  {
                    field: 'timeCount',
                    title: '工作时间(分钟)',
                    sort: true,
                    event: 'timeCount',
                    unresize: true
                  },
                  {
                    field: 'appCount',
                    title: '软件数量',
                    sort: true,
                    event: 'appCount',
                    unresize: true
                  }
                ]
              ]
            });
          }
        });
      },
      getTree: function(val) {
        $.ajax({
          url: url + ':9000/organization/getOrgTreeAndIncludeUsers',
          // url: 'http://10.1.3.98:9000/organization/getOrgTreeAndIncludeUsers',
          type: 'get',
          success: function(data) {
            layui.tree({
              elem: '#treeList', //指定元素
              nodes: data.data,
              spread: true,
              click: function (node) {
                console.log(node)
                if(node.children) {
                  params.departmentId = node.id
                  params.userId = ''
                  active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
                  active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
                }else{
                  params.userId = node.id
                  params.departmentId = ''
                  active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
                  active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
                }
              }
            })
          }
        })
        $('body').on('mousedown', '.layui-tree a', function () {
          $('.layui-tree a').css('background', '')
          $(this).css('background', 'rgb(18, 160, 255)')
        })
      }
    };

    active.getTree()

    form.on('select(organization)', function(data) {
      console.log(data);
      console.log(data.elem); //得到radio原始DOM对象
      console.log(data.value); //被点击的radio的value值
      $('.userBox').removeClass('display-none');
    });

    var myChart = echarts.init(document.getElementById('main'));
    active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
    active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);

    table.on('sort(userTable)', function(obj) {
      console.log(obj.field);
      console.log(obj.type);
      params.depType = obj.field;
      params.sort = obj.type;
      active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId);
    });

    var departmentId = '',
      userId = '',
      orgId = '',
      departmentName = '';

    
    
    $('body').on('mousedown', '.layui-tree a', function() {
      $('.layui-tree a').css('background', '#fff');
      $(this).css('background', 'rgb(18, 160, 255)');
    });

    $('.downpanel')
      .on('click', '.layui-select-title', function(e) {
        // 下拉树状图
        $('.layui-form-select')
          .not($(this).parents('.layui-form-select'))
          .removeClass('layui-form-selected');
        $(this)
          .parents('.downpanel')
          .toggleClass('layui-form-selected');
        layui.stope(e);
      })
      .on('click', 'dl i', function(e) {
        layui.stope(e);
      });

    var dataTypes;
    form.on('radio(dateType)', function(data) {
      //获取自定义时间
      var stateTime = $('#test1').val('');
      var endTime = $('#test2').val('');
      data.value == '1' ? params.day = '7' : params.day = '30'
      console.log(params.day)
      active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
      active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
    });

    form.on('select(selectUsers)', function(data) {
      // 选择用户
      params.userId = data.elem[data.elem.selectedIndex].title;
      active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
      active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
    });

    laydate.render({
      //页面自定义开始时间渲染
      elem: '#test1' //指定元素
    });
    laydate.render({
      //页面自定义结束时间渲染
      elem: '#test2' //指定元素
    });

    $('.softwareReportQuery').click(function() {
      // 点击查询按钮
      console.log(123)
      var stateTime = $('#test1').val();
      var endTime = $('#test2').val();
      var departmentName = $('#monitor-user-report-input').val();
      if (stateTime == '' || endTime == '') {
        layer.msg('请输入正确的查询时间');
        return false;
      }
      $.ajax({
        //获取最近一周echarts数据
        type: 'POST',
        url: url + ':9006/UserMonitorHistory/DIYTimeData/' + stateTime + '/' + endTime,
        // url: 'http://10.1.1.213:9006/UserMonitorHistory/DIYTimeData/' + stateTime + '/' + endTime,
        data: JSON.stringify({
          departmentId: orgId,
          userId: userId,
          departmentName: departmentName
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function(data) {
          console.log(data);
          var data = data.data;
          var xName = [],
            users = [],
            avgTime = [],
            userNumber = [];
          for (var i = 0; i < data.length; i++) {
            xName.push(data[i].datetime);
            users.push(data[i].users);
            avgTime.push(data[i].avgTime);
            userNumber.push(data[i].userNumber);
          }
          // console.log(xName, users, avgTime, userNumber)
          option = {
            title: {
              // text: '折线图堆叠'
            },
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                // 坐标轴指示器，坐标轴触发有效
                type: 'line' // 默认为直线，可选为：'line' | 'shadow'
              },
              formatter: function(params) {
                // console.log(params)
                var tar = params[0].dataIndex,
                  tar1 = params[1];
                return '用户名:' + data[tar].users + '<br/>' + tar1.seriesName + ':' + tar1.value;
              }
            },
            // legend: {
            //     data:['使用人数','平均工作时间(分钟)']
            // },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true
            },
            toolbox: {
              feature: {
                // saveAsImage: {}  // 是否显示保存图片
              }
            },
            xAxis: {
              type: 'category',
              boundaryGap: false,
              data: xName
            },
            yAxis: {
              type: 'value',
              minInterval: 1
            },
            series: [
              {
                name: '使用人数',
                color: '#9ACD32',
                type: 'line',
                smooth: true,
                data: userNumber
              },
              {
                name: '平均工作时间(分钟)',
                color: '#CD9B1D',
                type: 'line',
                smooth: true,
                data: avgTime
              }
            ]
          };
          myChart.setOption(option);
        }
      });
      $.ajax({
        //获取最近一周表格数据
        type: 'POST',
        url: url + ':9006/UserMonitorHistory/TableDataByDIYTime/10/0/userName/desc',
        // url: 'http://10.1.1.213:9006/UserMonitorHistory/TableDataByDIYTime/' + stateTime + '/' + endTime,
        data: JSON.stringify({
          departmentId: orgId,
          userId: userId,
          startTime: stateTime,
          endTime: endTime
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function(data) {
          console.log(data);
          var data = data.data.content;
          table.render({
            elem: '#userTable',
            data: data,
            id: 'userTable',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
              layout: ['prev', 'page', 'next', 'skip']
            },
            // width: 900,
            cols: [
              [
                {
                  field: 'userName',
                  title: '用户',
                  sort: true,
                  unresize: true
                },
                {
                  field: 'parentDepartmentName',
                  title: '院所',
                  sort: true,
                  unresize: true
                },
                {
                  field: 'departmentName',
                  title: '科室',
                  sort: true,
                  unresize: true
                },
                {
                  field: 'loginCount',
                  title: '登录次数',
                  sort: true,
                  event: 'loginCount',
                  unresize: true
                }, //修改
                {
                  field: 'timeCount',
                  title: '工作时间(分钟)',
                  sort: true,
                  event: 'timeCount',
                  unresize: true
                },
                {
                  field: 'appCount',
                  title: '软件数量',
                  sort: true,
                  event: 'appCount',
                  unresize: true
                }
              ]
            ]
          });
        }
      });
      table.on('sort(userTable)', function(obj) {
        console.log(obj.field);
        console.log(obj.type);
        $.ajax({
          //获取最近一周表格数据
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/TableDataByDIYTime/10/0/' + obj.field + '/' + obj.type,
          // url: 'http://10.1.1.213:9006/UserMonitorHistory/TableDataByDIYTime/10/0/' + obj.field + "/" + obj.type,
          data: JSON.stringify({
            departmentId: orgId,
            userId: userId,
            startTime: stateTime,
            endTime: endTime
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            var data = data.data.content;
            table.render({
              elem: '#userTable',
              data: data,
              id: 'userTable',
              page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['prev', 'page', 'next', 'skip']
              },
              // width: 900,
              cols: [
                [
                  {
                    field: 'userName',
                    title: '用户',
                    sort: true,
                    unresize: true
                  },
                  {
                    field: 'parentDepartmentName',
                    title: '院所',
                    sort: true,
                    unresize: true
                  },
                  {
                    field: 'departmentName',
                    title: '科室',
                    sort: true,
                    unresize: true
                  },
                  {
                    field: 'loginCount',
                    title: '登录次数',
                    sort: true,
                    event: 'loginCount',
                    unresize: true
                  }, //修改
                  {
                    field: 'timeCount',
                    title: '工作时间(分钟)',
                    sort: true,
                    event: 'timeCount',
                    unresize: true
                  },
                  {
                    field: 'appCount',
                    title: '软件数量',
                    sort: true,
                    event: 'appCount',
                    unresize: true
                  }
                ]
              ]
            });
          }
        });
      });
    });

    $('.softwareReportExport').click(function() {
      // 点击导出按钮
      var stateTime = $('#test1').val();
      var endTime = $('#test2').val();
      if (stateTime != '' || endTime != '') {
        downloadFileByForm(url + ':9006/UserMonitorHistory/export/', 'diy');
        return false;
        // console.log("执行")
      }

      switch (
        dataTypes //折线数据
      ) {
        case '1': //导出星期
          downloadFileByForm(url + ':9006/UserMonitorHistory/export', 'week');
          break;
        case '2': //导出月
          downloadFileByForm(url + ':9006/UserMonitorHistory/export', 'month');
          break;
        // case '3': //导出自定义
        //   downloadFileByForm(url + ":9006/UserMonitorHistory/export/", "diy")
        //   break
      }
    });

    $('#monitor-user-report-btn').click(function() {
      // 点击搜索按钮
      var departmentName = $('#monitor-user-report-input').val();
      params.departmentName = departmentName
      active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
      active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
    });
    $('#monitor-user-report-input').on('keypress',function() {
      // 点击搜索按钮
        if(event.keyCode == "13") {
            var departmentName = $('#monitor-user-report-input').val();
          // 点击搜索按钮
          var departmentName = $('#monitor-user-report-input').val();
          params.departmentName = departmentName
          active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
          active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
            // $.ajax({
            //     //获取最近一周echarts数据
            //     type: 'POST',
            //     url: url + ':9006/UserMonitorHistory/TimeTypeData/7',
            //     data: JSON.stringify({
            //         departmentId: orgId,
            //         userId: userId,
            //         departmentName: departmentName
            //     }),
            //     headers: {
            //         'Content-Type': 'application/json;charset=utf-8'
            //     },
            //     dataType: 'json',
            //     success: function (data) {
            //         console.log(data);
            //         var data = data.data;
            //         var xName = [],
            //             users = [],
            //             avgTime = [],
            //             userNumber = [];
            //         for (var i = 0; i < data.length; i++) {
            //             xName.push(data[i].datetime);
            //             users.push(data[i].users);
            //             avgTime.push(data[i].avgTime);
            //             userNumber.push(data[i].userNumber);
            //         }
            //         // console.log(xName, users, avgTime, userNumber)
            //         option = {
            //             title: {
            //                 // text: '折线图堆叠'
            //             },
            //             tooltip: {
            //                 trigger: 'axis',
            //                 axisPointer: {
            //                     // 坐标轴指示器，坐标轴触发有效
            //                     type: 'line' // 默认为直线，可选为：'line' | 'shadow'
            //                 },
            //                 formatter: function (params) {
            //                     // console.log(params)
            //                     var tar = params[0].dataIndex,
            //                         tar1 = params[1];
            //                     return '用户名:' + data[tar].users + '<br/>' + tar1.seriesName + ':' + tar1.value;
            //                 }
            //             },
            //             // legend: {
            //             //     data:['使用人数','平均工作时间(分钟)']
            //             // },
            //             grid: {
            //                 left: '3%',
            //                 right: '4%',
            //                 bottom: '3%',
            //                 containLabel: true
            //             },
            //             toolbox: {
            //                 feature: {
            //                     // saveAsImage: {}  // 是否显示保存图片
            //                 }
            //             },
            //             xAxis: {
            //                 type: 'category',
            //                 boundaryGap: false,
            //                 data: xName
            //             },
            //             yAxis: {
            //                 type: 'value',
            //                 minInterval: 1
            //             },
            //             series: [
            //                 {
            //                     name: '使用人数',
            //                     color: '#9ACD32',
            //                     type: 'line',
            //                     smooth: true,
            //                     data: userNumber
            //                 },
            //                 {
            //                     name: '平均工作时间(分钟)',
            //                     color: '#CD9B1D',
            //                     type: 'line',
            //                     smooth: true,
            //                     data: avgTime
            //                 }
            //             ]
            //         };
            //         myChart.setOption(option);
            //     }
            // });
            // $.ajax({
            //     //获取最近一周表格数据
            //     type: 'POST',
            //     url: url + ':9006/UserMonitorHistory/TableDataByTimeType/7',
            //     data: JSON.stringify({
            //         departmentId: orgId,
            //         userId: userId,
            //         departmentName: departmentName
            //     }),
            //     headers: {
            //         'Content-Type': 'application/json;charset=utf-8'
            //     },
            //     dataType: 'json',
            //     success: function (data) {
            //         console.log(data);
            //         var data = data.data.content;
            //         table.render({
            //             elem: '#userTable',
            //             data: data,
            //             id: 'userTable',
            //             page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            //               layout: ['prev', 'page', 'next', 'skip']
            //             },
            //             // width: 900,
            //             cols: [
            //                 [
            //                     {
            //                         field: 'userName',
            //                         title: '用户',
            //                         sort: true,
            //                         unresize: true
            //                     },
            //                     {
            //                         field: 'parentDepartmentName',
            //                         title: '院所',
            //                         sort: true,
            //                         unresize: true
            //                     },
            //                     {
            //                         field: 'departmentName',
            //                         title: '科室',
            //                         sort: true,
            //                         unresize: true
            //                     },
            //                     {
            //                         field: 'loginCount',
            //                         title: '登录次数',
            //                         sort: true,
            //                         event: 'loginCount',
            //                         unresize: true
            //                     }, //修改
            //                     {
            //                         field: 'timeCount',
            //                         title: '工作时间(分钟)',
            //                         sort: true,
            //                         event: 'timeCount',
            //                         unresize: true
            //                     },
            //                     {
            //                         field: 'appCount',
            //                         title: '软件数量',
            //                         sort: true,
            //                         event: 'appCount',
            //                         unresize: true
            //                     }
            //                 ]
            //             ]
            //         });
            //     }
            // });
        }
    });

    $('#monitor-user-report-icon').click(function() {
      //清空按钮
      $('#monitor-user-report-input').val('');
      var departmentName = $('#monitor-user-report-input').val();
      // $.ajax({
      //   //获取最近一周echarts数据
      //   type: 'POST',
      //   url: url + ':9006/UserMonitorHistory/TimeTypeData/7',
      //   data: JSON.stringify({
      //     departmentName: ''
      //   }),
      //   headers: {
      //     'Content-Type': 'application/json;charset=utf-8'
      //   },
      //   dataType: 'json',
      //   success: function(data) {
      //     console.log(data);
      //     var data = data.data;
      //     var xName = [],
      //       users = [],
      //       avgTime = [],
      //       userNumber = [];
      //     for (var i = 0; i < data.length; i++) {
      //       xName.push(data[i].datetime);
      //       users.push(data[i].users);
      //       avgTime.push(data[i].avgTime);
      //       userNumber.push(data[i].userNumber);
      //     }
      //     // console.log(xName, users, avgTime, userNumber)
      //     option = {
      //       title: {
      //         // text: '折线图堆叠'
      //       },
      //       tooltip: {
      //         trigger: 'axis',
      //         axisPointer: {
      //           // 坐标轴指示器，坐标轴触发有效
      //           type: 'line' // 默认为直线，可选为：'line' | 'shadow'
      //         },
      //         formatter: function(params) {
      //           // console.log(params)
      //           var tar = params[0].dataIndex,
      //             tar1 = params[1];
      //           return '用户名:' + data[tar].users + '<br/>' + tar1.seriesName + ':' + tar1.value;
      //         }
      //       },
      //       // legend: {
      //       //     data:['使用人数','平均工作时间(分钟)']
      //       // },
      //       grid: {
      //         left: '3%',
      //         right: '4%',
      //         bottom: '3%',
      //         containLabel: true
      //       },
      //       toolbox: {
      //         feature: {
      //           // saveAsImage: {}  // 是否显示保存图片
      //         }
      //       },
      //       xAxis: {
      //         type: 'category',
      //         boundaryGap: false,
      //         data: xName
      //       },
      //       yAxis: {
      //         type: 'value',
      //         minInterval: 1
      //       },
      //       series: [
      //         {
      //           name: '使用人数',
      //           color: '#9ACD32',
      //           type: 'line',
      //           smooth: true,
      //           data: userNumber
      //         },
      //         {
      //           name: '平均工作时间(分钟)',
      //           color: '#CD9B1D',
      //           type: 'line',
      //           smooth: true,
      //           data: avgTime
      //         }
      //       ]
      //     };
      //     myChart.setOption(option);
      //   }
      // });
      // $.ajax({
      //   //获取最近一周表格数据
      //   type: 'POST',
      //   url: url + ':9006/UserMonitorHistory/TableDataByTimeType/7',
      //   data: JSON.stringify({
      //     departmentName: ''
      //   }),
      //   headers: {
      //     'Content-Type': 'application/json;charset=utf-8'
      //   },
      //   dataType: 'json',
      //   success: function(data) {
      //     console.log(data);
      //     var data = data.data.content;
      //     table.render({
      //       elem: '#userTable',
      //       data: data,
      //       id: 'userTable',
      //       page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
      //         layout: ['prev', 'page', 'next', 'skip']
      //       },
      //       // width: 900,
      //       cols: [
      //         [
      //           {
      //             field: 'userName',
      //             title: '用户',
      //             sort: true,
      //             unresize: true
      //           },
      //           {
      //             field: 'parentDepartmentName',
      //             title: '院所',
      //             sort: true,
      //             unresize: true
      //           },
      //           {
      //             field: 'departmentName',
      //             title: '科室',
      //             sort: true,
      //             unresize: true
      //           },
      //           {
      //             field: 'loginCount',
      //             title: '登录次数',
      //             sort: true,
      //             event: 'loginCount',
      //             unresize: true
      //           }, //修改
      //           {
      //             field: 'timeCount',
      //             title: '工作时间(分钟)',
      //             sort: true,
      //             event: 'timeCount',
      //             unresize: true
      //           },
      //           {
      //             field: 'appCount',
      //             title: '软件数量',
      //             sort: true,
      //             event: 'appCount',
      //             unresize: true
      //           }
      //         ]
      //       ]
      //     });
      //   }
      // });
      params.departmentName = departmentName
      active.getTimeTypeData(params.day, params.userId, params.departmentId, params.departmentName);
      active.getTableDataByTimeType(params.day, params.depType, params.sort, params.userId, params.departmentId, params.departmentName);
    });

    table.on('tool(userTable)', function(obj) {
      // 按照时间分类查询之后点击登录,工作,软件
      console.log(obj);
      var stateTime = $('#test1').val();
      var endTime = $('#test2').val();
      var idInfo = obj.data.userId;
      // console.log(dataTypes)
      if (obj.event === 'timeCount') {
        // 点击工作时间
        // console.log('工作时间:'+ obj.data.timeCount+'分钟')
        // console.log(obj.event)
        switch (dataTypes) {
          case '1':
            $.ajax({
              // 点击工作时间
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/WorkTimeCountByTimeType/7',
              // url: 'http://10.1.1.213:9006/UserMonitorHistory/WorkTimeCountByTimeType/7',
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'timeInfo';
                  oDiv.innerText = data[i].timeCount;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
          case '2':
            $.ajax({
              //点击工作时间
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/WorkTimeCountByTimeType/30',
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                // var data = data.data;
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'timeInfo';
                  oDiv.innerText = data[i].timeCount;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
          case '3':
            $.ajax({
              //点击工作时间
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/WorkTimeCountByDIYTime/' + stateTime + '/' + endTime,
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                // var data = data.data;
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'timeInfo';
                  oDiv.innerText = data[i].timeCount;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
        }
      }

      if (obj.event === 'appCount') {
        // 点击软件数量
        console.log('软件:' + obj.data.appCount);
        console.log(obj.event);
        switch (dataTypes) {
          case '1':
            $.ajax({
              //
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/AppsByTimeType/7',
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'appInfo';
                  oDiv.innerText = data[i].appName;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
          case '2':
            $.ajax({
              //
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/AppsByTimeType/30',
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'appInfo';
                  oDiv.innerText = data[i].appName;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
          case '3':
            $.ajax({
              //
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/AppsByDIYTime/' + stateTime + '/' + endTime,
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'appInfo';
                  oDiv.innerText = data[i].appName;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
        }
      }

      if (obj.event === 'loginCount') {
        // 点击登录次数
        switch (dataTypes) {
          case '1':
            $.ajax({
              //
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/loginLogByTimeType/7',
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'loginInformation';
                  oDiv.innerText = `${data[i].loginTime} - ${data[i].logoutTime}`;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
          case '2':
            $.ajax({
              //
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/loginLogByTimeType/30',
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'loginInformation';
                  oDiv.innerText = `${data[i].loginTime} - ${data[i].logoutTime}`;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
          case '3':
            $.ajax({
              //
              type: 'POST',
              url: url + ':9006/UserMonitorHistory/loginLogByTimeType/' + stateTime + '/' + endTime,
              data: JSON.stringify({
                userId: idInfo
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data);
                $('#userDetailHtml').text('');
                openInfo();
                var data = data.data;
                for (var i = 0; i < data.length; i++) {
                  var oDiv = document.createElement('div');
                  oDiv.className = 'loginInformation';
                  oDiv.innerText = `${data[i].loginTime} - ${data[i].logoutTime}`;

                  $('#userDetailHtml').append(oDiv);
                }
              }
            });
            break;
        }
      }
    });

    table.on('tool(userTable)', function(obj) {
      // 页面初始加载点击表格
      var idInfo = obj.data.userId;
      if (obj.event === 'timeCount') {
        // 点击工作时间
        $.ajax({
          // 点击工作时间
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/WorkTimeCountByTimeType/7',
          // url: 'http://10.1.1.213:9006/UserMonitorHistory/WorkTimeCountByTimeType/7',
          data: JSON.stringify({
            userId: idInfo
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $('#userDetailHtml').text('');
            openInfo();
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              var oDiv = document.createElement('div');
              oDiv.className = 'timeInfo';
              oDiv.innerText = data[i].timeCount;

              $('#userDetailHtml').append(oDiv);
            }
          }
        });
      }

      if (obj.event === 'appCount') {
        // 点击软件数量
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/AppsByTimeType/7',
          data: JSON.stringify({
            userId: idInfo
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $('#userDetailHtml').text('');
            openInfo();
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              var oDiv = document.createElement('div');
              oDiv.className = 'appInfo';
              oDiv.innerText = data[i].appName;

              $('#userDetailHtml').append(oDiv);
            }
          }
        });
      }

      if (obj.event === 'loginCount') {
        // 点击登录次数
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitorHistory/loginLogByTimeType/7',
          data: JSON.stringify({
            userId: idInfo
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            $('#userDetailHtml').text('');
            openInfo();
            var data = data.data;
            for (var i = 0; i < data.length; i++) {
              var oDiv = document.createElement('div');
              oDiv.className = 'loginInformation';
              oDiv.innerText = `${data[i].loginTime} - ${data[i].logoutTime}`;

              $('#userDetailHtml').append(oDiv);
            }
          }
        });
      }
    });

    function downloadFileByForm(url, queryType) {
      //导出按钮 公共函数
      console.log('ajaxDownloadSynchronized');
      // var fileName = appType;
      var stateTime = $('#test1').val();
      var endTime = $('#test2').val();
      var form = $('<form></form>')
        .attr('action', url)
        .attr('method', 'get');
      form.append(
        $('<input></input>')
          .attr('type', 'hidden')
          .attr('name', 'queryType')
          .attr('value', queryType)
      );
      form.append(
        $('<input></input>')
          .attr('type', 'hidden')
          .attr('name', 'userId')
          .attr('value', userId)
      ); //传递用户ID参数
      form.append(
        $('<input></input>')
          .attr('type', 'hidden')
          .attr('name', 'startTime')
          .attr('value', stateTime)
      ); //传递自定义开始时间参数
      form.append(
        $('<input></input>')
          .attr('type', 'hidden')
          .attr('name', 'endTime')
          .attr('value', endTime)
      ); //传递自定义结束时间参数
      form
        .appendTo('body')
        .submit()
        .remove();
      console.log(userId, stateTime, endTime);
    }

    function openInfo() {
      // 弹窗信息
      layer.open({
        type: 1,
        title: '详情', //不显示标题栏
        closeBtn: 1,
        area: ['26%', '300px'],
        shade: 0.3,
        id: 'detail_layui', //设定一个id，防止重复弹出
        btn: ['确定'],
        btnAlign: 'c',
        moveType: 1, //拖拽模式，0或者1
        content: $('#userDetailHtml'),
        yes: function(index, layero) {
          //do something
          layer.close(index); //如果设定了yes回调，需进行手工关闭
          $('#userDetailHtml').css('display', 'none');
        },
        end: function() {
          $('#userDetailHtml').css('display', 'none');
        }
      });
    }

    $('body').on('mouseenter', 'table.layui-table td', function(params) {
      if ($(this).text() != '') {
        layer.tips($(this).text(), $(this), {
          tips: [2, '#78BA32']
        });
      }
    });
  });
})();
