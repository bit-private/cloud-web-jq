
;(function() {
  //JavaScript代码区域
  layui.use(['jquery', 'element', 'form', 'layedit', 'laydate', 'tree', 'table', 'layer'], function() {
    var element = layui.element,
      tree = layui.tree,
      table = layui.table,
      jquery = layui.jquery,
      layer = layui.layer

    var myChart = echarts.init(document.getElementById('main'))
    var defaultInterval, clickInterval, searchInterval
    var departmentName=''
    var id

    function right(pages, totaPage, obj) {
      // 默认数据向后翻页
      // console.log(pages)
      if (pages < totaPage) {
        $.ajax({
          url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/' + pages,
          type: 'POST',
          data: JSON.stringify({}),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          success: function(data) {
            console.log(data)
            var data = data.data
            var names = [],
              department_id = [],
              proportion = [],
              userTimeCount = []
            for (var i = 0; i < data.content.length; i++) {
              names.push(data.content[i].department_name)
              department_id.push(data.content[i].department_id)
              proportion.push(data.content[i].proportion)
              userTimeCount.push(data.content[i].userTimeCount)
            }
            // console.log(myChart)
            option = {
              color: ['#6495ED', '#CDAD00'],
              title: {},
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'shadow'
                }
              },
              legend: {
                data: ['在线人数比(百分比)', '日均工作时间(分钟)']
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              xAxis: {
                type: 'category',
                data: names,
                axisLabel: {
                  clickable: true
                },
                triggerEvent: true //x坐标点击事件触发 可用
              },
              yAxis: {
                // type: 'value',
                // boundaryGap: [0, 0.01]
                show: false
              },
              series: [
                {
                  name: '在线人数比(百分比)',
                  type: 'bar',
                  data: proportion
                },
                {
                  name: '日均工作时间(分钟)',
                  type: 'bar',
                  data: userTimeCount,
                  id: department_id
                }
              ]
            }
            myChart.setOption(option)
            myChart.on('click', function(params) {
              // 点击柱状图 获取id 并请求表格数据
              // console.log(123)
              // var m = params.dataIndex,
              // n = params.seriesId,
              // ids = n.split(","),
              // id = ids[m];
              // console.log(appid)

              var m = params.xAxisIndex
              id = department_id[m]
              $.ajax({
                // 发送详情请求 - 点击详情信息
                type: 'POST',
                url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
                data: JSON.stringify({
                  departmentId: id
                }),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                dataType: 'json',
                success: function(data) {
                  console.log(data)
                  var data = data.data.content
                  table.render({
                    elem: '#tableData',
                    data: data,
                    // method: 'post',
                    cols: [
                      [
                        {
                          field: 'type',
                          title: '状态',
                          width: 60,
                          templet: function(d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              // 如果活动状态为2 渲染灰色
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          }
                        },
                        {
                          field: 'user_name',
                          title: '姓名',
                          event: 'hover',
                          style: 'cursor: pointer;',
                          sort: true
                        },
                        {
                          field: 'parent_department_name',
                          title: '院所',
                          sort: true
                        },
                        {
                          field: 'department_name',
                          title: '科室',
                          sort: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true
                        },
                        {
                          field: 'opentime',
                          title: '打开时间',
                          sort: true
                        },

                        {
                          field: 'time_count',
                          templet: function(d) {
                            if (d.time_count == null) {
                              d.time_count = 0
                            }
                            return d.time_count + '分钟'
                          },
                          title: '工作时长',
                          sort: true
                        }
                      ]
                    ],
                    id: 'tableData',
                    page: {
                      layout: ['prev', 'page', 'next', 'skip'],
                      groups: 1,
                      first: false,
                      last: false
                    }
                    //height: 315
                  })
                }
              })
            })
            obj.setOption(option)
          }
        })
      }
    }

    function left(pages, totaPage, obj) {
      // 默认数据向前翻页
      // console.log(pages)
      // console.log(pages)
      if (pages >= 0) {
        $.ajax({
          url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/' + pages,
          type: 'POST',
          data: JSON.stringify({}),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          success: function(data) {
            var data = data.data
            var names = [],
              department_id = [],
              proportion = [],
              userTimeCount = []
            for (var i = 0; i < data.content.length; i++) {
              names.push(data.content[i].department_name)
              department_id.push(data.content[i].department_id)
              proportion.push(data.content[i].proportion)
              userTimeCount.push(data.content[i].userTimeCount)
            }
            // console.log(myChart)
            option = {
              color: ['#6495ED', '#CDAD00'],
              title: {},
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'shadow'
                }
              },
              legend: {
                data: ['在线人数比(百分比)', '日均工作时间(分钟)']
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              xAxis: {
                type: 'category',
                data: names,
                axisLabel: {
                  clickable: true
                },
                triggerEvent: true //x坐标点击事件触发 可用
              },
              yAxis: {
                // type: 'value',
                // boundaryGap: [0, 0.01]
                show: false
              },
              series: [
                {
                  name: '在线人数比(百分比)',
                  type: 'bar',
                  data: proportion
                },
                {
                  name: '日均工作时间(分钟)',
                  type: 'bar',
                  data: userTimeCount,
                  id: department_id
                }
              ]
            }
            myChart.setOption(option)
            myChart.on('click', function(params) {
              // 点击柱状图 获取id 并请求表格数据
              // console.log(123)
              // var m = params.dataIndex,
              // n = params.seriesId,
              // ids = n.split(","),
              // id = ids[m];
              // console.log(appid)

              var m = params.xAxisIndex
              id = department_id[m]
              $.ajax({
                // 发送详情请求 - 点击详情信息
                type: 'POST',
                url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
                data: JSON.stringify({
                  departmentId: id
                }),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                dataType: 'json',
                success: function(data) {
                  console.log(data)
                  var data = data.data.content
                  table.render({
                    elem: '#tableData',
                    data: data,
                    // method: 'post',
                    cols: [
                      [
                        {
                          field: 'type',
                          title: '状态',
                          width: 60,
                          templet: function(d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              // 如果活动状态为2 渲染灰色
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          }
                        },
                        {
                          field: 'user_name',
                          title: '姓名',
                          event: 'hover',
                          style: 'cursor: pointer;',
                          sort: true
                        },
                        {
                          field: 'parent_department_name',
                          title: '院所',
                          sort: true
                        },
                        {
                          field: 'department_name',
                          title: '科室',
                          sort: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true
                        },
                        {
                          field: 'opentime',
                          title: '打开时间',
                          sort: true
                        },

                        {
                          field: 'time_count',
                          templet: function(d) {
                            if (d.time_count == null) {
                              d.time_count = 0
                            }
                            return d.time_count + '分钟'
                          },
                          title: '工作时长',
                          sort: true
                        }
                      ]
                    ],
                    id: 'tableData',
                    page: {
                      layout: ['prev', 'page', 'next', 'skip'],
                      groups: 1,
                      first: false,
                      last: false
                    }
                  })
                }
              })
            })
            obj.setOption(option)
          }
        })
      }
    }

    var parameter = {
      departmentId: '',
      userId: ''
    }
    var active = {
      getTree: function(val) {
        $.ajax({
          url: url + ':9000/organization/getOrgTreeAndIncludeUsers',
          // url: 'http://10.1.3.98:9000/organization/getOrgTreeAndIncludeUsers',
          type: 'get',
          success: function(data) {
            console.log(data)
            layui.tree({
              elem: '#treeList', //指定元素
              nodes: data.data,
              click: function (node) {
                console.log(node)
                if(node.children) {
                  parameter.departmentId = node.id
                  parameter.userId = ''
                  active.getEchart()
                  active.getTable()
                }else{
                  parameter.userId = node.id
                  parameter.departmentId = ''
                  active.getEchart()
                  active.getTable()
                }
              }
            })
          }
        })
        $('body').on('mousedown', '.layui-tree a', function () {
          $('.layui-tree a').css('background', '')
          $(this).css('background', 'rgb(18, 160, 255)')
        })
      },
      getEchart: function(val) {
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/0',
          data: JSON.stringify({
            departmentId: parameter.departmentId,
            userId: parameter.userId
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            var data = data.data
            var names = [],
              department_id = [],
              proportion = [],
              userTimeCount = []
            for (var i = 0; i < data.content.length; i++) {
              names.push(data.content[i].department_name)
              department_id.push(data.content[i].department_id)
              proportion.push(data.content[i].proportion)
              userTimeCount.push(data.content[i].userTimeCount)
            }
            myChart.clear()
            option = {
              color: ['#6495ED', '#CDAD00'],
              title: {},
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'shadow'
                }
              },
              legend: {
                data: ['在线人数比(百分比)', '日均工作时间(分钟)']
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              xAxis: {
                type: 'category',
                data: names,
                axisLabel: {
                  clickable: true
                },
                triggerEvent: true //x坐标点击事件触发 可用
              },
              yAxis: {
                // type: 'value',
                // boundaryGap: [0, 0.01]
                show: false
              },
              series: [
                {
                  name: '在线人数比(百分比)',
                  type: 'bar',
                  data: proportion,
                  barMaxWidth: 50
                },
                {
                  name: '日均工作时间(分钟)',
                  type: 'bar',
                  data: userTimeCount,
                  id: department_id,
                  barMaxWidth: 50
                }
              ]
            }
            myChart.setOption(option)
          }
        })
      },
      getTable: function(val) {
        departmentName = $('#monitor-user-input').val()
        $.ajax({
          // 显示 数据表格的信息
          type: 'POST',
          url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
          data: JSON.stringify({
            departmentId: parameter.departmentId,
            userId: parameter.userId,
            departmentName:departmentName,
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data)
            var data = data.data.content
            table.render({
              elem: '#tableData',
              data: data,
              // method: 'post',
              cols: [
                [
                  {
                    field: 'type',
                    title: '状态',
                    width: 60,
                    templet: function(d) {
                      if (d.state == '1') {
                        // 如果活动状态为1 渲染绿色
                        return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                      }
                      if (d.state == '0') {
                        // 如果活动状态为2 渲染灰色
                        return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                      }
                    }
                  },
                  {
                    field: 'user_name',
                    title: '姓名',
                    event: 'hover',
                    style: 'cursor: pointer;',
                    sort: true
                  },
                  {
                    field: 'parent_department_name',
                    title: '院所',
                    sort: true
                  },
                  {
                    field: 'department_name',
                    title: '科室',
                    sort: true
                  },
                  {
                    field: 'appName',
                    title: '软件',
                    sort: true
                  },
                  {
                    field: 'opentime',
                    title: '打开时间',
                    sort: true
                  },
    
                  {
                    field: 'time_count',
                    templet: function(d) {
                      if (d.time_count == null) {
                        d.time_count = 0
                      }
                      return d.time_count + '分钟'
                    },
                    title: '工作时长',
                    sort: true
                  }
                ]
              ],
              id: 'tableData',
              page: {
                layout: ['prev', 'page', 'next', 'skip'],
                groups: 1,
                first: false,
                last: false
              }
            })
          }
        })
      },
      reload: function() {
        var demoReload = $('#demoReload')

        //执行重载
        table.reload('testReload', {
          page: {
            curr: 1 //重新从第 1 页开始
          },
          where: {
            key: {
              id: demoReload.val()
            }
          }
        })
      }
    }

    active.getTree()
    active.getEchart()
    active.getTable()
    $('.demoTable .layui-btn').on('click', function() {
      var type = $(this).data('type')
      active[type] ? active[type].call(this) : ''
    })

    table.on('tool(tableData)', function(params) {
      var data = params.data
      // console.log(data);
    })

    $('#monitor-user-btn').click(function() {
      // 搜索按钮
      myChart.clear()
      clearInterval(defaultInterval)
      clearInterval(clickInterval)
      clearInterval(searchInterval)
      departmentName = $('#monitor-user-input').val()
      // console.log(userId)
      $.ajax({
        type: 'POST',
        url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/0',
        data: JSON.stringify({
          departmentName: departmentName
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function(data) {
          console.log(data)
          var data = data.data
          var names = [],
            department_id = [],
            proportion = [],
            userTimeCount = []
          for (var i = 0; i < data.content.length; i++) {
            names.push(data.content[i].department_name)
            department_id.push(data.content[i].department_id)
            proportion.push(data.content[i].proportion)
            userTimeCount.push(data.content[i].userTimeCount)
          }
          // console.log(myChart)
          option = {
            color: ['#6495ED', '#CDAD00'],
            title: {},
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            legend: {
              data: ['在线人数比(百分比)', '日均工作时间(分钟)']
            },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true
            },
            xAxis: {
              type: 'category',
              data: names,
              axisLabel: {
                clickable: true
              },
              triggerEvent: true //x坐标点击事件触发 可用
            },
            yAxis: {
              // type: 'value',
              // boundaryGap: [0, 0.01]
              show: false
            },
            series: [
              {
                name: '在线人数比(百分比)',
                type: 'bar',
                data: proportion,
                barMaxWidth: 50
              },
              {
                name: '日均工作时间(分钟)',
                type: 'bar',
                data: userTimeCount,
                id: department_id,
                barMaxWidth: 50
              }
            ]
          }
          myChart.setOption(option)
          myChart.on('click', function(params) {
            // 点击柱状图 获取id 并请求表格数据
            // console.log(123)
            // var m = params.dataIndex,
            // n = params.seriesId,
            // ids = n.split(","),
            // id = ids[m];
            // console.log(appid)

            var m = params.xAxisIndex
            id = department_id[m]
            $.ajax({
              // 发送详情请求 - 点击详情信息
              type: 'POST',
              url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
              data: JSON.stringify({
                departmentId: id
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data)
                var data = data.data.content
                table.render({
                  elem: '#tableData',
                  data: data,
                  // method: 'post',
                  cols: [
                    [
                      {
                        field: 'type',
                        title: '状态',
                        width: 60,
                        templet: function(d) {
                          if (d.state == '1') {
                            // 如果活动状态为1 渲染绿色
                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                          }
                          if (d.state == '0') {
                            // 如果活动状态为2 渲染灰色
                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                          }
                        }
                      },
                      {
                        field: 'user_name',
                        title: '姓名',
                        event: 'hover',
                        style: 'cursor: pointer;',
                        sort: true
                      },
                      {
                        field: 'parent_department_name',
                        title: '院所',
                        sort: true
                      },
                      {
                        field: 'department_name',
                        title: '科室',
                        sort: true
                      },
                      {
                        field: 'appName',
                        title: '软件',
                        sort: true
                      },
                      {
                        field: 'opentime',
                        title: '打开时间',
                        sort: true
                      },

                      {
                        field: 'time_count',
                        templet: function(d) {
                          if (d.time_count == null) {
                            d.time_count = 0
                          }
                          return d.time_count + '分钟'
                        },
                        title: '工作时长',
                        sort: true
                      }
                    ]
                  ],
                  id: 'tableData',
                  page: {
                    layout: ['prev', 'page', 'next', 'skip'],
                    groups: 1,
                    first: false,
                    last: false
                  }
                })
              }
            })
          })
        }
      })
      active.getTable()
      searchInterval = setInterval(function() {
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/0',
          data: JSON.stringify({
            departmentName: departmentName
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data)
            var data = data.data
            var names = [],
              department_id = [],
              proportion = [],
              userTimeCount = []
            for (var i = 0; i < data.content.length; i++) {
              names.push(data.content[i].department_name)
              department_id.push(data.content[i].department_id)
              proportion.push(data.content[i].proportion)
              userTimeCount.push(data.content[i].userTimeCount)
            }
            // console.log(myChart)
            option = {
              color: ['#6495ED', '#CDAD00'],
              title: {},
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'shadow'
                }
              },
              legend: {
                data: ['在线人数比(百分比)', '日均工作时间(分钟)']
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              xAxis: {
                type: 'category',
                data: names,
                axisLabel: {
                  clickable: true
                },
                triggerEvent: true //x坐标点击事件触发 可用
              },
              yAxis: {
                // type: 'value',
                // boundaryGap: [0, 0.01]
                show: false
              },
              series: [
                {
                  name: '在线人数比(百分比)',
                  type: 'bar',
                  data: proportion,
                  barMaxWidth: 50
                },
                {
                  name: '日均工作时间(分钟)',
                  type: 'bar',
                  data: userTimeCount,
                  id: department_id,
                  barMaxWidth: 50
                }
              ]
            }
            myChart.setOption(option)
            myChart.on('click', function(params) {
              // 点击柱状图 获取id 并请求表格数据
              // console.log(123)
              // var m = params.dataIndex,
              // n = params.seriesId,
              // ids = n.split(","),
              // id = ids[m];
              // console.log(appid)

              var m = params.xAxisIndex
              id = department_id[m]
              $.ajax({
                // 发送详情请求 - 点击详情信息
                type: 'POST',
                url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
                data: JSON.stringify({
                  departmentId: id
                }),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                dataType: 'json',
                success: function(data) {
                  console.log(data)
                  var data = data.data.content
                  table.render({
                    elem: '#tableData',
                    data: data,
                    // method: 'post',
                    cols: [
                      [
                        {
                          field: 'type',
                          title: '状态',
                          width: 60,
                          templet: function(d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              // 如果活动状态为2 渲染灰色
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          }
                        },
                        {
                          field: 'user_name',
                          title: '姓名',
                          event: 'hover',
                          style: 'cursor: pointer;',
                          sort: true
                        },
                        {
                          field: 'parent_department_name',
                          title: '院所',
                          sort: true
                        },
                        {
                          field: 'department_name',
                          title: '科室',
                          sort: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true
                        },
                        {
                          field: 'opentime',
                          title: '打开时间',
                          sort: true
                        },

                        {
                          field: 'time_count',
                          templet: function(d) {
                            if (d.time_count == null) {
                              d.time_count = 0
                            }
                            return d.time_count + '分钟'
                          },
                          title: '工作时长',
                          sort: true
                        }
                      ]
                    ],
                    id: 'tableData',
                    page: {
                      layout: ['prev', 'page', 'next', 'skip'],
                      groups: 1,
                      first: false,
                      last: false
                    }
                  })
                }
              })
            })
          }
        })
      }, 180000)
    })
    $('#monitor-user-input').on('keypress',function() {
      // 搜索按钮
      if(event.keyCode == "13") {
        myChart.clear()
        clearInterval(defaultInterval)
        clearInterval(clickInterval)
        clearInterval(searchInterval)
        departmentName = $('#monitor-user-input').val()
        // console.log(userId)
        $.ajax({
          type: 'POST',
          url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/0',
          data: JSON.stringify({
            departmentName: departmentName
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function (data) {
            console.log(data)
            var data = data.data
            var names = [],
                department_id = [],
                proportion = [],
                userTimeCount = []
            for (var i = 0; i < data.content.length; i++) {
              names.push(data.content[i].department_name)
              department_id.push(data.content[i].department_id)
              proportion.push(data.content[i].proportion)
              userTimeCount.push(data.content[i].userTimeCount)
            }
            // console.log(myChart)
            option = {
              color: ['#6495ED', '#CDAD00'],
              title: {},
              tooltip: {
                trigger: 'axis',
                axisPointer: {
                  type: 'shadow'
                }
              },
              legend: {
                data: ['在线人数比(百分比)', '日均工作时间(分钟)']
              },
              grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
              },
              xAxis: {
                type: 'category',
                data: names,
                axisLabel: {
                  clickable: true
                },
                triggerEvent: true //x坐标点击事件触发 可用
              },
              yAxis: {
                // type: 'value',
                // boundaryGap: [0, 0.01]
                show: false
              },
              series: [
                {
                  name: '在线人数比(百分比)',
                  type: 'bar',
                  data: proportion,
                  barMaxWidth: 50
                },
                {
                  name: '日均工作时间(分钟)',
                  type: 'bar',
                  data: userTimeCount,
                  id: department_id,
                  barMaxWidth: 50
                }
              ]
            }
            myChart.setOption(option)
            myChart.on('click', function (params) {
              // 点击柱状图 获取id 并请求表格数据
              // console.log(123)
              // var m = params.dataIndex,
              // n = params.seriesId,
              // ids = n.split(","),
              // id = ids[m];
              // console.log(appid)

              var m = params.xAxisIndex
              id = department_id[m]
              $.ajax({
                // 发送详情请求 - 点击详情信息
                type: 'POST',
                url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
                data: JSON.stringify({
                  departmentId: id
                }),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                dataType: 'json',
                success: function (data) {
                  console.log(data)
                  var data = data.data.content
                  table.render({
                    elem: '#tableData',
                    data: data,
                    // method: 'post',
                    cols: [
                      [
                        {
                          field: 'type',
                          title: '状态',
                          width: 60,
                          templet: function (d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              // 如果活动状态为2 渲染灰色
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          }
                        },
                        {
                          field: 'user_name',
                          title: '姓名',
                          event: 'hover',
                          style: 'cursor: pointer;',
                          sort: true
                        },
                        {
                          field: 'parent_department_name',
                          title: '院所',
                          sort: true
                        },
                        {
                          field: 'department_name',
                          title: '科室',
                          sort: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true
                        },
                        {
                          field: 'opentime',
                          title: '打开时间',
                          sort: true
                        },

                        {
                          field: 'time_count',
                          templet: function (d) {
                            if (d.time_count == null) {
                              d.time_count = 0
                            }
                            return d.time_count + '分钟'
                          },
                          title: '工作时长',
                          sort: true
                        }
                      ]
                    ],
                    id: 'tableData',
                    page: {
                      layout: ['prev', 'page', 'next', 'skip'],
                      groups: 1,
                      first: false,
                      last: false
                    }
                  })
                }
              })
            })
          }
        })
        active.getTable()
        searchInterval = setInterval(function () {
          $.ajax({
            type: 'POST',
            url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/0',
            data: JSON.stringify({
              departmentName: departmentName
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function (data) {
              console.log(data)
              var data = data.data
              var names = [],
                  department_id = [],
                  proportion = [],
                  userTimeCount = []
              for (var i = 0; i < data.content.length; i++) {
                names.push(data.content[i].department_name)
                department_id.push(data.content[i].department_id)
                proportion.push(data.content[i].proportion)
                userTimeCount.push(data.content[i].userTimeCount)
              }
              // console.log(myChart)
              option = {
                color: ['#6495ED', '#CDAD00'],
                title: {},
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    type: 'shadow'
                  }
                },
                legend: {
                  data: ['在线人数比(百分比)', '日均工作时间(分钟)']
                },
                grid: {
                  left: '3%',
                  right: '4%',
                  bottom: '3%',
                  containLabel: true
                },
                xAxis: {
                  type: 'category',
                  data: names,
                  axisLabel: {
                    clickable: true
                  },
                  triggerEvent: true //x坐标点击事件触发 可用
                },
                yAxis: {
                  // type: 'value',
                  // boundaryGap: [0, 0.01]
                  show: false
                },
                series: [
                  {
                    name: '在线人数比(百分比)',
                    type: 'bar',
                    data: proportion,
                    barMaxWidth: 50
                  },
                  {
                    name: '日均工作时间(分钟)',
                    type: 'bar',
                    data: userTimeCount,
                    id: department_id,
                    barMaxWidth: 50
                  }
                ]
              }
              myChart.setOption(option)
              myChart.on('click', function (params) {
                // 点击柱状图 获取id 并请求表格数据
                // console.log(123)
                // var m = params.dataIndex,
                // n = params.seriesId,
                // ids = n.split(","),
                // id = ids[m];
                // console.log(appid)

                var m = params.xAxisIndex
                id = department_id[m]
                $.ajax({
                  // 发送详情请求 - 点击详情信息
                  type: 'POST',
                  url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
                  data: JSON.stringify({
                    departmentId: id
                  }),
                  headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                  },
                  dataType: 'json',
                  success: function (data) {
                    console.log(data)
                    var data = data.data.content
                    table.render({
                      elem: '#tableData',
                      data: data,
                      // method: 'post',
                      cols: [
                        [
                          {
                            field: 'type',
                            title: '状态',
                            width: 60,
                            templet: function (d) {
                              if (d.state == '1') {
                                // 如果活动状态为1 渲染绿色
                                return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                              }
                              if (d.state == '0') {
                                // 如果活动状态为2 渲染灰色
                                return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                              }
                            }
                          },
                          {
                            field: 'user_name',
                            title: '姓名',
                            event: 'hover',
                            style: 'cursor: pointer;',
                            sort: true
                          },
                          {
                            field: 'parent_department_name',
                            title: '院所',
                            sort: true
                          },
                          {
                            field: 'department_name',
                            title: '科室',
                            sort: true
                          },
                          {
                            field: 'appName',
                            title: '软件',
                            sort: true
                          },
                          {
                            field: 'opentime',
                            title: '打开时间',
                            sort: true
                          },

                          {
                            field: 'time_count',
                            templet: function (d) {
                              if (d.time_count == null) {
                                d.time_count = 0
                              }
                              return d.time_count + '分钟'
                            },
                            title: '工作时长',
                            sort: true
                          }
                        ]
                      ],
                      id: 'tableData',
                      page: {
                        layout: ['prev', 'page', 'next', 'skip'],
                        groups: 1,
                        first: false,
                        last: false
                      }
                    })
                  }
                })
              })
            }
          })
        }, 180000)
      }
    })

    $('#monitor-user-icon').click(function() {
      $('#monitor-user-input').val('')
      $.ajax({
        type: 'POST',
        url: url + ':9006/UserMonitor/DepartmentWorkInfo/10/0',
        data: JSON.stringify({
          departmentName: ''
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function(data) {
          console.log(data)
          var data = data.data
          var names = [],
            department_id = [],
            proportion = [],
            userTimeCount = []
          for (var i = 0; i < data.content.length; i++) {
            names.push(data.content[i].department_name)
            department_id.push(data.content[i].department_id)
            proportion.push(data.content[i].proportion)
            userTimeCount.push(data.content[i].userTimeCount)
          }
          // console.log(myChart)
          option = {
            color: ['#6495ED', '#CDAD00'],
            title: {},
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            legend: {
              data: ['在线人数比(百分比)', '日均工作时间(分钟)']
            },
            grid: {
              left: '3%',
              right: '4%',
              bottom: '3%',
              containLabel: true
            },
            xAxis: {
              type: 'category',
              data: names,
              axisLabel: {
                clickable: true
              },
              triggerEvent: true //x坐标点击事件触发 可用
            },
            yAxis: {
              // type: 'value',
              // boundaryGap: [0, 0.01]
              show: false
            },
            series: [
              {
                name: '在线人数比(百分比)',
                type: 'bar',
                data: proportion,
                barMaxWidth: 50
              },
              {
                name: '日均工作时间(分钟)',
                type: 'bar',
                data: userTimeCount,
                id: department_id,
                barMaxWidth: 50
              }
            ]
          }
          myChart.setOption(option)
          myChart.on('click', function(params) {
            // 点击柱状图 获取id 并请求表格数据
            // console.log(123)
            // var m = params.dataIndex,
            // n = params.seriesId,
            // ids = n.split(","),
            // id = ids[m];
            // console.log(appid)

            var m = params.xAxisIndex
            id = department_id[m]
            $.ajax({
              // 发送详情请求 - 点击详情信息
              type: 'POST',
              url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/user_name/desc',
              data: JSON.stringify({
                departmentId: id
              }),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function(data) {
                console.log(data)
                var data = data.data.content
                table.render({
                  elem: '#tableData',
                  data: data,
                  // method: 'post',
                  cols: [
                    [
                      {
                        field: 'type',
                        title: '状态',
                        width: 60,
                        templet: function(d) {
                          if (d.state == '1') {
                            // 如果活动状态为1 渲染绿色
                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                          }
                          if (d.state == '0') {
                            // 如果活动状态为2 渲染灰色
                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                          }
                        }
                      },
                      {
                        field: 'user_name',
                        title: '姓名',
                        event: 'hover',
                        style: 'cursor: pointer;',
                        sort: true
                      },
                      {
                        field: 'parent_department_name',
                        title: '院所',
                        sort: true
                      },
                      {
                        field: 'department_name',
                        title: '科室',
                        sort: true
                      },
                      {
                        field: 'appName',
                        title: '软件',
                        sort: true
                      },
                      {
                        field: 'opentime',
                        title: '打开时间',
                        sort: true
                      },

                      {
                        field: 'time_count',
                        templet: function(d) {
                          if (d.time_count == null) {
                            d.time_count = 0
                          }
                          return d.time_count + '分钟'
                        },
                        title: '工作时长',
                        sort: true
                      }
                    ]
                  ],
                  id: 'tableData',
                  page: {
                    layout: ['prev', 'page', 'next', 'skip'],
                    groups: 1,
                    first: false,
                    last: false
                  }
                })
              }
            })
          })
        }
      })
      active.getTable()
    })

    table.on('sort(tableData)', function(obj) {
      console.log(obj.field)
      console.log(obj.type)
      $.ajax({
        // 发送详情请求 - 点击详情信息
        type: 'POST',
        url: url + ':9006/UserMonitor/DepartmentWorkTableInfo/10/0/' + obj.field + '/' + obj.type,
        // url: "http://10.1.1.213:9006/UserMonitor/DepartmentWorkTableInfo/10/0/" + obj.field + "/" + obj.type,
        data: JSON.stringify({
          departmentId: id
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function(data) {
          console.log(data)
          var data = data.data.content
          table.render({
            elem: '#tableData',
            data: data,
            // method: 'post',
            cols: [
              [
                {
                  field: 'type',
                  title: '状态',
                  width: 60,
                  templet: function(d) {
                    if (d.state == '1') {
                      // 如果活动状态为1 渲染绿色
                      return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                    }
                    if (d.state == '0') {
                      // 如果活动状态为2 渲染灰色
                      return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                    }
                  }
                },
                {
                  field: 'user_name',
                  title: '姓名',
                  event: 'hover',
                  style: 'cursor: pointer;',
                  sort: true
                },
                {
                  field: 'parent_department_name',
                  title: '院所',
                  sort: true
                },
                {
                  field: 'department_name',
                  title: '科室',
                  sort: true
                },
                {
                  field: 'appName',
                  title: '软件',
                  sort: true
                },
                {
                  field: 'opentime',
                  title: '打开时间',
                  sort: true
                },

                {
                  field: 'time_count',
                  templet: function(d) {
                    if (d.time_count == null) {
                      d.time_count = 0
                    }
                    return d.time_count + '分钟'
                  },
                  title: '工作时长',
                  sort: true
                }
              ]
            ],
            id: 'tableData',
            page: {
              layout: ['prev', 'page', 'next', 'skip'],
              groups: 1,
              first: false,
              last: false
            }
          })
        }
      })
    })

    $('body').on('mouseenter', 'table.layui-table td', function(params) {
      if (this.getAttribute('data-field') == 0) {
        return false
      }
      if (this.getAttribute('data-field') == 'type') {
      } else {
        if ($(this).text() != '') {
          layer.tips($(this).text(), $(this), {
            tips: [2, '#78BA32']
          })
        }
      }
    })
    $('body').on('mouseenter', '.success', function(params) {
      layer.tips('活动中', $(this), {
        tips: [2, '#78BA32']
      })
    })
    $('body').on('mouseenter', '.fail', function(params) {
      layer.tips('未活动', $(this), {
        tips: [2, '#78BA32']
      })
    })
  })
})()
