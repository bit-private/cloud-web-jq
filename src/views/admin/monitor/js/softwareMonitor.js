;(function() {
  //JavaScript代码区域
  layui.use(["jquery", "element", "form", "layedit", "tree", "laydate", "table", "layer", "laypage"], function() {
    var element = layui.element,
      table = layui.table,
      jquery = layui.jquery,
      form = layui.form,
      tree = layui.tree,
      laypage = layui.laypage,
      // treeselect = layui.treeselect,
      layer = layui.layer
    // 类型选择-下拉菜单
    // var url = "http://10.1.2.121:9006";
    var param = {
      appName: "",
      appType: "",
      departmentId: "",
      userId: ""
    }
    var intervalAppId, intervalAppName
    var active = {
      // 获取图表
      getEchart: function(val) {
        $.ajax({
          // 显示数据
          url: url + ":9006/AppMonitor/pageApp/10/0",
          type: "POST",
          data: val ? JSON.stringify({ appName: val }) : JSON.stringify({ appType: param.appType, departmentId: param.departmentId, userId: param.userId }),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            var res = data // 因需要修改接口数据 把data的值存入res
            var myChart = echarts.init(document.getElementById("main"))
            var data = data.data.content
            var arrApp_name = []
            var arrCount = []
            var userName = []
            var arrAppId = []
            var appIds
            for (var i = 0; i < data.length; i++) {
              arrApp_name.push(data[i].app_name)
              arrCount.push(data[i].count)
              userName.push(data[i].appkey)
              arrAppId.push(data[i].appId)
            }
            myChart.clear()
            option = {
              title: {
                text: "单位(人)"
              },
              color: ["#3398DB"],
              tooltip: {
                trigger: "axis",
                axisPointer: {
                  // 坐标轴指示器，坐标轴触发有效
                  type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params, callback) {
                  // 鼠标悬浮显示信息，并赋值appId
                  // console.log(params)
                  params[0].seriesId = data[params[0].dataIndex].appId
                  params[0].seriesName = data[params[0].dataIndex].appkey

                  var res = params[0].seriesName
                  return res
                }
                //formatter:""
              },
              grid: {
                left: "3%",
                right: "4%",
                bottom: "3%",
                containLabel: true
              },
              xAxis: [
                {
                  type: "category",
                  data: arrApp_name,
                  axisLabel: {
                    interval: 0,
                    rotate: 40
                  },
                  axisTick: {
                    alignWithLabel: true
                  }
                }
              ],
              yAxis: [
                {
                  type: "value",
                  minInterval: 1
                }
              ],
              series: [
                {
                  // name: "userName",
                  id: arrAppId,
                  type: "bar",
                  // barWidth: '60%',
                  barMaxWidth: 50,
                  data: arrCount
                }
              ]
            }
            myChart.on("click", function(params) {
              console.log(params)
              // 点击柱状图显示信息
              var m = params.dataIndex,
                n = params.seriesId,
                ids = n.split(","),
                appid = ids[m]
              $.ajax({
                // 发送详情请求 - 点击详情信息
                type: "POST",
                url: url + ":9006/AppMonitor/AppModuleMonitor/10/1",
                data: JSON.stringify({
                  appId: appid
                }),
                headers: {
                  "Content-Type": "application/json;charset=utf-8"
                },
                dataType: "json",
                success: function(data) {
                  console.log(data)
                  if (data.data.content != 0) {
                    openDetail()
                    var res = data //因需要修改数据接口 将data的值存在res里
                    var echart = echarts.init(document.getElementById("detailEcharts")),
                      data = data.data.content,
                      modules = [],
                      number = [],
                      usercount = []

                    for (var i = 0; i < data.length; i++) {
                      modules.push(data[i].module_name)
                      number.push(data[i].number)
                      usercount.push(data[i].usercount)
                    }
                    console.log(modules)
                    option = {
                      title: {
                        text: "单位(人)"
                      },
                      color: ["#7EC0EE", "#7CCD7C"],
                      tooltip: {
                        trigger: "axis",
                        axisPointer: {
                          type: "shadow"
                        },
                        formatter: function(params, ticket, callback) {
                          var htmlStr = ""
                          for (var i = 0; i < params.length; i++) {
                            var param = params[i]
                            var xName = param.name //x轴的名称
                            var seriesName = param.seriesName //图例名称
                            var seriesIndex = param.seriesIndex
                            var value = param.value //y轴值
                            var color = param.color //图例颜色
                            // console.log( param)
                            if (i === 0) {
                              htmlStr += xName + "<br/>" //x轴的名称
                            }
                            if (seriesIndex === 1) {
                              value += "%"
                            }
                            htmlStr += "<div>"
                            //为了保证和原来的效果一样，这里自己实现了一个点的效果
                            htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>'

                            //圆点后面显示的文本
                            htmlStr += seriesName + "：" + value
                            // htmlStr += seriesName0 + '：' + value + seriesName1 + '：' + value + '%';

                            htmlStr += "</div>"
                          }
                          return htmlStr
                        }
                      },
                      legend: {
                        data: ["人数", "使用率"]
                      },

                      calculable: true,
                      xAxis: [
                        {
                          type: "category",
                          axisTick: {
                            show: false
                          },
                          data: modules,
                          axisLabel: {
                            interval: 0,
                            rotate: 40
                          }
                        }
                      ],
                      yAxis: [
                        {
                          type: "value",
                          minInterval: 1
                        }
                      ],
                      series: [
                        {
                          name: "人数",
                          type: "bar",
                          barGap: 0,
                          barMaxWidth: 50,
                          data: usercount
                        },
                        {
                          name: "使用率",
                          type: "bar",
                          barMaxWidth: 50,
                          data: number
                        }
                      ]
                    }
                    echart.setOption(option)

                    console.log(appid)
                    var echartPages = 1
                    var echartTotaPage = res.data.totalPages
                    $(".echartRight").click(function() {
                      //带参数请求 - 分页 向后
                      console.log(123)
                      echart.clear()
                      echartPages++
                      if (echartPages >= echartTotaPage) {
                        echartPages = echartTotaPage
                      }
                      echartRight(echartPages, echartTotaPage, echart, appid)
                    })

                    $(".echartLeft").click(function() {
                      // 带参数请求 - 分页 向前
                      echart.clear()
                      echartPages--
                      if (echartPages <= 0) {
                        echartPages = 0
                      }
                      echartRight(echartPages, echartTotaPage, echart, appid)
                    })
                  } else {
                    layer.msg("没有模块!")
                    return false
                  }
                }
              })
            })
            myChart.setOption(option)
          }
        })
      },
      getTree: function(val) {
        $.ajax({
          url: url + ":9000/organization/getOrgTreeAndIncludeUsers",
          type: "get",
          success: function(data) {
            console.log(data)
            layui.tree({
              elem: "#treeList", //指定元素
              nodes: data.data,
              spread: true,
              click: function(node) {
                console.log(node)
                if (node.children) {
                  param.appType = ""
                  param.userId = ""
                  param.departmentId = node.id
                  active.getEchart()
                } else {
                  param.appType = ""
                  param.departmentId = ""
                  param.userId = node.id
                  active.getEchart()
                }
              }
            })
          }
        })
        $("body").on("mousedown", ".layui-tree a", function() {
          $(".layui-tree a").css("background", "")
          $(this).css("background", "rgb(18, 160, 255)")
        })
      }
    }

    active.getEchart()
    active.getTree()

    form.on("select(typeSelection)", function(data) {
      // 点击下拉搜索 - 选择类型
      // console.log(data.elem[data.elem.selectedIndex])
      var appType = data.elem[data.elem.selectedIndex].getAttribute("data-id")
      param.appType = appType
      active.getEchart()
      clearInterval(intervalAppId)
      clearInterval(intervalAppName)
      intervalAppId = setInterval(function() {
        active.getEchart()
      }, 8000)
    })

    $("body").on("mousedown", ".layui-tree a", function() {
      $(".layui-tree a").css("background", "#fff")
      $(this).css("background", "rgb(18, 160, 255)")
    })

    $(".downpanel")
      .on("click", ".layui-select-title", function(e) {
        // 下拉树状图
        $(".layui-form-select")
          .not($(this).parents(".layui-form-select"))
          .removeClass("layui-form-selected")
        $(this)
          .parents(".downpanel")
          .toggleClass("layui-form-selected")
        layui.stope(e)
      })
      .on("click", "dl i", function(e) {
        layui.stope(e)
      })

    function openDetail() {
      layer.open({
        type: 1,
        shade: false,
        area: ["70%", "520px"],
        closeBtn: 1,
        btn: ["确定"],
        id: "detail_layui", //设定一个id，防止重复弹出
        btnAlign: "c",
        title: "详情", //不显示标题
        content: $("#detailHtml"), //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响
        end: function() {
          $("#detailHtml").hide()
        }
      })
    }

    function right(pages, totaPage, obj) {
      // 默认数据向后翻页
      $.ajax({
        url: url + ":9006/AppMonitor/pageApp/10/" + pages,
        type: "POST",
        data: JSON.stringify({}),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          console.log(data)
          if (data.code == 200) {
            arrApp_name = []
            arrCount = []
            userName = []
            arrAppId = []
            var data = data.data.content
            for (var i = 0; i < data.length; i++) {
              arrApp_name.push(data[i].app_name)
              arrCount.push(data[i].count)
              userName.push(data[i].appkey)
              arrAppId.push(data[i].appId)
            }
            option = {
              title: {
                text: "单位(人)"
              },
              color: ["#3398DB"],
              tooltip: {
                trigger: "axis",
                axisPointer: {
                  // 坐标轴指示器，坐标轴触发有效
                  type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params, callback) {
                  // 鼠标悬浮显示信息，并赋值appId
                  // console.log(params)
                  params[0].seriesId = data[params[0].dataIndex].appId
                  params[0].seriesName = data[params[0].dataIndex].appkey

                  var res = params[0].seriesName
                  return res
                }
                //formatter:""
              },
              grid: {
                left: "3%",
                right: "4%",
                bottom: "3%",
                containLabel: true
              },
              xAxis: [
                {
                  type: "category",
                  data: arrApp_name,
                  axisTick: {
                    alignWithLabel: true
                  },
                  axisLabel: {
                    interval: 0,
                    rotate: 40
                  }
                }
              ],
              yAxis: [
                {
                  type: "value",
                  minInterval: 1
                }
              ],
              series: [
                {
                  // name: "userName",
                  id: arrAppId,
                  type: "bar",
                  // barWidth: '60%',
                  barMaxWidth: 50,
                  data: arrCount
                }
              ]
            }
            obj.setOption(option)
          } else {
            layer.msg(data.retmsg)
          }
        }
      })
    }

    function left(pages, totaPage, obj) {
      // 默认数据向前翻页
      // console.log(pages)
      if (pages >= 0) {
        $.ajax({
          url: url + ":9006/AppMonitor/pageApp/10/" + pages,
          type: "POST",
          data: JSON.stringify({}),
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          },
          success: function(data) {
            console.log(data.data.totalPages)
            arrApp_name = []
            arrCount = []
            userName = []
            arrAppId = []
            var data = data.data.content
            for (var i = 0; i < data.length; i++) {
              arrApp_name.push(data[i].app_name)
              arrCount.push(data[i].count)
              userName.push(data[i].appkey)
              arrAppId.push(data[i].appId)
            }
            option = {
              title: {
                text: "单位(人)"
              },
              color: ["#3398DB"],
              tooltip: {
                trigger: "axis",
                axisPointer: {
                  // 坐标轴指示器，坐标轴触发有效
                  type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params, callback) {
                  // 鼠标悬浮显示信息，并赋值appId
                  // console.log(params)
                  params[0].seriesId = data[params[0].dataIndex].appId
                  params[0].seriesName = data[params[0].dataIndex].appkey

                  var res = params[0].seriesName
                  return res
                }
                //formatter:""
              },
              grid: {
                left: "3%",
                right: "4%",
                bottom: "3%",
                containLabel: true
              },
              xAxis: [
                {
                  type: "category",
                  data: arrApp_name,
                  axisTick: {
                    alignWithLabel: true
                  },
                  axisLabel: {
                    interval: 0,
                    rotate: 40
                  }
                }
              ],
              yAxis: [
                {
                  type: "value",
                  minInterval: 1
                }
              ],
              series: [
                {
                  // name: "userName",
                  id: arrAppId,
                  type: "bar",
                  // barWidth: '60%',
                  barMaxWidth: 50,
                  data: arrCount
                }
              ]
            }
            obj.setOption(option)
          }
        })
      }
    }

    function echartRight(echartPages, totaPage, obj, id) {
      $.ajax({
        url: url + ":9006/AppMonitor/AppModuleMonitor/10/" + echartPages,
        type: "POST",
        data: JSON.stringify({
          appId: id
        }),
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        success: function(data) {
          // console.log(data)
          console.log(data)
          var echart = echarts.init(document.getElementById("detailEcharts")),
            data = data.data.content,
            modules = [],
            number = [],
            usercount = []

          for (var i = 0; i < data.length; i++) {
            modules.push(data[i].module_name)
            number.push(data[i].number)
            usercount.push(data[i].usercount)
          }
          option = {
            title: {
              text: "单位(人)"
            },
            color: ["#7EC0EE", "#7CCD7C"],
            tooltip: {
              trigger: "axis",
              axisPointer: {
                type: "shadow"
              },
              formatter: function(params, ticket, callback) {
                var htmlStr = ""
                for (var i = 0; i < params.length; i++) {
                  var param = params[i]
                  var xName = param.name //x轴的名称
                  var seriesName = param.seriesName //图例名称
                  var seriesIndex = param.seriesIndex
                  var value = param.value //y轴值
                  var color = param.color //图例颜色
                  // console.log( param)
                  if (i === 0) {
                    htmlStr += xName + "<br/>" //x轴的名称
                  }
                  if (seriesIndex === 1) {
                    value += "%"
                  }
                  htmlStr += "<div>"
                  //为了保证和原来的效果一样，这里自己实现了一个点的效果
                  htmlStr += '<span style="margin-right:5px;display:inline-block;width:10px;height:10px;border-radius:5px;background-color:' + color + ';"></span>'

                  //圆点后面显示的文本
                  htmlStr += seriesName + "：" + value
                  // htmlStr += seriesName0 + '：' + value + seriesName1 + '：' + value + '%';

                  htmlStr += "</div>"
                }
                return htmlStr
              }
            },
            legend: {
              data: ["人数", "使用率"]
            },

            calculable: true,
            xAxis: [
              {
                type: "category",
                axisTick: {
                  show: false
                },
                data: modules,
                axisLabel: {
                  interval: 0,
                  rotate: 40
                }
              }
            ],
            yAxis: [
              {
                type: "value",
                minInterval: 1
              }
            ],
            series: [
              {
                name: "人数",
                type: "bar",
                barGap: 0,
                barMaxWidth: 50,
                data: usercount
              },
              {
                name: "使用率",
                type: "bar",
                barMaxWidth: 50,
                data: number
              }
            ]
          }
          obj.setOption(option)
        }
      })
    }

    $("#monitor-software-monitor-btn").click(function() {
      // 搜索按钮
      var appName = $("#monitor-software-monitor-input").val()
      param.appName = appName
      active.getEchart(param.appName)
      clearInterval(intervalAppId)
      clearInterval(intervalAppName)
      intervalAppName = setInterval(function() {
        active.getEchart(param.appName)
      }, 8000)
    })
    $("#monitor-software-monitor-input").on("keypress", function() {
      // 搜索按钮
      if (event.keyCode == "13") {
        var appName = $("#monitor-software-monitor-input").val()
        param.appName = appName
        active.getEchart(param.appName)
        clearInterval(intervalAppId)
        clearInterval(intervalAppName)
        intervalAppName = setInterval(function() {
          active.getEchart(param.appName)
        }, 8000)
      }
    })

    $("#monitor-software-monitor-icon").click(function() {
      $("#monitor-software-monitor-input").val("")
      active.getEchart()
      clearInterval(intervalAppId)
      clearInterval(intervalAppName)
    })

    // 单选按钮选择
    form.on("radio(selectType)", function(data) {
      console.log($(this).val())
      console.log(data)
      var params = data.value
      switch (params) {
        case "1": // 选择用户
          $("#treeList").html("")
          active.getTree()
          break
        case "2": // ;连接类型
          $("#treeList").html("")
          $.ajax({
            url: url + ":9006/AppMonitor/AppType",
            tyep: "GET",
            success: function(res) {
              console.log(res.data)
              var res = res.data
              console.log(res)
              let li = ""
              let str = "<div id='page'></div>"
              for (let i = 0; i < res.length; i++) {
                li += "<div data-id=" + res[i].typeKey + ">" + res[i].value + "</div>"
              }
              $("#treeList").html("")
              $("#treeList").append(li)
              $("#paging").append(str)
              $("#treeList div").click(function() {
                $(this)
                  .css("background", "rgb(18, 160, 255)")
                  .siblings()
                  .css("background", "")
                // 点击用户名称
                console.log($(this).attr("data-id"))
                var res = $(this).attr("data-id")
                // console.log(connectionType)
                param.appType = res
                param.departmentId = ""
                console.log(param)
                active.getEchart()
              })
              $("#treeList div")
                .mouseover(function() {
                  $(this).addClass("mouseover")
                })
                .mouseout(function() {
                  $(this).removeClass("mouseover")
                })
            }
          })
          break
        default:
          "1"
          break
      }
    })
  })
})()
