(function() {
  //JavaScript代码区域
  layui.use(['jquery', 'element', 'form', 'tree', 'layedit', 'laydate', 'table', 'layer'], function() {
    var element = layui.element,
      table = layui.table,
      jquery = layui.jquery,
      layer = layui.layer,
      tree = layui.tree,
      laydate = layui.laydate,
      form = layui.form;

    var params = {
      day: '7',
      appName: '',
      appType: '',
      userId: '',
      departmentId: '',
      stateTime: '',
      endTime: ''
    };
    var active = {
      getEchart: function(day, appName, appType, departmentId,userId) {
        $.ajax({
          // 默认最近一周数据 - 页面加载显示
          type: 'POST',
          url: url + ':9006/AppMonitorHistory/WeekOrMonth/' + day,
          data: appName == '' ? JSON.stringify({ appType: appType, departmentId: departmentId,userId: userId}) : JSON.stringify({ appName: appName }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            myChart.clear();
            if (data.data != '') {
              for (var j in data.data[0]) {
                count++;
              }
              count = count - 1;
            }
            if (data.data != '') {
              // 当数据不为空的时候渲染echart;
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];
              for (var i = 0; i < datas[software].length; i++) {
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            } else {
              layer.msg('暂无数据');
            }
          }
        });
      },
      getDiyEchart: function(stateTime, endTime, appName, appType, departmentId) {
        $.ajax({
          type: 'POST',
          url: url + ':9006/AppMonitorHistory/DIYTime/' + stateTime + '/' + endTime,
          data: JSON.stringify({ appName: appName, appType: appType, departmentId: departmentId }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function(data) {
            console.log(data);
            if (data.data != '') {
              for (var j in data.data[0]) {
                count++;
              }
              count = count - 1;
            }
            console.log(data)
            if (data.data != '') {
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];
              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(2);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            } else {
              layer.msg('暂无数据');
            }
          }
        });
      },
      exports: function(url, queryType, appType, departmentId, stateTime, endTime) {
        var form = $('<form></form>')
          .attr('action', url)
          .attr('method', 'get');
        form.append(
          $('<input></input>')
            .attr('type', 'hidden')
            .attr('name', 'queryType')
            .attr('value', queryType)
        );
        form.append(
          $('<input></input>')
            .attr('type', 'hidden')
            .attr('name', 'appType')
            .attr('value', appType)
        );
        form.append(
          $('<input></input>')
            .attr('type', 'hidden')
            .attr('name', 'departmentId')
            .attr('value', departmentId)
        );
        form.append(
          $('<input></input>')
            .attr('type', 'hidden')
            .attr('name', 'startTime')
            .attr('value', stateTime)
        ); //传递自定义开始时间参数
        form.append(
          $('<input></input>')
            .attr('type', 'hidden')
            .attr('name', 'endTime')
            .attr('value', endTime)
        ); //传递自定义结束时间参数
        form
          .appendTo('body')
          .submit()
          .remove();
      },
      getTree: function(val) {
        $.ajax({
          url: url + ':9000/organization/getOrgTreeAndIncludeUsers',
          // url: 'http://10.1.3.98:9000/organization/getOrgTreeAndIncludeUsers',
          type: 'get',
          success: function(data) {
            console.log(data)
            layui.tree({
              elem: '#treeList', //指定元素
              nodes: data.data,
              spread: true,
              click: function (node) {
                console.log(node)
                if(node.children) {
                  params.appType = ''
                  params.userId = ''
                  params.departmentId = node.id
                  active.getEchart(params.day, params.appName, params.appType, params.departmentId,params.userId)
                }else{
                  params.userId = node.id
                  params.departmentId = ''
                  params.appType = ''
                  active.getEchart(params.day, params.appName, params.appType, params.departmentId,params.userId)
                }
              }
            })
          }
        })
        $('body').on('mousedown', '.layui-tree a', function () {
          $('.layui-tree a').css('background', '')
          $(this).css('background', 'rgb(18, 160, 255)')
        })
      }
    };

    // 默认获取一次周数据
    active.getEchart(params.day, params.appName, params.appType, params.departmentId);
    active.getTree()
    $.ajax({
      //获取设备类型
      type: 'GET',
      url: url + ':9006/AppMonitor/AppType',
      success: function(data) {
        // console.log(data);
        var data = data.data;
        var root = document.getElementById('typeSelection');
        console.log(root)
        for (var i = 0; i < data.length; i++) {
          var option = document.createElement('option');
          option.setAttribute('title', data[i].typeKey);
          option.innerText = data[i].value;
          root.appendChild(option);
          form.render('select');
        }
      }
    });



    $('body').on('mousedown', '.layui-tree a', function() {
      $('.layui-tree a').css('background', '#fff');
      $(this).css('background', 'rgb(18, 160, 255)');
    });

    $('.downpanel')
      .on('click', '.layui-select-title', function(e) {
        // 下拉树状图
        $('.layui-form-select')
          .not($(this).parents('.layui-form-select'))
          .removeClass('layui-form-selected');
        $(this)
          .parents('.downpanel')
          .toggleClass('layui-form-selected');
        layui.stope(e);
      })
      .on('click', 'dl i', function(e) {
        layui.stope(e);
      });

    var myChart = echarts.init(document.getElementById('main'));
    var index = 0; // 软件数组内的软件下标,用来更改软件
    var count = 0;

    form.on('select(typeSelection)', function(data) {
      // 软件类型
      appType = data.elem[data.elem.selectedIndex].title;
      params.appType = appType;
      active.getEchart(params.day, params.appName, params.appType, params.departmentId);
    });
    form.on('radio(dateType)', function(data) {
      //获取自定义时间
      console.log(data);
      count = 0;
      var stateTime = $('#test1').val('');
      var endTime = $('#test2').val('');
      params.day = data.value == 1 ? '7' : '30';
      active.getEchart(params.day, params.appName, params.appType, params.departmentId);
    });

    laydate.render({
      //页面自定义开始时间渲染
      elem: '#test1' //指定元素
    });

    laydate.render({
      //页面自定义结束时间渲染
      elem: '#test2' //指定元素
    });

    $('.softwareReportQuery').click(function() {
      // 点击查询按钮
      params.stateTime = $('#test1').val();
      params.endTime = $('#test2').val();
      var appNames = $('#monitor-software-report-input').val();
      if (params.stateTime == '' || params.endTime == '') {
        layer.msg('请输入正确的查询时间');
        return false;
      }
      active.getDiyEchart(params.stateTime, params.endTime, params.appName, params.appType, params.departmentId);
    });

    $('.softwareReportExport').click(function() {
      // 点击导出按钮
      if (params.stateTime != '' || params.endTime != '') {
        //导出自定义
        active.exports(url + ':9006/AppMonitorHistory/export', 'diy', params.appType, params.departmentId, params.stateTime, params.endTime);
        return false;
      }
      switch (
        params.day //折线数据
      ) {
        case '7': //导出星期
          active.exports(url + ':9006/AppMonitorHistory/export', 'week', params.appType, params.departmentId);
          break;
        case '30': //导出月
          active.exports(url + ':9006/AppMonitorHistory/export', 'month', params.appType, params.departmentId);
          break;
      }
    });

    $('#monitor-software-report-btn').click(function() {
      // 点击搜索按钮
      var appName = $('#monitor-software-report-input').val();
      params.appName = appName;
      active.getEchart(params.day, params.appName, params.appType, params.departmentId);
    });
    $('#monitor-software-report-input').on('keypress',function() {
      // 点击搜索按钮
      if(event.keyCode == "13") {
        var appName = $('#monitor-software-report-input').val();
        params.appName = appName;
        active.getEchart(params.day, params.appName, params.appType, params.departmentId);
      }
    });

    $('#monitor-software-report-icon').click(function() {
      // 清空按钮
      $('#monitor-software-report-input').val('');
      active.getEchart(params.day, '', params.appType, params.departmentId);
    });

    $('.arrowRight').click(function() {
      // 向后翻页 - 切换软件名
      console.log(index, count);
      var stateTime = $('#test1').val();
      var endTime = $('#test2').val();
      switch (params.day) {
        case '7':
          myChart.clear();
          index >= count ? (index = count) : index++;
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            // url: url + ':9006/AppMonitorHistory/DIYTime/'+stateTime+'/'+endTime,
            url: url + ':9006/AppMonitorHistory/WeekOrMonth/7',
            data: JSON.stringify({
              appType: params.appType,
              departmentId: params.departmentId,
              appName: params.appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
                // console.log(key)
              }
              software = software[index];
              console.log(datas[software]);
              if (datas[software] === undefined) {
                index = index;
              }
              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
          break;
        case '30':
          myChart.clear();
          index >= count ? (index = count) : index++;
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            url: url + ':9006/AppMonitorHistory/WeekOrMonth/30',
            data: JSON.stringify({
              appType: params.appType,
              departmentId: params.departmentId,
              appName: params.appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index, count);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
          break;
        case '3':
          myChart.clear();
          index >= count ? (index = count) : index++;
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            url: url + ':9006/AppMonitorHistory/DIYTime/' + stateTime + '/' + endTime,
            // url: 'http://10.1.1.213:9006/AppMonitorHistory/DIYTime/'+stateTime+'/'+endTime,
            data: JSON.stringify({
              appType: appType,
              departmentId: departmentId,
              appName: appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
                // console.log(key)
              }
              software = software[index];
              console.log(software[index]);

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
          break;
        default:
          myChart.clear();
          index >= count ? (index = count) : index++;
          console.log(index, count);
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            // url: url + ':9006/AppMonitorHistory/DIYTime/'+stateTime+'/'+endTime,
            url: url + ':9006/AppMonitorHistory/WeekOrMonth/7',
            data: JSON.stringify({
              appType: params.appType,
              departmentId: params.departmentId,
              appName: params.appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];
              console.log(software);

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
      }
    });

    $('.arrowLeft').click(function() {
      // 向前翻页 - 切换软件名
      var stateTime = $('#test1').val();
      var endTime = $('#test2').val();
      switch (params.day) {
        case '1':
          myChart.clear();
          if (index == 0) {
            index = 0;
          } else {
            index--;
          }
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            url: url + ':9006/AppMonitorHistory/WeekOrMonth/7',
            data: JSON.stringify({
              appType: params.appType,
              departmentId: params.departmentId,
              appName: params.appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
          break;
        case '2':
          myChart.clear();
          if (index == 0) {
            index = 0;
          } else {
            index--;
          }
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            url: url + ':9006/AppMonitorHistory/WeekOrMonth/30',
            data: JSON.stringify({
              appType: params.appType,
              departmentId: params.departmentId,
              appName: params.appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
          break;
        case '3':
          myChart.clear();
          if (index == 0) {
            index = 0;
          } else {
            index--;
          }
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            url: url + ':9006/AppMonitorHistory/DIYTime/' + stateTime + '/' + endTime,
            data: JSON.stringify({
              appType: appType,
              departmentId: departmentId,
              appName: appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
          break;
        default:
          myChart.clear();
          if (index == 0) {
            index = 0;
          } else {
            index--;
          }
          $.ajax({
            // 默认最近一周数据
            type: 'POST',
            url: url + ':9006/AppMonitorHistory/WeekOrMonth/7',
            data: JSON.stringify({
              appType: params.appType,
              departmentId: params.departmentId,
              appName: params.appNames
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function(data) {
              console.log(data);
              var datas = data.data[0],
                xName = [],
                userCount = [],
                fullDay = 0,
                isFull = [],
                software = [];
              if (data.data == '') {
                layer.msg('暂无数据!');
                return false;
              }
              for (var key in data.data[0]) {
                // 将软件名push进数组
                software.push(key);
              }
              software = software[index];

              for (var i = 0; i < datas[software].length; i++) {
                console.log(datas[software][i]);
                xName.push(datas[software][i]['X1'].date);
                userCount.push(datas[software][i]['X1'].count);
                if (datas[software][i]['X2'].mNames != '') {
                  // 计算占满天数
                  fullDay++;
                }
                if (datas[software][i]['X2'].isfull === 'yes') {
                  // 计算是否占满
                  isFull.push(5);
                }
                if (datas[software][i]['X2'].isfull === 'no') {
                  isFull.push('');
                }
              }
              console.log(isFull, index);
              // console.log(userCount)
              option = {
                title: {
                  text: '软件:' + data.data[0][software][0].key.appName,
                  subtext: '占满天数:' + fullDay
                },
                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                  },
                  formatter: function(params) {
                    // console.log(params)
                    var tar1 = params[0];
                    var tar2 = params[1];
                    return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
                  }
                },
                // grid: {
                //     left: '3%',
                //     right: '4%',
                //     bottom: '3%',
                //     containLabel: true
                // },
                xAxis: {
                  type: 'category',
                  splitLine: {
                    show: false
                  },
                  data: xName
                },
                yAxis: {
                  type: 'value',
                  minInterval: 1
                },
                series: [
                  {
                    name: '许可占满',
                    type: 'bar',
                    color: ['#EEB422'],
                    stack: '总量',
                    data: isFull,
                    barMaxWidth: 80
                  },
                  {
                    name: '使用人数',
                    type: 'line',
                    color: ['#66CD00'],
                    data: userCount
                  }
                ]
              };
              myChart.setOption(option);
            }
          });
      }
    });

    function SoftwareReportRight(appType, departmentId, obj, totaPage, date) {
      console.log(222222)
      $.ajax({
        // 默认最近一周数据
        type: 'POST',
        url: url + ':9006/AppMonitorHistory/WeekOrMonth/7',
        data: JSON.stringify({}),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function(data) {
          console.log(data);
          var datas = data.data[0],
            xName = [],
            userCount = [],
            fullDay = 0,
            isFull = [],
            software = [];
          for (var key in data.data[0]) {
            // 将软件名push进数组
            software.push(key);
          }
          software = software[index];

          for (var i = 0; i < datas[software].length; i++) {
            console.log(datas[software][i]);
            xName.push(datas[software][i]['X1'].date);
            userCount.push(datas[software][i]['X1'].count);
            if (datas[software][i]['X2'].mNames != '') {
              // 计算占满天数
              fullDay++;
            }
            if (datas[software][i]['X2'].isfull === 'yes') {
              // 计算是否占满
              isFull.push(5);
            }
            if (datas[software][i]['X2'].isfull === 'no') {
              isFull.push('');
            }
          }
          console.log(isFull, index);
          // console.log(userCount)
          option = {
            title: {
              text: '软件:' + data.data[0][software][0].key.appName,
              subtext: '占满天数:' + fullDay
            },
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                // 坐标轴指示器，坐标轴触发有效
                type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
              },
              formatter: function(params) {
                // console.log(params)
                var tar1 = params[0];
                var tar2 = params[1];
                return tar1.name + '<br/>' + tar2.seriesName + ' : ' + tar2.value + '<br/>' + tar1.seriesName + ' : ' + tar1.value + '<br/>';
              }
            },
            xAxis: {
              type: 'category',
              splitLine: {
                show: false
              },
              data: xName
            },
            yAxis: {
              type: 'value',
              minInterval: 1
            },
            series: [
              {
                name: '许可占满',
                type: 'bar',
                color: ['#EEB422'],
                stack: '总量',
                data: isFull,
                barMaxWidth: 80
              },
              {
                name: '使用人数',
                type: 'line',
                color: ['#66CD00'],
                data: userCount
              }
            ]
          };
          myChart.setOption(option);
        }
      });
    }

    // 单选按钮选择
    form.on("radio(selectType)", function(data) {
      console.log($(this).val())
      console.log(data)
      var param = data.value
      switch (param) {
        case "1": // 选择用户
          $("#treeList").html("")
          active.getTree()
          break
        case "2": // ;连接类型
          $("#treeList").html("")
          $.ajax({
            url: url + ':9006/AppMonitor/AppType',
            tyep: "GET",
            success: function(res) {
              console.log(res.data)
              var res = res.data
              console.log(res)
              let li = ""
              let str = "<div id='page'></div>"
              for (let i = 0; i < res.length; i++) {
                // console.log(res[i].userName)
                // li += `<li data-value="${res[i].value}">${res[i].value}</li>`;
                li += "<div data-id=" + res[i].typeKey + ">" + res[i].value + "</div>"
              }
              $("#treeList").html("")
              $("#treeList").append(li)
              $("#paging").append(str)
              $("#treeList div").click(function() {
                $(this).css("background", "rgb(18, 160, 255)").siblings().css("background", "")
                // 点击用户名称
                console.log($(this).attr("data-id"))
                var res = $(this).attr("data-id")
                params.appType = res
                active.getEchart(params.day, params.appName, params.appType, params.departmentId)
                
              })
              $("#treeList div").mouseover(function() {
                $(this).addClass("mouseover")
              }).mouseout(function() {
                $(this).removeClass("mouseover")
              })
            }
          })
          break
        default:
          "1"
          break
      }
    })
  });
})();
