vm = new Vue({
  el: "#Journal",
  data: function() {
    return {
      orders: ["ascending", "descending"],
      searchVal: "",
      date: "",
      tableData: [],
      currentPage: 1,
      size: 15,
      total: 0,
      sortString: "",
      sortMode: "",
      startTime: "",
      endTime: ""
    }
  },
  mounted() {
    this.init()
  },
  methods: {
    init() {
      this.getTableData()
      // this.handleCurrentChange()
    },
    // 获取表格信息
    getTableData(val) {
      var _this = this
      $.ajax({
        url: url + ":9001/mongo/logList",
        type: "post",
        data: {
          currentPage: val || _this.currentPage,
          pageSize: _this.size,
          conditions: _this.searchVal,
          startTime: _this.startTime,
          endTime: _this.endTime,
          sortString: _this.sortString,
          sortMode: _this.sortMode
        },
        success: function(data) {
          _this.tableData = data.data.logList
          _this.total = data.data.count
        }
      })
    },
    // 导出报表
    handleExport() {
      // window.location.href = url + ":9001/mongo/exportExcel" + "?val=" + this.searchVal + "&startTime=" + this.date[0] || "" + "&endTime=" + this.date[1] || ""
      if(this.date!='' && this.date!=null){
        window.location.href = url + `:9001/mongo/exportExcel?val=${this.searchVal}&startTime=${this.date[0] + ' 00:00:00' || ""}&endTime=${this.date[1] + ' 23:59:59' || ""}`
      }else{
        window.location.href = url + `:9001/mongo/exportExcel?val=${this.searchVal}&startTime=&endTime=`
      }
    },
    // 排序
    handleSort(column, prop, order) {
      this.sortString = column.prop
      if (column.order == "ascending") {
        this.sortMode = "asc"
      } else {
        this.sortMode = "desc"
      }
      this.getTableData()
    },
    // 分页
    handleCurrentChange(val) {
      this.currentPage = val
      this.getTableData(val)
    },
    // 查询按钮 日期+搜索框
    handleSearchDate() {
      console.log(this.date, this.searchVal)
      if(this.date!='' && this.date!=null){
        this.startTime = this.date[0] + " 00:00:00"
        this.endTime = this.date[1] + " 23:59:59"
      }else{
        this.startTime = ''
        this.endTime =''
      }
      this.getTableData()
    },
    // 搜索按钮
    handleSearch() {
      this.getTableData()
    }
  }
})
