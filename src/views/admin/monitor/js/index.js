(function () {
  //JavaScript代码区域
  layui.use(['element', 'form', 'layedit', 'laydate', "tree", "table"], function () {
    var element = layui.element,
      tree = layui.tree,
      table = layui.table,
      form = layui.form;
    tree({
      elem: '#treeList',
      nodes: [{ //节点数据
        name: '节点A',
        spread: true,
        children: [{
          name: '节点A1'
        }]
      }, {
        name: '节点B',
        children: [{
          name: '节点B1',
          alias: 'bb' //可选
            ,
          id: '123' //可选
        }, {
          name: '节点B2'
        }]
      }],
      click: function (node) {
        console.log(node) //node即为当前点击的节点数据
      }
    });


    //方法级渲染
    table.render({
      elem: '#LAY_table_user',
      //url: '/demo/table/user/',
      cols: [
        [{
          checkbox: true,
          fixed: true
        }, {
          field: 'id',
          title: 'ID',
          width: 80,
          sort: true,
          fixed: true
        }, {
          field: 'username',
          title: '用户名',
          width: 80
        }, {
          field: 'sex',
          title: '性别',
          width: 80,
          sort: true
        }, {
          field: 'city',
          title: '城市',
          width: 80
        }, {
          field: 'sign',
          title: '签名'
        }, {
          field: 'experience',
          title: '积分',
          sort: true,
          width: 80
        }, {
          field: 'score',
          title: '评分',
          sort: true,
          width: 80
        }, {
          field: 'classify',
          title: '职业',
          width: 80
        }, {
          field: 'wealth',
          title: '财富',
          sort: true,
          width: 135
        }]
      ],
      id: 'testReload',
      page: true,
      //height: 315
    });

    /*         var $ = layui.$,
                active = {
                    reload: function() {
                        var demoReload = $('#demoReload');

                        //执行重载
                        table.reload('testReload', {
                            page: {
                                curr: 1 //重新从第 1 页开始
                            },
                            where: {
                                key: {
                                    id: demoReload.val()
                                }
                            }
                        });
                    }
                }; */

    $('.demoTable .layui-btn').on('click', function () {
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });

    form.on('submit(demo1)', function (data) {
      cosnole.log(data)
      // layer.alert(JSON.stringify(data.field), {
      //     title: '最终的提交信息'
      // })
      return false;
    });

    $("#linkPost").click(function () {
      /* $.post("http://10.1.1.149:9000/AppConnection/open", { "appId": "1", "userId": "1", "resolution": "1920 x768", "screenCount": "1" }, function(params) {
          console.log("params")
          console.log(params)
          $.post("http://10.1.1.149:9000/AppConnection/downLoadVnc", {}, function(params2) {
              console.log("params2")
              console.log(params2)
          })
      }, "application/json") */
      $.ajax({
        type: "POST",
        url: "http://10.1.1.234:9006/AppConnection/open",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify({
          "userId": "5",
          "appId": "7",
          "resolution": "1920x1080",
          "screenCount": "1"
        }),
        success: function (msg) {
          console.log("msg")
          console.log(msg)
          $("#cont").val(msg[1]);
          $("#fileName").val(msg[0]);
          // $.post("http://10.1.1.149:9000/AppConnection/downLoadVnc", { fileName: msg[0], cont: msg[1] }, function(params2) {
          //     console.log("params2")
          //     console.log(params2)
          // }) 
        }
      });
    })

  });


})()