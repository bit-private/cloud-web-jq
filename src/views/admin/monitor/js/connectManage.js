
; (function () {
  //记录页数
  var pageNum = 0
  //JavaScript代码区域
  layui.use(['jquery', 'element', 'form', 'layedit', 'laydate', 'tree', 'table', 'laypage', 'layer'], function () {
    var element = layui.element,
      tree = layui.tree,
      table = layui.table,
      jquery = layui.jquery,
      layer = layui.layer,
      laypage = layui.laypage,
      form = layui.form
    var active = {
      init: function name (params) {
        // this.treeList();
        // this.tableList();
      },
      reload: function () {
        var demoReload = $('#demoReload')

        //执行重载
        table.reload('tableData', {
          page: {
            curr: 1 //重新从第 1 页开始
          },
          where: {
            /* key: {
                id: demoReload.val()
            } */
          }
        })
      },
      treeList: function (params) {
        tree({
          elem: '#treeList',
          nodes: [
            {
              //节点数据
              name: '节点',
              spread: true,
              children: [
                {
                  name: '节点A1'
                }
              ]
            },
            {
              name: '节点B',
              children: [
                {
                  name: '节点B1',
                  alias: 'bb', //可选
                  id: '123' //可选
                },
                {
                  name: '节点B2'
                }
              ]
            }
          ],
          click: function (node) {
            console.log(node) //node即为当前点击的节点数据
          }
        })
      },
      tableList: function (params) {
        // 列表数据
        laypage.render({
          elem: 'page',
          count: data.data.pageCount,
          layout: ['prev', 'page', 'next', 'refresh', 'skip'],
          // limit:10,
          jump: function (obj, first) {
            $.ajax({
              type: 'POST',
              url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
              data: JSON.stringify({}),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              success: function (data) {
                table.render({
                  elem: '#tableData',
                  data: data.data.content,
                  limit: 10,
                  id: 'batch',
                  cols: [
                    [
                      {
                        checkbox: true
                      },
                      {
                        field: 'type',
                        title: '状态',
                        // width: 60,
                        templet: function (d) {
                          if (d.state == '1') {
                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                          }
                          if (d.state == '0' && d.leaveTime == '0') {
                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                          }
                          if (d.state == '0') {
                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                          }
                          if (d.state == null) {
                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                          }
                        },
                        unresize: true
                      },
                      {
                        field: 'type',
                        title: '连接状态',
                        templet: function (d) {
                          if (d.connectionStatus == '1') {
                            // 如果活动状态为1 渲染绿色
                            return '<div class="connected">已连接</div>'
                          }
                          if (d.connectionStatus == '0') {
                            // 如果活动状态为1 渲染绿色
                            return '<div class="noConnected">未连接</div>'
                          }
                          if (d.connectionStatus == null) {
                            // 如果活动状态为1 渲染绿色
                            return '<div></div>'
                          }
                        },
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'realName',
                        title: '用户',
                        sort: true,
                        event: 'hover',
                        unresize: true
                      },
                      {
                        field: 'systemUserName',
                        title: '系统用户',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'departmentName',
                        title: '科室',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'ip',
                        title: 'IP',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'appName',
                        title: '软件',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'equipmentName',
                        title: '设备',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'port',
                        title: '端口',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'connectionType',
                        title: '连接方式',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'openTime',
                        title: '打开时间',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'leaveTime',
                        title: '闲置时间',
                        templet: function (d) {
                          return d.leaveTime + '小时'
                        },
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'shareUsers',
                        title: '共享信息',
                        sort: true,
                        unresize: true
                      }
                    ]
                  ]
                })
              }
            })
          }
        })
      },
      openDetail: function (params) {
        layer.open({
          type: 1,
          title: '详情', //不显示标题栏
          closeBtn: 1,
          area: ['95%', '520px'],
          shade: 0.3,
          id: 'detail_layui', //设定一个id，防止重复弹出
          btn: ['确定'],
          btnAlign: 'c',
          moveType: 1, //拖拽模式，0或者1
          content: $('#detailHtml'),
          end: function () {
            $('#detailHtml').hide()
          }
        })
      },
      tableFunction: function (params) {
        table.render({
          elem: '#tableData',
          data: params,
          limit: 10,
          id: 'batch',
          cols: [
            [
              {
                checkbox: true
              },
              {
                field: 'type',
                title: '状态',
                // width: 60,
                templet: function (d) {
                  if (d.state == '1') {
                    return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                  }
                  if (d.state == '0' && d.leaveTime == '0') {
                    return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                  }
                  if (d.state == '0') {
                    return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                  }
                  if (d.state == null) {
                    return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                  }
                },
                unresize: true
              },
              {
                field: 'realName',
                title: '用户',
                sort: true,
                event: 'hover',
                unresize: true
              },
              {
                field: 'systemUserName',
                title: '系统用户',
                sort: true,
                unresize: true
              },
              {
                field: 'departmentName',
                title: '科室',
                sort: true,
                unresize: true
              },
              {
                field: 'ip',
                title: 'IP',
                sort: true,
                unresize: true
              },
              {
                field: 'appName',
                title: '软件',
                sort: true,
                unresize: true
              },
              {
                field: 'equipmentName',
                title: '设备',
                sort: true,
                unresize: true
              },
              {
                field: 'port',
                title: '端口',
                sort: true,
                unresize: true
              },
              {
                field: 'connectionType',
                title: '连接方式',
                sort: true,
                unresize: true
              },
              {
                field: 'openTime',
                title: '打开时间',
                sort: true,
                unresize: true
              },
              {
                field: 'leaveTime',
                title: '闲置时间',
                templet: function (d) {
                  return d.leaveTime + '小时'
                },
                sort: true,
                unresize: true
              },
              {
                field: 'shareUsers',
                title: '共享信息',
                sort: true,
                unresize: true
              }
            ]
          ]
        })
      },
      getTree: function(val) {
        $.ajax({
          url: url + ':9000/organization/getOrgTreeAndIncludeUsers',
          // url: 'http://10.1.3.98:9000/organization/getOrgTreeAndIncludeUsers',
          type: 'get',
          success: function(data) {
            console.log(data)
            layui.tree({
              elem: '#treeList', //指定元素
              nodes: data.data,
              click: function (node) {
                console.log(node)
                if(node.children) {
                  active.getTable(node.id, '')
                  parameter = {
                    connectionType: '',
                    departmentId: node.id,
                    objValue: '',
                    userId: '',
                    appId: ''
                  }
                }else{
                  active.getTable('', node.id)
                  parameter = {
                    connectionType: '',
                    departmentId: '',
                    objValue: '',
                    userId: node.id,
                    appId: ''
                  }
                }
              }
            })
          }
        })
        $('body').on('mousedown', '.layui-tree a', function () {
          $('.layui-tree a').css('background', '')
          $(this).css('background', 'rgb(18, 160, 255)')
        })
      },
      getTable: function(departmentId, userId) {
        $.ajax({
          type: 'POST',
          url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
          data: JSON.stringify({
            departmentId: departmentId,
            userId: userId
          }),
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function (data) {
            console.log(data)
            laypage.render({
              elem: 'page',
              count: data.data.pageCount,
              layout: ['prev', 'page', 'next', 'refresh', 'skip'],
              jump: function (obj, first) {
                $.ajax({
                  type: 'POST',
                  url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                  data: JSON.stringify({
                    departmentId: departmentId,
                    userId: userId
                  }),
                  headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                  },
                  success: function (data) {
                    table.render({
                      elem: '#tableData',
                      data: data.data.content,
                      limit: 10,
                      id: 'batch',
                      cols: [
                        [
                          {
                            checkbox: true
                          },
                          {
                            field: 'type',
                            title: '状态',
                            // width: 60,
                            templet: function (d) {
                              if (d.state == '1') {
                                // 如果活动状态为1 渲染绿色
                                return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                              }
                              if (d.state == '0' && d.leaveTime == '0') {
                                return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                              }
                              if (d.state == '0') {
                                return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                              }
                              if (d.state == null) {
                                return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                              }
                            },
                            unresize: true
                          },
                          {
                            field: 'type',
                            title: '连接状态',
                            templet: function (d) {
                              if (d.connectionStatus == '1') {
                                // 如果活动状态为1 渲染绿色
                                return '<div class="connected">已连接</div>'
                              }
                              if (d.connectionStatus == '0') {
                                // 如果活动状态为1 渲染绿色
                                return '<div class="noConnected">未连接</div>'
                              }
                              if (d.connectionStatus == null) {
                                // 如果活动状态为1 渲染绿色
                                return '<div></div>'
                              }
                            },
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'realName',
                            title: '用户',
                            sort: true,
                            event: 'hover',
                            unresize: true
                          },
                          {
                            field: 'systemUserName',
                            title: '系统用户',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'departmentName',
                            title: '科室',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'ip',
                            title: 'IP',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'appName',
                            title: '软件',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'equipmentName',
                            title: '设备',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'port',
                            title: '端口',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'connectionType',
                            title: '连接方式',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'openTime',
                            title: '打开时间',
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'leaveTime',
                            title: '闲置时间',
                            templet: function (d) {
                              return d.leaveTime + '小时'
                            },
                            sort: true,
                            unresize: true
                          },
                          {
                            field: 'shareUsers',
                            title: '共享信息',
                            sort: true,
                            unresize: true
                          }
                        ]
                      ]
                    })
                  }
                })
              }
            })
          }
        })
      }
    }
    var parameter = {
      connectionType: '',
      departmentId: '',
      objValue: '',
      userId: '',
      appId: ''
    }
    active.init()
    $('.text-right .layui-btn').on('click', function () {
      var type = $(this).data('type')
      active[type] ? active[type].call(this) : ''
      // console.log($(this).html())
    })
    
    $.ajax({
      // 默认请求一次全部的数据
      type: 'POST',
      url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
      data: JSON.stringify({
        userId: ''
      }),
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      dataType: 'json',
      success: function (data) {
        console.log(data.data.pageCount)
        laypage.render({
          elem: 'page',
          count: data.data.pageCount,
          layout: ['prev', 'page', 'next', 'refresh', 'skip'],
          // limit:10,
          jump: function (obj, first) {
            $.ajax({
              type: 'POST',
              url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
              data: JSON.stringify({}),
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              success: function (data) {
                table.render({
                  elem: '#tableData',
                  data: data.data.content,
                  limit: 10,
                  id: 'batch',
                  cols: [
                    [
                      {
                        checkbox: true
                      },
                      {
                        field: 'type',
                        title: '状态',
                        // width: 60,
                        templet: function (d) {
                          if (d.state == '1') {
                            // 如果活动状态为1 渲染绿色
                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                          }
                          if (d.state == '0' && d.leaveTime == '0') {
                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                          }
                          if (d.state == '0') {
                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                          }
                          if (d.state == null) {
                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                          }
                        },
                        unresize: true
                      },
                      {
                        field: 'type',
                        title: '连接状态',
                        templet: function (d) {
                          if (d.connectionStatus == '1') {
                            // 如果活动状态为1 渲染绿色
                            return '<div class="connected">已连接</div>'
                          }
                          if (d.connectionStatus == '0') {
                            // 如果活动状态为1 渲染绿色
                            return '<div class="noConnected">未连接</div>'
                          }
                          if (d.connectionStatus == null) {
                            // 如果活动状态为1 渲染绿色
                            return '<div></div>'
                          }
                        },
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'realName',
                        title: '用户',
                        sort: true,
                        event: 'hover',
                        unresize: true
                      },
                      {
                        field: 'systemUserName',
                        title: '系统用户',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'departmentName',
                        title: '科室',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'ip',
                        title: 'IP',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'appName',
                        title: '软件',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'equipmentName',
                        title: '设备',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'port',
                        title: '端口',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'connectionType',
                        title: '连接方式',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'openTime',
                        title: '打开时间',
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'leaveTime',
                        title: '闲置时间',
                        templet: function (d) {
                          return d.leaveTime + '小时'
                        },
                        sort: true,
                        unresize: true
                      },
                      {
                        field: 'shareUsers',
                        title: '共享信息',
                        sort: true,
                        unresize: true
                      }
                    ]
                  ]
                })
              }
            })
          }
        })
      }
    })

    table.on('sort(tableData)', function (obj) {
      $.ajax({
        // 默认请求一次全部的数据
        type: 'POST',
        url: url + ':9006/AppConnection/appConnectionPage/10/0/' + obj.field + '/' + obj.type,
        data: JSON.stringify({parameter}),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function (data) {
          console.log(data)
          laypage.render({
            elem: 'page',
            count: data.data.pageCount,
            layout: ['prev', 'page', 'next', 'refresh', 'skip'],
            // limit:10,
            jump: function (obj, first) {
              $.ajax({
                type: 'POST',
                url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                data: JSON.stringify({}),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                success: function (data) {
                  table.render({
                    elem: '#tableData',
                    data: data.data.content,
                    limit: 10,
                    id: 'batch',
                    cols: [
                      [
                        {
                          checkbox: true
                        },
                        {
                          field: 'type',
                          title: '状态',
                          // width: 60,
                          templet: function (d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0' && d.leaveTime == '0') {
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                            if (d.state == null) {
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          },
                          unresize: true
                        },
                        {
                          field: 'type',
                          title: '连接状态',
                          templet: function (d) {
                            if (d.connectionStatus == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<div class="connected">已连接</div>'
                            }
                            if (d.connectionStatus == '0') {
                              // 如果活动状态为1 渲染绿色
                              return '<div class="noConnected">未连接</div>'
                            }
                            if (d.connectionStatus == null) {
                              // 如果活动状态为1 渲染绿色
                              return '<div></div>'
                            }
                          },
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'realName',
                          title: '用户',
                          sort: true,
                          event: 'hover',
                          unresize: true
                        },
                        {
                          field: 'systemUserName',
                          title: '系统用户',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'departmentName',
                          title: '科室',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'ip',
                          title: 'IP',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'equipmentName',
                          title: '设备',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'port',
                          title: '端口',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'connectionType',
                          title: '连接方式',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'openTime',
                          title: '打开时间',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'leaveTime',
                          title: '闲置时间',
                          templet: function (d) {
                            return d.leaveTime + '小时'
                          },
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'shareUsers',
                          title: '共享信息',
                          sort: true,
                          unresize: true
                        }
                      ]
                    ]
                  })
                }
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                // console.log(first);
                //首次不执行
                // if(!first){
                //   // pageNum = obj.curr-1;
                //   active.tableList(obj.curr,orgId,where,sort,column);
                //   //do something
                // }
              })
            }
          })
        }
      })
    })

    var openInfo
    var closeIds = ''
    table.on('checkbox(tableData)', function (obj) {
      console.log(obj)
      return (openInfo = obj)
    })

    $('#treeList').html('')
    active.getTree()

    $('#monitor-connect-btn').click(function () {
      // 搜索功能
      var appName = $('#monitor-connect-input').val()
      parameter = {
        connectionType: '',
        departmentId: '',
        objValue: appName,
        userId: '',
        appId: ''
      }
      // console.log(appName)
      $.ajax({
        type: 'POST',
        url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
        data: JSON.stringify({
          objValue: appName
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function (data) {
          // console.log(data)
          laypage.render({
            elem: 'page',
            count: data.data.pageCount,
            layout: ['prev', 'page', 'next', 'refresh', 'skip'],
            // limit:10,
            jump: function (obj, first) {
              $.ajax({
                type: 'POST',
                url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                data: JSON.stringify({ objValue: appName }),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                success: function (data) {
                  table.render({
                    elem: '#tableData',
                    data: data.data.content,
                    limit: 10,
                    id: 'batch',
                    cols: [
                      [
                        {
                          checkbox: true
                        },
                        {
                          field: 'type',
                          title: '状态',
                          // width: 60,
                          templet: function (d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0' && d.leaveTime == '0') {
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                            if (d.state == null) {
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          },
                          unresize: true
                        },
                        {
                          field: 'type',
                          title: '连接状态',
                          templet: function (d) {
                            if (d.connectionStatus == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<div class="connected">已连接</div>'
                            }
                            if (d.connectionStatus == '0') {
                              // 如果活动状态为1 渲染绿色
                              return '<div class="noConnected">未连接</div>'
                            }
                            if (d.connectionStatus == null) {
                              // 如果活动状态为1 渲染绿色
                              return '<div></div>'
                            }
                          },
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'realName',
                          title: '用户',
                          sort: true,
                          event: 'hover',
                          unresize: true
                        },
                        {
                          field: 'systemUserName',
                          title: '系统用户',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'departmentName',
                          title: '科室',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'ip',
                          title: 'IP',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'equipmentName',
                          title: '设备',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'port',
                          title: '端口',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'connectionType',
                          title: '连接方式',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'openTime',
                          title: '打开时间',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'leaveTime',
                          title: '闲置时间',
                          templet: function (d) {
                            return d.leaveTime + '小时'
                          },
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'shareUsers',
                          title: '共享信息',
                          sort: true,
                          unresize: true
                        }
                      ]
                    ]
                  })
                }
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                // console.log(first);
                //首次不执行
                // if(!first){
                //   // pageNum = obj.curr-1;
                //   active.tableList(obj.curr,orgId,where,sort,column);
                //   //do something
                // }
              })
            }
          })
        }
      })
    })
    $('#monitor-connect-input').on('keypress',function () {
      // 搜索功能
        if(event.keyCode == "13") {
            var appName = $('#monitor-connect-input').val()
            // console.log(appName)
            $.ajax({
                type: 'POST',
                url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
                data: JSON.stringify({
                    objValue: appName
                }),
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                dataType: 'json',
                success: function (data) {
                    // console.log(data)
                    laypage.render({
                        elem: 'page',
                        count: data.data.pageCount,
                        layout: ['prev', 'page', 'next', 'refresh', 'skip'],
                        // limit:10,
                        jump: function (obj, first) {
                            $.ajax({
                                type: 'POST',
                                url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                                data: JSON.stringify({objValue: appName}),
                                headers: {
                                    'Content-Type': 'application/json;charset=utf-8'
                                },
                                success: function (data) {
                                    table.render({
                                        elem: '#tableData',
                                        data: data.data.content,
                                        limit: 10,
                                        id: 'batch',
                                        cols: [
                                            [
                                                {
                                                    checkbox: true
                                                },
                                                {
                                                    field: 'type',
                                                    title: '状态',
                                                    // width: 60,
                                                    templet: function (d) {
                                                        if (d.state == '1') {
                                                            // 如果活动状态为1 渲染绿色
                                                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                                        }
                                                        if (d.state == '0' && d.leaveTime == '0') {
                                                            return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                                        }
                                                        if (d.state == '0') {
                                                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                                        }
                                                        if (d.state == null) {
                                                            return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                                        }
                                                    },
                                                    unresize: true
                                                },
                                              {
                                                field: 'type',
                                                title: '连接状态',
                                                templet: function (d) {
                                                  if (d.connectionStatus == '1') {
                                                    // 如果活动状态为1 渲染绿色
                                                    return '<div class="connected">已连接</div>'
                                                  }
                                                  if (d.connectionStatus == '0') {
                                                    // 如果活动状态为1 渲染绿色
                                                    return '<div class="noConnected">未连接</div>'
                                                  }
                                                  if (d.connectionStatus == null) {
                                                    // 如果活动状态为1 渲染绿色
                                                    return '<div></div>'
                                                  }
                                                },
                                                sort: true,
                                                unresize: true
                                              },
                                                {
                                                    field: 'realName',
                                                    title: '用户',
                                                    sort: true,
                                                    event: 'hover',
                                                    unresize: true
                                                },
                                                {
                                                    field: 'systemUserName',
                                                    title: '系统用户',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'departmentName',
                                                    title: '科室',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'ip',
                                                    title: 'IP',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'appName',
                                                    title: '软件',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'equipmentName',
                                                    title: '设备',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'port',
                                                    title: '端口',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'connectionType',
                                                    title: '连接方式',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'openTime',
                                                    title: '打开时间',
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'leaveTime',
                                                    title: '闲置时间',
                                                    templet: function (d) {
                                                        return d.leaveTime + '小时'
                                                    },
                                                    sort: true,
                                                    unresize: true
                                                },
                                                {
                                                    field: 'shareUsers',
                                                    title: '共享信息',
                                                    sort: true,
                                                    unresize: true
                                                }
                                            ]
                                        ]
                                    })
                                }
                                //obj包含了当前分页的所有参数，比如：
                                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                                // console.log(obj.limit); //得到每页显示的条数
                                // console.log(first);
                                //首次不执行
                                // if(!first){
                                //   // pageNum = obj.curr-1;
                                //   active.tableList(obj.curr,orgId,where,sort,column);
                                //   //do something
                                // }
                            })
                        }
                    })
                }
            })
        }
    })

    $('#monitor-connect-icon').click(function () {
      // 清空按钮
      $('#monitor-connect-input').val('')
      $.ajax({
        type: 'POST',
        url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
        data: JSON.stringify({
          objValue: ''
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function (data) {
          // console.log(data)
          laypage.render({
            elem: 'page',
            count: data.data.pageCount,
            layout: ['prev', 'page', 'next', 'refresh', 'skip'],
            // limit:10,
            jump: function (obj, first) {
              $.ajax({
                type: 'POST',
                url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                data: JSON.stringify({}),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                success: function (data) {
                  table.render({
                    elem: '#tableData',
                    data: data.data.content,
                    limit: 10,
                    id: 'batch',
                    cols: [
                      [
                        {
                          checkbox: true
                        },
                        {
                          field: 'type',
                          title: '状态',
                          // width: 60,
                          templet: function (d) {
                            if (d.state == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0' && d.leaveTime == '0') {
                              return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                            }
                            if (d.state == '0') {
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                            if (d.state == null) {
                              return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                            }
                          },
                          unresize: true
                        },
                        {
                          field: 'type',
                          title: '连接状态',
                          templet: function (d) {
                            if (d.connectionStatus == '1') {
                              // 如果活动状态为1 渲染绿色
                              return '<div class="connected">已连接</div>'
                            }
                            if (d.connectionStatus == '0') {
                              // 如果活动状态为1 渲染绿色
                              return '<div class="noConnected">未连接</div>'
                            }
                            if (d.connectionStatus == null) {
                              // 如果活动状态为1 渲染绿色
                              return '<div></div>'
                            }
                          },
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'realName',
                          title: '用户',
                          sort: true,
                          event: 'hover',
                          unresize: true
                        },
                        {
                          field: 'systemUserName',
                          title: '系统用户',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'departmentName',
                          title: '科室',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'ip',
                          title: 'IP',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'appName',
                          title: '软件',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'equipmentName',
                          title: '设备',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'port',
                          title: '端口',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'connectionType',
                          title: '连接方式',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'openTime',
                          title: '打开时间',
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'leaveTime',
                          title: '闲置时间',
                          templet: function (d) {
                            return d.leaveTime + '小时'
                          },
                          sort: true,
                          unresize: true
                        },
                        {
                          field: 'shareUsers',
                          title: '共享信息',
                          sort: true,
                          unresize: true
                        }
                      ]
                    ]
                  })
                }
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                // console.log(first);
                //首次不执行
                // if(!first){
                //   // pageNum = obj.curr-1;
                //   active.tableList(obj.curr,orgId,where,sort,column);
                //   //do something
                // }
              })
            }
          })
        }
      })
    })

    var lineId
    // 点击id显示详情
    table.on('tool(tableData)', function (params) {
      // 点击ID弹出详情页
      var data = params.data
      console.log(params)
      active.openDetail()
      // var checkStatus = table.checkStatus("tableData")
      //var url = url + ":9006/AppConnection/detail";
      var obj = data.id
      detailInfo(url, obj)
      function detailInfo (url, obj) {
        $.ajax({
          type: 'POST',
          url: url + ':9006/AppConnection/detail',
          data: JSON.stringify({
            id: obj
          }), // id的参数obj
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function (data) {
            console.log(data)
            $('.userid').html('')
            $('.institutes').html('')
            $('.department').html('')
            $('.tel').html('')
            $('.phone').html('')
            $('.software').html('')
            $('.city').html('')
            $('.sysusername').html('')
            $('.server').html('')
            $('.port').html('')
            $('.opentime').html('')
            $('.leavetime').html('')
            if (data.code === 400) {
              layer.msg(data.message)
            } else {
              var data = data.data
              $('.userid').html(data.userInfo.userName)
              $('.rankName').html(data.userInfo.rankName + '：')
              $('.institutes').html(data.userInfo.placesDepartment)
              $('.department').html(data.userInfo.department)
              $('.tel').html(data.userInfo.telephone)
              $('.phone').html(data.userInfo.phone)
              $('.software').html(data.userInfo.appName)
              $('.city').html(data.userInfo.ip)
              $('.sysusername').html(data.userInfo.sysUserName)
              $('.server').html(data.userInfo.equipmentName)
              $('.port').html(data.userInfo.port)
              $('.opentime').html(data.userInfo.openTime)
              $('.leavetime').html(data.userInfo.leaveTime + '小时')
              table.render({
                elem: '#detailTable',
                data: data.appModuleInfo,
                id: 'detailTable',
                limit: 10,
                // width: 800,
                cols: [
                  [
                    {
                      checkbox: true
                    },
                    {
                      field: 'id',
                      title: 'ID',
                      width: 50,
                      style: 'display:none;',
                      unresize: true
                    },
                    {
                      field: 'moduleName',
                      title: '模块',
                      unresize: true,
                      unresize: true
                    },
                    {
                      field: 'handle',
                      title: 'Handle No.',
                      unresize: true,
                      unresize: true
                    },
                    {
                      field: 'appOpentime',
                      title: '打开时间',
                      unresize: true,
                      unresize: true
                    },
                    {
                      field: 'operation',
                      title: '操作',
                      unresize: true,
                      templet: '#releaseBtn',
                      event: 'click',
                      unresize: true
                    }
                  ]
                ],
                done: function (res, curr, count) {
                  $('table.layui-table thead tr th:nth-child(1)').addClass('layui-hide')
                }
              })
            }
          }
        })
      }

      table.on('tool(detailTable)', function (params) {
        // 点击弹出页面释放按钮
        var data = params.data
        console.log(data.id)
        $.ajax({
          type: 'POST',
          url: url + ':9006/licenseService/closeLicenseProcess',
          data: JSON.stringify({
            id: data.id
          }), // id的参数obj
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function (data) {
            console.log(data)
          }
        })
      })
    })

    //详情打开
    $('#detail').on('click', function (params) {
      var checkStatus = table.checkStatus('batch'),
        data = checkStatus.data
      if (data.length == 1) {
        active.openDetail()
        var obj = checkStatus.data[0].id
        detailInfo(url, obj)
        function detailInfo (url, obj) {
          $.ajax({
            type: 'POST',
            url: url + ':9006/AppConnection/detail',
            // url: "http://10.1.1.213:9006/AppConnection/detail",
            data: JSON.stringify({
              id: obj
            }), // id的参数obj
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function (data) {
              // console.log(data.data.appModuleInfo)
              console.log(data)
              $('.userid').html('')
              $('.rankName').html('')
              $('.institutes').html('')
              $('.department').html('')
              $('.tel').html('')
              $('.phone').html('')
              $('.software').html('')
              $('.city').html('')
              $('.sysusername').html('')
              $('.server').html('')
              $('.port').html('')
              $('.opentime').html('')
              $('.leavetime').html('')
              if (data.code === 400) {
                layer.msg(data.message)
              } else {
                var data = data.data
                $('.userid').html(data.userInfo.userName)
                $('.rankName').html(data.userInfo.rankName + '：')
                $('.institutes').html(data.userInfo.placesDepartment)
                $('.department').html(data.userInfo.department)
                $('.tel').html(data.userInfo.telephone)
                $('.phone').html(data.userInfo.phone)
                $('.software').html(data.userInfo.appName)
                $('.city').html(data.userInfo.ip)
                $('.sysusername').html(data.userInfo.sysUserName)
                $('.server').html(data.userInfo.equipmentName)
                $('.port').html(data.userInfo.port)
                $('.opentime').html(data.userInfo.openTime)
                // $(".leavetime").html(data.userInfo.leaveTime)
                $('.leavetime').html(data.userInfo.leaveTime + '小时')
                table.render({
                  elem: '#detailTable',
                  data: data.appModuleInfo,
                  id: 'detailTable',
                  limit: 10,
                  // width: 800,
                  cols: [
                    [
                      {
                        checkbox: true
                      },
                      // { field: 'id', title: 'ID', width: 50, style: 'display:none;', 'unresize': true },
                      {
                        field: 'moduleName',
                        title: '模块',
                        unresize: true,
                        unresize: true
                      },
                      {
                        field: 'handle',
                        title: 'Handle No.',
                        unresize: true,
                        unresize: true
                      },
                      {
                        field: 'appOpentime',
                        title: '打开时间',
                        unresize: true,
                        unresize: true
                      },
                      {
                        field: 'operation',
                        title: '操作',
                        unresize: true,
                        templet: '#releaseBtn',
                        event: 'click',
                        unresize: true,
                        templet: function (d) {
                          return '<span class="layui-btn layui-btn-xs release" lay-event="release">释放</span>'
                        }
                      }
                    ]
                  ],
                  done: function (res, curr, count) { }
                })
              }
            }
          })
        }
      } else {
        layer.msg('请选择一条信息打开')
      }
    })
    table.on('tool(detailTable)', function (obj) {
      // 点击释放按钮
      // console.log(obj)
      var id = obj.data.id
      if (obj.event === 'release') {
        $.ajax({
          type: 'POST',
          url: url + ':9006/licenseService/closeLicenseProcess',
          data: JSON.stringify({
            id: id
          }), // id的参数obj
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          dataType: 'json',
          success: function (data) {
            console.log(data)
            layer.msg(data.message)
          }
        })
      }
    })
    form.on('radio(selectType)', function (data) {
      console.log($(this).val())
      console.log(data)
      var param = data.value
      switch (param) {
        case '1': // 选择用户
          $('#treeList').html('')
          active.getTree()
          break
        case '2': // 选择软件
          $('#treeList').html('')
          $.ajax({
            url: url + ':9006/AppConnection/appTypeOrder',
            tyep: 'GET',
            success: function (res) {
              var res = res.data
              parameter = {
                connectionType: '',
                departmentId: '',
                objValue: '',
                userId: '',
                appId: res
              }
              layui.tree({
                elem: '#treeList', //指定元素
                nodes: res,
                click: function (node) {
                  // console.log(appId); //node即为当前点击的节点数据
                  var res = node.id
                  $.ajax({
                    type: 'POST',
                    url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
                    data: JSON.stringify({
                      appId: res
                    }),
                    headers: {
                      'Content-Type': 'application/json;charset=utf-8'
                    },
                    dataType: 'json',
                    success: function (data) {
                      console.log(data)
                      laypage.render({
                        elem: 'page',
                        count: data.data.pageCount,
                        layout: ['prev', 'page', 'next', 'refresh', 'skip'],
                        // limit:10,
                        jump: function (obj, first) {
                          $.ajax({
                            type: 'POST',
                            url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                            data: JSON.stringify({ appId: res }),
                            headers: {
                              'Content-Type': 'application/json;charset=utf-8'
                            },
                            success: function (data) {
                              table.render({
                                elem: '#tableData',
                                data: data.data.content,
                                limit: 10,
                                id: 'batch',
                                cols: [
                                  [
                                    {
                                      checkbox: true
                                    },
                                    {
                                      field: 'type',
                                      title: '状态',
                                      // width: 60,
                                      templet: function (d) {
                                        if (d.state == '1') {
                                          // 如果活动状态为1 渲染绿色
                                          return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                        }
                                        if (d.state == '0' && d.leaveTime == '0') {
                                          return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                        }
                                        if (d.state == '0') {
                                          return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                        }
                                        if (d.state == null) {
                                          return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                        }
                                      },
                                      unresize: true
                                    },
                                    {
                                      field: 'type',
                                      title: '连接状态',
                                      templet: function (d) {
                                        if (d.connectionStatus == '1') {
                                          // 如果活动状态为1 渲染绿色
                                          return '<div class="connected">已连接</div>'
                                        }
                                        if (d.connectionStatus == '0') {
                                          // 如果活动状态为1 渲染绿色
                                          return '<div class="noConnected">未连接</div>'
                                        }
                                        if (d.connectionStatus == null) {
                                          // 如果活动状态为1 渲染绿色
                                          return '<div></div>'
                                        }
                                      },
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'realName',
                                      title: '用户',
                                      sort: true,
                                      event: 'hover',
                                      unresize: true
                                    },
                                    {
                                      field: 'systemUserName',
                                      title: '系统用户',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'departmentName',
                                      title: '科室',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'ip',
                                      title: 'IP',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'appName',
                                      title: '软件',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'equipmentName',
                                      title: '设备',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'port',
                                      title: '端口',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'connectionType',
                                      title: '连接方式',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'openTime',
                                      title: '打开时间',
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'leaveTime',
                                      title: '闲置时间',
                                      templet: function (d) {
                                        return d.leaveTime + '小时'
                                      },
                                      sort: true,
                                      unresize: true
                                    },
                                    {
                                      field: 'shareUsers',
                                      title: '共享信息',
                                      sort: true,
                                      unresize: true
                                    }
                                  ]
                                ]
                              })
                            }
                            //obj包含了当前分页的所有参数，比如：
                            // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                            // console.log(obj.limit); //得到每页显示的条数
                            // console.log(first);
                            //首次不执行
                            // if(!first){
                            //   // pageNum = obj.curr-1;
                            //   active.tableList(obj.curr,orgId,where,sort,column);
                            //   //do something
                            // }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
          $('body').on('mousedown', '.layui-tree a', function () {
            $('.layui-tree a').css('background', '')
            $(this).css('background', 'rgb(18, 160, 255)')
          })
          break
        case '3': // ;连接类型
          $('#treeList').html('')
          $.ajax({
            url: url + ':9006/AppConnection/getAppConnectionType',
            tyep: 'GET',
            success: function (res) {
              console.log(res.data)
              var res = res.data
              console.log(res)
              let li = ''
              let str = "<div id='page'></div>"
              for (let i = 0; i < res.length; i++) {
                // console.log(res[i].userName)
                // li += `<li data-value="${res[i].value}">${res[i].value}</li>`;
                li += '<div data-id=' + res[i].typeKey + '>' + res[i].value + '</div>'
              }
              $('#treeList').html('')
              $('#treeList').append(li)
              $('#paging').append(str)
              $('#treeList div').click(function () {
                $(this)
                  .css('background', 'rgb(18, 160, 255)')
                  .siblings()
                  .css('background', '')
                // 点击用户名称
                // $(this).css("color","red").siblings().css("color","");  // 高粱提示
                console.log($(this).attr('data-id'))
                var res = $(this).attr('data-id')
                // console.log(connectionType)
                parameter = {
                  connectionType: res,
                  departmentId: '',
                  objValue: '',
                  userId: '',
                  appId: ''
                }
                $.ajax({
                  type: 'POST',
                  url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
                  data: JSON.stringify({
                    connectionType: res
                  }),
                  headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                  },
                  dataType: 'json',
                  success: function (data) {
                    console.log(data)
                    laypage.render({
                      elem: 'page',
                      count: data.data.pageCount,
                      layout: ['prev', 'page', 'next', 'refresh', 'skip'],
                      // limit:10,
                      jump: function (obj, first) {
                        $.ajax({
                          type: 'POST',
                          url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                          data: JSON.stringify({ connectionType: res }),
                          headers: {
                            'Content-Type': 'application/json;charset=utf-8'
                          },
                          success: function (data) {
                            table.render({
                              elem: '#tableData',
                              data: data.data.content,
                              limit: 10,
                              id: 'batch',
                              cols: [
                                [
                                  {
                                    checkbox: true
                                  },
                                  {
                                    field: 'type',
                                    title: '状态',
                                    // width: 60,
                                    templet: function (d) {
                                      if (d.state == '1') {
                                        // 如果活动状态为1 渲染绿色
                                        return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                      }
                                      if (d.state == '0' && d.leaveTime == '0') {
                                        return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                      }
                                      if (d.state == '0') {
                                        return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                      }
                                      if (d.state == null) {
                                        return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                      }
                                    },
                                    unresize: true
                                  },
                                  {
                                    field: 'type',
                                    title: '连接状态',
                                    templet: function (d) {
                                      if (d.connectionStatus == '1') {
                                        // 如果活动状态为1 渲染绿色
                                        return '<div class="connected">已连接</div>'
                                      }
                                      if (d.connectionStatus == '0') {
                                        // 如果活动状态为1 渲染绿色
                                        return '<div class="noConnected">未连接</div>'
                                      }
                                      if (d.connectionStatus == null) {
                                        // 如果活动状态为1 渲染绿色
                                        return '<div></div>'
                                      }
                                    },
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'realName',
                                    title: '用户',
                                    sort: true,
                                    event: 'hover',
                                    unresize: true
                                  },
                                  {
                                    field: 'systemUserName',
                                    title: '系统用户',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'departmentName',
                                    title: '科室',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'ip',
                                    title: 'IP',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'appName',
                                    title: '软件',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'equipmentName',
                                    title: '设备',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'port',
                                    title: '端口',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'connectionType',
                                    title: '连接方式',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'openTime',
                                    title: '打开时间',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'leaveTime',
                                    title: '闲置时间',
                                    templet: function (d) {
                                      return d.leaveTime + '小时'
                                    },
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'shareUsers',
                                    title: '共享信息',
                                    sort: true,
                                    unresize: true
                                  }
                                ]
                              ]
                            })
                          }
                        })
                      }
                    })
                  }
                })
              })
              $('#treeList div')
                .mouseover(function () {
                  $(this).addClass('mouseover')
                })
                .mouseout(function () {
                  $(this).removeClass('mouseover')
                })
            }
          })
          break
        default:
          '1'
          break
      }
    })

    $('.text-right button').click(function () {
      // 连接管理-打开、关闭信息
      var checkStatus = table.checkStatus('batch'),
        data = checkStatus.data
      console.log(data)
      for (var i = 0; i < data.length; i++) {
        closeIds += `,${data[i].id}`
      }
      closeIds = closeIds.substring(1) // 去掉id串前面的逗号
      var openOrClose = $(this).attr('data-name')
      switch (openOrClose) {
        case 'open':
          console.log(openInfo)
          console.log(data.length)
          if (data.length == 1) {
            $.ajax({
              type: 'POST',
              url: url + ':9006/AppConnection/open',
              data: JSON.stringify({
                connId: openInfo.data.id
              }), // 传id
              headers: {
                'Content-Type': 'application/json;charset=utf-8'
              },
              dataType: 'json',
              success: function (data) {
                console.log(data)
                layer.msg(data.message)
                var data = data.data
                var form = $('<form></form>')
                  .attr('action', url + ':9006/AppConnection/downLoadVnc')
                  .attr('method', 'post')
                form.append(
                  $('<input></input>')
                    .attr('type', 'hidden')
                    .attr('name', 'fileName')
                    .attr('value', data[0])
                )
                form.append(
                  $('<input></input>')
                    .attr('type', 'hidden')
                    .attr('name', 'cont')
                    .attr('value', data[1])
                )
                form
                  .appendTo('body')
                  .submit()
                  .remove()
                $.ajax({
                  type: 'POST',
                  url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
                  data: JSON.stringify({}),
                  headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                  },
                  dataType: 'json',
                  success: function (data) {
                    // console.log(data)
                    layer.close(close) // 关闭加载层
                    laypage.render({
                      elem: 'page',
                      count: data.data.pageCount,
                      layout: ['prev', 'page', 'next', 'refresh', 'skip'],
                      // limit:10,
                      jump: function (obj, first) {
                        $.ajax({
                          type: 'POST',
                          url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                          data: JSON.stringify({}),
                          headers: {
                            'Content-Type': 'application/json;charset=utf-8'
                          },
                          success: function (data) {
                            table.render({
                              elem: '#tableData',
                              data: data.data.content,
                              limit: 10,
                              id: 'batch',
                              cols: [
                                [
                                  {
                                    checkbox: true
                                  },
                                  {
                                    field: 'type',
                                    title: '状态',
                                    // width: 60,
                                    templet: function (d) {
                                      if (d.state == '1') {
                                        // 如果活动状态为1 渲染绿色
                                        return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                      }
                                      if (d.state == '0' && d.leaveTime == '0') {
                                        return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                      }
                                      if (d.state == '0') {
                                        return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                      }
                                      if (d.state == null) {
                                        return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                      }
                                    },
                                    unresize: true
                                  },
                                  {
                                    field: 'type',
                                    title: '连接状态',
                                    templet: function (d) {
                                      if (d.connectionStatus == '1') {
                                        // 如果活动状态为1 渲染绿色
                                        return '<div class="connected">已连接</div>'
                                      }
                                      if (d.connectionStatus == '0') {
                                        // 如果活动状态为1 渲染绿色
                                        return '<div class="noConnected">未连接</div>'
                                      }
                                      if (d.connectionStatus == null) {
                                        // 如果活动状态为1 渲染绿色
                                        return '<div></div>'
                                      }
                                    },
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'realName',
                                    title: '用户',
                                    sort: true,
                                    event: 'hover',
                                    unresize: true
                                  },
                                  {
                                    field: 'systemUserName',
                                    title: '系统用户',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'departmentName',
                                    title: '科室',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'ip',
                                    title: 'IP',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'appName',
                                    title: '软件',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'equipmentName',
                                    title: '设备',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'port',
                                    title: '端口',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'connectionType',
                                    title: '连接方式',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'openTime',
                                    title: '打开时间',
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'leaveTime',
                                    title: '闲置时间',
                                    templet: function (d) {
                                      return d.leaveTime + '小时'
                                    },
                                    sort: true,
                                    unresize: true
                                  },
                                  {
                                    field: 'shareUsers',
                                    title: '共享信息',
                                    sort: true,
                                    unresize: true
                                  }
                                ]
                              ]
                            })
                          }
                          //obj包含了当前分页的所有参数，比如：
                          // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                          // console.log(obj.limit); //得到每页显示的条数
                          // console.log(first);
                          //首次不执行
                          // if(!first){
                          //   // pageNum = obj.curr-1;
                          //   active.tableList(obj.curr,orgId,where,sort,column);
                          //   //do something
                          // }
                        })
                      }
                    })
                  }
                })
              }
            })
          } else {
            layer.msg('请选择一条信息打开!')
          }
          closeIds = ''
          break
        case 'close':
          console.log(closeIds)
          var close = layer.load(0, {
            shade: [0.8, '#393D49']
          })
          $.ajax({
            type: 'POST',
            url: url + ':9006/AppConnection/close',
            data: JSON.stringify({
              ids: closeIds
            }),
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            dataType: 'json',
            success: function (data) {
              layer.msg(data.message)
              $.ajax({
                type: 'POST',
                url: url + ':9006/AppConnection/appConnectionPage/10/0/realName/desc',
                data: JSON.stringify({}),
                headers: {
                  'Content-Type': 'application/json;charset=utf-8'
                },
                dataType: 'json',
                success: function (data) {
                  // console.log(data)
                  layer.close(close) // 关闭加载层
                  laypage.render({
                    elem: 'page',
                    count: data.data.pageCount,
                    layout: ['prev', 'page', 'next', 'refresh', 'skip'],
                    // limit:10,
                    jump: function (obj, first) {
                      $.ajax({
                        type: 'POST',
                        url: url + ':9006/AppConnection/appConnectionPage/10' + '/' + obj.curr + '/realName/desc',
                        data: JSON.stringify({}),
                        headers: {
                          'Content-Type': 'application/json;charset=utf-8'
                        },
                        success: function (data) {
                          table.render({
                            elem: '#tableData',
                            data: data.data.content,
                            limit: 10,
                            id: 'batch',
                            cols: [
                              [
                                {
                                  checkbox: true
                                },
                                {
                                  field: 'type',
                                  title: '状态',
                                  // width: 60,
                                  templet: function (d) {
                                    if (d.state == '1') {
                                      // 如果活动状态为1 渲染绿色
                                      return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                    }
                                    if (d.state == '0' && d.leaveTime == '0') {
                                      return '<i class="layui-icon success" style="font-size:25px;color: green;">&#xe616;</i>'
                                    }
                                    if (d.state == '0') {
                                      return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                    }
                                    if (d.state == null) {
                                      return '<i class="layui-icon fail" style="font-size:25px;color: red;">&#x1007;</i>'
                                    }
                                  },
                                  unresize: true
                                },
                                {
                                  field: 'type',
                                  title: '连接状态',
                                  templet: function (d) {
                                    if (d.connectionStatus == '1') {
                                      // 如果活动状态为1 渲染绿色
                                      return '<div class="connected">已连接</div>'
                                    }
                                    if (d.connectionStatus == '0') {
                                      // 如果活动状态为1 渲染绿色
                                      return '<div class="noConnected">未连接</div>'
                                    }
                                    if (d.connectionStatus == null) {
                                      // 如果活动状态为1 渲染绿色
                                      return '<div></div>'
                                    }
                                  },
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'realName',
                                  title: '用户',
                                  sort: true,
                                  event: 'hover',
                                  unresize: true
                                },
                                {
                                  field: 'systemUserName',
                                  title: '系统用户',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'departmentName',
                                  title: '科室',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'ip',
                                  title: 'IP',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'appName',
                                  title: '软件',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'equipmentName',
                                  title: '设备',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'port',
                                  title: '端口',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'connectionType',
                                  title: '连接方式',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'openTime',
                                  title: '打开时间',
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'leaveTime',
                                  title: '闲置时间',
                                  templet: function (d) {
                                    return d.leaveTime + '小时'
                                  },
                                  sort: true,
                                  unresize: true
                                },
                                {
                                  field: 'shareUsers',
                                  title: '共享信息',
                                  sort: true,
                                  unresize: true
                                }
                              ]
                            ]
                          })
                        }
                        //obj包含了当前分页的所有参数，比如：
                        // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                        // console.log(obj.limit); //得到每页显示的条数
                        // console.log(first);
                        //首次不执行
                        // if(!first){
                        //   // pageNum = obj.curr-1;
                        //   active.tableList(obj.curr,orgId,where,sort,column);
                        //   //do something
                        // }
                      })
                    }
                  })
                }
              })
            }
          })
          closeIds = ''
          break
        default:
          break
      }
    })

    //鼠标悬浮
    $('#dataBox').on('mouseenter', 'table.layui-table td', function (params, obj) {
      // console.log(params,obj)
    })

    table.on('tool(tableData)', function (obj) {
      console.log(this)
      console.log(obj.data.userId)
      var infoId = obj.data.userId,
        _this = this
      $.ajax({
        type: 'POST',
        url: url + ':9006/AppConnection/getUserInfo',
        data: JSON.stringify({
          userId: infoId
        }),
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        dataType: 'json',
        success: function (data) {
          console.log(data)
          var data = data.data
          var info = `科室:${data.department}<br/>电话:${data.telephone}<br/>手机:${data.phone}`
          // layer.msg(data.data)
          layer.tips(info, _this, {
            tips: [2, '#78BA32']
          })
        }
      })
    })
  })

  $('body').on('mouseenter', 'table.layui-table td', function (params) {
    if (this.getAttribute('data-field') == 0) {
      return false
    }
    if (this.getAttribute('data-field') == 'type') {
    } else {
      if ($(this).text() != '') {
        layer.tips($(this).text(), $(this), {
          tips: [2, '#78BA32']
        })
      }
    }
  })
  $('body').on('mouseenter', '.success', function (params) {
    layer.tips('活动中', $(this), {
      tips: [2, '#78BA32']
    })
  })
  $('body').on('mouseenter', '.fail', function (params) {
    layer.tips('未活动', $(this), {
      tips: [2, '#78BA32']
    })
  })

  function timestampToTime (timestamp) {
    var date = new Date(timestamp) //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-'
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-'
    D = date.getDate() + ' '
    h = date.getHours() + ':'
    m = date.getMinutes() + ':'
    s = date.getSeconds()
    return Y + M + D + h + m + s
  }
})()
